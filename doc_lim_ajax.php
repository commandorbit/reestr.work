<?php
require 'init.php';
$fp_year = (int) $_GET['fp_year'];
$zfo_id = (int) $_GET['zfo_id'];
$fin_source_id = (int) $_GET['fin_source_id'];

$sql = "select DISTINCT pre.smeta_id, CONCAT('#',pre.smeta_id,' /',zfo.name,'/ ',pre.description) name  
FROM 
(
SELECT DISTINCT ss.smeta_id,s.zfo_id, s.description 
FROM smeta_sostav ss INNER JOIN smeta s ON ss.smeta_id=s.id 
WHERE s.fp_year_id = $fp_year AND ss.fin_source_id = $fin_source_id AND (ss.zfo_id = $zfo_id OR ss.zfo_id IS NULL AND s.zfo_id = $zfo_id)
UNION SELECT DISTINCT
	sf.smeta_id,smeta.zfo_id, smeta.description 
	FROM smeta_fin_source sf
	INNER JOIN smeta ON sf.smeta_id = smeta.id
WHERE (smeta.fp_year_id = $fp_year AND sf.fin_source_id = $fin_source_id AND smeta.zfo_id = $zfo_id )
) pre INNER JOIN zfo ON pre.zfo_id = zfo.id 

ORDER BY pre.smeta_id";

$rows = sql_to_assoc($sql);

echo json_encode(['result'=>$rows],JSON_UNESCAPED_UNICODE);