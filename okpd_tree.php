<?php

require $_SERVER['DOCUMENT_ROOT'] . '/init.php';
require $_SERVER['DOCUMENT_ROOT'] . '/template/header.php';

$rows = sql_rows("SELECT code as id, name, parent_code as parent_id FROM okpd");

?>

<div class="tree" style="width: 100px; height: 100px; background: #fff"></div>

<script>
	var element = document.querySelector('.tree');
	var obj = libUnicorn.TreePlugin.create(element, false, {opened: false, checkboxes: true});
	obj.setOptions(<?=json_encode($rows)?>);
	new resizable(obj.container, {directions: ["southeast"]});
</script>