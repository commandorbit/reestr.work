<?php
	$title = 'Профиль пользователя';
	include $_SERVER['DOCUMENT_ROOT'] . '/init.php';
	
	$update_result = '';
	$user_id = $_SESSION['user_id'];
	$sql_result = 0;
	$update_result = '';
	if (isset($_REQUEST['delete_pictute']))
	{
		$id = $_REQUEST['user_id'];
		$sql = "UPDATE user SET picture = NULL, picture_left = 0, picture_top = 0, picture_width = 100 WHERE id = '$id'";
		$update_result = 'Фотография удалена<br/>';
		sql_query($sql);
	}
	if (isset($_REQUEST['save_file']))
	{
		//Фото
		$id = $_REQUEST['user_id'];
		$pictures_folder = '/img/users/';
		$path = $_SERVER['DOCUMENT_ROOT'] . $pictures_folder;
		//$new_name = $path . $_FILES['userfile']['name'];		
		$tmp_name = $_FILES['userfile']['tmp_name'];
		$fileinfo = new SplFileInfo($_FILES['userfile']['name']);
		$new_name = $path . $user_id . '.' . $fileinfo->getExtension();
		if (is_uploaded_file($tmp_name))
		{
			copy($tmp_name, $new_name);
			$photo_url = $pictures_folder . $user_id . '.' . $fileinfo->getExtension();
			$sql = "UPDATE user SET picture = '$photo_url', picture_left = 0, picture_top = 0, picture_width = 100 WHERE id = '$id'";
			$sql_result = sql_query($sql);
			//echo $sql_result;
			$update_result .= "Фотография загружена $photo_url<br/>";
			//$_SESSION["photo_url"] = $photo_url;
		}
	}
	$user_profile = sql_rows("SELECT * FROM user WHERE id = $user_id");	
	if (isset($_REQUEST['save'])|| $update_result)
	{
		//Данные профиля
		$fio = $_REQUEST['lastname'] . ' ' . $_REQUEST['firstname'] . ' ' . $_REQUEST['middlename'];
		$email = $_REQUEST['email'];
		$id = $_REQUEST['user_id'];
		$sql = "UPDATE user SET fio = '$fio', email = '$email' WHERE id = '$id'";
		$sql_result = sql_query($sql);
		$user_palette = $_REQUEST['user_palette'];
		$sql = "UPDATE user SET palette = '$user_palette' WHERE id = '$id'";
		$sql_result = $sql_result && sql_query($sql);
		$update_result .= 'Данные профиля сохранены';
		//Пароль
		$password = $_REQUEST['password'];
		$password1 = $_REQUEST['password1'];
		$password2 = $_REQUEST['password2'];
		if ($password || $password1 || $password1)
		{
			if (md5($password) == $user_profile[0]['pass'])
			{
				if ($password1 == $password2)
				{
					if ($password1)
					{
						//MD5('123') == '202cb962ac59075b964b07152d234b70'
						$sql = "UPDATE user SET pass = '" . MD5($password1) . "' WHERE id = '$id'";
						$sql_result = $sql_result && sql_query($sql);
						if ($sql_result)
							$update_result .= '<br/>Пароль изменён';
						else
							$update_result .= '<br/>Произошла какая-то ошибка при изменении пароля<br/>Пароль не был изменён';
					}
					else						
						$update_result .= '<br/>Новый пароль не должен быть пустым<br/>Пароль не был изменён';
				}
				else
					$update_result .= '<br/>Ввеедённые пароли не совпадают<br/>Пароль не был изменён';
			}
			else
				$update_result .= '<br/>Старый пароль введён неверно<br/>Пароль не был изменён';
		}
		//Картинка
		if ($user_profile[0]['picture'])
		{
			$picture_left = $_REQUEST['picture_left'];
			$picture_top = $_REQUEST['picture_top'];
			$picture_width = $_REQUEST['picture_width'];
			$sql = "UPDATE user SET picture_left = '$picture_left', picture_top = '$picture_top', picture_width = '$picture_width' WHERE id = '$id'";
			$sql_result = $sql_result && sql_query($sql);
			$update_result .= '<br/>Данные фотографии сохранены';
		}
	}
	
	include $_SERVER['DOCUMENT_ROOT'] . '/template/header.php';
	
	require_once($_SERVER['DOCUMENT_ROOT'] . "/modules/avdfunctions.php");

	$user_profile = sql_rows("SELECT * FROM user WHERE id = $user_id");	
?>
<? //echo '<pre>'; var_dump($user_profile); die() ?>
<link rel="stylesheet" type="text/css" href="/css/background<?=$_SESSION["palette"]?>.css?ver=<?=VER?>">
<div class="center">
<div class="php-dialog" data-moveable="true">
<br>
<h1><?=$title?></h1>
<form id="userForm" method="post" enctype="multipart/form-data">
<table>
<tr>
<td>
<?php
	//
	function input($key, $value, $type = "text")
	{
		return '<input type="' . $type . '" name="' . $key . '" value = "' . $value . '" />';
	}
	//echo $user_id 
	$fullname = explode(" ", $user_profile[0]['fio']);
	if (!isset($fullname[0])) $fullname[0] = '';
	if (!isset($fullname[1])) $fullname[1] = '';
	if (!isset($fullname[2])) $fullname[2] = '';
	$user_data = Array();
	$user_data['Логин'] = $user_profile[0]['login'];
	$user_data['Фамилия'] = input('lastname', $fullname[0]);
	$user_data['Имя'] = input('firstname', $fullname[1]);
	$user_data['Отчество'] = input('middlename', $fullname[2]);
	$user_data['E-mail'] = input('email', $user_profile[0]['email']);
	$user_palette = $user_profile[0]['palette'];
	$user_data['Оформление'] = '<select name="user_palette">' 
		. '<option ' . ($user_palette == 1 ? "selected" : "") . ' value="1">Тема 1 (Оранжевая)</option>'
		. '<option ' . ($user_palette == 2 ? "selected" : "") . ' value="2">Тема 2 (Серая)</option>'
		. '<option ' . ($user_palette == 3 ? "selected" : "") . ' value="3">Тема 3 (Синяя)</option>'
		. '<option ' . ($user_palette == 4 ? "selected" : "") . ' value="4">Тема 4 (Зелёная)</option>'
		. '</select>';
	$user_data2 = Array();
	$user_data2['Старый пароль'] = input('password', '', 'password');
	$user_data2['Новый пароль'] = input('password1', '', 'password');
	$user_data2['Подтвердите'] = input('password2', '', 'password');
	echo '<input type="hidden" name="user_id" value="' . $user_id . '"/>';
	avd_print_assoc_values($user_data);
	echo '<table><tr><th>Для смены пароля введите старый, <br>а затем дважды укажите новый пароль</th></tr></table>';
	avd_print_assoc_values($user_data2);
?>
</td>
<td>
	<div style="text-align:center;">
	<div class="profile_pic" style="margin: 0 auto;">
		<div class="profile_pic2" data-tooltip="Перемещайте изображание с помощью мыши">
			<img alt="" id="userpic" ondrag="return false" ondragstart="return false" data-moveable="true" 
				style="position: relative; left: <? echo $user_profile[0]['picture_left'] . 'px' ?>; top: <? echo $user_profile[0]['picture_top'] . 'px' ?>; width: <? $w = $user_profile[0]['picture_width']; if (!$w) $w = 100; echo $w . '%' ?>"
				src="<? echo $user_profile[0]['picture'] ? $user_profile[0]['picture'] : '/img/users/default-user.png'; // user_pic(200) // ?>">
		</div>
		<!-- input id="fileselect" name="userfile" type="file" class="file" data-tooltip="Загрузить фотографию" -->
	</div>
	<? 
		//if (isset($_SESSION["photo_url"])) echo $_SESSION["photo_url"] . '<br>'; 
		//echo $user_profile[0]['picture'] ? $user_profile[0]['picture'] : '/img/users/default-user.png';
	?>
	<!--
	<img src="/img/png/delete.png"><br/>
	<input name="delete_pictute" type="submit" class="delete" value="Удалить фотографию"  data-tooltip="Удалить фотографию">
	-->
	<br>
	<div id="pictureinfo">...</div>
	<br>
	
	<input type="button" onclick="pictureReduce()" value="-" data-tooltip="Уменьшить"/>
	<input type="button" onclick="pictureReset()" value="X" data-tooltip="Сброс"/>
	<input type="button" onclick="pictureIncrease()" value="+" data-tooltip="Увеличить"/>
	<hr>
	<input id="fileselect" name="userfile" type="file" data-tooltip="Загрузить фотографию"><hr>
	<input name="delete_pictute" type="submit" value="Удалить фотографию"  data-tooltip="Удалить фотографию">
	</div>
</td>
</tr>
</table>
<br/><div style="text-align:center">
<input type="hidden" id="save_file" name="save_file" value="">
<input type="hidden" id="picture_left" name="picture_left" value="">
<input type="hidden" id="picture_top" name="picture_top" value="">
<input type="hidden" id="picture_width" name="picture_width" value="">
<input name="save" type="submit" value="Сохранить изменения">
&nbsp;&nbsp;&nbsp;
<input name="cancel" type="submit" value="Отмена">
</div>
</form>
</div>
<?php
	//echo $user_id 
	//avd_print_assoc_values($user_profile[0]);	
	if ($update_result)
	{
		echo '<p data-hide-timeout="5000"><div class="tooltip" data-hide-timeout="5000">' . $update_result . '</div></p>';
	}
	//echo '<br/><br/><div class="tooltip" data-hide-timeout="15000">'; avd_print_assoc_values($_REQUEST);	echo '</div>';	
?>
</div>
<script>

	'use strict';
	
	var fse = document.getElementById('fileselect');
	if (fse)
	{
		//alert('Найден');
		fse.addEventListener("change",
		function(e) 
		{
			//alert('Значение изменилось');
			var form = document.getElementById("userForm");
			if (form)
			{
				var save_file = document.getElementById("save_file");
				if (save_file)
				{
					save_file.setAttribute('value', 1);
					pictureReset();
					form.submit();
				}
			}
		});
	}
	
	function updatePictureInfo()
	{
		var pi = document.getElementById('pictureinfo');
		var pic = document.getElementById('userpic');
		if (pi)
			if (pic)
			{
				pi.innerHTML = '' + pic.style.left + ', ' + pic.style.top + ', ' + pic.style.width;

				var picture_left = document.getElementById("picture_left");
				if (picture_left)
					picture_left.setAttribute('value', parseInt(pic.style.left) || 0);

				var picture_top = document.getElementById("picture_top");
				if (picture_top)
					picture_top.setAttribute('value', parseInt(pic.style.top) || 0);

				var picture_width = document.getElementById("picture_width");
				if (picture_width)
					picture_width.setAttribute('value', parseInt(pic.style.width) || 100);				
			}		
	}
	
	function pictureIncrease()
	{
		//alert('++++');
		var pic = document.getElementById('userpic');
		if (pic)
		{
			var w = parseInt(pic.style.width);
			if (!w) w = 100;
			w *= 1.1;
			if (Math.abs(w - 100) < 8) w = 100;			
			pic.style.width = w + '%';
			
			var cx = parseInt(pic.style.left) - 100;
			var cy = parseInt(pic.style.top)  - 100;
			cx *= 1.1;
			cy *= 1.1;
			cx += 100;
			cy += 100;
			if (Math.abs(cx) < 8) cx = 0;
			if (Math.abs(cy) < 8) cy = 0;
			pic.style.left = cx + 'px';
			pic.style.top  = cy + 'px';
			
			updatePictureInfo();			
		}
	}
	
	function pictureReduce()
	{
		//alert('----');
		var pic = document.getElementById('userpic');
		if (pic)
		{
			var w = parseInt(pic.style.width);
			if (!w) w = 100;
			w /= 1.1;
			if (Math.abs(w - 100) < 8) w = 100;
			pic.style.width = w + '%';

			var cx = parseInt(pic.style.left) - 100;
			var cy = parseInt(pic.style.top)  - 100;
			cx /= 1.1;
			cy /= 1.1;
			cx += 100;
			cy += 100;
			if (Math.abs(cx) < 8) cx = 0;
			if (Math.abs(cy) < 8) cy = 0;
			pic.style.left = cx + 'px';
			pic.style.top  = cy + 'px';
			
			updatePictureInfo();
		}
	}
	
	function pictureReset()
	{
		//alert('----');
		var pic = document.getElementById('userpic');
		if (pic)
		{
			var w = 100;
			pic.style.width = w + '%';
			pic.style.left = '0px';
			pic.style.top  = '0px';
			updatePictureInfo();
		}
	}
	
	document.addEventListener("mousemove", function(event)
	{
		updatePictureInfo();
	});
	
	window.addEventListener('load', function() {updatePictureInfo();});
	
</script>
<?php
	include $_SERVER['DOCUMENT_ROOT'] . '/template/footer.php';
?>
