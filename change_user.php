<?php

include $_SERVER['DOCUMENT_ROOT'] . '/init.php';

if((is_admin() || is_admin_changed()) && isset($_GET['newer_user_id'])) {
	$newer_user_id = (int) sql_escape($_GET['newer_user_id']);
	change_user($newer_user_id);
	header("Location: " . $_SERVER['HTTP_REFERER']);
} else {
	header("Location: index.php");
}