<?php

//define('NO_THESIS',true);

function thesis_reestr_guids_sql() {
	$_GUID_CAT_SUP_IDs = ['E6B65CF0-78E9-45C2-AB37-635293F5F53D','D02D3F44-693D-7132-C193-8A3C03A8E2BF'];
	return  implode(',',array_map(function($el) {return "'$el'";}, $_GUID_CAT_SUP_IDs));
}

$thesis_connect = thesis_connect();

function thesis_connect() {
	if (defined('NO_THESIS')) return false;
	if (!function_exists('mssql_connect')) {
		if (!defined('SHARED_PHP_PATH')) {
			define ('SHARED_PHP_PATH','/home/dekanat/modules');
		}
		require_once (SHARED_PHP_PATH . '/mssql/mssql_pdo.php');
	}
	//$db_host = 'thesis';
	$db_host = '10.145.1.29';
	$db_user = 'achernov';
	$db_pass = '9966asD';
	return mssql_connect($db_host, $db_user, $db_pass);
}

function get_thesis_guid($dogovor_id) {
	global $thesis_connect;
	if (!$thesis_connect) return false;
	if($dogovor_id === 'new') return false;
	$bad_guids = get_old_resstr_guids();
	$guids = thesis_reestr_guids_sql();
	$guid_query = mssql_query("SELECT dd.CARD_ID FROM dbo.SYS_ATTR_VALUE av INNER JOIN dbo.DF_DOC dd  ON av.ENTITY_ID=dd.CARD_ID 
	WHERE av.CATEGORY_ATTR_ID in ($guids) 
	AND INTEGER_VALUE = $dogovor_id 
	AND av.ENTITY_ID not in ('$bad_guids') 
	AND dd.VERSION_OF_ID is null ORDER BY dd.CREATE_TS desc",$thesis_connect);
	$guid = mssql_fetch_array($guid_query,MSSQL_ASSOC);
	return ($guid) ? mssql_guid_string($guid['CARD_ID']) : false;
}

function get_old_resstr_guids() {
	$guids_sql = sql_query("SELECT guid FROM thesis_old_reestr");
	$res_arr = array();
	while($row = sql_array($guids_sql)) {
		$res_arr[] = $row['guid'];
	}
	$result = implode('\', \'', $res_arr);
	return $result;
}

function thesis_db_find_short_url($entity_id) {
	global $thesis_connect;
	$entity_id = strtolower($entity_id);
	$pattern = '/open?screen=df$Contract.edit&item=df$Contract-'.$entity_id.'&params=item:df$Contract-'.$entity_id;
	$sql = "SELECT SHORT_URL FROM dbo.DF_SHORT_URL WHERE LONG_URL = '$pattern'";
	$urls_query = mssql_query($sql,$thesis_connect);
	$urls = mssql_fetch_array($urls_query,MSSQL_ASSOC);
	return $urls ? $urls['SHORT_URL'] : false;
}

function get_thesis_lnk($dogovor_id) {
	if($dogovor_id === 'new') return false;
	$entity = get_thesis_guid($dogovor_id);
	$short_url = ($entity) ? thesis_db_find_short_url($entity) : false;
	return $short_url;
}

function get_thesis_status($dogovor_id) {
	global $thesis_connect;
	$card_id = get_thesis_guid($dogovor_id);
	$query = mssql_query("SELECT STATE FROM WF_CARD_PROC WHERE CARD_ID = '$card_id'",$thesis_connect);
	$result = mssql_fetch_array($query,MSSQL_ASSOC);	
	return ($result) ? $result['STATE'] : false;
}

function get_thesis_status_str($str) {
	$stat_list = [
	'Ok'=>'ОК',
	'Soglasovat'=>'Согласовать',
	'Otklonit'=>'Отклонить',
	'Otpravit_povtorno'=>'Отправить повторно',
	'Dogovor_zakluchen'=>'Договор заключен',
	'Da'=>'Да',
	'FinishByInitiator'=>'Завершен инициатором',
	'Complete'=>'Завершен',
	'Zakluchenie_dogovora_kontrakta'=>'Заключение договора/контракта',
	'Rassmotrenie_PU'=>'Рассмотрение в ПУ',
	'Rassmotrenie_v_OMA'=>'Рассмотрение в ОМА',
	'Rassmotrenie_v_OSK'=>'Рассмотрение в ОСК',
	'Rassmotrenie_v_OB'=>'Рассмотрение в ОУУ', // ОБ в ОУУ - просьба Сезелневой от 13,10,2017
	'Prorektor_po_napravleniu'=>'Проректор по направлению',
	'Nachalnik_CFO'=>'Начальник ЦФО',
	'Started'=>'Запущен',
	'Nachalnik_FEU'=>'Начальник ФЭУ',
	'Nachalnik_UBU'=>'Начальник УБУ',
	'Provedenie_zakupki'=>'Проведение закупки',
	'Rassmotrenie_ORD'=>'Рассмотрение ОРД',
	'Rassmotrenie_SOK'=>'Рассмотрение СОК',
	'Sotrudnik_dekanata'=>'Сотруденик деканата',
	'Canceled'=>'Отменен'
	];
	
	$str_no_comma = str_replace(',','',$str);
	return (isset($stat_list[$str_no_comma])) ? $stat_list[$str_no_comma] : $str_no_comma;
}