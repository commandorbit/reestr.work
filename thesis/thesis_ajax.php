<?php 

include $_SERVER['DOCUMENT_ROOT'] . '/init.php';
require_once __DIR__ . '/thesis_functions.php';

if (isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) === 'xmlhttprequest') {
	if (!isset($_SESSION['user_id']) or time_out()) {
		echo json_encode(array('status'=>'notok', 'result'=>'Время сессии истекло, либо вы не авторизированны в системе'));
		exit(0);
	}
	
	if(isset($_GET['guid']) && isset($_GET['proc'])) {
		$thesis_card_id = sql_escape($_GET['guid']);
		$proc_id = sql_escape($_GET['proc']);
		
		$assignment_col_names = array(
			"PROC_NAME"=>"Процесс",
			"CREATE_TS"=>"Дата поступления",
			"FINISHED"=>"Завершено",
			"USER_NAME"=>"Пользователь",
			"STATE"=>"Состояние",
			"ITTERATION"=>"Итерация",
			"OUTCOME"=>"Результат",
			"ASSIGNMENT_COMMENT"=>"Комментарий"
		);

		$html = '<br/><br /><h1>Процесс согласования договора в "ТЕЗИС"</h1>';
		$html .= '<table class="td-table" style="margin-top: 20px;">';
		$html .= '<tr>';
		foreach($assignment_col_names as $name) {
			$html .= '<th>' . $name . '</th>';
		}
		$html .= '</tr>';
		
		$thesis_assignment_query = mssql_query("SELECT dwp.NAME as PROC_NAME, dwa.CREATE_TS, dwa.FINISHED, dsu.NAME as USER_NAME, dwa.NAME as STATE, dwa.ITERATION, dwa.OUTCOME, dwa.ASSIGNMENT_COMMENT FROM dbo.WF_ASSIGNMENT dwa LEFT JOIN dbo.SEC_USER dsu ON dsu.ID = dwa.USER_ID LEFT JOIN dbo.WF_PROC dwp on dwa.PROC_ID = dwp.ID LEFT JOIN dbo.DF_DOC dd ON dd.CARD_ID = dwa.CARD_ID WHERE dwa.CARD_ID = '$thesis_card_id' AND dwa.PROC_ID = '$proc_id'",$thesis_connect);
		
		while($assignment = mssql_fetch_array($thesis_assignment_query) ) {
			$html .= '<tr>';
			foreach($assignment as $fld=>$val) {
				if ($fld == "STATE" || $fld == 'OUTCOME') $val = get_thesis_status_str($val);
				$val = (!($timestamp = strtotime($val))) ? $val : date('d.m.Y H:i', $timestamp);
				$html .= '<td>' . $val . '</td>';
			}
			$html .= '</tr>';
		}
		$html .= '</table>';
		//print_r($html);
		echo json_encode(array('status'=>'OK', 'result'=>$html));
	}
}

?>