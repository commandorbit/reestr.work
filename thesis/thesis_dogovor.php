<?php
/*
 *	Подключаем init
 */
include $_SERVER['DOCUMENT_ROOT'] . '/init.php';
require_once __DIR__ . '/thesis_functions.php';

$soup_dogovor_id = (int) sql_escape($_GET['id']);

/*
 *	Set title
 */
$title = 'ТЕЗИС / Договор ID: ' . $soup_dogovor_id;
/*
 *	Подключаем header
 */
include $_SERVER['DOCUMENT_ROOT'] . '/template/header.php';

/*
 *	Получаем данные по договору из БД Тезиса
 */
 $thesis_card_id = get_thesis_guid($soup_dogovor_id);
 $sql = "SELECT wp.NAME as PROC_NAME, dd.NUMBER, wcp.STATE, wcp.PROC_ID, dd.CARD_ID, dd.COMMENT, dd.INCOME_DATE, ddc.AMOUNT FROM dbo.SYS_ATTR_VALUE av INNER JOIN dbo.DF_DOC dd  ON av.ENTITY_ID=dd.CARD_ID INNER JOIN dbo.DF_CONTRACT ddc ON dd.CARD_ID = ddc.CARD_ID INNER JOIN dbo.WF_CARD_PROC wcp ON wcp.CARD_ID = dd.CARD_ID INNER JOIN dbo.WF_PROC wp ON wp.ID = wcp.PROC_ID WHERE 
 CATEGORY_ATTR_ID in (".thesis_reestr_guids_sql().") 
 AND INTEGER_VALUE = $soup_dogovor_id AND dd.VERSION_OF_ID is null ORDER BY dd.CREATE_TS desc, wcp.CREATE_TS DESC";
 //$sql = "SELECT ddd.NUMBER, ddd.COMMENT, ddd.INCOME_DATE, ddc.AMOUNT FROM dbo.DF_DOC ddd  LEFT JOIN dbo.DF_CONTRACT ddc ON ddd.CARD_ID = ddc.CARD_ID WHERE ddd.CARD_ID='$thesis_card_id'";
 
 
$thesis_attrs_query = mssql_query($sql,$thesis_connect);
$thesis_attrs = [];
while ($r = mssql_fetch_array($thesis_attrs_query)) {
	$thesis_attrs[] = $r;	
}
$thesis_attr_first = $thesis_attrs ? $thesis_attrs[0] : false;

$rus_current_status = (count($thesis_attrs) && !is_null($thesis_attrs[0]['STATE'])) ?  get_thesis_status_str($thesis_attrs[0]['STATE'])  : NULL;

$attrs_col_names = array(
	"PROC_NAME"=>"Процесс",
	"NUMBER" => "Номер в \"ТЕЗИС\"",
	"STATE"=>"Состояние",
	"COMMENT" => "Предмет договора",
	"INCOME_DATE" => "Дата создания",
	"AMOUNT" => "Сумма"
);

?>
<div class="thesis-info">
	<h1>Информация по договору ID: <?=$soup_dogovor_id?></h1>

	<? if($rus_current_status): ?>
	<h3>Текущий статус: <?=$rus_current_status?></h3>
	<? endif; ?>

	<?php
	if($thesis_lnk = get_thesis_lnk($soup_dogovor_id)) {
		echo '<div style="width: 100%; line-height: 30px; text-align: center;"><a href="http://10.145.1.29:8080/app/o?u='. $thesis_lnk .'" target="_blank">Договор в системе "Тезис"</a></div>';
	}
	?>

	<table class="td-table" style="margin-top: 20px;" id="versions">
		<tr>
			<?php foreach($attrs_col_names as $name): ?>
				<th><?=$name?></th>
			<?php endforeach; ?>
		</tr>
		<tr>
			<?php foreach($thesis_attrs as $row): ?>
				<tr class="thesis_ver" data-guid="<?=mssql_guid_string($row['CARD_ID'])?>" data-proc="<?=mssql_guid_string($row['PROC_ID'])?>">
					<?php foreach($row as $k=>$attr): ?>
						<?php if($k == 'CARD_ID' || $k == 'PROC_ID') continue; ?>
						<? if($k == 'STATE') $attr = get_thesis_status_str($attr);?>
						<td><?=($k == 'INCOME_DATE' && $attr) ? date('d.m.Y', strtotime($attr)) : $attr?></td>
					<?php endforeach; ?>
				</tr>
			<?php endforeach; ?>
		</tr>
	</table>
</div>
<div class="reloaded"></div>
<div class="spinner"></div>

<script>
	window.onload = function() {
		var ver_table = document.querySelector('#versions');
		var thesis_vers = ver_table.querySelectorAll('.thesis_ver');
		var first_ver = ver_table.querySelector('.thesis_ver');
		var reloaded_part = document.querySelector('.reloaded');
		var spinner = document.querySelector('.spinner');
		var active = null;
		for(var i = 0; i < thesis_vers.length; i++) {
			thesis_vers[i].addEventListener('click', get_process);
		}
		
		var evt = document.createEvent("HTMLEvents");
		evt.initEvent("click", false, true);
		first_ver.dispatchEvent(evt);
		
		function get_process(e) {
			var self = this;
			self.classList.add('active');
			if(active != null && active != self) active.classList.remove('active');
			active = self;
			var guid = this.getAttribute('data-guid');
			var proc = this.getAttribute('data-proc');
			$.ajax({
				type: "GET",
				url: "thesis_ajax.php",
				data: "guid="+guid+"&proc="+proc,
				success: function(data){
					spinner.style.display = 'none';
					var data = JSON.parse(data);
					if(data.status == 'OK') {
						reloaded_part.innerHTML = data.result;
					} else {
						alert(data.result);
						location.href = '/login.php?url='+encodeURIComponent(document.referrer);
					}
				},
				beforeSend: function() {
					spinner.style.display = 'block';
				}
			});
		}
	}
</script>