<?php
define ("CONTROL_EXAM",1);
define ("CONTROL_ZACHET",2);
define ("CONTROL_DIFF_ZACHET",3);
define ('OSN_SESS',0);
define ('DOP_SESS',1);
define ('DOUCH',2);
define ('DOC_VEDOMOST',1);
define ('DOC_NAPRAVLENIE',2);
define ('CD_EXAM',20);
if (!defined('ENT_HTML401')) define('ENT_HTML401',0);

define('LESSON_TYPE_LECT',1);
define('LESSON_TYPE_PRACT',2);
define('LESSON_TYPE_LAB',3);
define('LESSON_TYPE_EXAM',4);
define('LESSON_TYPE_ZACH',5);

// ��������
function check_mx_exist($email) {
	$hosts=array();
	list($user,$domain)=explode('@',$email);
	return getmxrr($domain,$hosts);
}

function email_validate($email) {
	if (!preg_match("/^([a-zA-Z0-9])+([a-zA-Z0-9\._-])*@([a-zA-Z0-9_-])+([a-zA-Z0-9\._-]+)+$/" , $email)) {
		return false;
	} else {
		return check_mx_exist($email);
	}
}

function mail_utf_encode($s,$charset) {
	if ($charset == 'windows-1251') $s = iconv('cp1251','utf-8',$s);
	return '=?utf-8?b?' . base64_encode($s) . '?=';
}

function send_mail_utf($email,$subject,$body,$from='priem@finec.ru') {
	return send_mail($email,$subject,$body,$from,'utf-8');
}

function send_mail_win($email,$subject,$body,$from='priem@finec.ru') {
	return send_mail($email,$subject,$body,$from,'windows-1251');
}

function send_mail($email,$subject,$body,$from='priem@finec.ru',$charset='windows-1251') {
	$headers = 'MIME-Version: 1.0' . "\r\n" ;
	$headers .= "From: $from\r\n";
	$headers .= "Reply-To: $from\r\n";
	$headers .= 'Content-type: text/html; charset="'.$charset.'"';
	/* ��������� ����� ������� user www � trusted users � sendmail.conf */
	if (mail ($email,mail_utf_encode($subject,$charset),$body,$headers,"-f$from")) return true;
}

// ���������������

function ip_is_local($ip) {
	$is = false;
	$a = explode(".",$ip);
	if (is_dim()) {
		$is = false;
	} elseif (count($a)==4) {
		$is = $a[0]=='10' || ($a[0]=='169' && $a[1]=='254')	|| ($a[0]=='192' && $a[1]=='168');
	}
	return $is;
}

function request_val($name,$def) {
	return (isset($_REQUEST[$name]) && is_string($_REQUEST[$name])) ? sql_escape($_REQUEST[$name]) : $def;
}

function request_val_noescape($name,$def) {
	return (isset($_REQUEST[$name]) && is_string($_REQUEST[$name])) ? $_REQUEST[$name] : $def;
}

function request_numeric_val($name,$def) {
	return (isset($_REQUEST[$name]) && is_numeric($_REQUEST[$name])) ? sql_escape((int)$_REQUEST[$name]) : $def;
}

function session_val($key,$default = '') {
	return (isset($_SESSION[$key])) ? $_SESSION[$key] : $default;
}

function is_dim() {
	//return ($_SERVER['REMOTE_ADDR']=='10.123.124.210' || current_user_kod()==1);
	return ($_SERVER['REMOTE_ADDR']=='10.123.124.210');
}

function is_alex() {
	//return ($_SERVER['REMOTE_ADDR']=='10.123.124.215' || current_user_kod()==34);
	return ($_SERVER['REMOTE_ADDR']=='10.123.124.215');
}

function is_petroff() {
	return ($_SERVER['REMOTE_ADDR']=='10.174.0.229');
}

function is_gol() {
	return ($_SERVER['REMOTE_ADDR']=='10.123.124.220');
}

function is_developer() {
	return is_dim() || is_alex() || is_gol() || is_petroff();
}

function send_dim($text) {
	send_mail('dim@unecon.ru','-?-',$text);
}

function send_gol($text) {
	send_mail('akg@finec.ru','-?-',$text);
}

function leader_zero($int,$len) {
	$str = (string)$int;
	if (is_numeric($int) && $int > 0) while (strlen($str) < $len) $str = '0'.$str;
	return $str;
}

function date_2format($date,$new_format) {
	$new_date = '';
	if ($date) {
		$date = new DateTime($date);
		$new_date = $date->format($new_format);
	}
	return $new_date;
}

function date_add_interval($d,$add_interval){
	$date = date_create($d);
	date_modify($date,$add_interval);
	return date_format($date, 'Y-m-d');
}

function datetime_add_interval($dt,$add_interval){
	return date('Y-m-d H:i:s',strtotime($add_interval,strtotime($dt)));
}

function date_difference($date_1,$date_2, $format = '%a') {
	if (!$date_1 || !$date_2) return false;
	$datetime1 = date_create($date_1);
	$datetime2 = date_create($date_2);
	$interval = date_diff($datetime1, $datetime2);
	return $interval->format($format);
}

function is_date($date) {
	// ������ ���� �����
	$is = false;
	if ($date) {
		$d = date_create($date);
		if ($d) {
			$year = $d->format('Y');
			$month = $d->format('m');
			$day = $d->format('d');
			$is = (checkdate($month, $day, $year)) ? true : false;
		}
	}
	return $is;
}

function html_special_chars($text) {
	if (phpversion() == '5.3.3' || phpversion() == '5.3.13') return htmlspecialchars($text,ENT_COMPAT,'cp1251');
	else return htmlspecialchars($text,ENT_COMPAT | ENT_HTML401,'cp1251');
}

function array_key_value($array,$key) {
	$value = '';
	if (in_array(gettype($key),array('string','integer'))) {
		if (array_key_exists($key,$array)) $value = $array[$key];
	}
	return $value;
}


/************************************************/
// ��� ������ � utf
/************************************************/

function utf_substr($string,$first,$len){
	return mb_substr($string,$first,$len,'utf-8');
}

function utf_win($s) {
	if (is_array($s)) {
		$s = array_map("utf_win",$s);
	} elseif (is_string($s)) {
		$s = iconv('utf-8','cp1251//ignore',$s);
	}
	return $s;
}

function utf_win_array($a) {
	return utf_win($a);
}

function win_utf($s) {
	if (is_array($s)) {
		$s = array_map("win_utf",$s);
	} elseif (is_string($s)) {
		$s = iconv('cp1251','utf-8//ignore',$s);
	}
	return $s;
}

function win_utf_array($a) { 
    return win_utf($a); 
} 


/************************************************/

function prikaz_type_is_stip($prikaz_type_kod) {
	return in_array($prikaz_type_kod,stip_prikaz_types());
}

function prikaz_type_is_zach($prikaz_type_kod) {
	return in_array($prikaz_type_kod,zach_prikaz_types());
}

function stip_prikaz_types() {
	return array(75,76,1005);
}

function zach_prikaz_types() {
	return array(1,59,6);
}
 
function message_die($message){
	header('Content-Type: text/html; charset=windows-1251');
	die('<h2 style="color: #ff0000;">'.$message.'</h2>');
}

function message_net_dostupa() {
	message_die('� ��� ��� ������� � ���� ��������!');
}

function message_prikaz_not_exists(){
	message_die('������� ������� �� ����������!');
}

// ��� �� �������� (stud_kod)

function stud_info($stud_kod) {
	$s = "SELECT ".stud_fio_concat()." AS fio, a.fam, a.im, a.otc, 
	a.pol_kod, a.data_rogdenia, a.mesto_rogdenia, a.gragd_kod, a.tel_dom, a.tel_mob, 
	a.pasp_type_kod, a.serp, a.nomp, a.data_pasp, a.vidan_pasp, a.email, 
	a.is_interdecanat, 
	ao.obraz_kod, ao.data_obraz_doc, ao.obraz_year, ao.inostr_obraz_doc_name, 
	grp.grp_nomer, 
	grp.uch_plan_kod up_kod,
	ds.status_kod, ".stud_kurs_expression()." as kurs,  
	ds.nomer_zach, ds.kudapost_kod, ds.grp_spec_kod, ds.profile_kod, ds.ob_osnova_kod, ds.srok_kod, 
	ds.grp_kod, ds.fact_grp_kod, 
	ds.fakultet_kod, ds.fact_fakultet_kod, 
	ds.ob_forma_kod, ds.fact_ob_forma_kod, 
	ds.is_sps, ds.is_slush, ds.osn_zach_kod, ds.target_org_kod, 
	ds.lang_1_kod, ds.lang_2_kod, ds.lang_3_kod, ds.is_starosta, ds.contract_nom, ds.contract_date, 
	ds.zach_data_s, ds.zach_data_po, ds.zach_aspir_kurs, ds.prepod_kod, ds.kafedra_kod, ds.zashita_date   
	FROM decanat_stud ds	
	INNER JOIN abitur a ON ds.stud_kod=a.abitur_kod 
	LEFT JOIN abitur_obraz ao ON ds.stud_kod=ao.abitur_kod
	LEFT JOIN decanat_fakultet_grp grp ON ds.fact_grp_kod=grp.grp_kod 
	WHERE ds.stud_kod='$stud_kod'";
    
	$result = sql_query($s);
	$row = sql_array($result);
	return $row;
}

function stud_kurs($stud_kod) {
	$sql = "SELECT ".stud_kurs_expression(). " as kurs FROM decanat_stud ds WHERE ds.stud_kod='$stud_kod'";
	return sql_fetch_value($sql);
}

function stud_fakultet_kod($stud_kod) {
	return sql_get_value("fakultet_kod","decanat_stud","stud_kod='$stud_kod'");
}

function stud_fact_fakultet_kod($stud_kod) {
	return sql_get_value("fact_fakultet_kod","decanat_stud","stud_kod='$stud_kod'");
}

function stud_is_interdekanat($stud_kod) {
	return sql_get_value("is_interdecanat","abitur","abitur_kod='$stud_kod'");
}

function stud_is_aspir($stud_kod) {
	return (stud_fakultet_kod($stud_kod)==30);
}

function stud_is_kursy($stud_kod) {
	return (stud_fakultet_kod($stud_kod)==73);
}

function stud_is_sps($stud_kod) {
	return sql_get_value("is_sps","decanat_stud","stud_kod='$stud_kod'");
}

function stud_gragd_kod($stud_kod) {
	return sql_get_value("gragd_kod","abitur","abitur_kod='$stud_kod'");
}

function stud_grp_kod($stud_kod) {
	return sql_get_value("grp_kod","`dekanat`.decanat_stud","stud_kod='$stud_kod'");
}

function stud_fact_grp_kod($stud_kod) {
	return sql_get_value("fact_grp_kod","`dekanat`.decanat_stud","stud_kod='$stud_kod'");
}

function stud_fact_grp_nomer($stud_kod) {
	return grp_nomer(stud_fact_grp_kod($stud_kod));
}

function stud_kudapost_kod($stud_kod) {
	return sql_get_value("kudapost_kod","`dekanat`.decanat_stud","stud_kod='$stud_kod'");
}

function stud_obraz_urov_kod($stud_kod) {
	return kudapost_obraz_urov_kod(stud_kudapost_kod($stud_kod));
}

function stud_osn_zach($stud_kod) {
	return sql_get_value("osn_zach_kod","decanat_stud","stud_kod='$stud_kod'");
}

function stud_ob_forma_kod($stud_kod) {
	return sql_get_value("ob_forma_kod","decanat_stud","stud_kod='$stud_kod'");
}

function stud_ob_osnova_kod($stud_kod) {
	return sql_get_value("ob_osnova_kod","decanat_stud","stud_kod='$stud_kod'");
}

function stud_is_budget($stud_kod) {
	return (stud_ob_osnova_kod($stud_kod)==1);
}

function stud_is_dogovor($stud_kod) {
	return (stud_ob_osnova_kod($stud_kod)==2);
}

function stud_is_goslinia($stud_kod) {
	return (stud_ob_osnova_kod($stud_kod)==3);
}

function stud_nomer_zach($stud_kod) {
	return sql_get_value("nomer_zach","decanat_stud","stud_kod='$stud_kod'");
}

function stud_status_kod($stud_kod) {
	return sql_get_value("status_kod","decanat_stud","stud_kod='$stud_kod'");
}

function stud_up($stud_kod) {
	return grp_kod_up_kod(stud_fact_grp_kod($stud_kod));
}

function stud_kursovik_tema($stud_kod,$upp_kod) {
	return trim(sql_get_value("tema","kursovik_tema", "stud_kod='$stud_kod' AND uch_plan_punkt_kod='$upp_kod'"));
}

function stud_is_obuch($stud_kod) {
	return status_is_obuch(stud_status_kod($stud_kod));
}

function stud_is_otpusk($stud_kod) {
	return status_is_otpusk(stud_status_kod($stud_kod));
}

function stud_zach_data_po($stud_kod) {
	return sql_get_value("zach_data_po","decanat_stud","stud_kod='$stud_kod'");
}

function stud_finish_d($stud_kod) {
	$d = stud_zach_data_po($stud_kod);
	if (!$d) {
		$up_kod = stud_up($stud_kod);
		$d = ($up_kod) ? up_finish_d($up_kod) : '2100-01-01';
	}
	return $d;
}

function stud_has_diplom($stud_kod,$obraz_urov_kod) {
	return sql_get_value("count(*)","stud_diplom","stud_kod='$stud_kod' and obraz_urov_kod='$obraz_urov_kod'");
}

function stud_is_uskor($stud_kod) {
	// �� � ���� ������ �� ������ � ����������� ������ �������� ! ������������ ������ � ������� ���� !!!
	return ($stud_kod == 268651);	// ���������� ����������
}

function stud_is_socr($stud_kod) {
	$up = up_info(stud_up($stud_kod));
	// ���������� ������� ������� 5 ���.
	return ($stud_kod == 271477) ? 1 : srok_is_socr($up['srok_kod']);
}

function stud_want_brs($stud_kod) {
	$want = sql_get_value("want_site","stud_want_brs","stud_kod='$stud_kod'");
	return (is_numeric($want)) ? $want : 1;
}

function stud_print_brs($stud_kod) {
	$want_brs = stud_want_brs($stud_kod);
	$ip_is_local = ($_SERVER && array_key_exists('REMOTE_ADDR',$_SERVER)) 
		? ip_is_local($_SERVER['REMOTE_ADDR'])
		: false;
	return ($want_brs) ? true : $ip_is_local;
}

function student_is_slush($stud_kod) {
	return sql_get_value("is_slush","decanat_stud","stud_kod='$stud_kod'");
}

function stud_fio($stud_kod) {
	return sql_get_value("concat(fam, ' ', im, ' ', ifnull(otc, '')) AS fio","abitur","abitur_kod='$stud_kod' ");
}

function stud_fio_short($stud_kod) {
	return sql_get_value("concat(fam, ' ', substring(im,1,1), '.', if(otc is null or otc='', '', concat(substring(otc,1,1),'.'))) AS fio","`dekanat`.abitur","abitur_kod='$stud_kod' ");
}

function stud_email($stud_kod) {
	return sql_get_value("email","abitur","abitur_kod='$stud_kod'");
}

function stud_fio_link($stud_kod,$type='card') {
	$links=array('card'=>'student.php','ocenka'=>'student_ocenka.php');
	$stud_fio = stud_fio($stud_kod);
	if ($stud_old_fio = stud_old_fio($stud_kod)) $stud_fio .= ' ('.$stud_old_fio.')';
	return ('<a href="/'.$links[$type].'?s='.$stud_kod.'">'.$stud_fio.'</a>');
}

function stud_fio_concat() {
	return "concat(a.fam, ' ', a.im, ' ', ifnull(a.otc, '')) ";
}

function stud_old_fio($stud_kod,$prikaz_nomer_kod = '') {
	$w =  ($prikaz_nomer_kod) ? " and o.prikaz_nomer_kod='$prikaz_nomer_kod'" : '';
	$old = [];
	$rows = sql_rows("SELECT CONCAT(o.fam, ' ', o.im, ' ', ifnull(o.otc,'') ) as old_fio FROM abitur_old_fio o 
		LEFT JOIN prikaz_nomer p ON o.prikaz_nomer_kod=p.prikaz_nomer_kod 
		WHERE o.abitur_kod='$stud_kod' $w ORDER BY p.prikaz_data desc, o.id desc ");
	foreach ($rows as $row) $old[] = $row['old_fio'];
	$s = implode("; ",$old);
	return $s;
}

function stud_kurs_expression() {
	$curdate = date("Y-m-d");
	return "if (ds.zach_aspir_kurs is null, ds.kurs, FLOOR(DATEDIFF('$curdate',ds.zach_data_s)/365.25)+ds.zach_aspir_kurs) ";
}

function stud_kurs_expression_where($v) {
	$curdate = date("Y-m-d");
	$s = (is_array($v)) ? implode(",",$v) : $v;
	return "if (ds.zach_aspir_kurs is null, ds.kurs in ($s), (FLOOR(DATEDIFF('$curdate',ds.zach_data_s)/365.25)+ds.zach_aspir_kurs) IN ($s)) ";
}

function stud_from_other_vuz($stud_kod) {
	return (stud_osn_zach($stud_kod)==999);
}

function stud_exists($stud_kod) {
	return sql_get_value("count(*)","`dekanat`.decanat_stud","stud_kod='$stud_kod'");
}

function stud_this_fakultet($stud_kod,$fak_kod) {
	$w = ($fak_kod==0) ? '' : " AND fakultet_kod='$fak_kod'";
	return sql_get_value("count(*)","decanat_stud","stud_kod='$stud_kod' $w");
}

function stud_may_portfolio($stud_kod) {
	$sql = "SELECT count(*) as cnt 
		FROM `dekanat`.decanat_stud ds 
		INNER JOIN `dekanat`.s_student_status s ON ds.status_kod=s.status_kod 
		WHERE true 
			AND ds.stud_kod='$stud_kod' 
			AND ds.kudapost_kod=7 
			AND ds.kurs IN (1,2,3) 
			AND s.is_obuch=1 
			AND ds.zach_aspir_kurs is null ";
	return sql_fetch_value($sql);
}

function abitur_kods_by_fio($fio) {
    $abiturs = array();
    $rows = sql_rows("
    	select abitur_kod from abitur 
    	where fio like '$fio%' 
    	union 
    	select abitur_kod from abitur_old_fio 
    	where fio like '$fio%'");
    foreach ($rows as $row) {
        $abiturs[] = $row['abitur_kod'];
    }
    return $abiturs;
}

// ������ �� ������� - ���������� ��� ������� �.�.

function zdanie_info($zdanie_kod) {
	return sql_get_array("`dekanat`.s_zdanie","zdanie_kod='$zdanie_kod'");
}

function zdanie_name($zdanie_kod) {
	$z = zdanie_info($zdanie_kod);
	return $z['zdanie_name'];
}

function grp_kod_zdanie_kod($grp_kod) {
	$g = grp_info($grp_kod);
	return $g['zdanie_kod'];
}

function grp_kod_fakultet_kod($grp_kod) {
	$g = grp_info($grp_kod);
	return $g['fakultet_kod'];
}

function stud_zdanie_kod($stud_kod) {
	return grp_kod_zdanie_kod(stud_grp_kod($stud_kod));
}

function stud_zdanie_name($stud_kod) {
	return zdanie_name(stud_zdanie_kod($stud_kod));
}

// ���������� �������� �� ������������
// ������������� �� ��������

function da_net_name($v) {
	return sql_get_value("name","`dekanat`.s_da_net","id='$v'");
}

function cd_type_info($cd_type_kod) {
	return sql_get_array("`dekanat`.cd_type","cd_type_kod='$cd_type_kod'");
}

function cd_type_name($cd_type_kod) {
	$cd_type = cd_type_info($cd_type_kod);
	return $cd_type['cd_type_name'];
}

function cd_type_socr_name($cd_type_kod) {
	$cd_type = cd_type_info($cd_type_kod);
	return $cd_type['cd_type_socr_name'];
}

function cicl_info($cicl_kod) {
	return sql_get_array("`dekanat`.s_upp_cicl","cicl_kod='$cicl_kod'");
}

function cicl_name($cicl_kod) {
	$cicl = cicl_info($cicl_kod);
	return $cicl['cicl_name'];
}

function cicl_socr_name($cicl_kod) {
	$cicl = cicl_info($cicl_kod);	
	return $cicl['cicl_official_name'];
}

function cicl_prim($cicl_kod) {
	$cicl = cicl_info($cicl_kod);
	return $cicl['prim'];
}

function control_form_info($control_form_kod) {
	return sql_get_array("`dekanat`.s_control_form","control_form_kod='$control_form_kod'");
}

function control_form_name($control_form_kod) {
	$c = control_form_info($control_form_kod);
	return $c['control_form_name'];
}

function control_form_socr_name($control_form_kod) {
	$c = control_form_info($control_form_kod);
	return $c['control_form_socr_name'];
}

function control_form_may_cd($control_form_kod) {
	$c = control_form_info($control_form_kod);
	return $c['may_cd'];
} 

function control_form_is_kursovik($control_form_kod) {
	$c = control_form_info($control_form_kod);
	return $c['is_kursovik'];
}

function diplom_upp_kod($up_kod) {
	$up_parent_kod = up_parent_kod($up_kod);
	$sql = "SELECT upp.uch_plan_punkt_kod 
		FROM uch_plan_punkt upp 
		INNER JOIN s_upp_cicl c ON upp.upp_cicl_kod=c.cicl_kod 
		WHERE c.is_diplom=1 AND upp.uch_plan_kod in ('$up_kod','$up_parent_kod')
	";
	return sql_fetch_value($sql);
}

function diplom_naimenovanie($obraz_urov_kod,$is_dublicat,$is_pril_dublicat) {
	$s = ($is_dublicat) ? '��������' : '������';
	if ($is_dublicat && $is_pril_dublicat) $s .= ' ���������� ';
	$s .= ' '.obraz_urov_name($obraz_urov_kod);
	return $s;
}

function gragd_info($gragd_kod) {
	return sql_get_array("`dekanat`.gragd","gragd_kod='$gragd_kod'");
}

function gragd_name($gragd_kod) {
	$g = gragd_info($gragd_kod);
	return $g['gragd_name'];
}

function gragd_type_name($gragd_kod) {
	$gr = '';
	if ($gragd_kod == 643) {
		$gr = '���������� ���������';
	} else {
		$gr = '����������� �����������';
	}
	return $gr;
}

function gragd_full_name($gragd_kod) {
	$g = gragd_info($gragd_kod);
	return $g['gragd_full_name'];
}

function grp_info($grp_kod) {
	return sql_get_array("`dekanat`.decanat_fakultet_grp","grp_kod='$grp_kod'");
}

function grp_nomer($grp_kod) {
	$g = grp_info($grp_kod);
	return $g['grp_nomer'];
}

function grp_has_grp_kurs($grp_kod) {
	$g = grp_info($grp_kod);
	return $g['has_grp_kurs'];
}

function grp_nomer_kurs($grp_kod,$kurs='?') {
	$has_grp_kurs = grp_has_grp_kurs($grp_kod);
	$find = array("_","������ ");
	$grp_nomer = str_replace($find,"",grp_nomer($grp_kod));
	if ($has_grp_kurs) {
		if (strstr($grp_nomer,"-")) {
			return preg_replace('/-[1-5]/','-'.$kurs,$grp_nomer);
		} else {
			return ($kurs . substr($grp_nomer,1));
		}
	} else {
		return $grp_nomer;
	}
}

function grp_kod_up_kod($grp_kod) {
	$g = grp_info($grp_kod);
	return $g['uch_plan_kod'];
}

function grp_inostr_info($grp_inostr_kod) {
	return sql_get_array("`dekanat`.grp_inostr","grp_inostr_kod='$grp_inostr_kod'");
}

function grp_inostr_name($grp_inostr_kod) {
	$gi = grp_inostr_info($grp_inostr_kod);
	return $gi['grp_inostr_name'];
}

function grp_inostr_prepod_kod($grp_inostr_kod) {
	$gi = grp_inostr_info($grp_inostr_kod);
	return $gi['prepod_kod'];
}

function fakultet_info($fakultet_kod) {
	return sql_get_array('`dekanat`.fakultet',"fakultet_kod='$fakultet_kod'");
}

function fakultet_name($fakultet_kod) {
	$fak = fakultet_info($fakultet_kod);
	return $fak['fakultet_name'];
}

function fakultet_lower_name($fakultet_kod) {
	return sql_get_value("REPLACE (LOWER(fakultet_name), '���������', '')","`dekanat`.fakultet","fakultet_kod='$fakultet_kod'");
}

function fakultet_rp_name($fakultet_kod) {
	$fak = fakultet_info($fakultet_kod);
	return $fak['fakultet_rp_name'];
}

function fakultet_socr_name($fakultet_kod) {
	$fak = fakultet_info($fakultet_kod);
	return $fak['fakultet_socr_name'];
}

function fakultet_is_filial($fakultet_kod) {
	$fak = fakultet_info($fakultet_kod);
	return $fak['is_filial'];
}

function fakultet_filial_kod($fakultet_kod) {
	$fak = fakultet_info($fakultet_kod);
	return $fak['filial_kod'];
}

function fakultet_decan_dolzn($fakultet_kod) {
	$fak = fakultet_info($fakultet_kod);
	return $fak['decan_dolzn'];
}

function fakultet_decan_dolzn_rp($fakultet_kod) {
	$fak = fakultet_info($fakultet_kod);
	return $fak['decan_dolzn_rp'];
}

function fakultet_decan_fio($fakultet_kod) {
	$fak = fakultet_info($fakultet_kod);
	return $fak['fakultet_decan_fio'];
}

function fakultet_phone($fakultet_kod) {
	$fak = fakultet_info($fakultet_kod);
	return $fak['phone'];
}

function filial_name($filial_kod) {
	return sql_get_value("filial_name","`dekanat`.s_filial","filial_kod='$filial_kod'");
}

function inostr_lang_name($lang_kod) {
	return sql_get_value("lang_name","s_lang","lang_kod='$lang_kod'"); 
}

function kafedra_name($kafedra_kod) {
	return sql_get_value("kafedra_name","`dekanat`.kafedra","kafedra_kod='$kafedra_kod'");
}

function kopl_name($korp_id) {
	return sql_get_value("name","`stip`.s_kopl","id='$korp_id'");
}

function kudapost_info($kudapost_kod) {
	return sql_get_array('`dekanat`.s_kudapost',"kudapost_kod='$kudapost_kod'"); 
}

function kudapost_name($kudapost_kod) {
	$k = kudapost_info($kudapost_kod);
	return $k['kudapost_name'];
}

function kudapost_kto($kudapost_kod) {
	$k = kudapost_info($kudapost_kod);
	return $k['kudapost_kto'];
}

function kudapost_obraz_urov_kod($kudapost_kod) {
	$k = kudapost_info($kudapost_kod);
	return $k['obraz_urov_kod'];
}

function kudapost_spec_type_name($kudapost_kod) {
	$k = kudapost_info($kudapost_kod);
	return $k['spec_type_name'];
}

function kudapost_spec_type_name_2($kudapost_kod) {
	$k = kudapost_info($kudapost_kod);
	return $k['spec_type_name_2'];
}

function kudapost_spec_type_name_dp($kudapost_kod) {
	$k = kudapost_info($kudapost_kod);
	return $k['spec_type_name_dp'];
}

function kudapost_spec_poduroven_name($kudapost_kod) {
	$k = kudapost_info($kudapost_kod);
	return $k['spec_poduroven'];
}

function bakalavr_type_name($bakalavr_type_kod) {
	return sql_get_value("bakalavr_type_name","s_bakalavr_type","bakalavr_type_kod='$bakalavr_type_kod'");
}

function lgota_name($lgota_kod) {
	return sql_get_value("lgota_name","s_lgota","lgota_kod='$lgota_kod'");
}

function obraz_urov_name($obraz_urov_kod) {
	return sql_get_value('obraz_urov_name','s_obraz_urov',"obraz_urov_kod='$obraz_urov_kod'");
}

function obraz_name($obraz_kod) {
	return sql_get_value("obraz_name","s_obraz","obraz_kod='$obraz_kod'"); 
}

function obraz_short_name($obraz_kod) {
	return sql_get_value("obraz_short_name","s_obraz","obraz_kod='$obraz_kod'");
}

function obraz_doc_name($obraz_kod) {
	return sql_get_value('obraz_dok_name','s_obraz',"obraz_kod='$obraz_kod'");
}

function ob_forma_name($ob_forma_kod) {
	return sql_get_value("ob_forma_name","`dekanat`.ob_forma","ob_forma_kod='$ob_forma_kod'");
}

function ob_forma_full_name($ob_forma_kod) {
	return sql_get_value("ob_forma_full_name","`dekanat`.ob_forma","ob_forma_kod='$ob_forma_kod'");
}

function ob_forma_name_rp($ob_forma_kod) {
	return sql_get_value("ob_forma_name_rp","`dekanat`.ob_forma","ob_forma_kod='$ob_forma_kod'");
}

function ob_osnova_name($ob_osnova_kod) {
	return sql_get_value("ob_osnova_name","s_ob_osnova","ob_osnova_kod='$ob_osnova_kod'");
}

function ob_osnova_full_name($ob_osnova_kod) {
	return sql_get_value("ob_osnova_full_name","s_ob_osnova","ob_osnova_kod='$ob_osnova_kod'");
}

function org_name($org_kod) {
	return sql_get_value("org_name","s_org","org_kod='$org_kod'");
}

function ocenka_name($ocenka_kod) {
	return sql_get_value("ocenka_name","s_ocenka","ocenka_kod='$ocenka_kod'");
}

function ocenka_socr_name($ocenka_kod) {
	return sql_get_value("ocenka_socr_name","s_ocenka","ocenka_kod='$ocenka_kod'");
}

function oc_is_good($oc_kod) {
	return sql_get_value("is_good","s_ocenka","ocenka_kod='$oc_kod'");
}

function oc_color($oc_kod) {
	return sql_get_value("color","s_ocenka","ocenka_kod='$oc_kod'");
}

function oc_sess_type_name($sess_type_kod) {
	return sql_get_value("sess_type_name","s_oc_sess_type","sess_type_kod='$sess_type_kod'");
}

function oc_sess_type_socr_name($sess_type_kod) {
	return sql_get_value("sess_type_socr_name","s_oc_sess_type","sess_type_kod='$sess_type_kod'");
}

function osn_zach_name($osn_zach_kod) {
	return sql_get_value("osn_zach_name","s_osn_zach","osn_zach_kod='$osn_zach_kod'");
}

function pasp_type_name($pasp_type_kod) {
	return sql_get_value("pasp_type_name","s_pasp_type","pasp_type_kod='$pasp_type_kod'");
}

function peregon_type_name($peregon_type_kod) {
	return sql_get_value("peregon_type_name","s_peregon_type","peregon_type_kod='$peregon_type_kod'");
}

function pol_name($pol_kod) {
	return sql_get_value("pol_name","s_pol","pol_kod='$pol_kod'"); 
}

function predmet_info($predmet_kod) {
	return sql_get_array("`dekanat`.predmet","predmet_kod='$predmet_kod'");
}

function predmet_name($predmet_kod) {
	$p = predmet_info($predmet_kod);
	return $p['predmet_name'];
}

function predmet_socr_name($predmet_kod) {
	$p = predmet_info($predmet_kod);
	return $p['predmet_socr_name'];
}

function predmet_has_tema($predmet_kod) {
	$p = predmet_info($predmet_kod);
	return $p['has_tema'];
}

function predmet_is_inostr($predmet_kod) {
	$p = predmet_info($predmet_kod);
	return $p['is_inostr'];
}

function predmet_is_matem($predmet_kod) {
	$p = predmet_info($predmet_kod);
	return $p['is_matem'];
}

function prichina_name($prichina_kod) {
	return sql_get_value('prichina_name','`dekanat`.s_prichina',"prichina_kod='$prichina_kod'");
}

function region_name($region_kod) {
	return sql_get_value("post_name","`kladr`.kladr_region","reg_code='$region_kod'"); 
}

function spravka_type_name($spravka_type_kod) {
	return sql_get_value('type_name','s_student_spravka_type',"type_kod='$spravka_type_kod' ");
}

function srok_name($srok_kod) {
	return sql_get_value("srok_name","`dekanat`.s_srok","srok_kod='$srok_kod'");
}

function srok_socr_name($srok_kod) {
	return sql_get_value("srok_socr_name","`dekanat`.s_srok","srok_kod='$srok_kod'");
}

function srok_is_socr($srok_kod) {
	return sql_get_value("is_socr","`dekanat`.s_srok","srok_kod='$srok_kod'");
}

function srok_cnt_semestr($srok_kod) {
	return sql_get_value("cnt_semestr","`dekanat`.s_srok","srok_kod='$srok_kod'");
}

function status_name($status_kod) {
	return sql_get_value('status_name','s_student_status',"status_kod='$status_kod'");
}

function status_is_obuch($status_kod) {
	return sql_get_value("is_obuch","s_student_status","status_kod='$status_kod'");
}

function status_is_otpusk($status_kod) {
	return sql_get_value("is_otpusk","s_student_status","status_kod='$status_kod'");
}

function status_is_otchisl($status_kod) {
	return sql_get_value("is_otchisl","s_student_status","status_kod='$status_kod'");
}

function status_is_vipusk($status_kod) {
	return sql_get_value("is_vipusk","s_student_status","status_kod='$status_kod'");
}

function target_name($target_kod) {
	return sql_get_value("target_name","s_target","target_kod='$target_kod'");
}

function voenkomat_name($voenkomat_kod) {
	return sql_get_value("name","s_vus_voenkomat","voenkomat_kod='$voenkomat_kod'");
}

function vus_spec_name($vus_spec_kod) {
	return sql_get_value("CONCAT(shifr, ' (', descr,')') as name ","s_vus_spec","vus_spec_kod='$vus_spec_kod'");
}

function vuz_name($vuz_kod) {
	return sql_get_value("vuz_name","s_vuz","vuz_kod='$vuz_kod'");
}

function vuz_type_name($vuz_type_kod) {
	return sql_get_value("vuz_type_name_full","s_vuz_type","vuz_type_kod='$vuz_type_kod'");
}

function vuz_type_name_socr($vuz_type_kod) {
	return sql_get_value("vuz_type_name","s_vuz_type","vuz_type_kod='$vuz_type_kod'");
}

// ������

function hotel_info($hotel_kod) {
	return sql_get_array("s_hotel","hotel_kod='$hotel_kod'");
}

function hotel_name($hotel_kod) {
	return sql_get_value("hotel_name","s_hotel","hotel_kod='$hotel_kod'");
}

function hotel_room($room_kod) {
	return sql_get_value("room_name","s_hotel_room","room_kod='$room_kod'");
}

// ������������� � �������

function grp_spec_info($grp_spec_kod) {
	return sql_get_array('grp_spec',"grp_spec_kod='$grp_spec_kod'");
}

function grp_spec_is_lingv($grp_spec_kod) {
	return sql_get_value('is_lingv','grp_spec',"grp_spec_kod='$grp_spec_kod'");
}

function grp_spec_name($grp_spec_kod) {
	return sql_get_value("grp_spec_name","grp_spec","grp_spec_kod='$grp_spec_kod'");
}

function grp_spec_name_full($grp_spec_kod) {
	return sql_get_value("grp_spec_name_full","grp_spec","grp_spec_kod='$grp_spec_kod'");
}

function grp_spec_shifr($grp_spec_kod) {
	return sql_get_value("shifr","grp_spec","grp_spec_kod='$grp_spec_kod'");
}

function grp_spec_name_shifr($grp_spec_kod) {
	$s = '';
	if ($spec = grp_spec_info($grp_spec_kod)) {
		$s = kudapost_name($spec['kudapost_kod']) . ' - ' . $spec['shifr'] . ' - ' . $spec['grp_spec_name'];
	}
	return $s;
}

function grp_spec_parent_kod($grp_spec_kod) {
	return sql_get_value("parent_kod","`dekanat`.grp_spec","grp_spec_kod='$grp_spec_kod'");
}

function grp_spec_kudapost_kod($grp_spec_kod) {
	return sql_get_value("kudapost_kod","`dekanat`.grp_spec","grp_spec_kod='$grp_spec_kod'");
}

function spec_kudapost_kod($grp_spec_kod) {
	return grp_spec_kudapost_kod($grp_spec_kod);
}

function grp_spec_qualification($grp_spec_kod) {
	return sql_get_value("spec_qualification","`dekanat`.grp_spec","grp_spec_kod='$grp_spec_kod'");
}

function grp_spec_normativ_srok($grp_spec_kod) {
	if ($grp_spec_kod==10017) {
    	$s = '5 ��� 6 �������';
    } elseif ($grp_spec_kod==12048) {
    	$s = '4 ����';
    } elseif ($grp_spec_kod==10376) {
    	$s = '4 ����';
   	} elseif ($grp_spec_kod==10467 || $grp_spec_kod==10468) {
   		$s = '6 ���';
    } elseif ($grp_spec_kod==10407) {
    	// ����������� �����������
    	$s = '4 ����';
    } else {
    	$kudapost_kod = grp_spec_kudapost_kod($grp_spec_kod);
        $s = kudapost_normativ_srok($kudapost_kod);
	}
	return $s;
}

function kudapost_normativ_srok($kudapost_kod) {
	$s = '';
	if ($kudapost_kod==1 || $kudapost_kod==10) $s = '4 ����';
	if ($kudapost_kod==2 || $kudapost_kod==6) $s = '2 ����';
	if ($kudapost_kod==3) $s = '5 ���';
	if ($kudapost_kod==7) $s = '3 ����';
	return $s;
}

function trajectory_name($trajectory_kod) {
	return sql_get_value("trajectory_name","s_trajectory","trajectory_kod='$trajectory_kod'");
}

// ������� �����

function up_info($up_kod) {
	return sql_get_array("`dekanat`.uch_plan", "uch_plan_kod='$up_kod'");
}

function up_finish_d($up_kod) {
	$up = up_info($up_kod);
	return $up['d_finish'];
}

function up_name($up_kod) {
	$up = up_info($up_kod);
	return $up['uch_plan_name'];
}

function up_year($up_kod) {
	$up = up_info($up_kod);
	return $up['year'];
}

function up_diplom_pokolenie($up_kod) {
	$up = up_info($up_kod);
	return $up['diplom_pokolenie'];
}

function up_parent_kod($up_kod) {
	$up = up_info($up_kod);
	return ($up['parent_up_kod']) ? $up['parent_up_kod'] : 0;
}

function up_kudapost_kod($up_kod) {
	return grp_spec_kudapost_kod(up_grp_spec_kod($up_kod));
}

function up_grp_spec_kod($up_kod) {
	$up = up_info($up_kod);
	return $up['grp_spec_kod'];
}

function up_profile_kod($up_kod) {
	$up = up_info($up_kod);
	return $up['profile_kod'];
}

function up_fak_kod($up_kod) {
	$up = up_info($up_kod);
	return $up['fak_kod'];
}

function up_is_sess_now($up_kod) {
	$up = up_info($up_kod);
	return $up['is_sess_now'];
}

function up_semestr_sess_finished($up_kod) {
	$up = up_info($up_kod);
	return $up['semestr_sess_finished'];
}

function up_is_brs($up_kod) {
	$up = up_info($up_kod);
	return !$up['is_not_brs'];
}

function up_upps($up_kod,$semestr = '') {
	$parent_up_kod = up_parent_kod($up_kod);
	$w = " and upp.uch_plan_kod in ('$up_kod', '$parent_up_kod') ";
	if ($semestr) $w .= " and upp.semestr='$semestr' ";
	
	$s = "SELECT upp.uch_plan_punkt_kod 
	FROM uch_plan_punkt upp 
	INNER JOIN predmet p ON upp.predm_kod=p.predmet_kod 
	LEFT JOIN s_upp_cicl c ON upp.upp_cicl_kod=c.cicl_kod 
	WHERE true $w 
	ORDER BY upp.semestr, p.predmet_name, upp.control_form_kod ";
	$res = sql_query($s);
	$a = array();
	while ($row = sql_array($res)) {
		$a[] = $row['uch_plan_punkt_kod'];
	}
	return $a;
}

function up_qualification($up_kod) {
	$grp_spec_kod = up_grp_spec_kod($up_kod);
	$diplom_pokolenie = up_diplom_pokolenie($up_kod);
	$qualification = '';
	if ($grp_spec_kod == 12021) {
		$qualification = '���������� ����������� ����';
	} elseif ($grp_spec_kod == 12055) {
		$qualification = '���������';
	} elseif ($diplom_pokolenie == 3) {
		$kudapost_kod = grp_spec_kudapost_kod($grp_spec_kod);
		if ($kudapost_kod==1) $qualification = '��������';
		elseif ($kudapost_kod==2 || $kudapost_kod==6) $qualification = '�������';
		elseif ($kudapost_kod==3) $qualification = '����������';
		elseif ($kudapost_kod==7) $qualification = '�������������. �������������-�������������';
	} else {
		$qualification = grp_spec_qualification($grp_spec_kod);
	}
	return $qualification;
}

function up_fakultet_year_exists($up_kod,$fakultet_kod,$year) {
	return sql_get_value("count(*)","uch_plan","uch_plan_kod='$up_kod' and fak_kod='$fakultet_kod' and year='$year' ");
}

// ������� ����� ��������

function upp_info($upp_kod) {
	return sql_get_array("`dekanat`.uch_plan_punkt","uch_plan_punkt_kod='$upp_kod'");
}

function upp_name_concat($upp_kod,$semestr = '') {
	$name = '';
	$name = predmet_name(upp_predmet_kod($upp_kod));
	$name .= '('.control_form_name(upp_control_form($upp_kod)).')';
	if (strstr(cicl_prim(upp_cicl_kod($upp_kod)),"���")) $name .= '(���)';
	if (!$semestr) $name = upp_semestr($upp_kod) . ' ������� - ' . $name;
	return $name;
}

function upp_up_kod($upp_kod) {
	$upp = upp_info($upp_kod);
	return $upp['uch_plan_kod'];
}

function upp_predmet_kod($upp_kod) {
	$upp = upp_info($upp_kod);
	return $upp['predm_kod'];
}

function upp_predmet_name($upp_kod) {
	return predmet_name(upp_predmet_kod($upp_kod));
}

function upp_semestr($upp_kod) {
	$upp = upp_info($upp_kod);
	return $upp['semestr'];
}

function upp_module($upp_kod) {
	$upp = upp_info($upp_kod);
	return $upp['module'];
}

function upp_is_inostr($upp_kod) {
	return predmet_is_inostr(upp_predmet_kod($upp_kod));
}

function upp_has_podgrp($upp_kod) {
	$upp = upp_info($upp_kod);
    if ($upp['has_podgrp']) {
        return true;
    } elseif (up_is_brs(upp_up_kod($upp_kod))) {
        return  upp_is_inostr($upp_kod);
    }   
}

function upp_is_matem($upp_kod) {
	return predmet_is_matem(upp_predmet_kod($upp_kod));
}

function upp_uch_year($upp_kod) {
	if (!is_numeric($upp_kod)) return '';
	$semestr = upp_semestr($upp_kod);
	$up_year = up_year(upp_up_kod($upp_kod));
	$nomer_kurs = (ceil($semestr / 2));
	$uch_year = ($up_year + $nomer_kurs - 1) . '/' . ($up_year + $nomer_kurs);
	return $uch_year; 
}

function upp_control_form($upp_kod) {
	$upp = upp_info($upp_kod);
	return $upp['control_form_kod'];
}

function upp_cicl_kod($upp_kod) {
	$upp = upp_info($upp_kod);
	return $upp['upp_cicl_kod'];
}

function upp_cicl_info($upp_kod) {
	$cicl_kod = upp_cicl_kod($upp_kod);
	return cicl_info($cicl_kod);
}

function upp_is_brs($upp_kod) {
	$cicl_kod = upp_cicl_kod($upp_kod);
	$cicl = upp_cicl_info($upp_kod);
	return !$cicl['is_not_brs'];
}

function upp_is_pract($upp_kod) {
	$cicl = upp_cicl_info($upp_kod);
	return $cicl['is_pract'];
}

function upp_is_gak($upp_kod) {
	$cicl = upp_cicl_info($upp_kod);
	return $cicl['is_gak'];
}

function upp_is_gos($upp_kod) {
	return upp_is_gak($upp_kod);
}

function upp_has_tema($upp_kod) {
	return (upp_has_kursovik($upp_kod) || predmet_has_tema(upp_predmet_kod($upp_kod)));
}

function upp_has_exam($upp_kod) {
	return (upp_control_form($upp_kod)==CONTROL_EXAM);
}

function upp_has_ocenka($upp_kod) {
	return in_array(upp_control_form($upp_kod), array(CONTROL_EXAM,CONTROL_DIFF_ZACHET));
}	

function upp_has_kursovik($upp_kod) {
	return control_form_is_kursovik(upp_control_form($upp_kod));
}

/*
function upp_has_exam_koeff($upp_kod) {
	return sql_fetch_value("select count(ex.semestr) 
		from uch_plan_punkt upp 
		inner join exam_koeff ex on upp.uch_plan_kod=ex.up_kod and upp.predm_kod=ex.predmet_kod
		where upp.uch_plan_punkt_kod='$upp_kod'");
}
*/

function upp_cds($upp_kod) {
	$cds=array();
	$r=sql_query("select * from cd where uch_plan_punkt_kod='$upp_kod' order by cd_nomer");
	while ($row=sql_array($r)) {
		$cds[]=$row;
	}
	return $cds;
}

function upp_has_0_cd_nomer($upp_kod) {
	return sql_get_value("count(*) as c","cd","uch_plan_punkt_kod='$upp_kod' AND cd_nomer=0 ");
}

function upp_exam_min_ball($upp_kod) {
	return sql_get_value("ball_min*use_ball_min","cd","uch_plan_punkt_kod='$upp_kod' AND cd_type_kod=20");
}

function upp_prepod_grp_pract($upp_kod,$grp_kod) {
	return sql_get_value("prepod_kod","`dekanat`.prepod_grp_pract","grp_kod='$grp_kod' and uch_plan_punkt_kod='$upp_kod' ");
}

function upp_prepod_grp_exam($upp_kod,$grp_kod) {
	return sql_get_value("prepod_kod","`dekanat`.prepod_grp_exam","grp_kod='$grp_kod' and uch_plan_punkt_kod='$upp_kod' ");
}

function has_prepod_grp_pract($upp_kod,$grp_kod) {
	$prepod_kod = upp_prepod_grp_pract($upp_kod,$grp_kod);
	return is_numeric($prepod_kod) ? true : false;
}

// ������� ������ � ������ � ������

function prepod_upp_grp_rows($prepod_kod) {
	$sql = "
		SELECT 1 as type, p1.uch_plan_punkt_kod, p1.grp_kod, null as grp_inostr_kod 
			FROM `dekanat`.prepod_grp_pract p1 
			WHERE p1.prepod_kod='$prepod_kod'
		UNION ALL
		SELECT 2 as type, upp.uch_plan_punkt_kod, null as grp_kod, g.grp_inostr_kod 
			FROM `dekanat`.uch_plan_punkt upp 
			INNER JOIN `dekanat`.grp_inostr g ON upp.predm_kod=g.predm_kod AND upp.uch_plan_kod=g.up_kod 
			WHERE g.prepod_kod='$prepod_kod' 
				AND g.is_actual=1 
				AND g.grp_inostr_kod IN (SELECT sg.grp_inostr_kod FROM `dekanat`.stud_grp_inostr sg) 
		UNION ALL 
		SELECT DISTINCT 3 as type, t.uch_plan_punkt_kod, t.grp_kod, null as grp_inostr_kod 
			FROM 
			(	SELECT MIN(s.prepod_kod) as min_prepod_kod, s.uch_plan_punkt_kod, s.grp_kod  
				FROM `rasp`.schedule_published s  
				WHERE s.uch_plan_punkt_kod is not null 
					AND s.lesson_type_kod=".LESSON_TYPE_PRACT." 
				GROUP BY s.uch_plan_punkt_kod, s.grp_kod  
				HAVING MIN(s.prepod_kod)=MAX(s.prepod_kod)
			) t
			WHERE t.min_prepod_kod='$prepod_kod' 
		ORDER BY type ";
	$rows = sql_rows($sql);
	$new_rows = [];
	foreach ($rows as $row) {
		$upp_kod = $row['uch_plan_punkt_kod'];
		if (upp_has_podgrp($upp_kod) && $row['type'] != 2) {
			// ignore
		} elseif ($row['type']==3 && has_prepod_grp_pract($row['uch_plan_punkt_kod'],$row['grp_kod'])) {
			// ignore
		} else {
			$new_rows[] = $row;
		}
	}

	return $new_rows;
}

function prepod_may_upp($prepod_kod,$upp_kod,$grp_kod,$grp_inostr_kod) {
	$rows = prepod_upp_grp_rows($prepod_kod);
	$use_grp_inostr_kod = ($grp_inostr_kod) ? 1 : 0;
	$may = false;
	foreach ($rows as $row) {
		if ($use_grp_inostr_kod) {
			if ($row['uch_plan_punkt_kod']==$upp_kod && $row['grp_inostr_kod']==$grp_inostr_kod) $may = true;
		} else {
			if ($row['uch_plan_punkt_kod']==$upp_kod && $row['grp_kod']==$grp_kod) $may = true;
		}
	}
	return $may;
}

function upp_document_prepod($upp_kod,$grp_kod,$grp_inostr_kod) {
	$upp_info = upp_info($upp_kod);
	if (upp_has_podgrp($upp_kod)) {
		// ��������� 
		$prepod_kod = grp_inostr_prepod_kod($grp_inostr_kod);
	} else if ($control_form_kod == 2 || $control_form_kod == 3) {
		// ����� ��� ����.�����
		$prepod_kod = upp_prepod_grp_pract($upp_kod,$grp_kod);
		if (!$prepod_kod) {
			$prepod_kod = sql_fetch_value("SELECT MIN(s.prepod_kod) as min_prepod_kod   
				FROM `rasp`.schedule_published s  
				WHERE s.uch_plan_punkt_kod='$upp_kod' 
					AND s.grp_kod='$grp_kod'
					AND s.lesson_type_kod IN (".LESSON_TYPE_PRACT.",".LESSON_TYPE_ZACH.")   
				GROUP BY s.uch_plan_punkt_kod, s.grp_kod  
				HAVING MIN(s.prepod_kod)=MAX(s.prepod_kod) ");
		}
	} else {
		// ������� �������� � ��
		$prepod_kod = upp_prepod_grp_exam($upp_kod,$grp_kod);
		if (!$prepod_kod) {
			$prepod_kod = sql_fetch_value("SELECT MIN(s.prepod_kod) as min_prepod_kod   
				FROM `rasp`.schedule_published s  
				WHERE s.uch_plan_punkt_kod='$upp_kod' 
					AND s.grp_kod='$grp_kod'
					AND s.lesson_type_kod IN (".LESSON_TYPE_LECT.",".LESSON_TYPE_EXAM.")   
				GROUP BY s.uch_plan_punkt_kod, s.grp_kod  
				HAVING MIN(s.prepod_kod)=MAX(s.prepod_kod) ");
		}
		
	}
	return $prepod_kod;
}

// ����������� �����

function cd_info($cd_kod) {
	return sql_get_array("`dekanat`.cd","cd_kod='$cd_kod'");
}

function cd_upp_kod($cd_kod) {
	$cd = cd_info($cd_kod);
	return $cd['uch_plan_punkt_kod'];
}

function cd_predmet_kod($cd_kod) {
	$upp_kod = cd_upp_kod($cd_kod);
	$predm_kod = upp_predmet_kod($upp_kod);
	return $predm_kod;
}

function cd_count($upp_kod) {
	return sql_fetch_value("select count(*) from `dekanat`.cd where uch_plan_punkt_kod='$upp_kod' AND cd_type_kod<>20 ");
}

function stud_cd_info($stud_kod,$cd_kod) {
	return sql_get_array("`dekanat`.stud_cd","cd_kod='$cd_kod' AND stud_kod='$stud_kod' ");
}

function stud_cd_ball($stud_kod,$cd_kod) {
	$stud_cd = stud_cd_info($stud_kod,$cd_kod);
	return $stud_cd['ball'];
}

function stud_has_cd($stud_kod,$cd_kod) {
	$stud_cd = stud_cd_info($stud_kod,$cd_kod);
	return $stud_cd ? 1 : 0;
}

function stud_upp_finished_balls ($stud_kod,$upp_kod) {
	return sql_get_array("`dekanat`.v_stud_ocenka_actual","stud_kod='$stud_kod' AND uch_plan_punkt_kod='$upp_kod'");
}

function stud_upp_has_open_doc($stud_kod,$upp_kod) {
	$has = stud_upp_open_doc($stud_kod,$upp_kod);
	return $has;
}

function stud_has_good_oc($stud_kod,$upp_kod) {
	$has = sql_fetch_value("SELECT t2.is_good FROM v_stud_ocenka_actual t1 
	INNER JOIN s_ocenka t2 ON t1.ocenka_kod=t2.ocenka_kod 
	WHERE t1.stud_kod=$stud_kod AND t2.is_good=1 AND t1.uch_plan_punkt_kod=$upp_kod ");
	return $has;
}

function stud_upp_open_doc($stud_kod,$upp_kod) {
	// ���������� �� �������� (��� �� ����� ����) ��������
	$s = "SELECT vn.vedomost_kod
	FROM decanat_vedomost_nomer vn
	INNER JOIN decanat_vedomost_sostav vs ON vn.vedomost_kod=vs.vedomost_kod 
	WHERE vn.uch_plan_punkt_kod='$upp_kod'
	AND vs.stud_kod='$stud_kod' and (vn.is_finished=0 or vs.may_change=1) 
	ORDER BY vn.vedomost_data desc LIMIT 0,1";
	$ved = sql_fetch_value($s);
	
	$s = "SELECT ns.naprav_kod
	FROM decanat_naprav_sostav ns
	WHERE ns.uch_plan_punkt_kod='$upp_kod' 
	AND ns.stud_kod='$stud_kod' AND ns.is_finished=0 AND ns.d_po > " . naprav_expire_sql() . "
	ORDER BY ns.d_create desc LIMIT 0,1";
	$naprav = sql_fetch_value($s);
	
	if ($ved) return array('doc_type'=>DOC_VEDOMOST,'doc_kod'=>$ved);
	elseif ($naprav) return array('doc_type'=>DOC_NAPRAVLENIE,'doc_kod'=>$naprav);
	else return false;
}


function stud_upp_open_naprav($stud_kod,$upp_kod) { //����������  ����� 1-�� �� ��������� ����������� �� �������� ��� False
	$s = "select naprav_kod from decanat_naprav_sostav 
	where is_finished=0 and stud_kod='$stud_kod' and uch_plan_punkt_kod='$upp_kod' ";
	$row = sql_numeric_array(sql_query($s));
	return ($row) ? naprav_info($row[0]) : false;
}

function oc_doc_type_name($oc_doc_type_kod) {
	return sql_get_value("oc_doc_type_name","s_oc_doc_type","oc_doc_type_kod='$oc_doc_type_kod'");
}

function oc_doc_info($doc_kod,$doc_type_kod) {
	$m = array();
	if (!is_numeric($doc_kod)) return array('nom'=>'','d'=>'','type'=>'');
	$m['type'] = oc_doc_type_name($doc_type_kod);
	if ($doc_type_kod==1) {
		$ved = vedomost_info($doc_kod);
		$m['nom'] = $ved['vedomost_nomer'];
		$m['d'] = sql_date2rus($ved['vedomost_data']);
		$m['is_finished'] = $ved['is_finished'];
	} elseif ($doc_type_kod==2) {
		$naprav = naprav_info($doc_kod);
		$m['nom'] = $naprav['nom'];
		$m['d'] = sql_date2rus($naprav['d_create']);
		$m['is_finished'] = $naprav['is_finished'];
	} elseif ($doc_type_kod==3) {
		$rash = rashod_list_info($doc_kod);
		$m['nom'] = $rash['rashod_list_nomer'];
		$m['d'] = sql_date2rus($rash['rashod_list_data']);
		$m['is_finished'] = $rash['is_finished'];
	} else {
		$m['nom'] = $m['d'] = '';
		$m['is_finished'] = 0;
	}
	return $m;
}

function stud_has_good_oc_vibor($stud_kod,$up_kod,$semestr,$n_vibor) {
	$up_parent_kod = up_parent_kod($up_kod);
	$s = "select count(*) from v_stud_ocenka_actual s
	inner join uch_plan_punkt upp on s.uch_plan_punkt_kod=upp.uch_plan_punkt_kod 
	inner join s_ocenka s_oc on s.ocenka_kod=s_oc.ocenka_kod
	where stud_kod=$stud_kod and upp.uch_plan_kod IN ($up_kod,$up_parent_kod) and upp.semestr=$semestr and upp.n_vibor=$n_vibor 
	and s_oc.is_good=1 ";
	$res = sql_query($s);
	$row = sql_numeric_array($res);
	return ($row[0]>0);
}

function count_dolg_vibor($stud_kod,$up_kod,$semestr) {
	$up_parent_kod = up_parent_kod($up_kod);
	$s="select distinct n_vibor from uch_plan_punkt where uch_plan_kod in ($up_kod,$up_parent_kod) and semestr=$semestr and n_vibor is not null and is_fakultativ=0 ";
	$res = sql_query($s);
	$dolg=0;
	while ($row = sql_array($res)) {
		if (!stud_has_good_oc_vibor($stud_kod,$up_kod,$semestr,$row['n_vibor'])) {
			$dolg++;
		}
	}
	return $dolg;
}

function stud_count_bad($stud_kod,$semestr) {
	$up_kod = stud_up($stud_kod);
	$up_parent_kod = up_parent_kod($up_kod);
	$count_upp = sql_get_value('count(*)','uch_plan_punkt',"semestr=$semestr AND uch_plan_kod in ('$up_kod','$up_parent_kod') 
	AND is_fakultativ=0 
	AND control_form_kod <> 5
	AND n_vibor is null ");
	$count_good_upp = sql_fetch_value("select count(*) from  v_stud_ocenka_actual s_o
			INNER JOIN s_ocenka ON s_o.ocenka_kod=s_ocenka.ocenka_kod 
			INNER JOIN uch_plan_punkt upp on s_o.uch_plan_punkt_kod=upp.uch_plan_punkt_kod
			WHERE s_ocenka.is_good=1 
			and upp.uch_plan_kod in ('$up_kod','$up_parent_kod')  
			and upp.is_fakultativ=0 and n_vibor is null and control_form_kod <> 5
			and s_o.stud_kod='$stud_kod' and upp.semestr='$semestr' ");
	$count_bad_upp = $count_upp - $count_good_upp; 
	$count_bad_upp_vibor = count_dolg_vibor($stud_kod,$up_kod,$semestr);
	
	$cnt_bad = $count_bad_upp + $count_bad_upp_vibor;
	return $cnt_bad;
}

function stud_count_dolgov($stud_kod) {
	$up_kod = stud_up($stud_kod);
	$semestr_sess_finished = up_semestr_sess_finished($up_kod);
	$cnt = 0;
	for ($i=1; $i<=$semestr_sess_finished; $i++) {
		$cnt += stud_count_bad($stud_kod,$i);
	}
	return $cnt;
}

function stud_avg_oc($stud_kod,$semestr = '') {
	$up_kod = stud_up($stud_kod);
	$up_parent_kod = up_parent_kod($up_kod);
	
	$w_semestr = (is_numeric($semestr)) ? " and upp.semestr='$semestr' " : '';
	$s=" select avg(oc.ocenka_kod) as avg_oc 
	from v_stud_ocenka_actual oc 
	inner join uch_plan_punkt upp on oc.uch_plan_punkt_kod=upp.uch_plan_punkt_kod 
	where upp.uch_plan_kod in ($up_kod,$up_parent_kod) and oc.stud_kod='$stud_kod' 
		and upp.control_form_kod in (1,3,4,6,7) and oc.ocenka_kod in (3,4,5) $w_semestr
	";
	$avg_oc = sql_fetch_value($s);
	if (!$avg_oc) $avg_oc = 0;
	return $avg_oc;
}

function stud_has_exam_dopusk($stud_kod,$upp_kod) {
	$s = "SELECT COUNT(t2.cd_kod) FROM cd t1
	LEFT JOIN stud_cd t2 ON t1.cd_kod=t2.cd_kod 
	WHERE t2.stud_kod='$stud_kod' AND t1.cd_type_kod<>20 AND t1.uch_plan_punkt_kod='$upp_kod' 
	AND (t2.ball is not null and t2.ball >= t1.ball_min) ";
	$res = sql_query($s);
	$row = sql_numeric_array($res);
	return ($row[0]==cd_count($upp_kod));
}

function stud_has_oc_doc_later($stud_kod,$upp_kod,$d) {
	$r = sql_query("SELECT COUNT(t.doc) FROM 
		(SELECT vn.vedomost_kod as doc
			FROM decanat_vedomost_nomer vn
			INNER JOIN decanat_vedomost_sostav vs ON vn.vedomost_kod=vs.vedomost_kod 
			WHERE vn.uch_plan_punkt_kod=$upp_kod 
				AND vs.stud_kod=$stud_kod 
				AND vn.vedomost_data > '$d'
		UNION 
		SELECT ns.naprav_kod as doc 
			FROM decanat_naprav_sostav ns
			WHERE ns.uch_plan_punkt_kod=$upp_kod 
				AND ns.stud_kod=$stud_kod 
				AND ns.d_create >= '$d' 
		) t ");
	$row = sql_numeric_array($r);
	return ($row[0]>0);
}

// ������ �� ������ �����

function print_zv($n) {
	$s = "";
	for ($i = 1; $i <= $n; $i++) {
		$s .= "*";
	}
	return $s;
}

function stud_oc_vuz_array($stud_kod, $n = '') {
	$w  = ($n) ? " AND n='$n' " : "";
	$res = sql_query("SELECT stud_kod, n, vuz_kod, vuz_type_kod, vuz_gragd_kod, vuz_name 
	FROM stud_oc_vuz 
	WHERE stud_kod='$stud_kod' $w ORDER BY n ");
	$a = array();
	while ($row = sql_array($res)) {
		$str = print_zv($row['n']);
		if ($row['vuz_name']) {
			if ($row['vuz_type_kod']) $str .= vuz_type_name_socr($row['vuz_type_kod']) . ' ';
			$str .= $row['vuz_name'];
			if ($row['vuz_gragd_kod'] && $row['vuz_gragd_kod']<>643) $str .= ' (' . gragd_name($row['vuz_gragd_kod']) . ')';
		} elseif ($row['vuz_kod']) {
			if ($row['vuz_type_kod']) $str .= vuz_type_name_socr($row['vuz_type_kod']) . ' ';
			$str .= vuz_name($row['vuz_kod']);
		} else {
			$str .= '<b style="color:#ff0000;">�������� ���� ����������</b>';
		}
		$a[$row['n']] = $str; 
	}
	return $a;
}

function stud_oc_vuz_html($stud_kod, $n = '') {
	$a = stud_oc_vuz_array($stud_kod, $n);
	$html = '';
	foreach ($a as $n => $str) {
		$html .= '<span id="vuz_'.$n.'">' . $str . '</span><br>';
	}
	return $html;
}

function stud_oc_document_html($row) {
	$doc_info = ($row['ocenka_kod']) ? oc_doc_info($row['doc_kod'],$row['oc_doc_type_kod']) : array('nom'=>'','d'=>'','type'=>'');
	if ($doc_info['nom']=='' or $doc_info['d']=='') {
		$doc = '<b style="color: #004930;">' . $doc_info['type'] . '</b>';
		if (!empty($row['d'])) $doc .= '<br>(' . sql_date2rus($row['d']).')';
	} else $doc = $doc_info['type'] . ' �' . $doc_info['nom'] . ' �� ' . $doc_info['d'];
	return $doc;
}

function may_add_stud_oc_doc($stud_kod,$upp_kod) {
	return (current_user_is_admin()) 
		? true 
		: (!stud_upp_has_open_doc($stud_kod,$upp_kod) && !stud_has_good_oc($stud_kod,$upp_kod));
}

function _set_ocenka($upp_kod,&$mas_ocenka) {
	$total_ball = $mas_ocenka['total_balls'];
	if (!upp_has_ocenka($upp_kod)) {
		$ocenka = ($mas_ocenka['less_then_min']==0 and $total_ball >= 55) ? 6: 7;
	} elseif ($mas_ocenka['less_then_min']) {
		$ocenka=9 ; //��������
	} elseif (upp_has_exam($upp_kod) and $mas_ocenka['exam_balls'] === NULL) {
		$ocenka=8; //null �� ����
	} elseif (upp_has_exam($upp_kod) and $mas_ocenka['exam_balls'] < upp_exam_min_ball($upp_kod)) { // min exam ball ?
		$ocenka=2; 
	} else {
		if ($total_ball<55) $ocenka =2;
		elseif ($total_ball<70) $ocenka =3;
		elseif ($total_ball<85) $ocenka =4;
		else $ocenka =5;
	}
	$mas_ocenka['ocenka_kod']=$ocenka;
}

function stud_upp_exam_ball($stud_kod,$upp_kod) {
	$s = "SELECT ball FROM stud_cd 
	INNER JOIN cd ON stud_cd.cd_kod=cd.cd_kod 
	WHERE stud_kod='$stud_kod' AND uch_plan_punkt_kod='$upp_kod' AND cd_type_kod=20 ";
	$res = sql_query($s);
	$row = sql_numeric_array($res);
	return $row[0];
}

function _stud_upp_has_null_cd($stud_kod,$upp_kod) {
	$sql="select sum(ball_min*use_ball_min) from cd where uch_plan_punkt_kod='$upp_kod' AND cd_type_kod<>20 and 
		cd_kod not in (select cd_kod from stud_cd where stud_kod='$stud_kod' AND uch_plan_punkt_kod='$upp_kod' ) ";
	$res = sql_query($sql);	
	$row = sql_numeric_array($res);
	return 	$row[0]>0 ;
}

function _res_cd_balls($stud_kod,$upp_kod) {
	$up_kod=upp_up_kod($upp_kod);	
	$predmet_kod=upp_predmet_kod($upp_kod);
	$sql="SELECT sum(ex.koeff*res.ball_sum) res_ball_summ  
		FROM exam_koeff ex inner join
		(SELECT upp.semestr,sum(ball) as ball_sum FROM stud_cd 
			INNER JOIN cd ON stud_cd.cd_kod=cd.cd_kod 
			INNER JOIN uch_plan_punkt upp ON cd.uch_plan_punkt_kod =upp.uch_plan_punkt_kod
		WHERE stud_kod='$stud_kod' AND upp.uch_plan_kod='$up_kod' AND upp.predm_kod='$predmet_kod'
			AND cd_type_kod<>20 
		GROUP BY semestr) res
		ON ex.semestr=res.semestr WHERE ex.predmet_kod='$predmet_kod' AND ex.up_kod='$up_kod'
		";
	$res = sql_query($sql);	
	$row = sql_numeric_array($res);
	return 	$row[0] ;			
}

function _fill_cd_ball($stud_kod,$upp_kod,&$mas) {
	$s = "SELECT cd_nomer,ball,ball_min*use_ball_min as min FROM stud_cd 
	INNER JOIN cd ON stud_cd.cd_kod=cd.cd_kod 
	WHERE stud_kod='$stud_kod' AND uch_plan_punkt_kod='$upp_kod' AND cd_type_kod<>20 ";
	$res = sql_query($s);
	$cd_ball=0;
	$count_cd = cd_count($upp_kod);
	
	$i=0;
	while ($row = sql_array($res)) {
		$i++;
		$n=$row['cd_nomer'];
		$cd_ball +=$row['ball'];
		$mas['cd'][$n]=$row['ball'];
		if ($row['ball']<$row['min']) 
			$mas['less_then_min'] =1;
	}
	if ($i<$count_cd && _stud_upp_has_null_cd($stud_kod,$upp_kod)) 
		 $mas['less_then_min']=1;
	$mas['res_cd_balls'] = $mas['cd_balls'] = $cd_ball;
	/*
	if (upp_has_exam_koeff($upp_kod)) {
		$mas['res_cd_balls'] =_res_cd_balls($stud_kod,$upp_kod);
	}
	*/
}

function _stud_kurs($stud_kod,$upp_kod,&$mas) {
	$mas['res_exam_balls']=null;		
	$sql="select ball from stud_cd inner join cd on stud_cd.cd_kod=cd.cd_kod 
	where stud_kod='$stud_kod' and cd.uch_plan_punkt_kod='$upp_kod' and cd.cd_type_kod=12";
	$res=sql_query($sql);
	$row=sql_numeric_array($res);
	if ($row)  {
		$ball=$row[0];
		$mas['res_cd_balls']=$mas['cd_balls']=$ball;
		if ($ball===NULL) 
			$mas['ocenka_kod']=8;
		else 
			$mas['ocenka_kod']=sql_get_value('ocenka_kod','kursovik_ball_oc',"$ball>=ball_from and $ball<=ball_to and upp_kod=$upp_kod");
	} else {
		$mas['res_cd_balls']=$mas['cd_balls']=null;
		$mas['ocenka_kod']=8;
	}
}

function stud_upp_balls($stud_kod,$upp_kod,$ved_kursovik=0) {
	error_reporting(E_ALL | E_STRICT);
	$mas = array();
	$mas['less_then_min'] = 0;
	if ($ved_kursovik) 
		_stud_kurs($stud_kod,$upp_kod,$mas);
	else
		_fill_cd_ball ($stud_kod,$upp_kod,$mas);
		
	$cd_ball= $mas['cd_balls'] ;
	$mas['is_exam']=(upp_has_exam($upp_kod) and $ved_kursovik==0);
	
	if ($mas['is_exam']) {
		$koeff_exam = sql_get_value("koeff_exam*use_koeff_exam","uch_plan_punkt","uch_plan_punkt_kod='$upp_kod'");		
		$exam_ball=stud_upp_exam_ball($stud_kod,$upp_kod);
		$mas['cd_balls'] = $cd_ball;
		$mas['res_cd_balls'] *=  (1 - $koeff_exam);
		if ($exam_ball===null)  {
			$mas['exam_balls'] = $mas['res_exam_balls'] = $total_res = null;
		} else {
			$mas['res_exam_balls'] = $mas['exam_balls'] = $exam_ball ;
			if ($koeff_exam) $mas['res_exam_balls'] *= $koeff_exam ;
			$total_res = ceil($mas['res_exam_balls'] + $mas['res_cd_balls']);
		}
	} else {
		$mas['res_exam_balls'] = $mas['exam_balls'] = null;
		$total_res = ceil($cd_ball);
	}
	$mas['total_balls'] = $total_res; 
	if ($ved_kursovik==0 ) _set_ocenka($upp_kod,$mas);
	return $mas;
}

// ���������

function vedomost_info($vedomost_kod) {
	return sql_get_array('decanat_vedomost_nomer',"vedomost_kod='$vedomost_kod'");
}

function vedomost_exists($vedomost_kod) {
	return sql_get_value("COUNT(*)","decanat_vedomost_nomer","vedomost_kod='$vedomost_kod'");
}

function vedomost_is_finished($vedomost_kod) {
	return sql_get_value("is_finished","decanat_vedomost_nomer","vedomost_kod='$vedomost_kod'");
}

function vedomost_may_change($vedomost_kod) {
	return sql_get_value("count(*)","decanat_vedomost_sostav","vedomost_kod='$vedomost_kod' and may_change=1");
}

function ved_upp_kod($vedomost_kod) {
	return sql_get_value('uch_plan_punkt_kod','decanat_vedomost_nomer',"vedomost_kod='$vedomost_kod'");
}

function ved_grp_kod($vedomost_kod) {
	return sql_get_value('grp_kod','decanat_vedomost_nomer',"vedomost_kod='$vedomost_kod'");
}

function ved_prepod_kod($vedomost_kod) {
	return sql_get_value('prepod_kod','decanat_vedomost_nomer',"vedomost_kod='$vedomost_kod'");
}

function vedomost_update_ball($vedomost_kod) {
	$s = "select stud_kod from decanat_vedomost_sostav where vedomost_kod=$vedomost_kod ";
	$res = sql_query($s);
	while ($tmp = sql_array($res)) {
		$stud_kod = $tmp['stud_kod'];
		vedomost_update_ball_stud($vedomost_kod,$stud_kod);
	}
}

function vedomost_update_ball_stud($vedomost_kod,$stud_kod) {
	$upp_kod=ved_upp_kod($vedomost_kod);
	$cd_sostav = '';
	$mas_oz=stud_upp_balls($stud_kod,$upp_kod);
	$count_cd = cd_count($upp_kod);
	$start_i = upp_has_0_cd_nomer($upp_kod) ? 0 : 1;
	for ($i=$start_i;$i<=$count_cd;$i++) {
		if (!isset($mas_oz['cd'][$i])) $cd_sostav .= ';';
		else $cd_sostav .= $mas_oz['cd'][$i] . ';';
	}
	$cd_sostav = substr($cd_sostav,0,-1);
	$exam_ball = ($mas_oz['res_exam_balls']!==null) ? $mas_oz['res_exam_balls']: 'null';
	$cd_balls = ($mas_oz['res_cd_balls']!==null) ? $mas_oz['res_cd_balls']: 'null';
	$total_balls = ($mas_oz['total_balls']!==null) ? $mas_oz['total_balls']: 'null';
	
	$upd = "UPDATE  decanat_vedomost_sostav SET  cd_sostav='$cd_sostav', cd_balls=$cd_balls, exam_balls=$exam_ball, total_balls=$total_balls, ocenka_kod=${mas_oz['ocenka_kod']} 
	WHERE stud_kod=$stud_kod and vedomost_kod=$vedomost_kod";
	sql_query($upd);
}

function vedomost_has_empty_oc($vedomost_kod) {
	return sql_get_value("count(*)","decanat_vedomost_sostav","ocenka_kod is null AND vedomost_kod='$vedomost_kod' ");
}

function update_block_status($vedomost_kod,$is_finished) {
	if (!vedomost_has_empty_oc($vedomost_kod) or $is_finished==0) {
		sql_query("UPDATE decanat_vedomost_sostav SET may_change=0 WHERE vedomost_kod=$vedomost_kod ");
		$s = "UPDATE decanat_vedomost_nomer SET is_finished=$is_finished WHERE vedomost_kod=$vedomost_kod ";
		sql_query($s);
		sql_query("CALL ocenka_from_vedomost($vedomost_kod) ");
	}
}

// �����������

function naprav_info($naprav_kod) {
	$row=sql_get_array("decanat_naprav_sostav","naprav_kod='$naprav_kod'");
	$mas=array();
	$mas['naprav_kod']=$row['naprav_kod'];
	$mas['upp_kod']=$row['uch_plan_punkt_kod'];	
	$mas['nom'] = (!empty($row['nomer_prefix'])) ? $row['nomer_prefix'] . '-' . $row['naprav_nomer'] : $row['naprav_nomer'];
	$mas['d_create']=$row['d_create'];
	$mas['d_po']=$row['d_po'];
	$mas['is_finished']=$row['is_finished'];
	$mas['stud_kod']=$row['stud_kod'];
	$mas['prepod_kod']=$row['prepod_kod'];
	$mas['pract_prepod_kod']=$row['pract_prepod_kod'];
	$mas['d_sdano']=$row['d_sdano'];
	$mas['is_zav_kaf']=$row['is_zav_kaf'];
	$mas['ocenka_kod']=$row['ocenka_kod'];
	$mas['predmet_name']=upp_predmet_name($row['uch_plan_punkt_kod']);
	$stud_info=stud_info($row['stud_kod']);
	$mas['stud_fio']=$stud_info['fio'];
	$mas['grp_nomer']=$stud_info['grp_nomer'];
	return $mas;
}

function naprav_expire_sql() {
	$curdate = date("Y-m-d");
	return "SUBDATE('$curdate', INTERVAL 3 DAY)";
}

function naprav_exists($naprav_kod) {
	return sql_get_value("COUNT(*)","decanat_naprav_sostav","naprav_kod='$naprav_kod'");
}

function naprav_is_expired($naprav_kod) {
	return (sql_get_value("count(naprav_kod)","decanat_naprav_sostav","naprav_kod='$naprav_kod' and d_po < " . naprav_expire_sql())>0);
}

function naprav_is_finished($naprav_kod) {
	return sql_get_value("is_finished","decanat_naprav_sostav","naprav_kod='$naprav_kod'");
}

function naprav_update_ball($naprav_kod) {
	$naprav=sql_get_array("decanat_naprav_sostav","naprav_kod='$naprav_kod'");
	$count_cd = cd_count($naprav['uch_plan_punkt_kod']);
	$mas_oz = stud_upp_balls($naprav['stud_kod'],$naprav['uch_plan_punkt_kod']);
	$cd_sostav = '';
	$start_i = upp_has_0_cd_nomer($naprav['uch_plan_punkt_kod']) ? 0 : 1;
	for ($i=$start_i;$i<=$count_cd;$i++) {
		if (!isset($mas_oz['cd'][$i])) $cd_sostav .= ';';
		else $cd_sostav .= $mas_oz['cd'][$i] . ';';
	}
	$cd_sostav = substr($cd_sostav,0,-1);
	$exam_ball = ($mas_oz['res_exam_balls']!==null) ? $mas_oz['res_exam_balls']: 'null';
	$cd_balls = ($mas_oz['res_cd_balls']!==null) ? $mas_oz['res_cd_balls']: 'null';
	$total_balls = ($mas_oz['total_balls']!==null) ? $mas_oz['total_balls']: 'null';
	$d_sdano = date("Y-m-d");
	$upd = "UPDATE decanat_naprav_sostav SET cd_sostav='$cd_sostav', cd_balls=$cd_balls, exam_balls=$exam_ball, total_balls=$total_balls, ocenka_kod=${mas_oz['ocenka_kod']}, d_sdano='$d_sdano' 
	WHERE naprav_kod=$naprav_kod";
	sql_query($upd);
}

function update_naprav_block_status($naprav_kod,$is_finished) {
	$upp_kod = sql_get_value("uch_plan_punkt_kod","decanat_naprav_sostav","naprav_kod=$naprav_kod");
	$up_kod = upp_up_kod($upp_kod);
	$s = "UPDATE decanat_naprav_sostav SET is_finished=$is_finished WHERE naprav_kod=$naprav_kod ";
	sql_query($s);
	sql_query("CALL ocenka_from_naprav($naprav_kod) ");
}

// ����� �����������

function rashod_list_info($rashod_list_kod) {
	return sql_get_array("decanat_rashod_list_nomer","rashod_list_kod='$rashod_list_kod'");
}

function update_rashod_list_block_status($rashod_list_kod,$is_finished) {
	$s = "UPDATE decanat_rashod_list_nomer SET is_finished=$is_finished WHERE rashod_list_kod=$rashod_list_kod ";
	sql_query($s);
	sql_query("CALL ocenka_from_rashod_list($rashod_list_kod) ");
}

// �������

function prikaz_info($prikaz_nomer_kod) {
	return sql_get_array("prikaz_nomer","prikaz_nomer_kod='$prikaz_nomer_kod'");
}

function prikaz_type_name($prikaz_type_kod) {
	return sql_get_value('prikaz_type_name','s_prikaz_type',"prikaz_type_kod='$prikaz_type_kod'");
}

function prikaz_type_status_kod($prikaz_type_kod) {
	return sql_get_value("status_kod","s_prikaz_type","prikaz_type_kod='$prikaz_type_kod'");
}

function prikaz_type_is_stipendia($prikaz_type_kod) {
	return sql_get_value("is_stipendia","s_prikaz_type","prikaz_type_kod='$prikaz_type_kod'");
}

function prikaz_type_approver($prikaz_type_kod) {
	return sql_get_value("approver_kod","s_prikaz_type","prikaz_type_kod='$prikaz_type_kod'");
}

function prikaz_is_stipendia($prikaz_nomer_kod) {
	$pn = prikaz_info($prikaz_nomer_kod);
	$is_stip = prikaz_type_is_stipendia($pn['prikaz_type_kod']);
	if (!$is_stip) {
		$is_stip = sql_fetch_value("select t.is_stipendia
			from prikaz_sostav ps 
			inner join s_prikaz_type t on ps.prikaz_type_kod=t.prikaz_type_kod 
			where prikaz_nomer_kod='$prikaz_nomer_kod' order by t.is_stipendia limit 0,1 ");
	}
	return $is_stip;
}

function prikaz_approvers($prikaz_nomer_kod) {
	$pss = sql_rows("SELECT DISTINCT prikaz_type_kod FROM prikaz_sostav WHERE prikaz_nomer_kod='$prikaz_nomer_kod'");
	$a = [];
	foreach ($pss as $ps) {
		$a[] = prikaz_type_approver($ps['prikaz_type_kod']);
	}
	return $a;
}

function prikaz_finec_ru_nomer_in($n) {
	$in = array();
	if ($n) {
		$in[] = "'$n'";
		$in[] = "'".str_replace("�","C",$n)."'";
		$in[] = "'".str_replace("�","c",$n)."'";
		$in[] = "'".str_replace("C","�",$n)."'";
		$in[] = "'".str_replace("c","�",$n)."'";
		$in[] = "'".str_replace("�","K",$n)."'";
		$in[] = "'".str_replace("�","k",$n)."'";
		$in[] = "'".str_replace("K","�",$n)."'";
		$in[] = "'".str_replace("k","�",$n)."'";
		$in[] = "'".str_replace(" ","",$n)."'";
	} else {
		$in[] = '-1';
	}
	return implode(",",$in);
}

function prikaz_finec_ru_exists($prikaz_nomer_kod) {
	$pn = prikaz_info($prikaz_nomer_kod);
	$exists = false;
	if ($pn && $_SERVER['SERVER_NAME']=='dekanat.unecon.ru') {
		$exists = (sql_get_value("file_id","`odipdb`.`files`","nomer='".html_special_chars($pn['prikaz_nomer'])."' AND d='" . $pn['prikaz_data'] . "'")>0);
	}
	return $exists;
}

function prikaz_nomer_data($prikaz_nomer_kod) {
	$p = prikaz_info($prikaz_nomer_kod);
	$s = '';
	if ($p['prikaz_nomer']) $s .= $p['prikaz_nomer'];
	if ($p['prikaz_data']) $s .= ' �� ' . sql_date2rus($p['prikaz_data']);
	return $s; 
}

function prikaz_about_name($about_kod) {
	return sql_get_value("about_name","s_prikaz_about","about_kod='$about_kod'");
}

function prikaz_about_print_name($about_kod) {
	return sql_get_value("print_name","s_prikaz_about","about_kod='$about_kod'");
}

function prikaz_preambula_name($preambula_kod) {
	return sql_get_value("preambula_name","s_prikaz_preambula","preambula_kod='$preambula_kod'");
}

function prikaz_osnovanie_name($osnovanie_kod) {
	return sql_get_value("prikaz_osnovanie_name","s_prikaz_osnovanie","prikaz_osnovanie_kod='$osnovanie_kod'");
}

function prikaz_osnovanie_has_protocol($osnovanie_kod) {
	return sql_get_value("has_protocol","s_prikaz_osnovanie","prikaz_osnovanie_kod='$osnovanie_kod'");
}

function prikaz_formulirovka_text($prikaz_nomer_kod,$paragraf) {
	return sql_get_value("formulirovka_text","prikaz_struct","prikaz_nomer_kod='$prikaz_nomer_kod' and paragraf='$paragraf'");
}

// �������

function prepod_info($prepod_kod) {
	$prepod = sql_get_array("`dekanat`.prepod","prepod_kod='$prepod_kod'");
	if ($prepod) {
		$prepod['fio'] =  $prepod['prepod_fio'];
		$prepod['fio_socr'] =  prepod_fio_socr($prepod_kod);
		$prepod['kafedra_name'] = kafedra_name($prepod['kafedra_kod']);
	}
	return $prepod;
}

function prepod_fio($prepod_kod) {
	$p = prepod_info($prepod_kod);
	return $p['prepod_fio'];
}

function prepod_fio_socr($prepod_kod) {
	return sql_get_value("concat(prepod_fam, ' ', substring(prepod_im,1,1), '.', if(prepod_otc is null or prepod_otc='', '', 
		concat(substring(prepod_otc,1,1),'.'))) AS prepod_fio","`dekanat`.prepod","prepod_kod='$prepod_kod' ");
}

function prepod_email($prepod_kod) {
	return sql_get_value("email","`staff`.v_user","user_id='$prepod_kod'");
}

function prepod_is_zav_kaf($prepod_kod) {
	$p = prepod_info($prepod_kod);
	return $p['is_zav_kaf'];
}

function prepod_zav_kafedra_kod($prepod_kod) {
	$p = prepod_info($prepod_kod);
	return $p['is_zav_kaf'] ? $p['kafedra_kod'] : 0;
}

function prepod_kafedra_kod($prepod_kod) {
	$p = prepod_info($prepod_kod);
	return $p['kafedra_kod'];
}

function prepods_dubli_fio() {
	$sql = "SELECT p.prepod_fio, count(p.prepod_kod) AS cnt
	FROM `dekanat`.prepod p GROUP BY prepod_fio HAVING count(p.prepod_kod)>1 ";
	$res = sql_query($sql);
	$dubli_fio = [];
	while ($row = sql_array($res)) {
		$dubli_fio[] = $row['prepod_fio'];
	}
	return $dubli_fio;
}

function prepod_row_2autocomplete_fio($row,$has_dubli = false) {
	$s = $row['prepod_fio'];
	if ($has_dubli && $row['kafedra_kod']) $s .= ' ('.$row['kafedra_name'].')';
	if (array_key_exists('uvolen',$row) && $row['uvolen']) $s .= ' ('.$row['uvolen'].')';
	return $s;
}

function prepod_kod_2autocomplete_fio($prepod_kod) {
	$dubli_fio = prepods_dubli_fio();
	$prepod = prepod_info($prepod_kod);
	$has_dubli = (in_array($prepod['prepod_fio'],$dubli_fio));
	$prepod['uvolen'] = ($prepod['is_actual']==0) ? '������' : '';
	return prepod_row_2autocomplete_fio($prepod,$has_dubli);
}

function prepod_kod_fio($prepod_fio) {
	if (strlen($prepod_fio)==0) return '';
	$len = strpos($prepod_fio,'(');
	if (!$len) $len = strlen($prepod_fio);
	$w_prepod_fio = trim(substr($prepod_fio,0,$len));
	$prepods = sql_rows("SELECT p.prepod_kod, p.prepod_fio, p.is_actual, p.kafedra_kod, k.kafedra_name 
	FROM `dekanat`.prepod p 
	LEFT JOIN `dekanat`.kafedra k ON p.kafedra_kod=k.kafedra_kod 
	WHERE prepod_fio like '$w_prepod_fio%' ");
	$has_dubli = count($prepods)>1;
	$prepod_kod = false; 
	foreach ($prepods as $prepod) {
		$prepod['uvolen'] = ($prepod['is_actual']==0) ? '������' : '';
		if (prepod_row_2autocomplete_fio($prepod,$has_dubli) == $prepod_fio) {
			$prepod_kod = $prepod['prepod_kod'];
			break;
		}
	}
	
	return $prepod_kod;
}

function prepod_fio_concat($alias = 'prep') {
	return "concat($alias.prepod_fam, ' ', $alias.prepod_im, ' ', ifnull($alias.prepod_otc, '')) ";
}

function prepod_dol_name($prepod_kod) {
	$p = prepod_info($prepod_kod);
	return $p['prepod_dol_name'];
}

function prepod_stepen_name($prepod_kod) {
	$p = prepod_info($prepod_kod);
	return $p['prepod_uch_stepen'];
}

function prepod_zvanie_name($prepod_kod) {
	$p = prepod_info($prepod_kod);
	return $p['prepod_uch_zv'];
}

// ������ 
function fms_name($code) {
	return sql_get_value("name","`dekanat`.s_fms","code='$code'");
} 

function vuz_rekvizit() {
	$a = array();
	$rr = sql_rows("select * from s_vuz_rekvizit ");
	foreach ($rr as $r) {
		$a[$r['rekvizit']] = $r['rekvizit_val'];
	}
	return $a;
}

function array_numeric_keys($a) {
	$new = array();
	if (is_array($a)) {
		foreach ($a as $val) {
			if (is_numeric($val)) $new[] = $val;
		}
	}
	return $new;
}

function zero_pad($s,$len) {
	return str_pad($s,$len,'0',STR_PAD_LEFT);
}

function value_is_all($v) {
	return (is_array($v)) ? (count($v)==1 && $v[0]=='all') : ($v=='all');
}

function row2hiddenInps($row) {
	$s = '';
	foreach ($row as $key=>$value) $s .= '<input type="hidden" name="'.$key.'" value="'.$value.'">';
	return $s;
}

function rus_mec_name($n,$rp = false) {
	$a = array	('01'=>'������/������',
				'02'=>'�������/�������',
				'03'=>'����/�����',
				'04'=>'������/������',
				'05'=>'���/���',
				'06'=>'����/����',
				'07'=>'����/����',
				'08'=>'������/�������',
				'09'=>'��������/��������',
				'10'=>'�������/�������',
				'11'=>'������/������',
				'12'=>'�������/�������'
	);
	if (isset($a[$n])) {
		$mec = explode("/",$a[$n]);
		if ($rp) $mec_name = $mec[1];
		else $mec_name = $mec[0];
	} else $mec_name = '';
	return $mec_name;
}

function date_full_name($sql_d,$add_g = false) {
	// 2015-07-01 => 01 ���� 2015 �.
	$new_d = '';
	if (preg_match('/^[0-9]{4}-[0-9]{2}-[0-9]{2}$/', $sql_d)) {
		$y = substr($sql_d, 0, 4);
		$m = substr($sql_d, 5, 2);
		$d = substr($sql_d, 8, 2);
		$new_d = $d.' '.rus_mec_name($m,true).' '.$y;
		if ($add_g) $new_d .= ' �.'; 
	}
	return $new_d;
}

function oc_forms_default_y() {
	$current_date = date("Y-m-d");
	$current_year = date("Y");
	$default_year = ($current_date > $current_year.'-07-15') ? $current_year : ($current_year - 1); 
	return $default_year;
}

/***********************************************************************************/
/********************** ���������� �����, ��������� � �� ***************************/
/***********************************************************************************/

function year_semestr_def_sess($year) {
	$tek_year = date("Y");
	$tek_mec = date("m");
	$tek_date = date("Y-m-d");
	/* ������ ������ � 1 ������� �� 30 ������ ����.���� */
	/* 
		���� ������ �������, �� ������ �������� � ����.���� !!! 
		���� ���, �� ��� �������� � ����.���� !!!
	*/
	if ($tek_mec == 12) {
		$date_s = $tek_year . '-12-01';
		$date_po = $tek_year + 1 . '-04-01';
	} else {
		$date_s = $tek_year - 1 . '-12-01';
		$date_po = $tek_year . '-04-01';
	}
	$diff = (is_numeric($year)) ? ($tek_year-$year) : 0;
	if ($tek_date > $date_s and $tek_date < $date_po) {
		if ($tek_mec == 12) {
			$semestr = $diff * 2 + 1;
		} else {
			$semestr = $diff * 2 - 1;
		}
	} else {
		$semestr = $diff * 2;
	}
	if ($semestr==0) $semestr = 1;
	return $semestr;
}

function year_semestr_def_semestr($year,$cur_date = null) {
	// 04.02.17 dim �������:
	if (!is_numeric($year) || !preg_match('/^[0-9]{4}$/',$year)) $year = date("Y");
	
	$start_date = $year.'-09-01';
	if (!$cur_date) $cur_date = date("Y-m-d");

	if ($start_date > $cur_date) {
		$semestr = 1;
	} else {
		$days = date_difference($start_date,$cur_date);
		$years = round($days/365.25,2);
		$years_part = $years-intval($years);
		if ($years_part >= 0.5) {
			// ������� � ���� ���������
			$semestr = round($years)*2;
		} else {
			$semestr = round($years)*2 + 1;
		}
		if ($semestr==0) $semestr = 1;
	}
	return $semestr;
}

/*
function year_semestr_def_semestr($year) {
	$tek_year = date("Y");
	$tek_date = date("Y-m-d");
	// ������� ������� � 1 �������� �� 31 ������� 
	$date_s = $tek_year . '-09-01';
	$date_po = $tek_year . '-12-31';
	$diff = ($tek_year-$year);
	if ($tek_date > $date_s and $tek_date <= $date_po) {
		$semestr = $diff * 2 + 1;
	} else {
		$semestr = $diff * 2;
	}
	if ($semestr==0) $semestr = 1;
}

*/

function __year_kurs_date($parametr,$value) {
	$d = date("Y-m-d");
	$y = date("Y");
	// ������� ���� ���� �������� ���, ������� ��� ���� �������� ����
	$v = $y - $value;
	if ($d > ($y . '-07-15')) $v++;
	return $v;
}

function __kurs_year($kurs) {
	return __year_kurs_date('kurs',$kurs);
}

function __year_kurs($year) {
	return __year_kurs_date('year',$year);
}

/***********************************************************************************/




/// ������ �����!!!!!!!!!!!!!

function stud_zachisl_prikaz($stud_kod) {
	$ps = sql_rows("select ps.*, pn.prikaz_nomer, pn.prikaz_data from prikaz_sostav ps
	inner join prikaz_nomer pn on ps.prikaz_nomer_kod=pn.prikaz_nomer_kod 
	where ps.abitur_kod='$stud_kod'
		and ps.prikaz_type_kod in (1,6) 
	order by pn.prikaz_data desc, ps.stud_prikaz_ord desc 
	limit 0,1 
	");
	return $ps;
}

function stud_prikaz($stud_kod,$array_prikaz_kod,$is_pdf = false) {
	$str_prikaz_kod = '';
	foreach ($array_prikaz_kod as $value) {
		$str_prikaz_kod .= $value . ',';
	}
	$str_prikaz_kod = substr($str_prikaz_kod,0,-1);
	
	$s = "SELECT pn.prikaz_nomer, pn.prikaz_data, ps.prikaz_text_kod, ps.paragraf, ps.punkt, ps.nomer, ps.prikaz_type_kod, ps.prichina_kod  
	FROM prikaz_sostav ps
	INNER JOIN prikaz_nomer pn ON ps.prikaz_nomer_kod=pn.prikaz_nomer_kod 
	WHERE ps.abitur_kod=$stud_kod AND ps.prikaz_type_kod IN ($str_prikaz_kod) AND ps.is_otchisl=0 AND ps.prikaz_nomer_kod <> 27 
	ORDER BY pn.prikaz_nomer, pn.prikaz_data ";
	$res = sql_query($s);
	$net_data = ($is_pdf) ? '��� ������' : '<b style="color: #ff0000;">��� ������</b>';
	if (sql_count_rows($res)>0) {
		$s = '';
		while ($row = sql_array($res)) {
			$n = (!empty($row['prikaz_nomer'])) ? '� ' . $row['prikaz_nomer'] : $net_data;
			$para = (!empty($row['paragraf'])) ? '  &sect;' . $row['paragraf'] : ' �. ';
			$pu = (!empty($row['punkt'])) ? $row['punkt'] . '.' : '';
			$nom = (!empty($row['nomer'])) ?  $row['nomer'] . '.' : '';
			if ($is_pdf) {
				$d = (!empty($row['prikaz_data'])) ? ' �� ' . sql_date2rus($row['prikaz_data']) : '';
				$s .= $n . $d;
			} else {
				$d = (!empty($row['prikaz_data'])) ? ' �� ' . sql_date2rus($row['prikaz_data'])  . ' �.' : '';
				$s .= $n . $para . $pu . $nom . $d;
			}
			if ($row['prikaz_type_kod']==1 or $row['prikaz_type_kod']==6) {
				$osn_zach_kod = sql_get_value("osn_zach_kod","decanat_stud","stud_kod=$stud_kod");
				if ($osn_zach_kod > 0) {
					$s .= ' (���������: ' . osn_zach_name($osn_zach_kod) . ')';
				} else $s .= ' (��������� ����������)';
			} elseif ($row['prikaz_type_kod']==2) {
				if (!empty($row['prichina_kod'])) {
					$s .= ' (�������: ' . prichina_name($row['prichina_kod']) . ')';
				} else $s .= ' (������� ����������)';
			}
			if (!$is_pdf) $s .= '<br>';
		}
	} else $s = $net_data . '<br>';
	return $s;
}

// files

function file_mime($filename) {
	$mime = '';
	if (file_exists($filename)) {
		$finfo = finfo_open(FILEINFO_MIME_TYPE);
		$mime = finfo_file($finfo,$filename);
		finfo_close($finfo);
	}
	return ($mime);
}

// stud personal results 

function pr_type_info($pr_type_kod) {
	return sql_get_array("`dekanat`.s_pr_type","pr_type_kod='$pr_type_kod'");
}

function pr_type_print_name($pr_type_kod) {
	$pr_type = pr_type_info($pr_type_kod);
	return $pr_type['print_name'];
}

function pr_type_prim($pr_type_kod) {
	$pr_type = pr_type_info($pr_type_kod);
	return $pr_type['prim'];
}

function pr_doc_type_info($pr_doc_type_kod) {
	return sql_get_array("`dekanat`.s_pr_doc_type","pr_doc_type_kod='$pr_doc_type_kod' ");
}

function pr_doc_type_name($pr_doc_type_kod) {
	$pr_doc = pr_doc_type_info($pr_doc_type_kod);
	return $pr_doc['pr_doc_type_name'];
}

function pr_doc_types($pr_type_kod) {
	$w = '';
	if ($pr_type_kod == 1) $w .= " AND is_diplom=1 ";
	elseif ($pr_type_kod == 2) $w .= " AND is_publ=1 ";

	return sql_to_assoc("SELECT pr_doc_type_kod, pr_doc_type_name 
		FROM `dekanat`.s_pr_doc_type 
		WHERE true $w ORDER BY ord ");
}

function pr_type_cols($pr_type_kod) {
	if ($pr_type_kod == 1) $cols = pr_table_cols(1);
	elseif ($pr_type_kod == 2) $cols = pr_table_cols(2);
	else $cols = pr_table_cols(3);
	return $cols;
}

function pr_table_cols($cols_type_kod) {
	$pre_cols = sql_rows("SELECT * FROM `dekanat`.s_pr_table_cols ORDER BY type, ord ");
	$cols = [];
	foreach ($pre_cols as $pre_col) {
		$cols[$pre_col['type']][$pre_col['name']] = $pre_col['label'];
	}
	return (array_key_exists($cols_type_kod, $cols)) ? $cols[$cols_type_kod] : $cols[3];
}

function pr_attachment_new_filename($file_name,$file_path) {
	$name = substr($file_name, 0, strrpos($file_name,'.'));
	$ext = substr($file_name, strrpos($file_name,'.'));

	$new_name = $name.$ext;
	$full_name = $file_path . $new_name;
	$n = 0;
	while (file_exists($full_name)) {
		$n++;
		$new_name = $name.'('.$n.')'.$ext;
		$full_name = $file_path . $new_name;
		if ($n > 1000) exit();
	}
	return $new_name;
}

function pr_attachment_download_url($path,$file_name) {
	$doc_name_encoded = urlencode($file_name);
	return 'https://dekanat.unecon.ru/DOCS/stud_files/'.$path.'/'.$doc_name_encoded;
}

function pr_attachment_name_link($doc,$url_encoding = 'cp1251') {
	$url = pr_attachment_download_url($doc['path'],$doc['file_name'],$url_encoding);
	$attachment_name = ($doc['stud_pr_doc_name']) ? $doc['stud_pr_doc_name'] : pr_doc_type_name($doc['pr_doc_type_kod']);
	$s = '<a href="'.$url.'" target="_blank">'.$attachment_name .'</a>';
	return $s;
}

function morph_padej($letter) {
	$a = array(
		'�'=>'rod',
		'�'=>'dat',
		'�'=>'vin',
		'�'=>'tvor',
		'�'=>'predl',
		'�_�'=>'predl-o',
		'�'=>'gde'
	);
	return array_key_exists($letter,$a) ? $a[$letter] : '';
}


function str_morph($s,$padej) {
	if (function_exists('morpher_inflect')) {
		if (strlen($s)>0) {
			$s = iconv('windows-1251','utf-8',$s);
			$s = morpher_inflect($s,morph_padej($padej));
			$s = iconv('utf-8','windows-1251',$s);
		}	
	} else die ('Morpher is not installed!');
	return $s;
}

?>