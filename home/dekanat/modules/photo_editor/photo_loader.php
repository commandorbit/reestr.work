<?php
require_once $_SERVER['DOCUMENT_ROOT'].'/init.php';
error_reporting(E_ALL | E_STRICT);
$may_edit_photo = current_user_may("may_stud_photo");

$stud_kod = request_numeric_val('s',0);

if ($may_edit_photo && $stud_kod && isset($_FILES['photo_file'])) {
	$photo_file_dest = $_SERVER['DOCUMENT_ROOT'].'/photoes/temp/'.$stud_kod.'.jpg';
	$mime = $_FILES["photo_file"]["type"];
	if ($mime == "image/jpeg" || $mime == "image/jpg") {
		move_uploaded_file($_FILES['photo_file']['tmp_name'], $photo_file_dest);
	} else {
		die('wrong mime type of file!');
	}
	header('Location: /student.php?s='.$stud_kod.'&upload_ok');
	exit();
}
?>