<?php
require_once $_SERVER['DOCUMENT_ROOT'].'/init.php';
error_reporting(E_ALL | E_STRICT);
ini_set('gd.jpeg_ignore_warning', 1);
$may_edit_photo = current_user_may("may_stud_photo");

$stud_kod = request_numeric_val('stud_kod',0);
$size = request_val('size',0);
$coords = request_val('coords',0);

if ($may_edit_photo && $stud_kod) {
	$ok = photo_obrabotka($stud_kod,$size,$coords);
	if ($ok) {
		$tmp_name = $_SERVER['DOCUMENT_ROOT'].'/photoes/temp/' . $stud_kod .'.jpg';
		if (file_exists($tmp_name)) unlink($tmp_name);
		header('Location: /student.php?s='.$stud_kod);
		exit();
	}
}

function photo_obrabotka($stud_kod,$size,$coords) {
	$ok = false;
	$orig_filename = $new_filename = null;
	// ������������ 3x4
	if ($size == '3x4') {
		$orig_filename = $_SERVER['DOCUMENT_ROOT'].'/photoes/temp/' . $stud_kod .'.jpg';
		$new_filename = $_SERVER['DOCUMENT_ROOT'].'/photoes/abitur_' . $stud_kod .'.jpg';
	}
	// ������������ 2x2
	if ($size == '2x2') {
		$orig_filename = $_SERVER['DOCUMENT_ROOT'].'/photoes/abitur_' . $stud_kod .'.jpg';
		$new_filename = $_SERVER['DOCUMENT_ROOT'].'/photoes/gazprom/abitur_2x2_' . $stud_kod .'.jpg';
	}
	// ������������ ���� 
	$a_coords = explode("-",$coords);
	if (count($a_coords)==4 && $orig_filename && $new_filename) {
		// ��������� ������ ���� 4
		// ������� ����.���� ���� ��� ����
		if (file_exists($new_filename)) {
			copy($new_filename, $new_filename.'.'.time());
			unlink($new_filename); 
		}

		$image_size = getimagesize($orig_filename);
		$image_w = $image_size[0];
		$image_h = $image_size[1];
		$k = $image_w/$image_h;
		$x1 = $a_coords[0]/300*$image_w;
		$y1 = $a_coords[1]/300*$image_h*$k;
		$x2 = $a_coords[2]/300*$image_w;
		$y2 = $a_coords[3]/300*$image_h*$k;
		
		$new_image = imagecreatetruecolor($x2-$x1,$y2-$y1);
		$image = @imagecreatefromjpeg($orig_filename);
		imagecopyresampled($new_image, $image, 0, 0, $x1, $y1, $x2-$x1, $y2-$y1, $x2-$x1, $y2-$y1);
		if (imagejpeg($new_image, $new_filename, 100)) {
			imagedestroy($new_image);
			$ok = true;
			if ($size == '3x4') {
				sql_query("UPDATE `dekanat`.abitur SET has_photo=1 WHERE abitur_kod='$stud_kod'");
			}
			if ($size == '2x2') {
				sql_query("UPDATE `dekanat`.abitur SET has_photo_2x2=1 WHERE abitur_kod='$stud_kod'");
			}
		}
	}
	return $ok;
}

?>