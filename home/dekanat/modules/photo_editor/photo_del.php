<?php
require_once $_SERVER['DOCUMENT_ROOT'].'/init.php';
error_reporting(E_ALL | E_STRICT);
$may_edit_photo = current_user_may("may_stud_photo");

$stud_kod = request_numeric_val('s',0);
$del_2x2 = request_numeric_val('del_2x2',0);

if ($may_edit_photo && $stud_kod) {
	$photo_3x4 = $_SERVER['DOCUMENT_ROOT']."/photoes/abitur_" . $stud_kod . ".jpg";
	$photo_2x2 = $_SERVER['DOCUMENT_ROOT']."/photoes/gazprom/abitur_2x2_" . $stud_kod . ".jpg";
	if ($del_2x2) {
		if (file_exists($photo_2x2)) {
			copy($photo_2x2, $photo_2x2.'.'.time());
			unlink($photo_2x2);
			sql_query("UPDATE `dekanat`.abitur SET has_photo_2x2=0 WHERE abitur_kod='$stud_kod'");
		}
	} else {
		if (file_exists($photo_3x4)) {
			copy($photo_3x4, $photo_3x4.'.'.time());
			unlink($photo_3x4);
			sql_query("UPDATE `dekanat`.abitur SET has_photo=0 WHERE abitur_kod='$stud_kod'");
		}
	}
}
header('Location: /student.php?s='.$stud_kod);
exit();
?>