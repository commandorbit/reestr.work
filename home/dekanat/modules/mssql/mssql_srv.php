<?

define('MSSQL_PDO',true);
define('MSSQL_ASSOC',1);
define('MSSQL_NUM',2);

function mssql_connect($host,$user,$pass) {
	$serverName = "$host"; //serverName\instanceName
//	$connectionInfo = array( "Database"=>"KeeperMSSQL", "UID"=>$user, "PWD"=>$pass);
	$connectionInfo = array("UID"=>$user, "PWD"=>$pass, "CharacterSet" =>"UTF-8",'ReturnDatesAsStrings'=>true);
	$conn = sqlsrv_connect($serverName, $connectionInfo);

	if( $conn ) {
		// echo "Connection established.<br />";
	} else {
		 echo "Connection could not be established.<br />";
		 die( print_r( sqlsrv_errors(), true));
	}	
	return $conn;
}

function mssql_select_db($db,$link) {
	$link->query("USE [$db]");
}

function mssql_query($sql,$link) {
	if (!$link) {
		throw new Exception(" No connection!");
	}
	$result = @sqlsrv_query($link,$sql);
	if (!$result) {
		$err_info = implode(', ',$link->errorInfo());
		throw new Exception("error in sql!" . $sql . " err: " . $err_info);
	}
	return $result;
}

function mssql_fetch_array($cursor,$opt = MSSQL_ASSOC) {	
	if ($opt == MSSQL_ASSOC) {
		$newopt = SQLSRV_FETCH_ASSOC;
	} elseif ($opt == MSSQL_NUM) {
		$newopt = SQLSRV_FETCH_NUMERIC;
	} else {
		$newopt = SQLSRV_FETCH_BOTH;
	}
	return sqlsrv_fetch_array($cursor,$newopt);
}

function mssql_guid_string($binguid) {
	if (empty($binguid)) return null;
	if (preg_match('/^[0-9A-F]{8}[-]([0-9A-F]{4}[-]){3}[0-9A-F]{12}$/',$binguid)) {
		return $binguid;
	}

	$u = unpack('Va/v2b/n2c/Nd', $binguid);
	return sprintf('%08X-%04X-%04X-%04X-%04X%08X', $u['a'], $u['b1'], $u['b2'], $u['c1'], $u['c2'], $u['d']);
}