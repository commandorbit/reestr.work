<?
define('MSSQL_PDO',true);
define('MSSQL_ASSOC',1);
define('MSSQL_NUM',2);

function mssql_connect($host,$user,$pass) {
	$opt = array(
		// PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION
		//,PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC
	);
	
	//$mssql = new PDO("dblib:host=172.17.200.1:1433;dbname=$db_name;charset=UTF-8", $db_user, $db_pass,$opt);
	
	$link = new PDO("dblib:host=$host;charset=UTF-8", $user, $pass,$opt);
	if (!$link) die("error connecting!");	
	return $link;
}

function mssql_select_db($db,$link) {
	$link->query("USE [$db]");
}

function mssql_query($sql,$link) {
	$result = $link->query($sql);
	if (!$result) {
		$err_info = implode(', ',$link->errorInfo());
		throw new Exception("error in sql!" . $sql . " err: " . $err_info);
	}
	return $result;
}

function mssql_execute($sql,$link) {
	return $link->exec($sql);
}

function mssql_num_rows($cursor) {
	/*
	If the last SQL statement executed by the associated PDOStatement was a SELECT statement, some databases may return the number of rows returned by that statement. However, this behaviour is not guaranteed for all databases and should not be relied on for portable applications.
	*/
	return $cursor->rowCount();
}


function mssql_fetch_array($cursor,$opt = MSSQL_ASSOC) {
	if ($opt == MSSQL_ASSOC) {
		$newopt = PDO::FETCH_ASSOC;
	} elseif ($opt == MSSQL_NUM) {
		$newopt = PDO::FETCH_NUM;
	} else {
		$newopt = PDO::FETCH_BOTH;
	}
	return $cursor->fetch($newopt);
}

function mssql_num_fields($cursor) {
	return $cursor->columnCount(); 
}

/*
function mssql_fetch_field($cursor,$n) {
	return $cursor->fetchColumn($n);
}
*/

function mssql_column_meta($cursor,$n) {
	return $cursor->getColumnMeta($n);
}

function mssql_guid_string($binguid) {
	if (empty($binguid)) return null;
	$u = unpack('Va/v2b/n2c/Nd', $binguid);
	return sprintf('%08X-%04X-%04X-%04X-%04X%08X', $u['a'], $u['b1'], $u['b2'], $u['c1'], $u['c2'], $u['d']);
}