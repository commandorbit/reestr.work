﻿/*

uses: 

jquery 2
jquery ui 
jquery ui autocomplete
jquery ui dialogbox

*/

staff_user_complete.path = '/modules/staff_user_complete/1.0';
staff_user_complete.show_log = true;

function staff_user_complete(input_fio,input_id) {
	var PATH = staff_user_complete.path;
	var SHOW_LOG = staff_user_complete.show_log;
	input_fio.autocomplete({
		autoFocus: true,
		minLength: 1,
		source: PATH+'/ajax.php?func=options',
		select: function(event,ui) {
			$.ajax({
  				url: PATH+'/ajax.php?func=option_select&term='+ui.item.value,
  				context: document.body
			}).done(function(data) {
				var data = JSON.parse(data);
				if (data.user_id) {
					input_id.val(data.user_id);
					if (SHOW_LOG) console.log('id=',data.user_id);
				} else {
					if (data.options) {
						if (SHOW_LOG) console.log(data.options);
						$('#staff_user_dialog').remove();
						$('body').append('<div id="staff_user_dialog"></div>');
						for (i in data.options) {
							$('#staff_user_dialog').append('<input type="radio" name="staff_user_radio" value="'+ data.options[i].user_id+'">');
							$('#staff_user_dialog').append(' - '+data.options[i].fio);
							$('#staff_user_dialog').append(' '+data.options[i].dr+'<br><br>');
						}	
						$('#staff_user_dialog').dialog({
							title: 'Выберите пользователя',
							closeOnEscape: true,
							width: 500,
							height: 180,
							maxWidth: 500,
							maxHeight: 180,
							modal: true,
						});
					}
				}
			});
		}, 
		response: function(event, ui) {
			if (ui.content.length === 0) {
				input_id.val('');
				if (SHOW_LOG) console.log('id removed');
			}
		}
	});
	$('body').on('change','input[name="staff_user_radio"]',function(){
		if ($('input[name="staff_user_radio"]')) {
			var user_id = $('input[name="staff_user_radio"]:checked').val();
			if (user_id) {
				input_id.val(user_id);
				if (SHOW_LOG) console.log('id=',user_id);
			}
		}
	});
}


