<?php
require_once('/home/dekanat/modules/sql.php');
sql_connect('localhost','staff_ro','NEBUn2ReSe','staff');
sql_set_charset('utf8');
header('Access-Control-Allow-Origin: *');

$term = array_key_exists('term',$_REQUEST) ? sql_escape($_REQUEST['term']) : '';
$func = array_key_exists('func',$_REQUEST) ? sql_escape($_REQUEST['func']) : '';
$funcs = ['options','option_select'];

if ($func == 'options') {
	$sql = "SELECT DISTINCT fio, fio  
		FROM `staff`.v_user 
		WHERE fio LIKE '$term%' 
		ORDER BY fio ";
	$json = [];
	$res = sql_query($sql);
	while($row = sql_array($res)) {
		$json[] = $row['fio'];
	}
	echo json_encode($json);
} elseif ($func == 'option_select') {
	$fio = iconv("cp1251","utf-8",$term);
	// 1251
	$sql = "SELECT user_id, dr, fio, dept_name, post_name, is_actual 
		FROM `staff`.v_user  
		WHERE fio='$fio' ";
	$res = sql_query($sql);
	if (!sql_count_rows($res)) {
		// utf-8 
		$sql = "SELECT user_id, dr, fio, is_actual 
			FROM `staff`.v_user  
			WHERE fio='$term' ";
		$res = sql_query($sql);
	}
	$json = [];
	if (sql_count_rows($res)==1) {
		$row = sql_array($res);
		$json['user_id'] = $row['user_id'];
		$json['options'] = null;
	} else {
		$json['user_id'] = null;
		$json['options'] = [];
		while ($row = sql_array($res)) {
			$dr = sql_date2rus($row['dr']);
			$photo_file = 'photo/'.$row['user_id'].'.jpg';
			$photo_link = (file_exists('/home/staff/'.$photo_file)) ? '//staff.unecon.ru/'.$photo_file : '';
			$json['options'][] = [
				'user_id'=>$row['user_id'],
				'fio'=>$row['fio'],
				'dr'=>$dr,
				'dept'=>$row['dept_name'],
				'post'=>$row['post_name'],
				'is_actual'=>$row['is_actual'],
				'photo_link'=>$photo_link
			];
		}
	}
	echo json_encode($json);
}

?>