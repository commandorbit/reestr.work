﻿/*

uses: 

jquery 2
jquery ui 
jquery ui autocomplete
jquery ui dialogbox

version 2.0
- added department
- added post

*/

staff_user_complete.version = '2.0';
staff_user_complete.path = '/modules/staff_user_complete/'+staff_user_complete.version;
staff_user_complete.show_log = true;

function staff_user_complete(input_fio,input_id) {
	var PATH = staff_user_complete.path;
	var SHOW_LOG = staff_user_complete.show_log;

	input_fio.autocomplete({
		autoFocus: true,
		minLength: 1,
		source: PATH+'/ajax.php?func=options',
		select: function(event,ui) {
			$.ajax({
  				url: PATH+'/ajax.php?func=option_select&term='+ui.item.value,
  				context: document.body
			}).done(function(data) {
				var data = JSON.parse(data);
				if (data.user_id) {
					input_id.val(data.user_id);
					if (SHOW_LOG) console.log('id=',data.user_id);
				} else {
					if (data.options) {
						if (SHOW_LOG) console.log(data.options);
						$('#staff_user_dialog').remove();
						$('body').append('<div id="staff_user_dialog" style="font-size: 13px;"></div>');
						for (i in data.options) {
							var border = (i > 0) ? ' border-top: 1px dotted #ccc;' : '';
							var html = '<div style="position: relative; min-height: 90px; padding-top: 8px; '+border+'">';
							html += '<input type="radio" name="staff_user_radio" value="'+ data.options[i].user_id+'"> - ';
							html += '<div style="position: absolute; top: 10px; left: 28px; width: 370px;">';
							html += data.options[i].fio+'<br>';
							if (data.options[i].dr) html += 'Дата рождения: '+data.options[i].dr+'<br>';
							if (data.options[i].dept) html += data.options[i].dept+'<br>';
							if (data.options[i].post) html += data.options[i].post+'<br>';
							html += '</div>';
							if (data.options[i].photo_link) {
								var img = '<img src="'+data.options[i].photo_link+'" alt="" width="60">';
								html += '<div style="position: absolute; top: 10px; right: 0; width: 70px;">'+img+'</div>';
							}
							html += '</div>';
							$('#staff_user_dialog').append(html);
						}	
						$('#staff_user_dialog').dialog({
							title: 'Выберите пользователя',
							closeOnEscape: true,
							minWidth: 500,
							minHeight: 200,
							maxWidth: 500,
							modal: true,
						});
					}
				}
			});
		}, 
		response: function(event, ui) {
			if (ui.content.length === 0) {
				input_id.val('');
				if (SHOW_LOG) console.log('id removed');
			}
		}
	});

	input_fio.change(function(){
		if ($(this).val()=='') {
			input_id.val('');
			if (SHOW_LOG) console.log('id removed');
		}
	});

	$('body').on('change','input[name="staff_user_radio"]',function(){
		if ($('input[name="staff_user_radio"]')) {
			var user_id = $('input[name="staff_user_radio"]:checked').val();
			if (user_id) {
				input_id.val(user_id);
				if (SHOW_LOG) console.log('id=',user_id);
			}
		}
	});
}


