<?php
function print_options($option_array,$selected_val,$need_return = false) {
	$return = '';
	foreach ($option_array as $key=>$value) {
		// if Added by chernov
		if (is_array($selected_val))
			$selected = in_array((string)$key,$selected_val) ? 'selected' : '';
		else
			$selected = ((string)$key == (string)$selected_val) ? 'selected' : '';
		if (trim($value)=='') $value='&nbsp;'; 
		$option = '<option ' . $selected . ' value="' . html_special_chars($key) . '">' . $value . "</option>\n";
		if ($need_return) $return .= $option;
		else echo $option;
	}
	if ($need_return) return $return;
}

function _array_merge ($a, $b) {
	$c=array();
	foreach ($a as $k =>$v) {
		$c[$k]=$v;
	}
	foreach ($b as $k =>$v) {
		$c[$k]=$v;
	}
	return $c;
}

function array_add_val($a,$val,$val_name) {
	return _array_merge (array($val=>$val_name), $a);
}

function array_to_assoc($a) {
	$b = array();
	foreach ($a as $el) $b[$el] = $el;
	return $b;
}

/* ��������� � ������ ������������� ������ ������ �������� �� ������� */

function array_add_null($a) {
	return _array_merge (array('null'=>'���'), $a);
}

function array_add_null_2($a) {
	return _array_merge (array('null'=>''), $a);
}

function array_add_empty($a) {
	return _array_merge (array(''=>''), $a);
}

function array_add_all($a) {
	return _array_merge (array('all'=>'���'), $a);
}

function array_add_both($a) {
	$b = array_add_null($a);
	$c = array_add_all($b);
	return $c;
}

function array_null() {
	return array('null'=>'&nbsp;');
}

function array_kurs() {
	$a = array();
	for($i=1; $i<7; $i++) {
		$a[$i] = $i;
	}
	return $a;
}

// ���������� 

function fakultet_array($name = 'fakultet_name') {
	$sql = "
	SELECT f.fakultet_kod, f.fakultet_option_list_name 
		FROM `dekanat`.fakultet f 
	WHERE is_parent=0 
		ORDER BY f.is_actual desc, f.fakultet_option_list_name 
	";
	return sql_to_assoc($sql);
}

function fakultet_socr_array() {
	return fakultet_array('fakultet_socr_name');
}

function user_fakultet_array() {
	$user_may_fakultets = user_fakultet_list(current_user_kod());
	$fakultets = fakultet_array();
	foreach ($fakultets as $fakultet_kod=>$fakultet_name) {
		if (!in_array($fakultet_kod,$user_may_fakultets)) unset($fakultets[$fakultet_kod]);
	}
	return $fakultets;
}

function user_fakultet_socr_array() {
	$user_may_fakultets = user_fakultet_list(current_user_kod());
	$fakultets = fakultet_socr_array();
	foreach ($fakultets as $fakultet_kod=>$fakultet_name) {
		if (!in_array($fakultet_kod,$user_may_fakultets)) unset($fakultets[$fakultet_kod]);
	}
	return $fakultets;
}

// ������ � �������� �������

function user_up_array() {
	$user_fakultet_list_sql = user_fakultet_list_sql(current_user_kod());
	return sql_to_assoc("SELECT uch_plan_kod, uch_plan_name FROM uch_plan WHERE fak_kod IN ($user_fakultet_list_sql) ORDER BY uch_plan_name");
}

function user_up_year_fakultet_array($year,$fakultet_kod,$show_parents = false) {
	$user_fakultet_list_sql = user_fakultet_list_sql(current_user_kod());
	$w_show_in_user_forms = ($show_parents) ? "" : " AND show_in_user_forms=1 ";
	return sql_to_assoc("SELECT uch_plan_kod, uch_plan_name FROM uch_plan 
		WHERE fak_kod IN ($user_fakultet_list_sql) $w_show_in_user_forms
		AND year='$year' AND fak_kod='$fakultet_kod' ORDER BY uch_plan_name");
}






function bakalavr_type_array() {
	return sql_to_assoc("SELECT bakalavr_type_kod, bakalavr_type_name FROM s_bakalavr_type ORDER BY bakalavr_type_kod ");
}

function control_form_array() {
	return sql_to_assoc("SELECT control_form_kod, control_form_socr_name FROM s_control_form ORDER BY control_form_kod ");
}

function da_net_array() {
	return sql_to_assoc("SELECT id, name FROM `dekanat`.s_da_net ORDER BY id ");
}

function null_not_null_array() {
	$a = array("null"=>"�����","not_null"=>"�� �����");
	return $a;
}

function diplom_regbook_array($is_actual = null, $fakultet_kod = null, $is_dublicat = null, $is_pril_dublicat = null) {
	$w = '';
	if (is_numeric($is_actual)) $w .= " and is_actual='$is_actual' ";
	if (is_numeric($fakultet_kod)) $w .= " and (fak_kod='$fakultet_kod' or fak_kod=0) ";
	if (is_numeric($is_dublicat)) $w .= " and is_dublicat='$is_dublicat' ";
	if (is_numeric($is_pril_dublicat)) $w .= " and is_pril_dublicat='$is_pril_dublicat' ";
	return sql_to_assoc("SELECT `index`, `index` FROM s_diplom_regbook 
		WHERE true and is_academ_sprav_regbook=0 $w ORDER BY `index` ");
}

function academ_sprav_regbook_array($is_actual = '', $fakultet_kod = '') {
	$w = '';
	if (is_numeric($is_actual)) $w .= " and is_actual='$is_actual' ";
	if (is_numeric($fakultet_kod)) $w .= " and (fak_kod='$fakultet_kod' or fak_kod=0) ";
	return sql_to_assoc("SELECT `index`, `index` FROM s_diplom_regbook 
		WHERE true and is_academ_sprav_regbook=1 $w ORDER BY `index` ");
}

function gragd_array() {
	return sql_to_assoc("SELECT gragd_kod, gragd_name FROM gragd ORDER BY gr, gragd_name");
}

function filial_array() {
	return sql_to_assoc("SELECT filial_kod, filial_name FROM s_filial ORDER BY filial_name");
}

function hotel_array() {
	return sql_to_assoc("SELECT hotel_kod, hotel_name FROM s_hotel ORDER BY hotel_name ");
}

function hotel_array_actual() {
	return sql_to_assoc("SELECT hotel_kod, hotel_name FROM s_hotel WHERE has_priem=1 ORDER BY hotel_name ");
}

function hotel_room_array($hotel_kod = '') {
	$w = (is_numeric($hotel_kod)) ? " AND hotel_kod='$hotel_kod' " : "";
	return sql_to_assoc("SELECT room_kod, room_name FROM s_hotel_room WHERE true $w ORDER BY floor, ord, flat, room, room_name ");
}

function hotel_price_array($params = array()) {
	foreach ($params as $param=>$value) $$param = $value;
	$w = " AND is_actual=1 ";
	if (isset($hotel_kod) && is_numeric($hotel_kod) && $hotel_kod>0) $w .= " AND hotel_kod='$hotel_kod'";
	if (isset($room_kod) && is_numeric($room_kod) && $room_kod>0)  {
		if ($hotel_kod == 6) {
			// ������������ - ������ � ������� ����� � �������� (s_hotel_room)
			$w .= " AND price_kod IN (SELECT price_kod FROM s_hotel_room WHERE room_kod='$room_kod') ";
		} else {
			// ������ ��������� - ������ � ������� ����� � ��������� (price)
			$w .= " AND room_kod='$room_kod' ";
		}
	}
	if (isset($ob_osnova_kod) && is_numeric($ob_osnova_kod) && $ob_osnova_kod>0) {
		$w .= " AND (ob_osnova_kod='$ob_osnova_kod' OR ob_osnova_kod is null) ";
	}
	if (isset($hotel_lgota_type_kod) && is_numeric($hotel_lgota_type_kod)) {
		$hotel_lgota_type = sql_get_array("s_hotel_lgota_type","hotel_lgota_type_kod='$hotel_lgota_type_kod'");
		// � ������ � ������� where ���������� ����������!
		if ($hotel_lgota_type['is_sirota']) $w = " AND price_kod='".$hotel_lgota_type['price_kod']."' ";
		else $w .= " AND (is_lgota is null OR is_lgota='".$hotel_lgota_type['is_lgota']."')";
	}

	return sql_to_assoc("SELECT price_kod, price_name 
		FROM `dogovor`.price 
		WHERE true $w 
		ORDER BY ob_osnova_kod desc, is_lgota desc, price_name ");
}

function inostr_lang_array() {
	return sql_to_assoc("SELECT DISTINCT lang_kod, lang_name FROM s_lang ORDER BY lang_name ");
}

function is_inter_array() {
	return array("all"=>"���","1"=>"������ ������������","0"=>"����� �������������");
}

function kafedra_array_actual() {
	return kafedra_array($is_actual = 1);
}

function kafedra_array($is_actual = '') {
	$w_is_actual = (is_numeric($is_actual)) ? " AND is_actual=$is_actual " : '';
	return sql_to_assoc("SELECT kafedra_kod, kafedra_name 
		FROM kafedra 
		WHERE is_kafedra=1 $w_is_actual
		ORDER BY kafedra_name ");
}

function kudapost_array() {
	return sql_to_assoc("SELECT kudapost_kod, kudapost_name FROM s_kudapost 
		WHERE kudapost_kod NOT IN (6) ORDER BY kudapost_kod ");
}

function kudapost_array_all() {
	return sql_to_assoc("SELECT kudapost_kod, kudapost_name FROM s_kudapost ORDER BY kudapost_kod ");
}

function kurs_array() {
	$a = array();
	for ($i=1; $i<7; $i++) {
		$a[$i] = $i;
	}
	return $a;
}

function spec_array($kudapost_kod = '') {
	// ������ ������ ��������������
	$w = (is_numeric($kudapost_kod)) ? " and s.kudapost_kod='$kudapost_kod' " : "";
	return sql_to_assoc("SELECT s.grp_spec_kod, CONCAT(k.kudapost_name, ' - ', s.grp_spec_name_full) as spec 
	FROM grp_spec s
	INNER JOIN s_kudapost k ON s.kudapost_kod=k.kudapost_kod 
	WHERE s.parent_kod is null AND s.is_actual=1 $w 
	ORDER BY k.kudapost_name, s.grp_spec_name, s.grp_spec_name_full ");
}

function profile_array($spec_kod = '') {
	// ������ ������ ��������. ���� ������� ��� ������������� - ������� ������ ������ �������������
	$w = (is_numeric($spec_kod)) ? " and s.parent_kod='$spec_kod' " : "";
 	return sql_to_assoc("SELECT s.grp_spec_kod, CONCAT(k.kudapost_name, ' - ', s.grp_spec_name_full) as spec 
	FROM grp_spec s
	INNER JOIN s_kudapost k ON s.kudapost_kod=k.kudapost_kod
	WHERE s.parent_kod is not null AND s.is_actual=1 $w
	ORDER BY k.kudapost_name, s.grp_spec_name, s.grp_spec_name_full ");
}

function ob_forma_array() {
	return sql_to_assoc("SELECT ob_forma_kod, ob_forma_name FROM ob_forma ORDER BY ord");
}

function ob_forma_actual_array() {
	return sql_to_assoc("SELECT ob_forma_kod, ob_forma_name FROM ob_forma WHERE is_actual=1 ORDER BY ord");
}

function ob_osnova_array() {
	return sql_to_assoc("SELECT ob_osnova_kod, ob_osnova_name FROM s_ob_osnova ORDER BY ob_osnova_kod");
}

function obraz_array() {
	return sql_to_assoc("SELECT obraz_kod, obraz_name FROM s_obraz WHERE ord is not null ORDER BY ord");
}

function ocenka_array($control_form_kod = 'all', $add_ne_sdaet = false) {
	$w = "";
	if (is_numeric($control_form_kod)) {
		if ($control_form_kod == 1) $w = ' AND exam=1';
		elseif ($control_form_kod == 2 || $control_form_kod == 8) $w = ' AND zach=1';
		elseif ($control_form_kod == 3) $w = ' AND diff_zach=1';
		elseif ($control_form_kod == 4 || $control_form_kod == 7) $w = ' AND exam=1';
		elseif ($control_form_kod == 6) $w = ' AND exam=1';
	}
	if (!$add_ne_sdaet) $w .= " AND ocenka_kod<>1 ";
	$s = "SELECT ocenka_kod, ocenka_socr_name FROM s_ocenka WHERE true $w ORDER BY ocenka_kod ";
	return sql_to_assoc($s);
}

function org_array() {
	return sql_to_assoc("SELECT org_kod, org_name FROM s_org ORDER BY org_kod ");
}

function osn_zach_array() {
	return sql_to_assoc("SELECT osn_zach_kod, osn_zach_name FROM s_osn_zach WHERE not_used=0 ORDER BY osn_zach_name ");
}

function pasp_type_array() {
	return sql_to_assoc("SELECT pasp_type_kod, pasp_type_name FROM s_pasp_type ORDER BY pasp_type_kod");
}

function peregon_type_array() {
	return sql_to_assoc("SELECT peregon_type_kod, peregon_type_name FROM s_peregon_type ORDER BY peregon_type_kod");
}

function vuz_rename_array() {
	return sql_to_assoc("SELECT peregon_type_kod, IF (peregon_type_kod=0, '�����' ,peregon_type_name) as name 
		FROM s_peregon_type 
		WHERE peregon_type_kod NOT IN (3) ORDER BY peregon_type_kod");
}

function pol_array() {
	return sql_to_assoc("SELECT pol_kod, pol_name FROM s_pol ORDER BY pol_kod");
}

function predmet_array() {
	return sql_to_assoc("SELECT predmet_kod, predmet_name FROM predmet ORDER BY predmet_name");
}

function predmet_kafedra_array($predmet_kod) {
	return sql_to_assoc("select distinct pk.kafedra_kod, k.kafedra_name 
		from predmet_kafedra pk 
		inner join kafedra k on pk.kafedra_kod=k.kafedra_kod 
		where pk.predmet_kod='$predmet_kod' and k.is_actual=1
		order by k.kafedra_name ");
}

function prepod_array() {
	return sql_to_assoc("select prepod_kod, concat(prepod_fam, ' ', prepod_im, ' ', ifnull(prepod_otc, '')) as prepod_fio from prepod order by prepod_fio");
}

function prepod_array_aspir() {
	return sql_to_assoc("SELECT DISTINCT ds.prepod_kod, 
		concat(prep.prepod_fam, ' ', prep.prepod_im, ' ', ifnull(prep.prepod_otc, '')) as prepod_fio 
	FROM decanat_stud ds 
	INNER JOIN prepod prep ON ds.prepod_kod=prep.prepod_kod 
	ORDER BY prepod_fio ");
}

function prichina_array() {
	return sql_to_assoc(" SELECT prichina_kod, prichina_name FROM s_prichina ORDER BY prichina_name ");
}

function region_array() {
	return sql_to_assoc("SELECT reg_code, post_name FROM `kladr`.kladr_region ORDER BY reg_code");
}

function region_array_ordered() {
	return sql_to_assoc("SELECT reg_code, post_name FROM `kladr`.kladr_region ORDER BY ord, post_name ");
}

function status_array() {
	return sql_to_assoc("SELECT status_kod, status_name FROM s_student_status WHERE use_in_dekanat=1 ORDER BY status_name ");
}

function semestr_array($start = 1) {
	$a = array();
	for ($i=$start; $i<13; $i++) $a[$i] = $i;
	return $a;
}

function session_type_array() {
	return array('�������� ������','�������������� ������','����������');
}

function sps_array() {
	return array("all"=>"���","1"=>"� ������");
}

function sps_array_show() {
	return array("1"=>"��� ��������","2"=>"��� ��������","3"=>"�����");
}

function srok_array($kudapost_kod = '') {
	$w = (is_numeric($kudapost_kod)) ? " and kudapost_kod='$kudapost_kod' " : "";
	return sql_to_assoc("SELECT srok_kod, srok_name FROM s_srok WHERE true $w ORDER BY srok_name ");
}

function target_array() {
	return sql_to_assoc("SELECT target_kod, target_name FROM s_target WHERE not_used=0 ORDER BY target_name ");
}

function upp_cicl_array() {
	return sql_to_assoc("SELECT cicl_kod, cicl_name FROM s_upp_cicl ORDER BY cicl_name");
}

function upp_cicl_kudapost_pokolenie_array($kudapost_kod = '',$diplom_pokolenie = '',$is_actual = '') {
	$w = '';
	if ($kudapost_kod == 8) $kudapost_kod = 7; // ����������� � ������������
	if (is_numeric($kudapost_kod)) $w .= " and (kudapost_kod='$kudapost_kod' or kudapost_kod is null) ";
	if (is_numeric($diplom_pokolenie)) $w .= " and diplom_pokolenie='$diplom_pokolenie' ";
	if (is_numeric($is_actual)) $w .= " and is_actual='$is_actual' ";

	$cicles = sql_to_assoc("SELECT cicl_kod, cicl_name 
		FROM s_upp_cicl 
		WHERE true $w ORDER BY cicl_name");
	return $cicles;
}

function vuz_type_array() {
	return sql_to_assoc("SELECT vuz_type_kod, vuz_type_name FROM s_vuz_type ORDER BY vuz_type_name ");
}

function year_array() {
	return sql_to_assoc("SELECT DISTINCT year, year FROM uch_plan ORDER BY year");
}

function year_kurs_now_array() {
	$years = year_array();
	$cur_d = date("Y-m-d");
	$cur_y = date("Y");
	$uch_y_starts_at = $cur_y . "-07-15";
	$a = array();
	foreach ($years as $up_y) {
		// ������ �� �����
		$cur_kurs = ($cur_d > $uch_y_starts_at) ? $cur_y-$up_y+1 : $cur_y-$up_y;
		$a[$up_y] = $up_y . ' ('.$cur_kurs.' ����)';
	}

	if (defined('UTF-8')) {
		$a = win_utf($a);
	}

	return $a;
}

function zdanie_array() {
	return sql_to_assoc("SELECT zdanie_kod, zdanie_name FROM s_zdanie ORDER BY zdanie_name ");
}


// ���

function vus_voenkomat_array() {
	return sql_to_assoc("SELECT voenkomat_kod, name FROM s_vus_voenkomat ORDER BY ord desc,name");
}

function vus_sostav_array() {
	return sql_to_assoc("SELECT sostav_kod, name FROM s_vus_sostav ORDER BY name");
}

function vus_profile_array() {
	return sql_to_assoc("SELECT profile_kod, profile_name FROM s_vus_profile ORDER BY profile_name");
}

function vus_zvanie_array() {
	return sql_to_assoc("SELECT zvanie_kod, name FROM s_vus_zvanie ORDER BY name");
}

function vus_godnost_array() {
	return sql_to_assoc("SELECT godnost_kod, concat(name,' ',socr) AS fullname FROM s_vus_godnost order by fullname");
}

function vus_priz_vo_array() {
	return sql_to_assoc("SELECT priz_vo_kod, name FROM s_vus_priz_vo ORDER BY name");
}

function vus_uchet_array() {
	return sql_to_assoc("SELECT uchet_kod, name AS name FROM s_vus_uchet ORDER BY name");
}

function vus_spec_array() {
	return sql_to_assoc("SELECT vus_spec_kod, CONCAT(shifr, ' (', descr, ')') AS name FROM s_vus_spec ORDER BY shifr");
}

function vus_datapost_array() {
	return array_da_net();
}

// ������ � ���������

function prikaz_type_field_array() {
	return sql_to_assoc("SELECT DISTINCT t.prikaz_type_kod, prikaz_type_name FROM s_prikaz_type_field t
	INNER JOIN s_prikaz_type ON t.prikaz_type_kod=s_prikaz_type.prikaz_type_kod 
	ORDER BY prikaz_type_name ");
}

function prikaz_about_array($is_actual = 'all') {
	$w_is_actual = (is_numeric($is_actual)) ? " AND is_actual='$is_actual' " : "";
	return sql_to_assoc("SELECT about_kod, IF(is_actual=1, about_name, CONCAT('[',about_name,']')) AS name 
		FROM s_prikaz_about WHERE true $w_is_actual ORDER BY about_name");
}

function prikaz_osnovanie_array() {
	return sql_to_assoc("SELECT prikaz_osnovanie_kod, prikaz_osnovanie_name FROM s_prikaz_osnovanie ORDER BY prikaz_osnovanie_name");
}

function prikaz_podpis_array() {
	return sql_to_assoc("SELECT podpis_kod, podpis_fio FROM s_prikaz_podpis ORDER BY podpis_fio");
}

function prikaz_preambula_array() {
	return sql_to_assoc("SELECT preambula_kod, preambula_name FROM s_prikaz_preambula ORDER BY preambula_name");
}

function prikaz_finished_array(){
	return sql_to_assoc("SELECT is_finished, name FROM s_prikaz_finished ORDER BY is_finished ");
}

function prikaz_approve_array() {
	return sql_to_assoc("SELECT approver_kod, approver_name FROM s_prikaz_approver ORDER BY approver_name ");
}

// ������ � ��������� (������������)

function prikaz_nomer_array($year='',$fakultet_kod='') {
	$w = '';
	if (is_numeric($year)) $w .= " AND YEAR(pn.prikaz_data)='$year' ";
	elseif ($year == 'null') $w .= " AND pn.prikaz_data is null ";
	if ($fakultet_kod && is_numeric($fakultet_kod)) $w .= " AND ps.abitur_kod IN (SELECT ds.stud_kod FROM decanat_stud ds WHERE ds.fakultet_kod='$fakultet_kod') ";
	$sql = "SELECT DISTINCT pn.prikaz_nomer_kod, 
	IF (prikaz_nomer is not null, 
		CONCAT (pn.prikaz_nomer, ' �� ', SUBSTR(pn.prikaz_data,9,2), '.', SUBSTR(pn.prikaz_data,6,2), '.', SUBSTR(pn.prikaz_data,1,4) ,' �.'), 
		pn.project_name 
	) as prikaz 
	FROM prikaz_nomer pn
	INNER JOIN prikaz_sostav ps ON pn.prikaz_nomer_kod=ps.prikaz_nomer_kod 
	WHERE true $w
	ORDER BY prikaz_data desc, prikaz_nomer desc ";
	return sql_to_assoc($sql);
}

// ����� ������� ������

function uch_plan_array($w = '') {
	$s = "SELECT up.uch_plan_kod, CONCAT(f.fakultet_socr_name, ' - ', up.uch_plan_name) AS up_name  
	FROM uch_plan up
	INNER JOIN fakultet f ON up.fak_kod=f.fakultet_kod 
	WHERE true $w ORDER BY up_name ";
	return sql_to_assoc($s);
}

function up_array($fakultet_kod_int_or_array,$year,$is_admin = false,$has_org = false,$ob_forma_kod = 'all') {
	/*	���� is_admin == false  - �� ���������� ������ "���������" �����
		���� is_admin == true  - ���������� ��� (��� �������)
	*/
	$w = '';
	// ������� ���� ��������� ���������� ���� ���� ������� + 1 ����� ������ �������
	if ($year == 'vipusk') $w .= " AND DATE_ADD(IF(up.d_finish='0000-00-00','2100-06-30',up.d_finish), INTERVAL 6 MONTH) < CURDATE()  ";
	elseif (is_numeric($year)) $w .= " AND up.year=$year AND DATE_ADD(IF(up.d_finish='0000-00-00','2100-06-30',up.d_finish), INTERVAL 6 MONTH) > CURDATE() ";
	if (is_numeric($fakultet_kod_int_or_array) and $fakultet_kod_int_or_array > 0) $w .= " AND up.fak_kod=$fakultet_kod_int_or_array ";
	elseif (is_array($fakultet_kod_int_or_array) && count($fakultet_kod_int_or_array)>0) $w .= " AND up.fak_kod IN (".implode(",",$fakultet_kod_int_or_array).") ";
	if (is_numeric($ob_forma_kod)) $w .= " AND up.ob_forma_kod='$ob_forma_kod' ";
	if ($has_org) {
		$user_info = current_user_info();
		$user_org_kod = $user_info['org_kod'];
		// ���� ����������� �� ����������� �����
		if ($user_org_kod > 1) $w .= " AND up.org_kod='$user_org_kod' ";
	}
	$a = uch_plan_array($w);
	if ($is_admin) {
		$b = $a;
	} else {
		$b = array();
		foreach ($a as $up_kod => $up_name) {
			if (sql_get_value("show_in_user_forms","uch_plan","uch_plan_kod='$up_kod'")==1) $b[$up_kod] = $up_name;
		}
	}
	return $b;
}

// ����� ��������� 

function upp_array($up,$semestr) {
	$upps = up_upps($up,$semestr);
	$a = array();
	foreach ($upps as $upp_kod) {
		$a[$upp_kod] = upp_name_concat($upp_kod,$semestr);
	}
	return $a;
}

function upp_kursovik_array($up,$semestr) {
	$upps = up_upps($up,$semestr);
	$a = array();
	foreach ($upps as $upp_kod) {
		if (upp_has_tema($upp_kod)) $a[$upp_kod] = upp_name_concat($upp_kod,$semestr);
	}
	return $a;
}
/*
function upp_inostr_array($up,$semestr) {
	$upps = up_upps($up,$semestr);
	$a = array();
	foreach ($upps as $upp_kod) {
		if (upp_is_inostr($upp_kod)) $a[$upp_kod] = upp_name_concat($upp_kod,$semestr);
	}
	return $a;
}
*/

function predmet_up_array($up) {
	if (!is_numeric($up)) return array();
	else return sql_to_assoc("SELECT DISTINCT p.predmet_kod, p.predmet_name 
	FROM uch_plan_punkt upp
	INNER JOIN predmet p ON upp.predm_kod=p.predmet_kod 
	WHERE upp.uch_plan_kod='$up' OR upp.uch_plan_kod IN (select parent_up_kod from uch_plan where uch_plan_kod='$up' ) 
	ORDER BY p.predmet_name");
}

// ������ � ��������

function grp_array($fakultet_kod='',$show_all=false) {
	
	$w = (is_numeric($fakultet_kod) and $fakultet_kod > 0) ? " AND t.fakultet_kod=$fakultet_kod " : "";
	if (!$show_all) $w .= "  AND t.is_actual=1 AND t.is_vipusk=0 ";
	return sql_to_assoc("SELECT t.grp_kod, 
	IF((SELECT count(t2.grp_kod) FROM decanat_fakultet_grp t2 WHERE t2.grp_nomer=t.grp_nomer)>1, 
		CONCAT(t.grp_nomer, ' (', (SELECT up.uch_plan_name FROM uch_plan up WHERE up.uch_plan_kod=t.uch_plan_kod), ')' ), t.grp_nomer) 
	FROM decanat_fakultet_grp t 
	WHERE true $w
	ORDER BY t.grp_nomer");
}

function grp_depend_array($kurs,$fakultet_kod,$ob_forma_kod) {
	$w = '';
	if (is_array($kurs) && $kurs = array_numeric_keys($kurs)) $w.= " AND t.kurs IN (" . implode(",",$kurs) . ")";
	elseif (is_numeric($kurs)) $w .= " AND t.kurs=$kurs ";
	
	if (is_array($fakultet_kod) && $fakultet_kod = array_numeric_keys($fakultet_kod)) $w.= " AND t.fakultet_kod IN (" . implode(",",$fakultet_kod) . ")";
	elseif (is_numeric($fakultet_kod) && $fakultet_kod > 0) $w .= " AND t.fakultet_kod=$fakultet_kod ";
	
	if (is_array($ob_forma_kod) && $ob_forma_kod = array_numeric_keys($ob_forma_kod)) $w.= " AND up.ob_forma_kod IN (" . implode(",",$ob_forma_kod) . ")";
	elseif (is_numeric($ob_forma_kod)) $w .= " AND up.ob_forma_kod=$ob_forma_kod ";
	
	$s = "SELECT t.grp_kod, 
	IF ( (SELECT count(t2.grp_kod) FROM decanat_fakultet_grp t2 WHERE t2.grp_nomer=t.grp_nomer)>1, 
		IF (t.uch_plan_kod, CONCAT(t.grp_nomer, ' (', (SELECT up.uch_plan_name FROM uch_plan up WHERE up.uch_plan_kod=t.uch_plan_kod), ')' ), t.grp_nomer),  
		t.grp_nomer
	) 
	FROM decanat_fakultet_grp t 
	INNER JOIN uch_plan up ON t.uch_plan_kod=up.uch_plan_kod 
	WHERE t.is_actual=1 $w
	ORDER BY t.grp_nomer";
	return sql_to_assoc($s);
}

function grp_depend_up_array($fakultet_kod_int_or_array,$up_kod) {
	$w = '';
	if (is_numeric($fakultet_kod_int_or_array) and $fakultet_kod_int_or_array > 0) $w .= " AND fakultet_kod=$fakultet_kod_int_or_array ";
	elseif (is_array($fakultet_kod_int_or_array) && count($fakultet_kod_int_or_array) >0 ) $w .= " AND fakultet_kod IN (".implode(",",$fakultet_kod_int_or_array).")"; 
	if (is_numeric($up_kod)) $w .= " AND uch_plan_kod=$up_kod ";
	return sql_to_assoc("SELECT grp_kod, grp_nomer FROM decanat_fakultet_grp WHERE is_actual=1 $w ORDER BY grp_nomer ");
}

function grp_stud_array($stud_kod) {
	/* ����� ��� ��������, � ����� �� ����� ���� �������, ������ �� ���������� */
	$grp_kod = stud_grp_kod($stud_kod);
	$fact_grp_kod = stud_grp_kod($stud_kod);

	return sql_to_assoc("SELECT g.grp_kod, 
		IF((SELECT count(t2.grp_kod) FROM decanat_fakultet_grp t2 WHERE t2.grp_nomer=g.grp_nomer)>1, 
			CONCAT(g.grp_nomer, ' (', (SELECT up.uch_plan_name FROM uch_plan up WHERE up.uch_plan_kod=g.uch_plan_kod), ')' ), g.grp_nomer) 
	FROM decanat_fakultet_grp g
	INNER JOIN decanat_stud s ON g.fakultet_kod=s.fakultet_kod 
	WHERE s.stud_kod='$stud_kod'
		AND (g.is_actual=1 OR g.grp_kod IN ('$grp_kod','$fact_grp_kod') )
	ORDER BY g.grp_nomer");
}

function grp_inostr_array($up_kod,$predmet_kod,$show_all = true) {
	$up_parent_kod = up_parent_kod($up_kod);
	if ($show_all) {
		$s = "SELECT grp_inostr_kod, grp_inostr_name 
		FROM grp_inostr 
		WHERE predm_kod=$predmet_kod AND up_kod IN ($up_kod,$up_parent_kod) ORDER BY grp_inostr_name ";
	} else {
		$s = "SELECT DISTINCT g.grp_inostr_kod, g.grp_inostr_name FROM grp_inostr g
		INNER JOIN stud_grp_inostr s ON g.grp_inostr_kod=s.grp_inostr_kod  
		WHERE g.predm_kod=$predmet_kod AND g.up_kod IN ($up_kod,$up_parent_kod)
		AND s.stud_kod IN (SELECT ds.stud_kod FROM decanat_stud ds WHERE ds.status_kod IN (1,997,14) ) 
		ORDER BY grp_inostr_name ";
	}
	return sql_to_assoc($s);
}

// ���� (���) ��� ������ ������

function year_kurs_array($is_admin = false) {
	$s = "SELECT DISTINCT year FROM uch_plan ORDER BY year";
	$date_now = date("Y-m-d");
	$year_now = date("Y");
	
	$res = sql_query($s);
	$a = array();
	while ($row = sql_array($res)) {
		$kurs_now = $year_now - $row['year'];
		if ($date_now > ($year_now . "-07-15")) {
			$kurs_now++;
		}
		if ($kurs_now < 8) $a[$row['year']] = $row['year'] . ' (������ �� ' . $kurs_now . ' �����)';
	}
	if ($is_admin) {
		$a['vipusk'] = '���������� �����';
	}

	if (defined('UTF-8')) {
		$a = win_utf($a);
	}

	return $a;
}

?>