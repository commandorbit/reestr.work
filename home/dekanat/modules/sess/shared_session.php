<?php

Class Unecon_Shared_Session {
    const STAFF_cookie='_STAFF_UNECON';
    
    public static function start() {
        if (self::cookie_is_valid()) {
            $cookie = $_COOKIE[self::STAFF_cookie];
            self::load_session($cookie);
            session_start();
            /*
            баг с Головановым и Васильевой
            Гриша вероятно иногда почему-то не может выполнить session_id() от нашего id записанного в базу !
            из-за этого пользователь нажимает войти а его выкидывает 
            (т.к. в запущенной сессии НИЧЕГО НЕТ!!! user_kod-а и тому подобного!!!)
            почему ничего нет не понимаю, но id новой, запущенной сессии уже другой! не такой как наш_id_записанный_в_базу!!! 
            не такой с каким была запущена команда на session_id!!!
            
            следующий код включил пока понаблюдать что происходит, и много ли таких проблем
            */
            $saved = self::cookie_sesson_id($cookie);
            $loaded = session_id();
            if ($saved != $loaded) {
                $mysqli = auth_db_connect();
                $cookie = $mysqli->real_escape_string($cookie);
                $ip = isset($_SERVER['REMOTE_ADDR']) ? "'".$_SERVER['REMOTE_ADDR']."'" : 'null';
                $ip_x_forw = isset($_SERVER['HTTP_X_FORWARDED_FOR']) ? "'".$_SERVER['HTTP_X_FORWARDED_FOR']."'" : 'null';
                $mysqli->query("insert into error_log (saved_session,loaded_session,cookie,ip,ip_x_forw) 
                    VALUES('$saved','$loaded','$cookie',$ip,$ip_x_forw)") or die("error save! ERROR:" . $mysqli->error);
                $mysqli->close();
            }

            /* конец */
        } else {
            self::new_session();
        }
    }

    private static function new_session() {
        $val = self::RandomString();
        setcookie(self::STAFF_cookie,$val, time()+60*60*24*30, '/','.unecon.ru');
        session_start();
        self::save_session($val);
    }
    
    private static function cookie_is_valid() {
        $valid = false;
        if (isset($_COOKIE[self::STAFF_cookie])) {
            $cookie = $_COOKIE[self::STAFF_cookie];
            if (is_string($cookie) && self::cookie_sesson_id($cookie)) {
                $valid = true;
            }
        }

        return $valid;
    }

    private static function RandomString() {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $randstring = '';
        for ($i = 0; $i < 20; $i++) {
            $randstring .= $characters[rand(0, strlen($characters)-1)];
        }
        return $randstring;
    }

    private static function cookie_sesson_id($cookie) {
        $mysqli = auth_db_connect();
        $cookie = $mysqli->real_escape_string($cookie);
        $res = $mysqli->query("select * from  auth where cookie='$cookie'") or die("error load! ERROR:" . $mysqli->error);
        $row = $res->fetch_assoc();
        $mysqli->close();

        $current_ip = $_SERVER['REMOTE_ADDR'];
        $current_ip_x_forw = array_key_exists('HTTP_X_FORWARDED_FOR',$_SERVER) ? $_SERVER['HTTP_X_FORWARDED_FOR'] : '';
        if ($row['ip']==$current_ip && $row['ip_x_forw']==$current_ip_x_forw) {
            return $row['session'];
        }
    }

    public static function load_session($cookie) {
        session_id(self::cookie_sesson_id($cookie));
    } 

    public static function save_session($cookie) {
        $mysqli = auth_db_connect();
        $cookie = $mysqli->real_escape_string($cookie);
        $session = session_id();
        $ip = isset($_SERVER['REMOTE_ADDR']) ? "'".$_SERVER['REMOTE_ADDR']."'" : 'null';
        $ip_x_forw = isset($_SERVER['HTTP_X_FORWARDED_FOR']) ? "'".$_SERVER['HTTP_X_FORWARDED_FOR']."'" : 'null';
        $mysqli->query("insert into auth (cookie,session,ip,ip_x_forw) VALUES('$cookie','$session',$ip,$ip_x_forw)") or die("error save! ERROR:" . $mysqli->error);
        $mysqli->close();
    }
    
    public static function kill_session() {
        setcookie(self::STAFF_cookie, "", time()-3600,'/','.unecon.ru');
        if (session_status() != PHP_SESSION_NONE) session_destroy();
    }
    
    
}
?>