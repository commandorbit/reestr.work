<?php
error_reporting(E_ALL | E_STRICT);
if (!defined('STAFF_cookie')) define('STAFF_cookie','_STAFF_UNECON');

require_once 'auth_db_connect.php';

function start_session_dekanat() {
	session_start();
	if (!isset($_SESSION['dek_user_kod'])) {
	    if ($staff_user_id = session_staff_user_id()) {
	    	$_SESSION['staff_user_id'] = $staff_user_id;
		}
	}
}

function start_session_dogovor() {
	session_start();
	if (!isset($_SESSION['dogovor_user_kod'])) {
	    if ($staff_user_id = session_staff_user_id()) {
	    	$_SESSION['staff_user_id'] = $staff_user_id;
		}
	}
}

function start_session_reestr() {
	session_start();
	if (!isset($_SESSION['user_id'])) {
	    if ($staff_user_id = session_staff_user_id()) {
	    	$_SESSION['staff_user_id'] = $staff_user_id;
		}
	}
}

function start_session_bank() {
	session_start();
	if (!isset($_SESSION['user_id'])) {
	    if ($staff_user_id = session_staff_user_id()) {
	    	$_SESSION['staff_user_id'] = $staff_user_id;
		}
	}
}

function start_session_pk() {
	session_start();
	if (!isset($_SESSION['pk'])) {
	    if ($staff_user_id = session_staff_user_id()) {
	    	$_SESSION['staff_user_id'] = $staff_user_id;
		}
	}
}

function session_staff_user_id() {
	$staff_user_id = null;

    $id = session_id();
    session_abort();

    if (isset($_COOKIE[STAFF_cookie])) {
        $row = cookie_session_row($_COOKIE[STAFF_cookie]);
       	if ($row && ip_checked($row['ip'],$row['ip_x_forw'])) {
       		$staff_sess_id = $row['session'];
       		session_id($staff_sess_id);
       		session_start();
       		if (array_key_exists('staff_user_id',$_SESSION)) $staff_user_id = $_SESSION['staff_user_id'];
       		session_abort();
       	}
    }
    session_id($id);
    session_start();
    return $staff_user_id;
}

function cookie_session_row($cookie) {
	$mysqli = auth_db_connect();
	$cookie = $mysqli->real_escape_string($cookie);
	$res = $mysqli->query("select * from auth where cookie='$cookie'") or die("error load! ERROR:" . $mysqli->error);
	$row = $res->fetch_assoc();
	$mysqli->close();
	return $row;
}

function ip_checked($ip,$ip_x_forw) {
	$current_ip = $_SERVER['REMOTE_ADDR'];
	$current_ip_x_forw = array_key_exists('HTTP_X_FORWARDED_FOR',$_SERVER) ? $_SERVER['HTTP_X_FORWARDED_FOR'] : '';
	return ($ip == $current_ip && $ip_x_forw == $current_ip_x_forw);
}

?>