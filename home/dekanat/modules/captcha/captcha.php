<?php 

/*

$captcha = new captcha;
$captcha->set_width(120)->set_height(60)->set_cnt_chars(4)->set_cnt_false_chars(50)->set_font('comicz.ttf')->set_font_size(16);
$captcha->print_captcha();

*/


class captcha {
	private $w, $h, $cnt_chars, $cnt_false_chars, $font, $font_filename, $font_size;
	
	public function __construct() {
		$this->w = 120;
		$this->h = 60;
		$this->cnt_chars = 4;
		$this->cnt_false_chars = 30;
		$this->font = $this->font_filename('comicz.ttf');
		$this->font_size = 18;
		$this->chars = array("0","1","2","3","4","5","6","7","8","9");
		$this->colors = array("90","110","130","150","170","190","210");
		return $this;
	}

	public function set_width($w) {
		$this->w = $w;
		return $this;
	}

	public function set_height($h) {
		$this->h = $h;
		return $this;
	}

	public function set_cnt_chars($cnt_chars) {
		$this->cnt_chars = $cnt_chars;
		return $this;
	}

	public function set_cnt_false_chars($cnt_false_chars) {
		$this->cnt_false_chars = $cnt_false_chars;
		return $this;
	}

	public function set_font($font_filename) {
		$font_filename = $this->font_filename($font_filename);
		if (!file_exists($font_filename)) die('Wrong font file name <b>'.$font_filename.'</b>!');
		$this->font = $font_filename;
		return $this;
	}

	public function set_font_size($font_size) {
		$this->font_size = $font_size;
		return $this;
	}

	private function font_filename($font_filename) {
		$font_filename = dirname(__FILE__)."/fonts/".$font_filename;
		return $font_filename;
	}

	public function print_captcha() {
		$w = $this->w;
		$h = $this->h;
		$cnt_chars = $this->cnt_chars;
		$cnt_false_chars = $this->cnt_false_chars;
		$chars = $this->chars;
		$colors = $this->colors;
		$font = $this->font;
		$font_size = $this->font_size;

		$src = imagecreatetruecolor($w,$h);       
		$fon = imagecolorallocate($src,255,255,255);
		imagefill($src,0,0,$fon);
		if ($cnt_false_chars > 0) {
			// заполнение фона ложными символами
			for ($i=0; $i < $cnt_false_chars; $i++) {
				$color = imagecolorallocatealpha($src,rand(0,255),rand(0,255),rand(0,255),100);
				$char = $chars[rand(0,sizeof($chars)-1)];
				$size = rand($font_size-8,$font_size-4);
				imagettftext($src,$size,rand(0,45),rand($w*0.1,$w-$w*0.1),rand($h*0.2,$h),$color,$font,$char);
			}
		}

		$captcha = '';
		for ($i=0; $i < $cnt_chars; $i++) {
			$color = imagecolorallocatealpha($src,$colors[rand(0,sizeof($colors)-1)],
				$colors[rand(0,sizeof($colors)-1)],
				$colors[rand(0,sizeof($colors)-1)],rand(20,40));
			$char = $chars[rand(0,sizeof($chars)-1)];
			$size = rand($font_size*2-2,$font_size*2+2);
			$x = ($i+1)*$font_size + rand(1,5);
			$y = (($h*2)/3) + rand(0,5);
			$captcha .= $char;
			imagettftext($src,$size,rand(0,15),$x,$y,$color,$font,$char);
		}
		$this->set_session($captcha);
		header ("Content-type: image/gif");
		imagegif($src);
	}

	private function set_session($captcha) {
		if (version_compare(phpversion(), '5.4', '>')) {
			if (session_status() == PHP_SESSION_NONE) session_start();
		} else {
			if (session_id() == '')  session_start();
		}
		$_SESSION['captcha']=$captcha;
	}
}


