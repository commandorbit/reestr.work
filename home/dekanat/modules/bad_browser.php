<?php 

function good_browser() {
	$user_agent = (isset($_SERVER['HTTP_USER_AGENT'])) ? $_SERVER['HTTP_USER_AGENT'] : '';
	$is_good = 0;
	if ($user_agent) {
		require_once('user_agent_parse.php');
		$d = parse_user_agent($user_agent);
		$browser = $d['browser'];
		$version = $d['version'];
		$good = array('Firefox','Opera Next','Opera','AppleWebKit','Chrome','Safari','MSIE');
		if (in_array($browser,$good)) {
			if ($browser == 'MSIE') {
				if ($version >= '9.0') $is_good = 1;
			} elseif ($browser == 'Safari') {
				if ($version >= '5.1') $is_good = 1;
			} elseif ($browser == 'Opera') {
				if ($version >= '12.1') $is_good = 1;
			} else $is_good = 1;
		}
	}
	return $is_good;
}

function bad_browser_message() {
	echo <<<EOF
<html>
<head>
	<title>�� ����������� ���������� �������.</title>
	<style>
		html, body {width: 100%;height: 100%;background: #F7F7F7;padding: 0px;margin: 0px; font-family: Tahoma; color: #004930;}
		#bad_browser {position: absolute;left: 50%;top: 50%;text-align: center;width: 530px;margin: -200px 0px 0px -250px;background: #FFF;line-height: 180%;border-bottom: 1px solid #E4E4E4;-webkit-box-shadow: 0 0 3px rgba(0, 0, 0, 0.15);-moz-box-shadow: 0 0 3px rgba(0, 0, 0, 0.15);box-shadow: 0 0 3px rgba(0, 0, 0, 0.15);}
		#content {padding: 20px;font-size: 1.19em;}
		#content div {margin: 10px 0 15px 0;}
		#content #browsers {width: 480px;height: 136px;margin: 15px auto 0px;}
		#browsers a {float: left;width: 120px;height: 20px;padding: 106px 0px 13px 0;-webkit-border-radius: 4px;-khtml-border-radius: 4px;-moz-border-radius: 4px;border-radius: 4px;color: #004930;}
		#browsers a:hover {text-decoration: none;background-color: #edf1f5!important;}
		.prim {font-size: 12px; text-align: center;}
	</style>
	<!--[if lte IE 8]>
		<style>
			#bad_browser {border: none;}
			#wrap {border: solid #C3C3C3;border-width: 0px 1px 1px;}
			#content {border: solid #D9E0E7;border-width: 0px 1px 1px;}
		</style>
	<![endif]-->
</head>
<body>
<div id="bad_browser">
	<div id="wrap">
		<div id="content">
		<h3>������� �� ��������������</h3>
		<div>��� ������ � ������, <br>��������� � ���������� ���� �� ���� ���������:
			<div id="browsers" style="width: 360px;">
				<a href="http://www.google.com/chrome/" target="_blank" style="background: url(/modules/browsers/chrome.png?1) no-repeat 50% 17px;">Chrome</a>
				<a href="http://www.opera.com/" target="_blank" style="background: url(/modules/browsers/opera.png?1) no-repeat 50% 15px;">Opera</a>
				<a href="http://www.mozilla-europe.org/" target="_blank" style="background: url(/modules/browsers/firefox.png?1) no-repeat 50% 17px;">Firefox</a>
			</div>
		</div>
		<p class="prim">
			���. ��������� �������������� �������:<br>
			������� 5523, 
			��������� (812)458-9705 
		</p>
		</div>
	</div>
</div>
</body>
</html>
EOF;
}
?>