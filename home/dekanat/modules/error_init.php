<?php


/* 

	utf-8 
	
*/

$DEVELOPERS = array(
'localhost'=>array('ip'=>'127.0.0.1','email'=>'','send_all_errors'=>false),
'Dmitriy Kuznetsov'=>array('ip'=>'10.123.124.210','email'=>'dim@unecon.ru','send_all_errors'=>true),
'Alexey Chernov'=>array('ip'=>'10.123.124.215','email'=>'alex_tche@mail.ru','send_all_errors'=>false),
'Alexander Golovanov'=>array('ip'=>'10.123.124.220','email'=>'akg@finec.ru','send_all_errors'=>false),
'Irina Budagova'=>array('ip'=>'10.123.124.211','email'=>'','send_all_errors'=>false),
'Elena Vasilieva'=>array('ip'=>'10.123.124.218','email'=>'','send_all_errors'=>false),
'Nadejda Malinina'=>array('ip'=>'10.123.124.214','email'=>'','send_all_errors'=>false),
);

// бабы в списке нужны чтобы внятно могли сказать текст ошибки

$REMOTE_IP = array_key_exists('REMOTE_ADDR',$_SERVER) ? $_SERVER['REMOTE_ADDR'] : '';

$DEV_IP = array();
foreach ($DEVELOPERS as $v) {
	$DEV_IP[]=$v['ip'];
}

$EMAIL_ADDRS = array();
foreach ($DEVELOPERS as $name=>$v) {
	if (strlen($v['email'])>0) {
		if ($v['send_all_errors']) {
			// разработчик получающий все ошибки
			$EMAIL_ADDRS[]=$v['email'];
		} else {
			// разработчик не получающий ошибки других разработчиков
			if (!in_array($REMOTE_IP,$DEV_IP) || $REMOTE_IP == $v['ip']) {
				// получает если удаленный адрес не входит в список разработчиков, или удаленный адрес = адресу текущего разработчика
				if ($v['email']) $EMAIL_ADDRS[]=$v['email'];
			}
		}
	}
}
$SERVER_NAME = array_key_exists('SERVER_NAME',$_SERVER) ? $_SERVER['SERVER_NAME'] : '';
$SERVER_SHORT_NAME = str_replace(".unecon.ru","",$SERVER_NAME);
$EMAIL_ADDR_TO = implode(",",$EMAIL_ADDRS);
$EMAIL_ADDR_FROM = 'noreply@unecon.ru';

$SITES_SUPPORT_BY_LENA = [
	 'dekanat.unecon.ru'
	,'dekanat.finec.ru'
	,'rating.unecon.ru'
	,'rating.finec.ru'
];

$SUPPORT_IS_LENA = (array_key_exists('HTTP_HOST',$_SERVER) && in_array($_SERVER['HTTP_HOST'],$SITES_SUPPORT_BY_LENA));

if ($_SERVER['HTTP_HOST'] == 'priem.unecon.ru' || $_SERVER['HTTP_HOST'] == 'rasp.unecon.ru') {
	$DEV_CONTACT = '';
} elseif ($SUPPORT_IS_LENA) {
	$DEV_CONTACT = 'Телефон отдела сопровождения ИС: 458-9705 (местный: 5523)';
} else {
	$DEV_CONTACT = 'Телефон местной связи: 1001 (Дмитрий Кузнецов)';
}

require_once dirname(__FILE__) . "/error/error.php";
$e = new Err(false,true);
$e->setServerName($SERVER_NAME)->setServerShortName($SERVER_SHORT_NAME)->setDebugIps($DEV_IP)->setMailFrom($EMAIL_ADDR_FROM)->setMailTo($EMAIL_ADDR_TO)->setContact($DEV_CONTACT);
set_error_handler(array($e,'handler'));

