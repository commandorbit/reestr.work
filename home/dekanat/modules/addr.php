<?php 
require_once '/home/dekanat/modules/kladr_address/kladr_address.php';


function stud_addr($stud_kod,$is_pasp = true,$is_priem = false) {
	$addr = '';
	$database = ($is_priem) ? "`abit2014`" : "`dekanat`";
	if ($row = sql_get_array("$database.abitur","abitur_kod='$stud_kod'")) {
		if ($is_pasp) {
			$addr = (!empty($row['pasp_addr_text'])) ? $row['pasp_addr_text'] : kladr_address::row2adress($row,'pasp');
			if (!strlen($addr)) $addr = '����������';
		} else {
			if ($row['no_fact_adr']) $addr = '��� ��';
			else $addr = kladr_address::row2adress($row,'fact');
		}
	}
	return $addr;
}
?>