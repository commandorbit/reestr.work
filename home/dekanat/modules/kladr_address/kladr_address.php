<?php

class kladr_address{
	public static function row2adress($row,$prefix,$kvart='kvart') {
		$s="";
		$fname=$prefix . '_index';
		if (!empty($row[$fname]) && $row[$prefix . '_grdn_kod']==643) {
			$s = self::add_comma($s);
			$s .= $row[$fname];
		}
		$fname=$prefix .'_grdn_kod';
		if ($row[$fname]==643) {
			$s = self::add_comma($s);
			$s.= self::kladr_np_name($row[$prefix. '_region'],$row[$prefix. '_gorod_code'],$row[$prefix. '_gorod']);
		} else {
			$s.=self::gragd_name($row[$fname]) ;
			$fname=$prefix . '_index';
			if (!empty($row[$fname]) ) {
				$s = self::add_comma($s);
				$s .= $row[$fname];
			}
			$fname=$prefix . '_gorod';
			if (!empty($row[$fname]) ) {
				$s = self::add_comma($s);
				$s .= $row[$fname];
			}
		}
		$fname=$prefix . '_street';
		if (!empty($row[$fname])) {
			$s = self::add_comma($s);
			$s.=self::street_name($row[$prefix . '_street']);
		}
		$fname=$prefix . '_dom';
		if (!empty($row[$fname])) $s .=' '.self::get_param_value('dom') . $row[$fname];
		$fname=$prefix . '_korp';	
		if (!empty($row[$fname])) $s .=' '.self::get_param_value('korpus') . $row[$fname];
		$fname=$prefix . '_kvart';
		if (!empty($row[$fname])) $s .=' ' .self::get_param_value($kvart) . $row[$fname];
		return $s;
	}
	
	private static function add_comma($s) {
		if (strlen($s)>0) $s .= ', ';
		return $s;
	}

	private static function get_param_value($param_name) {
		$param_name = sql_escape($param_name);
		return sql_get_value("param_value","`kladr`.params","param_name='$param_name'");
	}
	
	private static function need_dot($socr,$level) {
		$socr = sql_escape($socr);
		return sql_get_value("NEED_DOT","`kladr`.socrbase","LEVEL=$level and SCNAME='$socr'");
	}
	
	private static function street_has_socr($socr) {
		$socr = sql_escape($socr);
		return sql_get_value("count(*)","`kladr`.socrbase","LEVEL=5 and SCNAME='$socr'");
	}
	
	private static function kladr_rekurs_name($code) {
		$code = sql_escape($code);
		$sql="select * from `kladr`.kladr where CODE='$code' ORDER BY ACTUAL_CODE";
		$result = sql_query($sql);
		$name="";
		if ($row=sql_array($result)) {
			if ($row['LEVEL']<>1) {
				$socr=$row['SOCR'];
				if (self::need_dot($socr,$row['LEVEL'])) $socr .='.';
				if ($row['LEVEL']==2) $name=$row['NAME'] . ' '. $socr;
				else $name=$socr .' '. $row['NAME'];
				if (!empty($row['PARENT_CODE']) && $row['STATUS']==0) {
					$parent=self::kladr_rekurs_name($row['PARENT_CODE']);
					if (!empty($parent) ) $name= $parent . ', ' .$name;
				} 
			}
		}
		return $name;
	}
	
	private static function kladr_np_name($region_kod,$np_code,$np_name) {
		$np_code = sql_escape($np_code);
		$np_name = sql_escape($np_name);
		$s=self::region_name($region_kod);
		$sql="select * from `kladr`.kladr where CODE='$np_code' and FULL_NAME='$np_name'";
		$result = sql_query($sql);
		if (sql_count_rows($result)>0) $name = self::kladr_rekurs_name($np_code);
		else $name = $np_name;
		if (!empty($name)) $s .=', '.$name;
		return $s;
	}
	
	private static function gragd_name($gragd_kod) {
		return sql_get_value("gragd_name","`dekanat`.gragd","gragd_kod='$gragd_kod'");
	}
	
	private static function region_name($region_kod) {
		return sql_get_value("post_name","`kladr`.kladr_region","reg_code='$region_kod'");
	}

	private static function street_name($street) {
		$a=explode(' ',$street );
		$last=count($a)-1;
		if (!self::street_has_socr($a[0]) && self::street_has_socr($a[$last])) {
			$b = array();
			$b[0] = $a[$last];
			for ($i=0; $i<(count($a)-1); $i++) {
				$b[($i+1)]=$a[$i];
			}
			$a = $b;
		}
		if (self::need_dot($a[0],5)) $a[0].='.';
		return implode(' ',$a);
	}
}

?>