<?php
require_once $_SERVER['DOCUMENT_ROOT'].'/init.php';
if (!current_user_info()) exit;
db_connect_ro();
sql_set_charset("UTF8");

if (!current_user_info()) exit;
header('Content-Type: text/html;');
$term = clear_string(request_val('term',''));
$func = request_val('func','');
$funcs = array('_stud_fio','_stud_fio_1_kurs','_prepod_fio','_vuz_name');

if ($term && $func) {
	if (array_search($func,$funcs)!==false) $func($term);
	else echo 'unknown function ' . $func;
}

function _stud_fio_1_kurs($fio) {
	$w = " AND t.zach_data_s >='2015-09-01' ";
	_stud_fio($fio,$w);
}

function _stud_fio($fio,$w = '') {
	$fio = str_replace(iconv("cp1251","utf-8","Ё"),iconv("cp1251","utf-8","Е"),$fio);
	$fio = str_replace(iconv("cp1251","utf-8","ё"),iconv("cp1251","utf-8","е"),$fio);
	$s = "SELECT DISTINCT t.fio   
	FROM 
	(SELECT a.fio, ds.zach_data_s FROM abitur a 
		INNER JOIN decanat_stud ds ON a.abitur_kod=ds.stud_kod 
		WHERE a.fio like '$fio%' 
	UNION 
		SELECT o.fio, ds.zach_data_s FROM abitur_old_fio o 
		INNER JOIN decanat_stud ds ON o.abitur_kod=ds.stud_kod 
		WHERE o.fio like '$fio%'
	) t 
	WHERE true $w 
	ORDER BY t.fio LIMIT 0,30";
	$result = sql_query($s);
	$a=array();
	while($row = sql_array($result)) {
		$a[] = $row['fio'];
	}
	echo json_encode($a);
}

function _prepod_fio($prepod_fio) {
	$dubli_fio = prepods_dubli_fio();
	$sql = "SELECT p.prepod_kod, p.prepod_fio, p.is_actual, p.kafedra_kod, k.kafedra_name 
	FROM prepod p
	LEFT JOIN kafedra k ON p.kafedra_kod=k.kafedra_kod 
	WHERE p.prepod_fio like '$prepod_fio%' ORDER BY p.prepod_fio LIMIT 0,15";
	$res = sql_query($sql);
	$a = array();
	while($row = sql_array($res)) {
		$has_dubli = (in_array($row['prepod_fio'],$dubli_fio));
		$row['uvolen'] = ($row['is_actual']==0) ? 'уволен' : '';
		$a[] = prepod_row_2autocomplete_fio($row,$has_dubli); 
	}
	echo json_encode($a);
}

function _vuz_name($vuz_name) {
	$s = "SELECT vuz_kod, vuz_name FROM s_vuz WHERE vuz_name like '%$vuz_name%' ORDER BY vuz_name LIMIT 0,15 ";
	$result = sql_query($s);
	$a = array();
	while($row = sql_array($result)) {
		$a[] = $row['vuz_name'];
	}
	echo json_encode($a);
}
?>