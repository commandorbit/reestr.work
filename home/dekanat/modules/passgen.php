<?php 

function passgen($length = 7,$strength = 0) {
	$vowels = 'aeuy';
	$consonants = 'bdghjmnpqrstvz';
	if ($strength > 0) $vowels .= '23456789';
	if ($strength > 1) $consonants .= 'BDGHJLMNPQRSTVWXZ';
	if ($strength > 2) $vowels .= "AEUY";
	if ($strength > 3) $vowels .= '@#%!';
	$password = '';
	$alt = time() % 2;
	for ($i = 0; $i < $length; $i++) {
		if ($alt == 1) {
			$password .= $consonants[(rand() % strlen($consonants))];
			$alt = 0;
		} else {
			$password .= $vowels[(rand() % strlen($vowels))];
			$alt = 1;
		}
	}
	return $password;
}

?>