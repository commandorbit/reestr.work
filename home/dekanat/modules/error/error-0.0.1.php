<?php
/**
 Version: 0.0.1
 Author: Alex Chernov
 Last-modfied: 2013-08-10
*/ 
Class Err {
	var $serverName="";
	var $serverShortName="";
	var $debugIps=array();
	var $mailTo ="";
	var $mailFrom ="";
	var $devContact="";
	var $debug=false;
	var $trace=false;
	
	/**
	* 
	
	*/	
	public function __construct($debug=false,$trace=true) {
		$this->debug=$debug;
		$this->trace=$trace;
	}
	
	/**
	*	Перехват ошибок в самом обработчике ОШИБОК!
	*/
	protected function error_in_error_handler($errno, $errstr, $errfile, $errline) {
		$s = '<h1>Error in error handler!</h1>';
		$s .= '<br>Error Number:' . $errno ;
		$s .= '<br>Error String:' . $errstr ;
		$s .= '<br>File:' . $errfile ;
		$s .= '<br>Line:' . $errline ;		
		self::out_err_msg ($s);
		if ($this->mailTo) {
			self::mail($this->mailTo,$s,'Error in error handler!');
		}
		die();
	}	
	
	public function handler($errno, $errstr, $errfile, $errline)  {
		$e=$this;
		set_error_handler (array($e,'error_in_error_handler'));
		$show_error= $this->debug || ( isset($_SERVER['REMOTE_ADDR']) && in_array($_SERVER['REMOTE_ADDR'],$this->debugIps));
		$html_text=self::html_err_text($errno,$errstr,$errfile,$errline); 
		if ($this->mailFrom && $this->mailTo) {
			$topic = 'Error: ' . $this->serverShortName;
			if (array_key_exists('REQUEST_URI',$_SERVER)) $topic .= $_SERVER['REQUEST_URI'];
			self::mail($this->mailTo,$html_text,$topic);
		}
		$text = ($show_error) ? $html_text : '';
		self::out_err_msg($text);
		die();
	}
	
	private function out_err_msg($text) {
		while (ob_get_level()>0) ob_end_clean();
		$error_text= mb_convert_encoding($text, 'UTF-8', 'UTF-8');		
		require_once (dirname(__FILE__) . '/error.html');
	}
	
	private function html_err_text($error_kode, $error_text, $error_file, $error_line) {
		$url = (array_key_exists('REQUEST_URI',$_SERVER)) ? $_SERVER['REQUEST_URI'] : "";
		$a_href = '<a href="http://' . $this->serverName . $url . '">http://' . $this->serverName . $url . '</a>';
		$error_time = "<br>Время ошибки: " . date("d.m.Y H:i");
		$text = $a_href . '<br>' . $error_time .'<br>';
		$text .= '<b>Error:</b>' . $error_text .'<br>';
		$text .= '<b>File:</b>' . $error_file . '<br> <b>Line:</b>' . $error_line . ' <br>';
		if ($this->trace) {
			$text .='<br><b>Trace:</b><br>' . self::backtrace(3) . '<br><br>'; // handler + backtrace + html_err_text
		}

		$ref = (array_key_exists('HTTP_REFERER',$_SERVER)) ? '<br><b>ref = </b>' . $_SERVER['HTTP_REFERER']: "";
		$browser = (array_key_exists('HTTP_USER_AGENT',$_SERVER)) ? '<br><b>browser = </b>' . $_SERVER['HTTP_USER_AGENT']: "";
		$ip = (array_key_exists('REMOTE_ADDR',$_SERVER)) ? '<br><b>remote_ip = </b>' . $_SERVER['REMOTE_ADDR']: "";
		
		$text .= $ip .$ref . $browser .  "<br><br>" ;
		if (isset($_SESSION)) $text .= self::array_dump($_SESSION,'SESSION');
		$text .= self::array_dump($_REQUEST,'REQUEST');
		$text .= self::array_dump($_SERVER,'SERVER');
	
		return $text;
	}
	
	private function mail($to,$text,$topic='error') {
		$from=$this->mailFrom;
		$headers = 'MIME-Version: 1.0' . "\r\n" ;
		if ($from) {
			$headers .= "From: $from\r\n";
			$headers .= "Reply-To: $from\r\n";
		}
		$headers .= 'Content-type: text/html; charset="utf-8"';
		if ($from)
			mail ($to,$topic,$text,$headers,"-f$from");
		else
			mail ($to,$topic,$text,$headers);
	}
	
	public function setServerName($serverName) {
		$this->serverName=$serverName;
		return $this;
	}
	public function setServerShortName($serverShortName) {
		$this->serverShortName=$serverShortName;
		return $this;
	}

	public function setDebugIps($ips) {
		$this->debugIps=$ips;
		return $this;		
	}
	public function setMailFrom($mail) {
		$this->mailFrom=$mail;
		return $this;
	}
	public function setMailTo($mail) {
		$this->mailTo=$mail;
		return $this;		
	}
	public function setContact($text) {
		$this->devContact=$text;
		return $this;		
	}
	
	public static function backtrace($traces_to_ignore = 0){
		$traces = debug_backtrace();
		$ret = array();
		foreach($traces as $i => $call){
			if ($i < $traces_to_ignore ) {
				continue;
			}
			$object = '';
			if (isset($call['class'])) {
				$object = $call['class'].$call['type'];
				
				if (isset($call['args']) && is_array($call['args'])) {
					foreach ($call['args'] as &$arg) {
						self::get_arg($arg);
					}
				}
			}
			if (isset($call['args'])) 
				$args=self::implode_r(', ', $call['args']);
			else 
				$args='';
			$file = isset($call['file']) ? $call['file'] : ' ? ';
			$line = isset($call['line']) ? $call['line'] : ' ? ';
			
			$r='#'.str_pad($i - $traces_to_ignore, 3, ' ');
			$r.=$object.$call['function'].'('.$args.') called at ['.$file.':'.$line.']';
			$ret[] =$r;
		}
		return implode("<br>\n",$ret);
	}

	private static function get_arg(&$arg) {
		if (is_object($arg)) {
			$arr = (array)$arg;
			$args = array();
			foreach($arr as $key => $value) {
				if (strpos($key, chr(0)) !== false) {
					$key = '';    // Private variable found
				}
				$args[] =  '['.$key.'] => '.self::get_arg($value);
			}
			$arg = get_class($arg) . ' Object ('.self::implode_r(',', $args).')';
		}
	}
	private static function implode_r($glue,$arr){
        $str = "";
		$first=true;
        foreach($arr as $a){
            if (!$first) $str .= ',';
             /// var_export() does not handle circular reference
            try {
                if (is_array($a)) $str .= 'array: {' .self::implode_r($glue,$a) .'}';
                else if (is_object($a)) $str .= 'object: {}';
                else $str .= var_export($a,true);
            } catch (Exception $e) {
                $str .= "{error parsing argument}";
            }
			$first=false;
        }
        return $str;
	}

	private function array_dump($array,$name) {
		$s = '';
		if (isset($array)) {
			$s .= '<b>' . $name . ':</b><br>';
			foreach ($array as $key => $value) {
				if (is_string($value)) {
					$s .= $key . '=' . $value . '<br>';
				}
			}
			$s .= '<br>';
		
		}
		return $s;
	}
}

