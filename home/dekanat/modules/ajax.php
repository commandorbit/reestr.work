<?php
define('UTF-8',1);
require_once $_SERVER['DOCUMENT_ROOT'].'/init.php';
if (!current_user_info()) exit;
db_connect_ro();

$func = request_val('func','');
$kurs = request_numeric_val('kurs',0);
$stud_kod  = request_numeric_val('s',0);
$nomer_zach = request_val('nz',0);
if ($nomer_zach) $nomer_zach = clear_string(win_utf($nomer_zach));
$fakultet_kod = request_numeric_val('fakultet_kod',0);
$kudapost_kod = request_numeric_val('kudapost_kod',0);
$grp_spec_kod = request_numeric_val('grp_spec_kod',0);
$profile_kod = request_numeric_val('profile_kod',0);
$ob_forma_kod = request_numeric_val('ob_forma_kod',0);
$ob_osnova_kod = request_numeric_val('ob_osnova_kod',0);
$srok_kod = request_numeric_val('srok_kod',0);
$grp_kod = request_numeric_val('grp_kod',0);
$upp_kod = request_numeric_val('upp_kod',0);

$hotel_kod = request_numeric_val('hotel_kod',0);
$hotel_lgota_type_kod = request_numeric_val('hotel_lgota_type_kod',0);
$guest_type_kod = request_numeric_val('guest_type_kod',0);
$room_kod = request_numeric_val('room_kod',0);
$date_s = request_val('date_s',0);
$date_po = request_val('date_po',0);
$fms_code = request_val('fms_code','');

// fix лингвистика
if ($grp_spec_kod && grp_spec_is_lingv($grp_spec_kod)) $grp_spec_kod=9;
if ($profile_kod && grp_spec_is_lingv($profile_kod)) $profile_kod=9;

$w = "";
if ($kurs) $w .= " and t.kurs='$kurs' ";
if ($fakultet_kod) $w .= " and t.fak_kod='$fakultet_kod' ";
if ($ob_forma_kod) $w .= " and t.ob_forma_kod='$ob_forma_kod' ";
if ($grp_spec_kod) $w .= " and t.grp_spec_kod='$grp_spec_kod' ";
if ($profile_kod) $w .= " and (t.profile_kod='$profile_kod' or t.profile_kod is null) ";
if ($srok_kod) $w .= " and t.srok_kod='$srok_kod' ";

$funcs = array(
	 'prikaz_grps'
	,'spiski_grps'
	,'fms_name'
	,'hotel_room_options'
	,'hotel_room_has_mesta'
	,'hotel_room_has_mesta_options'
	,'hotel_guest_tarif'
	,'hotel_price_options'
	,'kudapost_spec_options'
	,'grp_spec_profile_options'
	,'kudapost_srok_options'
	,'check_nz'
	,'upp_kafedra_options'
);

if (in_array($func,$funcs)) {
	if ($func == 'kudapost_spec_options' || $func == 'kudapost_srok_options') $func($kudapost_kod);
	elseif ($func == 'grp_spec_profile_options') $func($grp_spec_kod);
	elseif ($func == 'hotel_room_options') $func($hotel_kod,$room_kod);
	elseif ($func == 'hotel_room_has_mesta_options') $func($hotel_kod,$room_kod,$stud_kod);
	elseif ($func == 'hotel_guest_tarif') $func($hotel_kod,$guest_type_kod,$date_s,$date_po);
	elseif ($func == 'check_nz') echo utf_win(nomer_zach_exists_error($nomer_zach,$stud_kod));
	elseif ($func == 'fms_name') echo fms_name($fms_code);
	elseif ($func == 'hotel_room_has_mesta') {
		$mesta = hotel_room_mesta($room_kod,$stud_kod);
		if (!$mesta['has_mesta']) echo json_encode($mesta);
	} elseif ($func == 'hotel_price_options') {
		$params = [
			'hotel_kod'=>$hotel_kod,
			'room_kod'=>$room_kod,
			'ob_osnova_kod'=>$ob_osnova_kod,
			'hotel_lgota_type_kod'=>$hotel_lgota_type_kod
		];
		$func($params);
	} else $func($w);
} else {
	echo 'undefined func!';
}

function prikaz_grps($w) {
	global $grp_kod;
	print_options(array_add_null_2(grps($w)),$grp_kod);
}

function grps($w) {
	$s = "SELECT t.grp_kod, t.grp_nomer  FROM 
	(SELECT g.grp_kod, g.grp_nomer, g.kurs, up.* 
		FROM decanat_fakultet_grp g 
		INNER JOIN uch_plan up ON g.uch_plan_kod=up.uch_plan_kod 
		ORDER BY g.grp_nomer 
	) t 
	WHERE true $w ";
	$a = sql_to_assoc($s);
	return $a;
}

function upp_kafedra_options() {
	global $upp_kod;
	if ($upp_kod && $predmet_kod = upp_predmet_kod($upp_kod)) {
		$kafs = predmet_kafedra_array($predmet_kod);
	} else {
		$kafs = kafedra_array_actual();
	}
	print_options(array_add_null_2($kafs),'');
}

function hotel_room_options($hotel_kod,$room_kod) {
	print_options(array_add_null_2(hotel_room_array($hotel_kod)),$room_kod);
}

function hotel_room_has_mesta_options($hotel_kod,$default_room_kod,$stud_kod) {
	$rooms = hotel_room_array($hotel_kod,1);
	foreach ($rooms as $room_kod=>$dummy) {
		$room_status_kod = sql_get_value("room_status_kod","s_hotel_room","room_kod='$room_kod'");
		$room_status_may_zaselenie = ($room_status_kod) 
			? sql_get_value("may_zaselenie","s_hotel_room_status","room_status_kod='$room_status_kod'") 
			: true;
		$mesta = hotel_room_mesta($room_kod,$stud_kod);
		if (!$mesta['has_mesta'] || !$room_status_may_zaselenie) unset($rooms[$room_kod]);
	}
	print_options(array_add_null_2($rooms),$default_room_kod);
}

function hotel_room_mesta($room_kod,$stud_kod) {
	$info = ['has_mesta'=>1];
	if ($room = sql_get_array("s_hotel_room","room_kod='$room_kod'")) {
		$zanyato = sql_get_value("count(*)","v_stud_hotel","room_kod='$room_kod' and stud_kod <> '$stud_kod' ");
		$info['has_mesta'] = ($zanyato >= $room['mest']) ? 0 : 1;
		$info['room'] = $room['room'];
		$info['mest'] = $room['mest'];
		$info['zanyato'] = $zanyato;
	}
	return $info;
}

function hotel_guest_tarif($hotel_kod,$guest_type_kod,$date_s,$date_po) {
	// не учитывает, когда период может охватывать несколько разных тарифов
	$tarif = sql_get_value("cost","s_hotel_cost_guest","hotel_kod='$hotel_kod' and guest_type_kod='$guest_type_kod' and date_s < now() order by date_s desc limit 0,1 ");
	$date_s = date_2format($date_s,"Y-m-d");
	$date_po = date_2format($date_po,"Y-m-d");
	$cnt_days = ($date_s && $date_po) ? date_difference($date_s,$date_po) : 0;
	$sum = ($cnt_days && $cnt_days>0) ? $tarif*$cnt_days : 0;
	echo json_encode(array('tarif'=>$tarif,'sum'=>$sum));
}

function hotel_price_options($params) {
	$prices = hotel_price_array($params);
	if (!$prices) $prices = array_add_null_2(array());
	print_options($prices,'');
}

function kudapost_spec_options($kudapost_kod) {
	print_options(array_add_null_2(spec_array($kudapost_kod)),'');
}

function grp_spec_profile_options($grp_spec_kod) {
	print_options(array_add_null_2(profile_array($grp_spec_kod)),'');
}

function kudapost_srok_options($kudapost_kod) {
	print_options(array_add_null_2(srok_array($kudapost_kod)),'');
}

?>