<?php 

class logger {
	private static $is_ok, $d, $server, $url, $user_agent, $remote_ip, $remote_ip_forw, $login_ip, $session_id, $msg;

	private static function set_d() {
		self::$d = date("Y-m-d");
	}
	
	private static function set_server() {
		if (array_key_exists('SERVER_NAME', $_SERVER)) {
			self::$server = sql_escape($_SERVER['SERVER_NAME']);
			if ($_SERVER['SERVER_NAME']=='priem.unecon.ru') {
				if (strstr($_SERVER['REQUEST_URI'],"/priem/")) self::$server .= "/priem";
			}
		}
	}

	private static function set_user_agent() {
		if (array_key_exists('HTTP_USER_AGENT',$_SERVER)) self::$user_agent = sql_escape($_SERVER['HTTP_USER_AGENT']);
	}

	private static function set_request_url() {
		if (array_key_exists('REQUEST_URI',$_SERVER)) self::$url = sql_escape($_SERVER['REQUEST_URI']);
	}
	
	private static function set_remote_ip() {
		if (array_key_exists('REMOTE_ADDR',$_SERVER)) self::$remote_ip = $_SERVER['REMOTE_ADDR'];
	}
	
	private static function set_remote_ip_forw() {
		if (array_key_exists('HTTP_X_FORWARDED_FOR',$_SERVER)) self::$remote_ip_forw = $_SERVER['HTTP_X_FORWARDED_FOR'];
	}

	private static function user_login_ip() {
		if (array_key_exists('login_ip',$_SESSION)) self::$login_ip = $_SESSION['login_ip'];
	}

	public static function user_session_id() {
		if (array_key_exists('session_id',$_SESSION)) self::$session_id = $_SESSION['session_id'];
	}

	private static function log_db_connect() {
		sql_connect('localhost','log','r4byh6g4Ger5','log');
		sql_set_charset('utf8');
	}

	private static function default_db_connect() {
		if (self::$server == 'dogovor.unecon.ru' || self::$server == 'dogovor.ru') {
			connect_ro();
		} else {
			db_connect_ro();
		}
	}

	private static function set_msg($msg_kod) {
		$errors = array(
			'LOGIN'=>array(true,'успешный вход в систему'),
			'LOGIN_BY_STAFF_ID'=>array(true,'вход в систему используя staff_user_id'),
			'ERROR_EMPTY_PASSWORD'=>array(false,'в базе пустой пароль'),
			'ERROR_BAD_PASSWORD'=>array(false,'введен неверный пароль'),
			'ERROR_BAD_LOGIN'=>array(false,'введен неверный логин'),
			'ERROR_BAD_LOGIN_PASSWORD'=>array(false,'введен неверный логин и пароль'),
			'ERROR_USER_BLOCKED'=>array(false,'пользователь заблокирован'),

			'PASSWORD_SENDED'=>array(true,'отправка логина и пароля'),
			'PASSWORD_SENDED_ADMIN'=>array(true,'отправка логина и пароля админом'),
			'PASSWORD_SENDED_CHIEF'=>array(true,'отправка логина и пароля начальником отдела'),
			'PASSWORD_LINK_SENDED'=>array(true,'отправка ссылки для получения пароля'),
			'PASSWORD_LINK_SENDED_FIRST_TIME'=>array(true,'первичная отправка ссылки для получения пароля'),
			
			'ERROR_LINK_EXPIRED'=>array(false,'время ссылки на получение пароля истекло'),
			'ERROR_LINK_BAD'=>array(false,'неверная ссылка для получения пароля'),
			'ERROR_INVALID_EMAIL'=>array(false,'попытка отправки ссылки для некорректного email'),
			'ERROR_UNKNOWN_EMAIL'=>array(false,'попытка отправки ссылки для незарегистрированного email'),

			'ERROR_CAPTHA'=>array(false,'неверно введены символы с картинки'),
			'ERROR_SENDMAIL'=>array(false,'проблемы с отправкой почты'),
			'ERROR_UNKNOWN'=>array(false,'неизвестное событие')
		);

		if (!array_key_exists($msg_kod,$errors)) $msg_kod = 'ERROR_UNKNOWN';
		self::$msg = $errors[$msg_kod][1];
		self::$is_ok = $errors[$msg_kod][0];
	}

	public static function set_session($login,$pass,$msg_kod,$user_kod) {
		self::log_db_connect();
		self::set_d();
		self::set_server();
		self::set_user_agent();
		self::set_remote_ip();
		self::set_remote_ip_forw();
		self::set_msg($msg_kod);
		$login = (self::$is_ok) ? 'null' : "'$login'";
		$pass = (self::$is_ok) ? 'null' : "'$pass'";
		$is_ok = (self::$is_ok) ? '1' : '0';
		$user_kod = ($user_kod) ? $user_kod : 'null';
		$sql = "
INSERT INTO log.session (server, login, pass, is_ok, msg, user_kod, d, ip, ip_forw, user_agent) 
VALUES ('".self::$server."', $login, $pass, $is_ok, '".self::$msg."', $user_kod, '".self::$d."', '".self::$remote_ip."', '".self::$remote_ip_forw."', '".self::$user_agent."')
		";
		sql_query($sql);
		$_SESSION['session_id']=sql_last_id();
		$_SESSION['login_ip']=self::$remote_ip;
		self::default_db_connect();
	}

	public static function session_has_many_ip() {
		self::set_remote_ip();
		self::set_remote_ip_forw();
		self::user_login_ip();
		$has_problem = self::$remote_ip != self::$login_ip;
		//  ЧАВ 2016-08-09 {
		// google yandex jokes like Opera Mini((
		if ($has_problem && self::$remote_ip_forw) {
			$has_problem = self::$remote_ip_forw != self::$login_ip;
		}
		//  } ЧАВ 2016-08-09 
		return $has_problem;
	}

	public static function set_session_many_ip() {
		$has_problem = self::session_has_many_ip();
		if ($has_problem || is_dim()) {
			self::log_db_connect();
			self::user_session_id();
			self::set_d();
			self::set_server();
			self::set_request_url();
			self::set_user_agent();
			self::set_remote_ip();
			self::set_remote_ip_forw();
			self::user_login_ip();
			$sql = "
INSERT INTO log.session_many_ip (session_id, server, d, url, 
	current_ip, current_ip_forw, login_ip, user_agent) 
VALUES ('".self::$session_id."','".self::$server."','".self::$d."','".self::$url."',
	'".self::$remote_ip."','".self::$remote_ip_forw."','".self::$login_ip."','".self::$user_agent."')
			";
			sql_query($sql);
			self::default_db_connect();
		}
	}

	public static function save_password($user_kod,$password) {
		self::log_db_connect();
		self::set_d();
		self::set_server();
		self::set_request_url();
		self::set_user_agent();
		self::set_remote_ip();
		self::set_remote_ip_forw();
		$sql = "
INSERT INTO log.`password` (server, user_kod, pass, d, url, ip, ip_forw, user_agent) 
VALUES ('".self::$server."',$user_kod,'$password','".self::$d."','".self::$url."',
	'".self::$remote_ip."','".self::$remote_ip_forw."','".self::$user_agent."')
		";
		sql_query($sql);
		self::default_db_connect();
	}

	public static function set_send_password($email,$msg_kod,$user_kod,$admin_kod = false) {
		self::log_db_connect();
		self::set_d();
		self::set_server();
		self::set_user_agent();
		self::set_remote_ip();
		self::set_remote_ip_forw();
		self::set_msg($msg_kod);
		$email = ($email) ? "'$email'" : 'null';
		$is_ok = (self::$is_ok) ? '1' : '0';
		$user_kod = ($user_kod) ? $user_kod : 'null';
		$admin_kod = ($admin_kod) ? $admin_kod : 'null';
		$sql = "
INSERT INTO log.send_password (server, email, is_ok, msg, user_kod, admin_kod, d, ip, ip_forw, user_agent) 
VALUES ('".self::$server."',$email,$is_ok,'".self::$msg."', $user_kod, $admin_kod, '".self::$d."','".self::$remote_ip."','".self::$remote_ip_forw."','".self::$user_agent."')
		";
		sql_query($sql);
		self::default_db_connect();
	}
}

?>