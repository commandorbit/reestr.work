<?php
require_once $_SERVER['DOCUMENT_ROOT'].'/init.php';
require_once $_SERVER['DOCUMENT_ROOT'].'/modules/save_table/class/save_table_class.php';
sql_set_charset('utf8');

$tables = [
	's_hotel_room',
	'profile_mest',
	'stud_hotel',
];
$method = request_val('method','save');
$table = request_val('table','');
$permission_id = sql_get_value("permission_id","data_table","name='$table'");
if (!$table) exit();


$status = '';
$row = [];
$errors = [];

if (!in_array($table,$tables) && !sql_get_value("id","data_table","name='$table'")) {
	$status = 'ERROR';
	$message = 'К сожалению, данная таблица не подлежит редактированию с помощью данного инструмента!';
	echo json_encode(array('status'=>$status,'row'=>$row,'message'=>$message,'errors'=>$errors));
	exit();
}

if (current_user_is_ro()) {
	$status = 'ERROR';
	$message = 'Ваша учетная запись не имеет права записи в базу!';
	echo json_encode(array('status'=>$status,'row'=>$row,'message'=>$message,'errors'=>$errors));
	exit();
}

if ($permission_id && !current_user_may(dekanat_permission::permission_name($permission_id))) {
	$status = 'ERROR';
	$message = 'Ваша учетная запись не имеет права вносить изменения в этой таблице!';
	echo json_encode(array('status'=>$status,'row'=>$row,'message'=>$message,'errors'=>$errors));
	exit();
}

$key = sql_table_key($table);
if (is_array($key)) {
	// sql_table_key может возвращать составные primary ключи в массиве! 
	// такие таблицы сейчас обнолять не умеем...
	$status = 'ERROR';
	$message = 'Составной primary_key! Не могу изменять данные!';
	echo json_encode(array('status'=>$status,'row'=>$row,'message'=>$message,'errors'=>$errors));
	exit();
}

if ($method == 'del') {
	$value = (array_key_exists($key,$_POST)) ? $_POST[$key] : '';
	global $mysqli;
	$sql = "DELETE FROM $table WHERE $key = '$value' ";
	$result = $mysqli->query($sql);
	if ($result === false) {
		$status = 'ERROR';
		$message = $mysqli->error;
		echo json_encode(array('status'=>$status,'row'=>$row,'message'=>$message,'errors'=>$errors));
		exit();
	}

	$cnt_affected = sql_affected_rows();
	if ($cnt_affected) {
		$status = 'OK';
		$message = 'Удалено строк: '.$cnt_affected;
	} else {
		$status = 'ERROR';
		$message = 'Ничего не было удалено!';
	}
	echo json_encode(array('status'=>$status,'row'=>$row,'message'=>$message,'errors'=>$errors));
	exit();
} else {
	if (array_key_exists($key,$_POST)) {
		if ($method == 'saveAs' || $_POST[$key]=='') {
			unset($_REQUEST[$key]);
			unset($_POST[$key]);
		}
	}
	// проверки на уникальность записей
	if ($table == 'predmet') {
		$predmet_kod = request_val('predmet_kod','');
		$predmet_name = request_val('predmet_name','');
		$w = "predmet_name='$predmet_name' and predmet_kod<>'$predmet_kod' ";
		$has = sql_get_value("count(*)",$table,$w);
		if ($has) {
			$status = 'ERROR';
			$message = 'Такой предмет уже существует!';
			echo json_encode(array('status'=>$status,'row'=>$row,'message'=>$message,'errors'=>$errors));
			exit();
		}
	}

	if ($table == 'uch_plan') {
		$semestr_all_cnt = request_numeric_val('semestr_all_cnt','');
		$semestr_sess_finished = request_numeric_val('semestr_sess_finished','');
		if (is_numeric($semestr_all_cnt) && is_numeric($semestr_sess_finished)) {
			if ($semestr_sess_finished > $semestr_all_cnt) {
				$status = 'ERROR';
				$message = 'Общее число семестров меньше номера завершенного!';
				echo json_encode(array('status'=>$status,'row'=>$row,'message'=>$message,'errors'=>$errors));
				exit();
			}
		}
	}

	/*
	пример:
	if ($table == 'mytable') {
		$w = '';
		$has = sql_get_value("count(*)",$table,$w);
		if ($has) {
			$status = 'ERROR';
			$message = 'Такая запись уже заведена!';
			echo json_encode(array('status'=>$status,'row'=>$row,'message'=>$message,'errors'=>$errors));
			exit();
		}
	}
	*/
	// конец проверки на уникальность записей

	$result = save_table::save($table);
	if ($result['saved']) {
		$id = $result['key'];
		$table = (sql_table_exists("v_".$table) && $table != 'stud_hotel') ? "v_".$table : $table;
		$sql = "SELECT * FROM $table WHERE $key='$id' ";
		$res = sql_query($sql);
		$row = sql_row_fix_numeric($res);
		$status = 'OK';
		$message = $result['message'];
	} else {
		$status = 'ERROR';
		if ($result['errors']) {
			foreach ($result['errors'] as $i=>$v) {
				foreach ($v as $col=>$message) {
					$result['message'] .= "\n".$message;
					$errors[]=$col;
				}
			}
		}
		$message = $result['message'];
	}

	echo json_encode(array('status'=>$status,'row'=>$row,'message'=>$message,'errors'=>$errors));
	exit();
}

