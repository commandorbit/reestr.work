<?php 
error_reporting(E_ALL | E_STRICT);
$cnt_iters = isset($_GET['cnt_iters']) ? $_GET['cnt_iters'] : 100;
?>

<form method="get">
	<input type="text" name="cnt_iters" value="<?=$cnt_iters?>">
	<input type="submit" name="go" value="GO">
</form>

<?
if (isset($_GET['go'])) {
	$start_time = microtime(true);
	for ($i=0; $i<$cnt_iters; $i++) {
		$p = passgen();
		if (strlen($p) != 7) echo '<h1>bad password='.$p.'</h1>';
		//$p_crypt = password_hash($p,PASSWORD_DEFAULT);
		$p_crypt = crypt(passgen(),PASSWORD_DEFAULT);
		
		if (strlen($p_crypt) < 4 || strstr($p_crypt,"*") || strlen($p_crypt) > 100) {
			echo '<h1>password='.$p.'<br>';
			echo 'was crypted='.$p_crypt.'</h1>';
		}
		if ($i%($cnt_iters/10)==0) echo $i.' iters done.............<br>';

	}
	echo 'Время запроса <b>'. (int)((microtime(true)-$start_time)*1000)/1000 .'</b> сек.<br>';
}

?>

<?php
function passgen($length = 7,$strength = 0) {
	$vowels = 'aeuy';
	$consonants = 'bdghjmnpqrstvz';
	if ($strength > 0) $vowels .= '23456789';
	if ($strength > 1) $consonants .= 'BDGHJLMNPQRSTVWXZ';
	if ($strength > 2) $vowels .= "AEUY";
	if ($strength > 3) $vowels .= '@#%!';
	$password = '';
	$alt = time() % 2;
	for ($i = 0; $i < $length; $i++) {
		if ($alt == 1) {
			$password .= $consonants[(rand() % strlen($consonants))];
			$alt = 0;
		} else {
			$password .= $vowels[(rand() % strlen($vowels))];
			$alt = 1;
		}
	}
	return $password;
}
?>