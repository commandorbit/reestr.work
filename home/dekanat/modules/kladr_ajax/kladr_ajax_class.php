<?php 
header('Content-Type: text/html;');
class kladr_ajax {
	public function get_val($param) {
		return isset($_GET[$param]) ? sql_escape(trim($_GET[$param])) : '';
	}

	public function kladr_db($db = 'kladr') {
		return ($db) ? "`$db`." : "";
	}

	public function gorod(){		
		$gorod=$this->get_val('term');
		$region=$this->get_val('region');
		if ($gorod && $region) {
			if ($pos = mb_strpos($gorod,".")) { // если в первой позиции - игнорирую
				$gorod = mb_substr($gorod,$pos+1);
			}
			$gorod=trim($gorod);
			$limit=100;
			$i=0;
			$data=array();
			if ($i<$limit) {
				$s = " SELECT code, full_name FROM ".$this->kladr_db()."kladr 
				WHERE actual_code=0 and reg_code='$region' and full_name like '$gorod%' order by ord,full_name limit $limit ";
				
				$result = sql_query($s);
				while($row = sql_array($result)) {
					$data[]=array('value'=>$row["full_name"] ,'label'=> $row["full_name"],'data'=>$row["code"]);
					$i++;
				}
			}
			if ($i==0) {
				$s = " SELECT code, full_name FROM ".$this->kladr_db()."kladr  
				WHERE actual_code=0 and reg_code='$region' and not full_name like '$gorod%' and full_name like '%$gorod%' order by ord,full_name limit $limit ";
				$result = sql_query($s);
				while($row = sql_array($result)) {
					$data[]=array('value'=>$row["full_name"] ,'label'=> $row["full_name"],'data'=>$row["code"]);
					$i++;
				}
			}
			// другой регион
			if ($i==0) {
				$s = " SELECT code, kladr.full_name,  kladr_region.name as region_name FROM ".$this->kladr_db()."kladr 
				inner join ".$this->kladr_db()."kladr_region on kladr.reg_code=kladr_region.reg_code  
				WHERE  actual_code=0 and kladr.reg_code<>'$region' and (full_name like '$gorod%' or full_name like '% $gorod%') order by kladr.ord limit $limit ";
				$result = sql_query($s);
				while($row = sql_array($result)) {	
					$data[]=array('value'=>$row["full_name"] ,'label'=> $row["full_name"] ."\t" . $row["region_name"],'data'=>$row["code"]);
					$i++;
				}
			}
			echo json_encode($data);
		}
	}
	
	public function street(){
		$street=$this->get_val('term');
		$gorod_code=$this->get_val('gorod_code');
		if ($street && $gorod_code) {
			$limit=100;
			$i=0;
			$data = array();
			if ($i<$limit) {
				$s = " SELECT  street_code, name  FROM ".$this->kladr_db()."kladr_street  WHERE actual_code=0 and code='$gorod_code' and name like '$street%' order by name limit $limit ";
				$result = sql_query($s);
				while($row = sql_array($result)) {
					$data[]=array('value'=>$row["name"] ,'label'=> $row["name"],'data'=>$row['street_code']);
					$i++;
				}
			}
			if ($i==0) {
				$s = " SELECT  street_code, name  FROM ".$this->kladr_db()."kladr_street  WHERE actual_code=0 and code='$gorod_code' 
				and name not like '$street%' and (name like '% $street%' or name like '%.$street%') order by name limit $limit ";
				$result = sql_query($s);
				while($row = sql_array($result)) {
					$data[]=array('value'=>$row["name"] ,'label'=> $row["name"],'data'=>$row['street_code']);
					$i++;
				}
			}
			echo json_encode($data);
		}
	}
	
	public function index(){
		$gorod_code = $this->get_val('gorod_code');
		$street_code = $this->get_val('street_code');
		if ($gorod_code && $street_code) {
			
			$dom = iconv( "windows-1251", "utf-8", $this->get_val('dom'));
			$int_dom = (int)$dom;
			$korpus = $this->get_val('korpus');
			$index = '';
			$dom_chet = ($int_dom%2 == 0) ? 'Ч' : 'Н';
			
			$found = false;
			$s = " SELECT name,`index` FROM ".$this->kladr_db()."kladr_doma WHERE code='$gorod_code' and street_code='$street_code' order by ORD";
			$result = sql_query($s);
			while (!$found && $row = sql_array($result)) {
				$nn = explode(",",$row['name']);		
				for ($i=0; !$found and $i<count($nn);$i++) {
					$s = $nn[$i];
					$first = mb_substr($s,0,1);
					if ($first=='Ч' or  $first=='Н') { // Четный или нечетный инетрвал
						$found=($first==$dom_chet and $this->dom_in_spec_interval($int_dom , $s)) ;
					} else if (strpos($s,"-")>0) { //Интервал без указания четности
						$int=explode("-",$s);
						if (count($int)==2) { //иначе ошибка
							$found=($int_dom>=$int[0] and $int_dom<=$int[1]) ;
						}
					} elseif (is_numeric($s)) { //Дом задан числом
						$found = ($int_dom == (int) $s);	// в приведение типов необходимостит нет
					} else {	//Дом задан через дробь с троением или корпусом
						$found= ($dom==$s or $dom .'к' . $korpus == $s);
					}
				}
				if ($found) $index=$row['index'];
			}
			if (!$found) {
				$s = " SELECT `index` FROM ".$this->kladr_db()."kladr_street WHERE actual_code=0 and code='$gorod_code' and street_code='$street_code' ";
				$result = sql_query($s);
				if ($row = sql_array($result)) {
					$index = $row['index'];
				}
			}
			echo $index;
		}
	}
	
	private function dom_in_spec_interval($dom,$interval) {
		if (strlen($interval)==1) {
			return true;
		} else {
			$int=explode("-",mb_substr($interval,2,mb_strlen($interval)-3));  //интервал
			if (count($int)<2) {
				return false; //error
			} else {
				return $dom>=$int[0] and $dom<=$int[1];
			}
		}
	}
}
?>