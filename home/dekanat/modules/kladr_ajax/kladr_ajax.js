function myAutoComplete(elem,options,extra) {
	var url=options.url;
	options.source = new _Extra(url,extra).callback;
	elem.autocomplete(options);
	
	function _Extra(url,extra) {
		self=this;
		self.xhr=null;
		self.extra=extra;
		self.callback=cbAutoComplete;
		function cbAutoComplete(request,response) {
			var data = {term:request.term};
			for(var p in extra) {
				var elem=extra[p];
				if (typeof  elem === 'function')
					data[p]=elem();
				else if  (elem.tagName && (elem.tagName=='INPUT' || elem.tagName=='SELECT'))
					data[p]=elem.value;
				else 
					data[p]=extra[p];
			}
			//console.log(data);
			ajax(data,response);
		}	
		function ajax ( data, response ) {
			if ( self.xhr ) {
				self.xhr.abort();
			}
			self.xhr = $.ajax({
				url: url,
				data: data,
				crossDomain: true,
    			dataType: 'json',
				success: function( data ) {
					response( data );
				},
				error: function() {
					response( [] );
				}
			});
		};	
	}	
}

function setKladr(prefix,hasStreet) {
	/*
		Fields on form:
		region
		gorod_id
		gorod_code_id
		street
		street_code_id
		all may be with prefix fact -> fact_region, fact_gorod_id
	*/
	var url='//dekanat.unecon.ru/modules/kladr_ajax/kladr_ajax.php';
	
	var byId = function(id) {
		return document.getElementById(id);
	};

	var region_id = obj_id('region',prefix);
	var gorod_id = obj_id('gorod',prefix);
	var gorod_code_id = obj_id('gorod_code',prefix);
	var street_id = obj_id('street',prefix);
	var street_code_id = obj_id('street_code',prefix);
	var dom_id = obj_id('dom',prefix);
	var korp_id = obj_id('korp',prefix);
	var index_id = obj_id('index',prefix);

	$('#'+region_id).change(function(){
		if ($(this).val()=='') $('#'+index_id).val('');});
	$('#'+gorod_id).change(function(){
		if ($(this).val()=='') {
			$('#'+gorod_code_id).val('');
			$('#'+index_id).val('');
		}
		kladrSetPostIndex(prefix);
	});
	$('#'+street_id).change(function(){
		if ($(this).val()=='') {
			$('#'+street_code_id).val('');
			$('#'+index_id).val('');
		}
		kladrSetPostIndex(prefix);
	});

	myAutoComplete($('#'+gorod_id),{
		autoFocus: true,
		minLength: 1,
		url: url, 
		select: function(event,ui) {
			var reg_code = parseInt(ui.item.data.substr(0,2),10);
			if (reg_code != $('#'+region_id).val()) {
				$('#'+region_id).val(reg_code);
			}
			$('#'+gorod_code_id).val(ui.item.data);
			$('#'+street_code_id).val('');
			kladrSetPostIndex(prefix);
		}
	}, {func: 'gorod', region: byId(region_id) });
	if (hasStreet) {
		myAutoComplete($('#'+street_id),{
			autoFocus: true, 
			minLength: 1, 
			url: url, 
			select: function(event,ui) {
				$('#'+street_code_id).val(ui.item.data);
				kladrSetPostIndex(prefix);
			}
		}, {func: 'street', gorod_code: byId(gorod_code_id)});
		$('#'+dom_id).change(function(){kladrSetPostIndex(prefix);});
		$('#'+korp_id).change(function(){kladrSetPostIndex(prefix);});
	}
}

function obj_id(name,prefix) {
	var id = prefix + name + '_id';
	if ($('#'+id).length == 0) {
		id = 'id_' + prefix + name;
		if ($('#'+id).length == 0) {
			if (name == 'gorod_code') id = 'id_' + prefix + 'gorod_code_hidden';
			else if (name == 'street_code') id = 'id_' + prefix + 'street_code_hidden';
		}
	}
	return id;
}

function kladrSetPostIndex(prefix) {
	var gorod_code = $('#'+obj_id('gorod_code',prefix)).val();
	var street_code = $('#'+obj_id('street_code',prefix)).val();
	var dom = $('#'+obj_id('dom',prefix)).val();
	var korp = $('#'+obj_id('korp',prefix)).val();
	var server = "//dekanat.unecon.ru/modules/kladr_ajax/kladr_ajax.php";
	$.ajax({
		url: server+"?func=index&gorod_code="+gorod_code+"&street_code="+street_code+"&dom="+dom+"&korpus="+korp,
	}).done(function(data) {
		if (data) {
			var index_id = obj_id('index',prefix);
			$('#'+index_id).val(data);
		}
	});
}