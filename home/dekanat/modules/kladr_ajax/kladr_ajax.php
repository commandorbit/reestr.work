<?php 
error_reporting(E_ALL | E_STRICT);
require_once $_SERVER['DOCUMENT_ROOT'].'/modules/error_init.php';
require_once $_SERVER['DOCUMENT_ROOT'].'/modules/sql.php';
require_once $_SERVER['DOCUMENT_ROOT'].'/modules/sql_connect.php';
require_once $_SERVER['DOCUMENT_ROOT'].'/modules/functions_shared.php';
require_once $_SERVER['DOCUMENT_ROOT'].'/modules/kladr_ajax/kladr_ajax_class_v2.php';

header("Access-Control-Allow-Origin: * ");
$db=kladr_connect();
sql_set_charset('UTF8');
mb_internal_encoding("UTF-8");

$kladr_ajax = new kladr_ajax();
$function = $kladr_ajax->get_val('func');

if ($function) {
	if (method_exists($kladr_ajax,$function)) call_user_func(array($kladr_ajax,$function));
	else echo 'Неизвестный метод: ' . $function;
}
?>