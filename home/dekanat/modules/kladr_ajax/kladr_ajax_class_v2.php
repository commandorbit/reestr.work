<?php 
header('Content-Type: text/html;');
class kladr_ajax {
	public function get_val($param) { 
		$val = isset($_GET[$param]) ? trim($_GET[$param]) : '';
		$val = preg_replace('/[^A-zА-яЁё0-9%\/ -.]/u','',$val);
		$val = str_replace("ё","е",$val);
		return sql_escape($val);
	}

	public function kladr_db($db = 'kladr') {
		return "`$db`";
	}

	private function _replace($str,$finds) {
		// после каждой итерации trim обязательно!
		$str = trim(str_replace('.','. ',$str));
		foreach ($finds as $find) {
			// только с начала строки
			// чтобы при этом "Великий НовГОРОД гор." не стал "Великий Новгор."
			if (mb_substr($str,0,strlen($find)) == $find) {
				$str = trim(str_ireplace($find.' ','',$str));
				// "деревня Кудрово" => "Кудрово";
				// "улица Жукова" => "Жукова"
			}
		}
		foreach ($finds as $find) {
			// начало или конец строки значения не имеют
			$str = trim(str_ireplace($find.'.','',$str));
			// "дер. Кудрово" => "Кудрово"
			// "ул. Чехова" => "Чехова"
		}
		foreach ($finds as $find) {
			// только с конца строки
			// чтобы при этом "Нижнее ГОРОДище" не стало "НижнееИще"
			if (mb_substr($str,-strlen($find)) == $find) {
				$str = trim(str_ireplace(' '.$find,'',$str));
				// "Санкт-Петербург г" => "Санкт-Петербург"
				// "Садовая улица" => "Садовая"
			}
		}
		return $str;
	}

	private function gorod_replace($gorod) {
		// важно!!! более длинные замены должны стоять вначале. т.е. город->гор->г
		$a = [
			 'город'
			,'гор'
			,'г'
			,'деревня'
			,'дер'
			,'д'
			,'пгт'
			,'рп'
			,'поселок'
			,'пос'
			,'станица'
			,'ст-ца'
			,'село'
			,'с'
		];
		$gorod = $this->_replace($gorod,$a);
		return $gorod;
	}

	private function street_replace($street) {
		// важно!!! более длинные замены должны стоять вначале. 
		//т.е. проспект->просп->пр
		$a = [
			 'улица'
			,'ул'
			,'бульвар'
			,'б-р'
			,'набережная'
			,'наб'
			,'шоссе'
			,'ш'
			,'проезд'
			,'п-д'
			,'проспект'
			,'просп'
			,'пр-кт'
			,'пр-т'
			,'пр'
			,'переулок'
			,'пер'
			,'площадь'
			,'пл'
			,'квартал'
			,'кв'
			,'микрорайон'
			,'м-н'
			,'мкр'
			,'тупик'
			,'туп'
		];
		$street = $this->_replace($street,$a);
		return $street;
	}

	public function gorod() {
		$db = $this->kladr_db();
		$gorod = $this->get_val('term');
		$region = $this->get_val('region');

		if ($gorod && $region) {
			if ($pos = mb_strpos($gorod,".")) { // если в первой позиции - игнорирую
				$gorod = mb_substr($gorod,$pos+1);
			}
			$gorod = $this->gorod_replace($gorod);
			$gorod = trim($gorod);
			$limit=100;
			$i=0;
			$data=array();

			$gg = explode(" ",$gorod);
			array_unshift($gg,$gorod); // город полностью на первое место
			$added = [];
			foreach ($gg as $g) {
				$g = trim($g);
				if ($i<$limit) {
					$s = " SELECT code, full_name 
					FROM $db.kladr 
					WHERE actual_code=0 
						and reg_code='$region' 
						and REPLACE(full_name,'ё','е') like '$g%' 
					ORDER BY ord, full_name limit $limit ";
					$result = sql_query($s);
					while($row = sql_array($result)) {
						if (!in_array($row["code"],$added)) {
							$added[] = $row["code"];
							$data[]=array('value'=>$row["full_name"] ,'label'=> $row["full_name"],'data'=>$row["code"]);
							$i++;
						}
					}
				}
				if ($i==0) {
					$s = " SELECT code, full_name 
					FROM $db.kladr  
					WHERE actual_code=0 
						and reg_code='$region' 
						and (REPLACE(full_name,'ё','е') like '$g%' or REPLACE(full_name,'ё','е') like '%$g%')  
					ORDER BY ord, full_name limit $limit ";
					$result = sql_query($s);
					while($row = sql_array($result)) {
						if (!in_array($row["code"],$added)) {
							$added[] = $row["code"];
							$data[]=array('value'=>$row["full_name"] ,'label'=> $row["full_name"],'data'=>$row["code"]);
							$i++;
						}
					}
				}
				// другой регион
				if ($i==0) {
					$s = " SELECT code, kladr.full_name, kladr_region.name as region_name 
					FROM $db.kladr 
					INNER JOIN $db.kladr_region on kladr.reg_code=kladr_region.reg_code  
					WHERE actual_code=0 
						and kladr.reg_code<>'$region' 
						and (REPLACE(full_name,'ё','е') like '$g%' or REPLACE(full_name,'ё','е') like '% $g%') 
					ORDER BY kladr.ord limit $limit ";
					$result = sql_query($s);
					while($row = sql_array($result)) {
						if (!in_array($row["code"],$added)) {
							$added[] = $row["code"];
							$data[]=array('value'=>$row["full_name"] ,'label'=> $row["full_name"] ."\t" . $row["region_name"],'data'=>$row["code"]);
							$i++;
						}
					}
				}
			}
			echo json_encode($data);
		}
	}
	
	public function street(){
		$db = $this->kladr_db();
		$street = $this->get_val('term');
		$gorod_code = $this->get_val('gorod_code');

		if ($street && $gorod_code) {
			$street = $this->street_replace($street);
			$street = trim($street);

			$limit=100;
			$i=0;
			$data = array();

			$sts = explode(" ",$street);
			array_unshift($sts,$street); // улицу полностью на первое место
			$added = [];
			foreach ($sts as $st) {
				$st = trim($st);
				if ($i<$limit) {
					$s = " SELECT  street_code, name  
						FROM $db.kladr_street  
						WHERE actual_code=0 
							and code='$gorod_code' 
							and REPLACE(name,'ё','е') like '$st%' 
						ORDER BY name limit $limit ";
					$result = sql_query($s);
					while($row = sql_array($result)) {
						if (!in_array($row["street_code"],$added)) {
							$added[] = $row["street_code"];
							$data[]=array('value'=>$row["name"] ,'label'=> $row["name"],'data'=>$row['street_code']);
							$i++;
						}
					}
				}
				if ($i==0) {
					$s = " SELECT  street_code, name 
						FROM $db.kladr_street  
						WHERE actual_code=0 
							and code='$gorod_code' 
							and name not like '$st%' 
							and (REPLACE(name,'ё','е') like '% $st%' or REPLACE(name,'ё','е') like '%.$st%') 
						ORDER BY name limit $limit ";
					$result = sql_query($s);
					while($row = sql_array($result)) {
						if (!in_array($row["street_code"],$added)) {
							$added[] = $row["street_code"];
							$data[]=array('value'=>$row["name"] ,'label'=> $row["name"],'data'=>$row['street_code']);
							$i++;
						}
					}
				}
			}
			echo json_encode($data);
		}
	}
	
	public function index(){
		$db = $this->kladr_db();
		$gorod_code = $this->get_val('gorod_code');
		$street_code = $this->get_val('street_code');

		if ($gorod_code && $street_code) {
			$dom = $this->get_val('dom');
			$int_dom = (int)$dom;
			$korpus = $this->get_val('korpus');
			$index = '';
			$dom_chet = ($int_dom%2 == 0) ? 'Ч' : 'Н';
			
			$found = false;
			$s = " SELECT name,`index` 
			FROM $db.kladr_doma 
			WHERE code='$gorod_code' 
				and street_code='$street_code' 
			ORDER BY ORD";

			$result = sql_query($s);
			while (!$found && $row = sql_array($result)) {
				$nn = explode(",",$row['name']);		
				for ($i=0; !$found and $i<count($nn);$i++) {
					$s = $nn[$i];
					$first = mb_substr($s,0,1);
					if ($first=='Ч' or  $first=='Н') { // Четный или нечетный инетрвал
						$found=($first==$dom_chet and $this->dom_in_spec_interval($int_dom , $s)) ;
					} else if (strpos($s,"-")>0) { //Интервал без указания четности
						$int=explode("-",$s);
						if (count($int)==2) { //иначе ошибка
							$found=($int_dom>=$int[0] and $int_dom<=$int[1]) ;
						}
					} elseif (is_numeric($s)) { //Дом задан числом
						$found = ($int_dom == (int) $s);	// в приведение типов необходимостит нет
					} else {	//Дом задан через дробь с троением или корпусом
						$found= ($dom==$s or $dom .'к' . $korpus == $s);
					}
				}
				if ($found) $index=$row['index'];
			}
			if (!$found) {
				$s = " SELECT `index` 
					FROM $db.kladr_street 
					WHERE actual_code=0 
						and code='$gorod_code' 
						and street_code='$street_code' ";
				$result = sql_query($s);
				if ($row = sql_array($result)) {
					$index = $row['index'];
				}
			}
			echo $index;
		}
	}
	
	private function dom_in_spec_interval($dom,$interval) {
		if (strlen($interval)==1) {
			return true;
		} else {
			$int=explode("-",mb_substr($interval,2,mb_strlen($interval)-3));  //интервал
			if (count($int)<2) {
				return false; //error
			} else {
				return $dom>=$int[0] and $dom<=$int[1];
			}
		}
	}
}
?>