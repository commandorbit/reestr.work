<?php

// ������ session_start()

function start_session() {
	$use_cookies = ini_get('session.use_cookies');
	if ($use_cookies && isset($_COOKIE['PHPSESSID'])) {
	    $sessid = $_COOKIE['PHPSESSID'];
	} elseif (!$use_cookies && isset($_GET['PHPSESSID'])) {
	    $sessid = $_GET['PHPSESSID'];
	} else {
	    session_start();
	    return false;
	}
	if (!preg_match('/^[a-z0-9]{'.default_sessid_len().'}$/', $sessid)) {
		$error = '';
		if (isset($_SESSION)) {
			$error .= "SESSION: <br>";
			foreach ($_SESSION as $key=>$value) $error .= " $key = $value <br>";
		}
		$error .= "SESS_ID: $sessid<br>";
		$error .= "NEED SESS_ID LENGTH: ".default_sessid_len()."<br>";
		send_dim($error);
		die('Bad Session Id. Please clean your Cookies!');
	}
	session_start();
	return true;
}

function default_sessid_len() {
	$algoritm = ini_get('session.session.hash_function');
	// algoritm = 0 - md5 (128)
	// algoritm = 1 - sha1 (160)
	$bits_per_char = ini_get('session.hash_bits_per_character');
	$algoritm_digest = ($algoritm == 1) ? 160 : 128;
	$len = ceil($algoritm_digest/$bits_per_char);
	return $len;
}

?>