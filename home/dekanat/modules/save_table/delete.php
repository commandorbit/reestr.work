<?php
include $_SERVER['DOCUMENT_ROOT'].'/init.php';
include $_SERVER['DOCUMENT_ROOT'].'/modules/save_table/save_table.php';
if (!current_user_kod()) exit();

sql_set_charset('utf8');

$db = request_val('db',"dekanat");
$table = request_val('table',false);
$primary_keys = table_primary_keys($table);
$value = request_val('value',false);
$a = '';

// из таблиц где составной primary key не удаляем
if ($table && count($primary_keys)==1 && $value) {
	$field = $primary_keys[0];
	$status = reference_status($table,$field,$value);
	if (!$status) {
		sql_query("DELETE FROM `$db`.$table WHERE $field='$value'");
		$a = '<p class="good_msg">Запись удалена!</p>';
	} else {
		$s = '<table>';
		foreach ($status as $child_table=>$cnt) {
			$s .= '<tr><td align="right">'.$child_table.': </td><td> '.$cnt . ' записей</td></tr>';
		}
		$s .= '</table>';
		$a = '<p class="bad_msg">Запись удалить невозможно!<br>В этих таблицах присутствуют зависимые записи: <br>'.$s.'<br>Код: '.$value.'</p>';
	}
}
echo $a;


function reference_status($table,$field,$value) {
	// возвращает массив с указанием таблиц и количества записей
	$sql="SELECT TABLE_NAME, CONSTRAINT_SCHEMA, CONSTRAINT_NAME 
		FROM information_schema.REFERENTIAL_CONSTRAINTS 
		WHERE REFERENCED_TABLE_NAME ='$table' ";
	$refs = sql_rows($sql);
	$status = array();
	foreach ($refs as $ref) {
		$child = sql_get_array('information_schema.KEY_COLUMN_USAGE',
			"CONSTRAINT_NAME='".$ref['CONSTRAINT_NAME']."' AND CONSTRAINT_SCHEMA='".$ref['CONSTRAINT_SCHEMA']."' ");
		$child_db = $child['TABLE_SCHEMA'];
		$child_table = $child['TABLE_NAME'];
		$child_field = $child['COLUMN_NAME'];
		
		$cnt = sql_get_value("count(`$child_field`)","`$child_db`.$child_table", "$child_field='$value'");
		if ($cnt) $status[$child_table] = $cnt;
	}

	return $status;
}

?>