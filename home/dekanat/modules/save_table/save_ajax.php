<?php
require_once($_SERVER['DOCUMENT_ROOT'] . '/init.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/modules/save_table/save_table.php');
sql_set_charset('utf8');

$tables = array('uch_plan_punkt','cd');
$table = request_val('table','');
$primary_keys = table_primary_keys($table);
$keys = array();

foreach ($primary_keys as $key) {
	if (isset($_POST[$key])) {
		$keys[$key] = $_POST[$key];
		if ($keys[$key]=='new') $keys[$key] = '';//sql_get_autoincrement($table);
		// удаляется чтобы избежать дублирования
		unset($_POST[$key]);
	}
}

$a = array();
if (in_array($table,$tables)) {
	$has_error = false;
	if ($table=='cd') {
		if (!empty($_POST['comp_test_id'])) $_POST['is_comp_test']=1;
	}
	if ($table=='uch_plan_punkt') {
		$upp_kod = request_numeric_val('uch_plan_punkt_kod','');	
		$up_kod = request_numeric_val('uch_plan_kod','');
		$cicl_kod = request_numeric_val('upp_cicl_kod','');
		$cicl_is_diplom = sql_get_value("is_diplom","s_upp_cicl","cicl_kod='$cicl_kod'");
		
		if ($cicl_is_diplom) {
			$is_bak_on_spec = sql_get_value("may_diplom_bakalavr","uch_plan","uch_plan_kod='$up_kod'");
			$cnt = sql_fetch_value("SELECT count(*) FROM uch_plan_punkt upp 
			INNER JOIN s_upp_cicl cicl ON upp.upp_cicl_kod=cicl.cicl_kod
			WHERE cicl.is_diplom=1 
				AND upp.uch_plan_punkt_kod<>'$upp_kod' AND upp.uch_plan_kod='$up_kod'");

			if ($cnt > 0 && !$is_bak_on_spec) {
				$has_error = true;
				$msg = 'Может быть только 1 дипломная работа в этом учебном плане!';
			}
			if ($cnt > 1 && $is_bak_on_spec) {
				$has_error = true;
				$msg = 'Может быть только 2 дипломных работы в этом учебном плане!';
			}
		}
	}

	if ($has_error) {
		$a['error_fields'] = [];
		$a['msg'] = '<p class="bad_msg">'.$msg.'</p>';
	} else {
		$status = save_table($table,$keys);
		if ($status['saved']) {
			$a['msg'] = '<p class="good_msg">Данные успешно сохранены!</p>';
		} else {
			$a['msg'] = '<p class="bad_msg">'.iconv("cp1251","utf-8",implode("<br>",$status['errors'])).'</p>';
			$a['error_fields'] = $status['error_fields'];
		}
	}
}
echo json_encode($a);

?>