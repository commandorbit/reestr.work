<?php

class save_table{
	public static function save($table){
		$status = array(
			'saved'=>false,
			'table'=>$table,
			'key'=>'',
			'sql'=>'',
			'errors'=>array(),
			'message'=>''
		);
		$data = self::request2array();
		$cols = sql_rows("SHOW COLUMNS FROM $table");
		foreach ($data as $nRow=>$v) {
			if ($errors = self::dataErrors($table,$cols,$v)) $status['errors'][]=$errors;
		}
		if ($status['errors']) {
			$status['message'] = '������ � ������!';
		} else {
			foreach ($data as $nRow=>$v) {
				$s=array();
				foreach ($cols as $col) {
					$field = $col['Field'];
					if (array_key_exists($field,$v)) {
						$value = $v[$field];
						if (self::col_has_null($col) && self::val_is_empty($v,$field)) {
							$s[]="`$field`=null ";
						} elseif (self::col_is_date($col)) {
							$s[]="`$field`='".self::date_to_format($value,"Y-m-d")."'";
						} elseif (self::col_is_binary($col)) {
							$s[]="`$field`='".hex2bin($value)."'";
						} else {
							$s[]="`$field`='$value' ";
						}
					}	
				}

				$update = self::primary_key_exists($table,$v);
				if ($update) {
					$sql = "UPDATE $table SET ".implode(",",$s)." WHERE true ".self::where($table,$v);
				} else {
					$sql = "INSERT INTO $table SET ".implode(",",$s);
				}
				$status['sql']=$sql;
				if (sql_query($sql)) {
					if ($update) {
						$pk = sql_table_key($table);
						if (!is_array($pk)) $status['key'] = $v[$pk];
					} else {
						$status['key'] = sql_last_id();
					}
					$status['message'] = '������ ������� ���������!';
					$status['saved'] = true;
				} else break;
			}
		}
		return $status;
	}

	public static function status_errors_to_str($status,$delimiter = '<br>') {
		$str = '';
		if (is_array($status) && array_key_exists('errors',$status)) {
			foreach ($status['errors'] as $nRow=>$v) {
				foreach ($v as $nRow=>$error) $str .= $error.$delimiter;
			}
		}
		return $str;
	}

	private static function primary_key_exists($table,$row) {
		if ($where = self::where($table,$row)) return sql_get_value("count(*)",$table,"true ".$where);
		else return false;
	}

	private static function where($table,$row) {
		$pk = sql_table_key($table);
		$where = '';
		if (is_array($pk)) {
			foreach ($pk as $key) {
				if (array_key_exists($key,$row)) {
					$v = $row[$key];
					$where.= " AND $key='$v' ";
				}
			}
		} else {
			if (array_key_exists($pk,$row)) {
				$v = $row[$pk];
				$where.= " AND $pk='$v' ";
			}
		}
		return $where;
	}

	private static function dataErrors($table,$cols,$row){
		$errors = array();
		foreach ($cols as $col) {
			$field = $col['Field'];
			if ($col['Type'] !=='timestamp') {
				if (!self::col_has_null($col) && self::val_is_empty($row,$field) && !self::col_has_autoincrement($col)) {
					if (self::primary_key_exists($table,$row) && !self::col_in_request($row,$field));
					elseif (!self::col_has_default($col)) $errors[$field] = '�� ��������� ������������ ���� '.$field.'!';
				} elseif (!self::val_is_empty($row,$field)) {
					if (self::col_is_numeric($col) && !is_numeric($row[$col['Field']])) {
						$errors[$field] = '���� '.$field.' ����� ��������� ������ �������� ��������!';
					} elseif (self::col_is_date($col)) {
						$bad_date = false;
						$d = self::date_to_format($row[$field],"Y-m-d");
						if (!$d) $bad_date = true;
						else {
							$month = substr($d,5,2);
							$day = substr($d,8,2);
							$year = substr($d,0,4);
							if (!checkdate($month, $day, $year)) $bad_date = true;
						}
						if ($bad_date) {
							$errors[$field] = '������������ �������� ���� � ���� '.$field.'!';
						}
					}
				}
			}
		}
		return $errors;
	}

	private static function request2array() {
		$data = array();
		$nRow = 0;
		foreach ($_POST as $key=>$v) {
			if (is_array($v)) {
				foreach ($v as $value) {
					$data[$nRow][$key]=sql_escape(trim($value));
					$nRow++;
				}
			} else {
				$data[$nRow][$key]=sql_escape(trim($v));
			}
		}
		return $data;
	}

	private static function col_has_default($col) {
		return ($col['Default']===NULL) ? 0 : 1;
	}

	private static function col_has_null($col) {
		return ($col['Null']=='NO') ? 0 : 1;
	}

	private static function col_in_request($row,$col_name) {
		return (array_key_exists($col_name,$row)) ? true : false;
	}

	private static function val_is_empty($row,$col_name) {
		$is = (self::col_in_request($row,$col_name)) ? false : true;
		if (!$is) {
			if ($row[$col_name]=='') $is = true;
			elseif ($row[$col_name]=='null') $is = true;
			elseif ($row[$col_name]=='00.00.0000') $is = true;
		}
		return $is;
	}

	private static function col_is_numeric($col) {
		return (strstr($col['Type'],"int")) ? 1 : 0;
	}

	private static function col_has_autoincrement($col) {
		return ($col['Extra']=='auto_increment') ? 1 : 0;
	}

	private static function col_is_character($col) {
		return (strstr($col['Type'],"char")) ? 1 : 0;
	}

	private static function col_is_date($col) {
		return (strstr($col['Type'],"date") && !self::col_is_datetime($col)) ? 1 : 0;
	}

	private static function col_is_datetime($col) {
		return (strstr($col['Type'],"datetime")) ? 1 : 0;
	}

	private static function col_is_binary($col) {
		return (strstr($col['Type'],"binary")) ? 1 : 0;
	}

	private static function date_to_format($date,$format) {
		$date_formatted = '';
		if ($date) {
			$date = new DateTime($date);
			$date_formatted = $date->format($format);
		}
		return $date_formatted;
	}
}