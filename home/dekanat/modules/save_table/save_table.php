<?php 
function table_primary_keys($table) {
	// ���������� ������ primary keys
	$r = sql_query("SHOW COLUMNS FROM $table");
	$keys = array();
	while($row = sql_array($r)) {
		if ($row['Key']=='PRI') $keys[] = $row['Field'];
	}
	return $keys;
}

function table_primary_keys_exists($table,$kods) {
	// ���������� ������� ������ � ������� �� primary ������
	$w = array();
	foreach ($kods as $key=>$value) {
		if ($value != '') $w[] = "$key='$value'";
	}
	$sql_w = implode(" and ",$w);
	return ($sql_w) ? sql_get_value("count(*)","$table",$sql_w) : 0;
}

function save_table($table,$kods) {
	// ��������� ������ �� post � ����� ������� ��� kods = ������ primary_key=>value
	// ���������� ������ �� �������� ����������, ���� ��������.
	// ��������, ������������ �� ������� ��� ��������������� ���.
	$convert_dates = 1;
	$p = array();
	$status = array();
	$save = array();
	$errors = array();
	$error_fields = array();
	
	foreach ($_POST as $key=>$value){
		if (!is_array($value)) $p[$key] = trim(sql_escape($value));
	}
	$primary_keys = table_primary_keys($table);
	foreach ($primary_keys as $key) {
		if (!isset($kods[$key])) $errors[] = '�� ������� ����������� primary key ��� �������!';
	}
	foreach ($kods as $key=>$value) {
		if (!in_array($key,$primary_keys)) $errors[] = '������� �������� �� ���������� primary key!';
	}
	$r = sql_query("SHOW COLUMNS FROM $table");
	while ($cols = sql_array($r)) {
		if (isset($p[$cols['Field']])) {
			if ($cols['Null']=='NO' && ($p[$cols['Field']]=='' || $p[$cols['Field']]=='null' || $p[$cols['Field']]=='00.00.0000' || $p[$cols['Field']]=='0000-00-00')) {
				/* ������������ ���� �� ���� ��������� */
				if (strstr($cols['Type'],"char") && $cols['Default']==(string)'') {
					/* ��� ������ � ���� Default ������ ������ */
					$save[] = $cols['Field']."=''";
				} elseif ($cols['Default']) {
					/* ���� ������ Default �������� */
					$save[] = $cols['Field']."='".$cols['Default']."'";
				} else {
					$errors[] = '�� ��������� ������������ ����: '.$cols['Field'];
					$error_fields[] = $cols['Field'];
				}
			} else {
				if ($cols['Type']=='date' && $convert_dates && $p[$cols['Field']]) $value = sql_rus2date($p[$cols['Field']]);
				elseif ($cols['Null']=='YES' && ($p[$cols['Field']]=='null' || $p[$cols['Field']]=='')) $value = "null";
				else $value = "'".$p[$cols['Field']]."'";
				
				$save[] = $cols['Field']."=".$value;
			}
		} elseif ($cols['Null']=='NO' && !in_array($cols['Field'],array_merge($primary_keys,array('nomer_zach'))) && $cols['Default']=='') {
			/* ������������ ���� �� �������� � POST � ��� default value */
			$errors[] = '������������� ���� ��� � POST: '.$cols['Field'];
			$error_fields[] = $cols['Field'];
		}
	}
	if (count($save)>0 && !$errors) {
		$s = implode(", ",$save);
		$w = array();
		if (table_primary_keys_exists($table,$kods)) {
			foreach ($kods as $key=>$value) $w[] = "$key='$value'";
			$sql = "UPDATE $table SET $s WHERE " . implode(" and ",$w);
		} else {
			foreach ($kods as $key=>$value) if ($value) $w[] = "$key='$value'";
			if ($w) $s .= "," . implode(", ",$w);
			$sql = "INSERT INTO $table SET $s ";
		}
		sql_query($sql);
		$status = array('saved'=>true,'errors'=>false,'table'=>$table,'kods'=>$kods,'error_fields'=>$error_fields);
	} elseif ($errors) $status = array('saved'=>false,'errors'=>$errors,'table'=>$table,'kods'=>$kods,'error_fields'=>$error_fields);
	return $status;
}

?>