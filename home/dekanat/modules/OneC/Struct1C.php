<?php
class Struct1C {
    const REGEXP_GUID = '(?P<guid>[0-9a-f-]{36})';
    const FLD_NAME = 'Fld';
    const TBL_NAME = 't';
    const GUID_T = 'guid_t';
    const HEX_N = '_hex_nom';
    const GUID_T_RESURS = '13134203-f60b-11d5-a3c7-0050bae0a776';
    const REF_TYPE = '0x08'; //для субконтно ссылка на Reference??
    public static $mssql = null;
    public static $debug = false;
    public static $_save_struct_file = '';
    protected static $_t = [];
    protected static $_er = [];
    protected static $_t_guids=[];
    protected static $_types = [];
    protected static $_type = ''; //current type parsed;

    
    
    public static function init($mssql) {
        self::$mssql = $mssql;
    //    self::connect_ms();
        self::_init();
    }
	
    public static function save($file) {
        file_put_contents($file."_t", serialize(self::$_t));
        file_put_contents($file."_er", serialize(self::$_er));
    }
    
    public static function load($file) {
		if (file_exists($file."_t") && file_exists($file."_er")) {
			self::$_t = unserialize(file_get_contents($file."_t"));
			self::$_er = unserialize(file_get_contents($file."_er"));
			return true;
		} else {
			return false;
		}
    }
    
    protected static function debug($msg) {
        if (self::$debug) {
            echo $msg;
        }
    }
	
    public static function getInternalStruct() {
        return self::$_t;
    }
	
    public static function getInternalEnums() {
        return self::$_er;
    }    
	
    protected static function _fldIsRef(&$lines,$i) {
        $is_ref = null;
        if (isset($lines[$i+2]) && $lines[$i+2]=='{"Pattern",') {
            $is_ref = (substr($lines[$i+3],0,3)=='{"#') ;
        }
        return $is_ref;
    }
    
    protected static  function _isNewBlock($line,&$matches) {
        // ловим строку вида
        // {13134203-f60b-11d5-a3c7-0050bae0a776,3,
        // это начала блока описания полей определенного  типа ИЛИ подчиненных таблиц
        return preg_match('/^{'.self::REGEXP_GUID.',(?P<cnt>[0-9]+),$/',$line, $matches);
    }

    protected static  function _isNewElement($line,&$matches) {
        // ловим строку вида
        // это название поля или подч таблицы):
        // {0,0,bc2cd979-0bc0-4506-a77f-45f037cd8409},"Учреждение",  
        $found = preg_match('/^{0,0,'.self::REGEXP_GUID.'},"/',$line, $matches);
        if ($found) {
            $matches['name'] = explode('"', $line)[1];
        }
        return $found;
    }
    
    protected static function _parseConfig(&$string,$type,$table_nom) {     
        $lines = explode("\r\n", $string);    
        //self::$_table_rus_name =  "";
        //self::$_table_sql_name = "";
        $matches = [];
        $top_name = '';
       // $i_from = self::_findTableName($lines,$matches);
		$parent = &self::$_t[$type];

		
        for($i=0, $l=count($lines); $i<$l; $i++) {
            $line = $lines[$i];
            if (self::_isNewElement($line, $matches)) {
                if ($top_name) die('New element found not in block');
                $name = $matches['name'];              
                $guid = $matches['guid'];
				if (self::$debug) {
					echo "name:$name guid:$guid <br>\n";
					if ($name == 'ЕПСБУ') {
					//	var_dump( $string);
					//	die();
					}
				}
				
                $el = self::_element(null,$guid);
                $nom = self::$_types[$type][$guid];
                $maxchars = 8 ; 
                $hex = str_pad(dechex($nom), $maxchars, "0", STR_PAD_LEFT);
                $el[self::HEX_N] = '0x' . $hex;
                $el[self::TBL_NAME] = '_' . $type . $nom;
                $parent[$name] = $el;
                $parent = &$parent[$name];
            }
            if (self::_isNewBlock($line,$matches)) {
                $i = self::_parseConfigBlock($parent,$lines,$i,$l,$matches);
            }
        }
    }
	
	protected static function _parseConfig2(&$string,$type) {
		$parent = &self::$_t[$type];
		$data = self::config2array($string);
		
		//echo "<h3>$type</h3>";
	//	var_dump($data);
	//	var_dump($data);// . "<br>";
		self::_parseConfigData($data,$type);
	}
	
	protected static function config2array($string) {
		$string = substr($string,3); //remove BOM
		$string = preg_replace('/'.self::REGEXP_GUID.'/','"$1"',$string); // enquote GUID
		$string= str_replace('}',']',str_replace('{','[',$string)); // change brackets
		$string = self::escape_qoutes($string); // escape quotes in string 
		//$string = str_replace("\r","",$string);
		//$string = str_replace("\n","",$string);
		$data = null;
		try {
			eval('$data = ' . $string . ';');
		} catch(Exception $e) {
		}
		if ($data == null ) die("Cannot set Data!");
		return $data;
	}	
	
	private static function _parseConfigData(&$data,$path) {
		for ($i=0,$l=count($data); $i<$l; $i++) {
			if ($el = self::isDataElement($data[$i])) {				
				$name = $el['NAME'];
				$p = ".$i [El:$name]";
				if (isset($el[self::FLD_NAME]) && $i+1<$l) {
					if (self::isPatternReference($data[$i+1])) {
						$el[self::FLD_NAME] .= 'RRef';	
					}
				}
				$caption = isset($el['CAPTION']) ? $el['CAPTION'] : '';
				$fldname = isset($el[self::FLD_NAME]) ?  $el[self::FLD_NAME] : '';
			 	//echo "$path . $i $name $caption $fldname <br> ";
				//self::_parseConfigData($data[$i],$path.$p);
			} else  if(self::isDataBlock($data[$i])) {
				$guid = $data[$i][0];
				$p = ".$i [Bl#$guid]";
				self::_parseConfigData($data[$i],$path.$p);
			}	else if (is_array($data[$i])) {
				self::_parseConfigData($data[$i],$path.'.'.$i);
			}
		}
	}

	private static function isPattern($data) {
		return is_array($data) && $data[0] == 'Pattern';
	}
	
	private static function isPatternReference($data) {
		return self::isPattern($data) && is_array($data[1]) && $data[1][0] == '#';
	}
	
	
	private static function isDataBlock($data) {
		// почему-то всегда массив вида
		// && self::isGuid($data[0]) && is_int($data[1])
		return is_array($data) && count($data) >= 2 && self::isGuid($data[0]) && is_int($data[1]) ;
	}
	
	
	private static function isDataElement($data) {
		// {0,
		// {0,0,bc2cd979-0bc0-4506-a77f-45f037cd8409},"Учреждение",
		//
		$el = null;
		if  (is_array($data) && count($data)>=2) {
			if (is_array($data[1]) && count($data[1])==3) {
				$guid = $data[1][2];	
				if  ($data[1][0] == 0 && $data[1][1] == 0 && self::isGuid($guid) ) {
					if (is_string($data[2])) {
						$el = ['GUID'=>$data[1][2],'NAME'=>$data[2]];
						if (isset(self::$_types['Fld'][$guid])) {
							$fld_name = self::$_types['Fld'][$guid];
							$el[self::FLD_NAME] = '_Fld' . $fld_name;
						}
					}
					if (count($data)>=4 && is_array($data[3]) && count($data[3])>=3 && $data[3][1]=='ru')   {
						$el['CAPTION'] = $data[3][2];
					}
					if ($data[0]!==0) {
						//echo "BY CODE<br>";
					}
					/*
					if (count($data)>=8) {
						$ref_guid = $data[7];
						echo "BY $ref_guid";
						//} && $data[7]=='f440939a-f130-413b-9c9a-2b18c4af69c6') {
						if 	(isset(self::$_types['Fld'][$ref_guid])) echo "<br>BY_NAME<br>";
					}
					*/
				}
			}
		}
		return $el;
	}
	
	private static function isGuid($val) {
		return is_string($val) && preg_match('/^'.self::REGEXP_GUID.'$/',$val);
	}
	
	
	/* Заменяет " some ""Title"" " на " some \"Title\" " */
	private static function escape_qoutes($str) {
		$in_quotes = false;
		$quote = '"';
		$slash = '\\';
		$l = strlen($str);
		for ($i=0; $i<$l; $i++) {
			$char = $str[$i];
			if ($char == $quote[0])  {
				if ($in_quotes && $i+1<$l && $str[$i+1]==$quote[0]) {
					$str[$i] = $slash[0];
					$i++;
				} else {
					$in_quotes = ! $in_quotes;
				}
			}
		}
		return $str;		
	}
    
    // рекурсивно парсит блоки возвращает строку на которой остановился
    protected static function _parseConfigBlock(&$parent,&$lines,$from,$to,$matches) {
        $guid_t = $matches['guid'];
        if (!isset(self::$_t_guids[$guid_t])) self::$_t_guids[$guid_t] = 0;
        self::$_t_guids[$guid_t]++;
        $cnt = $matches['cnt'];
        $name = '';
        $to = self::_blockLineEnd($lines,$from,$to);

        for ($i=$from+1; $i<$to; $i++) {
            $line = $lines[$i];
            if (self::_isNewElement($line, $matches)) {
                if (--$cnt<0) die('wrong number of elements in block!');                
                $name = $matches['name'];                
                $el = self::_element($guid_t,$matches['guid']);
                // если поле и ссылка добавим rref к имени

                if (isset($el[self::FLD_NAME])) {
                    $is_ref = self::_fldIsRef($lines,$i); 
                    if (is_null($is_ref)) {
                        self::debug("Pattern not found for field $name\n"); 
                    }
                    if ($is_ref) {
                        $el[self::FLD_NAME] .= 'RRef';
                    }
                }
                $parent[$name] = $el;
            }
            if (self::_isNewBlock($line,$matches)) {
                if (empty($name)) die('No element name for block start!'); 
                $i = self::_parseConfigBlock($parent['name'],$lines,$i,$to,$matches);
            }
        }
        return $to;
    }
    
    protected static function _element($guid_t,$guid) {
        $type = self::$_type;
       // $table_rus = self::$_table_rus_name;        
        $el = [self::GUID_T=>$guid_t,'guid'=>$guid];
        if ($guid_t == GUID_TYPE_REF_VT) {
            // подчиненная таблцица
            $nom = self::$_types['VT'][$guid];
            $el[self::HEX_N] = '0x' . dechex($nom);
            $el[self::TBL_NAME] = '_VT'.$nom;
        } elseif ($type == 'Enum') {
            $el['enum'] = substr(UUIDAsHex($guid),2);
        } elseif (isset(self::$_types['Fld'][$guid])) {
            $fld_name = self::$_types['Fld'][$guid];
            $el[self::FLD_NAME] = '_Fld' . $fld_name;
        }
        return $el;
    }
    
    // находит строку с закрывающей скобкой
    protected static function _blockLineEnd(&$lines,$from,$to) {
        $brackets = 0;
        for ($i=$from;$i<$to;$i++) {
            $line = $lines[$i];
            for ($j=0,$l=strlen($line);$j<$l;$j++) {
                if ($line[$j]=='{') $brackets++;
                if ($line[$j]=='}') {
                    $brackets--;
                    if ($brackets==0) {
                        return $i;
                    }
                }
            }
        }
        die('Not found closing bracket }');
    }

    protected static function _init() {

        $res = mssql_query("Select BinaryData from Params where FileName='DBNames'",self::$mssql);
        $row = mssql_fetch_array($res,MSSQL_ASSOC);
        $strings = explode("\r\n",gzinflate($row['BinaryData']));
       
        foreach ($strings as $string) {
            $matches = array();
            if (preg_match('/^{'.self::REGEXP_GUID.',"(?P<type>\w+)",(?P<num>\d+)}[,]*$/', $string,$matches) && $matches['guid'] !='00000000-0000-0000-0000-000000000000'){
//            self::$_fields[$matches['guid']][$matches['type']]='_'.$matches['type'].$matches['num'];
            self::$_types[$matches['type']][$matches['guid']]=$matches['num'];
            }
        }
		

        $sql = "Select BinaryData from Config where FileName in ('" . implode("','",array_keys(self::$_types['Enum'])) . "')";
        $res = mssql_query($sql,self::$mssql);
        while ($row = mssql_fetch_array($res,MSSQL_ASSOC)) {
            $string = gzinflate($row['BinaryData']);

            $lines = explode("\r\n", $string);
            foreach($lines as $line) {
                if (preg_match('/^{0,0,(?P<guid>[0-9a-f-]+)},"/',$line, $matche)) {
                    $guid = UUIDAsHex($matche['guid']);
                    self::$_er[$guid]['n']=explode('"', $line)[1];
                }
                if (substr($line,0,9)=='{1,"ru","'){
                    self::$_er[$guid]['a']=explode('"', $line)[3];
                    self::$_er[$guid]['c']=(isset(explode('"', $line)[5])) ? explode('"', $line)[5]:"";
                }
            }
        }

        define ('GUID_TYPE_REF_VT','932159f9-95b2-4e76-a8dd-8849fe5c5ded');
        /*
        foreach (self::$_types as $type=>$table) {
            echo "$type:" .count($table) ."<br>";
        }
        die();
        */
        $known_types = ['Reference','InfoRg','Enum','ExtDim','Chrc','Const','Document','DocumentJournal'];
        foreach (self::$_types as $type=>$table) {
            //echo $type ."<br>";
            if (in_array($type,$known_types) || substr($type,0,3)=='Acc') {
         //   if ($type !== 'Fld' && $type == 'Reference') {
                $sql = "Select FileName, BinaryData from Config where FileName in ('" . implode("','",array_keys($table)) . "')";
                $res = mssql_query($sql,self::$mssql);               
                self::$_type =$type;
				$i = 0;
                while ($row = mssql_fetch_array($res,MSSQL_ASSOC)) {
                    $string = gzinflate($row['BinaryData']);                    
                    
                //    self::_parseConfig2($string,$type,$table[$row['FileName']]);
					self::_parseConfig($string,$type,$table[$row['FileName']]);
                    $i++;
					//var_dump($string);
					//if ($i>10) die();
                }
            }            
        }
        if (self::$debug) {
            var_dump(self::$_t_guids);
        }
    }
}    