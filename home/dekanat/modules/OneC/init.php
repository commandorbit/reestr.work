<?php
// for Kohana fake variable set
define ('SYSPATH', true);
if (!function_exists('mssql_connect')) {
	require_once( __DIR__ . '/../mssql/mssql_pdo.php');
} 
date_default_timezone_set('Europe/Moscow');
putenv('FREETDSCONF='. __DIR__ . '/freetds.conf') ;
ini_set('mssql.charset', 'UTF-8');
ini_set('mssql.textlimit',1024*1024*1024);
ini_set('mssql.textsize', 1024*1024*1024);

function UUIDAsHex($UUID) {
    $x1 = substr($UUID,19,4);
    $x2 = substr($UUID,24,12);
    $x3 = substr($UUID,14,4);
    $x4 = substr($UUID,9,4);
    $x5 = substr($UUID,0,8);
    return "0x" . $x1 . $x2 . $x3 . $x4 . $x5;
}


define ('DIR_CLASS_PATH',__DIR__ . '/classes/');

function auto_load($class)  {
    if (class_exists($class)) return true;
    
    // Transform the class name into a path
    $file = DIR_CLASS_PATH . str_replace('_', '/', strtolower($class)) .'.php';
	
    if (file_exists ($file))  {
          // Load the class file
          require_once  $file;
          return TRUE;
    }
   
   // die("class $class not found in  $file");
    // Class is not in the filesystem
    return FALSE;
}

spl_autoload_register('auto_load');

require (__DIR__ . '/Helper1C.php');
require (__DIR__ . '/Struct1C.php');

$mssql_1c = connect_ms();
$config = array('type'=>'1c','mssql'=>$mssql_1c);

//$file = '/usr/home/reestr/DOCS/struct_1c';
//$file = '/var/www/localhost/struct_1c';

$file = '/var/tmp/struct_1c';
if (!Struct1C::load($file)) {
	Struct1C::init($mssql_1c);
	Struct1C::save($file);
}

Database::instance(NULL, $config);
Helper1C::init(Struct1C::getInternalStruct(),$mssql_1c);
if ($mssql_1c) {
	Helper1C::setOrg('СПбГЭУ');
}


//Helper1C::setOrg('ГОБУ ВПО Университет искусств (Субсидия)');

//$t = Struct1C::getInternalStruct();
//print_r($t);
function connect_ms_cit() {	

    $db_server = 'MSSQL_1C_CIT';
    $db_user = 'achernov';
    $db_pass = '9966asD';
    $db_name = '1C2015bgu-cit';
    //$db_name = '1C2013bgu';
    if (substr($_SERVER["SCRIPT_FILENAME"], 0, 1) != "/" || substr($_SERVER['SERVER_ADDR'], 0, 3) == "192") {
//    $db_server = 'VSMIRNOV-W81';
        $db_server = '192.168.137.1';
        $db_user = 'usr_one_s';
        $db_pass = 'QEah8zWLfWpZlntDDtKI';
        $db_name = 'ONE_S';
    }
    $mssql = mssql_connect($db_server,$db_user,$db_pass);
    if(!$mssql) die("Невозможно подключиться к базе данных MsSQL\n");
    mssql_select_db ($db_name,$mssql);
    return $mssql;
}


function connect_ms() {	

    $db_server = 'MSSQL_2000';
    $db_user = 'achernov';
    $db_pass = '9966asD';
    //$db_name = '1C2015bgu-cit';
    $db_name = '1C2013bgu';
    if (substr($_SERVER["SCRIPT_FILENAME"], 0, 1) != "/" || substr($_SERVER['SERVER_ADDR'], 0, 3) == "192") {
//    $db_server = 'VSMIRNOV-W81';
        $db_server = '192.168.137.1';
        $db_user = 'usr_one_s';
        $db_pass = 'QEah8zWLfWpZlntDDtKI';
        $db_name = 'ONE_S';
    }
	try {
		$mssql = mssql_connect($db_server,$db_user,$db_pass);
		mssql_select_db($db_name,$mssql);
	} catch (Exception $e) {
		echo 'Caught exception: ',  $e->getMessage(), "\n";
	}
    if(!$mssql) die("Невозможно подключиться к базе данных MsSQL\n");
    
    return $mssql;
}
