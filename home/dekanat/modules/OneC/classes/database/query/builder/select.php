<?php defined('SYSPATH') or die('No direct script access.');

class Database_Query_Builder_Select extends Database_Query_Builder_SelectWrapper {
protected $_last_table;
protected $_table_alias_struct = [];
protected $_t = null;

public function from($table) {
    parent::from($table);
    
    //save link on last table for balance and other methods
    $n = count($this->_from)-1;
    unset($this->_last_table);
    $this->_last_table = &$this->_from[$n];
    return $this;
}

public function join($table, $type = NULL) {    
    parent::join($table,$type);    
    //save last join table for balance and other methods
    $n = count($this->_join)-1;
    unset($this->_last_table);
    $this->_last_table = $this->_join[$n];
    return $this;
}

protected function _last_table($table = null) {
    if ($this->_last_table instanceof Database_Query_Builder_Join) {
        return $this->_last_table->table($table);
    } 
    
    if ($table) $this->_last_table = $table;

    return $this->_last_table;
}

public function balance($date = null, $account,$subconto) {
    $this->_last_table(DB::balance($this->_last_table(),$date,$account,$subconto));
    return $this;
}

public function oborot_debet($account,$date_from,$date_to,$subconto) {
    $this->_last_table(DB::oborot_debet($this->_last_table(),$account,$date_from,$date_to,$subconto));
    return $this;
}

public function oborot_credit($account,$date_from,$date_to,$subconto) {
    $this->_last_table(DB::oborot_credit($this->_last_table(),$account,$date_from,$date_to,$subconto));
    return $this;
}

public function slice_last($date = null) {
    $this->_last_table(DB::slice_last($this->_last_table(),$date));
    return $this;
}

protected function _selectAll() {
    foreach ($this->_table_alias_struct as $table) {
        foreach ($table as $fldName=>$fldStruct) {
            $this->_select[] = ['_IDRRef','Ссылка'];
            $this->_select[] = ['_Description','Наименование'];            
            if (isset($fldStruct[Struct1C::FLD_NAME])) {
                $this->_select[] = [$fldStruct[Struct1C::FLD_NAME],$fldName];
            }
        }
    }
}

public function translate_column($column) {
    $alias = null;    
    if (is_array($column)) {
        list($column,$alias) = $column;
    }
    if ($column instanceof Database_Query) {
    } elseif ($column instanceof Database_Expression) {
    } else {
        $parts = explode('.',$column);
        $alias = ($alias) ? $alias : end($parts);
        
        if (count($parts)==2) {
            $table_alias = $parts[0];
            $name = $parts[1]; 
            if (isset($this->_table_alias_struct[$table_alias])) {
                $t = $this->_table_alias_struct[$table_alias];
                if (isset($t[$name][Struct1C::FLD_NAME])) {
                    return [$table_alias . '.' .$t[$name][Struct1C::FLD_NAME], $alias];
                } else if ($name == 'Ссылка'){
                    return [$table_alias . '._IDRRef', $alias];
                }  else if ($name == 'Наименование'){     
                    return [$table_alias . '._Description', $alias];
                }  else if ($name == 'Код'){     
                    return [$table_alias . '._Code', $alias];
                } 
//                else {
//                    if ($name !== '_Description')
//                        echo "unknown fld $name in table $table_alias  ";
//                        //die();
//                }
            } else {
               // echo "unknown table_alias $table_alias<br>";
            }
        }
    }
    return [$column, $alias];
}

protected function _parseTable($table) {
    $type_names = ['Справочник'=>'Reference'];
    $alias = null;    
    if (is_array($table)) {
        list($table,$alias) = $table;
    }
    if ($table instanceof Database_Query) {
        return null;
    }    
    $parts = explode('.',$table);
    $type = $parts[0];
    
    if (!isset($type_names[$type])) {
        return null;
    }
    $type = $type_names[$type];

    $t = Struct1C::getInternalStruct();
    if (isset($t[$type][$parts[1]])) {
        $table_struct = $t[$type][$parts[1]];
        $sql_table = $table_struct[Struct1C::TBL_NAME];
    } else {
        //echo "unknown table $table<br>";
    }
    $alias  = empty($alias) ? end($parts) : $alias;
    $this->_table_alias_struct[$alias] = $table_struct;
    return [$sql_table,$alias];
}

public function compile(Database $db) {
    if ($db instanceof Database_1c) {
        $db->current_subquery($this);
        
        // save for debugging
        $this->_orig_from = $this->_from;
        $this->_orig_join = $this->_join;

        $table_alias = [];
        
        for ($i= 0 ; $i<count($this->_from) ; $i++ ) {
            $table = $this->_from[$i];

            if ($new_table = $this->_parseTable($table)) {
                $this->_from[$i] = $new_table;
            }
        }
        
        for ($i = 0 ; $i<count($this->_join) ; $i++ ) {
            $table = $this->_join[$i]->table();
            if ($new_table = $this->_parseTable($table)) {
                $this->_join[$i]->table($new_table);
            }
//            $onAll = $this->_join[$i]->get_on();
        }
        
        for ($i = 0 ; $i<count($this->_join) ; $i++ ) {
            $on = $this->_join[$i]->get_on();
            for ($j=0;$j<count($on);$j++) {
                $fld1 = $this->translate_column($on[$j][0])[0]; // translate_column возвращает массив column alias
  //              $sign = $onAll[$j][1];
                $sign = $on[$j][1];
                $fld2 = $this->translate_column($on[$j][2])[0];
                $on[$j] = [$fld1,$sign,$fld2];
                
            }
            $this->_join[$i]->set_on($on);
        }
        
        for ($i = 0 ; $i<count($this->_select) ; $i++ ) {
            $this->_select[$i] = $this->translate_column($this->_select[$i]);
        }
        
        
        if (empty($this->_select))	{
            $this->_selectAll();
            // Select all columns
            // выбрать все колонки с алиасами
        }
        
        foreach ($this->_where as &$group) {
			// Process groups of conditions
			foreach ($group as $logic => &$condition){
				if ($condition === '(') {
				} elseif ($condition === ')') {
				} else {
					// Split the condition
					list($column, $op, $value) = $condition;

					// Database operators are always uppercase
					$op = strtoupper($op);

					if ($op === 'BETWEEN' AND is_array($value))	{
						// BETWEEN always has exactly two arguments
						list($min, $max) = $value;

						if ((is_string($min) AND array_key_exists($min, $this->_parameters)) === FALSE)	{
							// Quote the value, it is not a parameter
							$min = $db->quote($min);
						}

						if ((is_string($max) AND array_key_exists($max, $this->_parameters)) === FALSE)	{
							// Quote the value, it is not a parameter
							$max = $db->quote($max);
						}

						// Quote the min and max value
						//$value = $min.' AND '.$max;
					} elseif ((is_string($value) AND array_key_exists($value, $this->_parameters)) === FALSE)	{
						// Quote the value, it is not a parameter
						//$value = $db->quote($value);
					}

					if ($column) {
                        // вероятно value надо бы тоже обработать
                        $condition = [$this->translate_column($column),$op,$value];
					}
				}
			}
        }

        if ( ! empty($this->_group_by))	{
			for ($i = 0 ; $i<count($this->_group_by) ; $i++ ) {
				$this->_group_by[$i] = $this->translate_column($this->_group_by[$i])[0];
			}

            // Add grouping
            //$query .= ' '.$this->_compile_group_by($db, $this->_group_by);
        }

        if ( ! empty($this->_having))	{
            // Add filtering conditions
            //$query .= ' HAVING '.$this->_compile_conditions($db, $this->_having);
        }

        if ( ! empty($this->_order_by))		{
            // Add sorting
            //$query .= ' '.$this->_compile_order_by($db, $this->_order_by);
        }

        /*
        if ( ! empty($this->_union))
        {
            foreach ($this->_union as $u) {
                $query .= ' UNION ';
                if ($u['all'] === TRUE)
                {
                    $query .= 'ALL ';
                }
                $query .= $u['select']->compile($db);
            }
        }
        */
    }

    return parent::compile($db);
}
}
