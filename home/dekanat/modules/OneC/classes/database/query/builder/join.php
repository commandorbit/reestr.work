<?php defined('SYSPATH') or die('No direct script access.');

class Database_Query_Builder_Join extends Kohana_Database_Query_Builder_Join {
    public function table($table = null) {
        if ($table) {
            $this->_table = $table;
        }
        return $this->_table;
    }
    
    public function get_on() {
        return $this->_on;
    }
    public function set_on($on) {
        return $this->_on = $on;
    }
    
}
