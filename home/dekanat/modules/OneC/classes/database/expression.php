<?php defined('SYSPATH') or die('No direct script access.');

class Database_Expression extends Kohana_Database_Expression {
    protected $_columns =[];
    
    //принимает table.column 
    public function column($column) {
		$columns = func_get_args();
		$this->_columns = array_merge($this->_columns, $columns);
		return $this;
	}

	 
	public function compile($db = NULL)	{
		if ($db instanceof Database_1c) {
            if ($query = $db->current_subquery()) {                
                $translated = [];
                $value = $this->value();
                foreach ($this->_columns as $column) {
                    $translated = $query->translate_column($column)[0];
                    $value = str_replace($column,$translated,$value);
                }
                $this->_value = $value;
            }
		}
        return parent::compile($db);
	}

}