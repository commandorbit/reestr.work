<?php
class Database_1c extends Database {
    protected $_identifier_left  = '[';
    protected $_identifier_right = ']';
    protected $_current_subquery;
    protected $_mssql;
    
    public function  __construct($name, array $config)	{
        if (!isset($config['table_prefix'])) $config['table_prefix'] = '';
        $this->_mssql = $config['mssql'];
		parent::__construct($name,$config);
	}
    
    public function getStruct_1c() {
        return $this->_config['struct_1c'];
    }
    
    public function current_subquery($db_query=null) {
        if ($db_query) {
            if($db_query instanceof Database_Query) {
                $this->_current_subquery = $db_query;
            } else {
                die("db_query is not instance of  Database_Query");
            }
        }
        return $this->_current_subquery;
    }
    
    public function set_charset($charset){}
    public function list_tables($like = NULL){}
	public function query($type, $sql, $as_object = FALSE, array $params = NULL){
        return mssql_query($sql,$this->_mssql);
    }
	public function begin($mode = NULL){}
	public function commit(){}
	public function rollback(){}
    public function list_columns($table, $like = NULL, $add_prefix = TRUE){}
    public function connect() {}
    public function disconnect() {}
    public function escape($value) {
        // hex number
        if (substr($value,0,2) == '0x' && ctype_xdigit(substr($value,2))) {
            $val = $value;
        } else {
            $val = "'".$value."'";
        }        
        return $val;
    }
    
	public function quote_column($column)
	{
		if (is_array($column))
		{
			list($column, $alias) = $column;
		}

		if ($column instanceof Database_Query)
		{
			// Create a sub-query
			$column = '('.$column->compile($this).')';
		}
		elseif ($column instanceof Database_Expression)
		{
			// Compile the expression
			$column = $column->compile($this);
		}
		else
		{
			// Convert to a string
			$column = (string) $column;

			if ($column === '*')
			{
				return $column;
			}
			elseif (strpos($column, '"') !== FALSE)
			{
				// Quote the column in FUNC("column") identifiers
				$column = preg_replace_callback('/"(.+?)"/',function ($matches) {
					return $this->quote_column($matches[1]);
				},$column);
				//$column = preg_replace('/"(.+?)"/e', '$this->quote_column("$1")', $column);
			}
			elseif (strpos($column, '.') !== FALSE)
			{
				$parts = explode('.', $column);

				if ($prefix = $this->table_prefix())
				{
					// Get the offset of the table name, 2nd-to-last part
					$offset = count($parts) - 2;

					// Add the table prefix to the table name
					$parts[$offset] = $prefix.$parts[$offset];
				}

				foreach ($parts as & $part)
				{
					if ($part !== '*')
					{
						// Quote each of the parts
						$part = $this->_identifier_left.$part.$this->_identifier_right;
					}
				}

				$column = implode('.', $parts);
			}
			else
			{
				$column = $this->_identifier_left.$column.$this->_identifier_right;
			}
		}

		if (isset($alias))
		{
			$column .= ' AS '.$this->_identifier_left.$alias.$this->_identifier_right;
		}

		return $column;
	}

	/**
	 * Quote a database table name and adds the table prefix if needed.
	 *
	 *     $table = $db->quote_table($table);
	 *
	 * Objects passed to this function will be converted to strings.
	 * [Database_Expression] objects will be compiled.
	 * [Database_Query] objects will be compiled and converted to a sub-query.
	 * All other objects will be converted using the `__toString` method.
	 *
	 * @param   mixed   table name or array(table, alias)
	 * @return  string
	 * @uses    Database::quote_identifier
	 * @uses    Database::table_prefix
	 */
	public function quote_table($table)
	{
		if (is_array($table))
		{
			list($table, $alias) = $table;
		}

		if ($table instanceof Database_Query)
		{
			// Create a sub-query
			$table = '('.$table->compile($this).')';
		}
		elseif ($table instanceof Database_Expression)
		{
			// Compile the expression
			$table = $table->compile($this);
		}
		else
		{
			// Convert to a string
			$table = (string) $table;

			if (strpos($table, '.') !== FALSE)
			{
				$parts = explode('.', $table);

				if ($prefix = $this->table_prefix())
				{
					// Get the offset of the table name, last part
					$offset = count($parts) - 1;

					// Add the table prefix to the table name
					$parts[$offset] = $prefix.$parts[$offset];
				}

				foreach ($parts as & $part)
				{
					// Quote each of the parts
					$part = $this->_identifier_left.$part.$this->_identifier_right;
				}

				$table = implode('.', $parts);
			}
			else
			{
				// Add the table prefix
				$table = $this->_identifier_left.$this->table_prefix().$table.$this->_identifier_right;
			}
		}

		if (isset($alias))
		{
			// Attach table prefix to alias
			$table .= ' AS '.$this->_identifier_left.$this->table_prefix().$alias.$this->_identifier_right;
		}

		return $table;
	}    
    
	public function quote_identifier($value)
	{
		if (is_array($value))
		{
			list($value, $alias) = $value;
		}

		if ($value instanceof Database_Query)
		{
			// Create a sub-query
			$value = '('.$value->compile($this).')';
		}
		elseif ($value instanceof Database_Expression)
		{
			// Compile the expression
			$value = $value->compile($this);
		}
		else
		{
			// Convert to a string
			$value = (string) $value;

			if (strpos($value, '.') !== FALSE)
			{
				$parts = explode('.', $value);

				foreach ($parts as & $part)
				{
					// Quote each of the parts
					$part = $this->_identifier_left.$part.$this->_identifier_right;
				}

				$value = implode('.', $parts);
			}
			else
			{
				$value = $this->_identifier_left.$value.$this->_identifier_right;
			}
		}

		if (isset($alias))
		{
			$value .= ' AS '.$this->_identifier_left.$alias.$this->_identifier_right;
		}

		return $value;
	}
    
}