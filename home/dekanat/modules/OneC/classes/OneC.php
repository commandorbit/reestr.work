<?php

class OneC extends DB {
	public static function enum_options($name) {
		return Helper1C::enum_options($name);
	}
	
	public static function enum($name) {
		return Helper1C::enum($name);
	}
	
	public static function ff_tmp() {
		
	}
	
	public static function fetch_rows_with_subconto($query_result,$prefix="Субконто") {
		$subconto = [];
		/*
		$n = mssql_num_fields($query_result);		
		for ($i = 0; $i < $n; $i++) {
			$field = mssql_fetch_field ($query_result,$i);
			$column_name  = $field->name;
			$column_type  = $field->type;
			$label = $column_name;
			$flds[$column_name]= ["name"=>$column_name,"type"=>$column_type,"label"=>$label];
		}
		*/
		$rows = [];
		$cache = [];
		$cnt = 0;
		while ($row =  mssql_fetch_array($query_result,MSSQL_ASSOC)) {
			for ($i=1;$i<5;$i++) {				
				if (isset($row[$prefix.$i])) {
					$cnt++;
					$column = $prefix.$i;
					if ('0x'.bin2hex($row[$column.'_TYPE']) == Struct1C::REF_TYPE) {
						$ref_nomer = hexdec(bin2hex($row[$column.'_RT']));
						$t = Helper1C::$t;
						$sql = "Select * from [_Reference" .$ref_nomer ."] where [_IDRRef] = 0x" . bin2hex($row[$column]);
						if (isset($cache[$sql])) {
							$row[$column.'_name'] = $cache[$sql];
						} else {
							$tmp_row  = self::mssql_fetch($sql);
							$value = (isset($tmp_row['_Description'])) ? $tmp_row['_Description'] : $tmp_row['_Code'];							
							$row[$column.'_name'] = $cache[$sql] = $value;
							//$row[$column.'_name'] =  $value;
						}
					}
				}
					
			}
			$rows[] = $row;
		}
		echo "CHACHED: " . count($cache) . " total: " . $cnt;
		return $rows;
	}
	
	private static function mssql_fetch($sql) {
		global $mssql_1c;
		$res = mssql_query($sql,$mssql_1c);
		$row = mssql_fetch_array($res,MSSQL_ASSOC);
		return $row;
	}

	private static function mssql_fetch_val($sql) {
		return self::mssql_fetch($sql)[0];
	}
}
