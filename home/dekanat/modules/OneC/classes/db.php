<?php 
defined('SYSPATH') or die('No direct script access.');

class DB extends Kohana_DB {
    public static $i=0;
    public static function cast($expr,$type) {
        return Helper1c::cast($expr,$type);
    }
    
    public static function enum($val) {
        return Helper1c::enum($val);
    }
    
    protected static function _table_alias($table) {
        $alias = null;
        if (is_array($table)) {
            list($table,$alias) = $table;
        }   else {
            self::$i++;
            $alias = '_t'.self::$i;
        }
        return [$table,$alias];
    }
    
    public static function balance($table, $date=null, $account, $subconto) {     
        list($table,$alias) = self::_table_alias($table);
        $parts = (explode('.',$table));
        $table = end($parts);
        $sql = Helper1C::accRgBalanceSql($table,$date,$account,$subconto);
        $query = DB::query(Database::SELECT,$sql);
        return [$query,$alias];   
    }
    
    public static function oborot_debet($table, $account,$date_from,$date_to,$subconto) {
        list($table,$alias) = self::_table_alias($table);
        $parts = (explode('.',$table));
        $table = end($parts);
        $sql = Helper1C::oborot_acc_sql($table,$account,$date_from,$date_to,0,$subconto);
        $query = DB::query(Database::SELECT,$sql);
        return [$query,$alias];   
    }
    
    public static function oborot_credit($table, $account,$date_from,$date_to,$subconto) {
        list($table,$alias) = self::_table_alias($table);
        $parts = (explode('.',$table));
        $table = end($parts);        
        $sql = Helper1C::oborot_acc_sql($table,$account,$date_from,$date_to,1,$subconto);
        $query = DB::query(Database::SELECT,$sql);
        return [$query,$alias];   
    }
    
    public static function slice_last($table,$date=null) {
        list($table,$alias) = self::_table_alias($table);
        $sql = Helper1C::info_rg_slice_last_sql($table,$date);
        $query = DB::query(Database::SELECT,$sql);
        return [$query,$alias];   
    }
}
