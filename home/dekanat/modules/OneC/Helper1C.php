<?php

error_reporting(E_ALL | E_STRICT);
class Helper1C {

    public static $_mssql;
    public static $t;
    public static $org ;

    public static function init($t,$mssql) {
        self::$t = $t;
        self::$_mssql = $mssql;
    }

    public static function setOrg($orgName) {
        $res = mssql_query("SELECT _IDRRef FROM ".self::$t['Reference']['Организации']['t']." WITH(NOLOCK)  WHERE _Description = '$orgName'",self::$_mssql);
        $row = mssql_fetch_array($res,MSSQL_ASSOC);
        self:: $org = '0x'.bin2hex($row['_IDRRef']);
    }

    public static function tableReferenceAsAssoc($name) {
        $assoc = array();
        $res = mssql_query("SELECT _IDRRef as id, _Description as name  FROM ".self::$t['Reference'][$name]['t']."  WITH(NOLOCK)  WHERE _Marked = 0x00 ORDER BY _Description",self::$_mssql);
        while ( $row = mssql_fetch_array($res,MSSQL_ASSOC)){
            $id = '0x' . bin2hex($row['id']);
            $assoc[$id] = $row['name'];
        }
        return $assoc;
    }

    public static function subAccountsIds($account_code) {
        $acc_table = self::acc_table();
		if (is_array($account_code)) {
			$account_code = array_map(function($val) {return "'$val'";},$account_code);
			$sql = "SELECT _IDRRef FROM $acc_table WITH(NOLOCK) WHERE _Code IN (" . implode(',',$account_code) .")";
		} else {
			$sql = "SELECT _IDRRef FROM $acc_table WITH(NOLOCK) WHERE _Code = '$account_code'";
		}
        $res = mssql_query($sql,self::$_mssql);
        $acc_ids = [];
		
        while (mssql_num_rows($res)) {
            while ($row =  mssql_fetch_array($res,MSSQL_ASSOC)) {
                $acc_ids[] = '0x'.bin2hex($row['_IDRRef']);
            };
            $sql_acc_ids = implode(',',$acc_ids);
            $sql = "select _IDRRef from $acc_table where _IDRRef not in ($sql_acc_ids) and _ParentIDRRef IN ($sql_acc_ids) ";
            $res = mssql_query($sql,self::$_mssql);
        }
        return $acc_ids;
    }
	
	private static function acc_table() {
		return self::$t['Acc']['ЕПСБУ']['t'];
	}
	

    public static function getAccountsInfo() {
        $acc_table = self::$t['Acc']['ЕПСБУ']['t'];
        //$res = mssql_query("SELECT _IDRRef, _Code, _Description FROM $acc_table WITH(NOLOCK) WHERE _Code >= '101.00' AND _Code < '104.00' ORDER BY _Code");
        $res = mssql_query("SELECT _IDRRef, _Code, _Description FROM $acc_table WITH(NOLOCK) ORDER BY _Code",self::$_mssql);
        $acc = array();
        while ($row = mssql_fetch_array($res,MSSQL_ASSOC))
        { $acc['0x'.bin2hex($row['_IDRRef'])]=$row['_Code'].' '.$row['_Description'];}
        return $acc;
    }

    public static function info_rg_slice_last_sql($table,$d = null) {

        $struct = self::$t['InfoRg'][$table];
        $sql_table = $struct[Struct1C::TBL_NAME];
        $group_flds = [];
        $flds = [];
        foreach ($struct as $rus_name=>$fld) {
            if (isset($fld[Struct1C::FLD_NAME])) {
                $fld_name = $fld[Struct1C::FLD_NAME];
                $flds[] = "$fld_name AS $rus_name" ;
                if ($fld[Struct1C::GUID_T] == Struct1C::GUID_T_RESURS) {
                    $group_flds[] = $fld_name;
                }
            }
        }
        $pre1_where = " _Active = 0x01 ";
        if ($d) $pre1_where .= " AND  _Period <= $d ";

        // 1-й отбирвает MAX_PERIOD по ресурсам
        $pre1 = "SELECT ".implode(',',$group_flds).",MAX(_Period) AS MAXPERIOD FROM $sql_table t WITH(NOLOCK)
                WHERE $pre1_where
                GROUP BY ".implode(',',$group_flds)." ";

        $join = implode(' and ',array_map(function($val) {return 't.'.$val.'=pre.'.$val;}, $group_flds));
        // 2-й отбирает последний случайный(?) регистратор на время MAX_PERIOD
        $fn_add_t = function ($val){return 't.'.$val;};
        $select_flds = implode(',',array_map($fn_add_t,$group_flds));
        $pre2 = "select $select_flds,pre.MAXPERIOD,
                SUBSTRING(MAX(t._RecorderTRef + t._RecorderRRef),1,4) AS MAXRECORDERTRef,
                SUBSTRING(MAX(t._RecorderTRef + t._RecorderRRef),5,16) AS MAXRECORDERRRef
                from ($pre1) pre
                inner join $sql_table t on t._period = pre.MAXPERIOD and $join
                where t._Active = 0x01
                group by $select_flds,pre.MAXPERIOD ";

        // возвращаем данные по полученным отборам
        $sql = "select t._Period,t._RecorderTRef,t._RecorderRRef,".implode(',',array_map($fn_add_t,$flds))." from  ($pre2) pre inner join $sql_table t on t._period = pre.MAXPERIOD and $join and t._RecorderTRef = pre.MAXRECORDERTRef and t._RecorderRRef = MAXRECORDERRRef";
        return $sql;
    }
	
	public static function enum_options($name) {
		if (!isset(self::$t['Enum'][$name])) return null;
		$enums = self::$t['Enum'][$name];
		$options = [];
		foreach ($enums as $key=>$val) {
			if (is_array($val) && isset($val['enum'])) {
				$options[$val['enum']] = $key;
			}
		}
		return $options;
	}

    public static function enum($val) {
        $parts = explode(".",$val);
        $current = self::$t['Enum'];
        foreach($parts as $part) {
            if (!isset($current[$part])) {
                die("Не определен enum $part");
            } else {
                $current = $current[$part];
            }
        }
        return UUIDAsHex($current['guid']);
    }

    public static function cast($column,$type) {
        $parts = explode('.',$type);
        if ($parts[0]=='Справочник') {
            $parts[0]='Reference';
        }
        $current = &self::$t;
        foreach ($parts as $part) {
            if (!isset($current[$part])) {
                die("в выражении cast немзвестный тип $part");
            }
            // unset ($current);
            $current = &$current[$part];
        }
        $RT = $current[Struct1C::HEX_N];
        //if (
        $sql = 'CASE WHEN '.$column.'_TYPE = '.Struct1C::REF_TYPE.' AND '.$column.'_RT = '.$RT.' THEN '.$column.' END';
        return  DB::expr($sql);
    }
	
	public static function _ttt() {
		$t = &self::$t;
		var_dump($t);
        $table_ВидыСубконтоБюджет = $t['Chrc']['ВидыСубконтоБюджет']['t'];
        $sql = "SELECT * from $table_ВидыСубконтоБюджет ";

        //echo $sql;
        $res = mssql_query($sql,self::$_mssql);
		$rows = [];
        while ($row =  mssql_fetch_array($res,MSSQL_ASSOC)) {
			echo $row['_Description'] ."\n";
			echo $row['_Type']."\n\n";
			$lines = explode("\n",$row['_Type']);
			for ($i=2; $i<(count($lines)-2); $i++) {
				preg_match('/[-0-9a-f]+/',$lines[$i],$matches);
				$guid = $matches[0];
				echo "guid: " . $guid . "\n";
			}
		}	
	}
	
	public static function _subconto_names($account_code,$table='ЕПСБУ') {
		$t = &self::$t;
        $table_ExtDim = $t['Acc'][$table]['t'].$t['ExtDim'][$table]['t'];
        $table_ВидыСубконтоБюджет = $t['Chrc']['ВидыСубконтоБюджет']['t'];
        $table_Acc = $t['Acc'][$table]['t'];
        $sql = "SELECT exDim._LineNo,vSB._Description,vSB._Type FROM $table_ExtDim exDim inner join $table_ВидыСубконтоБюджет vSB on exDim._DimKindRRef=vSB._IDRRef inner join $table_Acc a on exDim.${table_Acc}_IDRRef = a._IDRRef
        where a._Code='$account_code' ORDER BY exDim._LineNo ";

        //echo $sql;
        $res = mssql_query($sql,self::$_mssql);
		$rows = [];
        while ($row =  mssql_fetch_array($res,MSSQL_ASSOC)) {
            $rows[] = $row['_Description'];
        }		
		return $rows;
	}
	

    private static function _analitics_to_numbers($table,$account_code,$analitics=null) {
        $sql_descriptions = null;
        if ($analitics) {
            $sql_descriptions = implode(',',array_map(function($val) {return "'".$val ."'";},$analitics));
        }
        $t = self::$t;
        $table_ExtDim = $t['Acc'][$table]['t'].$t['ExtDim'][$table]['t'];
        $table_ВидыСубконтоБюджет = $t['Chrc']['ВидыСубконтоБюджет']['t'];
        $table_Acc = $t['Acc'][$table]['t'];
        $sql = "SELECT exDim._LineNo FROM $table_ExtDim exDim inner join $table_ВидыСубконтоБюджет vSB on exDim._DimKindRRef=vSB._IDRRef inner join $table_Acc a on exDim.${table_Acc}_IDRRef = a._IDRRef
        where a._Code='$account_code'  ";
        if ($sql_descriptions) $sql .= " and vSB._Description in ($sql_descriptions) " ;

        //echo $sql;
        $res = mssql_query($sql,self::$_mssql);
        $lines = [];
        while ($row =  mssql_fetch_array($res,MSSQL_ASSOC)) {
            $lines[] = $row['_LineNo'];
        }
        return $lines;
    }
	
    private static function _kind_refs($analitics) {
        $table_ВидыСубконтоБюджет = self::$t['Chrc']['ВидыСубконтоБюджет']['t'];
        $sql_descriptions = implode(',',array_map(function($val) {return "'".$val ."'";},$analitics));
        $sql = "SELECT _IDRRef  FROM  $table_ВидыСубконтоБюджет WHERE _Description in ($sql_descriptions) " ;
        //echo $sql;
        $res = mssql_query($sql,self::$_mssql);
        $refs = [];
        while ($row =  mssql_fetch_array($res,MSSQL_ASSOC)) {
            $refs[] = '0x' . bin2hex($row['_IDRRef']);
        }
        return $refs;
    }
    private static function yOffset() {
        $res = mssql_query("SELECT Offset FROM _YearOffset",self::$_mssql);
        $row = mssql_fetch_array($res,MSSQL_ASSOC);
        $yoffset = $row['Offset'];
        return $yoffset;
    }
    private static function _subcont_flds($lines) {
        $s_flds = 'Субконто%line%_TYPE, Субконто%line%_RT, Субконто%line%,';
        $sql_flds = '';
        foreach ($lines as $line) {
            $sql_flds .= str_replace('%line%',$line,$s_flds);
        }
        return $sql_flds;
    }
    private static function _subcont_flds2($n) {
        $s_flds = 'Субконто%line%_TYPE, Субконто%line%_RT, Субконто%line%,';
        $sql_flds = '';
        for($i=0; $i<$n; $i++) {
            $sql_flds .= str_replace('%line%',$i,$s_flds);
        }
        return $sql_flds;
    }
    public static function oborot_acc_sql($table,$account_code,$d_from,$d_to,$is_credit=0,$analitics=null) {
		$account_ids = self::subAccountsIds($account_code);
		$acc_code = is_array($account_code) ? $account_code[0] : $account_code;
		// Предполагаем что если передано несколько счетов то у всех одинаковая аналитика (как у первого)
		// то же самое про субсчета
        $lines = self::_analitics_to_numbers($table,$acc_code,$analitics);
        $sql = self::sql_part_saldo($table,$account_ids,$lines,$is_credit);

        $yoffset = self::yOffset();

        $d_expr = "CONVERT(date,DATEADD(year, -".$yoffset.", _Period))";
        // $d_expr = '_Period';

        $sql_flds = self::_subcont_flds($lines);

        $sql = "SELECT  $d_expr as Period,КБК,КВД,$sql_flds SUM(Сумма) as Сумма from \n\n(" . $sql ;
        $sql .= ") pre WHERE $d_expr >= '$d_from' AND $d_expr<='$d_to' ";
        $sql .= " GROUP BY КБК,КВД,$sql_flds $d_expr ";
        return $sql;
    }

    public static function accRgBalanceSql($table,$d,$account_code,$analitics) {
        //$table =  'РегистрБухгалтерии.ЕПСБУ';
        $parts = (explode('.',$table));
        $table = end($parts);

        $account_ids = self::subAccountsIds($account_code);

        //$lines = self::_analitics_to_numbers($table,$account_code,$analitics);
        //$sql_pre = self::sql_part_saldo($table,$account_ids,$lines,0) . "\n UNION ALL \n" . self::sql_part_saldo($table, $account_ids,$lines,1);
        //$sql_flds = self::_subcont_flds($lines);
        $kind_refs = self::_kind_refs($analitics);
        $sql_pre = self::sql_part_saldo2($table,$account_ids,$kind_refs,0) . "\n UNION ALL \n" . self::sql_part_saldo2($table, $account_ids,$kind_refs,1);
        $sql_flds = self::_subcont_flds2(count($analitics));

        $sql = "SELECT $sql_flds
            Счет,КБК,КВД,
            CASE WHEN CAST(SUM(Сумма) AS NUMERIC(36, 8)) IS NULL THEN 0.0 ELSE CAST(SUM(Сумма) AS NUMERIC(36, 8)) END AS СуммаОстаток,
            CASE WHEN CAST(SUM(Сумма) AS NUMERIC(36, 8)) <0 THEN CAST(-SUM(Сумма) AS NUMERIC(36, 8)) END AS СуммаОстатокКт,
            CASE WHEN CAST(SUM(Сумма) AS NUMERIC(36, 8)) >0 THEN CAST(SUM(Сумма) AS NUMERIC(36, 8)) END AS СуммаОстатокДт,
            CASE WHEN CAST(SUM(Количество) AS NUMERIC(32, 8)) IS NULL THEN 0.0 ELSE CAST(SUM(Количество) AS NUMERIC(32, 8)) END AS КоличествоОстаток
            FROM ($sql_pre) PRE
            GROUP BY $sql_flds
            Счет,КБК,КВД
           HAVING (CASE WHEN CAST(SUM(Сумма) AS NUMERIC(36, 8)) IS NULL THEN 0 ELSE CAST(SUM(Сумма) AS NUMERIC(36, 8)) END) <> 0
           OR (CASE WHEN CAST(SUM(Количество) AS NUMERIC(32, 8)) IS NULL THEN 0 ELSE CAST(SUM(Количество) AS NUMERIC(32, 8)) END) <> 0 ";

        return $sql;

    }
    private static function sql_part_saldo2($table,$account_ids,$kind_refs,$correspond) {
        //,$rdate,$mdate;
        $t = self::$t;
        $fld_Сумма = $t['AccRg'][$table]['Сумма']['Fld'];
        $fld_Количество = $t['AccRg'][$table]['Количество']['Fld'];
        $table_AccRg = $t['AccRg'][$table]['t'];
        $table_ExtDim = $t['Acc'][$table]['t'].$t['ExtDim']['ЕПСБУ']['t'];
        $fld_Учреждение = $t['AccRg'][$table]['Учреждение']['Fld'];
        $table_ВидыСубконтоБюджет = $t['Chrc']['ВидыСубконтоБюджет']['t'];
        $fld_KBK = substr($t['AccRg']['ЕПСБУ']['КБК']['Fld'],0,-4); //убираем Rref

        $s_flds = " ED%i%._Value_TYPE AS Субконто%i%_TYPE, ED%i%._Value_RTRef AS Субконто%i%_RT,	ED%i%._Value_RRRef AS Субконто%i%, ";
        $s_from = "
            LEFT OUTER JOIN ".$t['AccRgED'][$table]['t']." ED%i% WITH(NOLOCK)
            ON ED%i%._RecorderTRef = AccRg._RecorderTRef AND ED%i%._RecorderRRef = AccRg._RecorderRRef AND
            ED%i%._LineNo = AccRg._LineNo AND ED%i%._Period = AccRg._Period AND
            ED%i%._Correspond = %correspond% AND ED%i%._KindRRef = %kind_ref% ";

        $sql_flds = '';
        $sql_from = '';
        for ($i=0; $i < count($kind_refs); $i++) {
            $sql_flds .= str_replace('%i%',$i,$s_flds);
            $sql_from .= str_replace(['%i%','%kind_ref%'],[$i,$kind_refs[$i]],$s_from);
        }
        $sql_account_ids = implode(',', $account_ids);
        $sql = "SELECT AccRg._Period,
            $sql_flds
            AccRg._Account%KtDt%RRef AS Счет,
            AccRg.$fld_KBK"."%KtDt%RRef AS КБК,
            AccRg.".$t['AccRg']['ЕПСБУ']['КВД']['Fld']." AS КВД,
            CAST(%znak% AccRg.".$fld_Сумма." AS NUMERIC(24, 2)) AS Сумма,
            CAST(%znak% AccRg.".$fld_Количество."%KtDt% AS NUMERIC(21, 3)) AS Количество
            FROM ". $table_AccRg ." AccRg WITH(NOLOCK)
            $sql_from
            WHERE AccRg._Active = 0x01 AND AccRg._Account%KtDt%RRef IN
            ($sql_account_ids) AND
            AccRg.".$fld_Учреждение." = ". self::$org;
        //        AND AccRg._Period >= ".$rdate." AND AccRg._Period < ".$mdate;
		

        if ($correspond==0) {
            $sql = str_replace(array('%KtDt%','%znak%','%correspond%'),array('Dt','','0'),$sql);
        } else {
            $sql = str_replace(array('%KtDt%','%znak%','%correspond%'),array('Ct','-','1'),$sql);
        }
        return $sql;
    }
	
	private static function sql_Журнал_Проводок() {
		
		
	}	
	

    private static function sql_part_saldo($table,$account_ids,$lines,$correspond) {
        //,$rdate,$mdate;
		// $table = 'ЕПСБУ'
        $t = &self::$t;
        $fld_Сумма = $t['AccRg'][$table]['Сумма']['Fld'];
        $fld_Количество = $t['AccRg'][$table]['Количество']['Fld'];
        $table_AccRg = $t['AccRg'][$table]['t'];
        $table_ExtDim = $t['Acc'][$table]['t'].$t['ExtDim'][$table]['t'];
        $fld_Учреждение = $t['AccRg'][$table]['Учреждение']['Fld'];
        $table_ВидыСубконтоБюджет = $t['Chrc']['ВидыСубконтоБюджет']['t'];
        $fld_KBK = substr($t['AccRg'][$table]['КБК']['Fld'],0,-4); //убираем Rref

        $s_flds = " ED%i%._Value_TYPE AS Субконто%line%_TYPE, ED%i%._Value_RTRef AS Субконто%line%_RT,	ED%i%._Value_RRRef AS Субконто%line%, ";
        $s_from = " LEFT OUTER JOIN ".$table_ExtDim." ExtDim%i% WITH(NOLOCK)
            ON ExtDim%i%.".$t['Acc'][$table]['t']."_IDRRef = AccRg._Account%KtDt%RRef AND ExtDim%i%._LineNo = %line%
            LEFT OUTER JOIN ".$t['AccRgED'][$table]['t']." ED%i% WITH(NOLOCK)
            ON ED%i%._RecorderTRef = AccRg._RecorderTRef AND ED%i%._RecorderRRef = AccRg._RecorderRRef AND
            ED%i%._LineNo = AccRg._LineNo AND ED%i%._Period = AccRg._Period AND
            ED%i%._Correspond = %correspond% AND ED%i%._KindRRef = ExtDim%i%._DimKindRRef ";

        $sql_flds = '';
        $sql_from = '';
        for ($i=0; $i < count($lines); $i++) {
            $sql_flds .= str_replace(array('%i%','%line%'),array($i,$lines[$i]),$s_flds);
            $sql_from .= str_replace(array('%i%','%line%'),array($i,$lines[$i]),$s_from);
        }
        $sql_account_ids = implode(',', $account_ids);
        $sql = "SELECT AccRg._Period,
            $sql_flds
            AccRg._Account%KtDt%RRef AS Счет,
            AccRg.$fld_KBK"."%KtDt%RRef AS КБК,
            AccRg.".$t['AccRg']['ЕПСБУ']['КВД']['Fld']." AS КВД,
            CAST(%znak% AccRg.".$fld_Сумма." AS NUMERIC(24, 2)) AS Сумма,
            CAST(%znak% AccRg.".$fld_Количество."%KtDt% AS NUMERIC(21, 3)) AS Количество
            FROM ". $table_AccRg ." AccRg WITH(NOLOCK)
            $sql_from
            WHERE AccRg._Active = 0x01 AND AccRg._Account%KtDt%RRef IN
            ($sql_account_ids) AND 
            AccRg.".$fld_Учреждение." = ". self::$org;
        //        AND AccRg._Period >= ".$rdate." AND AccRg._Period < ".$mdate;
        if ($correspond==0) {
            $sql = str_replace(array('%KtDt%','%znak%','%correspond%'),array('Dt','','0'),$sql);
        } else {
            $sql = str_replace(array('%KtDt%','%znak%','%correspond%'),array('Ct','-','1'),$sql);
        }
		
        return $sql;
    }
}
