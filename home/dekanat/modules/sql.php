<?php

/* ���������� global mysqli */

function sql_connect($host,$user,$pass,$db) {
	global $mysqli;
	$mysqli = new mysqli($host,$user,$pass,$db);
	if ($mysqli->connect_error) die('Connection error  (' . $mysqli->connect_errno . ') '. $mysqli->connect_error);
}

function sql_set_charset($charset) {
	global $mysqli;
	$mysqli->set_charset($charset);
}

function sql_query($sql) {
    global $mysqli;
	$result = $mysqli->query($sql) or trigger_error($sql . '<br><br>' .sql_error()) ;
	return $result;
}

function sql_query_error($sql) {
	global $mysqli;
	$result = $mysqli->query($sql);
	$error = '';
	if ($result === false) $error = sql_error();
	return $error;
}

function sql_error() {
	global $mysqli;
	return $mysqli->error;
}

function sql_errno() {
	global $mysqli;
	return $mysqli->errno;
}

function sql_escape($v) {
    global $mysqli;
	if (get_magic_quotes_gpc()) {
		$v = stripslashes($v);
	} else {
        $v = $mysqli->real_escape_string($v);
    }
	return $v;
}

function sql_escape_request() {
	foreach ($_REQUEST as $key=>$v) {
		if (is_array($v)) {
			foreach ($v as $key2=>$v2) {
				$p[$key][$key2] = sql_escape($v2);
			}
		} else $p[$key] = sql_escape($v);
	}
	return $p;
}

function sql_last_id() {
    global $mysqli;
	return	$mysqli->insert_id;
}

function sql_affected_rows() {
	global $mysqli;
	return $mysqli->affected_rows;
}

/* ������ � ������� */

function sql_count_rows($result) {
	return $result->num_rows;
}

function sql_numeric_array($result) {
	return $result->fetch_array(MYSQLI_NUM);
}

function sql_array($result) {
	return $result->fetch_assoc();
}

function sql_get_value($field,$table,$where='') {
    $s = "SELECT $field FROM $table ";
    if (strlen($where)>0) $s.=" WHERE " .$where;
    return sql_fetch_value($s);
}

function sql_fetch_value($sql) {
	$result = sql_query($sql);
	if ($row = sql_numeric_array($result)) {
		return $row[0];
	} else {
		return false;
	}
}

function sql_get_array($table,$where) {
	$s = "SELECT * FROM $table WHERE $where ";
	return sql_fetch_array($s);
}

function sql_get_good_array($table,$where) {
	// ��� ����� row ����������
	$s = "SELECT * FROM $table WHERE $where ";
	$result = sql_query($s);
	if ($row = sql_array($result)) {
		return $row;
	} else {
		$cols = sql_table_columns($table);
		$row = array();
		foreach ($cols as $col) {
			$value = ($col['default']) ? $col['default'] : '';
			$row[$col['name']]=$value;
		}
		return $row;
	}
}

function sql_fetch_array($s) {
	$result = sql_query($s);
	if ($row = sql_array($result)) {
		return $row;
	} else {
		return false;
	}
}

function sql_to_assoc($sql) {
	$res = sql_query($sql);
	$a = array();
	while ($row=sql_numeric_array($res)) {
		$a[(string)$row[0]]=(isset($row[1])) ? $row[1] : $row[0];
	}
	return $a;
}

function sql_rows($sql) {
	$a = array();
	$res = sql_query($sql);
	while ($row = sql_array($res)) {
		$a[] = $row;
	}
	return $a;
}

/* ������ � ������� - ������� �� ������� */

function sql_fields($result) {
	return $result->fetch_fields();
}

function row_fix_numeric($row,$num_flds=array(),$binary_flds=array()) {
	foreach($num_flds as $fld) {
		if (!is_null($row[$fld])) $row[$fld]+=0;
	}
	foreach ($binary_flds as $fld) {
		$row[$fld] = bin2hex($row[$fld]);
	}
	return $row;
}

function sql_rows_fix_numeric($res) {
    $flds=sql_fields($res);
    $num_flds = [];
    $binary_flds = [];
    foreach($flds as $fld) {
        if (sql_is_numeric_field($fld)) $num_flds[] = $fld->name;
        if (sql_is_binary_fld($fld)) $binary_flds[] = $fld->name;
    }
    $data=array();
    while ($row=sql_array($res)) {
    	$data[]=row_fix_numeric($row,$num_flds,$binary_flds);
    }
    return $data;
}

function sql_row_fix_numeric($res) {
    $d = sql_rows_fix_numeric($res);
    return isset($d[0]) ? $d[0] : false;
}

/* ������ � ��������� � ������ */

function sql_table_exists($table) {
	$res = sql_query("SHOW TABLE STATUS LIKE '$table'");
	$row = sql_array($res);
	return ($row) ? true : false;
}

function sql_field_exists($table,$field) {
	$result = sql_query("SHOW COLUMNS FROM $table LIKE '$field'");
	return (sql_count_rows($result))?TRUE:FALSE;
}

function sql_get_autoincrement($table) {
	$res = sql_query("SHOW TABLE STATUS LIKE '$table'");
	$row = sql_array($res);
	return $row['Auto_increment'];
}

function sql_is_numeric_field($fld) {
	$num_types=array(MYSQLI_TYPE_TINY,MYSQLI_TYPE_SHORT,MYSQLI_TYPE_LONG,MYSQLI_TYPE_INT24,MYSQLI_TYPE_LONGLONG,MYSQLI_TYPE_FLOAT,MYSQLI_TYPE_DOUBLE,MYSQLI_TYPE_DECIMAL,MYSQLI_TYPE_NEWDECIMAL);
	return in_array($fld->type ,$num_types);
}

function sql_is_binary_fld($fld) {
    return ($fld->type == MYSQLI_TYPE_STRING) && ($fld->flags &  MYSQLI_BINARY_FLAG);
}

/* ������ � ��������� � ������ - ������� ������� �� ������� */

function sql_table_columns($table) {
	$s = "SHOW COLUMNS FROM $table ";
	$res = sql_query($s);
	$m = array();
	while ($row = sql_array($res)) {
		$field= $row['Field'];
		$m[$field]['name'] = $row['Field'];
		$type=$row['Type'];
        $length=0;
		if (($pos=strpos($type,'('))>0) {
            $length=substr($type,$pos+1,strlen($type)-$pos-2);
			$type=substr($type,0,$pos);	
        }
		$m[$field]['type'] = $type;
		$m[$field]['length'] = $length;
		$m[$field]['null'] = $row['Null']=='YES' ? 1: 0;
		$m[$field]['key'] = $row['Key'];
		$m[$field]['extra'] = $row['Extra'];
		$m[$field]['default'] = $row['Default'];
	}
	return $m;
}

function sql_table_defaults($table) {
	$cols = sql_table_columns($table);
	$m = array();
	foreach ($cols as $col=>$v) {
		$m[$col]=$v['default'];
	}
	return $m;
}

function sql_table_field_default($table,$field) {
	$defaults = sql_table_defaults($table);
	$default = '';
	if (array_key_exists($field, $defaults)) {
		$default = $defaults[$field];
	}
	return $default;
}

function sql_table_key($table) {
    $a=sql_table_columns($table);
    $pk=array();
    foreach ($a as $fld) {
        if ($fld['key']=='PRI') {
            $pk[]=$fld['name'];
        }
    }
    return (count($pk)==1) ? $pk[0] : $pk;
}

function sql_table_has_references($table) {
    $res=sql_query("select * from information_schema.REFERENTIAL_CONSTRAINTS where REFERENCED_TABLE_NAME ='$table'");
    return sql_count_rows($res)>0;
}

/* ������ � ������ */

function sql_date2rus($d) {
	//����������� ���� �� ������� MYSQL yyyy-mm-dd ->dd.mm.yyyy
	if (!empty($d) && strlen($d)==10) {
		$ret=substr($d,8,2) .'.' . substr($d,5,2) . '.' . substr($d,0,4);
	} else {
		$ret='';
	}	
	return  $ret;
}
function sql_rus2date($d) {
	//����������� ���� dd.mm.yyyy->yyyy-mm-dd
	return (!empty($d)) ? "'". substr($d,6,4) .'-' . substr($d,3,2) . '-' . substr($d,0,2) ."'" : '';
}

function sql_date2year($d) {
	return ($d) ? substr($d,0,4) : '';
}

function sql_rus2year($d) {
	return ($d) ? substr($d,6,4) : '';
}
?>