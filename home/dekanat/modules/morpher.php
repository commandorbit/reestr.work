<?php

function morpher_inflect($text, $padeg, $encoding)   {
    if ($encoding) {
        $text = iconv ($encoding,'utf-8',$text);
        $padeg = iconv ($encoding,'utf-8',$padeg);
    }
    $credentials = array('Username'=>'test','Password'=>'test');

    $header = new SOAPHeader('http://morpher.ru/','Credentials', $credentials);
    $url = 'http://morpher.ru/WebService.asmx?WSDL';
    $client = new SoapClient($url);
    $client->__setSoapHeaders($header);
    $params = array('parameters'=>array('s'=>$text));
    $result = (array) $client->__soapCall('GetXml', $params);
    $singular = (array) $result['GetXmlResult'];
    $res = $singular[$padeg];
    if ($encoding) {
        $res = iconv('utf-8',$encoding,$res);
    }
    return $res;
}

?>