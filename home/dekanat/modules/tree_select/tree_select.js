﻿function createTreeControl(name,options,selected) {
	var ctrl = document.querySelector("[data-name='"+name+"']");
	var L = libUnicorn;
	var treePlugin = L.TreePlugin.create(ctrl, false, {displaySelectedOptions: true, opened: false, checkboxes: true});
	new resizable(treePlugin.container, {directions: ["southeast"]});
	treePlugin.setOptions(options);
	treePlugin.setSelected(selected);
	return ctrl;
}

function treeControlSetFormInputs(form,ctrl,name) {
	var selected = libUnicorn.getControl(ctrl).getSelectedOptions();
	for (var j =0 ; j<selected.length; j++) {
		var inp =document.createElement("input");
		inp.type = "hidden";
		inp.value = selected[j].id;
		inp.name = name +"[]";
		form.appendChild(inp);
	}
}

function formSubmit(form) {
	var ctrls = form.querySelectorAll(".mst-control");
	for (var i=0;i<ctrls.length;i++) {
		treeControlSetFormInputs(form,ctrls[i],ctrls[i].dataset.name);
	}
	form.submit();
}