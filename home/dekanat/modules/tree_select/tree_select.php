<script charset="utf-8" src="//cdn.unecon.ru/js/libunicorn/v1/multiselect.js?ver=222" type="text/javascript" ></script>
<script charset="utf-8" src="//cdn.unecon.ru/js/libunicorn/v1/treeplugin.js?ver=222" type="text/javascript" ></script>
<script charset="utf-8" src="//cdn.unecon.ru/js/libunicorn/v1/resizeplugin.js?ver=222" type="text/javascript" ></script>
<script charset="utf-8" src="//dekanat.unecon.ru/modules/tree_select/tree_select.js?ver=222" type="text/javascript" ></script>

<link rel="stylesheet" type="text/css" href="//cdn.unecon.ru/css/libunicorn/v1/multiselect.css?ver=222">
<link rel="stylesheet" type="text/css" href="//cdn.unecon.ru/css/libunicorn/v1/treeplugin.css?ver=222">
<link rel="stylesheet" type="text/css" href="//cdn.unecon.ru/css/libunicorn/v1/resizeplugin.css?ver=222">
<?php

class tree_select {
	public static function print_select($name,$label='') {
		echo self::create_select($name,$label);
	}

	public static function create_select($name,$label='') {
		$id = 'tree_'.$name.'_id';
		$s = '';
		if ($label) $s .= '<label for="'.$id.'">'.$label.': </label>';
		$s .= '<span class="mst-control" data-name="'.$name.'"></span>';
		return $s;
	}

	public static function fill_selected($selected) {
		$new = [];
		if (is_array($selected)) foreach ($selected as $v) if (is_numeric($v)) $new[] = $v;
		elseif (is_numeric($selected)) $new[] = $selected;
		return $new; 
	}
}

?>