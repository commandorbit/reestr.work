<?php

function print_header() {
	global $script_name, $original_user_kod, $page_head;
	require $_SERVER['DOCUMENT_ROOT'].'/template/header_utf.php';
}

function print_footer() {
	require $_SERVER['DOCUMENT_ROOT'].'/template/footer_utf.php';
}

function print_menu() {
	global $script_name, $original_user_kod, $page_head;
	require $_SERVER['DOCUMENT_ROOT'].'/menu_utf.php';
}

class dekanat_permission {
	public static function permission_id($name) {
		return sql_get_value("id","`dekanat`.permission","name='$name'");
	}

	public static function permission_name($id) {
		return sql_get_value("name","`dekanat`.permission","id='$id'");
	}

	public static function permission_info($id) {
		return sql_get_array("`dekanat`.permission","id='$id'");
	}

	public static function permission_options($id) {
		$row = self::permission_info($id);
		$class = get_class();
		$method = $row['func_options'].'_assoc';
		if ($row['has_value'] && method_exists($class,$method)) {
			return call_user_func(array($class,$method));
		} else {
			return false;
		}
	}

	public static function permission_has_option($id,$value) {
		$options = self::permission_options($id);
		return (array_key_exists($value,$options));
	}

	public static function v_prikaz_approver() {
		return sql_rows("SELECT approver_kod as id, approver_name as name 
			FROM `dekanat`.s_prikaz_approver ");
	}

	public static function v_prikaz_approver_assoc() {
		return sql_to_assoc("SELECT approver_kod, approver_name 
			FROM `dekanat`.s_prikaz_approver ORDER BY approver_name");
	}
}

// пользователи деканата и права

function user_info($user_kod) {
	return sql_get_array("`users`","user_kod='$user_kod'");
}

function staff_user_id_user_kod($staff_user_id) {
	return sql_get_value("user_kod","`dekanat`.`users`","staff_user_id='$staff_user_id'");
}

function staff_user_id_actual_user_kod($staff_user_id) {
	return sql_get_value("user_kod","`dekanat`.`users`","staff_user_id='$staff_user_id' and is_actual=1");
}

function user_fio($user_kod) {
	$user = user_info($user_kod);
	return $user['user_fio'];
}

function current_user_kod() {
	if ($user_kod = session_val('dek_user_kod')) {
		return $user_kod;
	} elseif ($user_kod = staff_user_id_actual_user_kod(session_val('staff_user_id'))) {
		$_SESSION['dek_user_kod'] = $user_kod;
		logger::set_session('','','LOGIN_BY_STAFF_ID',$user_kod);
		return $user_kod;
	} else {
		return false;
	}
}

function current_session_id() {
	return session_val('session_id');
}

function current_staff_user_id() {
	return session_val('staff_user_id');
}

function current_user_info() {
	return user_info(current_user_kod());
}

function current_user_fio() {
	return user_fio(current_user_kod());
}

function current_user_inst_kod() {
	$user_info = current_user_info();
	return $user_info['inst_kod'];
}

// права

function user_fakultet_list($user_kod) {
	$user_info = user_info($user_kod);
	$fakultet_group_kod = $user_info['fakultet_group_kod'];
	$sql = "SELECT fakultet_kod FROM fakultet_group_sostav WHERE fakultet_group_kod='$fakultet_group_kod'";
	$res = sql_query($sql);
	$fakultets = array();
	while ($row = sql_array($res)) {
		$fakultets[]=$row['fakultet_kod'];
	}
	return $fakultets;
}

function user_default_fakultet_kod($user_kod) {
	$user_info = user_info($user_kod);
	$fakultet_group_kod = $user_info['fakultet_group_kod'];
	$sql = "SELECT fakultet_kod FROM fakultet_group_sostav 
	WHERE is_default=1 AND fakultet_group_kod='$fakultet_group_kod'";
	$fakultet_kod = sql_fetch_value($sql);
	// 0 - не определен
	return ($fakultet_kod) ? $fakultet_kod : 0;
}

function user_fakultet_list_sql($user_kod) {
	$fakultets = user_fakultet_list($user_kod);
	return ($fakultets) ? implode(",",$fakultets) : '-1';
}

function user_is_aspirantura($user_kod) {
	$fakultets = user_fakultet_list($user_kod);
	$is = (count($fakultets)==1 && $fakultets[0]==30);
	return $is;
}

function user_is_technicum($user_kod) {
	$fakultets = user_fakultet_list($user_kod);
	$is = (count($fakultets)==1 && in_array($fakultets[0],[41,42,43]));
	return $is;
}

function user_is_aspir($user_kod) {
	return user_is_aspirantura($user_kod);
}

function user_is_miep($user_kod) {
	$fakultets = user_fakultet_list($user_kod);
	$is = (count($fakultets)==1 && $fakultets[0]==29);
	return $is;
}

function user_is_filial($user_kod) {
	$fakultets = user_fakultet_list($user_kod);
	// группа факультетов = "филиалы" (3) или единственный факультет в группе - филиал
	$is = false;
	if (user_may($user_kod,'fakultet_group_kod')==3) {
		$is = true;
	} else {
		foreach ($fakultets as $fakultet_kod) {
			if (fakultet_is_filial($fakultet_kod)) $is = true;
			else break;
		}

	}
	return $is;
}

function flag_is_not_ro($flag) {
	$flags_not_ro = array(
		'may_stud_photo',
		'may_add_oc_docs',
		'may_edit_stud_diplom',
		'may_hotel_dogovor',
		'may_edit_hotel',
		'may_edit_room',
		'may_edit_up',
		'may_edit_grp',
		'may_edit_upp',
		'may_edit_cd',
		'may_add_stud',
		'may_edit_abitur',
		'may_edit_abitur_obraz',
		'may_edit_stud_postupil',
		'may_edit_stud_oc_vuz',
		'may_edit_decanat_stud',
		'may_edit_nz'
	);
	return in_array($flag,$flags_not_ro);
}

function user_may($user_kod,$flag,$value = '') {
	if ($id = dekanat_permission::permission_id($flag)) {
		if ($value && dekanat_permission::permission_has_option($id,$value)) {
			$may = sql_get_value("count(*)","user_permission","user_id='$user_kod' and permission_id='$id' and value='$value' ");
		} else {
			$may = sql_get_value("count(*)","user_permission","user_id='$user_kod' and permission_id='$id' and value is null ");
		}
	} else {
		if (!$flag || !sql_field_exists('users',$flag)) die('flag "'.$flag.'"" is not exists!');
		$user_info = user_info($user_kod);
		if ($user_info['is_ro'] && flag_is_not_ro($flag)) {
			$may = false;
		} else {
			$may = $user_info[$flag];
		}
	}
	return $may;
}

function current_user_may($flag,$value = '') {
	return user_may(current_user_kod(),$flag,$value);
}

function user_is_interdekanat($user_kod) {
	return user_may($user_kod,'is_interdekanat');
}

function user_is_admin($user_kod) {
	return user_may($user_kod,'is_admin');
}

function current_user_is_admin(){
	return user_is_admin(current_user_kod());
}

function current_user_is_aspir(){
	return user_is_aspir(current_user_kod());
}

function user_is_kadr($user_kod) {
	return user_may($user_kod,'is_ok');
}

function user_is_ro($user_kod) {
	return user_may($user_kod,'is_ro');
}

function current_user_is_ro() {
	return user_is_ro(current_user_kod());
}

function user_may_stud($user_kod,$stud_kod) {
	$may = false;
	if (stud_exists($stud_kod)) {
		$stud_fakultet_kod = stud_fakultet_kod($stud_kod);
		$stud_fact_fakultet_kod = stud_fact_fakultet_kod($stud_kod);
		$user_fakultets = user_fakultet_list($user_kod);
		$may = (in_array($stud_fakultet_kod,$user_fakultets) || in_array($stud_fact_fakultet_kod,$user_fakultets));
	}
	return $may;
}

function current_user_may_stud($stud_kod) {
	return user_may_stud(current_user_kod(),$stud_kod);
}

function user_has_admin_menu($user_kod) {
	$uPerms = sql_rows("SELECT up.permission_id FROM user_permission up
		INNER JOIN permission p ON up.permission_id=p.id  
		WHERE up.user_id='$user_kod' AND p.in_admin_menu_section=1 ");
	$has = false;
	foreach ($uPerms as $uPerm) {
		$flag = dekanat_permission::permission_name($uPerm['permission_id']);
		if (user_may($user_kod,$flag)) {
			$has = true;
			break;
		}
	}
	return $has;
}


function current_user_has_admin_menu() {
	return user_has_admin_menu(current_user_kod());
}

function user_may_edit_stud($user_kod,$stud_kod,$flag) {
	$may_stud = false;
	if (user_may_stud($user_kod,$stud_kod)) {
		$may_stud = (user_is_interdekanat($user_kod)) ? stud_is_interdekanat($stud_kod) : true;
	}
	return ($may_stud && user_may($user_kod,$flag) && !user_is_ro($user_kod));
}

function current_user_may_edit_stud($stud_kod,$flag) {
	return user_may_edit_stud(current_user_kod(),$stud_kod,$flag);
}

function current_user_may_edit_stud_prikaz_zach($stud_kod) {
	$may = false;
	if (current_user_may_edit_stud($stud_kod,'may_add_prikaz')) {
		$user_kod = current_user_kod();
		$may = user_is_miep($user_kod) || user_is_aspirantura($user_kod) || user_is_admin($user_kod) || user_is_filial($user_kod) || stud_is_kursy($stud_kod);
	}
	return $may;
}

function current_user_may_edit_stud_prikaz_stip($stud_kod) {
	$may = false;
	if (current_user_may_edit_stud($stud_kod,'may_add_prikaz')) {
		$may = stud_is_budget($stud_kod) || stud_is_goslinia($stud_kod);
	}
	return $may;
}

function current_user_may_edit_stud_prikaz($stud_kod,$prikaz_type_kod) {
	$may = current_user_may_edit_stud($stud_kod,'may_add_prikaz');
	if ($may) {
		if (prikaz_type_is_zach($prikaz_type_kod)) $may = current_user_may_edit_stud_prikaz_zach($stud_kod);
		elseif (prikaz_type_is_stip($prikaz_type_kod)) $may = current_user_may_edit_stud_prikaz_stip($stud_kod);
	}
	return $may;
}

function current_user_may_approve_prikaz($prikaz_nomer_kod) {
	$a = prikaz_approvers($prikaz_nomer_kod);
	if ($a) {
		$may = true;
		foreach ($a as $approver_kod) {
			if (!current_user_may('may_approve_prikaz',$approver_kod)) $may = false;
		}
	} else {
		$may = false;
	}
	
	return $may;
}

function print_data_table_admin_menu() {
	$dTables = sql_rows("SELECT * FROM data_table WHERE id NOT IN (1) ORDER BY title");
	$s = '';
	foreach ($dTables as $dTable) {
		$permission_name = dekanat_permission::permission_name($dTable['permission_id']);
		if (current_user_may($permission_name)) {
			$s .= '<a href="/admin/editor.php?d='.$dTable['name'].'">'.$dTable['title'].'</a><br>';
		}
	}
	return $s;
}

function nomer_zach_is_exists($nomer_zach,$stud_kod) {
	$sql = "SELECT count(*) FROM decanat_stud 
		WHERE nomer_zach = '$nomer_zach' AND stud_kod <> '$stud_kod'";
	$is = sql_fetch_value($sql);
	return $is;
}

function nomer_zach_exists_error($nomer_zach,$stud_kod) {
	$error = '';
	if (nomer_zach_is_exists($nomer_zach,$stud_kod)) {
		$ds = sql_get_array("decanat_stud","nomer_zach = '$nomer_zach'");
		$error .= "Нарушена уникальность номера зачетной книжки!\n";
		$error .= 'Номер '.$nomer_zach.' использует '.stud_fio($ds['stud_kod']).' ('.fakultet_name($ds['fakultet_kod']).')';
	}
	return $error;
}

?>