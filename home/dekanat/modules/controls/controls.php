<?php

$remote_addr = (isset($_SERVER) && array_key_exists('REMOTE_ADDR',$_SERVER)) 
	? $_SERVER['REMOTE_ADDR'] 
	: '';

$developer_ip = '10.123.124.210';

$stable_version = '17.07.24';
$beta_version = '17.07.24';

$ver = ($remote_addr == $developer_ip)
	? $beta_version
	: $stable_version;

require_once('controls.'.$ver.'.php');

?>