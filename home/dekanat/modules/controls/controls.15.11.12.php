<?php 
class controls {
	/* 
	attr = array('class'=>'','id'=>'', 'style'=>'', � ������ �������������� ���������) 
	*/
	public $may_edit = true;
	private $use_labels = true;
	
	public function set_may_edit($may) {
		$this->may_edit = $may;
		return $this;
	}
	
	public function set_use_labels($use) {
		$this->use_labels = $use;
		return $this;
	}
	
	public function print_select_label($name,$options,$value,$label,$attrs = array()) {
		$for_id = (isset($attrs['id'])) ? $attrs['id'] : 'id_'.$name;
		$this->print_label($name,$label,array('for'=>$for_id));
		echo $this->select($name,$options,$value,$attrs);
		echo '<br>';
	}

	public function print_checkbox_label($name,$value,$label,$attrs = array()) {
		$for_id = (isset($attrs['id'])) ? $attrs['id'] : 'id_'.$name;
		$this->print_label($name,$label,array('for'=>$for_id));
		echo $this->checkbox($name,$value,$attrs);
		echo '<br>';
	}
	
	public function print_input_label($name,$value,$label,$attrs = array()) {
		$for_id = (isset($attrs['id'])) ? $attrs['id'] : 'id_'.$name;
		$this->print_label($name,$label,array('for'=>$for_id));
		echo $this->input($name,$value,$attrs);
		echo '<br>';
	}

	public function print_textarea_label($name,$value,$label,$attrs = array()) {
		$for_id = (isset($attrs['id'])) ? $attrs['id'] : 'id_'.$name;
		$this->print_label($name,$label,array('for'=>$for_id));
		echo $this->textarea($name,$value,$attrs);
		echo '<br>';
	}

	public function print_textarea_label_p($name,$value,$label,$attrs = array()) {
		echo '<p>';
		$this->print_textarea_label($name,$value,$label,$attrs);
		echo '</p>';
	}
	
	public function print_submit($name,$value = '���������',$attrs = array()) {
		if ($this->may_edit) echo '<input type="submit" name="'.$name.'" value="'.$value.'"'.$this->string_attrs($attrs).'>';
	}

	public function print_button($name,$value = '',$attrs = array()) {
		if ($this->may_edit) echo '<input type="button" name="'.$name.'" value="'.$value.'"'.$this->string_attrs($attrs).'>';
	}
	
	public function print_select($name,$options,$value,$attrs = array()) {
		echo $this->select($name,$options,$value,$attrs);
	}

	public function print_checkbox($name,$value,$attrs = array()) {
		echo $this->checkbox($name,$value,$attrs);
	}
	
	public function print_input($name,$value,$attrs = array()) {
		echo $this->input($name,$value,$attrs);
	}

	public function print_textarea($name,$value,$attrs = array()) {
		echo $this->textarea($name,$value,$attrs);
	}
	
	public function print_input_hidden($name,$value) {
		echo $this->input_hidden($name,$value);
	}
	
	public function print_label($name,$label,$attrs = array()) {
		echo $this->label($name,$label,$attrs);
	}
	
	public function select($name,$options,$value,$attrs = array()) {
		$s_attrs = ($this->may_edit) ? $this->string_attrs($attrs) : '';
		$s_id = ($this->use_labels && !isset($attrs['id'])) ? ' id="id_'.$name.'"' : '';
		
		if ($this->may_edit) {
			if (is_array($options)) {
				$s_options = $this->options($options,$value);
			} else {
				$s_options = $options;
			}
			$s = '<select name="'.$name.'"'.$s_attrs.$s_id.'>'.$s_options."</select>\n";
		} else {
			$s = '<span'.$s_attrs.$s_id.'>'.$this->selected_value($options,$value)."</span>\n";
		}
		return $s;
	}

	public function checkbox($name,$value,$attrs = array()) {
		$s_attrs = ($this->may_edit) ? $this->string_attrs($attrs) : '';
		$s_id = ($this->use_labels && !isset($attrs['id'])) ? ' id="id_'.$name.'"' : '';
		if ($this->may_edit) {
			$selected = (!$value) ? '' : ' checked="checked" ';
			$s = '<input type="checkbox" name="'.$name.'" value="1"'.$selected.$s_attrs.$s_id.">\n";
		} else {
			$value = (!$value) ? '���' : '��';
			$s = '<span'.$s_attrs.$s_id.'>'.$value."</span>\n";
		}
		return $s;
	}
	
	public function input($name,$value,$attrs = array()) {
		$s_attrs = ($this->may_edit) ? $this->string_attrs($attrs) : '';
		$s_id = ($this->use_labels && !isset($attrs['id'])) ? ' id="id_'.$name.'"' : '';
		
		if ($this->may_edit) {
			$s = '<input type="text" name="'.$name.'" value="'.html_special_chars($value).'"'.$s_attrs.$s_id.">\n";
		} else {
			$s = '<span'.$s_attrs.$s_id.'>'.$value."</span>\n";
		}
		return $s;
	}
	
	public function input_hidden($name,$value,$attrs = array()) {
		$s_attrs = $this->string_attrs($attrs);
		$s_id = ($this->use_labels && !isset($attrs['id'])) ? ' id="id_'.$name.'_hidden"' : '';
		
		$s = '';
		if ($this->may_edit) {
			$s = '<input type="hidden" name="'.$name.'" value="'.html_special_chars($value).'"'.$s_id.">\n";
		}
		return $s;
	}

	public function textarea($name,$value,$attrs = array()) {
		$s_attrs = ($this->may_edit) ? $this->string_attrs($attrs) : '';
		$s_id = ($this->use_labels && !isset($attrs['id'])) ? ' id="id_'.$name.'"' : '';
		
		if ($this->may_edit) {
			$s = '<textarea name="'.$name.'" '.$s_attrs.$s_id.'>'.html_special_chars($value)."</textarea>\n";
		} else {
			$s = '<span'.$s_attrs.$s_id.'>'.$value."</span>\n";
		}
		return $s;
	}
	
	public function label($name,$label,$attrs = array()) {
		if (!isset($attrs['for']) && $this->may_edit) $attrs['for']='id_'.$name;
		$s_attrs = $this->string_attrs($attrs);
		if ($this->use_labels) {
			$s = '<label'.$s_attrs.'>'.$label.': </label>';
		} else {
			$s = '<span'.$s_attrs.'>' . $label . '</span>';
		}
		return $s;
	}
	
	private function string_attrs($attrs) {
		$s = '';
		foreach ($attrs as $attr=>$value) {
			$s .= ' ' . $attr . '="' . $value . '"';
		}
		return $s;
	}
	
	private function selected_value($options,$v) {
		$a = array();
		foreach ($options as $id=>$name) {
			if ((is_array($v) && in_array($id,$v)) || (string)$id == (string)$v) {
				$a[] = $name;
			}
		}
		$s = implode("; ",$a);
		return $s;
	}
	
	private function options($options,$v) {
		$s = '';
		foreach ($options as $id=>$name) {
			if (is_array($v))
				$selected = in_array($id,$v) ? 'selected' : '';
			else
				$selected = ((string)$id == (string)$v) ? 'selected' : ''; 
			$s .= '<option ' . $selected . ' value="' . $id . '">' . $name . "</option>\n";
			
		}
		return $s;
	}

	public function print_options($options,$v) {
		echo $this->options($options,$v);
	}
}


?>