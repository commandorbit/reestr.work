<?php
error_reporting (E_ALL | E_STRICT);
require 'modules/sql_connect.php';
require 'modules/sql.php';
require 'modules/avdfunctions.php';

db_connect();

if (isset($_REQUEST['table']))
	$table = sql_escape($_REQUEST['table']);
else
	$table = 'holiday';

$columns = sql_table_columns($table);
//avd_print_assoc_array($columns); 
//echo '<pre>'; var_dump($columns); die();

if (isset($_REQUEST['queryfield']))
	$queryfield = $_REQUEST['queryfield'];
else
	$queryfield = 'name';

if (isset($_REQUEST['idfield']))
	$idfield = $_REQUEST['idfield'];
else
	$idfield = 'id';

$table = sql_escape($table);

if ($_SERVER['REQUEST_METHOD'] === 'POST') 
{
	//ob_start();
	// Получение данных с клиента на сервер
	//var_dump($_POST);
	//echo "$table\n\n";
	//echo "table = $table\n";
	$json = file_get_contents('php://input');
	$srcdata = json_decode($json);
	if (is_array($srcdata))
		$dataarray = $srcdata;
	else
		$dataarray = array('DATA' => $srcdata);
	$message = "";
	$success = true;
	$records = Array();
	sql_query("START TRANSACTION");
	foreach($dataarray as $dataindex => $data)
	{
		//echo "dataindex = $dataindex\n";
		$idvalue = 0;
		$fieldlist = array();
		$valuelist = array();
		//var_dump($data);
		foreach($data as $i => $v)
			if ($i == $idfield) 
				$idvalue = sql_escape($v);
			else
			{
				if ($columns[$i]['type'] == 'date')
				{
					$v = date("Y-m-d H:i:s", $v);
					$data->$i = $v;
				}
				//if (!is_null($v))
				$fieldlist[] = $i;
				if ($v)
					$valuelist[] = "'" . sql_escape($v) . "'";
				else
				{
					if ($columns[$i]['type'] == 'tinyint')
						$valuelist[] = '0';
					else if ($columns[$i]['type'] == 'int')
						$valuelist[] = 'NULL';
					else
						$valuelist[] = 'NULL';
				}
			}
		//echo "$idfield = $idvalue\n";
		$exprlist = array();
		for ($i = 0; $i < count($fieldlist); $i++)
			$exprlist[] = "${fieldlist[$i]} = ${valuelist[$i]}";	
		if ($idvalue >= 0)
		{
			if ($exprlist)
			{
				$sql = "UPDATE $table SET " . implode(", ", $exprlist) . " WHERE $idfield = '$idvalue'";
				$message .= "Обновление таблицы $table";
			}
			else
			{
				$sql = "DELETE FROM $table WHERE $idfield = '$idvalue'";			
				$message .= "Удаление из таблицы $table";
			}
		}
		else
		{
			$sql = "INSERT INTO $table (" . implode(", ", $fieldlist) . ") VALUES (" . implode(", ", $valuelist) . ")";
			$message .= "Добавление в таблицу $table";
			//$message .= "<br><br>$sql<br><br>";
		}
		//$message .= "\n$sql\n";
		//$success = $success && sql_query($sql);
		/**/
		try 
		{
			$r = sql_execute($sql);
			$success = $success && $r;
			// insert operation			
			//if ($success && $idvalue < 0) 
			if ($r && $idvalue < 0)
			{
				$data->clientId = $idvalue;
				$data->id = sql_last_id();
			}
			$message .= ", $idfield: $data->id";
			if ($r)
				$message .= " - успешное выполнение!\n";
			else
				$message .= " - ошибка:\n" . sql_error() . "\n";
		} 
		catch (Exception $e) 
		{
			$success = false;
			$message .= $e->getMessage() . "\n";
		}		
		if ($r) 
			$records[] = $data;
		/**/
		//echo "Результат: $success \n";
		//echo json_encode(['success' => false, 'message' => 'SOME ERROR!']);
		//echo 'Результат: ' . $result . "\n";
		//avd_printarrayvalues_r($data);
	}
	//$message = ob_get_clean();
	//ob_get_clean();
	$message = str_replace("\n", "<br>", $message);
	//$success = true;
	if ($success) {
		sql_query("COMMIT");
	} else {
		sql_query("ROLLBACK");
	}
	echo json_encode(array('success' => $success, 'message' => $message, 'records' => $records /*, 'records' => [['id' => $idvalue, 'subject' => 'My PHP SUBJECT']]*/ ));
	exit();
}

$sqltext = "SELECT * FROM $table WHERE 1 = 1";

/*
if (isset($_REQUEST['id']))
{
	$id = sql_escape($_REQUEST['id']);
	$sqltext .= " AND id = '$id'";
}
*/

foreach ($_REQUEST as $i => $v)
{
	$i = sql_escape($i);
	$v = sql_escape($v);
	//if (!in_array($i, Array('table', 'idfield', 'queryfield', 'query', '_dc', 'page', 'start', 'limit', 'test', 'callback', 'clientId')))
	if (isset($columns[$i])) {
		if ($v != 'root')
			$sqltext .= " AND $i = '$v'";
		else
			$sqltext .= " AND $i IS NULL";
	}
}

if (isset($_REQUEST['where']))
	//$sqltext .= ' AND ' . sql_escape($_REQUEST['where']);
	$sqltext .= ' AND ' . $_REQUEST['where'];

if (isset($_REQUEST['query']))
{
	$q = sql_escape($_REQUEST['query']);
	$sqltext .= " AND $queryfield like '%$q%'";
}

//die($sqltext);

if (isset($_REQUEST['orderby']))
	$sqltext .= ' ORDER BY ' . sql_escape($_REQUEST['orderby']);
else
    if (isset($columns['name'])) 
	    $sqltext .= " ORDER BY name";
 	
//die($sqltext);

$r = sql_rows($sqltext);

//Очищаем колонки binary
//if ($columns[$i]['type'] == 'binary')
foreach ($r as &$row) 
	foreach ($row as $i => &$v) 
		if ($columns[$i]['type'] == 'binary') 
			$v = bin2hex($row[$i]);
			//$row[$i] = bin2hex($row[$i]);
			//$v = null;

/*
foreach ($r as &$row) 
	if (isset($row['d'])) 
		$row['dd'] =  $row['d'] ? new DateTime($row['d']) : null;
*/

//var_dump($r); die();

//var_dump([$_REQUEST]);
if (isset($_REQUEST['test']))
{
	echo '<HTML><BODY>'; 
	avd_print_assoc_array($r); 
	echo '</BODY></HTML>'; 
}	
else 
	if ($table == 'holiday')
		echo json_encode($r);
	else
		echo json_encode(['records' => $r]);
?>