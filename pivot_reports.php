<?php
include $_SERVER['DOCUMENT_ROOT'] . "/init.php";

if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) && !empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
	$result = NULL;
	$result['error'] = false;
	$request = $_REQUEST;
	foreach($request as $key=>&$value) {
		$value = sql_escape($value);
	}
	$type = $request['type'];
	if($type === 'get') {
		$result = sql_array(sql_query("select id, settings, filters, name from pivot_reports where id = '${request['id']}' and pivot_type='${request['pivot_type']}'"));
	} else if($type === 'save') {
		$sql = "insert into pivot_reports (name, settings, filters, user_id, pivot_type, created_at) value('${request['name']}', '${request['settings']}', '${request['filters']}', '${_SESSION['user_id']}', '${request['pivot_type']}', NOW())";
		$result = sql_query($sql); 
	} else if($type === 'update_report_name') {
		$sql = "update pivot_reports set name = '${request['name']}' where id = '${request['id']}' and pivot_type='${request['pivot_type']}'";
		$result = sql_query($sql);
	} else if($type === 'update_report') {
		$sql = "update pivot_reports set name = '${request['name']}', settings = '${request['settings']}', filters = '${request['filters']}' where id = '${request['id']}' and pivot_type='${request['pivot_type']}'";
		$result = sql_query($sql);
	} else if($type == 'delete') {
		$sql = "delete from pivot_reports where id = '${request['id']}' and pivot_type='${request['pivot_type']}'";
		$result = sql_query($sql);
	}
	print_r(json_encode($result));
}

?>