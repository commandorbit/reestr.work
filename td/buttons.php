<link rel="stylesheet" type="text/css" href="/td/css/buttons.css">
<div class="but_panel button-container">
    <div class="button-group">
    <?
	
	$forbidden_add = array( 'v_dogovor',
							'v_zayav', 
							'v_zfo_limit',
							'virt_dogovor_sostav_rep',
							'v_report_zayav',
							'v_737',
							'v_sverka_kosgu_if',
							'doc_lim_change',
							'stat_738_ver',
							'v_pfhd_2016'
						);
	$custom_add = array(
				
	);
/*
	// Убили view v_pfhd_2016 2017.03.09
	if(is_admin() && $this->table == 'v_pfhd_2016') {
        echo '<a href="/asu_pfhd/save_pfhd_2016_ver.php" onclick="savePfhdVer(this); return false;" class="td-sava-pfhd-ver button icon add green primary" title="Добавить запись">Сохранить версию</a>';
    }
*/	
    if ($this->may_edit && !in_array($this->table, $forbidden_add))
        echo '<a class="td-add-button button icon add green primary" title="Добавить запись">Добавить</a>';
	if ($this->may_edit && in_array($this->table, $custom_add))
        echo '<a class="td-add-button-custom button icon add green primary" title="Добавить запись">Добавить</a>';
	
	$table = isset($_GET['t']) ? sql_escape($_GET['t']) : '';
	if (may_merge() && sql_table_has_references($table)) {
        echo '<a class="td-merge-button button icon chat green primary" title="Объединить записи">Объединить</a>';
    }
    ?>
    <a class="td-xls-button button green primary">XLS</a>
    <a class="td-show-full button green primary">Показать полные тексты</a>
    </div>
    <input name="search" class="but_search"  style="text-align:left; padding-left:4px;" placeholder="Искать в таблице..." type="search" ></input>
    <span class="td-info-found">Показывать:</span>
    <select name="maxrows" class="but_search" title="Показывать не более"><option value="100">100</option><option value="500">500</option><option value="1000">1000</option><option value="_all_">All</option></select>

    <span class="show-columns button green primary">Колонки<span class="td-drop-down-arrow"></span></span>
    <ul class="show-columns" style="display:none;">
    <? foreach ($this->columns as $col) {
        $class = !$col['hidden'] ? ' class="selected" ' : '';
        echo '<li ' . $class . ' data-fld-name="'.$col['name'].'">' . $col['label'] . '</li>';
       }
    ?>
    </ul>

    <a class="td-filters-remove-button button icon remove green primary" title="Снять все фильтры">Снять все фильтры</a>
	
	<? 
	if (is_admin() && $this->table == 'v_dogovor') {
        //echo '<a href="" onclick="setStatusInPFHD(); return false;" class="td-sava-pfhd-ver button icon add green primary">Включить в ПФХД</a>';
    }	
	?> 
	<script>
	function setStatusInPFHD() {
		var rows = TDv_dogovor.filteredDataSearched;
		var ids = [];
		for (var i=0 ;i<rows.length; i++) {
			ids.push("dogovor_id[]=" + rows[i].id);
		}
		var post_data = ids.join("&");		
		console.log(post_data);
		var STATUS_IN_PFHD = 4;
		var xhttp = new XMLHttpRequest();
		xhttp.onreadystatechange = function() {			
			if (this.readyState == 4 && this.status == 200) {
				console.log(this.responseText);
				var resp = JSON.parse(this.responseText);
				if (resp.status == 'OK') {
					for (var i=0; i< rows.length; i++) {
						rows[i]['dogovor_status_id'] = STATUS_IN_PFHD;
						rows[i].updated = true;
					}
					TDv_dogovor.redraw();
					alert('OK');
						
				} else {
					alert(resp.msg || 'Error' );
				}
			}
		};
		xhttp.open("POST", "/alcher/setStatusInPFHD.php", true);
		xhttp.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
		xhttp.send(post_data);
	}
	</script>
  <!--  <div id="but_filter_clear" class="button icon loop green primary">Очистить фильтры</div> -->
</div>

