<?php
require_once dirname(__FILE__) .'/td.php';

function newTDfiltered($table,$array=null) {
    if (!$array) $array=$_GET;
    return newTD($table,$array,true);
} 
/* by default filter =$_GET */
 
function newTD($table,$filter_array=false) {
	global $DB;
    if ($filter_array==false) $filter_array=$_GET;
	//$DB = new ReestrDB();
	$order = str_param('order','');
	$order_desc = int_param('order_desc',1);	
    $primary_key = $DB::is_virtual_table($table) ? null : sql_table_key($table);
    if ($order == '' && $primary_key) {
        $order = $primary_key;
        $order_desc = 1;
    }
	$params=array('t'=>$table,'order'=>$order,'order_desc'=>$order_desc);
    $TDfilter=array();
    $dataFilter=array();
	foreach ($filter_array as $key => $v) {
        if ($v <> "" && $v <> TD::$option_all) {
            if (substr($key, 0, 2) == 'f_' ) {
                $col = substr($key, 2);
                $dataFilter[$col] = $v;
            } elseif (substr($key, 0, 3) == 'ff_' ) {
                $col = substr($key, 3);
                $TDfilter[$col] = $v;
            }
            
                /* 
                if (is_array($v)) { //массив передаем в параметры
                  $params[$key]=$v;
                  }
                */
        }
    }
	
    $TD = new TD($table);
	$TD->may_edit = (bool) $primary_key;
    if (!user_may_edit_many()) {
        $TD->show_checkbox=false;
    }

    if (substr($table,0,4)=='log_') {
        $TD->is_log_table=true;
    }
    $TD->primary_key=$primary_key;
    $TD->columns=$DB::table_columns($table);
    $TD->filter=$TDfilter;
    $TD->dataFilter=$dataFilter;
    $TD->params=$params;
    
    $data = $DB::query($table,$dataFilter,$order,$order_desc);

    $TD->data=&$data;
 	//$TD->init($res,'sql_array');
	return $TD;
}

