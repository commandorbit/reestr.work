<?php

require $_SERVER['DOCUMENT_ROOT'].'/init.php';
require $_SERVER['DOCUMENT_ROOT'].'/modules/save.php';

$views = array('v_dogovor_sostav');

$table = sql_escape($_GET['t']);
if(in_array($table, $views)) $table = substr($table, 2);
$old_id = $_POST['id'];
$_POST['id'] = '';

if($table == 'plan_dohod') {
	if($_POST['plan_dohod_ver_id'] != plan_dohod_last_ver((int) $_POST['y'])) {
		echo json_encode(array('status'=>'NOTOK', 'message'=>'Невозможно изменить старую версию'));
		exit();
	}
}

if ($table == 'plat') {
	if (array_key_exists('bank_id',$_POST)) unset($_POST['bank_id']);
	if (array_key_exists('guid',$_POST)) unset($_POST['guid']);
}

$id = save($table,$_POST);

$key=sql_table_key($table);
$row = $DB::query_one($table,array($key=>$id));
//$row=sql_row_fix_numeric($res);

// преобразование типов!!!!!!!!!!!!!!
echo json_encode(array('status'=>'COPIED','row'=>$row));
exit();

?>