'use strict';
//included by edit.php	
// uses querySelectorAll IE8+

function EditBox(box) {
    var me = this;
    me.box = box; // div class="modal-editbox"
    
    // has initalizer
    if (EditBox.init) {
        // if initializer return false - stop other init;
        if (!EditBox.init(me)) return;
    }
    
    box.querySelector('button[name="cancel"]').onclick = close;

    var bSave = me.box.querySelector('button[name="save"]');
    if (bSave) bSave.onclick = save;
    
    var bRestore = me.box.querySelector('button[name="restore"]');
    if (bRestore) bRestore.onclick = restore;    
    
    var bMerge = me.box.querySelector('button[name="merge"]');
    if (bMerge) bMerge.onclick = merge;
    var bDel = me.box.querySelector('button[name="delete"]');
    if (bDel) bDel.onclick = del;
    me.bToDoc = me.box.querySelector('button[name="to_document"]');
    if (me.bToDoc) me.bToDoc.onclick = toDocument;
    me.box.querySelector('div.title').onmousedown = function(event) {
        WinMove.handler(this, event);
    };

    var tdChecked = me.box.querySelectorAll('td.checkbox');
    
    for (var i=0; i<tdChecked.length; i++) {
        tdChecked[i].onclick = checkBoxClick;
    }
    me.docOnkeyup = null; // saved handler
    //!!!!!!!!
    
    me.show = show;
    me.edit = edit;
    me.editChecked = editChecked;
    me.view = view;
    me.recmrg = recmrg;
    me.close = close;
    me.add = add;
    me.row = null; // data to edit
    me.editMany=false;
    
    function checkBoxClick() {
        var tr = this.parentNode;
        var span = this.querySelector('span.checkbox');
        var visible =  !MyFn.hasClass(span,'checked');
        MyFn.toggleClass(span,'checked',visible);
        var inp = tr.querySelector('input , select, textarea');
        if ( visible) {
            inp.style.display="inline-block";
        } else {
            inp.style.display="none";
        }
    }
    
    function destroy() {
        box = row = form_param = sql_table = docOnkeyup = null;
    }

    function onKeyESC(e) {

        if (e.keyCode === 27) {
            overlayHide();
            me.close();
        }
    }

    function show() {
        me.docOnkeyup = document.onkeyup;
        document.onkeyup = onKeyESC;
        me.box.style.display = 'block'; //while display none -> no Width height
        me.box.style.position = 'fixed';
        me.box.style.margin = 0;
        var left = (document.documentElement.clientWidth / 2) - (me.box.clientWidth / 2);
        var top = (document.documentElement.clientHeight / 2) - (me.box.clientHeight / 2);
        me.box.style.top = (top > 0 ? top : 0) + 'px';
        me.box.style.left = (left > 0 ? left : 0) + 'px';
        overlay(close);

        console.log('me box');
        //new resizable(me.box, {directions: ['southeast']});
    }
    
    function hideCheckBoxes() {
        var cols=me.box.getElementsByTagName("COL");
        if (cols.length==3) { // скрываем колонку чекбоков
            cols[1].style.width=0;
        }
        var checkboxes=me.box.querySelectorAll("td.checkbox"); 
        for (var i=0; i < checkboxes.length; i++) {
            checkboxes[i].style.visibility="collapse";
            checkboxes[i].innerHTML="";
        }
        var inp = me.box.querySelectorAll('input , select, textarea');
        for (var i = 0; i < inp.length; i++)  {
            inp[i].style.display="inline-block";
            if (inp[i].value) inp[i].value = '';
        }
        me.editMany = false;
    }
    
    function showCheckBoxes() {
        var cols=me.box.getElementsByTagName("COL");
        if (cols.length==3) { // скрываем колонку чекбоков
            cols[1].style.width="30px";
        }
        var checkboxes=me.box.querySelectorAll("td.checkbox"); 
        for (var i=0; i < checkboxes.length; i++) {
            checkboxes[i].style.visibility="visible";
            checkboxes[i].innerHTML='<span class="checkbox"></span>';
        }
        me.editMany = true;
    }

    function close() {
        document.onkeyup = me.docOnkeyup;
        me.box.style.display = 'none';
        hideCheckBoxes();
        if (me.onclose) me.onclose();
        overlayHide();
    }

    function del() {
        var form = me.box.querySelector('form.editbox-form');
        if (me.ondelete)
            me.ondelete(form);
        close();
    }


    function save() {
        var form = me.box.querySelector('form.editbox-form');
        if (me.editMany==false && has_nulls(form))
            return false;
        if (me.onsave)
            me.onsave(form);
        close();
    }
    
    function restore() {
        var form = me.box.querySelector('form.editbox-form');
        if (me.onrestore)
            me.onrestore(form);
        close(); 
    }
    
    function copy() {
    	var form = me.box.querySelector('form.editbox-form');

        if (me.oncopy) {
            me.oncopy(form);
        }

        close(); 
    }

    function merge() {
        var form = me.box.querySelector('form.mergebox-form');
        if (has_nulls(form))
            return false;
        if (me.onmerge)
            me.onmerge(form);
        close();
    }
    
	function toDocument() {
		var form = me.box.querySelector('form.editbox-form');
		var id= me.box.querySelector('input[name="id"]').value;
		var dest=this.getAttribute('data-php')+ '.php?id='+id;
		var win=window.open(dest, '_blank');
            win.focus();
		close();
	}

    /*
	function toLog() {
		var form = me.box.querySelector('form.editbox-form');
		var id= me.box.querySelector('input[name="id"]').vaZlue;
		var dest=this.getAttribute('data-php')+ '.php?id='+id;
		var win=window.open(dest, '_blank');
            win.focus();
		close();
	}
    */
    
    function editChecked() {
        me.box.querySelector('div.title').innerHTML = "Редактирование отмеченных записей" ;
        var inp = me.box.querySelectorAll('input , select, textarea');
        for (var i = 0; i < inp.length; i++)  {
            inp[i].style.display="none";
            inp[i].value = '';
        }
        showCheckBoxes();
        if (me.bToDoc) me.bToDoc.style.display='none';
        me.show();
        if (me.onload) me.onload();
    }
    // row
    // object of {fld1:value1,fld2:value2}
    function edit(row) {
        setContols(row);
        me.box.querySelector('div.title').innerHTML = "Редактирование записи: " + (row.id ? row.id : '');
        //me.box.querySelector('button[name="delete"]').style.display = 'block';
        if (me.bToDoc) me.bToDoc.style.display='block';
        me.show();
        if (me.onload) me.onload();
    }
    
    
    function setContols(row) {
        me.row = row;

        var select_codes = me.box.querySelectorAll('.handbook_code');

        var inp;
        for (var fld in row) {
            inp = me.box.querySelector('input[name="' + fld + '"]');

            if (!inp)
                inp = me.box.querySelector('select[name="' + fld + '"]');
            if (!inp)
                inp = me.box.querySelector('textarea[name="' + fld + '"]');
            if (inp)
                inp.value = row[fld];
        }

        //onchange trigger
        for(var i = 0; i < select_codes.length; i++) {
            var event = document.createEvent('HTMLEvents');
            event.initEvent('change', true, false);
            select_codes[i].dispatchEvent(event);
        }
    }
    
    function view(row) {
        setContols(row);
        me.box.querySelector('div.title').innerHTML = "Просмотр записи: " + (row.id ? row.id : '');
        //me.box.querySelector('button[name="delete"]').style.display = 'none';
        me.box.querySelector('button[name="save"]').style.display = 'none';
        if (me.bToDoc) me.bToDoc.style.display='block';
        me.show();
        if (me.onload) me.onload();       
    }

    function add() {
        me.row = null;
        if (me.onload)
            me.onload();
        var inp = me.box.querySelectorAll('input , select');
        for (var i = 0; i < inp.length; i++) {
            if(inp[i].getAttribute('data-default-value')) {
                inp[i].value = inp[i].getAttribute('data-default-value');
            } else {
                inp[i].value = '';
            }
        }

        box.querySelector('div.title').innerHTML = "Добавление новой записи";
        //box.querySelector('button[name="delete"]').style.display = 'none';
        if (me.bToDoc) me.bToDoc.style.display='none';
        me.show();
    }
    function recmrg() {
        me.row = null;
        if (me.onload)
            me.onload();
        var inp = box.querySelectorAll('input , select');
        for (var i = 0; i < inp.length; i++)
        {
            if (inp[i].getAttribute('type') !== 'button')
                inp[i].value = '';
        }
        box.querySelector('div.title').innerHTML = "Объединение записей";
        me.show();
    }
}