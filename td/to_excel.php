<?php
require $_SERVER['DOCUMENT_ROOT'] .'/init.php';
date_default_timezone_set('UTC');
require  $_SERVER['DOCUMENT_ROOT'] .'/PHPExcel/Classes/PHPExcel.php';

$table = $_GET['t'];
$T = newTD($table,$_GET,true);

$T->may_edit = false;

$objPHPExcel = new PHPExcel();

if (!$tablename=sql_get_value("short_name",'tab',"`table`='$table'")) {
	$tablename=$table;
}
	$objPHPExcel->getActiveSheet()->setTitle(mb_substr($tablename, 0, 5));

$j=0;

foreach ($T->columns as $col){
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($j,1,$col['label']);
	$j++;
}

$i=2;


foreach($T->data as $row){
	$j=0;
	foreach ($T->columns as $col){
		$colname=$col['name'];
		//$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($j,$i,$row[$colname]);
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($j, $i, in_array($col['type'], array('double','decimal')) ? $row[$colname] : $T->col_formated($col,$row[$colname]));
		$j++;
	}
	$i++;
}
$filename=sys_get_temp_dir() .'/'. $table.time().'.xlsx';
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

$objWriter->save($filename);

header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="'.$tablename.'_'.date('dm').'.xlsx"');
header('Cache-Control: max-age=0');
@readfile($filename);
?>