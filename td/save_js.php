<?php
require $_SERVER['DOCUMENT_ROOT'].'/init.php';
require $_SERVER['DOCUMENT_ROOT'].'/modules/save.php';

if (!isset($_GET['t'])) exit();

$table = sql_escape($_GET['t']);

//Обновляем сумму платежей в заявке, если создана платежка для заявки

$key=sql_table_key($table);

if($table == 'v_zayav_sostav') {
	$key = 'id';

	$ost = ds_zs_check_sum($_POST['id'], $_POST['sum']);

	$from_2014 = sql_get_value('id', 'zayav2014_2015', "id_2015 in (select zayav_id from zayav_sostav where id = '${_POST['id']}')");

	if(!$from_2014 && $ost < 0) {
		echo json_encode(array('status'=>'NOTOK', 'message'=>'Сумма превышает лимит на ' . abs($ost) . ' руб.'));
		exit();
	}
} else if($table == 'v_dogovor_sostav') {
	$key = 'id';
} else if($table == 'v_zayav_sostav_bad_dogovor_sostav_id') {
	$src_table  = $table;
	$table = 'zayav_sostav';
	$key = 'id';
} else if($table == 'plan_dohod') {
	if($_POST['plan_dohod_ver_id'] != plan_dohod_last_ver((int) $_POST['y'])) {
		echo json_encode(array('status'=>'NOTOK', 'message'=>'Невозможно изменить старую версию'));
		exit();
	}
}

try {
	$id=save($table,$_POST);

	if($table == 'plat') {
		$plat_id = sql_escape($id);
		$plat_sum = sql_escape((float)$_POST['s']);

		//modules/reestr_functions.php
		plat_zayav_sum_update($plat_id, $plat_sum);
	}
	
	$row = $DB::query_one($table,array($key=>$id));

	// преобразование типов!!!!!!!!!!!!!!
	echo json_encode(array('status'=>'OK','row'=>$row));
	exit();
} catch(Exception $e) {
	echo json_encode(array('status'=>'NOTOK','message'=>$e->getMessage()));
	exit();
}