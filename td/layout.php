<?php
function layout_table_data($table,$data=false)  {

    if (!isset($_SESSION['table_layout'])) {
        $_SESSION['table_layout']=array();
    }
    $uri=parse_url($_SERVER['REQUEST_URI'],PHP_URL_PATH);
    if (!isset($_SESSION['table_layout'][$uri])) {
        $_SESSION['table_layout'][$uri]=array();
    }
    if (!isset($_SESSION['table_layout'][$uri][$table])) {
        $_SESSION['table_layout'][$uri][$table]=array();
    }
    $link=&$_SESSION['table_layout'][$uri][$table];
    if ($data) $link=$data;
    return $link;
}

function is_layout_request() {
    return isset($_GET['layout_change']) && isset($_GET['t']);
}

function layout_save() {
	$data=file_get_contents("php://input"); 
	layout_table_data($_GET['t'],json_decode($data,true));
    print_r($_SESSION);
    
}
