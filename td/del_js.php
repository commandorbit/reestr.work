<?php
require $_SERVER['DOCUMENT_ROOT'].'/init.php';
require $_SERVER['DOCUMENT_ROOT'].'/modules/save.php';
require $_SERVER['DOCUMENT_ROOT'].'/soft_delete.php';

$table = sql_escape($_GET['t']);
// !!!!!!!!!!!

if($table == 'v_zayav_sostav') $table = 'zayav_sostav';
if($table == 'v_dogovor_sostav') $table = 'dogovor_sostav';

$key_name = sql_table_key($table);
$ids = array();


if(is_array($_POST[$key_name])) {
	for($i = 0; $i < count($_POST[$key_name]); $i++) {
		array_push($ids, sql_escape($_POST[$key_name][$i]));
	}
} else {
	array_push($ids, sql_escape($_POST[$key_name]));
}


sql_query("START TRANSACTION");
if($table == 'plat') {
	foreach ($ids as $id) {
		plat_zayav_sum_update($id, 0);
	}
} else if($table == 'plan_dohod') {
	if($_POST['plan_dohod_ver_id'] != plan_dohod_last_ver((int) $_POST['y'])) {
		echo json_encode(array('status'=>'NOTOK', 'message'=>'Невозможно изменить старую версию'));
		exit();
	}
} else if ($table == 'stat_pfhd_ver') {
	// удаляю без сохранения в логи

	sql_query("delete from stat_pfhd where stat_pfhd_ver_id  in (" . implode(",",$ids) . ")");
	// траназкцию не заврещаем автокоммит будет после завершения
}


$msg='';
$ok = del_row($table, $ids, $msg);
if (!$ok) {
	sql_query("ROLLBACK");
    echo json_encode(array('status'=>'ERROR','msg'=>$msg));
} else {
	sql_query("COMMIT");
    echo json_encode(array('status'=>'OK'));
	
}


?>