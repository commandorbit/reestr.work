<?php
// uses sql
require_once dirname(__FILE__) . '/layout.php';

class ConfigDB {
	protected static $DEF_COLUMN_W=60; //px	
	protected static $REF_SUFFIX='_id';
    protected static $NOT_ACTUAL='not_actual';
	
    const PARENT_FLD = 'parent_id';
    
	// Виртуальные таблицы для них поля определены фиксированно
	public static function is_virtual_table($table) {
		return false;
	}
	
	public static function virtual_table_columns($table) {
		return [];
	}
	
	public static function table_columns($table) {
		
		$cols = static::is_virtual_table($table) ? static::virtual_table_columns($table) : sql_table_columns($table);

		if($table == 'v_report_zayav') {
			unset($cols['plat_month']);
			unset($cols['plat_year']);
		}

		foreach ($cols as $key=>$val){
			if (substr($key,0,1)=='_') unset($cols[$key]);
		}
		
		foreach ($cols as $key=>$val) {
			$label=static::column_property($table,$key,'short_name');
			if (empty($label)) {
				$label=$key;
			}
			$cols[$key]['label'] = $label;
			$w=self::column_width($table,$key);
			if (strlen($label)*8 > $w) $w=strlen($label)*8 ;
			$cols[$key]['width'] = $w ;
			$cols[$key]['format'] =  static::column_property($table,$key,'format');
			$cols[$key]['has_sum'] =  (int) static::column_property($table,$key,'has_sum');
			$cols[$key]['description'] =  static::column_property($table,$key,'description');
			$cols[$key]['editable'] =  (int) static::column_property($table,$key,'editable');
			$cols[$key]['url'] =  static::column_property($table,$key,'url');   
			
			if ($cols[$key]['type']=='date' && !static::is_virtual_table($table)) {
				// для аналитики = stat.php
				$cols[$key]['date_options'] = static::column_date_options($table,$key);
			}
			$cols[$key]['options'] = static::column_options($table,$key);
            $cols[$key]['edit_options']=static::column_options($table,$key,true);
            $cols[$key]['tree_options'] = static::column_tree_options($table,$key);

            $cols[$key]['default_value']=static::column_property($table,$key,'default_value');
            if ($cols[$key]['default_value']=='TODAY_DATE')  $cols[$key]['default_value']=date('Y-m-d');

			$align= static::column_property($table,$key,'align');
			if (empty($align)) {
				if ($cols[$key]['type']=='varchar' || $cols[$key]['options'])
					$align='left';
				else 
					$align='center';
			}
			$cols[$key]['align'] = $align;
		}
		return static::order_cols($table,$cols);
	}

    protected static function column_date_options($table,$fld,$actual=false) {
        $options=static::arrayFromSql("select distinct $fld,$fld from $table where $fld is not null");
        //if (sql_get_value('count(*)',$table,"$fld is null")>0) {
        	//$options 
        //}
        return $options;
    }

    private static function ref_table_from_fld_name($fld_name) {
    	$t = '';
        $suffix = substr($fld_name,-strlen(static::$REF_SUFFIX));
    	if ($suffix == static::$REF_SUFFIX) {
    		$t = substr($fld_name,0,strlen($fld_name)-strlen(static::$REF_SUFFIX));
    	}
    	return $t;
    }

    protected static function column_tree_options($table,$fld) {
        $options=false;
        $table = static::column_property($table,$fld,'view_name');
        if (!$table)  {
        	$table = static::ref_table_from_fld_name($fld);
        }
        if ($table &&  sql_table_exists($table)) {
        	if (sql_field_exists($table, 'parent_id')) {
        		//var_dump('isset parent fld' . $fld);
        		$fields = array('id', 'name', 'parent_id');
        		$options = sql_rows("select " . implode(',',$fields) . " from $table");
        	}
        }
        return $options;
    }
	
    protected static function column_options($table,$fld,$actual=false) {
        $options=false;
        if ($view_name=static::column_property($table,$fld,'view_name'))
            $options=static::arrayFromTable($view_name,$actual);
        elseif (substr($fld,-strlen(static::$REF_SUFFIX))==static::$REF_SUFFIX) {
            $opt_table = substr($fld,0,strlen($fld)-strlen(static::$REF_SUFFIX));
            if (sql_table_exists($opt_table)) {
                $options=static::arrayFromTable($opt_table,$actual);
            }
        }
        return $options;
    }
   	// возвращает упорядоченный массив полей из сессии пользователя
   // меняет ширину полей
	protected static function order_cols($table,$cols) {
        foreach ($cols as $key=>$val) {
            $cols[$key]['hidden']=false;
        }
		if ($ord=layout_table_data($table)) {
			$newcols=array();
			foreach ($ord as $key=>$val) {
				// column in table was dropped ))
				if (isset($cols[$key])) {
					$newcols[$key]=$cols[$key];
					$newcols[$key]['width']=$val['width'];
	                //isset вероятно не нужен.
	                $hidden = (isset($val['hidden']) && $val['hidden']);
	                $newcols[$key]['hidden']=$hidden;
                }
			}

			// Добавляем колонки которых не было в layout
            foreach ($cols as $key=>$val) {
            	if (!isset($newcols[$key])) {
            		$newcols[$key] = $val;
            	}
            }
		} else {
			$newcols=&$cols;
		}
		return $newcols;
	}
		
	private static function column_width($table,$column) {
		$w=self::column_property($table,$column,'width');
		return ($w ? $w : static::$DEF_COLUMN_W);
	}
	
	
	
	// завернуть всю работу с таблиццам через эту функцию ради срезания прав на read пользователя
	public static function query($table,$filter=array(),$order_by='',$order_desc=false) {
		if (static::is_virtual_table($table)) {
			$method = 'query_' . $table;
			$rows = static::$method($filter,$order_by,$order_desc);
		}	else {
			$sql = static::sql($table,$filter,$order_by,$order_desc);
			$rows = sql_rows_fix_numeric(sql_query($sql));
		}
		//echo $s;
		return $rows;
	}
	
	// возвращает один row
	public static function query_one($table,$filter=array()) {
		$rows = static::query($table,$filter);
		return $rows[0];
	}
	
	protected static function arrayFromTable($table,$actual=false) {
		list($key,$name)=static::table_key_name($table);
		$flds = [$key,$name];
		//$table = (substr($table,0,2) == 'v_') ? substr($table, 2) : $table;
		if (sql_field_exists($table,static::PARENT_FLD)) {
			$flds[] = static::PARENT_FLD;
		}
		$flds_sql = implode(',',array_map(function($v) {return "`$v`";},$flds));
        $sql="select $flds_sql from `$table` ";
        if ($actual && sql_field_exists($table,static::$NOT_ACTUAL)) {
            $sql .= ' where ' . static::$NOT_ACTUAL . '=0';
        }
        
        $sql.=" order by `$name` ";
		return static::arrayFromSql($sql);
	}



	protected static function arrayFromSql($sql) {
		$r=sql_query($sql);
		$a=array();
		while ($row=sql_numeric_array($r)) {
            // for js processing as numeric field
            $key=(is_numeric($row[0])) ? $row[0]+0 :$row[0];
			$a[$key]=$row[1];
		}
		return $a;	
	}

	// получает набор свойств колонок из системной таблицы
	public static function column_property($table,$column,$propery) {
		$v=sql_get_value($propery,'col',"`table`='$table' and col_name='$column'");
		if (!$v) 
			$v=sql_get_value($propery,'col',"`table` is null and col_name='$column'");
		return $v;
	}

    // возвращает ключ и поле для отображения
    // предполагается что primary key - первый
    // отображаем всегда второе
	protected static function table_key_name($table) {
		$cols=sql_table_columns($table);
        $i=0;
        foreach ($cols as $col){
            if ($i==0)  $key=$col['name'];
            elseif ($i==1) $name=$col['name'];
            else break;
            $i++;
        }
		return array($key,$name);
	}

	// используется для ограничения по правам пользователя 
	// за счет  наследуемой sql_where
	public static function sql($table,$filter=array(),$order_by='',$order_desc=false) { 
		$s = "SELECT * FROM `$table` ";
        $w=static::sql_where($table,$filter);
        if ($w)	$s .= ' WHERE ' . $w ;

		$ord='';	
		if ($order_by)  {
			$ord=sql_escape($order_by);		
			$s .= static::order_by($table, $ord);
			if ($order_desc) $s .= ' desc ';
		}
		//echo $s;
		return $s;
	}
	
	protected static function sql_where($table,$filter) {
		$w=array();
		$columns = sql_table_columns($table);
		foreach ($columns as $col) {
			$col_name=$col['name'];
			if (isset($filter[$col_name])) {
				$v=$filter[$col_name];

                if (is_array($v)) {// in options
                	$w[]=static::where_parse_array($col,$v);
				} else {
					$sign=static::parse_sign($v);

					if($sign == 'range' || $sign == 'date_range') {
						$range = explode('/', $v);

						$w[]=static::col_where($col,'>=', $range[0]);
						$w[]=static::col_where($col,'<=', $range[1]);
					} else {
						$w[]=static::col_where($col,$sign,$v);
					}
				}
			}
		}
		
		return implode($w,' and ');
	}

	private static function parse_sign(&$v) {
		$sign='=';
		$a=array('=','>','<','>=','<=', '!=', '!~', 'range', 'date_range', '_LIKE_');

		if ($pos=array_search(substr($v,0,1),$a)) {
			$sign=substr($v,0,1);
			$v=substr($v,1);
		} elseif($pos=array_search(substr($v,0,2),$a)) {
			$sign=substr($v,0,2);
			$v=substr($v,2);
		} elseif($pos=array_search(substr($v,0,5),$a)) {
			$sign=substr($v,0,5);
			$v=substr($v,5);
		} elseif($pos=array_search(substr($v,0,6),$a)) {
			$sign=substr($v,0,6);
			$v=substr($v,6);
		} elseif($pos=array_search(substr($v,0,10),$a)) {
			$sign=substr($v,0,10);
			$v=substr($v,10);
		}

		return $sign;
	}
	
	protected static function col_like($col_name, $value) {
		$w_like = '';

		$w_like .= " OR `$col_name` LIKE '" . $value . "'";

		$where = " (" . substr($w_like,3) .")";

		return $where;
	}

	protected static function col_where($col,$sign,$val) {
		$col_name=$col['name'];
		if ($val=='_null_') 
			$w= $col_name. " is null " ;
		elseif (substr($col['type'],0,strlen('varchar'))=='varchar')
			$w= $col_name ." like '%" . sql_escape($val) ."%'";
/*		Дата передается теперь в sql format проверить что всюду!
		elseif ($col['type']=='date')
			$w=$col_name . $sign . sql_rus2date($val);*/
		else
			$w=$col_name . $sign . "'" . sql_escape($val) ."'";
		return $w;
	}
	
	//  filer = array($field=>$value)
	protected static function where_parse_array($field, $vals) {
		$NULL_VAL='null';
		$has_null = false;
		$has_like = [];
		$res=array();
		$where = '';
		$col_name = $field['name'];

		foreach ($vals as $key=>$val) {	
			$sign = self::parse_sign($val);

			if ($val==$NULL_VAL) {
				$has_null = true;
			} else if($field['type'] == 'date') {
				array_push($has_like, $val);
			} 
			else {
				$res[$key] = "'" . sql_escape($val) ."'";
			}
		}

		$s = implode(",",$res);	

		$w_like = '';

		if(!empty($has_like)) {
			foreach ($has_like as $value) {
				$w_like .= " OR `$col_name` LIKE '" . $value . "'";
			}

			$where .= " (" . substr($w_like,3) .")";
		}

		if(!empty($s)) {
			if (!$has_null) {
				$where .=" `$col_name` in ($s) ";
			} else {
				$where .=" (`$col_name` in ($s) or `$col_name` is null) ";
			}
		}

		//print_r($where);

		return $where;
	}

	protected static function order_by($source_table,$fld) {
		if (substr($fld,-strlen(static::$REF_SUFFIX))==static::$REF_SUFFIX) {
            $table=substr($fld,0,strlen($fld)-strlen(static::$REF_SUFFIX));
			list($key,$name)=static::table_key_name($table);
			$s= " order by (select `$name` from  `$table` where `$source_table`.`$fld`=`$table`.`$key` )" ;
		} else {
			$s= " order by  `$fld` ";
		}
		return $s;
	}
}

?>