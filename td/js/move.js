'use strict';
function colMoveResize(tbl,callback) {
    var resize= new ColResize(tbl).done(callback);
    var mv=new ColMove(tbl).done(function(tbl,oldIndex,newIndex) {resize.initPos(),callback(tbl,oldIndex,newIndex)});
}
// move smthing
function ColMove(tbl) {
    var me=this;
    me.tbl=tbl;
    me.drag = {
        src: null, //dragged th
        index: null //it's index
    };
	me.ptr=null;
    me.savedCss=null; //saved css  of dropable container
  
    me.done=function(callback) {me.ondone=callback;}
    
    function index(el) {
        var i = (el.cellIndex !== undefined) ? el.cellIndex : Array.prototype.indexOf.call(el.parentNode.children, el);
        return i;
    }
    
        
    me.dragstart = function dragstartF(ev) {
        var el = this;
        ev = ev || window.event;
        // need for FF
        ev.dataTransfer.setData("Text", el.textContent || el.innerText);
        ev.dataTransfer.effectAllowed = "move";
        ev.dataTransfer.dropEffect = "move"; //maybe i need to set it in dragover?
        me.drag.src = el;
        me.drag.index = index(el);
    }
    // вставлять левее элемента?
    function willDropLeft(el) {
        return index(el) < me.drag.index;
    }
    // можно drop если тот же родитель и не сам элемент
    function may(el) {
        return (me.drag.src.parentNode === el.parentNode && me.drag.src !== el);
    }
	function pointer() {
		var p=document.getElementById('move-pointer');
		if (!p) {
			p=document.createElement('div');
			p.id='move-pointer';
			document.body.appendChild(p);
		}
		return p;
	}
    // default обработчик для подкраски Droppable
    me.setStyle = function setStyleF(el) {
		var insLeft = willDropLeft(el);
        me.savedCss = el.style.cssText;
		var ptr=pointer();
		ptr.style.display='block';
		ptr.style.position='absolute';
		var of=MyFn.offset(el);
		ptr.style.left = (insLeft ? 0 : el.offsetWidth) + of.left-5 + 'px';
		ptr.style.top = of.top-20 + 'px';
    }

    me.restoreStyle = function restoreStyleF(el) {
		pointer().style.display='none';        
    }

    function dragInit() {
        me.drag.index = null;
        me.drag.src = null;
        me.savedCss = null;
    }
    
    // events
    // needs prevent default to enable drop 
    me.dragenter = function dragenterF(ev) {
        var el=this;
        ev = ev || window.event;
        if (may(el)) {           
            me.setStyle(el);
            ev.preventDefault && ev.preventDefault();
        }
    }

    me.dragleave= function dragleaveF(ev) {
        var el=this;
        ev = ev || window.event;
        me.restoreStyle(el);
    }

    // needs prevent default to enable drop 
    me.dragover = function dragoverF(ev) {
        var el=this;       
        ev = ev || window.event;        
        if (may(el)) {
			me.setStyle(el);
            ev.preventDefault();
        }
    }
    function copyEvents(from, to) {
        var props=Object.getOwnPropertyNames(from);
        for (var i=0; i<props.length; i++) {
            var prop=props[i];
            if ( prop.indexOf("on") == 0 && from[prop]  )  {
//                console.log(from.tagName,prop);
                to[prop]=from[prop];
                from[prop]=null;
            }
        }
        for (var i=0; i < from.children.length; i++) {
            copyEvents(from.children[i],to.children[i]);
        }
    }
    
    function xchgTR(tr,oldIndex,newIndex) {
        var old=tr.children[oldIndex];
        var newNode=old.cloneNode();
        if (newIndex<tr.children.length) {
            tr.insertBefore(newNode,tr.children[newIndex]);
        } else {
            tr.appendChild(newNode);
        }
        copyEvents(old,newNode);
        // copy assigned events 
        // events attached through attachEvent wouldn't be copied (
        tr.removeChild(old);
    }


    me.drop = function dropF(ev) {
        var el=this;       
        ev = ev || window.event;        
        parent = el.parentNode;
        me.restoreStyle(el);
        var oldIndex=me.drag.index;
        var newIndex=index(el);
        if (!willDropLeft(el)) newIndex++;
        
        if (parent.tagName=='TR') {
            // tbody table
            var table=parent.parentNode.parentNode;
            //tbody thead colgroup
            for (var i=0; i<table.children.length; i++) {
                var tag=table.children[i];
                if (tag.tagName=='TBODY' || tag.tagName=='THEAD') {
                    for (var j=0; j<tag.children.length; j++) {
                        var tr=tag.children[j];
                        xchgTR(tr,oldIndex,newIndex);
                    }
                } else if (tag.tagName=='COLGROUP') {
                    xchgTR(tag,oldIndex,newIndex);
                }
            }
            if (me.ondone) {
                me.ondone(me.tbl, newIndex, oldIndex);
            }
            dragInit();
        }
            
        //setElHandler(newEl);
		ev.preventDefault();
    }
    
    function setElHandler(el) {
        el.ondragstart = me.dragstart;
        el.ondragenter = me.dragenter;
        el.ondragleave = me.dragleave;
        el.ondragover =  me.dragover;
        el.ondrop = me.drop;
    }

    function setHandlers(nodes) {
        dragInit();
        for (var i = 0; i < nodes.length; i++) {
            setElHandler(nodes[i]);
        }
    }
    setHandlers(me.tbl.querySelectorAll('TH'));
}

function ColResize(tbl) {
	var me=this;
    me.tbl=tbl;
    me.row=MyFn.nodeParent(tbl.querySelector('TH'),'TR');
	var inResize=false;
	var savedMove;
	var savedUp;
	var col;
    var col2;
    //var me.tbl2,col2;
	var ind; //index of cell to resize
	// tblWidth as start columnWidth
	var tblWidth,colWidth,startX; //MouseX
	var pos=[];
	me.done=function(callback) {
        me.ondone=callback;
    };
    me.initPos=initPos;
    setHandler(me.row);
    
	function setHandler (row) {
		initPos();
		// во wrap лежат две таблицы (заголовки и данные)
		var div=MyFn.nodeParent(me.tbl,'DIV','td-table-wrap');
		if (div) {
			var tbls=div.getElementsByTagName("TABLE");
			me.tbl2=(tbls.length>1) ? tbls[1] : null;
		}
		row.onmousemove=mouseMove;		
		row.onmousedown=mouseDown;	
	}
	
	function mouseUp(event) {
		if (inResize) {
			inResize=false;
			document.body.style.cursor='';			
			if (col2) col2.style.width=col.style.width;
			col2=col=null;
			initPos();
			eventCancel(event);
			document.onmousemove=savedMove;
			document.onmouseup=savedUp;
			// callAfter
			if (me.ondone) me.ondone(me.tbl);
		}
	};
    
	function mouseDown(event) {
		event=event || window.event;
		ind=cellIndex(event);
		if (ind!==null) {			
			inResize=true;		
			savedMove = document.onmousemove;
			document.onmousemove = mouseMove;
			savedUp = document.onmouseup;
			document.onmouseup = mouseUp;
			document.body.style.cursor = 'col-resize';
			//row.style.cursor= 'col-resize';
			setCol(ind);
			colWidth=parseInt(col.style.width);
			tblWidth=parseInt(me.tbl.style.width);
			startX = event.pageX;			
			eventCancel(event);
		}
	}
	
	function mouseMove(event) {
		event=event || window.event;
		if (!inResize) {
			var i=cellIndex(event);
			me.row.style.cursor = (i!==null) ? 'col-resize' : '';
		} else {
			var addW=event.pageX-startX;
			var w=colWidth+addW
			col.style.width=colWidth+addW+'px';
			if (tblWidth) me.tbl.style.width=tblWidth+addW+'px';
			if (me.tbl2) me.tbl2.style.width=tblWidth+addW+'px';
			if (col2) col2.style.width=w+'px';
			eventCancel(event);		
		}
	}
	
	function eventCancel(evt) {	
		evt = evt || window.event;	
		if (evt.stopPropagation)    evt.stopPropagation();
		if (evt.cancelBubble!=null) evt.cancelBubble = true;
		if (evt.preventDefault) evt.preventDefault(); 
        else (evt.returnValue = false);  
	}

	function setCol(i) {
		var cols=me.tbl ? me.tbl.getElementsByTagName("COL") :null;
		col = cols.length>i ? cols[i] : null;
		if (me.tbl2) {
			col2=me.tbl2.getElementsByTagName("COL")[i];
		}
	}

	function initPos() {
		pos=[];
		var length=me.row.cells.length
		var left=MyFn.offset(me.row).left;
		// for fixed position
		if (left<0) left=0;
		for (var i=0;i<length;i++) {
			var c=me.row.cells[i];
			var p=left+c.offsetLeft+c.offsetWidth;
			pos.push(p);
		}
	}

	// индекс ячейки для resize
	function cellIndex(event) {
		event=event || window.event;
		var x = event.pageX;
		var ind=null;
		for (var i=0;i<pos.length;i++) {
			if (Math.abs(x-pos[i])<3) {
				ind=i;
			}
		}
		return ind;
	}

};