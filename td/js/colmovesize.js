'use strict';
function colMoveResize(tbl,callback) {
    var resize= new ColResize(tbl).done(callback);
    var mv=new ColMove(tbl).done(callback);
}
// move smthing
function ColMove(tbl) {
    var me=this;
    me.tbl=tbl;
    me.drag = {
        src: null, //dragged th
        index: null //it's index
    };
	me.ptr=null;
    me.savedCss=null; //saved css  of dropable container
  
    me.done=function(callback) {me.ondone=callback;return this;}
    
    function index(el) {
        var i = (el.cellIndex !== undefined) ? el.cellIndex : Array.prototype.indexOf.call(el.parentNode.children, el);
        return i;
    }
    
        
    me.dragstart = function dragstartF(ev) {
        var el = this;
        ev = ev || window.event;
        // need for FF
        ev.dataTransfer.setData("Text", el.textContent || el.innerText);
        ev.dataTransfer.effectAllowed = "move";
        ev.dataTransfer.dropEffect = "move"; //maybe i need to set it in dragover?
        me.drag.src = el;
        me.drag.index = index(el);
    }
    // вставлять левее элемента?
    function willDropLeft(el) {
        return index(el) < me.drag.index;
    }
    // можно drop если тот же родитель и не сам элемент
    function may(el) {
        return (me.drag.src.parentNode === el.parentNode && me.drag.src !== el && !MyFn.hasClass(el,'check-all'));
    }
	function pointer() {
		var p=document.getElementById('move-pointer');
		if (!p) {
			p=document.createElement('div');
			p.id='move-pointer';
			document.body.appendChild(p);
		}
		return p;
	}
    // default обработчик для подкраски Droppable
    me.setStyle = function setStyleF(el) {
		var insLeft = willDropLeft(el);
        me.savedCss = el.style.cssText;
		var ptr=pointer();
		ptr.style.display='block';
		ptr.style.position='absolute';
		var of=MyFn.offset(el);
		ptr.style.left = (insLeft ? 0 : el.offsetWidth) + of.left-5 + 'px';
		ptr.style.top = of.top-20 + 'px';
    }

    me.restoreStyle = function restoreStyleF(el) {
		pointer().style.display='none';        
    }

    function dragInit() {
        me.drag.index = null;
        me.drag.src = null;
        me.savedCss = null;
    }
    
    // events
    // needs prevent default to enable drop 
    me.dragenter = function dragenterF(ev) {
        var el=this;
        ev = ev || window.event;
        if (may(el)) {           
            me.setStyle(el);
            ev.preventDefault && ev.preventDefault();
        }
    }

    me.dragleave= function dragleaveF(ev) {
        var el=this;
        ev = ev || window.event;
        me.restoreStyle(el);
    }

    // needs prevent default to enable drop 
    me.dragover = function dragoverF(ev) {
        var el=this;       
        ev = ev || window.event;        
        if (may(el)) {
			me.setStyle(el);
            ev.preventDefault();
        }
    }
    function copyEvents(from, to) {
        if (from.onClone) from.onClone(to);
        
        var props=Object.getOwnPropertyNames(from);        
        for (var i=0; i<props.length; i++) {
            var prop=props[i];
            if ( prop.indexOf("on") == 0 && from[prop]  )  {
                to[prop]=from[prop];
                from[prop]=null;
            }
        }
        
        for (var i=0; i < from.children.length; i++) {
            copyEvents(from.children[i],to.children[i]);
        }
        
    }
    
    function xchgTR(tr,oldIndex,newIndex) {
        var old=tr.children[oldIndex];
        var newNode=old ; //old.cloneNode(true);
        if (newIndex<tr.children.length) {
            tr.insertBefore(newNode,tr.children[newIndex]);
        } else {
            tr.appendChild(newNode);
        }
        //copyEvents(old,newNode);
        // copy assigned events 
        // events attached through attachEvent wouldn't be copied (
       // tr.removeChild(old);
    }


    me.drop = function dropF(ev) {
        var el=this;       
        ev = ev || window.event;        
        parent = el.parentNode;
        me.restoreStyle(el);
        var oldIndex=me.drag.index;
        var newIndex=index(el);
        if (!willDropLeft(el)) newIndex++;
        
        if (parent.tagName=='TR') {
            // tbody table
            var table=parent.parentNode.parentNode;
            //tbody thead colgroup
            for (var i=0; i<table.children.length; i++) {
                var tag=table.children[i];
                if (tag.tagName=='TBODY' || tag.tagName=='THEAD') {
                    for (var j=0; j<tag.children.length; j++) {
                        var tr=tag.children[j];
                        xchgTR(tr,oldIndex,newIndex);
                    }
                } else if (tag.tagName=='COLGROUP') {
                    xchgTR(tag,oldIndex,newIndex);
                }
            }
            if (me.ondone) {
                me.ondone(me.tbl, newIndex, oldIndex);
            }
            dragInit();
        }
            
        //setElHandler(newEl);
		ev.preventDefault();
    }
    
    function setElHandler(el) {
        el.ondragstart = me.dragstart;
        el.ondragenter = me.dragenter;
        el.ondragleave = me.dragleave;
        el.ondragover =  me.dragover;
        el.ondrop = me.drop;
    }

    function setHandlers(nodes) {
        dragInit();
        for (var i = 0; i < nodes.length; i++) {
            setElHandler(nodes[i]);
        }
    }
    setHandlers(me.tbl.querySelectorAll('TH'));
}

function ColResize(tbl) {
	var me=this;
    me.tbl=tbl;
    var th=tbl.querySelector('TH');
    if (!th) throw ('ColResize no TH element!');
    me.row=MyFn.nodeParent(th,'TR');
	var inResize=false;
	var savedMove;
	var savedUp;
	var col;
   // var col2;
    //var me.tbl2,col2;
	var ind; //index of cell to resize
	// tblWidth as start columnWidth
	var tblWidth,colWidth,startX; //MouseX
	me.done=function(callback) {
        me.ondone=callback;
        return this;
    };
    setHandler(me.row);
    
	function setHandler (row) {
		row.onmousemove=mouseMove;		
		row.onmousedown=mouseDown;	
	}
	
	function mouseUp(event) {
		if (inResize) {
			inResize=false;
            col=null;
			//if (col2) {col2.style.width=col.style.width; col2=null;}
			document.body.style.cursor='';			
			MyFn.eventCancel(event);
			document.onmousemove=savedMove;
			document.onmouseup=savedUp;
			// callAfter
			if (me.ondone) me.ondone(me.tbl);
		}
	};
    
	function mouseDown(event) {
		event=event || window.event;
		ind=cellIndex(event);
		if (ind!==null) {			
			inResize=true;		
			savedMove = document.onmousemove;
			document.onmousemove = mouseMove;
			savedUp = document.onmouseup;
			document.onmouseup = mouseUp;
			document.body.style.cursor = 'col-resize';
			//row.style.cursor= 'col-resize';
			setCol(ind);
			colWidth=parseInt(col.style.width);
			tblWidth=parseInt(me.tbl.style.width);
			startX = event.pageX;			
			MyFn.eventCancel(event);
		}
	}
	
	function mouseMove(event) {
		event=event || window.event;
		if (!inResize) {
			var i=cellIndex(event);
			me.row.style.cursor = (i!==null) ? 'col-resize' : '';
		} else {
			var addW=event.pageX-startX;
			var w=colWidth+addW
			col.style.width=colWidth+addW+'px';
			if (tblWidth) me.tbl.style.width=tblWidth+addW+'px';
			//if (me.tbl2) me.tbl2.style.width=tblWidth+addW+'px';
			//if (col2) col2.style.width=w+'px';
			MyFn.eventCancel(event);		
		}
	}
	


	function setCol(i) {
		var cols=me.tbl ? me.tbl.getElementsByTagName("COL") :null;
		col = cols.length>i ? cols[i] : null;
		/*if (me.tbl2) {
			col2=me.tbl2.getElementsByTagName("COL")[i];
		}
        */
	}


	// индекс ячейки для resize
	function cellIndex(event) {
        ind=null;
		event=event || window.event;
		var x = event.pageX;
        var target=event.target || event.srcElement;
        var cell;
        while (target) {
            if (target.tagName=='TH') {
                cell=target;
                break;
            }
            target=target.parentNode;
        }
        if (cell) {
            var tr = cell.parentNode;
            var left = MyFn.offset(cell).left;
            var w = cell.offsetWidth
            if (Math.abs(x-left)<3) {                
                ind = cell.cellIndex-1;
                while (ind>=0 && tr.cells[ind].clientWidth<=0) {
                    ind--;
                }
            } else if (Math.abs(x-left-w)<3) {
                ind = cell.cellIndex;
            }
        }
        if (ind<0) ind = null;
		return ind;
	}

};