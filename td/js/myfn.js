'use strict';
String.prototype.trim = function() {
    return this.replace(/^\s+|\s+$/g, "");
};
var MyFn = {
	eventCancel: function (evt) {	
		evt = evt || window.event;	
		if (evt.stopPropagation)    evt.stopPropagation();
		if (evt.cancelBubble!=null) evt.cancelBubble = true;
		if (evt.preventDefault) evt.preventDefault(); 
        else (evt.returnValue = false);  
	},
	nodeTagClass: function (node, tag, className) {
		var found=true;
		found = found && (node.nodeType === Node.ELEMENT_NODE);
		found = found && (!tag || tag === node.tagName);
		found = found && (!className || MyFn.hasClass(node,className));
		return found;
	},
	nodeParent: function(node, tag, className) {
		var found=false,
			p=node.parentNode;
		while (p) {
            if (MyFn.nodeTagClass(p,tag,className)) break;
			p = p.parentNode;
		}
		return p;
	},
	offset: function(e) {
		var left = 0, top = 0;
		do {
			left += e.offsetLeft;
			top  += e.offsetTop;
		} while (e = e.offsetParent);
		return {"left":left, "top":top};
	},
	
	nodeNext: function(node, tag, className) {
		var p = node.nextSibling;
		while (p) {
            if (MyFn.nodeTagClass(p,tag,className)) break;
			p = p.nextSibling;
		}
		return p;
	},
    
    // node -string query selector
    // node
    // node list
    
    setHandler: function(nodes,event,handler) {
        if (typeof nodes == 'string') {
            nodes=document.querySelectorAll(nodes);
        }
        for (var i=0; i<nodes.length; i++) {
            nodes[i]['on'+event]=handler;
        }
    },
    // set Handler that delegates event to child iddentified by tag
    setLiveHandler: function(nodes,tag,event,handler) {
        if (typeof parent == 'string') {
            var nodes=document.querySelectorAll(nodes);
        } 
        for (var i=0; i<nodes.length; i++) {
            var node=nodes[i];
            node['on'+event]=function(e) {
                e = e || window.evenet;
                var target=e.target || e.srcElement;
                while (target) {
                    if (target===node) { // we have come up to the top
                        target=null;
                        break;
                    }
                    if (target.tagName==tag) break;
                    target=target.parentNode;
                }
                if (target) {
                    handler.call(target,e);
                }
            };
        }
    },
    
    /* class Functions*/
    hasClass: function(node,className) {
        var cl = node.className.split(/\s/);
        var found=false;
        for (var i=0; i<cl.length;i++) {
            if (cl[i]==className) {
                found = true;
                break;
            }
        }

        return found;
    },
    
    addClass: function(node,className) {
        if (! MyFn.hasClass(node,className)) {
            if (node.className) node.className += ' ';
            node.className += className;
        }
    },

    removeClass: function(node,className) {
        var cl = node.className.split(/\s/);
        var found=false;
        var str='';
        for (var i=0; i<cl.length;i++) {
            if (cl[i]===className) {
                cl.splice(i,1);
            }
        }
        node.className= cl.join(' ');
    },  
    toggleClass: function(node,className,flag) {
        if (flag) MyFn.addClass(node,className);
        else MyFn.removeClass(node,className);
    }, 
    hasScrollBar: function() {
        return (document.documentElement.scrollHeight>document.documentElement.clientHeight);
    },

    //Добавление инпута для загрузки в форму
    addNewDogovorFile: function() {
        var maxInputsLength = 3,
            dogovorForm = document.querySelector('#dogovor-files-upload'),
            inputsGroup = dogovorForm.querySelector('.inputs-group'),
            inputsLength = dogovorForm.querySelectorAll('input[type="file"]'),
            newInput = document.createElement('input');

        if(inputsLength.length > maxInputsLength - 1) return false;

        //Добавляем атрибуты новому инпуту
        newInput.setAttribute('type', 'file');
        newInput.setAttribute('name', 'filename[]');
        newInput.setAttribute('multiple', 'true');

        inputsGroup.appendChild(newInput);

        return;
    },

    deleteFileConfirmation: function() {
        var del = confirm('Вы действительно хотите удалить файл?');

        if(del) return true;

        return false;
    },

    addFileDescription: function(elem, id, table, edit) {

        var parent = elem.parentNode.parentNode;

        var oneDescript = parent.querySelector('.one-descript');

        if(oneDescript != null) return;

        var descriptionSpan = document.createElement('span');
        descriptionSpan.className = 's-title one-descript';
        descriptionSpan.innerHTML = 'Описание: ';

        var descriptionValue = document.createElement('span');
        descriptionValue.setAttribute('class', 'descript');
        descriptionValue.innerHTML = '<input type="text" size="30" /><button onclick="MyFn.saveFileDescription(this, '+id+', \''+table+'\')">Сохранить</button><button onclick="MyFn.cancelAddDescription(this)">Отмена</button>';

        if(edit) {
            var ed = parent.querySelector('.descript');
            ed.innerHTML = '<input class="one-descript" type="text" size="32" value="'+ed.innerHTML+'"/><button onclick="MyFn.saveFileDescription(this, '+id+', \''+table+'\')">Сохранить</button><button onclick="MyFn.cancelAddDescription(this, '+edit+')">Отмена</button>';
            return;
        }

        descriptionSpan.appendChild(descriptionValue);

        parent.appendChild(descriptionSpan);
    },

    saveFileDescription: function(elem, id, table) {
        var parent = elem.parentNode;
        var input = parent.querySelector('input[type="text"]');

        $.ajax({
                type: "GET",

                url: "/modules/ajax.php?func=add_file_description&arg1="+input.value+'&arg2='+id+'&arg3='+table,
                success: function(data){
                    parent.innerHTML = data;
                }
        });
    },

    cancelAddDescription: function(elem, edit) {
        var parent = elem.parentNode.parentNode;

        if(edit) {
            var inputValue = elem.parentNode.querySelector('input').value;

            elem.parentNode.innerHTML = inputValue;
        } else {
            parent.parentNode.removeChild(parent);
        }
    },

    /*URL encode*/
    // skipEmpty  - don't encode empty controls. Used in  filter for Excel export
    encodeURL: function _encodeURL(nodes,skipEmpty) {
        var node, i, l = nodes.length, data='', p; // encode result
        for (i = 0; i < l; i++) {
            node = nodes[i];
            if (node.tagName === 'FORM') {
                p = _encodeURL(node);
            } else {
                p = encode(node,skipEmpty);
            }
            if (p) { data += '&' + p; }
        }
        data = data ? data.substr(1) : '';
     //   console.log(data);
        return data;
        function needCheck(node) { //node need checked attr be set
            var type = node.type ? node.type.toLowerCase() : "";
            return (node.tagName === "INPUT" && (type === "checkbox" || type === "radio"));
        }
        function encode(node,skipEmpty) {
            var p;
            if (node.name) {
                if (node.tagName == 'SELECT') {
                    var opts=node.options;
                    var o=[];
                    for (var i=0;i<opts.length;i++) {
                        if (opts[i].selected && opts[i].value!=='_all_') 
                            o.push(encodeURIComponent(node.name) + "=" + encodeURIComponent(opts[i].value));
                    }
                    p=o.join('&');
                } else if (node.tagName == 'INPUT' && (!needCheck(node) || node.checked ) && (!skipEmpty || node.value)) {
                    p = encodeURIComponent(node.name) + "=" + encodeURIComponent(node.value);
                } else if (node.tagName == 'TEXTAREA' && (!needCheck(node) || node.checked ) && (!skipEmpty || node.value)) {
                    p = encodeURIComponent(node.name) + "=" + encodeURIComponent(node.value);
                }            
            }
            return p;
        // проверка node input и checkbox или radiobox
        }    
    }
    /*
    innerHTML: function(node,html) {
        if (node.tagName==="TR") { 
            // for IE
            var div=document.createElement('div');
            div.innerHTML='<table><tr>'+html+'</tr></table>';
            var tbl=div.childNode();
            var tr=tbl.rows(0);
            var cell=tr.insertCell;
            cell.innerHTML=;
            
            
        } else {
            node.innerHTML=html;
        }
    }
    */    
}