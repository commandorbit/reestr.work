/*
uses MyFn NodeParent

*/
function ColResize(tbl) {
    'use strict';
	var me=this;
    me.tbl=tbl;
    me.row=MyFn.nodeParent(tbl.querySelector('TH'),'TR');
	var inResize=false;
	var savedMove;
	var savedUp;
	var col;
    var col2;
    //var me.tbl2,col2;
	var ind; //index of cell to resize
	// tblWidth as start columnWidth
	var tblWidth,colWidth,startX; //MouseX
	var pos=[];
	me.done=function(callback) {
        me.ondone=callback;
    };
    me.initPos=initPos;
    setHandler(me.row);
    
	function setHandler (row) {
		initPos();
		// �� wrap ����� ��� ������� (��������� � ������)
		var div=MyFn.nodeParent(me.tbl,'DIV','td-table-wrap');
		if (div) {
			var tbls=div.getElementsByTagName("TABLE");
			me.tbl2=(tbls.length>1) ? tbls[1] : null;
		}
		row.onmousemove=mouseMove;		
		row.onmousedown=mouseDown;	
	}
	
	function mouseUp(event) {
		if (inResize) {
			inResize=false;
			document.body.style.cursor='';			
			if (col2) col2.style.width=col.style.width;
			col2=col=null;
			initPos();
			eventCancel(event);
			document.onmousemove=savedMove;
			document.onmouseup=savedUp;
			// callAfter
			if (me.ondone) me.ondone(me.tbl);
		}
	};
    
	function mouseDown(event) {
		event=event || window.event;
		ind=cellIndex(event);
		if (ind!==null) {			
			inResize=true;		
			savedMove = document.onmousemove;
			document.onmousemove = mouseMove;
			savedUp = document.onmouseup;
			document.onmouseup = mouseUp;
			document.body.style.cursor = 'col-resize';
			//row.style.cursor= 'col-resize';
			setCol(ind);
			colWidth=parseInt(col.style.width);
			tblWidth=parseInt(me.tbl.style.width);
			startX = event.pageX;			
			eventCancel(event);
		}
	}
	
	function mouseMove(event) {
		event=event || window.event;
		if (!inResize) {
			var i=cellIndex(event);
			me.row.style.cursor = (i!==null) ? 'col-resize' : '';
		} else {
			var addW=event.pageX-startX;
			var w=colWidth+addW
			col.style.width=colWidth+addW+'px';
			if (tblWidth) me.tbl.style.width=tblWidth+addW+'px';
			if (me.tbl2) me.tbl2.style.width=tblWidth+addW+'px';
			if (col2) col2.style.width=w+'px';
			eventCancel(event);		
		}
	}
	
	function eventCancel(evt) {	
		evt = evt || window.event;	
		if (evt.stopPropagation)    evt.stopPropagation();
		if (evt.cancelBubble!=null) evt.cancelBubble = true;
		if (evt.preventDefault) evt.preventDefault(); 
        else (evt.returnValue = false);  
	}

	function setCol(i) {
		var cols=me.tbl ? me.tbl.getElementsByTagName("COL") :null;
		col = cols.length>i ? cols[i] : null;
		if (me.tbl2) {
			col2=me.tbl2.getElementsByTagName("COL")[i];
		}
	}

	function initPos() {
		pos=[];
		var length=me.row.cells.length
		var left=MyFn.offset(me.row).left;
		// for fixed position
		if (left<0) left=0;
		for (var i=0;i<length;i++) {
			var c=me.row.cells[i];
			var p=left+c.offsetLeft+c.offsetWidth;
			pos.push(p);
		}
	}

	// ������ ������ ��� resize
	function cellIndex(event) {
		event=event || window.event;
		var x = event.pageX;
		var ind=null;
		for (var i=0;i<pos.length;i++) {
			if (Math.abs(x-pos[i])<3) {
				ind=i;
			}
		}
		return ind;
	}

};