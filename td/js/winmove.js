//document.getElementById('edittitle').onmousedown=winMove.handler;
//on (winmove.sethandler(this)

var WinMove=(function () {
	var elem,win,box,x,y;
	var xM,yM; //Mouseoffset
	var oldMouseMove;
	var oldMouseUp;
	var maxX,maxY ;
	if (window.innerHeight)  maxY=window.innerHeight-10;
	if (window.innerWidth)   maxX=window.innerWidth-10;
//	console.log('maxY'+maxY);
	
	function mouseMove(event) {
		event = event || window.event;
		x = event.clientX+xM;
		y = event.clientY+yM;	
		if (x > maxX ) x=maxX;
		if (y > maxY ) y=maxY;
		if (y < 0 ) y=0;
		boxPos();
		eventCancel(event);
	}
	function mouseUp(event) {
//		console.log ('y='+y);
		document.body.removeChild(box);
		document.onmousemove=oldMouseMove;
		document.onmouseup=oldMouseUp;
        box=null;
		win.style.left=x+'px';
		win.style.top=y+'px';		
                win.style.display='block';
	}
	// source here
	// http://erik.eae.net/archives/2007/07/27/18.54.15/
	function getStyle(el, prop) {
	  if (document.defaultView && document.defaultView.getComputedStyle) {
		return document.defaultView.getComputedStyle(el, null)[prop];
	  } else if (el.currentStyle) {
		return el.currentStyle[prop];
	  } else {
		return el.style[prop];
	  }
	}
	function boxPos() {
		box.style.left=x+'px';
		box.style.top=y+'px';		
	}
	function boxInit() {
		box = document.createElement('div');
		box.className='modal-editbox-move';
		box.style.height=win.clientHeight + 'px';
		box.style.width=win.clientWidth + 'px';				
		box.style.position='fixed';	
		box.style.zIndex=getStyle(win,'zIndex')+1;
		//win.parentOffset.
                win.style.display='none';
		document.body.appendChild(box);
	}
	function eventCancel(evt) {		
		if (evt.stopPropagation)    evt.stopPropagation();
		if (evt.cancelBubble!=null) evt.cancelBubble = true;
		evt.preventDefault ? evt.preventDefault() : (evt.returnValue=false);  
	}
	function handler (ctl,event) {				
			elem=ctl;
			win=ctl.parentNode;
			x=win.offsetLeft;
			y=win.offsetTop;
			boxInit();
			boxPos();
			event = event || window.event;
			xM = x-event.clientX;
			yM = y-event.clientY;
			oldMouseMove=document.onmousemove;
			oldMouseUp=document.onmouseup
			document.onmousemove=mouseMove;
			document.onmouseup=mouseUp;
			eventCancel(event);
	}	
	return {
		handler:handler
	}
})();