function Menu (selMain,selSubMenu) {

    var menu=document.querySelectorAll(selMain);
    for (var i=0; i<menu.length; i++) {
        menu[i].onmouseenter=onMainMenuMouseEneter;
        menu[i].onmouseleave=onMainMenuMouseLeave;
    }
    
    menu=document.querySelectorAll(selSubMenu);
    for (var i=0; i<menu.length; i++) {
        menu[i].onmouseenter=onSubMenuMouseEnter;
        menu[i].onmouseleave=onSubMenuMouseLeave;
    }
    

    function parentMenu(subMenu) {
         var id=subMenu.id;
         return document.querySelector('a[href="#'+id+'"]');
    }
    
    function subMenu(menu) {
        var id=menu.getAttribute('href').substr(1); //remove#;
        return document.getElementById(id);           
    }
    
    function hideOpened() {
        if (Menu.opened) {
            Menu.opened.style.display='none';
            var parent=parentMenu(Menu.opened);
            MyFn.removeClass(parent,'active');
        }
    }
    
    function onSubMenuMouseEnter() {
        console.log('sub');
        clearTimeout(Menu.timerhide);
        var m=parentMenu(this);
        MyFn.addClass(m,'active'); 
    }
    
    function onSubMenuMouseLeave() {
        Menu.timerhide = setTimeout(hideOpened, 500);
    }
    
    function onMainMenuMouseEneter() {
        clearTimeout(Menu.timerhide);
        menu=subMenu(this);
        if (menu!==Menu.opened) hideOpened();
        MyFn.addClass(this,'active');
        var of = MyFn.offset(this);
        menu.style.left =(of.left+1) + 'px';
        // зазор 2px а нужен??
        menu.style.top = (of.top+this.offsetHeight+2) +'px';
        menu.style.minWidth = this.offsetWidth+'px';;
        menu.style.display='block';
        Menu.opened=menu;            
    }
    function onMainMenuMouseLeave() {
        Menu.timerhide = setTimeout(hideOpened, 70);
    }
}
Menu.timerhide=null;
Menu.opened=null;