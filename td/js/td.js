'use strict';
/*jslint plusplus: true */

var libUnicorn = libUnicorn || {};

function overlay(fn) {
    var el=document.getElementById('overlay-level');
    if(!el) return false;
    el.style.display = "block";
    el.onclick = function() {el.style.display ='none'; fn() };
}
function overlayHide() {
   var el=document.getElementById('overlay-level');
   if(!el) return false;
   if (el)   el.style.display = "none";
}
var FixPos = (function() {
    var fixedObj = [];
    var index = -1;
    var useBottom = false;
    //var timer = null; //vik: unused
    function needFix(obj) {
        return window.pageYOffset > obj.top && (!useBottom || window.pageYOffset < obj.bottom);
    }

    function fix() {
        _fix();
        /*    if (timer) clearTimeout(timer);
         setTimeout(_fix,10);
         */
    }

    function _fix() {
        if (index >= 0) {
            var obj = fixedObj[index];
            if (!needFix(obj)) {
                obj.header.style.position = 'static';
                obj.body.style.marginTop = 0;
                index = -1;
            }
        }

        for (var i = fixedObj.length - 1; i >= 0; i--) {
            var obj = fixedObj[i];
            if (needFix(obj)) {
                if (index !== i) {
                    obj.header.style.position = 'fixed';
                    obj.header.style.top = '0px';
                    obj.header.style.zIndex = '1';
                    obj.x = window.pageXOffset;
                    obj.body.style.marginTop = obj.header.offsetHeight + 'px';
                    obj.header.style.left = obj.left - obj.x + 'px';

                    index = i;
                }
                if (obj.fixedX !== (obj.left - window.pageXOffset)) {
                    obj.fixedX = (obj.left - window.pageXOffset);
                    obj.header.style.left = obj.fixedX + 'px';
                }
            }
        }
    }
    
    function setup(nodes, _useBottom) {
        if (_useBottom)
            useBottom = _useBottom;
        useBottom = (nodes.length > 0);
        for (var i = 0; i < nodes.length; i++) {
            var header = nodes[i];
            var body = null;
            var body = MyFn.nodeNext(header, 'TABLE');
            if (!body)
                throw ('FixPos no second table for!');
            fixedObj.push({header: header, body: body});
        }
        if (fixedObj.length > 0) {
            window.onscroll = fix;
            window.onresize = init;
        }
        init();
    }
    
    function init() {
        var timeStart = (new Date().valueOf());
        for (var i = 0, l = fixedObj.length; i < l; i++) {
            var offset = MyFn.offset(fixedObj[i].body);
            fixedObj[i].top = offset.top - fixedObj[i].header.offsetHeight;
            if (useBottom)
                fixedObj[i].bottom = fixedObj[i].body.offsetHeight + offset.top - fixedObj[i].header.offsetHeight;
            //console.log(offset.left);
            fixedObj[i].left = offset.left;
        }
        console.log('fix init end ' + (new Date().valueOf() - timeStart));
        fix();
        FixPos.hasScrollBar = MyFn.hasScrollBar();
    }
    return {
        reinit: init,
        setup: setup
    };
})();

function has_nulls(form) {
    var inps = form.querySelectorAll('.not_null');
    var hasNulls = false;
    for (var i = 0; i < inps.length; i++) {
        if (inps[i].name !== 'id') {
            var isNull = (!inps[i].value);
            MyFn.toggleClass(inps[i], 'attention', isNull);
            if (isNull)
                hasNulls = true;
        }
    }
    return hasNulls;
}

function TD(wrap, params) {
    "use strict";
    var me = this;
    me.wrap = wrap;
    
	me.redraw = setJobs;
	
    me.checkedRows = []; //rows with ceckbox checked
    me.multiselectFilter = {};
    me.saveURL = TD.saveURL;
    me.restoreURL = TD.restoreURL;
    me.saveURLMany = TD.saveURLMany;
    me.mergeURL = TD.mergeURL;
    me.delURL = TD.delURL;
    me.toExcelURL = TD.toExcelURL;
    me.saveUserCols = TD.saveUserCols;
    me.data = params.data; //  array or all row objects
    me.columns = params.columns; // array of columns objects
    me.mayEdit = params.mayEdit;
    me.showCheckBox = params.showCheckBox;
    me.dataFilter=params.dataFilter; //array of how data is filtered to use in export
    me.defaultFilter=params.defaultFilter; 
    me.filerBeforeColumnMove = null;
    // for save new rec
    if (params.foreignKey) {
        me.foreignKey=params.foreignKey;
    }
    if (params.primaryKey) {
        me.primaryKey=params.primaryKey;
    }
    
    for (var fld in me.columns) {
        if (me.columns[fld].options)
            setColumnOrderedOptions(me.columns[fld]);
    }
    me.filteredData = null; // array of filtered rows from data array
    me.flds = null; // array of fields in right order
    me.orderColumn = null; // order Column
    me.orderColumnDesc = 0; //is order desc
    me.found = 0; //found counter
    me.rowEdited = {}; // редактиремый row
    me.rowsPerPage = 100;//
    me.pageNow = 0;
    me.countPages = 0;
    me.lastPage = null;
    me.selectedTr = null;
    me.showFullText = false;
    me.filtersEditor = null;
    me.savedFiltersList = null;
	me.favoritesColorKey = 2

    me.prevOrdSpan = null;

    var selMax = wrap.querySelector('select[name="maxrows"]');
    if (selMax) {
        me.rowsPerPage = selMax.value; // max Rows to show
        selMax.onchange = function() {
            me.rowsPerPage = selMax.value;
            filterApply();
        };
    }
    me.itogo = null; // {col1:itogo1,col2:itogo2,...}
    me.filter = null; //filterRules [{col:col1Name,sign:(==,<,>,in)  ,value:array or value},...]
    me.filterOptions = null; //{fld1:{val1:1,val2:1,..} ,...} //unique options for  select.filter elemtns
    me.fixed = wrap.querySelector('table.fixed');
    // colMove
    me.dataRequest = null;
    me.trEdited = null; //DOM TR of edited row saved to remove class edited
    var box = wrap.querySelector('div.modal-editbox');

    if (box) {
        me.editBox = new EditBox(box);
        me.editBox.ondelete = onDel;
        me.editBox.deleteChecked = onDelChecked;
        me.editBox.onclose = editBoxOnclose;

        console.log(box);

        //new resizable(box, {directions: ['southeast']});
    }
    if (me.showCheckBox) {
        me.checkAll = wrap.querySelector('th.check-all span.checkbox');
        me.checkAll.onclick = checkAllClick;
        var dropdown = me.checkAll.parentNode.querySelector('.td-drop-down-arrow');
        dropdown.style.display = 'none';

        if(me.mayEdit) {
            dropdown.style.display = 'inline-block';

            me.checkAllAction=wrap.querySelector('.check-all-action');
            wrap.querySelector('th.check-all').onclick=function () {
                me.checkAllAction.style.display='block';
                overlay (function() {me.checkAllAction.style.display='none'});
            }
            
            wrap.querySelector('.check-all-action .edit').onclick=function(evt) {
                if (MyFn.hasClass(me.checkAll,'checked') || MyFn.hasClass(me.checkAll,'uncheck')) {
                    me.editBox.onsave = onSaveChecked;
                    me.editBox.editChecked();
                } else {
                    alert ('Сначала отметьте записи!');
                }
                me.checkAllAction.style.display='none';
                overlayHide();
                MyFn.eventCancel(evt);
            }
            
            wrap.querySelector('.check-all-action .del').onclick=function(evt) {
                evt = evt || window.event;
                var target = evt.target || evt.srcElement;
                if (MyFn.hasClass(me.checkAll,'checked') || MyFn.hasClass(me.checkAll,'uncheck')) {
                    if (confirm("Вы уверены?")) {
                        var form = me.wrap.querySelector('form.editbox-form');

                        me.editBox.deleteChecked(form);
                    } 
                } else {
                    alert ('Сначала отметьте записи!');
                }
                me.checkAllAction.style.display='none';
                overlayHide();
                MyFn.eventCancel(evt);
            }
        }
    }
    var mbox = wrap.querySelector('div.modal-mergebox');
    if (mbox) {
        me.mergeBox = new EditBox(mbox);
        me.mergeBox.onmerge = onMerge;      
    }
    init();
    // hide invisible columns
    // better to hide in php
    onLayoutChange(me.DomTable);
	setDefaultFilter(me.defaultFilter);
    filterApply();

    function  editBoxOnclose() {
        if (me.trEdited) {
            MyFn.removeClass(me.trEdited, 'edited');
            me.trEdited = null;
        }
    }
    
    function forChecked(fn) {
        for (var i = 0, l = me.filteredDataSearched.length; i<l && (me.rowsPerPage === '_all_' || i < me.rowsPerPage); i++) {
            var row = me.filteredDataSearched[i];
            var index = me.checkedRows.indexOf(row);

            fn(row,index);
        }
    }
    
    function checkAllSetClass() {
        var el=me.checkAll;
        var notFound=false;
        var found=false;
        forChecked(function(row,index) {
                if (index>=0) found=true;
                else notFound=true;
            }
        ); 

        MyFn.removeClass(el,'checked');
        MyFn.removeClass(el,'uncheck');
        
        if (found && !notFound) {
            MyFn.addClass(el,'checked');
        } else if  (found && notFound) {
            MyFn.addClass(el,'uncheck');
        }

        createZayav(me.checkedRows);
    }
    
    
    function checkAllClick(evt) {
        var el=me.checkAll;
        if (MyFn.hasClass(el,'checked') || MyFn.hasClass(el,'uncheck')) {
            forChecked(function(row,index) {
                if (index>=0) me.checkedRows.splice(index,1);
            }); 
            MyFn.removeClass(el,'checked');
            MyFn.removeClass(el,'uncheck');
        } else { // check all
            forChecked(function(row,index) {
                if (index==-1)  me.checkedRows.push(row);
            }); 
            MyFn.addClass(el,'checked')
        }
        displayRows();
        MyFn.eventCancel(evt);
    }
    function hideColumn(tbl,ind) {
        for (var i=0; i<tbl.rows.length; i++) {
            var cell=tbl.rows[i].cells[ind];
            MyFn.addClass(cell,'hidden');
            var span=cell.querySelector("span.td-order");
            if (span) span.innerHTML='';
        }
    }
    function showColumn(tbl,ind,label) {
        for (var i=0; i<tbl.rows.length; i++) {
            var cell=tbl.rows[i].cells[ind];
            MyFn.removeClass(cell,'hidden');
            var span=cell.querySelector("span.td-order");
            if (span) span.innerHTML=label;
        }
    }
    // is Called after resize or change of columnns order 
    function onLayoutChange(tbl) {
        var cols = tbl.getElementsByTagName("COL");
        var cols2 = null;
        var tblWidth=0;
        var tbl2 = MyFn.nodeNext(tbl);
        if (tbl2 && tbl2.tagName === 'TABLE') {
            cols2 = tbl2.getElementsByTagName("COL");
        }
        setFlds(); //update fields order from hidden inputs or what else
        var data = {};
        for (var i = 0, l = me.flds.length; i < l; i++) {
            var ind = i;
            if (me.showCheckBox) ind++;
            var fld = me.flds[i];
            var hidden = me.columns[fld].hidden;
            var w = me.columns[fld].width;
            if (hidden) {
                cols[ind].style.width = 0;                
                hideColumn (tbl,ind);
            } else {
                var wCur = parseInt(cols[ind].style.width);
                if (wCur) {
                    me.columns[fld].width = w = wCur;
                } else {
                    showColumn (tbl,ind,me.columns[fld].label);
                    cols[ind].style.width = w + 'px';
                    tblWidth += w;
                }
            }
            // synchronize column styles of second table if exists
            if (cols2 && cols2[ind]) {
                cols2[ind].style.cssText = cols[ind].style.cssText;
            }
            data[fld] = {name: fld, width: w, hidden: hidden};
        }
        tbl2.style.width = tbl.style.width = tblWidth +'px';
        // save layout for this url in session data
        // (it is saved by init.php)
        var url = window.location.pathname + '?layout_change&' + filterURL(wrap);
        console.log(url);
        Ajax({
            type: "POST",
            url: url,
            data: JSON.stringify(data),
            success: function(data) {
                console.log(data);
            }
        });
        // I use it to  fill table in  correct column order hmm((.
    }

    function init() {
        var wrap = me.wrap;

        // fixPos      
        me.paramForm = wrap.querySelector('form[name="td-table-params"]');
        me.sqlTable = me.paramForm['t'].value;
        me.afterUpdate = null; // hander after update insert delete
        me.fixed = wrap.querySelector('table.fixed');
        if (me.fixed)
            FixPos.setup([me.fixed]);
        // если две таблиц, то у верхней шапки нет tbody
        // если две - то верхняя
        me.DomTable = wrap.querySelector('table.td-table');
        // Resize
        colMoveResize(me.DomTable, function(tbl) {
            //me.filerBeforeColumnMove = me.filter;
            onLayoutChange(tbl);
            FixPos && FixPos.reinit();
            //init();
            filterApply();
            //me.filerBeforeColumnMove = null;
            //_setFiltersSelected();
        });

        me.DomItogoRow = wrap.querySelector('table.td-table tr.itogo');
        me.DomFilterRow = wrap.querySelector('table.td-table tr.filter'); //Убирем строку с фильтрами TODO
        me.DomTbody = wrap.querySelector('table.td-table tbody');
        me.TextSearch = wrap.querySelector('input[name="search"]');
        if (me.TextSearch) {
            me.TextSearch.oninput = function() {
                filterSearchText(this.value);
            };
        }
		
        MyFn.setHandler(queryAll('th span.td-order'), 'click', onOrdClick);
//        restoreFitlerSelect();
        MyFn.setHandler(queryAll('.td-table tbody'), 'click', trSingleClick);
        MyFn.setHandler(queryAll('.td-table tbody'), 'dblclick', bodyClick);
        MyFn.setHandler(queryAll('.td-add-button'), 'click', addClick);
        MyFn.setHandler(queryAll('.td-filters-remove-button'), 'click', function() { _clearAllFilters(); filterApply(); });
        MyFn.setHandler(queryAll('ul.show-columns li'), 'click', toggleSelected);
        MyFn.setHandler(queryAll('span.show-columns'), 'click', toggleShowColumnsList);
        MyFn.setHandler(queryAll('.td-merge-button'), 'click', recMerge);
        MyFn.setHandler(queryAll('.td-xls-button'), 'click', toExcel);
        MyFn.setHandler(queryAll('.td-show-full'), 'click', toggleFullCellText);
        //MyFn.setHandler(queryAll('.td-save-filters'), 'click', toggleFiltersEditor);
        MyFn.setHandler(queryAll('input.filter'), 'change', filterApply);
        MyFn.setHandler(queryAll('input.filter'), 'search', filterApply);

        document.addEventListener('keydown', function(e) {
            if(me.selectedTr != null) {
                var sib = null;
                if(e.keyCode == 40) {
                    sib = me.selectedTr.nextSibling;
                } else if(e.keyCode == 38) {
                    sib = me.selectedTr.previousSibling;
                } else if(e.keyCode == 13) {
                    var evt = document.createEvent("HTMLEvents");
                    evt.initEvent("dblclick", bodyClick);
                    me.selectedTr.dispatchEvent(evt);
                }
                if(sib != null) {
                    me.selectedTr.classList.remove('active');
                    me.selectedTr = sib;
                    me.selectedTr.classList.add('active');
                    if(me.selectedTr.offsetTop > (document.documentElement.scrollTop + window.innerHeight) - 330) {
                        window.scrollTo(0, document.documentElement.scrollTop + me.selectedTr.offsetHeight + 30);
                    }
                    e.preventDefault();
                    return false;
                }
            }
        });

        var selects = queryAll('span.multiselect-control');
        for (var i=0, l = selects.length; i < l; i++) {
            var element = selects[i];
            var fld = selects[i].getAttribute('data-name');
            var obj = null;
            fld = fld.substr(2);
            if(!me.columns[fld]) continue;
            if(me.columns[fld].tree_options && me.columns[fld].tree_options.length) {
                obj = libUnicorn.TreePlugin.create(element, false, {opened: false, checkboxes: true});
                new resizable(obj.container, {directions: ["southeast"]});
                continue;
            }

            if(me.columns[fld].type == 'date') {
                obj = libUnicorn.TreePlugin.create(element, false, {opened: false, checkboxes: true});
            } else if(me.columns[fld].type == 'favorite') {
                obj = libUnicorn.FavoritesFilter.create(element, me.columns[fld].type);
            } else if(me.columns[fld].options) {
                obj = libUnicorn.MultiSelect.create(element, false);
            } else {
                obj = libUnicorn.ColumnsFilter.create(element, me.columns[fld].type);
            }

            new resizable(obj.container, {directions: ["southeast"]});
        }


        var change_color_control = me.wrap.querySelector('.favorite.change_color');
        if (change_color_control) libUnicorn.FavoriteColorSelect.create(change_color_control, 'varchar', setFavoritesColorKey);

        //MyFn.setHandler( queryAll('.multiselect-control') , 'click', filterHandler);
        MyFn.setHandler(queryAll('.multiselect-control') , 'change', filterOnChange);
		
        //if(window.location.pathname == '/tedit.php') createFiltersEditor();
        //getFilterDefault();
    }
    
    function toggleShowColumnsList() {
        var ul = me.wrap.querySelector('ul.show-columns');
        if (ul.style.display=='block') {
            ul.style.display='none';
        } else {
            ul.style.left = MyFn.offset(this).left + 'px';
            ul.style.display = 'block';
            overlay(toggleShowColumnsList); 
        }
    }
    
    function toggleSelected() {
        var has=MyFn.hasClass(this,'selected');
        MyFn.toggleClass(this,'selected',!has);

        var ul = me.wrap.querySelector('ul.show-columns');

        var li = ul.querySelectorAll('li');

        for (var i=0, l = li.length; i < l; i++) { 
            var visible = MyFn.hasClass(li[i],'selected');
            var fld = li[i].getAttribute('data-fld-name');
            me.columns[fld].hidden = !visible;
        }

        var hiddenCols = [];
        for(var key in me.columns) {
            if(me.columns[key].hidden) hiddenCols.push(key); 
        }

        $.ajax({
            type: "POST",
            url: me.saveUserCols,
            data: {'table': me.sqlTable, 'cols':hiddenCols},
            success: function(data){
                var d = JSON.parse(data);
                if (d && d.status != 'OK') {
                    alert ('Не удалось сохранить колонки в БД. Обратитесь к администратору.');
                    console.log(data);
                }
            }
        });
        onLayoutChange(me.DomTable);
        displayRows();
    }

    function toObject(arr) {
  var rv = {};
  for (var i = 0; i < arr.length; ++i)
    rv[i] = arr[i];
  return rv;
}

	function setFavoritesColorKey(key) {
        me.favoritesColorKey = key;
    }

    function getFavoritesColorKey() {
        return me.favoritesColorKey;
    }

    //FILTERS EDITOR
    function toggleFiltersEditor() {
        if(me.filtersEditor) me.filtersEditor.classList.toggle('active');
    }
    
    function createFiltersEditor() {
        var editor = document.createElement('div');
        var save = document.createElement('button');
        var deflt = document.createElement('button');
        var apply = document.createElement('button');
        var savedList = document.createElement('select');
        getSavedFilters(savedList);
        editor.classList.add('filters-editor');
        save.innerHTML = 'Сохранить текущий набор';
        deflt.innerHTML = 'Установить выбранное по умолчанию';
        apply.innerHTML = 'Применить выбранное';
        editor.appendChild(apply);
        editor.appendChild(save);
        editor.appendChild(deflt);
        editor.appendChild(savedList);
        me.filtersEditor = editor;
        me.savedFiltersList = savedList;
        var saveFilters = document.querySelector('.td-save-filters');
        saveFilters.appendChild(editor);

        save.addEventListener('click', saveCurrentFilters);
        savedList.addEventListener('click', function(e) {e.cancelBubble = true;});
        apply.addEventListener('click', function() {
            var id = me.savedFiltersList.options[me.savedFiltersList.selectedIndex].value;
            setFilterById(id);
        });
        deflt.addEventListener('click', function() {
            var id = me.savedFiltersList.options[me.savedFiltersList.selectedIndex].value;
            setFilterDefault(id);
        });

        console.log('filters editor init');
    }

    function getSavedFilters(select) {
        $.ajax({
            type: "POST",
            url: "/td/save_user_filters.php",
            data: {'type':'take_options', 'table':me.sqlTable, 'data':false},
            success: function(data){
                data = JSON.parse(data);
                select.innerHTML = '';
                for(var key in data.result) {
                    var option = document.createElement('option');
                    option.value = data.result[key].id;
                    option.innerHTML = data.result[key].name;
                    select.appendChild(option);
                }
            }
        });
    }

    function saveCurrentFilters() {
        var name = prompt('Введите название нового набора:');
        if(name) {
            $.ajax({
                type: "POST",
                url: "/td/save_user_filters.php",
                data: {'type':'save', 'table':me.sqlTable, 'data':JSON.stringify(me.filter), name: name},
                success: function(data){
                    data = JSON.parse(data);
                    if(data.status == 'OK' && data.message) {
                        alert(data.message);
                        getSavedFilters(me.savedFiltersList);
                    }
                }
            });
        }
    }

    function _clearAllFilters() {
        var all_filters = queryAll('.multiselect-control');
        for(var i = 0; i < all_filters.length; i++) {
            var ctrl = libUnicorn.getControl(all_filters[i]);
            ctrl.selected = Object.create(null);
            all_filters[i].classList.remove('filtered');
        }        
    }

    function _setFiltersSelected() {
        _clearAllFilters();
        for (var fld in me.filter) {
            var sel = Object.create(null);
            var control = _getFilterControl(fld);
            var obj = libUnicorn.getControl(control);  
            if(obj instanceof libUnicorn.MultiSelect) {
                for(var key in me.filter[fld].value) {
                    var val = me.filter[fld].value[key];
                    sel[val] = true;
                }  
            } else if(obj instanceof libUnicorn.ColumnsFilter) {
                sel = me.filter[fld];
            }
            obj.selected = sel;
            control.classList.add('filtered');
        }
    }

    function setFilterById(id) {
        $.ajax({
            type: "POST",
            url: "/td/save_user_filters.php",
            data: {'type':'get_by_id', 'table':me.sqlTable, 'id': id},
            success: function(data){
                data = JSON.parse(data);
                if(data.status == 'OK' && data.result) {
                    var res =  JSON.parse(data.result);
                    me.filter = res;
                    _setFiltersSelected();
                    filterProcess(false, me.filter);
                }
            }
        });
    }

    function getFilterDefault() {
        $.ajax({
            type: "POST",
            url: "/td/save_user_filters.php",
            data: {'type':'get_default', 'table':me.sqlTable},
            success: function(data){
                data = JSON.parse(data);
                if(data.status == 'OK' && data.result) {
                    me.filter = JSON.parse(data.result);
                    _setFiltersSelected();
                    filterProcess(false, me.filter);
                }
            }
        });
    }

    function setFilterDefault(id) {
        $.ajax({
            type: "POST",
            url: "/td/save_user_filters.php",
            data: {'type':'set_default', 'table':me.sqlTable, id:id},
            success: function(data){
                data = JSON.parse(data);
                if(data.status == 'OK' && data.message) {
                    alert(data.message);
                }
            }
        });
    }


    //editbox del checked
    function onDelChecked(form) {
        if (!me.mayEdit) return;
        //var wrap = me.wrap; //vik: unused
        var url = me.delURL;
        url += '?t=' + me.sqlTable;
        var checkedRows = me.checkedRows;
        var data = [], items = [];

        for(var key in checkedRows) {
            var data_url='';
            items.push(checkedRows[key]);
            data.push(checkedRows[key]['id']);
        }

        $.ajax({
            type: "POST",
            url: url,
            data: {'id':data},
            success: function(data){
                var d;
                try {
                    d = JSON.parse(data);
                    console.log(d);
                    if (d && d.status=='OK') {
                        for(var i = 0; i < items.length; i++) {
                            me.data.splice(me.data.indexOf(items[i]), 1);
                        }

                        //me.data.splice(i,1);
                        // заменить на remove одной TR
                        filterApply();
                    if (me.afterUpdate) me.afterUpdate();
                    } else {
                        alert ('Error ' + (d.msg || ''));
                    }
                }   catch (e) {
                    alert ('Error in server respons!');
                    console.log(data);
                }
            }
        });
        
    }

    // editbox del click
    function onDel(form) {
        if (!me.mayEdit) return;
        //var wrap = me.wrap; //vik: unused
        if (!me.rowEdited) {
            alert ('Error unknown record to delete!');
            return ;
        }
        var url = me.delURL;
        url += '?t=' + me.sqlTable;
        var data = MyFn.encodeURL(form);

        var i=me.data.indexOf(me.rowEdited);
        if (i<0) {
            alert ('Error unknown row index!');
            return ;
        }
        // synchronos Ajax ?
        Ajax({
            type: "POST",
            url: url,
            data: data,
            done: function(data) {
                var d;
                try {
                    d = JSON.parse(data);
                    console.log(d);
                    if (d && d.status=='OK') {
                        me.data.splice(i,1);
                        // заменить на remove одной TR
                        filterApply();
                        if (me.afterUpdate) me.afterUpdate();
                    } else {
                        alert (d.message || d.msg || 'Возникла непредвиденная ошибка, обратитесь к администратору');
                    }
                }   catch (e) {
                    alert ('Error in server respons!');
                    console.log(data);
                }
            }
        });
    }
    // mergebox ok click
    function onMerge(form) {
        if (!me.mayEdit) return;
        var url = me.mergeURL;
        url += '?t=' + me.sqlTable;
        var data = MyFn.encodeURL(form);        
        Ajax({
            type: "POST",
            url: url,
            data: data,
            done: function(data) {
                var d;
                try {
                    d = JSON.parse(data);
                    console.log(d);
                    if (d && d.status=='OK') {
                        d.row.updated=true;
                        for (var i = 0; i < me.data.length; i++) {
                           if (me.data[i].id == d.row.id) {
                            me.rowEdited = me.data[i];
                                        for (var fld in d.row) {
                                            me.rowEdited[fld] = d.row[fld];
                                        }
                              me.rowEdited.updated = true;
                              break;
                           } 
                        }
                        for (var i = 0; i < me.data.length; i++) {
                           if (me.data[i].id == d.row_del_id) {
                            me.data.splice(i,1);
                              break;
                           } 
                        }
                        // заменить на remove одной TR
                        filterApply();
                        if (me.afterUpdate) me.afterUpdate();
                    } else {
                        alert ('Error: ' + (d.msg || 'unknown'));
                    }
                }   catch (e) {
                    alert ('Error in server respons!');
                    console.log(data);
                }
            }
        });
    }
      // editbox save click with checkboxes
    function onSaveChecked(form) {
        //var wrap = me.wrap; //vik: unused
        var url = me.saveURLMany;
        url += '?t=' + me.sqlTable;
        var elems=[]
        for (var i=0; i<form.elements.length; i++) {
            if (form.elements[i].style.display !== 'none') {
                elems.push(form.elements[i]);
            }
        }
        var data = MyFn.encodeURL(elems);
        if (me.foreignKey) {
            for (var fld in me.foreignKey) {
                data+='&'+fld+'='+me.foreignKey[fld];
            }
        }
        var pk=[];
        forChecked(function(row,index) {
             if (index>=0) data += "&" + me.primaryKey+"[]="+row[me.primaryKey];
        });
        Ajax({
            type: "POST",
            url: url,
            data: data,
            done: function(data) {
                var d;
                try {
                    d = JSON.parse(data);
                }   catch (e) {
                    alert ('Error in server respons!');
                // Save ot server error log?
                    console.log('error json parse');
                    console.log(data);
                }
                
                if (d && d.status=='OK') {
                    var i=0;
                    forChecked(function(row,index){
                        if (index>=0) { // index>=0 => checked
                            if (row[me.primaryKey]!=d.rows[i][me.primaryKey]) {
                                // smthing strange happened. User selected other rows or changed filteredData
                                window.location = window.location;
                                return;
                            }
                            for (var fld in d.rows[i]) {
                                row[fld] = d.rows[i][fld];
                                row.updated = true;
                            }
                            i++;
                        }
                    });
                    displayRows();
                    if (me.afterUpdate) me.afterUpdate();
                } else if (d && d.msg) {
                    alert (d.msg || 'Unknown error !');
                }
            }
        });
    }
    
    // editbox save click
    function onSaveSingleRow(form) {
        var url = me.saveURL;
        url += '?t=' + me.sqlTable;
        save (form,url);
    }
    
    // editbox Restore Click
    function onRestore(form) {
        var url = me.restoreURL;
        url += '?t=' + me.sqlTable.substr(4); // remove log_
        save (form,url);
    }
    
    function onCopy(form) {
        var url = '/td/copy_js.php';
        url += '?t=' + me.sqlTable;
        save (form,url);
        console.log('copy!');
    }

    function save(form,url) {
        var data = MyFn.encodeURL(form);
        if (me.foreignKey) {
            for (var fld in me.foreignKey) {
                data+='&'+fld+'='+me.foreignKey[fld];
            }
        }

        // synchronos Ajax ?
        Ajax({
            type: "POST",
            url: url,
            data: data,
            done: function(data) {
                var d;
                try {
                    d = JSON.parse(data);
                    console.log(d);
                }   catch (e) {
                    alert ('Error in server respons!');
                // Save ot server error log?
                    console.log('error json parse');
                    console.log(data);
                }
                
                if (d && d.status=='OK') {
                    d.row.updated=true;
                    // update
                    if (me.rowEdited) {
                        for (var fld in d.row) {
                            me.rowEdited[fld] = d.row[fld];
                        }
                        me.rowEdited.updated = true;                   
                    } else {
                        console.log('insert');
                        me.data.splice(0,0,d.row);
                        me.filteredDataSearched.splice(0,0,d.row);
                    }

                    // заменить на reload одной записи
                    displayRows();
                    setItogo();
                    //filterApply();
                    if (me.afterUpdate) me.afterUpdate();

                    //console.log(me.filter);

                    //only for dropdownSticker(plugin) test
                    /*var upd = me.DomTbody.querySelectorAll('.updated');

                    var menu = {
                        r1: 'Пункт меню 1',
                        r2: 'Пункт меню 2',
                        r3: 'Пункт меню 3',
                    };

                    for(var i = 0; i < upd.length; i++) {
                        upd[i].innerHTML += '<div class="notice" style="position: relative;"></div>';
                        var notice = upd[i].querySelector('.notice');
                        dropdownSticker.init(notice, menu, dsChange);
                    }*/
                    //end dropdownSticker(plugin)
                } else if(d && d.status=='NOTOK') {
                    alert (d.message);
                }
                if(d && d.status == 'COPIED') {
                    //me.data.splice(0,0,d.row);
                    d.row.updated = true;
                    me.data.unshift(d.row);
                    filterApply();
                    if (me.afterUpdate) me.afterUpdate();
                }

            }
        });
    
    }

    function queryAll(selector) {
        return me.wrap.querySelectorAll(selector);
    }

    function setDefaultFilter(filter) {
        var elements = queryAll('.multiselect-control');
        var el = null;
        for(var fld in filter) {
            if ( el = elByName(fld)) {
             //   res[filterVal] = true;
                var filterVal = filter[fld];          
                var ctrl = libUnicorn.getControl(el);
                if(ctrl instanceof libUnicorn.ColumnsFilter) {
                    ctrl.setSelected({sign: '=', value: filterVal});
                } else {
                    //ctrl.setSelected(res);
                    ctrl.setSelected([filterVal]);
                    
                }
                el.classList.add("filtered");
            }
        }

        // убить заменить на querySelector

        function elByName(fldName) {
            for(var i = 0; i < elements.length; i++) {
                var el = elements[i];
                
               // var res = {};
                var  filterName = elements[i].name || elements[i].getAttribute("data-name");
                filterName = filterName.substr(2);
                if(filterName == fldName) return el;
            }
            return null;
        }
    }

    function makeFilter() {
        if(me.filerBeforeColumnMove) return me.filerBeforeColumnMove;
        // ПОМЕНЯТЬ selector на FILTER-CONTROL
        var elements = queryAll('.multiselect-control');
        var filter = {};
        for (var i = 0, l = elements.length; i < l; i++) {
            var el = elements[i];
            var  fld = el.name || el.getAttribute("data-name"); // for multiselect
            fld = fld.substr(2); // removes f_
            // ПЕРЕИМЕНОВАТЬ ВМЕСТЕ С ФУНКЦИЕЙ ? (подумать)
            var ctrl = libUnicorn.getControl(el);
            if (ctrl instanceof libUnicorn.MultiSelect) {
                var selected = ctrl.getSelected();  
                if (selected.length) {
                    filter[fld] = {sign: 'in', value: selected}; 
                }
            } else if (ctrl instanceof libUnicorn.ColumnsFilter) {
                var selected = ctrl.getSelected();
                if (selected.value && selected.value.length) {
                    filter[fld] = selected;
                }
            }
        }
        console.log("filter", filter);
        return filter;
    }

    // возврашает подходит ли row под выбарнные условия фильтра
    // + заполяет filterOptions
    // когда нарушено ровно одно правило
    function matchFilter(row) {
        var rule;
        var badFld = '';
        //var matched = true; //vik: unused
        var cntBad = 0; // кол-во ошибочных правил
        //var badFld = ''; //vik: повтор
        var e;
        for (var fld in me.filter) {
            var rule = me.filter[fld];
            if(fld.substr(fld.length-3,2) === '__'){fld=fld.substr(0,fld.length-3);}
            if (!row.hasOwnProperty(fld)) {
                throw "Unknown field " + fld;
            }
            var val = (row[fld] === null) ? '_null_' : row[fld];
			
            switch (rule.sign) {
                case 'isNull':
                    e = (val == '_null_');
                    break;
                case 'isNotNull':
                    e = (val != '_null_');
                    break;
				case 'isFavorite':
                    e = (val.indexOf("active") >= 0);
                    break;
                case 'isNotFavorite':
                    e = (val.indexOf("active") == -1);
                    break;
                case '>':
                    e = (val > rule.value);
                    break;
                case '>=':
                    e = (val >= rule.value);
                    break;
                case '<':
                    e = (val < rule.value);
                    break;
                case '<=':
                    e = (val <= rule.value);
                    break;
                case '=':
                    // == for compare string and numeric
                    // convert string to lowerCase???
                     e = (val == rule.value);
                     break;
                case '!=':
                    e = (val != rule.value);
                    break;
                case '~': 
                    //console.log(val);
                    val = val.toLowerCase();
                    e = (val.indexOf(rule.value.toLowerCase()) >= 0);
                    break;
                case '!~': 
                    //console.log(val);
                    val = val.toLowerCase();
                    e = (val.indexOf(rule.value.toLowerCase()) == -1);
                    break;
                case 'range':
                    var rul = rule.value.split('/');
                    
                    e = (val >= rul[0] && val <= rul[1]);
                    break;
                case 'date_range':
                    var rul = rule.value.split('/');
                    e = (val >= rul[0] && val <= rul[1]);
                    break;
                case 'in': // value - array of possible values
                    e=(rule.value.indexOf(val.toString())>=0);
                    break;                    
                default: //unknown rule
                    e = false;
            }
            if (!e) {
                cntBad++;
                badFld = fld;
            }
            // if (cntBad>1) break;
        }
        // если нарушено ровно одно правило то сохраняем значение поля для фильтра
        if (cntBad === 1 && me.filterOptions[badFld]) {
            var val =  (row[badFld] === null) ? '_null_' : row[badFld];
            me.filterOptions[badFld][val] = 1;
        }
        return cntBad === 0;
    }

    // сортирует массив data    
    function sort() {
        function cmp(a, b) {
            var res;
            if (a === null && b === null) {
                res = 0;
            } else if (a === null) {
                res = -1;
            } else if (b === null) {
                res = 1;
            } else if (a < b) {
                res = -1;
            } else if (a > b) {
                res = 1;
            } else {
                res = 0;
            }
            return res;
        }
        // order is not set - return
        if (!me.orderColumn)
            return;
        var fld = me.orderColumn;
        // sort by option list value
        if (me.columns[fld].options) {
            var opt = me.columns[fld].options;

            me.data.sort(function(a, b) {
                var c = opt.hasOwnProperty(a[fld]) ? opt[a[fld]] : a[fld];
                var d = opt.hasOwnProperty(b[fld]) ? opt[b[fld]] : b[fld];

                return cmp(c, d);
            });
            // sort by value
        } else if (me.columns[fld].type === 'varchar') {
            me.data.sort(function(a, b) {
                //текстовое поле состоящее из цифр сейчас  php отдает как число ((
                // '' + null == 'null' (((
                var c = a[fld] == null ? '' : '' + a[fld];
                var d = b[fld] == null ? '' : '' + b[fld];

                return cmp(c.toLowerCase(), d.toLowerCase());
            });
        } else {
            me.data.sort(function(a, b) {
                return cmp(a[fld], b[fld]);
            });
        }
    }
    
    function filterOptionsClear() {
        me.filterOptions = {};
        for (var fld in me.columns) {
            if (me.columns[fld].options || me.columns[fld].type=='date')
                me.filterOptions[fld] = {};
        }
    }

    function log(msg) {
        var ms = new Date().valueOf() - me.filterTimeStart.valueOf();
        try {
            console.log("time " + ms + "ms: " + msg);
        } catch (e) {
        }
    }

    //refresh, clearFilter
    function filterProcess() {
        setFlds();

        filterOptionsClear();

        // сортируем все потом фильтруем
        // возможно стоит сортировать отфильтрованный массив
        if (me.oldOrderColumn !== me.orderColumn) {
            sort();
            me.oldOrderColumn = me.orderColumn;
            log("sort complete");
        }

        var row;
        me.filter = makeFilter();

        me.filteredData = [];
        if (me.orderColumnDesc === 0) {
            // asc order
            for (var i = 0, l = me.data.length; i < l; i++) {
                row = me.data[i];
                if (matchFilter(row))
                    me.filteredData.push(row);
            }
        } else {
            // desc order
            for (var i = me.data.length - 1; i >= 0; i--) {
                row = me.data[i];
                if (matchFilter(row))
                    me.filteredData.push(row);
            }
        }
        if (me.TextSearch && me.TextSearch.value) {
            filterSearchText (me.TextSearch.value);
            // setJobs выполнит filterSearchText
        } else {
            me.filteredDataSearched = me.filteredData;
            if(me.filteredDataSearched.length < me.data.length) {
                showInfoMessage('Фильтр применен');
            }
            setJobs();
        }
        
    }


    //callback test for dropDown sticker (plugin)
    function dsChange(evt) {
        evt = evt || window.event;
        var target = evt.target || evt.srcElement;
        evt.stopPropagation();
        return false;
    }

    function onPageChange(nPage) {
        displayRows(nPage);
        me.pageNow = nPage;
    }

    function setJobs() {
        me.pageNow = 0;
        var paginationBoxes = me.wrap.querySelectorAll('.pagination-wraper');
        var nPages = Math.ceil(me.filteredDataSearched.length / me.rowsPerPage);
        var tablePosition = me.wrap.querySelector('.td-table').offsetLeft;
        me.jobs = [];
        me.jobs.push(setTimeout(displayFoundInfo, 0));
        me.jobs.push(setTimeout(displayRows, 0));
        me.jobs.push(setTimeout(setItogo, 0));
    //    if (me.DomFilterRow)
        me.jobs.push(setTimeout(filterOptionsSetAll, 0));
        me.jobs.push(setTimeout(fix, 0));
        var paginator = new Paginator(paginationBoxes, nPages, onPageChange, tablePosition);
     //   me.jobs.push(setTimeout(paginator, 0));
        console.log('jobs seted');

    }

    function fix() {
        FixPos.reinit();
        /*
         if (FixPos && FixPos.hasScrollBar!=MyFn.hasScrollBar()) {
         FixPos.reinit();
         }
         */
    }
    
    function filterSearchText(search, colName, contain) {
        me.filterTimeStart = new Date();
        me.SearchText = search.toLowerCase();
        if (me.SearchTextJob) {
            clearTimeout(me.SearchTextJob);
        }
        me.filteredDataSearched = [];
        me.searchTextIndex = 0;
        _filterSearchTextPart(colName, contain);
    }

    function _filterSearchTextPart(colName, contain) {
        //Можно добавить переменную colNmae для поиска по соответствующей колонке

        for (var i = me.searchTextIndex, l = me.filteredData.length; i < l && i < me.searchTextIndex + 5000; i++) {
            var row = me.filteredData[i];
            for (var fld in row) {
                (colName !== undefined) ? fld = colName : fld = fld;
                var val = row[fld];
                if (me.columns[fld] && me.columns[fld].options) {
                    var val = me.columns[fld].options[val];
                }
                val = ('' + val).toLowerCase();

                //Фильтр содержит/не содержит
                if(contain === "true") {
                    contain = true;
                } else if(contain === "false") {
                    contain = false;
                }
                if (!contain && val.indexOf(me.SearchText) >= 0) {
                    me.filteredDataSearched.push(row);
                    break;
                } else if (contain && val.indexOf(me.SearchText) == -1) {
                    me.filteredDataSearched.push(row);
                    break;
                }
            }
        }

        if (i < me.filteredData.length) {
            // Мы еще не закончили
            // Запускаем по таймеру на следующие 5000 строк
            me.searchTextIndex = i;
            me.SearchTextJob = setTimeout(_filterSearchTextPart, 0);
        } else {
            // everything DONE!
            // display Rows
            setJobs();
        }
    }

    function displayFoundInfo() {
        var panel = me.wrap.querySelector('span.td-info-found');
        if (panel)
            panel.innerHTML = 'Найдено <b>' + me.filteredDataSearched.length + '</b> из <b>' + me.data.length + '</b>';
        log("Filtered. Searched =" + me.data.length + " found=" + me.filteredDataSearched.length);
    }

    function toggleFullCellText(e) {
        var target = e.target || window.srcElement;
        target.innerHTML = (me.showFullText) ? 'Показать полные тексты' : 'Скрыть полные тексты';
        me.showFullText = !me.showFullText;
        if(me.DomTbody) {
            var cells = me.DomTbody.querySelectorAll('td');
            for(var i = 0; i < cells.length; i++) {
                cells[i].classList.toggle('small-txt');
            }
        }
    }

    //Вывод отфильтрованного списка
    function displayRows(pageNum) {
        var page = pageNum || 0;
        me.DomTbody.innerHTML = '';
        me.rowsPerPage = parseInt(me.rowsPerPage);
        for (var i = page * me.rowsPerPage, l = me.filteredDataSearched.length; (me.rowsPerPage === '_all_' || i < (page * me.rowsPerPage) + me.rowsPerPage) && i < l; i++) {
            var row = me.filteredDataSearched[i];
            var domRow = me.DomTbody.insertRow(-1);

            if (row && row.updated) {
                MyFn.addClass(domRow, 'updated');
            }
            if (me.showCheckBox) {
                var cellCheck = domRow.insertCell(-1);
                MyFn.addClass(cellCheck,'checkbox');
                if (me.checkedRows.indexOf(row)>=0) {
                    cellCheck.innerHTML = '<span class="checkbox checked"></span>';   
                } else {
                    cellCheck.innerHTML = '<span class="checkbox"></span>';   
                }
            }
            
            for (var j = 0, length = me.flds.length; j < length; j++) {
                var domCell = domRow.insertCell(-1);
                var fld = me.flds[j];
                var col = me.columns[fld];
                var val = row[fld];
                if (col) {
                    if (col.options && col.options[val]) {
                        val = col.options[val];
                    } else if (col.has_sum) {
                        val = (val === null || isNaN(val)) ? val : nf(val, 2);
                    } else if (col.type === 'date') {
                        val = df(val);
                    }
                   
                    if (col['js_format_func']) {
                        val = window[col.js_format_func](val);
                    }
                    if (col.align) {
                        domCell.style.textAlign = col.align;
                    }
                    
                    if (col.hidden) {
                        MyFn.addClass(domCell,'hidden');                     
                    } else {
                        if(val != null && val.length > 20 && fld!='files' && fld!='favorites' && !me.showFullText) {
                            domCell.title = val;
                            domCell.classList.add('small-txt');
                        }
						if (col.url) {
							if (val !== null) {
								val = '<a target="_blank" href="' + url(col.url, row) + '">' + val + '</a>';
							}
						}						
                        domCell.innerHTML = val;
                    }
                }
            }

        }

        if (me.showCheckBox) checkAllSetClass();
        log("Display rows");
    }

    function url(href, row) {
        var reg = /\$row\['(\w+)'\]/;
        while (reg.test(href)) {
            href = href.replace(reg, function(susbtr, w1) {
                return row[w1];
            });
        }
        return href;
    }

    function filterOptionsSetAll() {
        // обходи поля в порядке их вывода в таблице 

        for (var i = 0, l = me.flds.length; i < l; i++) {
            var fld = me.flds[i];
            // если есть опции    

            if (me.columns[fld].options || me.columns[fld].type=='date') {
                // можно вынести в функцию на таймер
                for (var j = 0; j < me.filteredData.length; j++) {
                    var row = me.filteredData[j];
                    var val = (row[fld] === null) ? '_null_' : row[fld];
                    me.filterOptions[fld][val] = 1;
                }

                //run filterOptionsSetTree
                if(me.columns[fld].tree_options) {
                    filterOptionsSetTree(fld);
                } else if (me.columns[fld].options) {
                    filterOptionSetOne(fld);
                } else if (me.columns[fld].type=='date') {
                    filterOptionsSetDate(fld);
                }
            }
        }

        log("Filter options set");
    }

    function makeHierFromDates(dates) {
        var parents_array = [];
        var year_month = Object.create(null);
        var year = Object.create(null);
        var isset_null = false

        for(var i=0; i<dates.length; i++) {
            var date = dates[i];
            if(date == '_null_') {
                isset_null = true;
                continue;
            }
            var dt = date.split('-');
            var element = Object.create(null);
            element.id = date;
            element.name = date;
            var parent_id = dt[0] + '-' +dt[1];
            if (!year_month[parent_id]) {
                addYearMonth(parent_id);
                year_month[parent_id] = true;
            }
            element.parent_id = parent_id;
            parents_array.push(element);
        }

        //Добавляем пустые в конец списка
        if(isset_null) parents_array.push({"id":"_null_", "name": " (Пустые)", parent_id: null});

        function addYearMonth(val) {
            var element = Object.create(null);
            element.id = val;
            element.name = val;             
            var y = val.substr(0,4);
            element.parent_id = y;
            parents_array.push(element);
            if (!year[y]) {
                var element = Object.create(null);
                element.id = y;
                element.name = y;             
                element.parent_id = null;
                year[y] = true;
                parents_array.push(element);
            }
        }

        return parents_array;
    }

    //only for treePlugin
    function filterOptionsSetTree(fld) {
        var select_by_fld = _getFilterControl(fld);
        var select = libUnicorn.getControl(select_by_fld);  
        var options = me.columns[fld].tree_options;
        if(select) select.setOptions(options);
    }

    function filterOptionsSetDate(fld) {
        var select_by_fld = _getFilterControl(fld); 
        var select = libUnicorn.getControl(select_by_fld);  
        var dates_sort=[];
        for(var key in me.filterOptions[fld]) {
            dates_sort.push(key);
        }
        var h = makeHierFromDates(dates_sort);
        select.setOptions(h);
    }

    function _getFilterControl(fld) {
        return  me.wrap.querySelector('TR.moveable span.multiselect-control[data-name="f_' + fld + '"]');
    }


    function filterOptionSetOne(fld) {
        var select_by_fld = _getFilterControl(fld); 
        var select = libUnicorn.getControl(select_by_fld);  
        var optionsOrdered = me.columns[fld].orderedOptions;
        var options = me.columns[fld].options;
        var o = [];
        var distinct = me.filterOptions[fld];
        for (var i = 0, l = optionsOrdered.length; i < l; i++) {
            var key = optionsOrdered[i].key;
            if (distinct[key]) {
                o.push({
                    id: key,
                    name: options[key],
                    //selected: selected.indexOf(key) >= 0
                });
                distinct[key] = 0; // clear it pushed
            }
        }
        // add values for which no options
        // something like reference errors
        for (var key in distinct) {
            if (distinct[key]) { // we haven't clear it in previous loop'
                if (key === '_null_') {
                    o.push({id: key, name: '(' + 'пустые' + ')'});
                } else {
                    o.push({id: key, name: '(' + key + ')'});
                }
            }
        }       
        select.setOptions(o);
    }

    function setItogo() {
        // init itogo move this block to other place
        var itogo = {};
        for (var col in me.columns) {
            if (me.columns[col].has_sum) {
                itogo[col] = 0;
            }
        }
        // reinit init
        for (var fld in itogo) {
            itogo[fld] = 0;
            for (var i = 0, l = me.filteredDataSearched.length; i < l; i++) {
                var row = me.filteredDataSearched[i];
                var val = row[fld];
                itogo[fld] += isNaN(val) ? 0 : val;
            }
        }
        // set html
        var itogoRow = me.DomItogoRow;
        for (var fld in itogo) {
            var i = me.flds.indexOf(fld);
            if (i >= 0) {
                if (me.showCheckBox) i++;
                itogoRow.cells[i].innerHTML = nf(itogo[fld], 2);
            }
        }
        log("Itogo set");
    }

    //number format
    function nf(n, dec) {
        var nStr = '' + n.toFixed(dec);
        var x = nStr.split('.');
        var x1 = x[0];
        var x2 = x.length > 1 ? ',' + x[1] : '';
        var rgx = /(\d+)(\d{3})/;
        while (rgx.test(x1)) {
            x1 = x1.replace(rgx, '$1 $2');
        }
        return x1 + x2;
    }
    // date format
    function df(d) {
        var rgx = /(\d{4})\-(\d{2})\-(\d{2})/;
        return d && d.replace ? d.replace(rgx, '$3.$2.$1') : '';
    }
    // reverse date format
    function rdf(d) {
        var rgx = /(\d{2})\.(\d{2})\.(\d{4})/;
        return d ? d.replace(rgx, '$3-$2-$1') : '';
    }
    // set select element html options 
    // move it to some other lib 

    // заполняем массив fields в  порядке как в HTML таблице
    function setFlds() {
        me.flds = [];
        var els = queryAll('table.td-table th span.td-order');
        for (var i = 0; i < els.length; i++) {
            me.flds.push(els[i].getAttribute('data-order-column'));
        }
    }
    
    function setColumnOrderedOptions(col) {
        var a = [];
        for (var key in col.options) {
			var val = (col.options[key]) ? col.options[key].toLowerCase() : '';
            a.push({key: key, value: val});
        }
        a.sort(function(a, b) {
            return a.value < b.value ? -1 : a.value > b.value ? 1 : 0;
        });
        col.orderedOptions = a;
    }
/*
    function _oldFilterApply() {
        me.filterTimeStart = new Date();
        if (!me.data) {
            var url = window.location.pathname + '?ajax_load&' + filterURL();
            Ajax({
                type: "POST",
                url: url,
                done: function(data) {
                    var ms = new Date().valueOf() - me.filterTimeStart.valueOf();
                    log("Response recieved ");
                    var v = JSON.parse(data);
                    log("JSON parsed");
                    me.data = v.data;
                    me.columns = v.columns;

                    filterProcess();
                }
            });
        } else {
            filterProcess();
        }        
    }
    */

    function filterApply() {
        me.filterTimeStart = new Date();
        filterProcess();
    }

    //Вызов информационнго сообщения
    var showTimeout;
	
	function removeInfoMessage() {
		var infoMessage = document.querySelector('.infoMessage');
		if(infoMessage && infoMessage.parentNode) infoMessage.parentNode.removeChild(infoMessage);
	}
	
    function showInfoMessage(msg) {		
        if(showTimeout !== null) {
            clearTimeout(showTimeout);
        }
		removeInfoMessage();
		var infoMessage = document.createElement('div');
		infoMessage.classList.add('infoMessage');
		infoMessage.innerHTML = msg;
		document.body.appendChild(infoMessage);
		infoMessage.style.display = 'block';
        showTimeout = setTimeout(function(){
            removeInfoMessage();
        }, 3000);
    }

    //Вызов модального окна
    function callingModal(event, title, content, sign, dataName, type) {
        event.preventDefault();
        var modal = document.createElement('div');
        modal.setAttribute('class', 'modal');
        modal.style.position = 'fixed';
        modal.innerHTML = '<div class="title">' + title + '</div><div class="modal-content">' + content + '</div><button class="modal-btn success">ОК</button><a href="#close" rel="modal:close" class="modal-btn">Отмена</a>';  
        document.body.appendChild(modal);
        $(modal).modal();
        modal.querySelector('input').focus();
        $(".datepicker" ).datepicker({
          showOn: "button",
          buttonImage: "../images/calendar.png",
          buttonImageOnly: true,
          dateFormat: 'dd.mm.yy',
          changeMonth: true,
          changeYear: true
        });

        $('.success').click(function(){
            var filter = modal.querySelector('input.filter-int'),
                rangeStart = modal.querySelector('input.range-start'),
                rangeEnd = modal.querySelector('input.range-end'),
                datepicker = modal.querySelector('input.datepicker'),
                datepickerRangeStart = modal.querySelector('input.dp-from'),
                datepickerRangeEnd = modal.querySelector('input.dp-to');
            if(datepicker != null && datepickerRangeStart == null) {
                me.multiselectFilter[dataName] = {sign: sign, value: rdf(datepicker.value), type: type};
            } else if(filter != null) {
                me.multiselectFilter[dataName] = {sign: sign, value: filter.value, type: type};
            } else if(rangeStart != null && rangeEnd != null) {
                me.multiselectFilter[dataName] = {sign: sign, value: rangeStart.value + '/' + rangeEnd.value, type: type};
            } else if(datepickerRangeStart != null && datepickerRangeEnd != null) {
                me.multiselectFilter[dataName] = {sign: sign, value: rdf(datepickerRangeStart.value) + '/' + rdf(datepickerRangeEnd.value), type: type};
            }

            filterApply();
            modal.parentNode.removeChild(modal);
            $.modal.close();
        });

        $(modal).keypress(function(e) {
            if(e.which == 13) {
                $('.success').trigger('click');
            }
        });
    }

    function showSelectedSubmenu(multiselectDiv, name, type) {
        if(me.filter[name]) {
            var item,
                that = multiselectDiv;
            if(type == "date") {
                item = that.submenu.querySelector('li[value="'+ me.filter[name].sign +'"]');
            } else {
                item = that.submenu.querySelector('li[data="'+ me.filter[name].sign +'"]');
            }
            if(item != null) {
                var i = item.querySelector('i');
                i.className = 'fa fa-check filter-check';
                item.innerHTML = item.innerHTML + ' <span>(' + me.filter[name].value + ')</span>';
            }
        }
    }

    var selectedOptions = {};

    function filterOnChange(e) {
        console.log(e);
        var control = libUnicorn.getControl(this);
        if(control.getSelected().length || (control.getSelected().value && control.getSelected().value.length)) this.classList.add('filtered');
        else this.classList.remove('filtered');
        filterApply();
    }
    /*
    //Обработчики на фильтр
    function filterHandler(e) {
        var control = this;
        var fld = control.getAttribute('data-name');
        fld = fld.slice(2);
        var obj = libUnicorn.getControl(control);
        if(obj instanceof libUnicorn.MultiSelect) {
            //Перегружаем функцию мультиселекта для работы в td.js
            obj._onOk = function() {
                if(this.getSelected().length) control.classList.add('filtered');
                else control.classList.remove('filtered');
                filterApply();
                this.dropDownClose();
            }
            obj._onDestroy = function() {
                console.log('DESTROY TD');
                // restore state
                this.setSelected(Object.create(null));
                this._onOk();
            }
        } else if(obj instanceof libUnicorn.ColumnsFilter) {
            obj._onOk = function() {
                if(this.selected.value.length) control.classList.add('filtered');
                else control.classList.remove('filtered');
                filterApply();
            }
            obj._onDestroy = function() {
                console.log('DESTROY');
                var last_vals = this.container.querySelectorAll('li span');
                for(var i = 0; i < last_vals.length; i++) {
                    last_vals[i].parentNode.removeChild(last_vals[i]);
                }
                // restore state
                this.selected = {sign: null, value: []};
                this._onCancel();
                this._onOk();
            }
        }
    }
    */

    function removeFiltersClick(e) {
        var control = queryAll('.multiselect-control');
        for(var i = 0; i < control.length; i++) {
            var obj = libUnicorn.getControl(control[i]);
            obj.select = Object.create(null);
            obj._onDestroy();
        }
        me.multiselectFilter = {};
        filterApply();
    }

    function filterURL() {
        var inp = queryAll('input.filter, select.filter');
        var filter = [];
        for (var i = 0; i < inp.length; i++) {
            if (inp[i].value) {
                filter.push(inp[i]);
            }
        }
        var form = queryAll('form[name="td-table-params"]');
        var url = MyFn.encodeURL(form) + '&' + MyFn.encodeURL(filter);
        return url;
    }

    function order(colName,direction) {
        me.orderColumn = colName;
        me.orderColumnDesc = direction;
        var span = me.fixed.querySelector("span[data-order-column='"+colName+"']");
        function cleanSortClass(node) {
            if (MyFn.hasClass(node,'sort-desc'))  MyFn.removeClass(node,'sort-desc');
            if (MyFn.hasClass(node,'sort-asc'))  MyFn.removeClass(node,'sort-asc');
        }
        if(me.prevOrdSpan != null && me.prevOrdSpan != span) {
            cleanSortClass(me.prevOrdSpan);
        }
        if (span) {
            cleanSortClass(span);
            MyFn.addClass(span, direction ? 'sort-desc'  :'sort-asc');
        }
        me.prevOrdSpan = span;
        filterApply();
        showInfoMessage('Список отсортирован');

    }

    function onOrdClick() {
        var el = this ;
        var orderColumn = el.getAttribute('data-order-column');
        var  direction = 1;
        if (me.orderColumn == orderColumn) {
            direction =  me.orderColumnDesc  ? 0 : 1;
        } 
        order (orderColumn,direction);
        // prevent propagate
        return false;
    }

    function trSingleClick(e) {
        e = e || window.evenet;
        var target = e.target || e.srcElement;
		var color_class = ['default', 'red', 'blue', 'green', 'orange'];
		if(target.tagName === 'SPAN' && target.classList.contains('favorite')) {
			var target_color = target.classList.item(1);
			var is_current_color = (target_color == color_class[me.favoritesColorKey]);
            //me.favoritesColorKey;
			var tr = target.parentNode.parentNode;
			var i = tr.sectionRowIndex;
			var row = me.filteredDataSearched[i];
			var table = (me.sqlTable.substr(0,2) == 'v_') ? me.sqlTable.substr(2) : me.sqlTable;
			var sended_color = (is_current_color) ? 0 : me.favoritesColorKey;
			$.ajax({
			  type: "GET",
			  url: "/modules/ajax.php",
			  data: "func=toggle_favorite&arg1=" + table + "&arg2=" + row.id + "&arg3=" + sended_color,
			  success: function(msg){
				msg = JSON.parse(msg);
				if(msg.success == 'OK') {
                    var current_color = target.classList.item(1);
                    target.classList.remove(current_color);		
					target.classList.add(color_class[sended_color]);
					//target.classList.toggle('active');
				}
				row.favorites = me.favoritesColorKey;
			  }
			});
			return;
		} 
        while (target) {
            if (target.tagName === 'TR')
                break;
            target = target.parentNode;
        }
        if (target.tagName === 'TR') {
            if(me.selectedTr != null && target != me.selectedTr) me.selectedTr.classList.remove('active');
            me.selectedTr = target;
            target.classList.add('active');
        } else {
            if(me.selectedTr != null) me.selectedTr.classList.remove('active');
            me.selectedTr = null;
        }
    }

    function bodyClick(e) {
        e = e || window.evenet;
        var target = e.target || e.srcElement;
        if (MyFn.hasClass(target,'checkbox') ) {
            checkBoxClick(target);
            return ;
        }
        while (target) {
            if (target.tagName === 'A' || target.tagName === 'TBODY') { // we have come up to the top or found a href
                target = null;
                break;
            }
            if (target.tagName === 'TR')
                break;
            target = target.parentNode;
        }
        if (target.tagName === 'TR') {
            var tr = target;
            var i = tr.sectionRowIndex;             // not rowIndex we can have header!!
            //Binding to the page now (pagination)
            i = (me.pageNow * me.rowsPerPage) + i;
            var row = me.filteredDataSearched[i];    

            if (me.onTrClick) {
                me.onTrClick.call(target, row);
            } else {
                trClick.call(target, e);
            }
        }
        //Cancel select text
        if(document.selection && document.selection.empty) {
            document.selection.empty();
        } else if(window.getSelection) {
            var sel = window.getSelection();
            sel.removeAllRanges();
        }
    }
    
    function checkBoxClick(target) {
        var span;
        if (target.tagName === 'SPAN') {
            span = target;  
        } else {
            span = target.querySelector('span.checkbox');
        }
        while (target) {
            if (target.tagName === 'TR') {
                var i = target.sectionRowIndex; 
                var row = me.filteredData[i];
                break;
            }
            target = target.parentNode;
        }
        if (MyFn.hasClass(span,'checked'))   {
            MyFn.removeClass(span,'checked');    
            var index = me.checkedRows.indexOf(row);
            if (index>=0) {
                me.checkedRows.splice(index,1);
            }
        } else {
            me.checkedRows.push(row);
            MyFn.addClass(span,'checked');
        }
        checkAllSetClass();        
    }

	function favClick(ev) {
		console.log("fav click");
	}
	
    // click on tr opens editBox    
    function trClick(ev) {
        me.trEdited = this;
        console.log(me.trEdited);
        MyFn.addClass(me.trEdited, 'edited');
        var box = me.wrap.querySelector('div.modal-editbox');
        if (box) {
            var i = me.trEdited.sectionRowIndex;
            console.log('section row index:');
            console.log(me.trEdited.sectionRowIndex);
            console.log('me.pageNow:');
            console.log(me.pageNow);
            i = (me.pageNow * me.rowsPerPage) + i;
            console.log('i before count:');
            console.log(i);
            me.rowEdited = me.filteredDataSearched[i];
                // me.rowEdited = me.filteredData[i];
            var data = {};
            for (var fld in me.rowEdited) {
                if (me.columns[fld] && me.columns[fld].type === 'date')
                    data[fld] = df(me.rowEdited[fld]);
                else
                    data[fld] = me.rowEdited[fld];
            }
            me.editBox.oncopy = onCopy;
            if (me.mayEdit) {
                me.editBox.onsave = onSaveSingleRow;
                me.editBox.edit(data, me.columns);
            } else {
                me.editBox.onsave = null;
                // if is log table and may restore!
                me.editBox.onrestore = onRestore;
                me.editBox.view(data, me.columns);
            }
            var to_reestr = box.querySelector('[name="to_reestr"]');
            if (to_reestr) {
                if(data['IS_REESTR_LOADED'] == 0) {
                    to_reestr.style.display = 'none';
                } else {
                    to_reestr.style.display = 'block';
                }
            }
        }
    }

    // EditBox for new record
    function addClick() {
        if (!me.mayEdit) return false;
        me.rowEdited = null;
        me.trEdited = null;
        me.editBox.onsave = onSaveSingleRow;
        me.editBox.add();
        return false;//may be link
    }
    
    // Merge record
    function recMerge() {
        if (!me.mayEdit) return false;
        me.rowEdited = null;
        me.trEdited = null;
        me.mergeBox.recmrg();
        return false;
    }

    function download(filename, text) {
        var URL = window.URL || window.webkitURL || window.mozURL || window.msURL;
        var blob = new Blob([text]);
        var url = URL.createObjectURL(blob);
        var pom = document.createElement('a');
        pom.setAttribute('href', url);
        pom.setAttribute('download', filename);
        if (document.createEvent) {
            var event = document.createEvent('MouseEvents');
            event.initEvent('click', true, true);
            pom.dispatchEvent(event);
        }
        else {
            pom.click();
        }
    }
    
    function toExcel() {
        var html = '<meta charset="utf-8"><style>.xls-text {mso-number-format:"\@";}</style>';
	html +="\r\n"+'<table border="1">';
        html += '<tr>';
        //th
        for(var key in me.columns) {
            html += '<th><b>' + me.columns[key].label + '</b></th>';
        }
        html += '</tr>';
        //rows
        for(var i = 0; i < me.filteredDataSearched.length; i++) {
            html += '<tr>';
            for (var j = 0, length = me.flds.length; j < length; j++) {            
                var fld = me.flds[j];
                var col = me.columns[fld];
                var val = me.filteredDataSearched[i][fld];
                if (val == null) val = '';
                if (col.options && col.options[val]) {
                    val = col.options[val];
                } else if (val+0 === val) {
                    val =  nf(val, 2);
                } else if (col.type === 'date') {
                    val = df(val);
                }
				var stringFormat = (me.columns[fld].type == 'varchar') ? 'class="xls-text"' : ''; 
                html += '<td '+stringFormat+'>' + val + '</td>';
            }
            html += '</tr>';
        }
        html += '</table>';
        download(me.sqlTable + '_' + new Date().getTime() + '.xls', html);
    }
}

TD.saveUserCols = '/td/save_user_cols.php';
TD.saveURL = '/td/save_js.php';
TD.restoreURL = '/td/restore_js.php';
TD.saveURLMany = '/td/save_js_many.php';
TD.mergeURL = '/td/merge_js.php';
TD.delURL = '/td/del_js.php';
TD.toExcelURL='/td/to_excel.php';


function selectColumnSetDescript(el) {
    var prefix = 'data-select-code-';
    var descript_id = el.getAttribute(prefix + "descript-id");
    var table = el.getAttribute(prefix + "table");
    var descript = document.getElementById(descript_id);
    MyFn.removeClass(descript, 'error_code');
    if(table != 'cod') {
        if (!el.value) {
            descript.innerHTML='Введите код для отображения расшифровки';
        } else {
            $.ajax({
                    type: "GET",

                    url: "/modules/ajax.php?func=get_description&arg1="+table +'&arg2='+el.value,
                    success: function(data){
                            descript.innerHTML=data;
                            if(!data) { 
                                descript.innerHTML = 'Указанный код не найден';
                                MyFn.addClass(descript, 'error_code');
                            }
                    }
            });
        }
    } else {
        return false;
    }
}