/*
uses MyFn NodeParent

*/
function ColResize(row) {
    'use strict';
	var me=this;
    me.row=row;
    me.tbl=MyFn.nodeParent(me.row,'TABLE');
	me.inResize=false;
	me.savedMove=null; // document.onmousemove
	me.savedUp=null; // document.onmouseup
	//var tbl,tbl2,row,col,col2;
	var ind; //index of cell to resize
	// tblWidth as start columnWidth
	me.tblWidth=null;
    me.colWidth=null;
    me.startX; //MouseX
	me.pos=[];
	me.afterResize=function(){};
	
	this.setHandler=function (){
		initPos();
		
        /*
		// �� wrap ����� ��� ������� (��������� � ������)
		var div=MyFn.nodeParent(tbl,'DIV','td-table-wrap');
		if (div) {
			var tbls=div.getElementsByTagName("TABLE");
			tbl2=(tbls.length>1) ? tbls[1] : null;
		}
        */
		me.row.onmousemove=mouseMove;
		me.row.onmousedown=mouseDown;
	}
	
	function mouseUp(event) {
		if (inResize) {
			inResize=false;
			document.body.style.cursor='';			
			//if (col2) col2.style.width=col.style.width;
			col=null;
			initPos();
			eventCancel(event);
			document.onmousemove=me.savedMove;
			document.onmouseup=me.savedUp;
			// callAfter
			me.afterResize(me.tbl);
		}
	};
    
	function mouseDown(event) {
		event=event || window.event;
		ind=cellIndex(event);
		if (ind!==null) {
			inResize=true;		
			me.savedMove = document.onmousemove;
			document.onmousemove = mouseMove;
			me.savedUp = document.onmouseup;
			document.onmouseup = mouseUp;
			document.body.style.cursor = 'col-resize';
			//row.style.cursor= 'col-resize';
			setCol(ind);
			me.colWidth=parseInt(me.col.style.width);
			me.tblWidth=parseInt(me.tbl.style.width);
			startX = event.pageX;			
			eventCancel(event);
		}
	}
	
	function mouseMove(event) {
		event=event || window.event;
		if (!inResize) {
			var i=cellIndex(event);
			me.row.style.cursor = (i!==null) ? 'col-resize' : '';
		} else {
			var addW=event.pageX-startX;
			var w=colWidth+addW
			me.col.style.width=colWidth+addW+'px';
			if (tblWidth) tbl.style.width=tblWidth+addW+'px';
			if (tbl2) tbl2.style.width=tblWidth+addW+'px';
			if (col2) col2.style.width=w+'px';
			eventCancel(event);		
		}
	}
	
	function eventCancel(evt) {	
		evt = evt || window.event;	
		if (evt.stopPropagation)    evt.stopPropagation();
		if (evt.cancelBubble!=null) evt.cancelBubble = true;
		if (evt.preventDefault) evt.preventDefault(); 
        else (evt.returnValue = false);  
	}

	function setCol(i) {
		var cols=tbl ? tbl.getElementsByTagName("COL") :null;
		me.col = cols.length>i ? cols[i] : null;
        /*
		if (tbl2) {
			col2=tbl2.getElementsByTagName("COL")[i];
		}
        */
	}

	function initPos() {
		pos=[];
		var length=me.row.cells.length
		var left=MyFn.offset(me.row).left;
		// for fixed position
		if (left<0) left=0;
		for (var i=0;i<length;i++) {
			var c=me.row.cells[i];
			var p=left+c.offsetLeft+c.offsetWidth;
			pos.push(p);
		}
	}

	// ������ ������ ��� resize
	function cellIndex(event) {
		event=event || window.event;
		var x = event.pageX;
		var ind=null;
		for (var i=0;i<pos.length;i++) {
			if (Math.abs(x-pos[i])<3) {
				ind=i;
			}
		}
		return ind;
	}

};