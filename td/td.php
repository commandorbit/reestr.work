<?php
// version 0.2 2013/11/18

Class TD { 
	/*	$columns	
		массив структуры
			$cols[$key]['label'] ;
			$cols[$key]['description'] ; //пока для title
			$cols[$key]['width'] ;
			$cols[$key]['format'] ;
			$cols[$key]['has_sum'] ;
	*/
	public $columns;
	// массив данных = массив rows
	public $data=array(); 
	public $primary_key=''; //ключевое поле если задано то tr id='id_' . $row[$key]
	// массив с текущими значениями фильтров встает в default в js
	public $filter;
    // фильтр данных нужен для составления описания и экспортов в Excel.
    public $dataFilter; 
	// массив параметров передается в url для получения данных
	// t - table
	// order - field_name
	// ord_desc = 1 or 0
	public $params=array();

	public $use_edit_box=true; // исользовать всплывающее окно для реадктирования
	public $use_buttons=true; // исользовать кнопки над таблицей
	public $use_pagination=true;
	public $fixed_headers=true; //фиксриованные заголовки
	public $may_edit=true;
    public $show_checkbox=true; // выводить колонку checkbox ов
    public $is_log_table = false; // признак прросмотр лога = отображать кнпоку восстановить
	public $has_filter=true;
	public $max_rows = 100; // максимум строк в таблице
	public static $option_all='_all_'; // для фильтров
	public static $option_none='_null_';
	// перекрыть для english
	protected static $text_str=array('filter'=>'Фильтр',
									 'clear_filter'=>'Очистить Фильтр',	
									 'add_rec'=>'Добавить запись',
									 'save_rec'=>'Сохранить запись',
									 'edit_rec'=>'Изменть запись',
									 'clear_filter'=>'Очистить фильтр',
									 'delete'=>'Удалить',
                                     'restore'=>'Восстановить',
									 );

	protected static $yes_no=array(1=>'Да',0=>'Нет');
	protected static $table_class='td-table'; // css class used by js!!!!!!!!!
	protected static $fixed_class='fixed';
	protected static $image_path='/td/images/';
	public $distinct=array() ; // набор уникальных значений
    protected $table=null;
    protected $wrap_id=null;//  'wrap-td-' . $table;
	private $sum=array(); // массив сумм для итого
    public $html_after = null;
    public $foreign_key = null;
    
    public function json_data() {
        echo json_encode($this->data);
    }
	public function out_all($empty=false) {
		$this->out_form_params();
		// расширенная форма фильтров
		//if ($this->has_filter) echo $this->out_filter_form();		
		if ($this->use_buttons) $this->out_buttons();

		if($this->use_pagination) $this->out_pagination();

		$this->out_table_head();
        if ($empty) {
            $this->out_table_data_empty();
		}
        else {
            $this->out_table_data();
		}
		if ($this->use_edit_box) $this->out_edit_box();
        if ($this->html_after) echo $this->html_after;
		// в excel
//		echo '<p style="text-align:center;"><a href="/td/to_excel.php?'.$_SERVER['QUERY_STRING'].'">В Excel</a></p>';
	}

	public function out_pagination() {
		echo '<div class="pagination-wraper"></div>';
	}

	public function out_js() {
        if ($this->table != 'dogovor_sostav' && $this->table != 'v_dogovor_sostav') $this->show_checkbox=false;

        echo '<div id="'.$this->wrap_id .'" class="td-table-wrap">'."\n";
        $this->out_all(true);
        echo '</div><!--td-table-wrap-->'."\n";
        ?>
        <script type="text/javascript">
        	console.log('start td');
            var params={};
            params.data=<?=json_encode($this->data)?>;
            params.columns=<?=json_encode($this->columns)?>;
            params.mayEdit=<?=json_encode($this->may_edit)?>;
            <? if ($this->foreign_key): ?>
                params.foreignKey=<?=json_encode($this->foreign_key)?>;
            <? endif ?>
            <? if ($this->primary_key): ?> 
                params.primaryKey=<?=json_encode($this->primary_key)?>;
            <? endif ?>
            params.dataFilter=<?=json_encode($this->dataFilter)?>;
            params.defaultFilter = <?=json_encode($this->filter)?>
            // is admin убрать
            params.showCheckBox=<?=json_encode($this->show_checkbox); ?>;

            //console.log(params);

            var TD<?=$this->table?> =  new TD(document.getElementById('<?=$this->wrap_id ?>'),params);
            
        </script>
        <?php
		// в excel
//		echo '<p style="text-align:center;"><a href="/td/to_excel.php?'.$_SERVER['QUERY_STRING'].'">В Excel</a></p>';
	}	
    
	function __get($id) { 
		throw new Exception("Trying to access undefined property '$id'."); 
	} 
    
	function __set($name,$id) { 
		throw new Exception("Trying to access undefined property '$id'."); 
	} 
	
    public function __construct($tname) {
        $this->table=$tname;
        $this->wrap_id= 'wrap-td-' . $tname;
    }

	public function control($col,$value=null,$use_default=false, $disabled = false) {
		$editable = '';
		$name = $col['name'] ;
		if (is_null($value) && $use_default) $value=$col['default_value'];
		if ($col['type']=='date')	{
			$value=static::format_date($value);
		} 
		if($col['editable'] == 0 || $name == 'id' || $disabled) $editable = ' disabled ';		
		$class = static::control_class($col);
		if ($col['is_select_code']) {
			$class .= ' select-code';
			$descript_id =  $name . '_descript' ;
			$table_name = $col['select_code_table'];
			$s= '<input class="handbook_code" onchange="selectColumnSetDescript(this)" style="text-align:right" type="text" data-select-code-table="'.$table_name.'" data-select-code-descript-id="'.$descript_id.'" ' .$class.' name="' . $name . '" value="'. $value . '"'  . '>';	
		//elseif ($col['edit_options'] !== false			
        } elseif ($col['edit_options']) {  // select
			$s= '<select name="' . $name . '"' . $class . $editable . ' data-default-value="'.$value.'">' . static::options($col['edit_options'],$value,false,$col['null']) .  '</select> ';
		} elseif ($col['type']=='double') { //Число
			$s= '<input style="text-align:right" type="text" ' .$class.' name="' . $name . '" value="'. $value . '"'  . $editable . '>';
		} else {
            //text
            if ($col['length'] > 100) {
                $s= '<textarea ' .$class.' name="' . $name . '"'.$editable.' >' .$value .'</textarea>';
            } else {
                $s= '<input type="text" ' . $editable . ' . ' .$class.' name="' . $name . '" data-default-value="'.$value.'" value="' .$value .'"'  . '>';
            }
		}
		return $s;
	}
    
	public static function col_formated($col,$value) {
		$s='';
		$value=htmlspecialchars($value);
	
		if ($col['options'] && isset($col['options'][$value])) 
			$value = $col['options'][$value] ;
		if ($col['format']=='N2')
			$value=$value ? number_format($value,2,',',' ') : '0,00';
		if (isset($col['url']) &&  $col['url'])
			$s .= '<a target="blank" href="'.str_replace('%val%',$value,$col['url']).'" >' .$value .'</a>';
		else
			$s .= $value;
		return $s;
	}

	// обязательно вызывать перед выводом
	// заполняет data, суммы, distinct для фильтров, 
	public function init($res=false,$fn=false) {
		$this->distinct=array();
		$this->sum=array();
		$this->data=array();
		// инициализируем массив сумм;
		foreach ($this->columns as $col) {
			if ($col['has_sum']) $this->sum[$col['name']]=0;
		}
		if ($res) while ($row=$fn($res)) $this->init_row($row);
		else foreach ($res as $row) $this->init_row($row);
	}
	
	protected function init_row($row) {
		$this->data[]=$row;
		foreach ($this->columns as $col) {
			$col_name=$col['name'];
			if ($col['has_sum']) $this->sum[$col_name]+=$row[$col_name];
//			$this->distinct[$col_name][$row[$col_name]]=1;
		}
	}

	// выводит форму для сортировка в том же диве
	protected function out_form_params() {
		echo '<form name="td-table-params" method="get" action="">';
		echo '<div>';
		foreach($this->params as $key=>$val) {
			if (is_array($val)) {
				foreach ($val as $v) {
					echo '<input type="hidden" name="'.$key.'[]" value="' .$v .'">';
				}
			} else {
				echo '<input type="hidden" name="'.$key.'" value="' .$val .'">';
			}
		}
		echo '</div>';
		echo '</form>';
	}
	
	protected function out_filter_form() {
		$s='';
		$s.= '<div style="text-align: right;width:53%;">';
		$s.= '<form name="filter_form">';
		foreach ($this->columns as $c) {
			if ($c['type']=='date') {
				$name= $c['name'] .'__s';
				$val= isset($this->filter[$name]) ? $this->filter[$name] : '';
				$s.= $c['label'] . ' с: <input type="text" class="filter date" name="f_'.$name.'" value="'.$val.'" style="width: 65px;">';
				$name= $c['name'] .'__po';
				$val= isset($this->filter[$name]) ? $this->filter[$name] : '';
				$s.= ' по <input type="text" class="filter date" name="f_'.$name.'" value="'.$val.'" style="width: 65px;">';
				$s.= '<br>';
			}
		}
		$s.= '</form>';
		$s.= '</div>'."\n";
		return $s;
	}
	
	protected function out_table_head() {
		$tw=$this->table_width();
		//		
		$t_class=static::$table_class;
		if ($this->fixed_headers) $t_class .= ' ' .static::$fixed_class;
		echo '<table class="'. $t_class . '" style="width:'.$tw. 'px">' ."\n";
		echo $this->out_cols();
//		echo '<tfoot>';		
//		echo '</tfoot>';
		echo '<thead>'."\n";
		echo '<tr class="itogo">' . $this->out_itogo() . '</tr>'."\n";
		//новая запись
		//if ($this->may_edit && $this->new_rec_above) $this->out_new_rec();
		// заголовки
		echo '<tr class="moveable">'.$this->out_th() . '</tr>' ."\n";
		/*// фильтр
		if ($this->has_filter) 
			echo '<tr class="filter"></tr>';
		*/
		echo '</thead>'."\n";
		if ($this->fixed_headers) {
			echo '</table>' ."\n";
		}
	}

	protected function out_table_data() {
		if ($this->fixed_headers) {
			$tw=$this->table_width();
			echo '<table  class="'.static::$table_class.'" style="width:'.$tw. 'px">';
			echo $this->out_cols();
		}
		echo '<tbody>';
		// данные
		$this->out_data();
		echo '</tbody>';
		echo '</table>'."\n";
	}
    
	protected function out_table_data_empty() {
		if ($this->fixed_headers) {
			$tw=$this->table_width();
			echo '<table  class="'.static::$table_class.'" style="width:'.$tw. 'px">';
			echo $this->out_cols();
		}
		echo '<tbody>';
		// данные
		//$this->out_data();
		echo '</tbody>';
		echo '</table>'."\n";
		$this->out_pagination();
	}
	
	protected static function format_date($d) {
		//dd-mm-yyyy
		return substr($d,8,2) .'.' . substr($d,5,2) . '.' . substr($d,0,4);
	}
	protected static function control_class($col) {
		if ($col['type']=='date') 
			$class = ($col['null']==0) ? 'class="date not_null"' : 'class="date"';
		else
			$class = ($col['null']==0) ? ' class="not_null"' : '';
		return $class;
	}
	
	
	protected function out_edit_box() {
		require dirname(__FILE__) .'/editbox.php';
	}
	protected function out_buttons() {
		require dirname(__FILE__) .'/buttons.php';
	}

	protected function out_edit_box_tbody() {
		foreach ($this->columns as $field=>$col) {

			if($this->table == 'v_dogovor_sostav' && $field == 'kosgu') continue;
			if($this->table == 'v_dogovor_sostav' && $field == 'sum_ost') continue;

			echo '<tr><th>'.$col['label'].'</th><td class="checkbox" style="visibility:collapse;"></td><td>'.$this->control($col, null, true).'</td></tr>'."\n";
	
			//Дурацкий костыль, который не нужен, если разобраться с is_select_code
			//Может is_select_code вообще не нужен?
			$exclude_tables = array('vid_rashodov');
			// добавляем description
			if  (!in_array($this->table, $exclude_tables) && isset($col['is_select_code']) && $col['is_select_code']) {
				$span_id = $col['name'] . '_descript';

				echo '<tr><th>Расшифровка</th><td class="checkbox" style="visibility:collapse;"></td><td><span id="'.$span_id.'">'.'</span></td></tr>'."\n";
			}	
		}
	}

	private function out_th() {
		$s='';
		$name=(isset($this->params['order'])) ? $this->params['order'] : '';
		$name_arrow = ($this->params['order_desc']==0) ? '<span class="td-order" style="color:#ff0000">&dArr;</span>' : '<span style="color:#ff0000">&uArr;</span>';
        if ($this->show_checkbox) {           
			$s.= '<th class="check-all"><span class="checkbox"></span><span class="td-drop-down-arrow"></span>';
            $s .= '<ul class="check-all-action" style="display:none"><li class="edit">Редактировать</li><li class="del">Удалить</li></ul></th>';
        }
		foreach ($this->columns as $col) {
			$arrow = ($col['name']==$name) ? $name_arrow : '';
			$col_name=$col['name'];
            $v=isset($this->filter[$col_name]) ? $this->filter[$col_name] : '';
			//$select = $this->col_filter($col,$v);	
			$favorite_color = ($col_name === 'favorites') ? '<span class="favorite blue change_color"></span>' : '';
            $multi_select = '<span class="multiselect-control" data-name="'.'f_'.$col_name .'" data-constant="true">';
			$s.= '<th  title="'.$col['description'].'" draggable="true"><span  class="td-order"  
			data-order-column="'.$col['name'].'">'.$col['label'].'</span><div class="filter-item-dropdown">'. $favorite_color . $multi_select .'</div></th>';
		}

		return $s;
	}
	
	// 

	private function out_row_filter() {
		$s='';
        if ($this->show_checkbox) {           
			$s.= '<td style="overflow:hidden;"></td>';
        }        
		foreach ($this->columns as $col) {
			$col_name=$col['name'];
            $v=isset($this->filter[$col_name]) ? $this->filter[$col_name] : '';

			$ss= $this->col_filter($col,$v);
			$s .= '<td height="20px">'. $ss .'</td>';
            /*
           	if ($col_name ==  'id') 
				$ss='<input type="image" src="'.static::$image_path.'add.gif" id="but_add" alt="add" title="Добавить запись" >';
            */
		}

		return $s;
	}
	
	private function col_filter($col,$value) {		
		$name = 'f_' . $col['name'];	
        if ($col['options']) {
			// фильтруем на только имеющееся в данных значения
			$options=$col['options'];
            /*
			foreach ($options as $key=>$dummy) {
				if (!isset($this->distinct[$col['name']][$key]))
					unset($options[$key]);
			}
            // пополняем неизвестынми значениями
            // $this->distinct пустой если не было записей
            if ($this->distinct) {
                 $hasEmpty=false;
                foreach ($this->distinct[$col['name']] as $key=>$dummy) {
                    if (empty($key)) $hasEmpty=true;
                    elseif (!isset($options[$key]))
                        $options[$key]=$key;
                }
                if ($hasEmpty)
                    $options[static::$option_none]='(Пустые)';
            }
            */
			$s= '<select class="filter" name="' . $name . '" >' . static::options($options,$value,true) . '</select>';
		} else {
			$class=($col['type']=='date')  ? ' date"' :'"';
			if (is_array($value)) $value='';
			$s= '<select class="filter" name="' . $name . '" value="' . $value . '"></select>';
			//$s= '<input class="filter' . $class . ' type="search"  name="' . $name . '" value="' . $value . '">';
		}
		return $s;
	}

	protected function out_cols() {
		$s='';
        if ($this->show_checkbox) {
            $width= 50 .'px' ;
			$s.= "<col style=\"width:$width;\"  >";
        }
		foreach ($this->columns as $col) {
			$width= $col['width'] .'px' ;
			$s.= "<col style=\"width:$width;\"  >";
		}
		return $s;
	}
	
	protected function out_itogo() {
		$s='';
        if ($this->show_checkbox) {
            $s.= '<td style="border:none;"></td>';
        }
		foreach ($this->columns as $col) {
			$name=$col['name'];
            $sum= isset($this->sum[$name]) ? $this->sum[$name] : 0;
			if ($col['has_sum']) 
				$s.= '<td style="background: #fff; text-align:' . $col['align'] . '">' . $this->col_formated($col,$sum) .'</td>';
			else
				$s.= '<td style="border:none;"></td>';
		}
		return $s;
	}
    
	public function out_data() {
		$i=0;
		foreach ($this->data as $key=>$row) {	
			$i++;
			$s='';
			if ($i>=$this->max_rows) {
             //   echo '</table><table style="display:none;"><tbody>';
			//	break; //to many rows I prefer to break
				$s.=' style="display:none;"';
			}
			echo "<tr $s>". $this->out_data_row($row)."</tr>\n";			
		}
	}

	protected function out_data_row($row) {
		$s='';
		foreach ($this->columns as $col) {			
			$col_name = $col['name'];		
			$value = $row[$col_name];	
			if (isset($col['format_function'])) {
				$fn=$col['format_function'];
				$v=$fn($col_name,$row);
			} else {
				if ($col['type']=='date')  $value=$this->format_date($value); 
				$v=$this->col_formated($col,$value,$this->may_edit);
			}
			$s.= '<td style="text-align:' . $col['align'] . '">'. $v  .'</td>';
		}
		return $s;
	}

	// converts array to select options list
	public static function options($a,$value,$show_all=false,$show_empty=false) {
		$s="";
		if ($show_all)            
			$s.='<option value="'. static::$option_all. '">Все</option>';
        if ($show_empty)    
			$s.='<option value="'.static::$option_none.'">(Пусто)</option>';

		foreach ($a as $key=>$val) {
			$sel=$key==$value ? 'selected' :'';
			$s.= '<option ' . $sel . ' value="' . $key . '">' . $val. "</option>\n";
		}
		return $s;
	}			
	
	protected function table_width() {
		$w=0;
		foreach ($this->columns as $col) {
			$w+=$col['width'];
		}
        if ($this->show_checkbox) $w+=50;
		return $w;
    }
}

?>