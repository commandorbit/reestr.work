<?php
require $_SERVER['DOCUMENT_ROOT'].'/init.php';
require $_SERVER['DOCUMENT_ROOT'].'/modules/save.php';

if (!isset($_GET['t'])) exit();
$table = sql_escape($_GET['t']);
$key=sql_table_key($table);
$p=array();
foreach ($_POST as $fld=>$val) {
    if ($fld<>$key) $p[$fld]=$val;
}
if (empty($key)) {
    echo json_encode(array('status'=>'ERROR','msg'=>"no primary key for table $table"));
    die();
} elseif (! isset($_POST[$key]) ) {
    echo json_encode(array('status'=>'ERROR','msg'=>'no primary key in POST'));
    die();
}
$rows=array();    
foreach ($_POST[$key] as $val) {
    $p[$key]=$val;
    $id=save($table,$p);
    $rows = $DB::query($table,array('id'=>$id));
}    
// преобразование типов!!!!!!!!!!!!!!
echo json_encode(array('status'=>'OK','rows'=>$rows));
exit();