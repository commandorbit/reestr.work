<!--included by td.pdp-->
<div id="overlay-level" class="modal-overlay"></div>

<div class="modal-editbox" syle="display:none;position:absolute;">

	<div class="title"></div>
	<div class="body"  style="overflow:auto;max-height:600px;width: 500px;">
		<form id="editbox-form" class="editbox-form">
		<table   style="width: 500px;margin:0 auto;" >
			<col style="width:150px"><col style="width:0px"><col style="width:355px">
		<tbody>
			<? $this->out_edit_box_tbody();?>
		</tbody>
		</table>
		</form>
	</div>
	<div class="body">
		<button type="button"  name="cancel">Отмена</button>	

		<?php 
			$disable_save_delete = array('v_sverka_kosgu_if', 'doc_lim_change', 'v_diff_fpa_fs', 'v_diff_ds_zs'); 
			if(in_array($this->table, $disable_save_delete)) {
				$this->may_edit = 0;
			}
		?>

		<?php if($this->table == 'virt_dogovor_sostav_rep'): ?>
			<button type="button"  name="go_to_dogovor" onclick="goToDogovor()">Перейти к договору</button>

			<script>
				function goToDogovor() {
					var dogovor_id = document.querySelector('.modal-editbox input[name="id"]').value;
					dogovor_id = parseInt(dogovor_id);
					window.open('/dogovor.php?id=' + dogovor_id, '_blank');
				}
			</script>
		<?php endif; ?>

			<?php if(may_dogovor() == 0 || !$this->may_edit): ?>
				<div style="display: none;">
					<button type="button"  name="save">Сохранить</button>
					<button type="button" name="delete"><img src="<?=static::$image_path?>del.png" alt="" style="vertical-align:middle; display: none;">&nbsp;<?=static::$text_str['delete']?>
					</button>
				</div>
			<?php else: ?>
				<button type="button"  name="save">Сохранить</button>
				<button type="button" name="delete"><img src="<?=static::$image_path?>del.png" alt="" style="vertical-align:middle">&nbsp;<?=static::$text_str['delete']?>
				</button>
			<?php endif; ?>

			<?php $copy_tables = array('v_dogovor_sostav', 'col', 'tab','plan_dohod','fin_source'); ?>
			
			<?php if($this->table == 'plat' && !user_has_zfo()): ?>
				<button type="button"  name="copy">Копировать</button>
			<?php endif; ?>
			
			<?php if(in_array($this->table, $copy_tables) && $this->may_edit <> 0): ?>
				<button type="button"  name="copy">Копировать</button>
			<?php endif; ?>

		<? if ($this->table == 'dogovor_sostav_restore') : ?>
            <button type="button" name="restore"><?=static::$text_str['restore']?> с новым ID</button>
            <script>
            	TD.restoreURL = '/td/restore_sostav_js.php';
            </script>
        <? endif ?>

        <? if (is_admin() && $this->is_log_table) : ?>
            <button type="button" name="restore"><?=static::$text_str['restore']?></button>
        <? endif ?>

		<? if ($php=sql_get_value('php','tab',"`table`='".$this->table."'")) :?>
            <button type="button" name="to_document" data-php="<?=$php?>">Документ</button>
		<? endif ?>
	</div>
</div>
<div class="modal-mergebox">
	<div class="title"></div>
	<div class="body" style="width: 420px;">
		<form id="mergebox-form" class="mergebox-form">
		<table style="width: 400px;margin:0 auto;">
			<colgroup><col style="width:150px"><col style="width:150px">
		</colgroup><tbody>
			<tr><th>Объединить id=</th><td><input type="text" class="not_null" name="id_from" value=""></td></tr>
			<tr><th>в id=</th><td><input type="text" class="not_null" name="id_to" value=""></td></tr>
		</tbody>
		</table>
		</form>
		<button type="button" name="cancel" title="Отмена">Отмена</button>
		<button type="button" name="merge" title="Запись с первым id удалится из таблицы">Ок</button>
	</div>
</div>