<?php

include ($_SERVER['DOCUMENT_ROOT'] . '/init.php');

if(!isset($_GET['fp_article_id'])) exit('Not valid require type!');

$fp_article_id = sql_escape($_GET['fp_article_id']);

$fp_object_type_id = sql_get_value('fp_object_type_id', 'fp_article', "id='$fp_article_id'");

if(is_null($fp_object_type_id)) {
	echo json_encode(array('status'=>'error'));
	exit();
}

$fp_objects = sql_rows("SELECT id, name FROM fp_object WHERE fp_object_type_id='$fp_object_type_id'");

if(count($fp_objects) > 0) {
	echo json_encode($fp_objects);
} else {
	echo json_encode(array('status'=>'error'));
	exit();
}

?>