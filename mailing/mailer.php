<?php
define ('MAIL_SEND_ADDR','no-reply@unecon.ru');

function email_validate($email) {
	if (!preg_match("/^([a-zA-Z0-9])+([a-zA-Z0-9\._-])*@([a-zA-Z0-9_-])+([a-zA-Z0-9\._-]+)+$/" , $email)) {
		return false;
	} else {
		return check_mx_exist($email);
	}
}
function check_mx_exist($email) {
	$hosts=array();
	list($user,$domain)=explode('@',$email);
	return getmxrr($domain,$hosts);
}

function mail_utf_encode($s) {
	return '=?utf-8?b?' . base64_encode($s) . '?=';
}

function send_mail($email,$subject,$body,$from = MAIL_SEND_ADDR) {
	$headers = 'MIME-Version: 1.0' . "\r\n" ;
	$headers .= "From: $from\r\n";
	$headers .= "Reply-To: $from\r\n";
	$headers .= 'Content-type: text/html; charset="utf-8"';
	/* Напомнить Грише добаить user www в trusted users в sendmail.conf */
	mail ($email,mail_utf_encode($subject),$body,$headers,"-f$from");
}

/*
function make_hash($email) {
	return (md5($email . rand(0,999)));
}

function send_password_link($abitur_user_kod) {
	$a = abitur_user_info($abitur_user_kod);
	$fio = make_fio_socr($a['fam'],$a['im'],$a['otc']);
	$href = 'http://' . $_SERVER['HTTP_HOST'] . '/get_pass.php?hash=' . urlencode($a['hash']);
	$body = <<<EOF
	Уважаемый(ая) <b>$fio</b>!<br><br>
	Для получения пароля для входа на сайт перейдите по ссылке:<br><a href="$href">$href</a><br><br>
	Если у вас возникли проблемы с переходом по ссылке, <br>
	попробуйте копировать <b>url ссылки</b> ("копировать ссылку") в адресную строку браузера.
EOF;
	send_mail($a['e_mail'],$_SERVER['HTTP_HOST'].' | Ваша ссылка для получения пароля',$body,MAIL_SEND_ADDR);
}

function send_password($abitur_user_kod,$password) {
	$a = abitur_user_info($abitur_user_kod);
	$fio = make_fio_socr($a['fam'],$a['im'],$a['otc']);
	$body = <<<EOF
	Уважаемый(ая) <b>$fio</b>!<br><br>
	Ваши данные для входа в личный кабинет: <br>
	<b>Логин: </b>${a['e_mail']}<br>
	<b>Пароль: </b>$password<br>
EOF;
	send_mail($a['e_mail'],$_SERVER['HTTP_HOST'].' | Доступы от сайта',$body,MAIL_SEND_ADDR);
}
*/

?>