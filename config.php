<?php


Class Config {
	// суффикс ref полей по умолчанию, пример: user_id
	const REF_SUF='_id';
	
	// Используется del_row для проверки references

	public static function references() {
		$r=array();
		/*
		$r= array(
			array('table'=>'smena','field'=>'start_user_id','ref_table'=>'user'),		
			array('table'=>'smena','field'=>'end_user_id','ref_table'=>'user'),
		);
		*/
		return $r;
	}
	
	public static function referenced_by($table) {
		$a=array();
		$r=self::references();
		foreach ($r as $v) {
			if ($v['ref_table']==$table) $a[]=$v;
		}
		return $a;
	}
	// используется функцией список запросов для выполнения после сохранения данных - псевода triggers
	public static function after_update($table) {
		$a = array (
				array('table'=>'bottle',
					  'query'=>'UPDATE prihod_sostav pr INNER JOIN bottle on pr.barcode=bottle.barcode SET pr.bottle_id=bottle.id , pr.good_id=bottle.good_id WHERE pr.bottle_id is null '
					  ),
				) ;
		$q=array();				
		foreach ($a as $v) {
			if ($v['table']==$table) $q[]=$v['query'];
		}
		return $q;
	}
	
	// базовая таблица на которой построе view
	// используется функцией Save для сохранения из нередактируемых view
	public static function view_table($view) {
		$a=array ('v_dogovor'=>'dogovor', 'v_zayav_sostav'=>'zayav_sostav', 'v_dogovor_sostav'=>'dogovor_sostav','v_zayav_sostav_bad_dogovor_sostav_id'=>'zayav_sostav');
		return  isset($a[$view]) ? $a[$view] : false;
	}
			
}