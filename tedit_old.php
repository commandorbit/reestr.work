<?php
require $_SERVER['DOCUMENT_ROOT'].'/init.php';

$time1=microtime(true);
$msg = '';

if (!isset($_GET['t'])) {
	header ('Location: /');
	exit;
}
$table = sql_escape($_GET['t']);
$files = array('v_dogovor','virt_dogovor','v_zayav');
$favorites = array('v_dogovor','virt_dogovor');
$links=array('v_dogovor'=>'dogovor','virt_dogovor'=>'dogovor','v_zayav'=>'zayav','virt_zayav_dog'=>'zayav');

$forbidden_tables = array('users_log', 'users_send_pass_log', 'user', 'role_permission');

if(in_array($table, $forbidden_tables) && !is_admin()) {
	header("Location: /index.php");
}

if ($table == 'v_stat_738') {
	$_GET['f_stat_738_ver_id'] = request_numeric_val('f_stat_738_ver_id',sql_get_value("MAX(id)","stat_738_ver"));
}

$TD = newTD($table);

foreach ($links as $key => $value) {
	if($key == $table) {
		$TD->use_edit_box = false;
	}
}

if (is_ro() || in_array($table,array('v_reestr', 'v_fk', 'dogovor_sostav_restore', 'v_dogovor_sostav')) ||
     substr($table,0,4)=='log_' || 
    ($table=='dogovor' && may_dogovor()<=1) ) {
// может редактировать договоры начиная с 2 значение 0 - вообще не должен видеть таблицу
    $TD->may_edit = false;
}

//Подключем tedit_fn с доп. функциями
require $_SERVER['DOCUMENT_ROOT'] . '/tedit_fn.php';
if ($table == 'v_dogovor' && user_has_zfo())  {
	$TD->data = vDogovorData($zfo, array_to_sql_string($fin_sources));
} elseif($table == 'v_dogovor_sostav') {
	$TD->may_edit = 0;
} elseif($table == 'v_reestr') {
	$TD->data = vReestrData();
} elseif($table == 'doc_lim_change' && user_has_zfo()) {
	$TD->data = vDocLimChangeData($zfo, array_to_sql_string($fin_sources));
	if(empty($TD->data)) $msg = '<div style="text-align: center; color: red;"><b>Не найдено документов изменения лимитов для Вашего ЦФО</b></div>'; 
} elseif ($table == 'plat') {
    $TD->columns['guid']['hidden'] = true;
    $TD->columns['bank_id']['hidden'] = true;
} elseif ($table=='v_stat_738') {
	$TD->columns['stat_738_ver_id']['hidden'] = true;
}

$title = get_header($table);
$description = get_table_description($table);

require 'template/header.php';
echo '<div class="header-tools"><h1>' /*. '<a title="Открыть в новом редакторе" href="' . str_replace('tedit_old.php', 'tedit_new.php', $_SERVER["REQUEST_URI"]) . '">'*/ . $title /* . '</a>'*/ . '</h1></div>';
echo '<p style="text-align: center; color: black;">'.$description.'</p>';
if(strlen($msg) > 0) {
	echo $msg;
	die();
}

if ($table=='temp_import_ish') {
   require 'tedit/temp_import_ish.php';
} elseif ($table=='temp_import_vh') {
	require 'tedit/temp_import_vh.php';
} elseif ($table=='v_reestr') {
    require 'tedit/v_reestr.php';
} elseif ($table=='v_zayav_sostav_bad_dogovor_sostav_id') {
    require 'tedit/v_zayav_sostav_bad_dogovor_sostav_id.php';
} elseif ($table=='virtual_738_diff') {
	require 'tedit/virtual_738_diff.php';
} elseif ($table=='asu_pfhd_load_log') {
	require 'tedit/asu_pfhd_load_log.php';
} elseif ($table=='virt_pfhd') {
	require 'tedit/virt_pfhd.php';
} elseif ($table=='v_stat_738') {
	require 'tedit/v_stat_738.php';
}

//files in table
if(in_array($table, $files) && isset($TD->columns['files'])) {
	foreach($TD->data as &$row) {
		if($row['files'] > 0) {
			$files_list = sql_rows("SELECT `id`,`filename`, `path` FROM dogovor_files WHERE dogovor_id='${row['id']}'");
			$html_files_list = html_files_list($files_list, $row['id']);
			$row['files'] = '<img src="/assets/images/attach.png" onclick="openCanvas('.$row['id'].')" class="attach-pic" title="Прикрепленные файлы">' . $html_files_list;
		} else {
			$row['files'] = null;
		}
	}
}

function html_files_list($list, $dogovor_id) {
	$html = '<div class="files_canvas" id="dog-'.$dogovor_id.'"><div class="close" onclick="openCanvas('.$dogovor_id.')"><i class="fa fa-times"></i></div><ul>';
	foreach ($list as $file) {
		$html .= '<li><a href="/download.php?id='.$dogovor_id.'&table=dogovor_files&id='.$file['id'].'">' . $file['filename'] . '</a></li>';
	}
	$html .= '</ul></div>';
	return $html;
}

//favorites
if(in_array($table, $favorites) && isset($TD->columns['favorites'])) {
	$TD->columns['favorites']['type'] = 'favorite';
	$native_table = (substr($table, 0, 2) == 'v_') ? substr($table, 2) : $table;
	$favorites_data = sql_rows("SELECT row_id, color_key FROM favorites WHERE user_id = '${_SESSION['user_id']}' and tab = '$native_table'");
	$user_favorites = array();
	foreach ($favorites_data as $favorite) {
		$user_favorites[$favorite['row_id']] = $favorite['color_key'];
	}
	foreach($TD->data as &$row) {
		$row_id = $row['id'];
		$row_value = (isset($user_favorites[$row_id])) ? $user_favorites[$row_id] : 0;
		$row['favorites'] = $row_value;
	}
}

//Status select
if($table == 'v_zfo_limit') {
	$statusSet = sql_rows("SELECT id, name FROM dogovor_status");
	require $_SERVER['DOCUMENT_ROOT'] . '/limits.php';

	$statuses = null;

	if(isset($_GET['status'])) {
		$status = $_GET['status'];
		$statuses = implode(',', $status);
	}

	$TD->data = vLimitsData($statuses, $zfo, array_to_sql_string($fin_sources));
} 

$TD->out_js();

require 'tedit/tedit_after.php';

?>

<?php if (in_array($table,array_keys($links) )) : ?>
	<script type="text/javascript">
        TD<?=$table?>.onTrClick = function(row) {
            var url="<?=$links[$table]?>.php?id="+row.id;
            var win=window.open(url, '_blank');
            win.focus();
        }
	</script>
<?php endif; ?>

<?php

if($table=='gz_44') {
	require 'tedit/gz_44.php';
}

//echo 'Время php ' .round((microtime(true)-$time1)*1000,3). ' ms';
require 'template/footer.php';

?>