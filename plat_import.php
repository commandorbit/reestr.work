<?php
date_default_timezone_set('Europe/Moscow');
$now = date_create();
$plat_filter=date_create();
date_sub($plat_filter, DateInterval::createFromDateString('1 month'));
require 'init.php';


sql_query("SET NAMES 'cp1251'");
sql_query("SET CHARACTER SET 'cp1251'");

if (isset($_GET['del_vh'])) {
	sql_query("delete from temp_import_vh");
}
if (isset($_GET['del_ish'])) {
	sql_query("delete from temp_import_ish");
}

if (isset($_GET['import'])){
	$sql="insert into plat(nom, d, plat_type_id, contragent_id,s, kvr,  zayav_id, bank_acc_id, booker_fin_source) 
						select nom, d, 3 as plat_type_id, contragent_id,s, vid_rashodov_nom, zayav_id, bank_acc_id, booker_fin_source from temp_import_ish ti ";
	sql_query($sql);
	sql_query("delete from temp_import_ish");
	header ('Location: tedit.php?t=plat');
	exit;
}
if (isset($_GET['import_dohod_plat'])){
	$sql="insert into dohod_plat(naznach, inn_poluch, inf_poluch, inn_plat, inf_plat, nom, d, plat_type_id, contragent_id,s, kosgu_id, bank_acc_id, fin_source_id, naznach_plat_id, file_d) select naznach, inn_poluch, inf_poluch, inn_plat,inf_plat, nom, d, 3 as plat_type_id, contragent_id,s, k.id, bank_acc_id, fin_source_id, naznach_plat_id, file_d from temp_import_vh ti left join kosgu k on cast(ti.kosgu as UNSIGNED)=k.kod";
	sql_query($sql);
	sql_query("delete from temp_import_vh");
	header ('Location: tedit.php?t=dohod_plat');
	exit;
}
$path = $_SERVER['DOCUMENT_ROOT']."/../reestr/DOCS/";
$filename=$path . "temp_plat.tff";
if (isset($_POST['uploaded'])){
	//echo 'загрузка файла \r';
	   if($_FILES["filename"]["size"] > 1024*10*1024)
	   {
		 echo ("Размер файла превышает десять мегабайт");
		 exit;
	   }
	   // Проверяем загружен ли файл
	   if (is_uploaded_file($_FILES["filename"]["tmp_name"])){
		move_uploaded_file($_FILES['filename']['tmp_name'], $filename);
	   } else {
		  echo("Ошибка загрузки файла");
		  exit;
	   }

}
//-------------------------------------парсинг файла-------------------------------	
	$columnarr=array(
	11=>array(1=>'vid_rashodov_nom'),//7=>'s',2=>'rs'
	42=>array(1=>'nom',2=>'d',3=>'s',8=>'inn_plat',10=>'inf_plat',15=>'inn_poluch',17=>'inf_poluch',25=>'naznach'));
	/*42=>array(1=>'nom',2=>'d',3=>'s',
		8=>'inn_plat',9=>'kpp_plat',10=>'inf_plat',11=>'schet_plat',12=>'bik_plat',13=>'bank_plat',
		15>'inn_poluch',16=>'kpp_poluch',17=>'inf_poluch',18=>'schet_poluch',19=>'bik_poluch',20=>'bank_poluch',	
		24=>'naznach'));
	*/	
	if ($_POST['plat_type']==1) $table="temp_import_ish";
	if ($_POST['plat_type']==2) $table="temp_import_vh";
	import_plat_file($filename,$columnarr,$table);
	
	if ($_POST['plat_type']==2) {//входящие платежки (доходные)
		$sql_upd_temp_import="delete from temp_import_vh where inn_poluch <> '7840483155'";
		$sql_upd_temp_import2="update temp_import_vh t inner join contragent c on t.inn_plat=c.inn set contragent_id=c.id";
		if (!empty($_POST['select_rs'])) {
			$sql_upd_temp_import3="update temp_import_vh t set bank_acc_id=".$_POST['select_rs'];
			$res=sql_query($sql_upd_temp_import3);}
		$res=sql_query($sql_upd_temp_import);
		$res=sql_query($sql_upd_temp_import2);
		
		header ('Location: tedit.php?t=temp_import_vh');
		exit;
	
	}
	else {// (=1)исходящие(расходные)
		$sql_upd_temp_import="delete from temp_import_ish where inn_plat <> '7840483155' and inn_plat <> '7710539135'";
		$sql_upd_temp_import2="update temp_import_ish t inner join contragent c on t.inn_poluch=c.inn set contragent_id=c.id";
		if (!empty($_POST['select_rs'])) {
			$sql_upd_temp_import3="update temp_import_ish t set bank_acc_id=".$_POST['select_rs'];
			$res=sql_query($sql_upd_temp_import3);}
		$res=sql_query($sql_upd_temp_import);
		$res=sql_query($sql_upd_temp_import2);
		
		header ('Location: tedit.php?t=temp_import_ish');
		exit;
	}
	
	

function arr_cr_assoc_value($arr1,$arr2){
	$arr3=array();
	foreach ($arr1 as $key=>$value){
		if (isset($arr2[$key])){
			$arr3[$value]=sql_escape($arr2[$key]);
		}
	}
	return $arr3;
}
function import_plat_file($fullflname,$columnarr,$table){
	//echo $fullflname ;
	$farr1 =explode("\n",file_get_contents ($fullflname, "r"));
	$i=0;
	$farr2=array();
	foreach ($farr1 as $fkey=>$fvalue){
		$farr2[]=explode("|",$fvalue);
		$cnt=count($farr2[$i]);
		if ($farr2[$i][0]=="BD") $file_d=substr(sql_rus2date($farr2[$i][2]),1,strlen($farr2[$i][2]));
		if ($cnt==11){
			//print_r($farr2[$i-1]);
			$farr2[$i-1]=arr_cr_assoc_value($columnarr[42],$farr2[$i-1]);
			if (isset($farr2[$i-1]['d'])) $farr2[$i-1]['d']=substr(sql_rus2date($farr2[$i-1]['d']),1,strlen($farr2[$i-1]['d']));
			$farr2[$i]=arr_cr_assoc_value($columnarr[11],$farr2[$i]);
			$farr2[$i]['vid_rashodov_nom']=substr($farr2[$i]['vid_rashodov_nom'],-3);
			
			$sql="insert into $table (`".implode("`, `",array_merge(array_keys($farr2[$i-1]),array_keys($farr2[$i])))."`, `file_d`) values ('".implode("', '",array_merge($farr2[$i-1],$farr2[$i]))."', '".$file_d."')";
			$q=sql_query($sql);
			//echo $sql." <p>";
		}
		//echo $q;
		//print_r($farr2[$i-1]);
		//echo '\n<p>';
		$i++;
	}
	//echo " ".$i." <p>";

}
?>
