<?php

require_once $_SERVER['DOCUMENT_ROOT'] . '/init.php';

$dogovor_id = array_key_exists('id',$_GET) ? sql_escape($_GET['id']) : '';
$ref_contr_1c = array_key_exists('ref_contr_1c',$_GET) ? sql_escape($_GET['ref_contr_1c']) : '';

if (!$dogovor_id) {
	header('Location: index.php');
	exit();
}

$title = 'Привязка договоров из 1С';
require_once($_SERVER['DOCUMENT_ROOT'].'/template/header.php');
$id = $dogovor_id; // dim:: dogovor sidebar wants dogovor_id as id!
include 'template/left_sidebar/dogovor.php';

$dogovor_info = sql_get_array("dogovor","id='$dogovor_id'");
$ref_sql = "SELECT `Договоры_Ref` as `Ссылка` FROM dogovor_link_1c WHERE dogovor_id = '$dogovor_id'";
$onec_ref = sql_array(sql_query($ref_sql));

$contragent_id = $dogovor_info['contragent_id'];
$contragent_info = sql_get_array("contragent","id='$contragent_id'");

$inn = $contragent_info['inn'];
if (!$ref_contr_1c) {
	if ($contragent_info['Контрагенты_Ref']) {
		$ref_contr_1c = '0x' . bin2hex($contragent_info['Контрагенты_Ref']);
	} elseif (!is_null($onec_ref)) {
		$docs_query = OneC::select('Д.Контрагент')
					->from(['Справочник.Договоры','Д'])
					->where('Д.Ссылка', '=','0x'.bin2hex($onec_ref['Ссылка']));
		$res = $docs_query->execute();
		$row = mssql_fetch_array($res,MSSQL_ASSOC);
		$ref_contr_1c = '0x' . bin2hex($row['Контрагент']);
	}
} 

$refs = array();
if ($ref_contr_1c) {
	$refs[] = $ref_contr_1c;
}
if ($inn) {
	$query =  OneC::select('К.Ссылка','К.Наименование','К.ИНН')
			->from(['Справочник.Контрагенты','К'])
			->where('К.ИНН', '=', $inn);
	$res = $query->execute();
	while ($row = mssql_fetch_array($res,MSSQL_ASSOC)) {
		$refs[] = '0x' . bin2hex($row['Ссылка']);
	}
}

$doc_rows = array();
if($refs) {
	$docs_query = OneC::select('Д.Наименование', 'Д.Ссылка')
				->from(['Справочник.Договоры','Д'])
				->where('Д.Контрагент', 'in', $refs);
	$res = $docs_query->execute();
	while ($row = mssql_fetch_array($res,MSSQL_ASSOC)) {
		$row['Ссылка'] = bin2hex($row['Ссылка']);
		$doc_rows[] = $row;
	}
}


?>

<?php if (is_null($onec_ref)) : ?>
<script type="text/javascript">
	$(document).ready(function(){
		$('#show_contragent_input').click(function(){
			var wrapper = $('#contragent_input_wrapper');
			if (wrapper.is(':visible')) {
				wrapper.hide();
			} else {
				wrapper.show();
				$(this).hide();
			}
		});

		var contragent_select = document.getElementById('contragent_select');
		var contragent_options = [];
		for (var i=0; i < contragent_select.options.length; i++) {
			contragent_options.push({
				label : contragent_select.options[i].text,
				value : contragent_select.options[i].value
			});
		}

		$('#contragent_input').autocomplete({
			minLength : 4,
			source : contragent_options,
			select : function(event, ui) {
				var url = 'onec_dogovor.php?id=<?=$dogovor_id?>&ref_contr_1c='+ui.item.value;
				window.location.href = url;
				event.preventDefault();
			},
		});
	});
</script>
<?php endif ?>

<h1>Договор ID: <?=$dogovor_id?> (<?=$dogovor_info['dogovor_num']?> от <?=$dogovor_info['d']?>)</h1>
<div style="width: 900px; margin: 20px auto; text-align: center;">

<?php 
if ($refs) {
	echo '<b>Контрагент(ы): </b>';
	foreach ($refs as $ref) {
		echo '"'.contragent_by_ref($ref).'"; ';
	}
	if (is_null($onec_ref)) echo '<input type="button" id="show_contragent_input" value="Выбрать другого контрагента">';
	echo '<br><br>';
} else {
	echo '<br>';
	echo '<span style="font-weight: bold;">Контрагент не найден в 1с!</span><br>';
}
?>

<?php if (is_null($onec_ref)) : ?>
	<div style="margin: 20px;">
	<select id="contragent_select" style="display: none;">
	<?php 
		$options = contragent_options();
		foreach ($options as $key=>$value) {
			echo '<option value="'.$key.'">'.$value.'</option>'."\r\n";
		}
	?>
	</select>
	<?php 
		$input_style = ($refs) ? ' style="display: none;"' : '';
		$input_label = ($refs) ? 'Выберите другого контрагента' : 'Укажите контрагента';
	?>
	<label id="contragent_input_wrapper"<?=$input_style?>>
		<?=$input_label?>:
		<input id="contragent_input" style="width: 350px;">
		<br>Начните набирать наименование контрагента, а затем выберите его из списка.
		<br>После этого дождитесь перезагрузки страницы.
	</label>
	</div>
<?php endif ?>

<?php if(!is_null($onec_ref)): ?>
	<form method="POST" action="/onec_dogovor_tie.php">
	<?php 
		foreach($doc_rows as $row) {
			if($row['Ссылка'] == bin2hex($onec_ref['Ссылка'])) {
				echo "Договор в 1С: <b>" . $row['Наименование'] . "</b>";				
			}
		}
	?>
	<input type="hidden" name="dogovor_id" value="<?=$dogovor_id?>">
	<input type="submit" value="Удалить связь" name="unref">
<?php elseif ($doc_rows) : ?>
	<form method="POST" action="/onec_dogovor_tie.php">
	<label>
		 Найденные договора в 1С:
		<select name="symlink">
			<?php foreach($doc_rows as $row): ?>
				<option value="<?=$row['Ссылка']?>" <?= (!is_null($onec_ref) && $row['Ссылка'] == bin2hex($onec_ref['Ссылка'])) ? 'selected' : '' ?>><?=$row['Наименование']?></option>
			<?php endforeach; ?>
		</select>
	</label>
	<input type="hidden" name="dogovor_id" value="<?=$dogovor_id?>">
	<input type="submit" value="Привязать" name="ref">
	</form>
<?php elseif ($inn || $ref_contr_1c) : ?>
	Договоров данного контрагента в 1с НЕ НАЙДЕНО!
<?php endif; ?>

</div>
<?php
include 'template/footer.php';

function contragent_by_ref($ref) {
	$query =  OneC::select('К.Ссылка','К.Наименование')
			->from(['Справочник.Контрагенты','К'])
			->where('К.Ссылка', '=',$ref);
	$res = $query->execute();
	$row = mssql_fetch_array($res,MSSQL_ASSOC);
	return $row['Наименование'];
}

function contragent_options() {
	$query = OneC::select('К.Ссылка','К.Наименование')->from(['Справочник.Контрагенты','К'])
            ->where('_Marked','=','0x00')
			->order_by('Наименование');
	$res = $query->execute();
	$options = [];
	while ($row = mssql_fetch_array($res,MSSQL_ASSOC)) {
		$options['0x'.bin2hex($row['Ссылка'])] = $row['Наименование'];
	}
	return $options;
}

function dogovor_1c_oborot($dogovor_ref,$debet = 0) {
	$ACC = '502.11';
	$method = $debet ? 'oborot_debet' :'oborot_credit';
	$query = OneC::select(['Оборот.Period','Дата'],['КОСГУ._Code','КОСГУ'],['Оборот.Сумма','Сумма'])
		->from(['РегистрБухгалтерии.ЕПСБУ', 'Оборот'])
		->$method($ACC,'2015-01-01','2016-12-31',['КЭК','Принятые обязательства','Разделы Лицевых Счетов','Дополнительная бюджетная классификация'])
		->join('Справочник.КОСГУ','LEFT')->
			on(OneC::cast('Оборот.Субконто1','Справочник.КОСГУ'),'=','КОСГУ.Ссылка')
		->join(['Справочник.РазделыЛицевыхСчетов', 'Разделы'],'LEFT')->
			on(OneC::cast('Оборот.Субконто3','Справочник.РазделыЛицевыхСчетов'),'=','Разделы.Ссылка')
		->join(['Справочник.Договоры', 'Договоры'],'INNER')->
			on(OneC::cast('Оборот.Субконто2','Справочник.Договоры'),'=','Договоры.Ссылка')
		->where('Договоры.Ссылка','=', '0x' . bin2hex($dogovor_ref))
		->order_by('Оборот.Period')
		;

	//echo (string) $query;
	$rows = [];
	$res = $query->execute();
	while ($row =  mssql_fetch_array($res,MSSQL_ASSOC)) {
		$rows[] = $row;
	}
	return $rows;
}
require $_SERVER['DOCUMENT_ROOT']. '/modules/avdfunctions.php';
$rows = dogovor_1c_oborot($onec_ref['Ссылка'],0);
avd_print_assoc_array($rows,'502.11 Кредит');
$rows = dogovor_1c_oborot($onec_ref['Ссылка'],1);
avd_print_assoc_array($rows,'502.11 Дебет');