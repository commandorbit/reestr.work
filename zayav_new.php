<?php
require 'init.php';
require 'modules/save.php';
$date_now = date('d.m.Y');

// ds = dogovor_sostav
$ds = array_key_exists('ds',$_GET) ? $_GET['ds'] : null;

request_val('ds','');

if($ds && is_array($ds)) {
    $ds_first = $ds[0];

    $zayav = [];
    $zayav['id'] = null;
    $zayav['dogovor_id'] = $dogovor_id = sql_get_value('dogovor_id','dogovor_sostav',"id = '$ds_first'");
    $zayav['zfo_id'] = sql_get_value('zfo_id','dogovor',"id = '$dogovor_id'");
    $zayav['plat_type_id'] = 3;
    $zayav['d'] = $date_now;

    $contragent_id = sql_get_value('contragent_id','dogovor',"id='$dogovor_id'");
    $contragent_id = ($contragent_id) ? $contragent_id : '-1';
    $zayav['contragent_id'] = $contragent_id;
    $zayav['notes'] = '';
    $zayav['zayav_status_id'] = 1;
    
    $zayav_sostav = [];
    foreach ($ds as $ds_id) {
        $row = sql_get_array("dogovor_sostav","id='$ds_id'");
        $ost = ds_zayav_ost($ds_id);
		$row['dogovor_sostav_id'] = $ds_id;
        $row['sum'] = $row['amount'];

        if(!is_null($ost) && $ost > 0 && $ost < $row['amount']) {
            $row['sum'] = $ost;     
        } else if(!is_null($ost) && $ost <= 0) {
            continue;
        }
        $zayav_sostav[] = $row;
    }

    if ($zayav_sostav) {
        $zayav_id = save('zayav',$zayav);
        foreach ($zayav_sostav as $row) {
            $row['id'] = null;
            $row['zayav_id'] = $zayav_id;
            save('zayav_sostav',$row);
        }
        header("Location: /zayav.php?id=" . $zayav_id);
    } else {
        $_SESSION['USER_MESSAGE']['ERROR'] = 'Ошибка! Заявка не создана, т.к. превышен лимит суммы по строке договора!';
        header("Location: /dogovor.php?id=" . $dogovor_id);
    }
}