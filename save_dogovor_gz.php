<?php

include $_SERVER['DOCUMENT_ROOT'] . '/init.php';

check_request($_POST);

//$dgz_id = 0;

function check_request(&$request_flds) {
	$flds_human_names = array(
		'pos_num'=>'№ позиции',
		'okved'=>'Классификация по ОКВЭД2',
		'gz_placing_way'=>'Способ определения поставщика',
		'contract_max_price'=>'Ориентировочная начальная стоимость контракта',
		'zayav_guarantee'=>'Размер обеспечения заявки',
		'contract_guarantee'=>'Размер обеспечения исполнения контракта',
		'contract_execution_stages'=>'Сроки исполнения отдельных этапов контракта',
		'periodicity'=>'Переодичность поставки товаров (выполнения работ, оказания услуг)',
		'gz_preferences'=>'Преимущества',
		'okpd'=>'ОКПД2',
		'gz_okei'=>'ОКЕИ',
		'quantity'=>'Шт.',
		'sum'=>'Сумма'
	);
	$may_empty = array('gz_change_reason', 'change_reason_info');
	//$may_empty = array('change_reason_info');
	$empty_flds = array();
	foreach($request_flds as $key=>&$fld) {
		if(in_array($key, $may_empty)) continue;
		if((!is_array($fld) && trim($fld) == '') || (is_array($fld) && empty($fld))) {
			$empty_flds[$key] = true;
		} else if (is_array($fld)) {
			check_request($fld);
		} else {
			$fld = sql_escape($fld);
		}
	}
	if(!empty($empty_flds)) {
		$message = 'Вы не заполнили некоторые поля: <br /> <ul>';
		foreach($empty_flds as $fld_name=>$dummy) {
			$message .= '<li>';
			$message .=  (isset($flds_human_names[$fld_name])) ? $flds_human_names[$fld_name] : $fld_name;
			$message .= '</li>';
		}
		$message .= '</ul>';
		$_SESSION['SYSTEM_MESSAGE'] = $message;
		header('Location: ' . $_SERVER['HTTP_REFERER']);
		exit();
	}
}

define("PRIMARY_KEY", "id");


if(isset($_POST['dogovor_id'])) {	
	$dogovor_id = sql_escape($_POST['dogovor_id']);
	sql_query("START TRANSACTION");
	$request = $_POST;
	//echo '<pre>'; var_dump($request); echo '</pre>';
	$current_dogovor_id = $request['dogovor_id'];
	$where_sql = (isset($request['ver'])) ? "AND ver='" . $request['ver'] . "'" : "ORDER BY create_ts LIMIT 1";
	$current_id = sql_get_value('id', 'dogovor_gz', "dogovor_id = '$current_dogovor_id' " . $where_sql);
	$dgz_id = sql_get_value('id', 'dogovor_gz', "dogovor_id = '$dogovor_id'" . ver_sql());
	flush_tables($current_id);
	//echo $current_id; die();
	$request['dogovor_gz_id'] = fill_dogovor_gz($_POST);
	fill_dogovor_gz_okved2($request);
	//fill_dogovor_gz_placing_way($request);//??
	fill_dogovor_gz_preferences($request);
	fill_dogovor_gz_position($request);
	fill_gz_44_sostav($current_id, $request['dogovor_gz_id']);
	sql_query("COMMIT");
	$_SEESION['SYSTEM_MESSAGE_SUCCESS'] = 'Договор (Гос. закупки) успешно сохранен!';

	header("Location: " . $_SERVER['HTTP_REFERER']);
}

function fill_gz_44_sostav($last_id, $new_id) {
	$last_gz_44_id = sql_get_value('id', 'gz_44_sostav', "dogovor_gz_id = '$last_id'");
	if($last_gz_44_id) {
		sql_query("UPDATE gz_44_sostav SET dogovor_gz_id = '$new_id' WHERE id = '$last_gz_44_id'");
	} else if(!$last_gz_44_id && $gz_44_draft_id = gz_44_draft_id()) {
		sql_query("INSERT INTO gz_44_sostav (gz_44_id, dogovor_gz_id) VALUES ('$gz_44_draft_id', '$new_id')");
	}
}

function mask($val) {
	if ($val === NULL) return 'null';
	else return "'$val'";
}

function fill_dogovor_gz($request) {
	$table = 'dogovor_gz';
	$flds = get_flds($table);
	$fld_values = bind_values($flds, $request);
	$fld_values['create_ts'] = date('Y-m-d H:i:s', time());

	$dogovor_id = $fld_values['dogovor_id'];
	$ver = $fld_values['ver'];

	$fld_values = array_map('mask', $fld_values);
	//echo '<hr>'; var_dump($request); echo '<hr>'; var_dump($fld_values); echo '<hr>';
	
	//echo "dogovor_id = $dogovor_id, ver = $ver"; echo '<hr>';
	
	$id = sql_get_value('id', 'dogovor_gz', "dogovor_id = $dogovor_id AND ver = $ver");
	
	if ($id != 0)
	{
		//echo "Запись обнаружена $id<hr>";
		$sql = "UPDATE dogovor_gz SET ";
		for ($i = 0; $i < count($flds); $i++)
		{
			if ($i > 0) 
				$sql .= ', ';
			$sql .= $flds[$i] . ' = ' . $fld_values[$flds[$i]];
		}
		$sql .= " WHERE dogovor_id = $dogovor_id AND ver = $ver";
		//echo $sql; echo '<hr>';
		sql_query($sql);
	}
	else
	{
		//echo 'Запись не обнаружена<hr>';
		$sql = "INSERT INTO dogovor_gz (" . implode(', ', $flds) . ") VALUES(" . implode(', ', $fld_values).")";	
		//echo $sql; echo '<hr>';
		sql_query($sql);
		$id = sql_last_id();
	}
	//echo "$id<hr>Договор гос. закупки $dogovor_id успешно обновлён<hr>"; echo '<a href="/dogovor.php?id=' . (string)$dogovor_id . '&page=dogovor_gz&action=edit">Вернуться к договору</a>';
	return $id;
	
	//var_dump (save_table('dogovor_gz', $fld_values));
	
	/*
	
	$fld_values = array_map('mask', $fld_values);
	//
	echo '<hr>'; var_dump($dgz_id); echo '<hr>';
	//echo "<hr>Добавление $dgz_id<hr>";
	
	$sql_insert = "INSERT INTO dogovor_gz (".implode(',', $flds ).") VALUES(".implode(',', $fld_values).")";	
	
	die($sql_insert);
	
	$sql_udate = "UPDATE dogovor_gz SET ";
	echo '<hr>'; var_dump($flds); echo '<hr>';
	//foreach ($flds as $k => $v)  $sql_udate .= 
	
	sql_query($sql_insert);
	echo '<hr>'; var_dump($sql_insert); echo '<hr>';
	
	return sql_last_id();
	
	*/
}

/*
function save_table($table, $fld_vals) {
	$pk = sql_table_key($table);
	foreach ($fld_vals as $k=>$v) {
		$fld_vals[$k] = mask($v);
	}
	if (isset($fld_vals[$pk])) { // UPDATE	
		return $fld_vals[$pk];
	} else { // insert	
		return sql_last_id();
	}
}
*/

function fill_dogovor_gz_okved2($request) {
	$table = 'dogovor_gz_okved2';
	$okveds = $request['okved'];
	$dgz_id = $request['dogovor_gz_id'];
	foreach ($okveds as $okved) {
		$okved = sql_escape($okved);
		sql_query("INSERT INTO $table (dogovor_gz_id, okved2_code) VALUES ('$dgz_id', '$okved')");
	}
}

function fill_dogovor_gz_preferences($request) {
	$table = 'dogovor_gz_preferences';
	//echo '<hr>'; var_dump($request); echo '<hr>'; 
	if (!array_key_exists ('gz_preferences', $request)) return;
	$prefs = $request['gz_preferences'];
	$dgz_id = $request['dogovor_gz_id'];
	foreach ($prefs as $pref) {
		$pref = sql_escape($pref);
		sql_query("INSERT INTO $table (dogovor_gz_id, pref_code) VALUES ('$dgz_id','$pref')");
	}
}

/*
function fill_dogovor_gz_placing_way($request) {
	//echo '<pre>'; var_dump($request); echo '</pre>'; 
	$table = 'dogovor_gz_placing_way';
	$dgz_id = $request['dogovor_gz_id'];
	$placing_way = (isset($request['gz_placing_way'])) ? $request['gz_placing_way'][0] : NULL;
	sql_query("INSERT INTO $table (dogovor_gz_id, gz_placing_way) VALUES ('$dgz_id', '$placing_way')");
}
*/

function split_positions($request) {
	$position_flds = array(
		'quantity',
		'sum',
		'okpd',
		'gz_okei'
	);
	$positions = array();
	foreach($request as $key=>$value) {
		if(in_array($key, $position_flds)) {
			for($i = 0; $i < count($value); $i++) {
				$positions[$i][$key] = $value[$i];
			}
		}
	}
	return $positions;
}

function fill_dogovor_gz_position($request) {
	$table = 'dogovor_gz_position';
	$positions = split_positions($request);
	$dgz_id = $request['dogovor_gz_id'];
	for($i = 0; $i < count($positions); $i++) {
		$position = $positions[$i];
		sql_query("INSERT INTO $table (dogovor_gz_id, ".implode(',', array_keys($position)).") VALUES ('$dgz_id', '".implode('\',\'', array_values($position))."')");
	}
}

function bind_values($flds, $request) {
	$fld_values = array();
	foreach ($flds as $fld) {
		//gz_placing_way_code 
		//gz_placing_way
		if ($fld == 'gz_placing_way_code') {
			$fld_values[$fld] = (isset($request['gz_placing_way']) && $request['gz_placing_way'][0] != '') ? $request['gz_placing_way'][0] : NULL;
		} else if ($fld == 'gz_change_reason_id') {
			$fld_values[$fld] = (isset($request['gz_change_reason']) && $request['gz_change_reason'][0] != '') ? $request['gz_change_reason'][0] : NULL;
		} else {
			$fld_values[$fld] = (isset($request[$fld]) && $request[$fld] != '') ? $request[$fld] : NULL;
		}
	}
	return $fld_values;
}

function get_flds($table, $only_names = true) {
	$flds = sql_table_columns($table);
	if(isset($flds[PRIMARY_KEY])) unset($flds[PRIMARY_KEY]);
	return ($only_names) ? array_keys($flds) : $flds;
}

function ver_sql() {
	$ver_sql = (isset($_POST['ver']) && $_POST['ver']) ? " AND ver = '" . sql_escape($_POST['ver']) . "'" : "";
	return $ver_sql;
}

function flush_tables($dgz_id) {	
	sql_query("DELETE FROM dogovor_gz_okved2 WHERE dogovor_gz_id = '$dgz_id'");
	sql_query("DELETE FROM dogovor_gz_position WHERE dogovor_gz_id = '$dgz_id'");
	sql_query("DELETE FROM dogovor_gz_preferences WHERE dogovor_gz_id = '$dgz_id'");
	//AVD
	sql_query("DELETE FROM gz_44_sostav WHERE dogovor_gz_id = '$dgz_id'");
	//
	//sql_query("DELETE FROM dogovor_gz WHERE id = '$dgz_id'");
}