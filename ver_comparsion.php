<?php

$title = "Сравнение версий по договору ID: ${_GET['dogovor_id']}";

include $_SERVER['DOCUMENT_ROOT'] . '/init.php';
include $_SERVER['DOCUMENT_ROOT'] . '/template/header.php';

?>

<script src="/assets/js/multiselect-plugin/modalWindowBody.js"></script>
<script src="/assets/js/multiselect-plugin/multiselect.js"></script>
<script src="/assets/js/tree-plugin/js/treeplugin.js"></script>

<?php 

define('DELETED', 'DELETED');
define('UPDATED', 'UPDATED');
define('ADDED', 'ADDED');

function draw_header() {
	echo "<h1>Сравнение версий по договору ID: ${_GET['dogovor_id']}</h1>";
}

function draw_form($dogovor_id) {
	$versions = sql_rows("SELECT id, date_format(ts, '%Y-%m-%d') as d, max(ts) as ts FROM log_dogovor_sostav WHERE dogovor_id = '$dogovor_id' GROUP BY user_id, d ORDER BY ts asc");
    echo '<div style="width: 80%; margin: 15px auto; text-align: center;">
    		<form id="ds-comparsion" method="GET" action="/ver_comparsion.php">';
    echo '<label>Версия 1: 
    		<select class="custom-select" name="ver_1">';
    echo 		'<option value="-1">Не выбрана</option>';
			    foreach ($versions as $ver) {
			    	$selected = '';
			    	if(isset($_GET['ver_1']) && $_GET['ver_1'] == $ver['ts']) $selected = 'selected';
			        echo '<option value="'.$ver['ts'].'" '.$selected.'>' . date('d.m.Y H:i:s', strtotime($ver['ts'])) . '</option>';
			    }
    echo '</select>
    	</label>';
    echo ' <label>Версия 2: 
    		<select class="custom-select" name="ver_2">';
    	echo '<option value="'.date('Y-m-d H:i:s').'">Текущая</option>';
		    foreach ($versions as $ver) {
		    	$selected = '';
		    	if(isset($_GET['ver_2']) && $_GET['ver_2'] == $ver['ts']) $selected = 'selected';
		        echo '<option value="'.$ver['ts'].'" '.$selected.'>' . date('d.m.Y H:i:s', strtotime($ver['ts'])) . '</option>';
		    }
    echo '</select>
    	</label>';
    echo '<input type="hidden" value="'.$dogovor_id.'" name="dogovor_id" />';
    echo ' <input type="submit" value="Сравнить">
	    </form>
	    </div>';
}

function draw_legend() {
	echo '<div style="width: 80%; height: 20px; margin: 25px auto;">
			<div style="background-color: gray; padding: 7px; color: #fff; float: left; border-radius: 4px 0 0 4px;">Удаленные</div>
			<div style="background-color: orange; padding: 7px; color: #fff; float: left">Измененные</div>
			<div style="background-color: green; padding: 7px; color: #fff; float: left; border-radius: 0 4px 4px 0;">Добавленные</div>
		 </div>';
}

function draw_table($ver, $ts) {
	$td = newTD('log_dogovor_sostav');
	$cols = $td->columns;
	$sum = 0;
	$html = '<div style="width: 80%; margin: 20px auto;">';
	$html .= '<h1 style="text-align: left">Версия на ' . date('d.m.Y H:i:s', strtotime($ts)) . '</h1><br/>';
		$html .= '<table class="td-table">';
			foreach ($cols as $col) {
				$html .= '<th>' . $col['label'] . '</th>';
			}
			foreach($ver as $id => $dummy) {
				$tr_tag = '<tr>';
				if ($status=$ver[$id]['status']) {
					if ($status == DELETED) $tr_tag = '<tr style="background:gray; color: white">';
					elseif ($status == UPDATED) $tr_tag = '<tr>'; //<tr style="background:yellow">';
					elseif ($status == ADDED) $tr_tag = '<tr style="background:green; color: white">';
				} 
				$html .= $tr_tag;
				$row = $ver[$id]['row'];
				foreach ($row as $fld => $val) {
					$td_tag = '<td>';
					if (in_array($fld,array_keys($ver[$id]['updated']))) {					
						$old_val = $ver[$id]['updated'][$fld];
						$td_tag = '<td style="background:orange; color: white" title="'.$old_val.'">';
					}
					if(isset($cols[$fld]) && $cols[$fld]['has_sum']) $sum += (float)$val;
					if ($options=$cols[$fld]['options']) {
						if (isset($options[$val])) $val = $options[$val];
					}
					$html .= $td_tag . $val . '</td>';
				}
				$html .= '</tr>';
			}
			$html .= '<tr><td colspan="'.count($cols).'" style="text-align: center; font-weight: bold;"> Сумма: ' . number_format($sum, 2, '.', ' ') . '</td></tr>';
		$html .= '</table><br/>';
	$html .= '</div>';
	return $html;
}

function compare(&$ver_1, &$ver_2) {
	// для ver_1 ставим только статус deleted
	foreach ($ver_1  as $key=>$dummy) {
		if (!isset($ver_2[$key])) {
			$ver_1[$key]['status'] = DELETED;
		}
	}
	foreach ($ver_2  as $key=>$dummy) {
		if (!isset($ver_1[$key])) {
			$ver_2[$key]['status'] = ADDED;
		} else {
			$row_new = $ver_2[$key]['row'];
			$row_old = $ver_1[$key]['row'];
			foreach($row_new as $fld=>$val) {
				if (! in_array($fld,array('user_id','ts','operation_type'))) {
					if ($val!=$row_old[$fld]) {
						$ver_2[$key]['status'] = UPDATED;	
						$ver_2[$key]['updated'][$fld]=$row_old[$fld];
					}
				}
			}
		}
	}
}

function get_ver($dogovor_id, $ver_d) {
	$result = array();
	$id = "id";
	$sql = "SELECT * 
			FROM `log_dogovor_sostav` l 
			WHERE dogovor_id='$dogovor_id' and operation_type in (1,2,4) and ts = ( 
			SELECT MAX( ts ) 
			FROM log_dogovor_sostav l2
			WHERE l2.id = l.id and ts < '$ver_d') order by id asc";
	$ver_rows = sql_rows($sql);
	foreach($ver_rows as $row) {
		$result[$row[$id]] = array("status"=>null, "row"=>$row, "updated"=>array());
	}
	return $result;
}

draw_header();
draw_form($_GET['dogovor_id']);
if(isset($_GET['ver_1']) && isset($_GET['ver_2'])) {
	$ver_1 = get_ver($_GET['dogovor_id'], $_GET['ver_1']);
	$ver_2 = get_ver($_GET['dogovor_id'], $_GET['ver_2']);
	compare($ver_1,$ver_2);
	draw_legend();
	echo draw_table($ver_1, $_GET['ver_1']);
	echo draw_table($ver_2, $_GET['ver_2']);
}