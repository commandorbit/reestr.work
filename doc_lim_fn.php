<?php

define('EMAIL_ADDR_FROM', 'no-reply@unecon.ru');

function doc_lim_file_upload($id) {
    $path='/DOCS/DOG/';
    $filename=file_upload($path);
	$path=cr_path_year_month($path,date('Y.m.d'));
    $author_id = $_SESSION['user_id'];
    foreach ($filename as $key => $value) {
        $sql = "INSERT INTO doc_lim_change_files (doc_lim_change_id, author_id, filename, path) VALUES($id, $author_id, '$value', '$path')";
        $res=sql_query($sql);   
    }
    header('Location: ' . $_SERVER['HTTP_REFERER']);
}

function doc_lim_out($T,$data) {
    echo '<div class="form-controls dogovor_fields textarea-min clearfix" style="width: 800px; margin: 30px auto;">';
    //print_r($T->columns);  

    foreach ($T->columns as $field=>$col) {
        //ПРОВЕРИТЬ НА ДОГОВОР НОМ - ОТКУДА БЕРЕТСЯ
        if($field == 'dogovor_nom' || $field == 'dogovor_status_id' || $field == 'is_approved')
            continue;   


        if ($field=='id') {
            echo '<tr style="display: none;"><td><input type="hidden" name="id" value="' .notset($data,$field) .'"></td></tr>';
        } else {
            echo '<div class="form-group" id="'.$field.'">';
            echo '<label><span>'. $col['label'] .'</span><br />';

            $disabled = !$T->may_edit;             
            echo $T->control($col,notset($data,$field), is_null($data), $disabled);
            echo '</label>';
            echo '</div>'."\n";
        }
    }
    echo '</div>';
}

function doc_zfo_sostav($id, $zfo) {
    $fin_sources = array_to_sql_string(user_own_fin_sources());
    $sql = "select * from doc_lim_change_sostav where doc_lim_change_id = '$id' and 
		(zfo_id in ($zfo) or fin_source_id in ($fin_sources))";
    $res=sql_query($sql);
    return sql_rows_fix_numeric($res);
}

function doc_lim_sostav_out($may_edit,$id) {
	$T = newTD('doc_lim_change_sostav',array('f_doc_lim_change_id'=>$id));
    /*if ($user_zfo=user_zfo_id()) {
        $T->columns['fp_article_id']['edit_options']=sql_to_assoc("select id,name from v_fp_article where id in (select fp_article_id from fp_article_zfo where zfo_id='$user_zfo') order by shifr");
    }*/
    if(user_has_zfo()) {
        $T->data = doc_zfo_sostav($id, user_zfo_id_str());
    }
	$T->has_filter=false;
    $T->use_buttons=false;
    $T->fixed_headers=true;
    $T->may_edit = $may_edit;
	unset($T->columns['doc_lim_change_id']);
	unset($T->columns['smeta_ver']);
    $T->foreign_key=array('doc_lim_change_id'=>$id);
    if ($may_edit) {
        $T->html_after = '<div  style="text-align:center; margin-top: -50px; margin-bottom: 40px;"><a style="display:block;" class="td-add-button" href="#">Новая запись</a></div>';
    }	
    return $T;
}

function files_form_out() {
	$form = '<form style="width: 320px; height: 50px; margin: 20px auto;" enctype="multipart/form-data" method="post">
			      <input type="file" name="filename[]" multiple="true" />
			      <input type = "hidden" name="uploaded" value = "1">
			      <input type="submit" name="submit" value="Загрузить" />
			</form>';

	return $form;
}

function apply_limits($id) {
	//array
    $err = array();
    $is_approved = sql_get_value('is_approved','doc_lim_change',"id = $id");
    if ($is_approved) {
        echo "Документ уже был утвержден!";
        return false;
    }
	$doc_lim_rows = sql_rows("select * from doc_lim_change_sostav where doc_lim_change_id = '$id'");
    $table = 'zfo_limit';
    sql_query("START TRANSACTION");

    $zfo_ids = array();

	foreach ($doc_lim_rows as $row) {
		$zfo_limit_id = sql_get_value('id', $table, "zfo_id = ".$row['zfo_id']." and "."fin_source_id = ".$row['fin_source_id']." and fp_year = ".$row['fp_year']);
        $zfo_ids[] = $row['zfo_id'];
        $row_limit = $row;
		if ($row['fp_year']>=2019) {
			if (!$row['smeta_id'] || !$row['kvr']) {
				echo "Не указана смета или КВР";
				return false;
			}
			$smeta_limit = sql_get_array("smeta_fin_source","smeta_id=".$row['smeta_id']." and fin_source_id=".$row['fin_source_id']);
			if ($smeta_limit===false) {
				echo "У сметы ".$row['smeta_id']. " не заполнен лимит по ИФ";
				return false;
			}
			if ($smeta_limit['amount_limit'] + $row['sum'] < 0) {
				echo "Для сметы ".$row['smeta_id']. " недопустимая корректировка текущий лимит " . $smeta_limit;
				return false;
			}
			save("smeta_fin_source",['id'=>$smeta_limit['id'],'amount_limit'=>$smeta_limit['amount_limit']+$row['sum']]);
//			sql_query("UPDATE smeta_fin_source SET amount_limit = amount_limit+ '".$row['sum']."' WHERE smeta_id=".$row['smeta_id']." and fin_source_id=".$row['fin_source_id']);
		}
        if ($zfo_limit_id)  {
            $row_limit['id'] = $zfo_limit_id;
            $sum_old = sql_get_value('sum','zfo_limit',"id=$zfo_limit_id");
            $row_limit['sum'] += $sum_old; 
        } else {
            $row_limit['id'] = NULL;
        }
        SaveData::$source_tname = 'doc_lim_change';
        SaveData::$source_tid = $id;
        save($table, $row_limit);
	}
	
	sql_query("INSERT INTO zfo_smeta_limit(is_doc_lim,y,smeta_id,zfo_id,fin_source_id,kvr,amount) 
			SELECT 1,fp_year,smeta_id,zfo_id,fin_source_id,kvr,sum from doc_lim_change_sostav WHERE fp_year>=2019 AND doc_lim_change_id='$id'");

    if($err = limit_fs_year_errors()) {
        sql_query("ROLLBACK");
        echo (implode('<br>',$err));
    } else {
        $subject = "Изменение лимитов в reestr2015.unecon.ru для вашего ЦФО";
        $body = 'Был изменен лимит для вашего ЦФО. <a href="https://reestr2015.unecon.ru/doc_lim.php?id='.$id.'">Подробности</a>.' ;
        sql_query("update doc_lim_change set is_approved = 1 where id = '$id'");
        sql_query("COMMIT");     
        $mail_id = add_mail_to_db(EMAIL_ADDR_FROM, $subject, $body);
        mailing_users($id,$zfo_ids,$mail_id);
        header('Location: '.$_SERVER['HTTP_REFERER']);
    }
}

function add_mail_to_db($from, $subject, $body) {
    sql_query("INSERT INTO mailing (from_email, subject, text, done) VALUES ('$from', '$subject', '$body', true)");
    return sql_last_id();
}

function send_mail_from_db() {
    $letters = sql_rows("   SELECT mu.id, m.from_email, m.subject, m.text, u.email 
							FROM mailing_user mu 
							LEFT JOIN mailing m ON m.mailing_id = mu.mailing_id 
							LEFT JOIN user u ON u.id = mu.user_id 
							WHERE mu.was_sent = 0   ");
    
    foreach ($letters as $letter) {
        send_mail($letter['email'], $letter['subject'], $letter['text'], $letter['from_email']);
        sql_query("UPDATE mailing_user SET was_sent = 1 WHERE id = '${letter['id']}'");
    }
}

function mailing_users($lim_id,$zfo_ids, $mail_id) {
    $zfo_ids_str = implode(',', $zfo_ids);
    $mailing_users = sql_rows("SELECT id, email FROM user 
        WHERE ( is_admin = 1 OR  
            id in (select user_id from user_zfo where zfo_id in ($zfo_ids_str) )
        )
        AND may_receive_mail = 1 ");
    foreach($mailing_users as $user) {
        if(email_validate($user['email'])) {
            sql_query("INSERT INTO mailing_user (mailing_id, user_id, add_ts, was_sent) VALUES('$mail_id', '${user['id']}', NOW(), false)");
        }
    }
    send_mail_from_db();
}

function limit_fs_year_errors() {
    $sql = "SELECT  t.fin_source_id,t.fp_year,t.zl_sum,fsl.sum from (
            SELECT fin_source_id, fp_year, sum(sum)  as zl_sum
			FROM  (
				SELECT  fin_source_id, fp_year, sum
				from zfo_limit 
				WHERE fp_year<2019 
				UNION ALL 
				SELECT fin_source_id,y,amount
				FROM zfo_smeta_limit
				WHERE y>=2019
			) pre 
            group by fp_year,fin_source_id) t
            left join fin_source_limit fsl on (t.fin_source_id = fsl.fin_source_id and t.fp_year = fsl.fp_year)
            where t.zl_sum>0 AND (t.zl_sum > fsl.sum or fsl.sum is null) ";

    $res = sql_query($sql);
    $err = array();
    while ($row = sql_array($res)) {
        $fin_source_name = sql_get_value('name','fin_source','id=' . $row['fin_source_id']);
        $err[] = "<p style='color: red;'>За " .$row['fp_year']. "г. по статье '$fin_source_name' лимит " . $row['sum'] . " сумма по  всем цфо  " . $row['zl_sum'] . "  превышение: "  . ( ($row['zl_sum'] - $row['sum']) ) . " руб.</p>" ;
    }
    return ($err);
}

function notset($array,$key,$default=null) {
    return isset($array[$key]) ? $array[$key] : $default;
}