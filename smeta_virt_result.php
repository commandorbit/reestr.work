<?
require_once ($_SERVER['DOCUMENT_ROOT'] . '/reestr_db.php');
require_once ($_SERVER['DOCUMENT_ROOT'] . '/modules/define.php');

class SmetaVirtResult {
	public static function getRows($smeta_id,$row_percent=null,$row_add_sum=null,$row_credit=null) {
		
		$return_part = ($row_percent || $row_add_sum);

		$rows_income = self::_rows_income($smeta_id);
		
		$row_income_itogo = self::_row_itogo($rows_income,$smeta_id,'Итого доходы','income_itogo');
		
		if (!$row_percent)
			$row_percent = self::_percent_row($smeta_id);

		$rows_outcome = self::_rows_outcome($smeta_id);
		
		$row_outcome_itogo = self::_row_itogo($rows_outcome,$smeta_id,'Итого расходы','outcome_itogo');
		
		//if (!$row_add_sum)
		$row_add_sum = self::_row_add_sum($smeta_id,$row_add_sum);
		$row_credit = self::_row_credit($smeta_id,$row_credit);
		
		$row_in_budg = self::_row_in_budget($smeta_id);
		$row_out_budg = self::_row_out_budget($smeta_id);
		$row_fin_activ  = self::_row_sub($row_in_budg,$row_out_budg,$smeta_id,'Финансовые активы','fin_activ');
		$rows_fin_activ = [];
		if ($row_in_budg) {
			$rows_fin_activ[] = $row_in_budg;
		}
		if ($row_out_budg) {
			$rows_fin_activ[] = $row_out_budg;
		}
		if ($rows_fin_activ) {
			$rows_fin_activ = array_merge([$row_fin_activ],$rows_fin_activ);
		}
		
		$row_otchisl = self::_row_otchisl($smeta_id);
		$s = self::smeta_start_sum($smeta_id);
		$row_start = ['id'=>'start','smeta_id'=>$smeta_id,'article_type'=>'Остаток на начало периода','v'=>null];
		$row_end = ['id'=>'end','smeta_id'=>$smeta_id,'article_type'=>'Остаток на конец','v'=>null];
		$row_pereschisl = ['id'=>'perechisl','smeta_id'=>$smeta_id,'article_type'=>'Перечисление в ЦФ руб','v'=>0];
		$row_money_potok = ['id'=>'money_potok','smeta_id'=>$smeta_id,'article_type'=>'Денежный поток','v'=>0];
		
		for ($i=1; $i<=12; $i++) {
			$fld = 'v' .$i;
			$row_start[$fld]=$s;
			$row_pereschisl['v'] += $row_pereschisl[$fld] = round(($row_income_itogo[$fld]-$row_otchisl[$fld])*$row_percent[$fld]/100,2);
			
			$ss = round($row_income_itogo[$fld]-$row_pereschisl[$fld]+$row_add_sum[$fld]+$row_credit[$fld]-$row_outcome_itogo[$fld],2);
			$row_money_potok['v'] += $row_money_potok[$fld] = $ss;
			$s+= $ss;
			$row_end[$fld] = $s;
		}
		$row_start['v'] = $row_start['v1'];
		$row_end['v'] = $row_end['v12'];
		
		// Если всюду проценты совпадают то и годовой - такой-же
		$row_percent['v'] = $row_percent['v1'];
		for ($i=1;$i<=12;$i++) {
			if ($row_percent['v'.$i]!=$row_percent['v']) {
				$row_percent['v']=null;
				break;
			}
		}
		
		
		if ($return_part) {
			// row_add_sum ради суммы по строке
			$res =  array_merge([$row_start],[$row_pereschisl],[$row_add_sum],[$row_credit],[$row_money_potok],[$row_end]);
		} else {
			$res =  array_merge([$row_start],[$row_income_itogo],$rows_income,[$row_percent],[$row_pereschisl],[$row_add_sum],[$row_credit],[$row_outcome_itogo],$rows_outcome,
			$rows_fin_activ,[$row_money_potok],[$row_end]);
		}
			
		$has_errors = false;	
		foreach($res as &$r) {
			$id = $r['id'];
			$r['id'] = $smeta_id . '-' .$id;
			if (!in_array($id,['start','end','percent'])) {
				$s = 0;
				for ($i=1;$i<=12;$i++) {
					$s+=$r['v'.$i];
				}
				$s = round($s,2);
				if (abs($r['v']-$s)>0.01) {
					$has_errors = true;
					$r['v0'] = $r['v']-$s;
				} else {
					$r['v0'] = null;
				}
			}
		}
		unset($r);
		
		return $res;
	}
	
	
	private static function _rows_income($smeta_id) {
		//По ФП статьям с НДС
		$sql = "select smeta_id, ai.kvr, 'Доходы' article_type, ai.name article_name,sum(amount) v ";
		for ($i=1;$i<=12;$i++) {
			$sql .= ", sum(amount$i) v$i";
		}
		$sql .= " FROM smeta_income si LEFT JOIN fp_article_income ai ON si.fp_article_income_id=ai.id ";
		$sql .= " WHERE smeta_id=$smeta_id AND (ai.kvr is null or (ai.kvr<>510 and ai.kvr<>610))";
		$sql .= " GROUP BY fp_article_income_id ";
		$sql .= " ORDER BY  ai.kvr ";
		
		$rows_income = self::_sql_rows($sql,'income');
		
		// НДС с минусом
		$sql = "select smeta_id, null kvr, 'Доходы' article_type, 'НДС' article_name,-sum(round(nds/100*amount/(1+nds/100),2)) v ";
		for ($i=1; $i<=12; $i++) {
			$sql .= ", -sum(round(nds/100*amount$i/(1+nds/100),2)) v$i";
		}
		$sql .= " FROM smeta_income si ";
		$sql .= " WHERE nds<>0 AND smeta_id=$smeta_id";
		$rows_income_nds = self::_sql_rows($sql,'income_nds');
		
		return array_merge($rows_income,$rows_income_nds);
	}
	
	private static function _rows_outcome($smeta_id) {
		$sql = "select smeta_id, kvr, 'Расходы' article_type, ss.fp_article_id article_id, a.name article_name, sum(amount) v ";
		for ($i=1;$i<=12;$i++) {
			$sql .= ", sum(amount$i) v$i";
		}
		$sql .= " FROM smeta_sostav ss LEFT JOIN fp_article a ON ss.fp_article_id=a.id ";
		$sql .= " WHERE smeta_id=$smeta_id AND (a.kvr is null or (a.kvr<>510 and a.kvr<>610))";	
		$sql .= " GROUP BY ss.fp_article_id ";
		$sql .= " ORDER BY a.kvr ";
		
		$rows_outcome = self::_sql_rows($sql,'outcome');
		
		// Расходы по ФП статьям
		$rows_outcome_building = self::_building_rows($smeta_id);
		$r_o = array_merge($rows_outcome,$rows_outcome_building);
		self::_setid($r_o,'outcome');
		return $r_o;
	}

	private static function _row_otchisl($smeta_id) {
		// Уменьшает сумму доходов для умножения на процент
		$sql = "select smeta_id, kvr, 'Расходы' article_type, a.name article_name,sum(amount) v ";
		for ($i=1;$i<=12;$i++) {
			$sql .= ", sum(amount$i) v$i";
		}
		$sql .= " FROM smeta_sostav ss INNER JOIN fp_article a ON ss.fp_article_id=a.id ";
		$sql .= " WHERE smeta_id=$smeta_id AND a.is_perechisl_for_smeta_percent=1";	
		$res = sql_query($sql);
		
		$rows_otchisl = self::_sql_rows($sql,'otchisl');
		if (count($rows_otchisl)) {
			$row_otchisl = $rows_otchisl[0];
		} else {
			$row_otchisl = ['smeta_id'=>$smeta_id,'v'=>0];
			for ($i=0;$i<=12;$i++) {
				$row_otchisl['v'.$i] = 0;
			}
		}
		return $row_otchisl;
	}
	
	private static function _sql_rows($sql,$id=null) {
		$res = sql_query($sql);
		$rows = sql_rows_fix_numeric($res);
		self::_setid($rows,$id);
		return $rows;
	}
	
	public static function smeta_start_sum($smeta_id) {
		$r = sql_get_array("smeta","id=$smeta_id");
		if ($r['amount_start_import']==0) {
			return $r['amount_start'];
		} else {
			$y = $r['fp_year_id'];
			$finsources = sql_to_assoc("select fin_source_id,fin_source_id from smeta_fin_source where smeta_id = $smeta_id ");
			$s = 0;
			foreach ($finsources as $fin_source_id) {
				if (sql_get_value("id","fin_source_remains","fin_source_id='$fin_source_id' and y='$y'")) {
					$s += sql_get_value("sum","fin_source_remains","fin_source_id=$fin_source_id and y=$y");
				} else {
					//  ИСПРАВИТЬ! проход по сметам прошлых лет?
					
				}
			}
			return $s; 
		}
	}
	
	private static function _row_in_budget($smeta_id) {
		//По ФП статьям с НДС
		$sql = "select smeta_id,  count(smeta_id) cnt, ai.kvr, 'Поступления на счета бюджетов' article_type, ai.name article_name, sum(amount) v ";
		for ($i=1;$i<=12;$i++) {
			$sql .= ", sum(amount$i) v$i";
		}
		$sql .= " FROM smeta_income si LEFT JOIN fp_article_income ai ON si.fp_article_income_id=ai.id ";
		$sql .= " WHERE smeta_id=$smeta_id AND ai.kvr=510";
		
		$rows = self::_sql_rows($sql,'in_budget');
		$row_in_budget = [];
		// sum всегда вернет строку
		if ($rows[0]['cnt']) {
			$row_in_budget = $rows[0];
			unset($row_in_budget['cnt']);
		}
		return $row_in_budget;
	}

	private static function _row_out_budget($smeta_id) {
		$sql = "select smeta_id, count(smeta_id) cnt, kvr, 'Выбытие со счетов бюджетов' article_type, ss.fp_article_id article_id, a.name article_name, sum(amount) v ";
		for ($i=1;$i<=12;$i++) {
			$sql .= ", sum(amount$i) v$i";
		}
		$sql .= " FROM smeta_sostav ss LEFT JOIN fp_article a ON ss.fp_article_id=a.id ";
		$sql .= " WHERE smeta_id=$smeta_id AND a.kvr=610";	
	
		$rows = self::_sql_rows($sql,'out_budget');
		$row_out_budget = [];
		if ($rows[0]['cnt']) {
			$row_out_budget = $rows[0];
			unset($row_out_budget['cnt']);
		}
		return $row_out_budget;
	}
	
	private static function _setid(&$rows,$sufix,$may_edit = false) {
		$smeta_id = 0;
		$i = 0;
		foreach($rows as &$r) {
			if ($r['smeta_id']<>$smeta_id) {
				$smeta_id=$r['smeta_id'];
				$i=0;
			}
			$i++;
			$r['id']= $sufix.'_'.$i;
			$r['may_edit'] = $may_edit;
		}
	}
	
	
	
	private static function __row_dop_sum($smeta_id,$fld,$src_sum) {

		$sql = "select smeta_id ";
		for ($i=1;$i<=12;$i++) {
			$sql .= ", $fld$i v$i";
		}
		$sql .=" from smeta_fin_param WHERE smeta_id = $smeta_id";
		$res = sql_query($sql);
		$rows = sql_rows_fix_numeric($res);
		if (count($rows) && !$src_sum) {
			$r = $rows[0];
		} else {
			$r = ['smeta_id'=>$smeta_id];
			for ($i=1;$i<=12; $i++) {
				$r['v'.$i] = isset($src_sum['v'.$i]) ? $src_sum['v'.$i] : 0;
			}
		}
		$sum = 0;
		for ($i=1;$i<=12;$i++) {
			$sum += $r['v'.$i];	
		}
		$r['v'] = round($sum,2);
		return $r;
	}
	
	
	private static function _row_add_sum($smeta_id,$src_sum=null) {
		$r = self::__row_dop_sum($smeta_id,'sum_add',$src_sum);
		$r['id'] = 'sum_add';
		$r['may_edit'] = true;
		$r['article_type'] = 'Дофинансирование';
		return $r;
	}
	
	private static function _row_credit($smeta_id,$src_sum=null) {
		$r = self::__row_dop_sum($smeta_id,'sum_credit',$src_sum);
		$r['id'] = 'sum_credit';
		$r['may_edit'] = true;
		$r['article_type'] = 'Заимствование';
		return $r;
	}

	
	private static function _percent_row($smeta_id) {
		// Процент
		$sql = "select smeta_id ";
		for ($i=1;$i<=12;$i++) {
			$sql .= ", perc$i v$i";
		}
		$sql .=" from smeta_fin_param WHERE smeta_id = $smeta_id";
		$rows_percent = self::_sql_rows($sql);
		if (count($rows_percent)) {
			$r_percent = $rows_percent[0];
		} else {
			$r_percent = ['smeta_id'=>$smeta_id];
			for ($i=1;$i<=12; $i++) {
				$r_percent['v'.$i] = 0;
			}
		}
		$r_percent['id'] = 'percent';
		$r_percent['may_edit'] = true;
		$r_percent['article_type'] = 'Отчисления в ЦФ %';

		return $r_percent;
	}
	
	private static function _row_itogo($rows,$smeta_id,$article_type='',$id=null) {
		$r = [];
		$r['smeta_id'] = $smeta_id;
		$r['id'] = $id;
		$r['article_type'] = $article_type;
		$r['v'] = 0;
		for ($i=1;$i<=12;$i++) {
			$r['v'.$i] = 0;
		}
		
		foreach ($rows as $r_o) {
			$r['v'] += $r_o['v'];
			for ($i=1;$i<=12;$i++) {
				$r['v'.$i] += $r_o['v'.$i];
			}
		}
		return $r;
	}
	
	private static function _row_sub($r1, $r2, $smeta_id, $article_type='', $id=null) {
		$r = [];
		$r['smeta_id'] = $smeta_id;
		$r['id'] = $id;
		$r['article_type'] = $article_type;
		
		$fld = 'v';
		$r[$fld] = 0;
		$r[$fld] += isset($r1[$fld]) ? $r1[$fld] : 0;
		$r[$fld] -= isset($r2[$fld]) ? $r2[$fld] : 0;
		
		for ($i=1;$i<=12;$i++) {
			$fld = 'v'.$i;
			$r[$fld] = 0;
			$r[$fld] += isset($r1[$fld]) ? $r1[$fld] : 0;
			$r[$fld] -= isset($r2[$fld]) ? $r2[$fld] : 0;
		}
		return $r;
	}
	
	private static function _building_rows($smeta_id) {
		$zfo_id = sql_get_value('zfo_id',"smeta","id=$smeta_id");
		$zfo_type_id = sql_get_value("zfo_type_id","zfo","id=$zfo_id");
		// Если не ЦФР - не считаем
		if ($zfo_type_id != ZFO_TYPE_ZFR) {
			return [];
		}
		$fp_year = sql_get_value("fp_year_id","smeta","id=$smeta_id");
		$fin_sources = sql_to_assoc("select distinct fin_source_id,fin_source_id from smeta_fin_source where smeta_id=$smeta_id ");
		$itog  = [];
		foreach($fin_sources as $fin_source) {
			$rows = ReestrDB::query_virt_smeta_building(['fp_year_id'=>$fp_year,'fin_source_id'=>$fin_source]);
			foreach($rows as $r) {
				$art_id = $r['fp_article_id'];
				$it = [];
				$it['v'] = isset($itog[$art_id]) ? $itog[$art_id]['v'] : 0;
				$it['v'] += ($r['amount']*$r['percent']/100);
				for ($i=1;$i<=12;$i++) {
					$it['v'.$i] = isset($itog[$art_id]) && isset($itog[$art_id]['v'.$i]) ? $itog[$art_id]['v'.$i] : 0;
					$it['v'.$i] += $r['amount' .$i]*$r['percent']/100;
				}
				$itog[$art_id] = $it;
			}
		}
		$rows = [];
		foreach($itog as $fp_article_id=>$r) {
			$r['smeta_id'] = $smeta_id;
			if ($fp_article_id) {
				$fp_article_row = sql_get_array('fp_article',"id=$fp_article_id");
				$r['article_name'] = $fp_article_row['name'];
				$r['kvr'] = $fp_article_row['kvr'];
			}
			$r['article_type']='Расходы на здания';
			$rows[] = $r;
		}
		return $rows;
	}
}