<?php
function cr_filename($path,$fullname){
	$maxnumb=-1;
    $fname=pathinfo($fullname,PATHINFO_FILENAME);
    $ext=pathinfo($fullname,PATHINFO_EXTENSION);
    
	$allfiles=scandir($path);		
	foreach ($allfiles as $tmpflname){
        $tmpname=pathinfo($tmpflname,PATHINFO_FILENAME);
        $tmpext=pathinfo($tmpflname,PATHINFO_EXTENSION);
		if (strlen($tmpname)>=strlen($fname)){
			if ($ext==$tmpext && substr($tmpname,0,strlen($fname))==$fname){
				if ($maxnumb==-1) $maxnumb=0;
                $tmpname=substr($tmpname,strlen($fname));
                $pos=strrpos($tmpname,'_');
				$tmpnumb=intval(substr($tmpname,$pos+1));
				if ($maxnumb<$tmpnumb) $maxnumb=$tmpnumb;
			}
		}
	}

	$maxnumb+=1;
	if ($maxnumb>0){$fname.="_$maxnumb";}
	$fname.='.'.$ext;
	return $fname;
}

function cr_path_year_month($base_path,$d){
    $base_path = $_SERVER['DOCUMENT_ROOT'] . '/../reestr' . $base_path;
	$year=substr($d,0,4);
	$month=substr($d,5,2);
	if (!is_dir($base_path.$year))
		mkdir($base_path.$year);
	if (!is_dir($base_path.$year.'/'.$month))
		mkdir($base_path.$year.'/'.$month);
	return $base_path.$year.'/'.$month.'/';
}

function file_upload($basepath) {
	$filename_set = array();

	for($i = 0; $i < count($_FILES["filename"]["name"]); $i++) {
		$max_filesize = 50;

		if($_FILES["filename"]["size"][$i] > $max_filesize*1024*1024) {
			die ("Размер файла превышает $max_filesize мегабайт");
		}
		// Проверяем загружен ли файл
		if(!is_uploaded_file($_FILES["filename"]["tmp_name"][$i])) {
	        die("Ошибка загрузки файла");
	    }
	    // Если файл загружен успешно, перемещаем его
	    // из временной директории в конечную
	    $path=cr_path_year_month($basepath,date("Y.m.d"));
	    $filename=cr_filename($path,$_FILES['filename']['name'][$i]);
	    move_uploaded_file($_FILES['filename']['tmp_name'][$i], $path.$filename);
	    array_push($filename_set, $filename);
	}

	return $filename_set;
}