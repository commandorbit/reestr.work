<?php
require 'init.php';
require dirname(__FILE__) . '/upload_file_fn.php';
//require dirname(__FILE__) . '/dogovor_fn.php';

/*
var_dump($_FILES);
$from = $_FILES["filename"]["tmp_name"];
$to = 'D:\\OpenServer\\domains\\reestr.work\\' .  $_FILES["filename"]["name"];
move_uploaded_file($from,$to);
*/

function ext_file_upload($basepath) 
{
	$filename_set = array();
	
	//var_dump($_FILES);

	//for($i = 0; $i < count($_FILES["filename"]["name"]); $i++) 
	//{
		//$max_filesize = 50;

		/*
		if($_FILES["filename"]["size"][$i] > $max_filesize*1024*1024) {
			die ("Размер файла превышает $max_filesize мегабайт");
		}
		*/
		
		// Проверяем загружен ли файл
		/*
		if(!is_uploaded_file($_FILES["filename"]["tmp_name"][$i])) {
	        die("Ошибка загрузки файла");
	    }
		*/
		
	    // Если файл загружен успешно, перемещаем его
	    // из временной директории в конечную
	    $path = cr_path_year_month($basepath, date("Y.m.d"));
		//var_dump($path);
	    $filename = cr_filename($path, $_FILES['filename']['name']);
		//var_dump($filename);
	    move_uploaded_file($_FILES['filename']['tmp_name'], $path . $filename);
	    array_push($filename_set, $filename);
	//}

	return $filename_set;
}

function ext_dogovor_file_upload($id) 
{
    $path = '/DOCS/DOG/';
    $filename = ext_file_upload($path);
	$path = cr_path_year_month($path, date('Y.m.d'));
    $author_id = $_SESSION['user_id'];
    foreach ($filename as $key => $value) 
	{
        $sql = "INSERT INTO dogovor_files (dogovor_id, author_id, filename, path) VALUES($id, $author_id, '$value', '$path')";
        $res = sql_query($sql);
		if ($res)
		{
			/*
			{
				"success":true, // note this is Boolean, not string
				"msg":"Consignment updated"
			}			
			*/
			echo json_encode(['success' => true, 'msg' => "Файл $value успешно загружен в папку $path"]);
		}
		else
		{
			/*
			{
				"success":false, // note this is Boolean, not string
				"msg":"You do not have permission to perform this operation"
			}
			*/			
			echo json_encode(['success' => false, 'msg' => 'Ошибка при загрузке файла']);
		}
    }
    //header('Location: ' . $_SERVER['HTTP_REFERER']);
}

//var_dump($_REQUEST);
if (isset($_REQUEST['dogovor_id']))
{
	$dogovor_id = sql_escape($_REQUEST['dogovor_id']);
	ext_dogovor_file_upload($dogovor_id);
}
else
	echo json_encode(['success' => false, 'msg' => 'Не задан идентификатор договора!']);

?>
