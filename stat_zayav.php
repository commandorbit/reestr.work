<?php
require 'init.php';
include 'stat/stat.php';
require_once 'stat/stat_stud.php';


$time_start = microtime(true);


$col = request_val('row','fp_issue_id');
$row = request_val('col','fp_podr_id');
$d = request_val('d','');

function stat_sql($f,$w,$d='') {
	global $DB;
	$sql = $DB::sql('v_report_zayav');
	$sql_w=	 Stat::where($w);

	if ($sql_w) {
		if (strstr($sql,'WHERE')) $sql.= ' and ' .$sql_w;	
		else $sql.= ' WHERE ' .$sql_w;	
	} 

	return $sql;
}

$attrs =  $DB::table_columns('v_report_zayav');

//echo '<pre>';

//	print_r($attrs);

//echo '</pre>';

//unset ($attrs['d']);
//unset ($attrs['id']);
unset ($attrs['s']);
unset ($attrs['nom']);
unset ($attrs['expend_issue_id']);
unset ($attrs['month_id']);
unset ($attrs['reestr_year']);
unset ($attrs['plat_month']);
unset ($attrs['plat_year']);
$cr=$attrs;
$cr["date_format(d,'%Y-%m')"]=array('label'=>'Год, месяц');


//unset ($attrs['reestr_d']);
//$attrs['month_id']['options']=array(1=>'Январь',2=>'Февраль',3=>'Март',4=>'Апрель',5=>'Май',6=>'Июнь',7=>'Июль',8=>'Август',9=>'Сентябрь',10=>'Октябрь',11=>'Ноябрь',12=>'Декабрь');
//$years=array(2012=>2012,2013=>2013,2014=>2014);
//$attrs['reestr_year']['options']=$years;
//$attrs['plat_year']['options']=$years;

$Stat = makeStat($attrs,$cr)->set_row_col($row,$col)->set_f($_REQUEST,'w_')->set_func_data('stat_sql')->set_has_d(false)->set_url("/view_stat.php?t=v_report_zayav&stat&");

if (isset($_POST['to_excel'])) {	
	$Stat->to_excel();
	exit();
}

$is_ajax=(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest');
if (!$is_ajax) {
	$title = 'Аналитика заявки';
	include 'template/header.php';
	echo "<h1>$title</h1>";
	echo '<div class="stat_filter_block">';
	echo $Stat->out_form();
	echo '</div>';
	echo '<div class="stat"></div>';
	include 'template/footer.php';

} else {
	if (isset($_POST['show']))  {
		$_SESSION['last_stat']=$_SERVER['REQUEST_URI'];
		echo '<h4>' . $Stat->opisanie() . '</h4>';
		echo  $Stat->print_cross(); 
	}
	echo '<p style="text-align: center;font-size:8pt;">Время выполнения: ' . round((microtime(true) - $time_start),2) . ' сек.</p>';
	exit();
}
