<?php
error_reporting(E_ALL);
$title = 'СУП - система управленческого планирования';
require $_SERVER['DOCUMENT_ROOT'] . '/init.php';
require $_SERVER['DOCUMENT_ROOT'] . '/template/header.php';
?>
<link rel="stylesheet" type="text/css" href="/css/background<?=$_SESSION["palette"]?>.css?ver=<?=VER?>">
<div id="ext-body" class="ext-body">

<!-- EXT 
<link rel="stylesheet" type="text/css" href="//cdn.unecon.ru/ext.js/css/classic/theme-neptune/theme-neptune-all.css">
<script type="text/javascript" src="//cdn.unecon.ru/ext.js/js/classic/ext-all-debug.js"></script>
<script type="text/javascript" src="/ext/locale-ru-debug.js"></script>
<script type="text/javascript" src="/ext/locale-avd.js"></script>
<script type="text/javascript" src="/extapp/app.js"></script>

<link rel="stylesheet" type="text/css" href="/extapp/build/testing/extapp/resources/extapp-all.css">
<script type="text/javascript" src="/extapp/build/testing/extapp/app.js"></script>
-->
<?php
require $_SERVER['DOCUMENT_ROOT'] . '/include_extjs.php';
?>

<script type="text/javascript">
	//console.log(Ext);
    var Ext = Ext || {};
	//console.log(Ext);
    Ext.manifest = '/extapp/<?=$theme_name[$_SESSION["palette"]]?>.json';  // loads "./foo.json" relative to your page
</script>
<script id="microloader" type="text/javascript" src="/extapp/bootstrap.js"></script>

<!-- 
<script id="microloader" type="text/javascript" src="/extapp/bootstrap.js" path="/extapp"></script>
<script id="microloader" type="text/javascript" src="/extavd/bootstrap.js" path="/extavd"></script>
<script id="microloader" type="text/javascript" src="/extapp/bootstrap.js" path="/extapp"></script>
-->

<!--
<link rel="stylesheet" type="text/css" href="//cdn.unecon.ru/ext.js/css/classic/theme-neptune/theme-neptune-all.css">
<script type="text/javascript" src="//cdn.unecon.ru/ext.js/js/classic/ext-all-debug.js"></script>
<script type="text/javascript" src="/ext/locale-ru.js"></script>
<script type="text/javascript" src="/ext/locale-avd.js"></script>
-->

<!--
<script type="text/javascript" src="/extapp/app/model/modelDogovor.js"></script>
<script type="text/javascript" src="/extapp/app/model/modelDogovorStatus.js"></script>
<script type="text/javascript" src="/extapp/app/store/storeDogovor.js"></script>
<script type="text/javascript" src="/extapp/app/store/storeDogovorStatus.js"></script>
<script type="text/javascript" src="/extapp/app/view/AgreementViewController.js"></script>
<script type="text/javascript" src="/extapp/app/view/AgreementViewModel.js"></script>
<script type="text/javascript" src="/extapp/app/view/Agreement.js"></script>
-->

<!--
<link rel="stylesheet" type="text/css" href="//cdn.unecon.ru/ext.js/css/classic/theme-neptune/theme-neptune-all.css">
<script type="text/javascript" src="//cdn.unecon.ru/ext.js/js/classic/ext-all-debug.js"></script>
-->
<!--
<script type="text/javascript" src="/ext/locale-ru.js"></script>
<script type="text/javascript" src="/ext/locale-avd.js"></script>
-->
<script>
	//Ждём загрузки EXTJS
	window.addEventListener("load", function(event)
	{
		var waitExtJSTimer = setInterval(function()
		{
			if (window.extapp && window.extapp.getApplication)
			{
				window.extapp.userId = <?=$user_id?>;
				//alert('USER = ' + window.extapp.userId);
				clearInterval(waitExtJSTimer);
				console.log('Библиотека загружена!');
			}
		},
		100);	
	});

function test() 
{
	//alert('Библиотека подключена!');	
	//var dogovor_id = location.search.substr(1).split('&')[0].split('=')[1];
	//Ext.create('extapp.store.storeDogovorStatus').load();
	//Ext.create('extapp.view.Agreement', {dogovor_id: dogovor_id});
	//Ext.create('extapp.view.DateCalc').show();
	//dogovor_id = location.search.substr(1).split('&')[0].split('=')[1];
	//Ext.create('extapp.view.Agreement').show();
	//Ext.openDogovor(dogovor_id);
	//dogovor_id = location.search.substr(1).split('&')[0].split('=')[1];
	dogovor_id = 1300; //12899;
	/*
	Ext.create('extapp.view.Agreement', { 
	renderTo: 'ext-body',
	constrain: true,
	dogovor_id: dogovor_id });
	*/
	extapp.getApplication().openDogovor(dogovor_id);
}

/*
function asyncStoreLoad(storeId)
{
	var store = Ext.data.StoreManager.lookup(storeId);
	store.setAsynchronousLoad(true);
	if (! store.isLoaded())	store.load();
}
*/

function test1() 
{
	extapp.getApplication().openTreeFPArticle();
}

function kalyaka()
{
	Ext.create('extapp.view.comboDialog1',
	{
		renderTo: 'ext-body',
		constrain: true,
	}).show();	
}

function test2() 
{
	Ext.create('extapp.view.selectTree',
	{
		renderTo: 'ext-body',
		constrain: true,
		options:
		{
			//edit: true,
			windowTitle: 'ОКВЭД',
			store: 'storeOKVED',
			parentField: 'parent_code',
		},
	}).show();	
}

function test3() 
{
	Ext.create('extapp.view.selectTree',
	{
		renderTo: 'ext-body',
		constrain: true,
		options:
		{
			//edit: true,
			windowTitle: 'ОКПД',
			store: 'storeOKPD',
			parentField: 'parent_code',
		},
	}).show();	
}

function test4() 
{	
	extapp.getApplication().openTreePFHDArticle();
	/*
	Ext.create('extapp.view.selectTree',
	{
		renderTo: 'ext-body',
		constrain: true,
		options:
		{
			//edit: true,
			windowTitle: 'Статьи ПФХД',
			store: 'storePFHDArticle',
			codeField: 'code_asu_pfhd',
			parentField: 'parent_id',
		},
		columns: 
		[
			{
				flex: 0,
				dataIndex: 'y',
				text: 'Год',
				editor: 
				{
					xtype: 'numberfield',
				}
			},
		],		
	}).show();	
	*/
}

function test5() 
{
	Ext.create('extapp.view.selectTree',
	{
		renderTo: 'ext-body',
		constrain: true,
		options:
		{
			//edit: true,
			windowTitle: 'Статьи ПФХД 2016',
			store: 'storePFHDArticle2016',
			codeField: 'code_asu_pfhd',
			parentField: 'parent_id',
		},
	}).show();	
}

function test6() 
{
	Ext.create('extapp.view.selectTree',
	{
		renderTo: 'ext-body',
		constrain: true,
		options:
		{
			//edit: true,
			windowTitle: 'Статьи ПФХД 2017',
			store: 'storePFHDArticle2017',
			codeField: 'code_asu_pfhd',
			parentField: 'parent_id',
		},
	}).show();	
}

function test7() 
{
	extapp.getApplication().openContragent();
}

function test8() 
{
	extapp.getApplication().openDogovorList();
}
//Ext.create('extapp.view.DateCalc').show();

window.addEventListener("load", function(event)
{
	//alert(document.width);
	var waitExtJSTimer = setInterval(function()
	{
		if (window.extapp)
		{
			//test8();
			clearInterval(waitExtJSTimer);
		}
	},
	100);
	
});

function testuser() 
{
	alert(window.extapp.userId);
}

function cher() {
	
	var store = Ext.create('Ext.data.Store', {
		fields: ['id', 'kvr','article_type', 'article_name', 'v','v1','v2','v3','v4','v5','v6','v7','v8','v9','v10','v11','v12'],
//		groupField: 'article_type',
		proxy: {
			type: 'ajax',
			url: '/cher.php',
			reader: {
				  type: 'json'
				 ,rootProperty: 'root'
			}
		}
	});	
	store.load();
	store.on('update',fnUpdate,store);
	
	var noFire = false;	
	function fnUpdate(store,record,operation) {
//		console.log("fnUpdate",store);
		if (noFire) {
			return;
		}
		noFire = true;
		store.each(function(rec) {
			rec.set('v2',123);
		});
		noFire = false;
	}
	var editor = Ext.create('Ext.grid.plugin.CellEditing', {
		clicksToEdit: 2
	});
	
	editor.on('beforeedit',function( editor, context, eOpts) { 
		var may = context.record.get('may_edit');
		console.log("may=",may);
		if (!may) return false;
	});
	
  

	Ext.create('Ext.grid.Panel', {
		title: 'Финанс результат Сметы!',
		store: store,
		columns: [
			 { text: 'id', dataIndex: 'id'}
			,{ text: 'Вид', dataIndex: 'article_type', flex: 2 }
			,{ text: 'КВР', dataIndex: 'kvr', flex: 2}
			,{ text: 'Статья', dataIndex: 'article_name', flex: 2}
			,{ text: 'Итого', 	dataIndex: 'v', flex: 1,summaryType: 'sum' }
			,{ text: 'Остаток', 	dataIndex: 'v0', summaryType: 'sum' }
			,{ text: 'Январь', 	dataIndex: 'v1',  editor: {xtype: 'textfield'}, summaryType: 'sum' }
			,{ text: 'Февраль', dataIndex: 'v2',  editor: {xtype: 'textfield'}, summaryType: 'sum' }
			,{ text: 'Март', 	dataIndex: 'v3',  editor: {xtype: 'textfield'}, summaryType: 'sum' }
			,{ text: 'Апрель', 	dataIndex: 'v4',  editor: {xtype: 'textfield'}, summaryType: 'sum' }
			,{ text: 'Май', 	dataIndex: 'v5',  editor: {xtype: 'textfield'}, summaryType: 'sum' }
			,{ text: 'Июнь', 	dataIndex: 'v6',  editor: {xtype: 'textfield'}, summaryType: 'sum' }
			,{ text: 'Июль', 	dataIndex: 'v7',  editor: {xtype: 'textfield'}, summaryType: 'sum' }
			,{ text: 'Август', 	dataIndex: 'v8',  editor: {xtype: 'textfield'}, summaryType: 'sum' }
			,{ text: 'Сентябрь',dataIndex: 'v9',  editor: {xtype: 'textfield'}, summaryType: 'sum' }
			,{ text: 'Октябрь', dataIndex: 'v10', editor: {xtype: 'textfield'}, summaryType: 'sum' }
			,{ text: 'Ноябрь', 	dataIndex: 'v11', editor: {xtype: 'textfield'}, summaryType: 'sum' }
			,{ text: 'Декабрь', dataIndex: 'v12', editor: {xtype: 'textfield'}, summaryType: 'sum' }
		],
/*	    plugins: {
			ptype: 'rowediting',
			clicksToEdit: 2
		},		
*/		plugins:[editor],
		features: [  {groupHeaderTpl: 'Группировка {name}', ftype:'groupingsummary'} ],
		renderTo:  'ext-body'
	});
}

</script>
<button onclick="kalyaka();">Каляка</button>
<button onclick="cher();">Чернов Проверка</button>
<button onclick="extapp.getApplication().openDogovorList();">Договоры</button>
<button onclick="extapp.getApplication().openZayavList();">Заявки</button>
<button onclick="extapp.getApplication().openDogovorList2();">Тест</button>
<button onclick="ura();">Простой тест!!!</button>
<button onclick="test();">Договор</button>
<button onclick="testuser();">Пользователь</button>
<br><br>
<button onclick="test1();">ФП статья</button>
<button onclick="test2();">ОКВЭД</button>
<button onclick="test3();">ОКПД</button>
<button onclick="test4();">Статьи ПФХД</button>
<button onclick="test5();">Статьи ПФХД 2016</button>
<button onclick="test6();">Статьи ПФХД 2017</button>
<br><br>
<button onclick="extapp.getApplication().openProrektor();">Проректоры</button>
<button onclick="extapp.getApplication().openZFOType();">Тип ЦФО</button>
<button onclick="extapp.getApplication().openASUPFHDDivision();">Подразделение АСУ ПФХД</button>
<button onclick="extapp.getApplication().openZFO();">ЦФО</button>
<br><br>
<button onclick="extapp.getApplication().openFPYear();">ФП Годы</button>
<button onclick="extapp.getApplication().openASUPFHDActivityDirection();">Направления деятельнсти АСУ ПФХД</button>
<button onclick="extapp.getApplication().openFinSource();">Источники финансирования</button>
<button onclick="extapp.getApplication().openZFOLimit();">Лимиты ЦФО</button>
<button onclick="extapp.getApplication().openKOSGUFPLimit();">ПФХД</button>
<br><br>
<button onclick="extapp.getApplication().openFPObjectType();">Типы ФП объектов</button>
<button onclick="extapp.getApplication().openFPObject();">ФП объекты</button>
<br><br>
<button onclick="extapp.getApplication().openPeriod();">Периоды</button>
<button onclick="extapp.getApplication().openPlaceMethod();">Способы размещения</button>
<button onclick="extapp.getApplication().openPlanGrafic();">План-график</button>
<button onclick="extapp.getApplication().openGZOKEI();">ОКЕИ</button>
<button onclick="extapp.getApplication().openUnits();">Единицы измерения</button>
<button onclick="extapp.getApplication().openDogovorStatus();">Статусы договоров</button>
<button onclick="extapp.getApplication().openZayavStatus();">Статусы заявок</button>
<button onclick="extapp.getApplication().openKVR();">КВР</button>
<button onclick="extapp.getApplication().openHoliday();">Праздники</button>
<button onclick="extapp.getApplication().openPlanPeriod();">Плановые периоды</button>
<button onclick="extapp.getApplication().openStatPFHDVer();">Версии ПФХД</button>
<button onclick="extapp.getApplication().openPlanDohodVer();">Версии планового периода</button>
<button onclick="extapp.getApplication().openPlanDohod();">Планы доходов</button>
<br><br>
<button onclick="test7();">Контрагенты</button>
<br><br><b>ACУ ПФХД</b><br><br>
<button onclick="extapp.getApplication().openPFHDParam();">Реквизиты</button>
<button onclick="extapp.getApplication().openPFHDDopPokaz();">Доп. показатели</button>
<button onclick="extapp.getApplication().openPFHDActivityKind();">Виды ФО</button>
<button onclick="extapp.getApplication().openPFHDActivityGoal();">Цели деятельности</button>
<button onclick="extapp.getApplication().openPFHDActivityKindPFHD();">Виды деятельности ПФХД</button>
<button onclick="extapp.getApplication().openPFHDServiceList();">Перечень услуг</button>
<button onclick="extapp.getApplication().openPFHDFinState();">Показатели ФСУ</button>
<br/>
<br/>
<button onclick="extapp.getApplication().openPFHDExpenseDirection();">Показатели расходов</button>
<button onclick="extapp.getApplication().openPFHDActivityDirection();">Направления расходов</button>			
<button onclick="extapp.getApplication().openPFHDProgramEvent();">Мероприятия - справочник</button>
<button onclick="extapp.getApplication().openPFHDInstitutionProgram();">Цели и задачи</button>
<button onclick="extapp.getApplication().openPFHDInstitutionSubProgram();">1 уровень</button>
<button onclick="extapp.getApplication().openPFHDCPPKProgram();">2 уровень</button>

<br><br>
<script>

function ura() {
	console.log("ura!");
	var root = {
			text: 'Root5345',
			records: [
				{
					text: 'План 2017',
					leaf: true,
					checked: true
				},
				{
					text: 'План будущих лет',
					checked: false,
					expanded: true,
					records: 
					[
						{
							text: '2018',
							leaf: true,
							checked: false,
						},
						{
							text: '2019',
							leaf: true,
							checked: false,
						},
					]
				},
				{
					text: '2018',
					checked: false,
					leaf: true,
				}
			]
	};
	
	var store = Ext.create('Ext.data.TreeStore', {
		root: root,
		defaultRootProperty: 'children'
	});
	
	var panel = Ext.create('Ext.tree.Panel', {
		renderTo: 'ext-body',
		xtype: 'check-tree',
		title: 'Простой тест',
		width: 300,
		height: 250,
		store: store,		
	});
	panel.show();
}
</script>
<!--
<script>
Ext.onReady(function()
{
	// иначе не работает Ext.data.StoreManager.lookup('storeDogovorStatus');
	/*
	for (store in extapp.store) {
		var obj_name = 'extapp.store.'  + store;
		Ext.create(obj_name);
	}
	*/
	
	//alert('Библиотека подключена!');
	var dogovor_id = location.search.substr(1).split('&')[0].split('=')[1];	
	Ext.create('extapp.store.storeDogovorStatus').load();
	Ext.create('extapp.view.Agreement', {dogovor_id: dogovor_id});	
	
	//var store = Ext.create('extapp.store.storeDogovor');
	/*
	var viewModel = new Ext.app.ViewModel({
		links: {
			theDogovor: {
				type: 'extapp.model.modelDogovor',
				id: dogovor_id
			}
		}
	})	;
	*/
	/*	
	Ext.create('extapp.view.Agreement',  {
		viewModel:viewModel,
		renderTo: Ext.getBody()
	});
	*/		
	/*
	store.load({
		params: {
			id: dogovor_id
		},
		callback: function(records, operation, success) {
			// the operation object
			// contains all of the details of the load operation
			//console.log(records);
			//alert(records.length);
			Ext.create('extapp.view.Agreement',  {
				viewModel:{
					data: records[0].data
				},
				renderTo: Ext.getBody()
			});
			//me.getView().setViewModel();
			//console.log("new",me.getView().viewModel);
			//console.log("rec",records[0]);
			//console.log("new",me.viewModel);

			// me.lookupReference('subject').setValue(avd.parseObj(records[0].data));
			//me.lookupReference('dogovor_n').setValue(records[0].data.dogovor_num);
			//me.lookupReference('fp_year').setValue(records[0].data.fp_year);
		}
	});
	*/
});
</script>
-->

<!--
<script id="microloader" type="text/javascript" src="bootstrap.js"></script>
-->

</div>

<?php 
	//include 'template/footer.php'; 
	require $_SERVER['DOCUMENT_ROOT'] . '/template/footer.php';
?>