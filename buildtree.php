<?php
error_reporting (E_ALL | E_STRICT);
require 'modules/sql_connect.php';
require 'modules/sql.php';
require 'modules/avdfunctions.php';

db_connect();

if (isset($_REQUEST['table']))
	$table = sql_escape($_REQUEST['table']);
else
	$table = 'okved';

$columns = sql_table_columns($table);

if (isset($_REQUEST['parentfield']))
	$parentfield = $_REQUEST['parentfield'];
else
	$parentfield = 'parent_id';

if (isset($_REQUEST['idfield']))
	$idfield = $_REQUEST['idfield'];
else
	$idfield = 'id';

if (isset($_REQUEST['nodeidfield']))
	$nodeidfield = $_REQUEST['nodeidfield'];
else
	$nodeidfield = 'id';

$table = sql_escape($table);

if ($_SERVER['REQUEST_METHOD'] === 'POST') 
{
	//ob_start();
	// Получение данных с клиента на сервер
	//var_dump($_POST);
	//echo "$table\n\n";
	//echo "table = $table\n";
	$json = file_get_contents('php://input');
	$srcdata = json_decode($json);
	if (is_array($srcdata))
		$dataarray = $srcdata;
	else
		$dataarray = array('DATA' => $srcdata);
	$message = "";
	//$message .= "$idfield<br>";
	$success = true;
	$records = Array();
	sql_query("START TRANSACTION");
	//ob_start();	var_dump($dataarray); $message .= '--------<pre>' . ob_get_clean() . '</pre>--------\n';
	foreach($dataarray as $dataindex => $data)
	{
		$idvalue = 0;
		$nodeidvalue = 0;
		$fieldlist = array();
		$blockedfieldlist = array();
		$valuelist = array();
		foreach($data as $i => $v)
		{
			//$message .= "ПФФФФФФ11111111111!!! $idfield <br>";
			if ($i == $idfield) 
			{
				$idvalue = sql_escape($v);
				//$message .= "ПФФФФФФ11111111111!!! $nodeidvalue <br>";
				//$message .= "$idfield == $idvalue<br>";
			}
			else
				if (isset($columns[$i])) 
				{
					//
					if ($i == $nodeidfield) 
					{
						$nodeidvalue = sql_escape($v);
						//$message .= "ПФФФФФФ!!! $nodeidvalue <br>";
					}
					//
					if ($columns[$i]['type'] == 'date')
					{
						$v = date("Y-m-d H:i:s", $v);
						$data->$i = $v;
					}
					//if (!is_null($v))
					$fieldlist[] = $i;
					if ($v)
					{
						if ($i == $parentfield && $v == 'root')
							$valuelist[] =  'NULL';
						else
							$valuelist[] = "'" . sql_escape($v) . "'";
					}
					else
					{
						if ($columns[$i]['type'] == 'tinyint')
							$valuelist[] = '0';
						else if ($columns[$i]['type'] == 'int')
							$valuelist[] = 'NULL';
						else
							$valuelist[] = 'NULL';
					}
				}
				else
					$blockedfieldlist[] = $i;
		}
		//echo "$idfield = $idvalue\n";
		$exprlist = array();		
		for ($i = 0; $i < count($fieldlist); $i++)
		{
			$exprlist[] = "${fieldlist[$i]} = ${valuelist[$i]}";
			//NB!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
			//$message .= "${exprlist[$i]}<br>";
		}
		$sql = '';
		$id_op = '';
		if ($idvalue >= 0)
		{
			if ($exprlist)
			{
				if ($idvalue > 0)
				{
					$sql = "UPDATE $table SET " . implode(", ", $exprlist) . " WHERE $idfield = '$idvalue'";
					//$message .= "11111111111111111<br>";
					$id_op = ", $idfield = '$idvalue'";
				}
				else
				{
					$sql = "UPDATE $table SET " . implode(", ", $exprlist) . " WHERE $nodeidfield = '$nodeidvalue'";
					//$message .= "22222222222222222<br>";
					$id_op = ", $nodeidfield = '$nodeidvalue'";
				}
				$message .= "Обновление таблицы $table";
			}
			else
				if (! $blockedfieldlist)
				{
					$sql = "DELETE FROM $table WHERE $idfield = '$idvalue'";
					$message .= "Удаление из таблицы $table";
					$id_op = ", $idfield = '$idvalue'";
				}
		}
		else
		{
			$sql = "INSERT INTO $table (" . implode(", ", $fieldlist) . ") VALUES (" . implode(", ", $valuelist) . ")";
			$message .= "Добавление в таблицу $table";
		}
		//NB!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
		//$message .= "<br><br>$sql<br><br>";
		//$success = $success && sql_query($sql);
		/**/
		$r = 0;
		if ($sql)
		try 
		{
			$r = NULL;
			/// '\n---------------\n' . $sql . '\n---------------\n';
			//$r = sql_execute('1111111111111' . $sql . ' errrrr');
			$r = sql_execute($sql);

			$success = $success && $r;
			// insert operation			
			//if ($success && $idvalue < 0) 
			if ($r && $idvalue < 0)
			{
				//$message .= '77777777777\n';
				$data->clientId = $idvalue;
				$data->$idfield = sql_last_id();
			}
			//$message .= ", $idfield: " . $data->$idfield;
			//$message .= ",---------------> $idvalue <br>";
			$message .= $id_op;
			if ($r)
				$message .= ' - успешное выполнение!\n';
			else
				$message .= ' - ошибка:\n' . sql_error() . '\n';
		} 
		catch (Exception $e) 
		{
			$success = false;
			$message .= $e->getMessage() . '\n';
		}		
		if ($r) 
			$records[] = $data;
		//else 
			//$message = '';
		/**/
		//echo "Результат: $success \n";
		//echo json_encode(['success' => false, 'message' => 'SOME ERROR!']);
		//echo 'Результат: ' . $result . "\n";
		//avd_printarrayvalues_r($data);
	}
	//$message = ob_get_clean();
	//ob_get_clean();
	$message = str_replace('\n', '<br>', $message);
	//$success = true;
	//$success = false;
	if ($success) 
		sql_query("COMMIT");
	else 
		sql_query("ROLLBACK");	
	echo json_encode(array('success' => $success, 'message' => $message, 'records' => $records /*, 'records' => [['id' => $idvalue, 'subject' => 'My PHP SUBJECT']]*/ ));
	exit();
}

//------------------------------------------------------------------------------------------------------------------------------------------------
function getnode($root)
{
	global $table, $columns, $parentfield, $idfield, $nodeidfield;

	//$sqltext = "SELECT * FROM $table WHERE (IFNULL($parentfield, '') = '$root' OR $parentfield = 0 AND '$root' = '')";
	$sqltext = "SELECT * FROM $table WHERE IFNULL($parentfield, '') = '$root'";
	
	foreach ($_REQUEST as $i => $v)
	{
		$i = sql_escape($i);
		$v = sql_escape($v);
		if (isset($columns[$i])) 
		{
			if ($i != $parentfield)
				$sqltext .= " AND $i = '$v'";			
		}
	}
	//die ($sqltext);
	
	if (isset($_REQUEST['query']))
	{
		$q = sql_escape($_REQUEST['query']);
		$sqltext .= " AND $queryfield like '%$q%'";
	}

	$r = sql_rows($sqltext);
	
	$rr = Array();
	
	foreach ($r as $n => $row)
	{
		$newrow = (object) Array();
		foreach ($row as $i => $v)
		{
			if ($i == $parentfield)
				if ($v === 0)
					$v = NULL;
			$newrow->$i = $v;
		}
		$items = getnode($newrow->$nodeidfield);
		$newrow->leaf = ($items) ? false : true;
		if ($items)
			$newrow->records = $items;
		$rr[$n] = $newrow;
	}	
	return $rr;
}
//------------------------------------------------------------------------------------------------------------------------------------------------

//die($parentfield);

if (isset($_REQUEST['root']))
	$root = $_REQUEST['root'];
else
	$root = '';

if ($root == 'root')
	$root = '';

$r = getnode($root);
//die($root);
 
/*
foreach ($r as &$row) 
	if (isset($row['d'])) 
		$row['dd'] =  $row['d'] ? new DateTime($row['d']) : null;
*/

//var_dump($r);
//var_dump([$_REQUEST]);
if (isset($_REQUEST['test']))
{
	echo '<HTML><BODY>'; 
	//avd_printarrayvalues_r($r); 
	echo '<PRE>'; var_dump($r); echo '</PRE>'; 
	echo '</BODY></HTML>'; 
}	
else 
	echo json_encode(['records' => $r]);
?>