<?php
error_reporting(E_ALL | E_STRICT);
require_once($_SERVER['DOCUMENT_ROOT'].'/init.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/modules/save_table_class.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/modules/mailer.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/modules/controls.php');


$user_id = request_numeric_val('u','new');
if (!is_sysadmin()) exit('Доступ к странице закрыт!');

$error = '';
$user_info = sql_select_row("user","id='$user_id'");

//staff.unecon.ru
$staff_user_complete_path = 'https://dekanat.unecon.ru/modules/staff_user_complete/2.0';
$staff_user_id = 0;
$staff_user_fio = 'НЕ ЗАДАНО';

/**/
if($user_id != 'new') {
	$staff_user_id = sql_get_value('staff_user_id', 'user', "id = '$user_id'");
}
$staff_user_fio = sql_get_value("fio","`staff`.v_user","user_id='$staff_user_id'");
/**/

if(isset($_POST['delete_user'])) {
	$user_id = (int) sql_escape($user_id);
	save_log($_SESSION['user_id'], 'user', 3, $user_id);
	sql_query('DELETE FROM user WHERE id='.$user_id);
    header ('Location: /admin/user_spisok.php');
}

if (isset($_POST['sendpass'])) {
	if (is_numeric($user_id) && $user_info['email']) {
		if (!mail::is_valid_email($user_info['email'])) $error = 'В базе установлен некорректный e-mail! ';
		else {
			$new_pass = passgen();
			sql_query("UPDATE `user` SET pass='".md5($new_pass)."', lostpass_link=null, lostpass_fire_time=null, access_send=1 
				WHERE id='$user_id'");
			$s = '
			Доступы от <b><a href="http://'.$_SERVER['SERVER_NAME'].'/">'.$_SERVER['SERVER_NAME'].'</b></a><br>
			Ваш логин <b>'.$user_info['login'].'</b><br>
			Ваш пароль <b>'.$new_pass.'</b><br>
			';
			mail::send($user_info['email'],'Новые доступы от '.$_SERVER['SERVER_NAME'],$s,'priem@finec.ru');
			$_SESSION['USER_MESSAGE']['SUCCESS'] = 'Доступы успешно отправлены на почту: ' . $user_info['email'];
		}
	} else $error = 'Сначала необходимо сохранить e-mail пользователя!';
}

if (isset($_POST['save'])) {
	if (is_numeric($user_id)) $_POST['id']=$user_id;
	/* fix tinyint flags in post */
	$rows = sql_rows("SHOW COLUMNS FROM user");
	$users = sql_rows("SELECT login FROM user");
	if($user_id == 'new') {
		foreach ($users as $key=>$u_login) {
			if(trim($_POST['login']) == trim($u_login['login'])) {
				$_SESSION['USER_MESSAGE']['ERROR'] = 'Пользователь с таким логином уже существует!';

				header("Location: " . $_SERVER['HTTP_REFERER']);

				exit();
			}
		}
	}

	foreach ($rows as $row) {
		if (!isset($_POST[$row['Field']]) && strstr($row['Type'],'tinyint')) $_POST[$row['Field']] = 0;
	}

	/* end fixing */
	$status = save_table::save('user');

	if ($user_id == 'new') $_POST['id']=sql_last_id();

	if ($status['saved']) {
		$_SESSION['USER_MESSAGE']['SUCCESS'] = 'Пользователь успешно сохранен!';
		save_log($_SESSION['user_id'], 'user', 2, $_POST['id']);
		header("Location: /admin/user_edit.php?u=${_POST['id']}");
		exit();
	}
	
	if ($status['errors'] && is_array($status['errors'])) {
		foreach ($status['errors'] as $nRow=>$v) {
			foreach ($v as $nRow=>$err) {
				$error .= $err.'<br>';
			}
		}
	}
	
	$user_info = fill_array_with_post($user_info);
}

//2 - create/edit, 3 - delete
function save_log($user,$table,$op_type,$id){
	$id = (int)$id;
	$cols=sql_table_columns($table);
	$key_name = sql_table_key($table);
	foreach ($cols as $columns){
		$colnames[]='`'.$columns['name'].'`';
	}
	$colsstr=implode(', ', $colnames);
	$sql="select * from $table where `" . $key_name."`=".$id."";
	$row=sql_array(sql_query($sql));
	foreach ($row as &$s) { 
		$s=sql_escape($s);
	}
	$valstr='"'.implode('", "', $row).'"';
	$sql1="insert into log_$table ($colsstr, user_id, operation_type) values ($valstr, $user, $op_type)";
	return sql_query($sql1);
}

$title = (is_numeric($user_id)) ? 'Пользователь: '.$user_info['fio'] : 'Новый пользователь';
require_once($_SERVER['DOCUMENT_ROOT'].'/template/header.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/template/menu.php');

$controls = new controls;
$controls->set_may_edit(is_admin() && !is_ro());
//$zfo_options = array('null'=>'Все');
$zfo_options = sql_to_assoc('select id,name from zfo order by name');
$user_zfo = user_zfo_ids($user_info['id']);

$role_options = array('null'=>'Не выбрано');
$role_options += sql_to_assoc('select id,name from role order by name');
?>
<link rel="stylesheet" type="text/css" href="/css/background<?=$_SESSION["palette"]?>.css">
<script type="text/javascript">
	$(document).ready(function() {
		$('input[name="sendpass"]').click(function(){
			var email = '<?=$user_info['email'] ?>';
			if (!email || email !== $('input[name="email"]').val()) {
				alert('Сначала необходимо сохранить e-mail пользователя!');
				return false;
			} else if (!confirm('Отправить доступы на '+email+'?')) return false;
		});
	});
</script>
<div class="filter_contaner_edit" data-moveable="true">
	<h2 class="kartochka_header"><?=$title?></h2><br>
	<form method="post">
	<ul>
		<li><?php $controls->print_input_label('staff_user_fio',$staff_user_fio,'ФИО (stuff.unecon.ru)'); ?></li>
		<li><?php $controls->print_input_hidden('staff_user_id',$staff_user_id); ?></li>
		<li><?php $controls->print_input_label('fio',$user_info['fio'],'ФИО', array('placeholder'=>'Фамилия Имя Отчество')); ?></li>
		<li><?php $controls->print_input_label('login',$user_info['login'],'Логин', array('placeholder'=>'Имя пользователя')); ?></li>
		<li><?php $controls->print_input_label('email',$user_info['email'],'Почта', array('placeholder'=>'example@gmail.com')); ?></li>
		<li><?php $controls->print_select_label('role_id',$role_options,$user_info['role_id'],'Роль в системе', array('onchange'=>'zfoChange(this); return false;')); ?></li>
		<li>
			<button id="selectZFOButton" type="button" onclick="selectZFO()">Выбрать ЦФО</button><br>
			<select name="user_zfo[]" multiple="multiple" style="display:none">
				<?php foreach($zfo_options as $value=>$name): ?>
					<?php $selected = (in_array($value, $user_zfo)) ? 'selected' : '' ?>
					<option <?=$selected?> value="<?= $value ?>"><?= $name ?></option>
				<?php endforeach; ?>
			</select>				
			<textarea id="selectedZFOList" style="width:100%" readonly></textarea>
		</li>
	</ul>
	<ul class="checkboxes custom-buttons">
		<li><?php $controls->print_checkbox_label('is_actual',$user_info['is_actual'],'Может заходить в систему'); ?></li>
		<li><?php $controls->print_checkbox_label('is_admin',$user_info['is_admin'],'Администратор'); ?></li>
		<li><?php $controls->print_checkbox_label('is_ro',$user_info['is_ro'],'Только для чтения'); ?></li>
		<li><?php $controls->print_checkbox_label('is_ku',$user_info['is_ku'],'Представитель контрактного упр.'); ?></li>
		<li><?php $controls->print_checkbox_label('may_dogovor',$user_info['may_dogovor'],'Редактирование договоров и заявок'); ?></li>
		<li><?php $controls->print_checkbox_label('may_edit_many',$user_info['may_edit_many'],'Множественный выбор строк'); ?></li>
		<li><?php $controls->print_checkbox_label('may_create_zayav',$user_info['may_create_zayav'],'Может создавать заявки'); ?></li>
		<li><?php $controls->print_checkbox_label('may_approve_limit',$user_info['may_approve_limit'],'Может утверждать лимиты'); ?></li>
		<li><?php $controls->print_checkbox_label('may_receive_mail',$user_info['may_receive_mail'],'Может получать e-mail от СУП'); ?></li>
		<li><?php $controls->print_checkbox_label('may_edit_zayav',$user_info['may_edit_zayav'],'Редактирование только заявок'); ?></li>
		<li><?php $controls->print_checkbox_label('may_smeta',$user_info['may_smeta'],'Может видеть сметы'); ?></li>
		<li><?php $controls->print_checkbox_label('may_edit_plat',$user_info['may_edit_plat'],'Может редактировать платежи'); ?></li>
		<?php if($user_id != 'new'): ?>
		<?php endif; ?>
	</ul>
	<div class="cleared"></div>
	<div class="filter_button custom-buttons" style="text-align:center">
		<?php 
		$controls->print_submit('save', "Сохранить");
		$controls->print_submit('sendpass', "Отправить доступы");
		$controls->print_submit('delete_user', 'Удалить пользователя', array('class'=>'custom-buttons delete-submit', 'onclick'=>'return confirmBox(\'Вы действительно хотите удалить пользователя?\')')); ?>
		<div class="error"><br><?=$error ?></div>
	</div>
	
	</form>
</div>

<script type="text/javascript" src="<?=$staff_user_complete_path?>/js.js" charset="UTF-8"></script>
<script>
staff_user_complete.path = '<?=$staff_user_complete_path?>';
</script>
<script src="//cdn.unecon.ru/js/jquery-ui-1.10.3.custom/development-bundle/ui/minified/jquery.ui.dialog.min.js" type="text/javascript"></script>
<script src="/js/avd.js"></script>
<script type="text/javascript">

	'use strict';	
	
	function zfoChange(el) 
	{
		var src_select = document.querySelector('select[name="user_zfo[]"]');
		var parent = src_select.parentNode;
		if (parent) 			
			if (el.value == 6) 
			{
				parent.style.display = 'block';
			//	src_select.removeAttribute('disabled');
			} 
			else 
			{
				parent.style.display = 'none';
				src_select.value = 'null';
			//	src_select.setAttribute('disabled', 'disabled');
			}
	}

	$(document).ready(function()
	{
		staff_user_complete.path = '<?=$staff_user_complete_path?>';
		var input_fio = $('#id_staff_user_fio');
		var input_id = $('#id_staff_user_id');
		staff_user_complete(input_fio,input_id);
	});
	
	//AVD
	//-----------------------------------------------------------
	
	function fillSelectedZFOList()
	{
		var ta = document.getElementById('selectedZFOList');
		var src_select = document.querySelector("select[name='user_zfo[]']");
		var o = src_select && src_select.options;
		var texts = [];	
		for (var i = 0; i < o.length; i++) {
			if (o[i].selected)  texts.push(o[i].text);
		}
		ta.value = texts.join(', ');
		//alert(ta.tagName);
	}

	window.addEventListener('load', function() 
	{
		var rie = document.getElementsByName("role_id")
		if (rie && rie.length)
			zfoChange(rie[0]);
		fillSelectedZFOList();
	});
	
	function selectZFO()
	{
		var target = document.getElementById("selectZFOButton");
		var src_select = document.querySelector("select[name='user_zfo[]']");
		avd.selectDialog(target, src_select, setCheckedValues);
		function setCheckedValues(checkedValues)
		{
			//alert(checkedValues.join(', '));
			//var src_select = document.querySelector("select[name='user_zfo[]']");
			var o = src_select && src_select.options;
			if (o)
			{
				//var t = '';
				for (var i = 0; i < o.length; i++) 
				{
					var checked = (checkedValues.indexOf(o[i].value) != -1);
					o[i].selected = checked;
				}
				fillSelectedZFOList();
			}			
		}
	
	}
	
	//-----------------------------------------------------------
	
</script>
<?php
require_once($_SERVER['DOCUMENT_ROOT'].'/template/footer.php');
?>