<?php
error_reporting(E_ALL | E_STRICT);
require_once($_SERVER['DOCUMENT_ROOT'].'/init.php');
if (!is_sysadmin()) exit('Доступ к странице закрыт!');

$title = 'Список пользователей';
require_once($_SERVER['DOCUMENT_ROOT'].'/template/header.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/template/menu.php');
?>

<div class="header-tools"><h1><?=$title?></h1></div>
<div class="table_contaner">

<?php

if(isset($_POST['delete_users']) && array_key_exists('delete_from_list',$_POST)) {
    $users = $_POST['delete_from_list'];

    foreach ($users as $user) {
        $user_id = (int) sql_escape($user);

        sql_query('DELETE FROM user WHERE id='.$user_id);
    }

    $_SESSION['USER_MESSAGE']['SUCCESS'] = 'Поьзователи успешно удалены';
}
    
$sql = "SELECT * FROM `user` ORDER BY login";
$res = sql_query($sql);
if (sql_count_rows($res)>0) : ?>
    <form method="POST">
        <input type="button" value="Добавить пользователя" onclick="location='user_edit.php?u=new'"><br><br>
        <div>
        <table class="td-table fixed" style="width:100%; border-collapse: collapse; border-spacing: none;" border="0">
            <thead>
                <tr>
					<!--
                    <th style="width:5%;">&nbsp;</th>
                    <th style="width:5%;">№</th>
                    <th style="width:15%;">Логин</th>
                    <th style="width:20%;">ФИО</th>
                    <th style="width:20%;">Email</th>
                    <th style="width:15%;">Администратор</th>
                    <th style="width:10%;">Только чтение</th>
                    <th style="width:10%;">Заблокирован</th>
                    <th style="width:10%;">Доступы отправлены</th>
					<th style="width:10%;">Роль</th>
					-->
                    <th style="width:5%;">ѵ</th>
                    <th style="width:5%;">№</th>
                    <th>Логин</th>
                    <th>ФИО</th>
                    <th>Email</th>
                    <th style="width:7%;">Админ.</th>
                    <th style="width:7%;">Только чтение</th>
                    <th>Заблокирован</th>
                    <th style="width:10%;">Доступы отправлены</th>
					<th>Роль</th>
                </tr>
            </thead>
            <tbody>
            <?php 
			$role_rows = sql_rows("SELECT * FROM role");
			$roles = array();
			for($i = 0; $i < count($role_rows); $i++) {
				$roles[$role_rows[$i]['id']] = $role_rows[$i]['name'];
			}
            $n=0;
            while ($row = sql_array($res)) {
				$role_name = ((int)$row['role_id'] != 'NULL') ? $roles[$row['role_id']] : "";
                $n++;
                echo '<tr>';
                echo '<td align="center"><input type="checkbox" name="delete_from_list[]" value="'.$row['id'].'" /></td>';
                echo '<td align="center">'.$n.'</td>';
                echo '<td><a href="/admin/user_edit.php?u='.$row['id'].'">'.$row['login'].'</a></td>';
                echo '<td>'.$row['fio'].'</td>';
                echo '<td>'.$row['email'].'</td>';
                echo '<td align="center">'.da_net_name($row['is_admin']).'</td>';
                echo '<td align="center">'.da_net_name($row['is_ro']).'</td>';
                $actual = ($row['is_actual']) ? '' : '<b class="error">Заблокирован</b>';
                echo '<td align="center">'.$actual.'</td>';
                echo '<td align="center">'.da_net_name($row['access_send']).'</td>';
				echo '<td align="center">'. $role_name.'</td>';
                echo '</tr>';
            } ?>
            </tbody>
        </table>
        </div>
        <br><input type="submit" name="delete_users" value="Удалить выбранных"  /><br><br>
    </form>


<?php endif;?>
</div>

<?php
require_once($_SERVER['DOCUMENT_ROOT'].'/template/footer.php');
?>