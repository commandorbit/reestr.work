EditBox.init = function (me) {
        
    var fp_aticle = me.box.querySelector('select[name="fp_article_id"]');
    var fp_object = me.box.querySelector('select[name="fp_object_id"]');
    console.log("Ebox init");
    if (fp_aticle && fp_object) {
		fp_article_init()
    }
	
	var kontragent_1c =  me.box.querySelector('select[name="Контрагенты_Ref"]');
	if (kontragent_1c) {
		//kontragent_1c.style.display = 'none';
		var kontragent_1c_input = document.createElement("input");
		kontragent_1c_input.type = "text";
		kontragent_1c.parentNode.insertBefore(kontragent_1c_input,kontragent_1c);
		var kontr_list = [];
		for (var i=0; i < kontragent_1c.options.length; i++) {
			kontr_list.push({label:kontragent_1c.options[i].text,  value:kontragent_1c.options[i].value});
		}
		$(kontragent_1c_input).autocomplete({ minLength: 5, source:kontr_list,  select: function( event, ui ) {
			
				kontragent_1c.value = ui.item.value;
				console.log(ui.item.label);
				kontragent_1c_input.value = ui.item.label;
				event.preventDefault();
		}});
	}
	
	function fp_article_init() {
        console.log("fp_Artic");
        fp_aticle.onchange = function() {
            var fp_article_id = this.value;

            $.ajax({
                type: "GET",
                url: "/td/fp_article_object.php?fp_article_id=" + fp_article_id,
                success: function(data) {
                            data = JSON.parse(data);

                            var options = '<option value="_null_">(Пусто)</option>';

                            if(data.hasOwnProperty('status') && data.status == 'error') {
                                fp_object.setAttribute('disabled', 'disabled');
                                fp_object.innerHTML = options;
                            } else {
                                if(fp_object.hasAttribute('disabled')) {
                                    fp_object.removeAttribute('disabled');
                                }

                                var lastSelected = null;
                                if  (fp_object.options[fp_object.selectedIndex]) {
                                     lastSelected = fp_object.options[fp_object.selectedIndex].text;
                                }
                                
                                fp_object.innerHTML = '';

                                for(var key in data) {
                                    var selected = '';

                                    if(lastSelected === data[key].name) {
                                        selected = 'selected';
                                    }

                                    options += '<option value="'+ data[key].id +'" '+selected+'>'+ data[key].name +'</option>';
                                }

                                fp_object.innerHTML = options;
                            }
                        }
            });
        }
	
	}
    me.box.onShow = function () {
		if (kontragent_1c) {
			kontragent_1c.style.display = 'none';
			kontragent_1c_input.value = '';
			var n = kontragent_1c.selectedIndex;
			if (n !== -1) {
				kontragent_1c_input.value = kontragent_1c.options[n].text;
			}
		}
        if(fp_aticle) {
            var evt = document.createEvent("HTMLEvents");
            evt.initEvent("change", false, true);
            fp_aticle.dispatchEvent(evt);
        }
    }   
    return true;
}