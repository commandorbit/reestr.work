<?php

class Pivdata {
	public $fld_names;
	public $sql;
	
	public function __construct($sql) {
		$this->sql = $sql;
	}
	
	public function load_pivot() {
		echo "<script>"."\n";
		sql_set_charset('utf8');
		$this->load_progress($this->sql);
		echo "var pivot = new libUnicorn.Pivot(d,flds,document.getElementById('PIVOT')); \n" ;
		echo "</script>"."\n";
	}

	public function load_table($sql) {
		echo "<script>"."\n";
		echo "var div = document.querySelector('div.data-table-wrap');";
		$this->load_progress($sql);
		echo "(new DataTable(d,flds,div)).draw(); \n" ;
		echo "</script>"."\n";
	}

	protected function stip_fields($res) {
		$flds = array();
		$ncols = sql_fields($res);
		for ($i = 0; $i < count($ncols); $i++) {
			$column_name  = $ncols[$i]->name;
			$column_type  = $ncols[$i]->type;
			$label = isset($this->fld_names[$column_name]) ? $this->fld_names[$column_name] : $column_name;
			$flds[$column_name]= array("name"=>$column_name,"type"=>$column_type,"label"=>$label);
		}
		return $flds;
	}

	protected function load_progress($sql) {
		$res = sql_query($sql);
		$num_rows = sql_count_rows($res);		
		$flds = $this->stip_fields($res);
		$numfields = array("sum", "sum_zayav", "sum_ost","sum_ost_2016","sum_1","sum_2","sum_diff");//_ora_numeric_fields($res);
		$rows = array();
		echo "libUnicorn.progressBar.start('Идет загрузка');\n";
		echo "var d=[];";
		$i=0;
		while ($row = sql_array($res))  {
			if ($i % 100 == 0) {
				echo "\n libUnicorn.progressBar($i,$num_rows);\n";
				echo "</script>\n<script>\n"; 
			}
			// приводим числовые поля к числам для js
			foreach ($numfields as $f) {
				if(!isset($flds[$f])) continue;
				if ($row[$f]!==null) $row[$f]+=0;
			}
			echo "d[$i] =" . json_encode($row) . ";\n";
			$i++;
		}
		echo "libUnicorn.progressBar($i,$num_rows);\n";
		echo "var flds = " . json_encode($flds) .";\n";
	}
}

