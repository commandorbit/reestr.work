SELECT 
d.id as dogovor_id,
d.dogovor_num,
d.`d`, 
d.`fp_year`, 
dstat.name as `dogovor_status`, 
dt.name as `document_type`, 
yn.name as concluded_ku, 
z.name as zfo, 
d.contragent_id,
d.date_start,
d.date_end,
c.name as `contragent`, 
pg.name as `plan_grafic`, 
pm.name as `place_method`,
fpa.name as fp_article,
fs.booker_fin_source as booker_fin_source,
fs.name as fin_source,
vr.code as vid_rashodov_code,
fpa.kosgu,
ds.amount as sum,
ds.sum_ost,
ds.sum_zayav
FROM `dogovor` d
LEFT JOIN v_dogovor_sostav_rep ds ON ds.id = d.id
LEFT JOIN zfo z ON z.id = d.zfo_id
LEFT JOIN document_type dt ON dt.id = d.document_type_id
LEFT JOIN contragent c ON c.id = d.contragent_id
LEFT JOIN dogovor_status dstat ON dstat.id = d.dogovor_status_id
LEFT JOIN plan_grafic pg ON pg.id = d.plan_grafic_id
LEFT JOIN place_method pm ON pm.id = d.place_method_id
LEFT JOIN yes_no yn ON yn.id = d.concluded_ku
LEFT JOIN fp_article fpa ON fpa.id = ds.fp_article_id 
left join vid_rashodov vr on fpa.vid_rashodov_id = vr.id
LEFT JOIN fin_source fs ON fs.id = ds.fin_source_id 
WHERE d.zfo_id IN (%ZFO_ID_IN%) OR fs.zfo_id IN (%ZFO_ID_IN%)