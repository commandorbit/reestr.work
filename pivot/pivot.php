<?php
$title = "Сводный очет";
$time = microtime(true);
require_once $_SERVER['DOCUMENT_ROOT'] . '/init.php';

//Дефолтный тип, чтобы не падать на ошибку в случае запроса без параметра "type"
define("DEFAULT_PIVOT_TYPE", 'zayav_summary');
//Тип содержимого по которому нужно строить отчеты
$pivot_type = (isset($_GET['type'])) ? sql_escape($_GET['type']) : DEFAULT_PIVOT_TYPE;
require __DIR__ . "/pivdata.php";
require_once($_SERVER['DOCUMENT_ROOT'].'/template/header.php');

//Массив с русскими именами полей
$fld_names = getFields();
$sql_file = null;
$replacement_sql_parts = array();
$referer = (isset($_SERVER['HTTP_REFERER'])) ? $_SERVER['HTTP_REFERER'] : 'index.php';
function getFields() {
	$fields = array(	
					"dogovor_status"=>"Статус договора", 
					"d"=>"Дата заявки", 
					"zayav_status"=>"Статус заявки", 
					"sum"=>"Сумма", 
					"reestr_d"=>"Дата реестра", 
					"month_name"=>"Месяц",
					"reestr_year"=>"Год", 
					"plat_d"=>"Дата платежки", 
					"fp_article"=>"ФП статья", 
					"vid_rashodov_code"=>"Вид расходов",
					"kosgu"=>"КОСГУ", 
					"contragent"=>"Контрагент", 
					"dogovor_id"=>"Договор ID", 
					"zfo"=>"ЦФО",
					"fin_source"=>"Источник финансирования", 
					"period"=>"Период",
					"fp_object"=>"ФП объект",
					"month_id"=>"Месяц", 
					"d_year"=>"Год", 
					"plat_type"=>"Тип платежа",
					"booker_fin_source"=>"ИФ бух.", 
					"bank_acc"=>"Расч. счет",
					"zayav"=>"Заявка ID",
					"zfo_id"=>"ЦФО",
					"fp_article_id"=>"ФП статья",
                    "pfhd_article_id"=>"ПФХД статья",
                    "pfhd_fs"=>"ПФХД ИФ",
                    "sum_diff"=>"Отклонение (А-B)",
					"pfhd_y"=>"Год ПФХД",
                    "fp_year"=>"Год ФП",
                    "sum_ost"=>"Остаток",
                    "sum_zayav"=>"Сумма заявок",
                    "place_method"=>"Способ размещения",
                    "plan_grafic"=>"План-график",
                    "plan_grafic_id"=>"План-график",
                    "date_start"=>"Дата начала",
                    "date_end"=>"Дата окончания",
                    "document_type"=>"Тип документа",
                    "concluded_ku"=>"Заключен КУ",
                    "dogovor_num"=>"Номер договора",
					"vid_rashodov_id"=>"КВР",
					"kvr"=>"КВР",
					"nom"=>"Номер"
			);

	return $fields;
}
// временно потом удалим
$pfhd_article_y = request_numeric_val('pfhd_article_y',0);
if ($pfhd_article_y==2017) {
	Pfhd::$pfhd_article_y=2017;
}


if($pivot_type == 'zayav_summary') {
	$title = 'Сводный отчет (Заявки)';
	$sql_file = '/zayav_summary.sql';
	$replacement_sql_parts['%ZFO_ID_IN%'] = implode(",",user_zfos());
} else if($pivot_type == 'plat_summary') {
	$title = 'Сводный отчет (Платежки)';
	$sql_file = '/plat_summary.sql';
	$replacement_sql_parts['%ZFO_ID_IN%'] = implode(",",user_zfos());
} else if($pivot_type == 'dogovor_summary') {
	$title = 'Сводный отчет (Договоры)';
	$sql_file = '/dogovor_summary.sql';
	$replacement_sql_parts['%ZFO_ID_IN%'] = implode(",",user_zfos());
} else if($pivot_type == 'pfhd' || $pivot_type == 'pfhd_diff') {
	$first_ver_id = sql_escape($_GET['first_ver']);
	$second_ver_id = ($pivot_type == 'pfhd_diff') ? sql_escape($_GET['second_ver']) : null;

	$title = 'Сводный отчет (ПФХД | Версии)';
//	$title = 'Сводный отчет (ПФХД отклонения)';
//	$title .= ' Справочник статей '.  (($pfhd_article_y==2017) ? '2017' : '2016');
	$rashod_sign = (isset($_GET['rashod_sign'])) ? true : false;
	$y = 2017;
	if (isset($_GET['y']) && $_GET['y'] == '2018') $y=2018;
	if ($y<=2017) {
		//2016 - 2017
		$sql =  Pfhd::getSql2017($first_ver_id,$second_ver_id,$rashod_sign);
	} else if ($y==2018) {
		$sql =  Pfhd::getSql2018($first_ver_id,$second_ver_id,$rashod_sign);
	}
	if ($pivot_type == 'pfhd_diff') {
		$fld_names["sum_1"] =  Pfhd::verName($first_ver_id) . ' (A)';
		$fld_names["sum_2"] = Pfhd::verName($second_ver_id). ' (B)';
	}

} else {
	header("Location: /index.php");
}


if (!isset($sql) && !isset($query)) {
	$sql_path = __DIR__ . $sql_file;
	if(is_file($sql_path)) {
	$sql = file_get_contents($sql_path);
	if(count($replacement_sql_parts)) $sql = str_replace(array_keys($replacement_sql_parts), array_values($replacement_sql_parts), $sql);
	} else {
		//Выбрасываем исключение, если вдруг sql файл не найден или переименован
		throw new Exception("Невозможно подключить файл");
	}
}	

require ((__DIR__ . "/pivot_html.php"));
$flds['dogovor_status_id'] = sql_rows("select id,name ,null as parent_id from dogovor_status");
$flds['contragent_id'] = sql_rows("select id,name ,null as parent_id from contragent order by name");
$flds['pfhd_article_id'] = sql_rows("select id as id,concat(code_asu_pfhd,' ',name) as name,parent_id,ord from pfhd_article ");
$flds['pfhd_fs'] = sql_rows("select id ,short_name as name,null as parent_id from asu_pfhd_activity_kind ");
$flds['fp_article_id'] = sql_rows("select id ,concat('/',`fp_article`.`shifr`,'/ ',`fp_article`.`name`) AS `name`,parent_id from fp_article ");
$flds['zfo_id'] = sql_rows("select id ,name  from zfo ");
$flds['plan_grafic_id'] = sql_rows("select id ,name  from plan_grafic ");
$flds['vid_rashodov_id'] = sql_rows("select id, concat('/',`vid_rashodov`.`code`,'/ ',`vid_rashodov`.`name`) as name from vid_rashodov");

echo "<script> libUnicorn.Pivot.setFldsOptions(" . json_encode($flds) . ");</script>";
//Подключаем класс pivdata и загружаем форму настроек отчета
$res = sql_query($sql);

$num_rows = sql_count_rows($res);	
$pivdata = new Pivdata($sql);
$pivdata->fld_names = $fld_names;
$pivdata->load_pivot();



		


