select z.id AS id,
dstatus.name as dogovor_status,
z.d AS d,
zstatus.name as zayav_status,
s.sum,
z.reestr_d AS reestr_d,
month.name AS month_name,
year(z.reestr_d) AS reestr_year,
(select max(plat.d) 
	from plat 
	where (plat.zayav_id = z.id)
) AS plat_d,
fpa.name AS fp_article,
vr.code as vid_rashodov_code,
fa.kosgu AS kosgu,
ctgnt.name AS contragent,
z.dogovor_id AS dogovor_id,
zfo.name AS zfo,
fsource.name AS fin_source,
fsource.booker_fin_source as booker_fin_source,
period.name AS period,
fp_object.name as fp_object
from zayav_sostav s 
join zayav z on s.zayav_id = z.id 
left join fp_article fa on s.fp_article_id = fa.id
left join dogovor dog on z.dogovor_id = dog.id 
left join dogovor_status dstatus on dog.dogovor_status_id = dstatus.id
left join zayav_status zstatus on z.zayav_status_id = zstatus.id
left join fin_source fsource on s.fin_source_id = fsource.id
left join contragent ctgnt on z.contragent_id = ctgnt.id
left join month on month(z.reestr_d) = month.id
left join fp_article fpa on s.fp_article_id = fpa.id
left join vid_rashodov vr on fpa.vid_rashodov_id = vr.id
left join zfo on z.zfo_id = zfo.id
left join period on s.period_id = period.id
left join dogovor_sostav on s.dogovor_sostav_id = dogovor_sostav.id
left join fp_object on fp_object.id = dogovor_sostav.fp_object_id
WHERE z.zfo_id IN (%ZFO_ID_IN%) OR fsource.zfo_id IN (%ZFO_ID_IN%)