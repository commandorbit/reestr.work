<?php
$title = 'Сводный отчет 1C [18 счет] Выбытия денежных средств со счетов учреждения';
$time = microtime(true);
require_once $_SERVER['DOCUMENT_ROOT'] . '/init.php';
require_once($_SERVER['DOCUMENT_ROOT'].'/template/header.php');

$pivot_type = (isset($_GET['type'])) ? sql_escape($_GET['type']) :'none';

$ACC = '18.01';//'18.01';
$PERIOD_START = '2017-01-01';
$PERIOD_END = '2017-12-31';

$query = OneC::select(['Оборот.Period','Дата'],['КОСГУ._Code','КОСГУ'],['КВД','КФО'],
   /* ['Направления.Ссылка','НД'],*/
    ['Разделы.Ссылка','Счет'],
    [OneC::expr('-Оборот.Сумма'),'Сумма1С'],[OneC::expr('0'),'СуммаСуп'],['Оборот.Сумма','Суп-1С'])
    ->from(['РегистрБухгалтерии.ЕПСБУ', 'Оборот'])
    ->oborot_credit($ACC,$PERIOD_START,$PERIOD_END,['КЭК','Разделы Лицевых Счетов','Направления Деятельности'])
    ->join('Справочник.КОСГУ','LEFT')->
        on(OneC::cast('Оборот.Субконто1','Справочник.КОСГУ'),'=','КОСГУ.Ссылка')
    ->join(['Справочник.РазделыЛицевыхСчетов', 'Разделы'],'LEFT')->
        on(OneC::cast('Оборот.Субконто2','Справочник.РазделыЛицевыхСчетов'),'=','Разделы.Ссылка');
/*		
    ->join(['Справочник.НаправленияДеятельности', 'Направления'],'LEFT')->
        on(OneC::cast('Оборот.Субконто4','Справочник.НаправленияДеятельности'),'=','Направления.Ссылка');
*/


$rows = [];
$res = $query->execute();
$flds = [];
$n = mssql_num_fields($res);
for ($i = 0; $i < $n; $i++) {
	if (defined('MSSQL_PDO')) {
		$field = mssql_column_meta($res,$i);
		$column_name  = $field['name'];
		$column_type  = $field['pdo_type'];
	} else {
		$field = mssql_fetch_field($res,$i);
		$column_name  = $field->name;
		$column_type  = $field->type;
	}
	$label = $column_name;
    $flds[$column_name]= ["name"=>$column_name,"type"=>$column_type,"label"=>$label];
}
define ('MYSQL_DATE_TYPE',10);
$flds['Дата']['type'] = MYSQL_DATE_TYPE;

while ($row =  mssql_fetch_array($res,MSSQL_ASSOC)) {
//    $row['НД'] = bin2hex($row['НД']);
    $row['КФО'] = bin2hex($row['КФО']);
    $row['Счет'] = bin2hex($row['Счет']);
    $row['Сумма1С']+=0;
    $row['СуммаСуп']=null;
    $row['Суп-1С']+=0;
    $rows[] = $row;
}

$sup_rows = sql_rows("SELECT d Дата, 
	p.kosgu КОСГУ, 
/*	bfs.НаправленияДеятельности_Ref НД,*/
	bfs.КВД_Enum КФО,
	acc.РазделыЛицевыхСчетов_Ref Счет, 
	0 as Сумма1С,
	sum(s) as СуммаСуп,
	sum(s) as `Суп-1С` 
	FROM plat p
	LEFT JOIN booker_fin_source bfs on p.booker_fin_source = bfs.code
	LEFT JOIN bank_acc acc on p.bank_acc_id = acc.id
	WHERE p.plat_type_id=3
	GROUP BY d, kosgu, КФО, /*НД,*/ Счет ");
                
foreach ($sup_rows as $row) {
//  $row['НД'] = bin2hex($row['НД']);
    $row['КФО'] = bin2hex($row['КФО']);
    $row['Счет'] = bin2hex($row['Счет']);
    $row['Сумма1С'] = null;
    $row['СуммаСуп'] +=0;
    $row['Суп-1С'] +=0;
    $rows[] = $row;
} 

require ((__DIR__ . "/pivot_html.php"));
echo "<script> libUnicorn.Pivot.setFldsOptions(" . json_encode(pivot_fld_options()) . ");</script>";

Pivdata_rows::load_pivot($rows,$flds);

function pivot_fld_options() {
    $flds = [];
    $kfos = OneC::enum_options('КВД');
    $rows = [];
    foreach ($kfos as $kfo=>$name) {
    	$rows[]=['id'=>$kfo,'name'=>$name];
    }
    $flds['КФО'] = $rows;

    $res = OneC::select('Н.Ссылка','Н.Наименование')->from(['Справочник.НаправленияДеятельности','Н'])->execute();
    $rows = [];
    while ($row = mssql_fetch_array($res,MSSQL_ASSOC)) {
        $rows[]=['id'=>bin2hex($row['Ссылка']),'name'=>$row['Наименование']];
    }
    $flds['НД']=$rows;
   
    $res = OneC::select('Р.Ссылка','Л.Наименование')->from(['Справочник.РазделыЛицевыхСчетов', 'Р'])
            ->join(['Справочник.ЛицевыеСчета','Л'])->on('Р._OwnerIDRRef','=','Л.Ссылка')
            ->execute();
    $rows = [];
    while ($row = mssql_fetch_array($res,MSSQL_ASSOC)) {
        $rows[]=['id'=>bin2hex($row['Ссылка']),'name'=>$row['Наименование']];
    }
    $flds['Счет']=$rows;
    $flds['ТипОперации'] = sql_rows("select id,name from plat_type");
    return $flds;
}

class Pivdata_rows {	
	public static function load_pivot($rows,$flds) {
		echo "<script>"."\n";
		self::load_progress($rows,$flds);
		echo "var pivot = new libUnicorn.Pivot(d,flds,document.getElementById('PIVOT')); \n" ;
		echo "</script>"."\n";
	}

	protected static function load_progress($rows,$flds) {
		$num_rows = count($rows);
		echo "libUnicorn.progressBar.start('Идет загрузка');\n";
		echo "var d=[];";
		$i=0;
		foreach ($rows as $row) {            
			if ($i % 100 == 0) {
				echo "\n libUnicorn.progressBar($i,$num_rows);\n";
				echo "</script>\n<script>\n"; 
			}
			echo "d[$i] =" . json_encode($row) . ";\n";
			$i++;
		}
		echo "libUnicorn.progressBar($i,$num_rows);\n";
		echo "var flds = " . json_encode($flds) .";\n";
	}
}

