<?php

$reports = get_saved_pivot_reports($pivot_type);
if(!isset($title)) $title = '';

?>

<script>
function download(filename, text) {
    var URL = window.URL || window.webkitURL || window.mozURL || window.msURL;
	var blob = new Blob([text]);
    var url = URL.createObjectURL(blob);

    var pom = document.createElement('a');
    /*
    var str = 'data:text/plain;charset=utf-8,' + encodeURIComponent(text);
    console.log ("export",url.length);
    return;
    if(str.length > 1999999 && window.chrome){
        alert("Sorry, this export method does not work in Google Chrome with file larger 1.9 M")
        return; 
    }
    */
    pom.setAttribute('href', url);
    pom.setAttribute('download', filename);

    if (document.createEvent) {
        var event = document.createEvent('MouseEvents');
        event.initEvent('click', true, true);
        pom.dispatchEvent(event);
    }
    else {
        pom.click();
    }
}

function toExcel() {
	var html = "<meta charset='utf-8' /><style> th {border:1px solid black} </style>";
	html += "<table border='1'>" + document.querySelectorAll("#PIVOT table")[1].innerHTML + "</table>";
	download('Сводный отчет.xls',html);
}
</script>
<?php if(count($reports)): ?>
    <div data-type="main-sidebar">
        <ul class="no-checkbox">
            <?php foreach($reports as $report): ?>
                <li style="padding-left: 10px" data-val="<?=$report['id']?>"><label><?=$report['name']?></label><span data-val="<?=$report['id']?>"><i class="fa fa-trash"></i></span></li>
            <?php endforeach; ?>
        </li>
    </div>
<?php endif; ?>
<div class="header-link">
    <label><?=(isset($layout_vars['action_button'])) ? $layout_vars['action_button'] : ''; ?></label>
    <div class="pivot-icons">
        <div class="service-ico load-report"  title="Загрузить отчет"></div>
        <div class="service-ico to-pivot"  title="Открыть настройки сводной таблицы"></div>
        <div class="service-ico to-excel"  title="Выгрузить отчет в формате XLS"></div>
        <div class="service-ico save"  title="Сохранить"></div>
		<div class="service-ico save-as"  title="Сохранить как..."></div>
    </div>
    <div class="pivot-report-title"><h1 style="color: #fff; margin-top: 0; margin-right: 15%; padding: 12px 0"><?=$title?></h1></div>
</div>
<div class="content">
    <div class="widget pivot-filters clearfix" style="margin-left: 13px;">
        
            <div class="filter-ico"></div>
			<span><b style="font-size: 14px;">Фильтры</b></span> 
        
        
            <button class="butt" data-type="filter-apply" style="padding: 5px; max-width: 130px;"><i class="fa fa-check"></i> Применить</button>
            <!--<button class="btn error" data-type="filter-clear"><i class="fa fa-ban"></i> Очистить</button>-->
        
        <div class="filters clearfix" data-type="filters-area" style="margin-top: 10px;"></div>
    </div>
	
    <div class="table_contaner" id="PIVOT">
    </div>
</div>

<div id="pivot-settings" style="visibility:hidden">
    <div class="header">
		Настройка полей Сводной таблицы
	</div>
    <div class="wrap">
    <div class="wrap-top">
    <div class="flds section">
        <ul class="select" id="pivot-flds">
        <!--<li draggable="true"><label><input type="checkbox" data-name="name1">name1</label>-->
        </ul>
    </div>
    </div>
    <div class="wrap-delimiter"><hr></div>
    <div class="wrap-bottom">
    <div class="filter section">
       <div class="header">Фильтр отчета:</div> 
       <div class="body">
           <ul class="select" id="pivot-filters">
           </ul>
       </div>
    </div>
    <div class="cols section">
       <div class="header"> Названия столбцов:</div> 
       <div class="body">
         <ul class="select" id="pivot-cols">       
         </ul>
       </div>
    </div>
    
    <div class="rows section">       
       <div class="header"> Названия строк:</div> 
       <div class="body">
         <ul class="select" id="pivot-rows">       
         </ul>
       </div>
    </div>

    <div class="cells section">
       <div class="header"> Значения:</div> 
       <div class="body">
        <ul class="select" id="pivot-cells">
       </div>
    </div>
    </div>    
    </div>
    <div class="wrap-buttons" >
		<button id="PivotApply" class="butt dark large" style="width: 100%; height: 30px">Применить</button>
    </div>    

</div>