<?php
error_reporting(E_ALL);
$title = 'СУП - система управленческого планирования';
require $_SERVER['DOCUMENT_ROOT'] . '/init.php';
require $_SERVER['DOCUMENT_ROOT'] . '/template/header.php';
?>
<link rel="stylesheet" type="text/css" href="/css/background<?=$_SESSION["palette"]?>.css?ver=<?=VER?>">
<div id="ext-body" class="ext-body">
<?php
require $_SERVER['DOCUMENT_ROOT'] . '/include_extjs.php';
?>
<script id="microloader" type="text/javascript" src="/extapp/bootstrap.js"></script>
<script>
	
	//Ждём загрузки EXTJS
	window.addEventListener("load", function(event)
	{
		var waitExtJSTimer = setInterval(function()
		{
			if (window.extapp && window.extapp.myAppWasLoaded)
			{
				clearInterval(waitExtJSTimer);
				console.log('Библиотека загружена!');
				//
				var tablename = '<?=$table?>';
				//alert('USER = ' + window.extapp.userId + '<br>TABLE = ' + tablename);
					
				/*
				Ext.Promise.all([
					promiseStoreLoad('storeTab'),
				]).then(function()
				{
				*/
					//console.log('Описания таблиц загружены!');
					if (! extapp.getApplication().openTable(tablename)) {
						//alert('Редактор для ' + tablename + ' не найден! ' + window.extapp.altUrl);
						window.location = window.extapp.altUrl;
					}
				/*
				});
				*/
				//
			}
		},
		100);	
	});

</script>

</div>

<?php 
	require $_SERVER['DOCUMENT_ROOT'] . '/template/footer.php';
?>