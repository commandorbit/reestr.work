<?php
error_reporting(E_ALL | E_STRICT);

define('FORM_URL','http://pfhd.edu.ru/API/Docs/edubpl/Put');
define('FORM_PART_URL','http://pfhd.edu.ru/API/Docs/%key%/PutPart');
//define('FORM_URL','http://10.123.124.215:3000');
define('FILE_NAME','extra.xml');
define('BOUNDARY',md5(rand(0,999)));
define('RN',"\r\n");
define('LOGIN','СПбГЭУ');
define('PASSWORD','October2015');

define('URI','extraValue');

function body($s,$len) {
	$str_file = file_get_contents(FILE_NAME);
	if ($len < strlen($str_file)) {
		$str_file = substr($str_file,$s,$len);
	}
	$s = "--".BOUNDARY.RN;
	$s .= 'Content-Disposition: form-data; name="AC.Login"'.RN.RN;
	$s .= LOGIN.RN;
	$s .= "--".BOUNDARY.RN;
	$s .= 'Content-Disposition: form-data; name="AC.Password"'.RN.RN;
	$s .= PASSWORD.RN;
	$s .= "--".BOUNDARY.RN;
	$s .= 'Content-Disposition: form-data; name="URI"'.RN.RN;
	$s .= URI.RN;
	$s .= "--".BOUNDARY.RN;
	$s .= 'Content-Disposition: form-data; name="Content"; filename="extra.xml"'.RN;
	$s .= 'Content-Type: text/xml'.RN.RN;
	$s .= $str_file.RN;
	$s .= "--".BOUNDARY."--".RN;

	return $s;
}

function post($cnt_parts,$size_part) {
	$res = '';
	$total = filesize(FILE_NAME);
	for ($i = 0; $i < $cnt_parts; $i++) {
		$headers = array(
			'Content-Type: multipart/form-data; boundary='.BOUNDARY,
			'Cache-Control: no-cache'
		);
		if ($cnt_parts > 1) {
			$s = $i * $size_part;
			$po = $s + $size_part - 1;
			if ($po >= $total) $po = $total-1; 
			$s = 'Content-Range:bytes '.$s.'-'.$po.'/'.$total;
			echo $s.'<br>';
			$headers[] = $s;
		}
		/*
		print_r($headers);
		echo '<br><br>';
		*/
		$body = body($i * $size_part,$size_part);
		$curl = curl_init();
		curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);

		if ($i > 0) {
			$url = str_replace("%key%",$key,FORM_PART_URL);
		} else {
			$url = FORM_URL;
		}
		curl_setopt($curl, CURLOPT_URL,$url);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER,true);
	    curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $body);
		curl_setopt($curl, CURLOPT_FORBID_REUSE, true);
		
		$r = curl_exec($curl);
		if (!$i) {
			$matches = array();
			preg_match('/<Key>[A-z0-9-]+<\/Key>/', $r, $matches);
			$key = (array_key_exists(0,$matches)) ? strip_tags($matches[0]) : '?';
		}
		$res .= 'POST url '.$url."\n\n".$r."\n\n\n";
		curl_close($curl);	
	}
	return $res;
}

$answer = 'Сюда будет написан ответ удаленного сервера!';
if (isset($_POST['run'])) {
	$filesize = filesize(FILE_NAME);
	$may_part = request_numeric_val('may_part',0);
	$size_part = ($may_part) ? request_numeric_val('size_part',$filesize) : $filesize;
	$cnt_parts = ($may_part) ? ceil($filesize / $size_part) : 1;

	foreach ($_POST as $k=>$v) unset($_POST[$k]);
	$answer = post($cnt_parts,$size_part);
}

function request_numeric_val($k,$def='') {
	return (array_key_exists($k,$_REQUEST)) ? $_REQUEST[$k] : $def;
}

?>

<style type="text/css">
textarea[name="answer"] {width: 900px; height: 500px;}
</style>

<form method="post">
	Разбить на части:
	<input name="may_part" type="checkbox" value="1">
	Размером:
	<input name="size_part" type="text" value="2000"> байт<br>
	<input name="run" type="submit" value="Запустить POST">
	<br><br>
	<textarea name="answer"><?=$answer ?></textarea>	
</form>     