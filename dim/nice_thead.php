<?php
error_reporting(E_ALL);

$heads = [
	'ректор | максимцев | игорь | 1',
	'ректор | помощники | всего | 1',
	'ректор | помощники | 1',
	'ректор | помощники | 2',
	'ректорат | чубаева',
	'ректорат | егорова',
	'вц | технический отдел | гриша | начальник',
	'вц | технический отдел | андрбша | сотрудники',
	'вц | программирования | голованов | сотрудники',
	'вц | программирования | я | сотрудники',
	'вц | программирования | чернов | начальник',
	'вц | петров | петров | сергеевич | алексей',
	'вц | информац.систем | травин',
	'вц | информац.систем | захарова',
	'вц | информац.систем | сектор | васильева',
	'вц | информац.систем | сектор | надя',
	'охрана'
];

$table = thead($heads);
echo '<table border="1" style="border-collapse: collapse;">';
foreach ($table as $tr=>$tds) {
	echo '<tr>';
	foreach ($tds as $td) {
		echo '<td colspan="'.$td['colspan'].'" rowspan="'.$td['rowspan'].'">
		colspan='.$td['colspan'].'<br>
		rowspan='.$td['rowspan'].'<br>
		text=<b>'.$td['text'].'</b></td>';
	}
	echo '</tr>';
}
echo '</table>';



function thead($heads) {
	$a = [];
	$max_j = 0;
	foreach ($heads as $i=>$head) {
		$head_parts = explode("|",$head);
		foreach ($head_parts as $j=>$head_part) {
			$a[$i][$j] = trim($head_part);
			if ($j > $max_j) $max_j = $j;
		}
	}

	$max_cnt_rows = $max_j+1;
	
	$ignore = [];
	$thead = [];
	foreach ($a as $i=>$b) {
		foreach ($b as $j=>$value) {
			if (!in_array(_hash($i,$j),$ignore)) {
				$thead[$j][$i]['text'] = $value;
				$thead[$j][$i]['colspan'] = 1;
				$thead[$j][$i]['rowspan'] = 1;
				$next_i = $i+1;
				while (isset($a[$next_i][$j]) && $value == $a[$next_i][$j]) {
					$ignore[] = _hash($next_i,$j);
					$thead[$j][$i]['colspan']++;
					$next_i++;
				}
				$next_j = $j+1;
				while (isset($a[$i][$next_j]) && $value == $a[$i][$next_j]) {
					$ignore[] = _hash($i,$next_j);
					$thead[$j][$i]['rowspan']++;
					$next_j++;
				}
			}
		}
		
		if (!in_array(_hash($i,$j),$ignore)) {
			$rowspan = $max_cnt_rows - $j;
			if ($rowspan > $thead[$j][$i]['rowspan']) {
				$thead[$j][$i]['rowspan'] = $rowspan;
			}
		}
	}

	return $thead;
}

function _hash($i,$j) {
	return $i.'!'.$j;
}
