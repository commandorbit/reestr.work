<?php
error_reporting(E_ALL | E_STRICT);
require_once $_SERVER['DOCUMENT_ROOT'].'/init.php';
require_once $_SERVER['DOCUMENT_ROOT'].'/PHPExcel/Classes/PHPExcel.php';

$allowed_types = array(
	'application/vnd.ms-excel',
	'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
);

// array (colname, labelname, content_type)
$format = array(
	array('id','id','numeric'),
	array('name','Название'),
	array('unit','Ед. изм.'),
	array('value','Значение','numeric')
);

if (isset($_POST['upload_file'])) {
	$error = '';
	if (!isset($_FILES['file'])) {
		$error = 'Файл не был загружен!';
	} elseif (!in_array($_FILES['file']['type'],$allowed_types)) {
		$error = 'Файл должен быть в формате xls/xlsx!';
	} else {
		// если надо сохранять подгруженные файлы выполнить move_uploaded_file(tmp_dest,new_dets)
		$file_name = $_FILES['file']['tmp_name'];
		$parse_result = _parse_excel($file_name,$format);
		$parse_errors = $parse_result['errors'];
		if (!empty($parse_errors)) {
			$error = implode("<br>",$parse_errors);
		} else {
			$rows = $parse_result['rows'];
			$table = 'asu_pfhd_dop_pokaz';
			$load_errors = array();
			foreach ($rows as $row) {
				$id = $row['id'];
				$orig = sql_get_array($table,"id='$id'");
				$check = array(1=>'name',2=>'unit');
				foreach ($check as $i => $el) {
					if ($orig[$el] != $row[$el]) 
						$load_errors[] = 'Строка с id '.$id.' содержит неверное значение в колонке "'.$format[$i][1].'" !';
				}
			}
			if (!empty($load_errors)) {
				$error = implode("<br>",$load_errors);
			} else {
				foreach ($rows as $row) {
					$id = $row['id'];
					$value = $row['value'];
					$sql = "UPDATE $table SET value = '$value' WHERE id='$id' ";
					//sql_query($sql);
					echo $sql.'<br>';
				}
			}
		}
	}
	if ($error) echo '<h3 style="color: #ff0000;">'.$error.'</h3>';
	else echo '<h3>Файл был успешно загружен и обработан!</h3>';
}



function _parse_excel($file_name,$format) {
	$data = array();
	$errors = array();

	$excel = PHPExcel_IOFactory::load($file_name);
	$sheet = $excel->getSheet(0);
	$nrow = 0;
	foreach ($sheet->getRowIterator() as $row) {
		$cnt_cols = 0;
		foreach ($row->getCellIterator() as $dummy) $cnt_cols++;
		if ($cnt_cols==count(array_keys($format))) {
			$a = array();
			$ncell = 0;
			foreach ($row->getCellIterator() as $cell) {
				$value = $cell->getValue();
				$coord = $cell->getCoordinate();
				if ($nrow == 0) {
					$label = $format[$ncell][1];
					if ($label != $value) $errors[] = 'Неверный заголовок у '.$coord.' ячейки!';
				} else {
					$type = (isset($format[$ncell][2])) ? $format[$ncell][2] : '';
					$colname = $format[$ncell][0];
					$is_numeric = ($type == 'numeric');
					if ($is_numeric && !is_numeric($value)) {
						$errors[] = 'Ячейка '.$coord.' может содержать только число! '.$value.' не является числом!';
					}
					$a[$colname] = $value;
				}
				$ncell++;
			}
			if ($a) $data[]=$a;
		} else {
			$errors[] = 'Строка '.($nrow+1).' содержит '.$cnt_cols.' колонок вместо '.count(array_keys($format)).'!';
		}
		$nrow++;
	}
	return array('rows'=>$data,'errors'=>$errors);
}



?>
<form method="post" enctype="multipart/form-data">
	<p>
		<input type="hidden" name="MAX_FILE_SIZE" value="0">
		<input name="file" type="file"><br>
		&nbsp;<input type="submit" name="upload_file" value="Загрузить"><br><br>
	</p>
</form>