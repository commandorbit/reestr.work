<style type="text/css">
.custom_table table {
	border-collapse: collapse;
}
.custom_table table td {
	padding: 3px;
	min-width: 100px;
	border: 1px solid grey;
	text-align: left;
	vertical-align: top;
}

.custom_table table thead {
	background: #F7F3DE;
}

</style>

<?php 
error_reporting(E_ALL);

$data = [
	'head'=>[
		'№',
		'А...',
		['Учреждение','ИФО','Регистратор','Период'],
		'К...',
		[['(ДТ) КПС','(ДТ) Счет'],'(ДТ) Субконто 1','(ДТ) Субконто 2','(ДТ) Субконто 3','(ДТ) Субконто 4'],
		[['(КТ) КПС','(КТ) Счет'],'(КТ) Субконто 1','(КТ) Субконто 2','(КТ) Субконто 3','(КТ) Субконто 4'],
		['(ДТ) количество','(ДТ) валюта'],
		['какие-то данные',['подъячейка 1','подъячейка 2'],'какие-то данные']
	],
	'rows'=>[
		[
			'1',
			'+',
			['СПбГЭУ','','Извещение №xxxxxx1','31.12.2015'],
			'2',
			[['0000000000000','401.21'],'180','180 - прочие доходы','Грант xxx',''],
			[['0000000000000','304.04'],'180','Филиал в Великом Новгороде','Расходы на целевые средства','-'],
			['',''],
			['какие-то данные',['подъячейка 1','подъячейка 2'],'какие-то данные']
		],
		[
			'2',
			'+',
			['СПбГЭУ2','','Извещение №x33333xxx1','31.12.2011'],
			'2',
			[['0000000000000','433.21'],'180','180 - прочие доходы','Грант xxx',''],
			[['0000000000000','1112.04'],'180','Филиал в Великом Пскове','Расходы на какие-то куда-то','-'],
			['',''],
			['какие-то данные',['подъячейка 1','подъячейка 2'],'какие-то 1111']
		],
		[
			'3',
			'-',
			['hjuffmrenf','','Извещение №x33333xxx1','31.12.2011'],
			'2',
			[['0000000000130','433.21'],'180','180 - прочие доходы','Грант xxx',''],
			[['0000000000000','1112.04'],'180','Филиал в Великом Пскове','Расходы на какие-то куда-то','-'],
			['',''],
			['какие-то данные',['подъячейка 1','подъячейка 2'],'какие-то 1111']
		]
	]
];

echo '<!DOCTYPE html>';
echo '<div class="custom_table">';
custom_table::show($data);
echo '</div>';



class custom_table {
	public static function show($data) {
		$table = self::build($data);
		echo $table;
	}

	public static function build($data) {
		self::check($data);

		$s = '<table>';
		$s .= '<thead>'.self::build_row($data['head']).'</thead>';
		$s .= '<tbody>';
		foreach ($data['rows'] as $row) {
			$s .= self::build_row($row);
		}
		$s .= '</tbody>';
		$s .= '</table>';
		return $s;
	}

	private static function build_row($rowdata) {
		$max_counts = self::max_counts($rowdata);
		$max_rows = $max_counts['max_rows'];
		$rows_opened = [0];

		$s = '<tr>';
		for ($i=0; $i<$max_rows; $i++) {
			foreach ($rowdata as $cell) {
				if (is_array($cell)) {
					// ячейка на много строк

					$max_counts_current = self::max_counts($cell,'row');
					$max_cells_current = $max_counts_current['max_cells'];
					$max_rows_current = $max_counts_current['max_rows'];

					if (array_key_exists($i,$cell)) {
						if (!in_array($i,$rows_opened)) {
							$s .= '<tr>';
							$rows_opened[] = $i;
						}
						if (is_array($cell[$i])) {
							// несколько ячеек в ячейке
							foreach ($cell[$i] as $subcell) {
								$rowspan = (!array_key_exists($i+1,$cell)) ? $max_rows - $max_rows_current + 1 : 1;
								$s .= '<td rowspan="'.$rowspan.'">'.$subcell."</td>\r\n";
							}
						} else {
							$colspan = $max_cells_current;
							$rowspan = (!array_key_exists($i+1,$cell)) ? $max_rows - $max_rows_current + 1 : 1;
							$s .= '<td rowspan="'.$rowspan.'" colspan="'.$colspan.'">'.$cell[$i]."</td>\r\n";
						}
					}
				} elseif ($i==0) {
					// ячейка только из 1 строки (если описать ее как массив, она попадет в верхний разбор, что тоже допустимо)
					$rowspan = $max_rows-$i;
					$s .= '<td rowspan="'.$rowspan.'">'.$cell."</td>\r\n";
				}
			}
		}
		return $s;
	}

	private static function max_counts($data,$even_element = 'cell') {
		$max_even = count($data);
		$max_odd = 1;

		$i = 0;
		foreach ($data as $c0) {
			if (is_array($c0)) {
				if (count($c0) > $max_odd) $max_odd = count($c0);
				foreach ($c0 as $r1) {
					if (is_array($r1)) $i += (count($r1)-1);
				}
			}
		}
		$max_even += $i;

		if ($even_element == 'cell') {
			return ['max_rows'=>$max_odd,'max_cells'=>$max_even]; 
		} else {
			return ['max_rows'=>$max_even,'max_cells'=>$max_odd];
		}	
	}

	private static function check($data) {
		$max_level = 3;
		if (!is_array($data)) error('Data must be array!');
		if (!array_key_exists('head', $data)) error('Data need "head" element!');
		if (!array_key_exists('rows', $data)) error('Data need "rows" element!');
		if (!is_array($data['head'])) error('"head" element must be array!');
		if (!is_array($data['rows'])) error('"rows" element must be array!');

		$head_level = self::array_max_level($data['head']);
		if ($head_level>$max_level) {
			self::error('Assoc max hierarchy level in "head" element cant be more then '.$max_level.'. You have '.$head_level.'!');
		}
		$n = 0;
		foreach ($data['rows'] as $row) {
			$n++;
			$row_level = self::array_max_level($row);
			if ($row_level != $head_level) {
				self::error('Assoc max hierarchy level in "rows" element must be equal level "head" element ('.$head_level.'). On row '.$n.' you have '.$head_level.'!');
			}
		}
	}

	private static function array_max_level($a) {
		$max = 1;
		foreach ($a as $v) {
			if (is_array($v)) {
				$current = self::array_max_level($v) + 1;
				if ($current > $max) $max = $current;
			}
		}
		return $max;
	}

	private static function error($e) {
		die($e);
	}
}




?>