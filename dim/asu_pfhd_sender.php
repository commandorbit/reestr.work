<?php 

define('FORM_URL','http://pfhd.edu.ru/API/Docs/edubpl/Put');
define('FORM_PART_URL','http://pfhd.edu.ru/API/Docs/%key%/PutPart');
define('FORM_CHECK_URL','http://pfhd.edu.ru/API/Docs/%key%/Check');

define('FILE_NAME','extra.xml');
define('BOUNDARY',md5(rand(0,999)));
define('RN',"\r\n");
define('LOGIN','СПбГЭУ');
define('PASSWORD','October2015');

function check_key($key) {
	$url = str_replace("%key%",$key,FORM_CHECK_URL);
	$headers = array(
		'Content-Type:application/x-www-form-urlencoded; charset=UTF-8',
		'Cache-Control: no-cache'
	);
	$body = "AC.Login=".LOGIN."&AC.Password=".PASSWORD;
	$answer = post($url,$headers,$body);
	if ($answer) {
		$status = answer_status($answer);
		$messages = answer_messages($answer);
		sql_query("UPDATE asu_pfhd_load_log SET message='$messages', status='$status', upd_ts=NOW() WHERE `key`='$key' ");
	}
	return $answer;
}

function send_xml_file($filename,$value_type,$basename) {
	$xml = file_get_contents(FILE_NAME);
	return send_xml($xml,$value_type,$basename);
}

function send_xml($xml,$value_type,$basename) {
	$headers = array(
		'Content-Type: multipart/form-data; boundary='.BOUNDARY,
		'Cache-Control: no-cache'
	);
	$size = strlen($xml);
	if ($size > MAX_SIZE) {
		$xml_parts = array();
		$i = 0;
		while (strlen($xml) > MAX_SIZE) {
			$xml_part = substr($xml,MAX_SIZE);
			$byte_s = $i * MAX_SIZE;
			$byte_po = $s + strlen($xml_part) - 1;
			$xml_parts[] = array('byte_s'=>$byte_s,'byte_po'=>$byte_po,'xml'=>$xml_part);
			$i++;
		}
		$i = 0;
		foreach ($xml_parts as $arr) {
			$xml_part = $arr['xml'];
			$current_headers = $headers;
			$current_headers[] = 'Content-Range:bytes '.$arr['byte_s'].'-'.$arr['byte_po'].'/'.$size;
			if (!$i) {
				$answer = send_xml_part($xml_part,$value_type,$basename,$current_headers);
				$key = answer_key($answer);
			} else {
				$answer = send_xml_part($xml_part,$value_type,$basename,$current_headers,$key);
			}
			$i++;
		}
	} else {
		$answer = send_xml_part($xml,$value_type,$basename,$headers);
		$key = answer_key($answer);
	}
	if ($answer) {
		$status = answer_status($answer);
		$messages = answer_messages($answer);
		sql_query("INSERT INTO asu_pfhd_load_log (name, send_ts, is_error, `key`, status, message) 
			VALUES ('$value_type', now(), 0, '$key', '$status', '$messages') ");
	} else {
		sql_query("INSERT INTO asu_pfhd_load_log (name, send_ts, is_error) 
			VALUES ('$value_type', now(), 1) ");
	}
	
	return $answer;
}

function send_xml_part($xml_part,$value_type,$basename,$headers,$key = '') {
	$body = body($xml_part,$value_type,$basename);
	if ($key) $url = str_replace("%key%",$key,FORM_PART_URL);
	else $url = FORM_URL;
	return post($url,$headers,$body);
}

function body($text,$value_type,$basename) {
	$s = "--".BOUNDARY.RN;
	$s .= 'Content-Disposition: form-data; name="AC.Login"'.RN.RN;
	$s .= LOGIN.RN;
	$s .= "--".BOUNDARY.RN;
	$s .= 'Content-Disposition: form-data; name="AC.Password"'.RN.RN;
	$s .= PASSWORD.RN;
	$s .= "--".BOUNDARY.RN;
	$s .= 'Content-Disposition: form-data; name="URI"'.RN.RN;
	$s .= $value_type.RN;
	$s .= "--".BOUNDARY.RN;
	$s .= 'Content-Disposition: form-data; name="Content"; filename="'.$basename.'"'.RN;
	$s .= 'Content-Type: text/xml'.RN.RN;
	$s .= $text.RN;
	$s .= "--".BOUNDARY."--".RN;

$body = "AC.Login=".LOGIN."&AC.Password=".PASSWORD;	return $s;
}

function post($url,$headers,$body) {
	$curl = curl_init();
	curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
	curl_setopt($curl, CURLOPT_URL,$url);
	curl_setopt($curl, CURLOPT_RETURNTRANSFER,true);
    curl_setopt($curl, CURLOPT_POST, true);
	curl_setopt($curl, CURLOPT_POSTFIELDS, $body);
	curl_setopt($curl, CURLOPT_FORBID_REUSE, true);
	$r = curl_exec($curl);
	curl_close($curl);
	return $r;
}

function answer_parse($answer,$return) {
	if (!$answer) return false;
	$xml = simplexml_load_string($answer);
	return $xml->$return;
}

function answer_key($answer) {
	return answer_parse($answer,'Key');
}

function answer_status($answer) {
	return answer_parse($answer,'Status');
}

function answer_messages($answer) {
	$o = answer_parse($answer,'Messages');
	$a = array();
	$i=0;
	while (isset($o->Message[$i])) {
		$a[] = $o->Message[$i];
		$i++;
	}
	return implode("; ",$a);
}



?>