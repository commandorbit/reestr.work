<?php
require 'init.php';
require 'modules/save.php';
require 'soft_delete.php';
require 'modules/attaching_files.php';
require 'modules/controls.php';
require 'modules/option_list.php';
require 'tedit_fn.php';
require dirname(__FILE__) . '/dogovor_fn.php';
require dirname(__FILE__) . '/upload_file_fn.php';
require_once 'thesis/thesis_functions.php';


header('Content-Type: text/html; charset=utf-8');

if (!isset($_GET['id']) ) {
	header ('Location: index.php');
	exit;
} 

$id = sql_escape($_GET['id']);
$table = 'dogovor';
$ref_key = 'dogovor_id';


define ('NEW_ID','new');

//Start POST handler

if(isset($_POST['dogovor_delete'])) {
    $msg='';
    $ok = del_row($table, $id, $msg);
    if ($ok) {
        $_SESSION['USER_MESSAGE']['SUCCESS'] = 'Запись успешно удалена!';

        header('Location: /tedit.php?t=' . $table);

        exit();
    } else {
        $_SESSION['SYSTEM_MESSAGE'] = $msg;
    }
}

if (isset($_POST['save_parent'])) {
	$id = save($table,$_POST);
	$_GET['id'] = $id;

    $_SESSION['USER_MESSAGE']['SUCCESS'] = 'Договор успешно сохранен!';

	refresh_after_save(); // перенавправляем На id
}

if (isset($_POST['uploaded'])){
    dogovor_file_upload($id);
}

if (isset($_POST['save'])) {
	$_POST[$ref_key] = $id;
	save($table_sostav, $_POST);    

	refresh_after_save();
}

//End POST handler

$T = newTD($table, array('f_id'=>$id));

if (!$T->data and $id !== NEW_ID) {
	echo '<p class="attention">Запись не найдена, либо у вас нет доступа к ней!</p>';
	exit();
}

$data = ($id!==NEW_ID) ? $T->data[0] : null;

$zfo = user_has_zfo();
if($id != NEW_ID && $zfo) {
    $T->may_edit = ((!is_ro() && may_dogovor()) && (check_may_edit_zfo($id, $table) == $zfo));
} else if(!$zfo) {
    $T->may_edit = true;
}
$T->columns['dogovor_nom']['null'] = 1;

if (user_has_zfo()) {
	$zfos = user_zfo_ids();
	if ($data && $data['zfo_id']) {
		$zfos[] = $data['zfo_id']; // ЦФО зашло в чужой договор
	}
	//user_zfo_id_str();
    $T->columns['zfo_id']['edit_options'] = sql_to_assoc("select id,name from zfo where id in (".implode(',',$zfos).")");
}

if ($id<>NEW_ID) {
    $dogovor_status = $data['dogovor_status_id'];

    $use_limit = sql_get_value('use_limit', 'dogovor_status', "id=$dogovor_status");

    if(!is_admin() && user_has_zfo() && !in_array($data['zfo_id'], user_zfo_ids()) || $dogovor_status <> 1) {
        $T->may_edit = 0;
    }
}

//THESIS

$bad_summ = 0;
if ($id!=NEW_ID) {
// вытащить в функцию !
    $bad_zs_view = 'v_zayav_sostav_bad_dogovor_sostav_id';
    $bad_summ = sql_get_value("sum(sum)",$bad_zs_view , "dogovor_id='$id'");
	//AVD
    //$bad_summ = 0;
} 
	
if ($id==NEW_ID) $title = 'Новый Договор';
else $title =  'Договор ID: ' . $id;
include 'template/header.php';


?>

<script src="/assets/js/multiselect-plugin/modalWindowBody.js"></script>
<script src="/assets/js/multiselect-plugin/multiselect.js"></script>
<script src="/assets/js/tree-plugin/js/treeplugin.js"></script>


<?


$page = request_val('page','dogovor');

if($page == 'dogovor') {
    echo '<div class="content-pane">';
    echo '<h1>'.$title.'</h1>';
    if($id != NEW_ID && get_thesis_guid($id) && $thesis_status = get_thesis_status($id)) {
    	$thesis_lnk = get_thesis_lnk($id);
		//AVD 02.12.2017
		$thesis_lnk = '/thesis/thesis_dogovor.php?id=' . $id;
		//
    	echo '<br/>Состояние в системе <a href="'.$thesis_lnk.'" target="_blank" title="Договор в системе ТЕЗИС">"Тезис"</a>: <b>' . get_thesis_status_str($thesis_status) . '</b>';
    }
    dogovor_status_out($data);
    echo '<form method="post" action="" class="custom-buttons">';
        dogovor_out($T,$data);
        $disabled = 'disabled';
        if ($T->may_edit) {
            $disabled = '';
        }
        echo '<div class="action_sidebar">';
            if($id != 'new') {
               echo '<p class="save_parent clearfix"><input '.$disabled.' type="submit" name="save_parent" value="Сохранить">&nbsp;&nbsp;&nbsp;<input '.$disabled.' type="submit" class="delete-submit" name="dogovor_delete" onclick="return confirmBox(\'Вы действительно хотите удалить договор?\')" value="Удалить договор">';
            } else {
                echo '<p class="save_parent clearfix"><input '.$disabled.' type="submit" name="save_parent" value="Сохранить">';
            }
        echo '</div>';
    echo '</form>';
    echo "</div>";
}

function get_dogovor_gz($dogovor_id, $ver = false) {
	
	$ver_sql = (!is_null($ver)) ? "AND ver = '$ver'" : "";
    $dogovor_gz = sql_array(sql_query("SELECT * FROM dogovor_gz WHERE dogovor_id = '$dogovor_id' $ver_sql ORDER BY create_ts DESC LIMIT 1"));
	$dgz_id = $dogovor_gz['id'];
    $okveds = sql_rows("SELECT okved2_code as code, concat('/',okved.code,'/ ',okved.name) AS name FROM dogovor_gz_okved2 LEFT JOIN okved on dogovor_gz_okved2.okved2_code = okved.code WHERE dogovor_gz_id = '$dgz_id'");
	$prefs = sql_rows("SELECT pref_code, gp.name AS name FROM dogovor_gz_preferences LEFT JOIN gz_preferences gp ON gp.code = pref_code WHERE dogovor_gz_id = '$dgz_id'");
	//AVD
    //$dogovor_gz['gz_placing_way'][] = sql_array(sql_query("SELECT gz_placing_way as code, concat('/',gpw.code,'/ ',gpw.name) as name FROM dogovor_gz_placing_way LEFT JOIN gz_placing_way gpw ON gpw.code = gz_placing_way WHERE dogovor_gz_id = '$dgz_id'"));
	$dogovor_gz['gz_placing_way'][] = sql_array(sql_query("SELECT code, CONCAT('/', gpw.code, '/ ', gpw.name) AS name FROM gz_placing_way gpw WHERE code = '${dogovor_gz['gz_placing_way_code']}'" ));
	//
	//$dogovor_gz['gz_change_reason'] = (isset($dogovor_gz['gz_change_reason_id']) && $dogovor_gz['gz_change_reason_id']) ? $dogovor_gz['gz_change_reason_id'] : NULL;
	$dogovor_gz['gz_change_reason'][] = (isset($dogovor_gz['gz_change_reason_id']) && $dogovor_gz['gz_change_reason_id']) ? array('code'=>$dogovor_gz['gz_change_reason_id'],'name'=>'testtt') : NULL;
    foreach($prefs as $pref) {
        $dogovor_gz['gz_preferences'][] = array('code'=>$pref['pref_code'], 'name'=>$pref['name']);
    }
    foreach($okveds as $okved) {
        $dogovor_gz['okved'][] = array('code'=>$okved['code'], 'name'=>$okved['name']);
    }
    return $dogovor_gz;
}

function get_dogovor_gz_positions($dogovor_gz_id) {
    $positions = sql_rows("SELECT okpd as okpd_code, concat('/',okpd.code,'/ ',okpd.name) as okpd_name, gz_okei as gz_okei_code, concat('/',go.code,'/ ',go.name) as gz_okei_name, quantity, sum FROM dogovor_gz_position LEFT JOIN okpd ON okpd.code = dogovor_gz_position.okpd LEFT JOIN gz_okei go ON go.code = gz_okei WHERE dogovor_gz_id = '$dogovor_gz_id'");
    foreach($positions as &$position) {
		$position['okpd'] = array();
        $position['okpd'][] = array('code'=>$position['okpd_code'], 'name'=>$position['okpd_name']);
        $position['gz_okei'] = array();
		$position['gz_okei'][] = array('code'=>$position['gz_okei_code'], 'name'=>$position['gz_okei_name']);
    }
    return $positions;
}

$contract_max_pice = sql_get_value('sum(amount) as sum', 'dogovor_sostav', "dogovor_id = '$id' GROUP BY dogovor_id");
?>

<div class="content-pane" id="dogovor-gz">
<?php 
//<div class="content-pane" id="dogovor-gz" style="width: 70%">
	$action = (isset($_GET['action'])) ? $_GET['action'] : NULL;
	if($page == 'dogovor_gz' && $action == NULL) require $_SERVER['DOCUMENT_ROOT'] . '/gz/dogovor_gz.php';
	else if($page == 'dogovor_gz' && $action == 'edit') require $_SERVER['DOCUMENT_ROOT'] . '/gz/dogovor_gz_edit.php';
?>
</div>

<?php
//--------------------------------------------------------------------------
if ($id <> NEW_ID){
    if($page == 'dogovor_files') {
        echo '<div class="content-pane" id="dogovor_files">';
    		echo '<h1>Файлы (Договор ID: '.$id.')</h1>';
            //Прикрепленные файлы и поля для загрузки
            echo html_out_attached_files($id, $table);
            //Подключаем форму для загрузки файлов
            html_out_load_form();
        echo '</div>';
    }
    if($page == 'dogovor_sostav') {
        echo '<div class="content-pane" id="dogovor_sostav">';
        	echo '<h1>Состав договора (Договор ID: '.$id.')</h1>';
            if ($bad_summ)  {
                echo "При расчете остатков не учитываются заявки на сумму: <a target=\"_blank\" href=\"/tedit.php?t=$bad_zs_view&f_dogovor_id=$id\" style=\"color:red\"> $bad_summ </a>";
            }
            $T_sostav=TD_dogovor_sostav($T->may_edit, $id);
        	$T_sostav->out_js();
        echo '</div>';
    }
    if($page == 'dogovor_zayav') {
        echo '<div class="content-pane" id="dogovor_zayav">';
        	echo '<h1>Прикрепленные заявки (Договор ID: '.$id.')</h1>';
        	TD_zayav_dog($id)->out_js();
        echo '</div>';
    }
    if($page == 'dogovor_plat') {
    	echo '<div class="content-pane" id="dogovor_plat">';
            echo '<h1>Прикрепленные платежки (Договор ID: '.$id.')</h1>';
            TD_plat_out($id)->out_js();
        echo '</div>';
    }
}
echo '</div>';

?>

<?php if($page == 'dogovor'): ?>
    <script>
        dogovorControlsOperation();
    </script>
<?php endif; ?>

<script>
	//AVD
	'use strict';

	window.addEventListener('load', function() 
	{
		var srcCtrlDate = document.querySelector('input[name="date_start"]');
		var trgCtrlDate = document.querySelector('input[name="date_end"]');
		/* 
		//NB!!!
		//Загрузка справочника avd.holiday перенесена в header.php
		var holiday = [];
		var xhr = new XMLHttpRequest();
		xhr.open('GET', '/ListM.php', false);
		xhr.send();
		if (xhr.status == 200) 
		{
			var tmp_holiday = JSON.parse(xhr.responseText);
			var holiday = [];
			for (var h = 0; h < tmp_holiday.length; h++)
			{
				var dh = new Date(tmp_holiday[h].date);
				dh.setHours(0, 0, 0, 0);
				holiday[dh] = {};
				holiday[dh].description = tmp_holiday[h].description;
				holiday[dh].workday = +tmp_holiday[h].workday;
			}
		}
		*/
		if (srcCtrlDate && trgCtrlDate)
		{
			var p = trgCtrlDate.parentNode;
			trgCtrlDate.style.width = '100px';
			var coords = trgCtrlDate.getBoundingClientRect();
			var btnCalc = document.createElement('input');
			btnCalc.type = 'button';
			btnCalc.value = 'Вычислить дату';
			btnCalc.style.position = 'relative';
			btnCalc.style.top = '-1px';
			btnCalc.style.left = '2px';
			btnCalc.style.height = (coords.bottom - coords.top) + 'px';
			p.appendChild(btnCalc);
			var openedDiv;
			btnCalc.onclick = function()
			{
				if (openedDiv) 
					document.body.removeChild(openedDiv);
				var div = document.createElement('div');
				openedDiv = div;
				div.className = 'php-dialog';
				var target = btnCalc;
				var head = div.appendChild(document.createElement('p'));
				head.align = 'center';
				head.innerHTML = btnCalc.value;
				head.style.fontWeight = 'bold';
				avd.makeElementMoveable(div);
				var tbl = document.createElement('table');
				div.appendChild(tbl);
				//Дата начала
				var tr = document.createElement('tr');
				tbl.appendChild(tr);
				var td = document.createElement('td');
				tr.appendChild(td);
				td.appendChild(document.createTextNode('Исходная дата: '));
				var td = document.createElement('td');
				tr.appendChild(td);
				var srcDate = document.createElement('input');
				srcDate.type = 'text';
				srcDate.value = srcCtrlDate.value;
				srcDate.style.width = '70px';
				td.appendChild(srcDate);
				//Поставка
				var tr = document.createElement('tr');
				tbl.appendChild(tr);
				var td = document.createElement('td');
				tr.appendChild(td);
				td.appendChild(document.createTextNode('Поставка: '));
				var td = document.createElement('td');
				tr.appendChild(td);
				var supplyType = document.createElement('select');
				td.appendChild(supplyType);
				var o = document.createElement('option');
				o.value = 1;
				o.text = 'рабочих дней';
				supplyType.appendChild(o);
				var o = document.createElement('option');
				o.value = 2;
				o.text = 'календарных дней';
				supplyType.appendChild(o);
				td.appendChild(document.createTextNode(' '));
				var supplyDays = document.createElement('input');
				supplyDays.type = 'number';
				supplyDays.min = 0;
				td.appendChild(supplyDays);
				supplyDays.value = '15';
				supplyDays.style.width = '40px';
				var coords = supplyDays.getBoundingClientRect();
				supplyType.style.height = '21px';
				//Оплата
				var tr = document.createElement('tr');
				tbl.appendChild(tr);
				var td = document.createElement('td');
				tr.appendChild(td);
				td.appendChild(document.createTextNode('Оплата: '));
				var td = document.createElement('td');
				tr.appendChild(td);
				var paymentType = document.createElement('select');
				td.appendChild(paymentType);
				var o = document.createElement('option');
				o.value = 1;
				o.text = 'рабочих дней';
				paymentType.appendChild(o);
				var o = document.createElement('option');
				o.value = 2;
				o.text = 'календарных дней';
				paymentType.appendChild(o);
				td.appendChild(document.createTextNode(' '));
				var paymentDays = document.createElement('input');
				paymentDays.type = 'number';
				paymentDays.min = 0;
				td.appendChild(paymentDays);
				paymentDays.value = '15';
				paymentDays.style.width = '40px';
				var coords = paymentDays.getBoundingClientRect();
				paymentType.style.height = '21px';
				//Вычисление
				var tr = document.createElement('tr');
				var calcLogRow = tr;
				calcLogRow.hidden = false;
				tbl.appendChild(tr);
				var td = document.createElement('td');
				tr.appendChild(td);
				td.appendChild(document.createTextNode('Вычисление: '));
				var td = document.createElement('td');
				tr.appendChild(td);
				var calcLogArea = document.createElement('textarea');
				calcLogArea.style.width = '181px';
				calcLogArea.style.height = '78px';
				//calcLogArea.style.width = '450px';
				//calcLogArea.style.height = '300px';
				td.appendChild(calcLogArea);
				//Результат
				var tr = document.createElement('tr');
				tbl.appendChild(tr);
				var td = document.createElement('td');
				tr.appendChild(td);
				td.appendChild(document.createTextNode('Результат: '));
				var td = document.createElement('td');
				tr.appendChild(td);
				var resultDate = document.createElement('input');
				resultDate.type = 'text';
				resultDate.style.width = '70px';
				td.appendChild(resultDate);
				//Блок кнопок
				var p = div.appendChild(document.createElement('p'));
				p.align = 'center';
				//Кнопка Вычисление
				var btnPreCalc = document.createElement('input');
				btnPreCalc.type = 'button';
				btnPreCalc.value = 'Вычисление';
				p.appendChild(btnPreCalc);
				btnPreCalc.onclick = function() 
				{
					//calcLogRow.hidden = !calcLogRow.hidden;
					//Загрузка справочника праздников
					var  log = 'Справочник праздничных выходных дней загружен\n';
					//
					//alert(this.value);
					//alert(e.target.value);					
					//alert(e.value);
					//
					log += 'Начальная дата ' + srcDate.value;
					log += '\n';
					if (avd.isDate(srcDate.value))
					{
						var stepDate = avd.strToDate(srcDate.value);
						//log += 'Начальная дата введена корректно\n';
						var period = 
						[
							{
								stepType: +supplyType.value,
								stepTypeDescription: supplyType.options[+supplyType.value - 1].textContent,
								stepDays: +supplyDays.value,
								stepDescription: 'Поставка'
							},
							{
								stepType: +paymentType.value,
								stepTypeDescription: paymentType.options[+paymentType.value - 1].textContent,
								stepDays: +paymentDays.value,
								stepDescription: 'Оплата'
							},							
						];
						//log += avd.parseObj(period);
						period.forEach(function(p)
						{
							log += p.stepDescription + ', ' + p.stepTypeDescription + ' - ' + p.stepDays.toString();
							log += '\n';
							if (p.stepType == 2)
								stepDate.setDate(stepDate.getDate() + p.stepDays);
							else 
							{
								for (var ii = 0; ii < p.stepDays;)
								{
									//Прибавляем 1 день
									stepDate.setDate(stepDate.getDate() + 1);
									//log += avd.dateToStr(stepDate) + ' - ' + avd.getWeekDay(stepDate) + '\n';
									var workday;
									var daydesc = avd.getWeekDay(stepDate);
									//Найдём в справочнике stepDate
									if (avd.holiday[stepDate])
									{
										workday = avd.holiday[stepDate].workday;
										daydesc += ', ' + avd.holiday[stepDate].description;
										log += avd.dateToStr(stepDate) + ' - ' + daydesc + ', ' + (workday ? 'рабочий день\n' : 'выходной\n');
									} 
									else 
										workday = !(stepDate.getDay() == 0 || stepDate.getDay() == 6);
									if (workday) 
										ii++;
								}		
							}									
						});
						log += 'Результат: ' + avd.dateToStr(stepDate) + ' - ' + avd.getWeekDay(stepDate);
						resultDate.value = avd.dateToStr(stepDate);
					}
					else
						log += 'Начальная дата введена некорректно\n';
					calcLogArea.textContent = log;							
				};
				//
				p.appendChild(document.createTextNode(' '));
				//Кнопка ОК
				var btnOK = document.createElement('input');
				btnOK.type = 'button';
				btnOK.value = 'ОК';
				p.appendChild(btnOK);
				btnOK.onclick = function() 
				{
					trgCtrlDate.value = resultDate.value;
					document.body.removeChild(div); 
					openedDiv = null;
				};
				//
				p.appendChild(document.createTextNode(' '));
				//Кнопка Отмена
				var btnCancel = document.createElement('input');
				btnCancel.type = 'button';
				btnCancel.value = 'Отмена';
				p.appendChild(btnCancel);
				btnCancel.onclick = function() 
				{ 
					document.body.removeChild(div); 
					openedDiv = null;
				};
				//
				document.body.appendChild(div);
				$(srcDate).datepicker({beforeShowDay: avd.highlightDays});
				$(resultDate).datepicker({beforeShowDay: avd.highlightDays});
				var coords = btnCalc.getBoundingClientRect();
				var left = coords.left;
				var top = coords.top - div.offsetHeight;
				div.style.position = "fixed";
				div.style.left = left + 'px';
				div.style.top = top + 'px';
			};
		}
	//alert(t);
	});
	
</script>
<?php
include 'template/left_sidebar/dogovor.php';
include 'template/footer.php';
?>
