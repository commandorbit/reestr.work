<?php
require_once $_SERVER['DOCUMENT_ROOT'] . '/init.php';
require_once($_SERVER['DOCUMENT_ROOT'].'/template/header.php');


$kvd = request_val('KVD',0);
$y = request_numeric_val('y',2016);
$years = ['2016'=>2016,'2017'=>2017];

$kvds = ['0'=>''];
$kvds += OneC::enum_options('КВД');

require "/home/dekanat/modules/controls/controls.php";
$c = new controls();
$TD = newTD('обязательства',['f_KVD'=>$kvd,'f_y'=>$y]);
$fld_y = ["Сумма_СУП_Год([0-9])","Разница_Год([0-9])","Сумма_1С_Год([0-9])"];
foreach($TD->columns as $fld=>$dummy) {
	foreach ($fld_y as $reg) {
		$matches = [];
		if (preg_match("/".$reg."/",$fld,$matches)) {
			$label = $TD->columns[$fld]['label'];
			$n = $matches[1];
			$label = str_replace('Год'.$n, $y+$n,$label);
			$TD->columns[$fld]['label'] = $label;
		}
	}
}

?>
<h1>Обязательства за <?=$y?> г. (Кредит счетов 502.11 502.21 502.31)</h1>
<form method="GET">
<div style="text-align:center;margin-top:20px">
		<label>
			ИФ:
			<? $c->print_select('KVD',$kvds,$kvd)?>
		</label>
		<label>
			Год:
			<? $c->print_select('y',$years,$y)?>
		</label>
		<button>Ок</button>
</div>		
</form>	
<div>
<? $TD->out_js(); ?>
</div>	
