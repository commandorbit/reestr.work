<?php
require_once $_SERVER['DOCUMENT_ROOT'] . '/init.php';
require_once($_SERVER['DOCUMENT_ROOT'].'/template/header.php');


$kvd = request_val('KVD',0);
$kvds = ['0'=>''];
$kvds += OneC::enum_options('КВД');
$y = request_numeric_val('y',2016);
$years = ['2016'=>2016,'2017'=>2017];


require "/home/dekanat/modules/controls/controls.php";
$c = new controls();
$TD = newTD('денежные_обязательства',['f_KVD'=>$kvd,'f_y'=>$y]);
?>
<h1>Денежные обязательства за <?=$y?> г. (Кредит счета 502.12)</h1>
<form method="GET">
<div style="text-align:center;margin-top:20px">
		<label>
			ИФ:
			<? $c->print_select('KVD',$kvds,$kvd)?>
		</label>
		<label>
			Год:
			<? $c->print_select('y',$years,$y)?>
		</label>		
		<button>Ок</button>
</div>		
</form>	
<div>
<? $TD->out_js(); ?>
</div>	
