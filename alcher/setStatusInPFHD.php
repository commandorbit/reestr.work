<?php
require $_SERVER['DOCUMENT_ROOT'].'/init.php';
require $_SERVER['DOCUMENT_ROOT'].'/modules/save.php';

$user_id = $_SESSION['user_id'];

if (is_admin()) {
	sql_query("START TRANSACTION;");
	
	foreach($_POST['dogovor_id'] as $_id) {
		$id = (int) $_id;
		$row = sql_get_array("dogovor","id = $id");
		$row['dogovor_status_id'] = 4; //  включено в ПФХД
		save ('dogovor',$row);
	}
	sql_query("COMMIT;");
	$resp = ['status'=>"OK","msg"=>"Нет"];
	
} else {
	$resp = ['status'=>"ERROR","msg"=>"Нет прав!"];
}

echo json_encode($resp);
