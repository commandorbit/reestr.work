<?php
$fname = __DIR__ . "/1.png";

$im = imagecreatefrompng($fname);
$width = imagesx($im);
$height = imagesy($im);
for ($i=0;$i<$width;$i++) {
	echo first_color($im,$i,$height,'R') . "\t" . first_color($im,$i,$height,'B') ."\n";
}

function first_color($im,$x,$maxY,$color) {
	$koeff = 1.2;
	for ($y=0;$y<$maxY;$y++) {
		$rgb = imagecolorat($im, $x, $y);
		
		$r = ($rgb >> 16) & 0xFF;
		$g = ($rgb >> 8) & 0xFF;
		$b = $rgb & 0xFF;
		
		if ($color=='R' && $r>128 && $r>$g*$koeff && $r>$b*$koeff) {
			return $y;
		} elseif($color=='B' && $b>128 && $b>$r*$koeff && $b>$g*$koeff) {
			return $y;
		}
	}
	return -1;
}
var_dump($r, $g, $b);