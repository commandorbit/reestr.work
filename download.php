<?php
header('Content-Type: text/html; charset=utf-8');

error_reporting(E_ALL);
include 'init.php';
$id = sql_escape($_GET['id']);
$table = sql_escape($_GET['table']);

if(file_exists(sql_get_value('path',$table,"id = $id").sql_get_value('filename',$table,"id = $id"))) {
	header('Content-type: application/octet-stream');
	header('Content-Disposition: attachment; filename="'.sql_get_value('filename',$table,"id = $id").'"');
	readfile(sql_get_value('path',$table,"id = $id").sql_get_value('filename',$table,"id = $id"));
} else {
	echo "Файл не найден!";
}