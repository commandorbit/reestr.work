<?php

require 'init.php';
require 'modules/save.php';
require 'soft_delete.php';
require 'modules/attaching_files.php';
require 'modules/controls.php';
require 'modules/option_list.php';
require dirname(__FILE__) . '/doc_lim_fn.php';
require dirname(__FILE__) . '/upload_file_fn.php';

header('Content-Type: text/html; charset=utf-8');

define('NEW_ID','new');
define('TEXT_IS_APPROVED', 'утвержден');
define('TEXT_IS_NOT_APPROVED', 'не утвержден');

if (!isset($_GET['id']) ) {
	header ('Location: index.php');
	exit;
} 

$title = "Изменение лимитов";
$id = sql_escape($_GET['id']);
$table = 'doc_lim_change';
$ref_key = 'doc_lim_change_id';

$is_approved = sql_get_value('is_approved', 'doc_lim_change', "id='$id'");
$approved_txt = ($is_approved) ? TEXT_IS_APPROVED : TEXT_IS_NOT_APPROVED;

$T = newTD($table, array('f_id'=>$id));
if($is_approved) $T->may_edit = 0;
if(empty($T->data) && $id != NEW_ID) die("<b><h3>К сожалению, запрошенная запись не найдена или недоступна для Вас, возможно она была удалена или перемещена.</h3></b> <br/>У Вас возникли вопросы? Тел.: 1024 (Борисов Деннис Сергеевич)");
$data = ($id!==NEW_ID) ? $T->data[0] : null;
include 'template/header.php';

if (isset($_POST['save_parent'])) {
	$id = save($table,$_POST);
	$_GET['id'] = $id;
    $_SESSION['USER_MESSAGE']['SUCCESS'] = 'Документ успешно сохранен!';
	refresh_after_save(); // перенавправляем На id
} else if (isset($_POST['uploaded']) && !$is_approved){
    doc_lim_file_upload($id);
} else if(isset($_POST['doc_approve']) && user_may_approve_limits()) {
	apply_limits($id);
}

?>

<script src="/assets/js/multiselect-plugin/modalWindowBody.js"></script>
<script src="/assets/js/multiselect-plugin/multiselect.js"></script>
<script src="/assets/js/tree-plugin/js/treeplugin.js"></script>

<?php

$disabled = ($T->may_edit) ? '' : 'disabled';

echo '<h1>' . $title . '</h1>';

if($id !== NEW_ID) echo "<h3>Статус лимита: $approved_txt</h3>";
else echo "<h3>Новый документ</h3>";

echo '<form method="post" action="" class="custom-buttons">';
	doc_lim_out($T, $data);
	echo '<div class="action_sidebar">';
	    if($id == 'new') {
	       echo '<p class="save_parent clearfix"><input '.$disabled.' type="submit" name="save_parent" value="Сохранить">';
	    } else {
	        echo '<p class="save_parent clearfix"><input '.$disabled.' type="submit" name="save_parent" value="Сохранить">&nbsp;&nbsp;&nbsp;';
			if(!user_may_approve_limits()) $disabled = 'disabled';
			echo '<input '.$disabled.' type="submit" name="doc_approve" onclick="return confirmBox(\'Вы действительно хотите утвердить лимит?\')" value="Утвердить">';
	    }
	echo '</div>';
echo '</form>';

if($id !== NEW_ID) {
	echo '<div id="dogovor_files">';
	    //Прикрепленные файлы и поля для загрузки
	    echo html_out_attached_files($id, $table, $T->may_edit);

	    //Подключаем форму для загрузки файлов
	    if(!$is_approved) echo files_form_out();
	echo '</div>';
	
	$T_sostav=doc_lim_sostav_out($T->may_edit, $id);
	echo '<script src="/td_reestr/js/edit-box-init-doc-lim.js"></script>';
	$T_sostav->out_js();
}
?>
