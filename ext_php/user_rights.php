<?php
require_once $_SERVER['DOCUMENT_ROOT'] . '/modules/functions.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/modules/reestr_functions.php';

function may_edit_table($table) 
{
	//return true; // если можешь хотя бы одну запись
	return may_edit_table_row($table, 0);
}

function may_edit_table_row($table, $id) 
{ 
	return user_may_edit_table($table);
}

?>