<?php
require_once $_SERVER['DOCUMENT_ROOT'] . '/modules/sql_connect.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/modules/sql.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/ext_php/user_rights.php';

if (isset($_REQUEST['table']))
	$table = $_REQUEST['table'];
else
	$table = NULL;

if (isset($_REQUEST['id']))
	$id = $_REQUEST['id'];
else
	$id = NULL;

if ($table)
{
	db_connect();
	$table = sql_escape($table);
	$id = sql_escape($id);
	$result = false;
	if ($id)
	{
		$result1 = may_edit_table($table);
		if ($result1)
		{
			$result2 = may_edit_table_row($table, $id);
			echo json_encode(['fn' => 'may_edit_table_row', 'result' => $result2, 'table' => $table, 'id' => $id]);
		}
		else
			echo json_encode(['fn' => 'may_edit_table', 'result' => $result1, 'table' => $table, 'id' => $id]);
	}
	else
	{
		$result = may_edit_table($table);
		echo json_encode(['fn' => 'may_edit_table', 'result' => $result, 'table' => $table, 'id' => $id]);
	}
}

?>