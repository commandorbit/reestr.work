<?php

//Получить описание таблицы
function get_table_name($table)
{
	$sql = "SELECT short_name FROM tab WHERE `table` = '$table'";
	$r = sql_rows($sql);
	if ($r)
		return $r[0]['short_name'];
	else 
		return $table;
}

//Существует ли таблица
function avd_table_exists($table)
{
	global $DB;
	if (sql_table_exists($table))
		return true;
	else if ($DB::is_virtual_table($table))
		return true;
	else
		return false;
}

//Получить колонки таблицы
function avd_table_columns($table)
{
	global $DB;
	if (!avd_table_exists($table))
		$columns = [];
	else if ($DB::is_virtual_table($table))
		$columns = $DB::virtual_table_columns($table);
	else
		$columns = sql_table_columns($table);
	//NB!!!!
	//$c = $columns[0];
	//$c = ['name' => '_npp_', 'type' => 'int', 'length'=>11, 'null' => null, 'key' => null, 'extra' => null, 'default' => null];	$columns['_npp_'] = $c;
	//
	foreach ($columns as &$c) {
		$precision = null;
		if ($c['length'] && $tmp = explode(',',$c['length'])) {
			if (count($tmp) == 2) {
				$precision = $tmp[1];
			}
			$c['precision'] = $precision;
		}
	}
	return $columns;
}

?>