<?php

define('SQL_NO_TRIGGER_ERRROR',true);

error_reporting (E_ALL | E_STRICT);
//session_start();
require_once $_SERVER['DOCUMENT_ROOT'] . '/init.php';
//require_once $_SERVER['DOCUMENT_ROOT'] . '/modules/reestr_functions.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/reestr_db.php';
//NB!!!
require_once $_SERVER['DOCUMENT_ROOT'] . '/modules/sql_connect.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/modules/sql.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/modules/avdfunctions.php';
//NB!!!
require_once $_SERVER['DOCUMENT_ROOT'] . '/ext_php/user_rights.php';

require_once $_SERVER['DOCUMENT_ROOT'] . '/modules/save.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/soft_delete.php';
//NB!!!
require_once $_SERVER['DOCUMENT_ROOT'] . '/ext_php/common.php';

//NB!!!
function sql_table_list() 
{
	$sqltext = "SHOW TABLES";
	$r1 = sql_rows($sqltext);
	$r = [];	
	foreach($r1 as $i => $v)
	{
		$newrow = [];
		//var_dump($v);
		foreach($v as $ii => $vv)
			$v0 = $vv;
		$newrow['table_name'] = $v0;
		$r[] = $newrow;
	}	
	return $r;
}

db_connect();

if (isset($_REQUEST['table']))
{
	$table = sql_escape($_REQUEST['table']);
	//NB!!!
	$table = explode('/', $table)[0];
}
else
	//exit;
	$table = 'holiday';

if (isset($_REQUEST['prefix']))
	$table = $_REQUEST['prefix'] . $table;

//05.07.2017
if (isset($_REQUEST['root']))
	$root = sql_escape($_REQUEST['root']);
else
	if ($table == 'holiday')
		$root = '';
	else
		if (isset($_REQUEST['columns']))
			$root = 'columns';
		else
			$root = 'records';		
		
$message = "";
$success = true;
$records = [];

$table = sql_escape($table);

/*
if (!sql_table_exists($table))
{
	if ($DB::is_virtual_table($table))
	{
		//$message .= "Таблица $table виртуальная.";
		$columns = $DB::virtual_table_columns($table);
		//var_dump($columns);
		//die();
	}
	else
	{
		//$success = false;
		//$message .= "Таблица $table не существует.";
		$columns = [];
	}
}	
else
	$columns = sql_table_columns($table);
*/
 
$columns = avd_table_columns($table);

//echo avd_table_exists($table); die();
//echo '<pre>'; var_dump($columns); die();

if (isset($_REQUEST['queryfield']))
	$queryfield = $_REQUEST['queryfield'];
else
	$queryfield = 'name';

if (isset($_REQUEST['idfield']))
	$idfield = $_REQUEST['idfield'];
else
	$idfield = 'id';

//--------------------------------------------
function isIntArray(array $array) 
{
    foreach ($array as $a => $b) 
	{
        if (!is_int($a)) 
		{
            return false;
        }
    }
    return true;
}
//--------------------------------------------

$table_name = get_table_name($table);

if ($_SERVER['REQUEST_METHOD'] === 'POST' || $_SERVER['REQUEST_METHOD'] === 'PUT' || $_SERVER['REQUEST_METHOD'] === 'DELETE' ) 
{
	//ob_start();
	// Получение данных с клиента на сервер
	//var_dump($_POST);
	//echo "$table\n\n";
	//echo "table = $table\n";
	
	//-----------------------------------------------------
	if (substr($table, 0, 2) == 'v_')
	{
		$alttable = substr($table, 2);
		if (sql_table_exists($alttable))
			$table = $alttable;
	}
	//-----------------------------------------------------
	if ($table == 'virt_zayav_dog') $table = 'zayav';
	//-----------------------------------------------------
	
	
	$json = file_get_contents('php://input');
	$srcdata = json_decode($json,true);
	// Ассоциативный - одна запись
	if (!isIntArray($srcdata)) 
		$dataarray = [$srcdata];
	else 
		$dataarray = $srcdata;
	
	$virt_tables = ['virt_smeta_result'];
	if (in_array($table,$virt_tables))	
	{
		proccess_virt_table_data($table,$dataarray,$idfield);
		
	}
	elseif (!sql_table_exists($table))
	{
		$alttable = str_replace('v_', '', $table);
		if (sql_table_exists($alttable))
			$table = $alttable;
		$alttable = str_replace('_limited', '', $alttable);
		//if (sql_table_exists($alttable))
			$table = $alttable;
	}

	
		
	
	if (!sql_table_exists($table))
	{
		$message = "Таблица $table не существует";
		$success = false;
	}
	else if (!may_edit_table($table))
	{
		$message = "У вас нет прав на редактирование таблицы $table_name";
		$success = false;
	}
	else
	{
		$records = [];
		$success = true;
		$message = '';
		sql_query("START TRANSACTION");
		//////////////////////////////
		//var_dump($dataarray);
		//die();
		//////////////////////////////
		//if (0) 
		foreach($dataarray as $data)
		{
			$idvalue = $data[$idfield];
			if ($idvalue < 0) 
				$data[$idfield] = null;			
			try 
			{
				if ($idvalue < 0) 
					$messageplus = "Добавление в таблицу $table_name новой записи - ";
				else
					$messageplus = "Обновление в таблице $table_name записи $idvalue - ";
				
				$cm = 0;
				
				if (is_array($data)) //NB!!!
				foreach($data as $i => $v)
				{
					//$message .= "$i => $v<br>";
					if ($i != $idfield) 
					{
						$cm++;
						if (isset($columns[$i]))
						{
							if (in_array($columns[$i]['type'],['date','timestamp','datetime']))
							{
								$data[$i] = date("Y-m-d H:i:s", $v);
							}
							//////////////////////////////////////////////////
							//////////////////////////////////////////////////
							if ($columns[$i]['type'] == 'int' && $v == 0) $data[$i] = '_null_'; // NULL но сейчас блядство в save.php fill_row 
						//	if ($columns[$i]['type'] == 'tinyint' && $v == null) $data[$i] = 0;
							//////////////////////////////////////////////////
							//////////////////////////////////////////////////
						}
					}
				}				
				///////////////////////////////
				$save_error = "NB!!! ";
				if ($cm)
				{
					$new_id = null;					
					try
					{
						$new_id = save($table, $data, $save_error);
						//NB!!!!!!! AVD
						if ($new_id)
						{
							//ДЛЯ СОХРАНЕНИЯ СПРАВОЧНИКОВ БЕЗ auto_increment
							/*
							$sql = "SELECT $idfield FROM $table WHERE $idfield = '$new_id'";
							$rr = sql_rows($sql);
							if (!$rr || !count($rr))
							{
								$sql = "INSERT INTO $table ($idfield) VALUES($new_id)";
								$success &= sql_query($sql);
								//$message .= "!!!!!!!!!!!!!!!!!\n";
								if ($success)
									$new_id = save($table, $data, $messageplus);
							}
							*/
						}
						//В случае, если $new_id не вернули...
						if (!$new_id && $idvalue > 0)
							$new_id = $idvalue;
					}
					catch (Exception $e) 
					{
						$success = false;
						$message .= $e->getMessage() . "\n" ;
					}		
					
					if ($idvalue < 0) 
					{
						$data['clientId'] = $idvalue;
						$data[$idfield] = $new_id;
					}
					$message .= $messageplus;
					$success = $success && $new_id;
					
					if ($new_id)
						$message .= "успешное выполнение!\n";// . $sql;
					else
						$message .= "ошибка: ". (/*sql_error() ||*/ $save_error . " - " . $new_id) . "\n";
				}
				else
				{
					$message .= "Удаление из таблицы $table_name записи $idvalue - ";
					$messageplus = "ОК";
					$del_ok = del_row($table, $idvalue, $messageplus);
					if (!$del_ok)
					{
						$message .= "ошибка: $messageplus\n";
						$success = false;
					}
					else
						$message .= "успешное выполнение! $table, $idvalue, $messageplus\n";
				}

				if ($cm)
					$records[] = $data; 
			} 
			catch (Exception $e) 
			{
				$success = false;
				$message .= $e->getMessage() . "\n" ;
			}		
		}
		if ($success) 
			sql_query("COMMIT");
		else
			sql_query("ROLLBACK");	
	}
	
	$message = str_replace("\n", "<br>", $message);
	if (substr($message, -4) == '<br>')
		$message = substr($message, 0, strlen($message) - 4);
	
	/*
	if ($table == 'smeta_sostav') {
		$records = bred_test($records);
	}
	*/
	echo json_encode(['success' => $success, 'message' => $message, $root => $records]);
	exit();	

//========================================================================================================================================================================	
/*
	if(0)
	{//--------------------------------------------------------------
		
	if ($success && $sql)
	{
		try 
		{
			$message .= $messageplus;
			$r = sql_execute($sql);
			$success = $success && $r;
			// insert operation			
			//if ($success && $idvalue < 0) 
			if ($r && $idvalue < 0)
			{
				$data->clientId = $idvalue;
				$data->id = sql_last_id();
			}
			//$message .= ", $idfield: $data->id";
			if ($r)
				$message .= " - успешное выполнение!\n";// . $sql;
			else
				$message .= " - ошибка:\n" . sql_error() . "\n" . $sql . "\n";
		} 
		catch (Exception $e) 
		{
			$success = false;
			$message .= $e->getMessage() . "\n" . $sql . "\n";
		}		
	}
	
	if ($r) 
	{
		//$data->name = '9999999';
		$records[] = $data;
	}		
	
	foreach($dataarray as $dataindex => $data)
	{
		//echo "dataindex = $dataindex\n";
		$idvalue = 0;
		$fieldlist = array();
		$valuelist = array();
		//var_dump($data);

		if (1)
	    {
		//save($table, $data);
		}
		else
		{
		//--------------------------------------------------------------------------------------------------------			
		foreach($data as $i => $v)
		{
			//$message .= "$i => $v<br>";
			if ($i == $idfield) 
				$idvalue = sql_escape($v);
			else
			{
				if (isset($columns[$i]))
			    {
					if ($columns[$i]['type'] == 'date' || $columns[$i]['type'] == 'timestamp')
					{
						$v = date("Y-m-d H:i:s", $v);
						$data->$i = $v;
					}
					//if (!is_null($v))
					$fieldlist[] = "`$i`"; //$i
					if ($v)
						$valuelist[] = "'" . sql_escape($v) . "'";
					else
					{
						if ($columns[$i]['type'] == 'tinyint')
							$valuelist[] = '0';
						else if ($columns[$i]['type'] == 'int')
							$valuelist[] = 'NULL';
						else
							//$valuelist[] = 'NULL'; //NB!!!
							$valuelist[] = "'" . sql_escape($v) . "'";
					}
				}
			}
		}
		//echo "$idfield = $idvalue\n";
		$exprlist = array();
		for ($i = 0; $i < count($fieldlist); $i++)
			$exprlist[] = "${fieldlist[$i]} = ${valuelist[$i]}";	
		$sql = "";
		if ($idvalue >= 0)
		{
			//if (sql_count_rows_by_id($table, $idfield, $idvalue))
			if (1)
			{
				if ($exprlist)
				{
					if (!may_edit_table_row($table, $idvalue))
					{
						$message .= "У вас нет прав на редактирование записи $idvalue таблицы $table_name\n";
						$success = false;
					}
					else 
					{
						$sql = "UPDATE $table SET " . implode(", ", $exprlist) . " WHERE $idfield = '$idvalue'";
						$messageplus = "Обновление таблицы $table_name";
					}
				}
				else
				{
					if (!may_edit_table_row($table, $idvalue))
					{
						$message .= "У вас нет прав на удаление записи $idvalue из таблицы $table_name\n";
						$success = false;
					}
					else 
					{
						$sql = "DELETE FROM $table WHERE $idfield = '$idvalue'";		
						$messageplus = "Удаление из таблицы $table_name";
					}
				}
			}
			else
			{
				array_push($fieldlist, $idfield);
				array_push($valuelist, "'$idvalue'");
				//$message .= "Необходимо добавить запись " . implode(", ", $fieldlist) . " <-- " . implode(", ", $valuelist); $success = false;
				$sql = "INSERT INTO $table (" . implode(", ", $fieldlist) . ") VALUES (" . implode(", ", $valuelist) . ")";
				$messageplus = "Добавление в таблицу $table_name";
			}
		}
		else
		{
			$sql = "INSERT INTO $table (" . implode(", ", $fieldlist) . ") VALUES (" . implode(", ", $valuelist) . ")";
			$messageplus = "Добавление в таблицу $table_name";
			//$message .= "<br><br>$sql<br><br>";
		}
		//--------------------------------------------------------------------------------------------------------
		}

		//$message .= "\n$sql\n";
		//$success = false;
		//$success = $success && sql_query($sql);

		$r = NULL;
		//$message .= "================";
		
		if ($success && $sql)
		try 
		{
			$message .= $messageplus;
			$r = sql_execute($sql);
			$success = $success && $r;
			// insert operation			
			//if ($success && $idvalue < 0) 
			if ($r && $idvalue < 0)
			{
				$data->clientId = $idvalue;
				$data->id = sql_last_id();
			}
			//$message .= ", $idfield: $data->id";
			if ($r)
				$message .= " - успешное выполнение!\n";// . $sql;
			else
				$message .= " - ошибка:\n" . sql_error() . "\n" . $sql . "\n";
		} 
		catch (Exception $e) 
		{
			$success = false;
			$message .= $e->getMessage() . "\n" . $sql . "\n";
		}		
		if ($r) 
		{
			//$data->name = '9999999';
			$records[] = $data;
		}	

		//echo "Результат: $success \n";
		//echo json_encode(['success' => false, 'message' => 'SOME ERROR!']);
		//echo 'Результат: ' . $result . "\n";
		//avd_printarrayvalues_r($data);
	}
	//$message = ob_get_clean();
	//ob_get_clean();
	$message = str_replace("\n", "<br>", $message);
	//$success = true;
	if ($success) 
		sql_query("COMMIT");
	else
		sql_query("ROLLBACK");	
	echo json_encode(['success' => $success, 'message' => $message, $root => $records]);
	
	} //-------------------------------------------------------------------------------------
	exit();
	*/	
}

//NB!!!
/*
if (!avd_table_exists($table))
{
	$table = 'user';
	unset($_REQUEST['where']);
	$_REQUEST['where'] = '1=0';
}
*/

$sqltext = "SELECT * FROM $table WHERE 1 = 1";

/*
if (isset($_REQUEST['id']))
{
	$id = sql_escape($_REQUEST['id']);
	$sqltext .= " AND id = '$id'";
}
*/
$filters = [];
//var_dump($_REQUEST); die();
foreach ($_REQUEST as $fld => $v)
{
	$spec_flds = ['use_in_editor'];
	$fld = sql_escape($fld);
	$v = sql_escape($v);

	//if (!in_array($i, Array('table', 'idfield', 'queryfield', 'query', '_dc', 'page', 'start', 'limit', 'test', 'callback', 'clientId')))
	//die($fld);
	if (in_array($fld, $spec_flds) || isset($columns[$fld]) && $fld != 'table') 
	{
		//echo " $fld => $v <br>";
		$filters[$fld] = $v == 'root' ? '_null_' : $v;
		if ($fld == 'parent_id') 
			$filters[$fld] = $v == '0' ? '_null_' : $v;		
		if ($v != 'root')
			$sqltext .= " AND $fld = '$v'";
		else
			$sqltext .= " AND $fld IS NULL";		
	}
}
//var_dump($filters); die();

if (isset($_REQUEST['where']))
	//$sqltext .= ' AND ' . sql_escape($_REQUEST['where']);
	$sqltext .= ' AND ' . $_REQUEST['where'];

if (isset($_REQUEST['query']))
{
	$q = sql_escape($_REQUEST['query']);
	$filters[$queryfield] = '_LIKE_' . $q;
	//$sqltext .= " AND $queryfield like '%$q%'";
}

//die($sqltext);

if (isset($_REQUEST['orderby']))	
	$sqltext .= ' ORDER BY ' . sql_escape($_REQUEST['orderby']);
else
	if (isset($columns['ord']))  {
		$sqltext .= " ORDER BY ord";
	}  else if (isset($columns['name'])) {
	    $sqltext .= " ORDER BY name";
		//$_REQUEST['orderby'] = $_REQUEST['orderby'] . ' name'
	}
 	
//die($sqltext);

$r = [];

//NB!!!!!!!!!!!!!!

if (!avd_table_exists($table))
{
	//$success = false;
	//$message .= "Таблица $table не существует.";
}

if (avd_table_exists($table))//NB!!!
if ($success)
{
	if (isset($_REQUEST['select']))
	{
		if (isset($_REQUEST['columns']))
			$r = $columns;
		else if (isset($_REQUEST['tables']))
			$r = sql_table_list();
		else
			$r = sql_rows($sqltext);
	}
	else
	{
		//Вместо этого...
		$p2 = explode("WHERE", $sqltext)[1];
		$where_clause = explode("ORDER BY", $p2)[0];
		if (isset(explode("ORDER BY", $p2)[1]))
			$orderby_clause = explode("ORDER BY", $p2)[1];
		else 
			$orderby_clause = "";

		list($order_by, $order_desc) = _get_order_by($_REQUEST);
		
		if ($table == 'fp_article') {
			//$filters['parent_id'] = "NULL";
		}

		
		if (isset($_REQUEST['columns']))
			$r = $columns;
		else if (isset($_REQUEST['tables']))
			$r = sql_table_list();
		else
			$r = ReestrDB::query($table, $filters, $order_by, $order_desc);
		/*	
		$r = [];
		while ($row = $res->fetch_array(MYSQLI_ASSOC)) 
		{
			$r[] = $row;
		}
		*/	
	}	
}
	
function _get_order_by($request) 
{
	global $columns;
	$desc = false;
	if (isset($request['orderby'])) 
		$ord_string = $request['orderby'];
	else
		if (isset($columns['ord']))
			$ord_string = 'ord';
		else if (isset($columns['name']))
			$ord_string = 'name';
		else
			$ord_string = '';

	//die('!!!' . $ord_string);
	if ($ord_string)
	{
		$ord_string = str_replace('  ', ' ', $ord_string);
		$ords = explode(' ', $ord_string);
		if (count($ords) == 2) 
			if (strtoupper($ords[1]) == 'DESC') 
				$desc = true;
		return [$ords[0], $desc];
	} 
	else
		return [null, false];
}

//var_dump($r);
//Очищаем колонки binary
//if ($columns[$i]['type'] == 'binary')

//$npp = 0;
if (!isset($_REQUEST['columns']))
	foreach ($r as &$row) 
	{
		//NB!!!!
		//$row['_npp_'] = ++$npp;
		//array_unshift($row, ++$npp);
		foreach ($row as $i => &$v) 
		{
			//if ($columns[$i]['type'] == 'binary') $v = bin2hex($v/*$row[$i]*/); 
			
				//$row[$i] = bin2hex($row[$i]);
				//$v = null;
			if ($i == 'parent_id') 
			{
				if (empty($v)) $v = 0;
				// ЗАПРЕТИЛИ ради v_fp_article_select
				if (!isset($row['leaf'])) $row['leaf'] = (sql_get_value('count(id)', $table, "parent_id='" . $row['id'] ."'") == 0);
				$row['loaded'] = true;
				// Обманем  EXTJS!!!
				//$row['parentId'] = $v;
				//unset($row['parent_id']);
			}
		}
	}	

// DEBUG
//if ($table == 'fp_article') { $r = array_slice($r, 0, count($r) - 5); }	
			
/*
foreach ($r as &$row) 
	if (isset($row['d'])) 
		$row['dd'] =  $row['d'] ? new DateTime($row['d']) : null;
*/

//var_dump($r); die();

//var_dump([$_REQUEST]);

//$message = 'Ура!!!'

if (isset($_REQUEST['test']))
{
	echo '<HTML><BODY>'; 
	if ($success)
		avd_print_assoc_array($r); 
	else
		echo($message);
	echo '</BODY></HTML>'; 
}	
else 
{
	
	if ($root != '')
		$rr = ['success' => $success, 'message' => $message, $root => $r];
	else
		$rr = $r;
	if (isset($_REQUEST['dump']))
	{
		echo '<HTML><BODY><PRE>'; 
		var_dump($rr);
		echo '</PRE></BODY></HTML>'; 
	}
	else
		echo json_encode($rr);
}
function proccess_virt_table_data(&$table,&$rows,&$idfield) {
	$out_row = [];
	if ($table == 'virt_smeta_result') {
		$cols = ['percent'=>'perc','sum_add'=>'sum_add','sum_credit'=>'sum_credit'];
		$idfield = 'smeta_id';
		$table = 'smeta_fin_param';
		foreach ($rows as $row) {
			$exploded = explode('-',$row['id']);
			$out_row['smeta_id'] = $exploded[0];
			foreach ($cols as $virt=>$fld) {
				if ($exploded[1]==$virt) {	
					for ($i=1; $i<=12; $i++) {
						if (isset($row['v'.$i])) $out_row[$fld.$i] = $row['v'.$i];
					}
				}
			}
		}
	}
	$rows = [$out_row];
}
?>