<?
require_once ($_SERVER['DOCUMENT_ROOT'] . '/init.php');
require_once ($_SERVER['DOCUMENT_ROOT'] . '/smeta_virt_result.php');
$body = file_get_contents('php://input');


$request = json_decode($body,true);
$row_credit = $row_percent = $row_sum_add = null;
$smeta_id = null;
foreach ($request['records'] as $row) {
	$exploded = explode('-',$row['id']);
	$smeta_id = (int) $exploded[0];
	$id = $exploded[1];
	if ($id=='percent') {
		$row_percent = $row;
	} else if ($id=='sum_add') {
		$row_sum_add = $row;
	} else if ($id =='sum_credit') {
		$row_credit = $row;
	}
}
$rows = SmetaVirtResult::getRows($smeta_id,$row_percent,$row_sum_add,$row_credit);
echo json_encode(['ver'=>$request['ver'],'records'=>$rows],JSON_UNESCAPED_UNICODE);




