<?php
define('SQL_NO_TRIGGER_ERRROR', true);
error_reporting (E_ALL | E_STRICT);
require_once $_SERVER['DOCUMENT_ROOT'] . '/init.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/modules/avdfunctions.php';
//require_once $_SERVER['DOCUMENT_ROOT'] . '/reestr_db.php';
//require_once $_SERVER['DOCUMENT_ROOT'] . '/modules/sql_connect.php';
//require_once $_SERVER['DOCUMENT_ROOT'] . '/modules/sql.php';
//require_once $_SERVER['DOCUMENT_ROOT'] . '/ext_php/user_rights.php';
//require_once $_SERVER['DOCUMENT_ROOT'] . '/modules/save.php';
//require_once $_SERVER['DOCUMENT_ROOT'] . '/soft_delete.php';

if (isset($_REQUEST['db']))
	db_connect();

if (isset($_REQUEST['root']))
	$root = sql_escape($_REQUEST['root']);
else
	$root = 'result';

//Получить описание таблицы
function get_table_name($table)
{
	$sql = "SELECT short_name FROM tab WHERE `table` = '$table'";
	$r = sql_rows($sql);
	if ($r)
		return $r[0]['short_name'];
	else 
		return $table;
}

//Получить атрибуты договора в системе Тезис
function get_thesis_attrs($soup_dogovor_id)
{
	//NB!!!
	require_once $_SERVER['DOCUMENT_ROOT'] . '/thesis/thesis_functions.php';
	//
	$thesis_attrs = [];
	$thesis_connect = thesis_connect();
	//Получаем данные по договору из БД Тезиса
	if ($soup_dogovor_id)
		$dogovor_clause = "AND INTEGER_VALUE = $soup_dogovor_id ";
	else
		$dogovor_clause = "";
	
	$thesis_guids = thesis_reestr_guids_sql();
	$sql = "SELECT wp.NAME as PROC_NAME, dd.NUMBER, wcp.STATE, wcp.PROC_ID, dd.CARD_ID, dd.COMMENT, dd.INCOME_DATE, ddc.AMOUNT FROM dbo.SYS_ATTR_VALUE av INNER JOIN dbo.DF_DOC dd  ON av.ENTITY_ID=dd.CARD_ID INNER JOIN dbo.DF_CONTRACT ddc ON dd.CARD_ID = ddc.CARD_ID INNER JOIN dbo.WF_CARD_PROC wcp ON wcp.CARD_ID = dd.CARD_ID INNER JOIN dbo.WF_PROC wp ON wp.ID = wcp.PROC_ID WHERE CATEGORY_ATTR_ID IN ($thesis_guids)  " . $dogovor_clause . "AND dd.VERSION_OF_ID is null ORDER BY dd.CREATE_TS desc, wcp.CREATE_TS DESC";
	$thesis_attrs_query = mssql_query($sql, $thesis_connect);
	while ($r = mssql_fetch_array($thesis_attrs_query))
		$thesis_attrs[] = $r;
	return $thesis_attrs;
}

//Получить статус в системе Тезис
function get_rus_current_status($soup_dogovor_id)
{
	//NB!!!
	require_once $_SERVER['DOCUMENT_ROOT'] . '/thesis/thesis_functions.php';
	//
	$thesis_attrs = get_thesis_attrs($soup_dogovor_id);
	$rus_current_status = (count($thesis_attrs) && !is_null($thesis_attrs[0]['STATE'])) ?  get_thesis_status_str($thesis_attrs[0]['STATE']) : NULL;
	return $rus_current_status;
}

function get_process($thesis_card_id, $proc_id)
{
	//NB!!!
	require_once $_SERVER['DOCUMENT_ROOT'] . '/thesis/thesis_functions.php';
	//
	$thesis_card_id = mssql_guid_string($thesis_card_id);
	$proc_id = mssql_guid_string($proc_id);
	
	$thesis_connect = thesis_connect();
	//header('Location: ' . $_SERVER['DOCUMENT_ROOT'] . '/thesis/thesis_ajax.php');
	
	$thesis_assignment_query = mssql_query("SELECT dwp.NAME as PROC_NAME, dwa.CREATE_TS, dwa.FINISHED, dsu.NAME as USER_NAME, dwa.NAME as STATE, dwa.ITERATION, dwa.OUTCOME, dwa.ASSIGNMENT_COMMENT FROM dbo.WF_ASSIGNMENT dwa LEFT JOIN dbo.SEC_USER dsu ON dsu.ID = dwa.USER_ID LEFT JOIN dbo.WF_PROC dwp on dwa.PROC_ID = dwp.ID LEFT JOIN dbo.DF_DOC dd ON dd.CARD_ID = dwa.CARD_ID WHERE dwa.CARD_ID = '$thesis_card_id' AND dwa.PROC_ID = '$proc_id'", $thesis_connect);	
	
	$r = [];
	
	while($assignment = mssql_fetch_array($thesis_assignment_query) ) 
	{
		$row = [];		
		foreach($assignment as $fld => $val) 
		{
			if ($fld == "STATE" || $fld == 'OUTCOME') 
				$val = get_thesis_status_str($val);
			$val = (!($timestamp = strtotime($val))) ? $val : date('d.m.Y H:i', $timestamp);
			$row[$fld] = $val;
		}
		$r[] = $row;
	}
	
	return $r;
}

function get_real_tablename($table)
{
	if (substr($table, 0, 2) == 'v_')
	{
		$alttable = substr($table, 2);
		if (sql_table_exists($alttable))
			$table = $alttable;
	}
	if (!sql_table_exists($table))
	{
		$alttable = str_replace('v_', '', $table);
		if (sql_table_exists($alttable))
			$table = $alttable;
		$alttable = str_replace('_limited', '', $alttable);
			$table = $alttable;
	}
	return $table;
}

function get_linked_tables($table, $id)
{
	$result = [];
	$table = get_real_tablename($table);
	$columns = sql_table_columns($table);
	$fieldlist = [];
	foreach($columns as $i => $v)
	{
		if ($i != 'id')
			$fieldlist[] = $i;
	}
	$lt = sql_rows('SHOW TABLES LIKE "'. $table.'_%"');	
	foreach($lt as $v) foreach($v as $vv) $linked_tables[] = $vv;
	$link_field = "{$table}_id";
	foreach($linked_tables as $t)
	{
		$columns = sql_table_columns($t);
		$fieldlist = [];
		$ok = false;
		foreach($columns as $i => $v)
			if ($i == $link_field)
				$ok = true;
		if ($ok)
			$result[] = $t;
	}
	return $result;
}

function duplicate_record($table, $id)
{
	sql_query("START TRANSACTION");
	$table = get_real_tablename($table);
	$columns = sql_table_columns($table);
	$fieldlist = [];
	foreach($columns as $i => $v)
		if ($i != 'id')
			$fieldlist[] = $i;
	$strfieldlist = join (', ', $fieldlist);
	$sql = "INSERT INTO $table ($strfieldlist) SELECT $strfieldlist FROM $table WHERE id = $id";	
	$success = sql_query($sql);
	$msg = '';
	if ($success)
	{
		$msg .= "Дублирование записи $id таблицы $table - успешное выполнение<br/>";
		$newid = sql_last_id();
		$link_field = "{$table}_id";
		$linked_tables = [];
		$lt = sql_rows('SHOW TABLES LIKE "'. $table.'_%"');	
		foreach($lt as $v) 
			foreach($v as $vv) 
				$linked_tables[] = $vv;
		foreach($linked_tables as $t)
		{
			$columns = sql_table_columns($t);
			$fieldlist = [];
			$ok = false;
			foreach($columns as $i => $v)
			{
				if ($i != 'id' && $i != $link_field)
					$fieldlist[] = $i;
				if ($i == $link_field)
					$ok = true;
			}
			$strfieldlist = join (', ', $fieldlist);
			if ($ok)
			{
				$sql = "INSERT INTO $t ($link_field, $strfieldlist) SELECT $newid AS $link_field, $strfieldlist FROM $t WHERE $link_field = $id";	
				$success &= sql_query($sql);
				if (!$success)
				{
					$msg .= "Дублирование записей таблицы $t - ". sql_error();
					break;
				}
				else
					$msg .= "Дублирование записей таблицы $t - успешное выполнение<br/>";
			}
		}
	}
	else 
		$msg .= sql_error();
	//-----------------------------------------------
	if ($success) 
		sql_query("COMMIT");
	else
		sql_query("ROLLBACK");	
	/*
	if ($success)
		return $newid;
	else
		$success = 0;
	*/
	if (substr($msg, -5) == '<br/>')
		$msg = substr($msg, 0, strlen($msg) - 5);
	return ['success' => $success, 'message' => $msg, 'sql' => $sql, 'newid' => $newid];
}

function delete_record($table, $id)
{
	sql_query("START TRANSACTION");
	$table = get_real_tablename($table);
	$link_field = "{$table}_id";
	$linked_tables = [];
	$lt = sql_rows('SHOW TABLES LIKE "'. $table.'_%"');	
	$success = true;
	foreach($lt as $v) foreach($v as $vv) $linked_tables[] = $vv;
	$msg = '';
	$newid = 0;
	foreach($linked_tables as $t)
	{
		$columns = sql_table_columns($t);
		$ok = false;
		foreach($columns as $i => $v)
			if ($i == $link_field)
				$ok = true;
		if ($ok)
		{
			$sql = "DELETE FROM $t WHERE $link_field = $id";
			$success = sql_query($sql);
			if (!$success)
			{
				$msg .= "Удаление записей таблицы $t - ". sql_error();
				break;
			}
			else
				$msg .= "Удаление записей таблицы $t - успешное выполнение<br/>";
		}
	}
	if ($success)
	{
		$sql = "DELETE FROM $table WHERE id = $id";	
		$success &= sql_query($sql);			
		if (!$success)
			$msg .= "Удаление записи $id из таблицы $table - " . sql_error();
		else
			$msg .= "Удаление записи $id из таблицы $table - успешное выполнение<br/>";
	}
	if ($success) 
		sql_query("COMMIT");
	else
		sql_query("ROLLBACK");	
	//return $success;
	if (substr($msg, -5) == '<br/>')
		$msg = substr($msg, 0, strlen($msg) - 5);
	return ['success' => $success, 'message' => $msg, 'sql' => $sql];
}

function create_upd($table, $id, $upd_prefix = 'upd_')
{
	$sql_r = '';
	sql_query("START TRANSACTION");
	$table = get_real_tablename($table);
	if (!$upd_prefix) 
		$upd_prefix = 'upd_';
	$target_table = $upd_prefix . $table;
	$link_field = $table . '_id';
	$columns = sql_table_columns($table);
	$target_columns = sql_table_columns($target_table);
	$fieldlist = [];
	foreach($columns as $i => $v)
		if ($i != 'id')
			if ($i != $link_field)
				if (isset($columns[$i]))
					if (isset($target_columns[$i]))
						$fieldlist[] = $i;
	$strfieldlist = join (', ', $fieldlist);
	$sql = "INSERT INTO $target_table ($strfieldlist, $link_field) SELECT $strfieldlist, $id FROM $table WHERE id = $id";	
	$sql_r .= $sql . '<br>';
	//die($sql);
	$success = sql_query($sql);
	$msg = '';
	$newid = 0;
	if ($success)
	{
		$msg .= "Создание копии записи $id таблицы $table в таблицу $target_table со ссылкой $link_field - успешное выполнение<br/>";
		$newid = sql_last_id();
		$link_field = "{$table}_id";
		$linked_tables = [];
		$lt = sql_rows('SHOW TABLES LIKE "'. $table. '_%"');	
		foreach($lt as $v) 
			foreach($v as $vv) 
				$linked_tables[] = $vv;
		foreach($linked_tables as $t)
		{
			$target_t = $upd_prefix . $t;
			$target_link_field = "{$target_table}_id";
			$src_link_field = "{$t}_id";
			$columns = sql_table_columns($t);
			
			if (!sql_table_exists($target_t))
				continue;
			
			$target_columns = sql_table_columns($target_t);
			//$msg .= "$target_t\n"; 			
			
			$fieldlist = [];
			$ok = false;
			foreach($columns as $i => $v)
			{
				if ($i != 'id' && $i != $link_field)
					if (isset($columns[$i]))
						if (isset($target_columns[$i]))
							$fieldlist[] = $i;
				if ($i == $link_field)
					$ok = true;
			}
			$strfieldlist = join (', ', $fieldlist);
			if ($ok)
			{
				$sql = "INSERT INTO $target_t ($target_link_field, $src_link_field, $strfieldlist) SELECT $newid AS $target_link_field, id as $src_link_field, $strfieldlist FROM $t WHERE $link_field = $id";	
				$sql_r .= $sql . '<br>';
				$success &= sql_query($sql);
				if (!$success)
				{
					$msg .= "Дублирование записей таблицы $t в таблицу $target_t - ". sql_error();
					//$msg .= "Дублирование записей таблицы $t в таблицу $target_t - ". sql;
					break;
				}
				else
					$msg .= "Дублирование записей таблицы $t в таблицу $target_t - успешное выполнение<br/>";
			}
		}
	}
	else 
		$msg .= sql_error();
	//-----------------------------------------------
	if ($success) 
		sql_query("COMMIT");
	else
		sql_query("ROLLBACK");	
	/*
	if ($success)
		return $newid;
	else
		$success = 0;
	*/
	if (substr($msg, -5) == '<br/>')
		$msg = substr($msg, 0, strlen($msg) - 5);
	return ['success' => $success, 'message' => $msg, 'sql' => $sql_r, 'newid' => $newid];
}

function compare_ver($table, $link_field, $id, $ver, $ver_prefix = 'ver_')
{
	$success = 1;
	$table = get_real_tablename($table);
	if (!$ver_prefix) 
		$ver_prefix = 'ver_';
	$msg = "$table, $link_field, $id, $ver, $ver_prefix";
	//$msg = "$table, $link_field, $id, $ver, $ver_prefix";
	$ver_table = $ver_prefix . $table;
	$sql1 = "SELECT * FROM $table WHERE $link_field = $id";
	$sql2 = "SELECT * FROM $ver_table WHERE $link_field = $id AND ver = $ver";
	$columns1 = sql_table_columns($table);
	$columns2 = sql_table_columns($ver_table);
	$rows1 = sql_rows($sql1);
	$rows2 = sql_rows($sql2);
	$compare = [];
	$added = [];
	$removed = [];
	foreach($rows1 as $i => $row1)
	{
		$found = 0;
		foreach($rows2 as $j => $row2)
			if ($row1['id'] == $row2['id'])				
			{
				$found++;
				foreach($row1 as $fld => $val)
					//if (isset($row2[$fld]))
					{
						$val2 = $row2[$fld];
						if ($val2 != $val)
							$compare[] = ['rowid' => $row1['id'], 'fieldname' => $fld, 'oldvalue' => $val2, 'newvalue' => $val];
					}
			}
		if (!$found)
			$added[] = $row1;
	}
	foreach($rows2 as $i => $row2)
	{
		$found = 0;
		foreach($rows1 as $j => $row1)
			if ($row2['id'] == $row1['id'])				
				$found++;
		if (!$found)
			$removed[] = $row2;
	}
	$changed_str = avd_assoc_result($compare);
	$added_str = avd_assoc_result($added);
	$removed_str = avd_assoc_result($removed);
	if (!$changed_str) $changed_str = 'не было изменений;<br/>';
	if (!$added_str) $added_str = 'не было добавлений;<br/>';
	if (!$removed_str) $removed_str = 'не было удалений;<br/>';
	return ['success' => $success, 'message' => $msg, 'sql1' => $sql1, 'sql2' => $sql2, 
		'changed' => $changed_str,
		'added' => $added_str,
		'removed' => $removed_str,
		/*, 'compare' => $compare, 'rows1' => $rows1, 'rows2' => $rows2*/];
}

function compare_smeta_ver($id, $ver)
{
	$r1 = compare_ver('smeta', 'id', $id, $ver);
	$r2 = compare_ver('smeta_income', 'smeta_id', $id, $ver);
	$r3 = compare_ver('smeta_sostav', 'smeta_id', $id, $ver);
	return 
		'Изменения в заголовке:<br/>' .
		$r1['changed'] .
		//$r1['added'] .
		//$r1['removed'] .
		'<br/>Изменения в доходах: '      . $r2['changed'] .
		'Добавленные строки в доходах: '  . $r2['added'] .
		'Удалённые строки в доходах: '    . $r2['removed'] .
		'<br/>Изменения в расходах: '     . $r3['changed'] .
		'Добавленные строки в расходах: ' . $r3['added'] .
		'Удалённые строки в расходах: '   . $r3['removed'];
}

function back_to_ver($table, $id, $ver, $ver_prefix = 'ver_')
{
	$sql_r = '';
	sql_query("START TRANSACTION");
	$success = 1;
	$msg = '';
	//---------------------------------------------------------------------------------
	$table = get_real_tablename($table);
	if (!$ver_prefix) 
		$ver_prefix = 'ver_';
	$target_table = $ver_prefix . $table;
	$link_field = $table . '_id';
	$columns = sql_table_columns($table);
	$target_columns = sql_table_columns($target_table);
	$fieldlist = [];
	foreach($columns as $i => $v)
		if (isset($columns[$i]))
			if (isset($target_columns[$i]))
				$fieldlist[] = "
            $table.$i = $target_table.$i";
	$strfieldlist = join (', ', $fieldlist);
	/*
	UPDATE smeta 
	JOIN ver_smeta ON ver_smeta.id = smeta.id SET 
	  smeta.id = ver_smeta.id,
	  smeta.ver = ver_smeta.ver,
	  smeta.description = ver_smeta.description,
	  smeta.amount_start = ver_smeta.amount_start
	WHERE smeta.id = 456 AND ver_smeta.ver = 6;	
	*/
	$sql = "UPDATE $table 
	JOIN $target_table ON $target_table.id = $table.id SET " . 
		$strfieldlist . "
	WHERE $table.id = $id AND $target_table.ver = $ver"; 
	$sql_r .= $sql . '<br>';
	$success &= sql_query($sql);
	if ($success)
	{
		$msg .= "Копирование записи $id таблицы $table из таблицы $target_table - успешное выполнение<br/>";
		$link_field = "{$table}_id";
		$linked_tables = [];
		$newid = $id; //sql_last_id();
		$lt = sql_rows('SHOW TABLES LIKE "'. $table. '_%"');	
		foreach($lt as $v) 
			foreach($v as $vv) 
				$linked_tables[] = $vv;
		foreach($linked_tables as $t)
		{
			$target_t = $ver_prefix . $t;
			$target_link_field = "{$target_table}_id";
			$columns = sql_table_columns($t);
			
			if (!sql_table_exists($target_t))
				continue;
			
			$target_columns = sql_table_columns($target_t);
			//$msg .= "$target_t\n"; 			
			
			$fieldlist = [];
			$ok = false;
			foreach($columns as $i => $v)
			{
				if (isset($columns[$i]))
					if (isset($target_columns[$i]))
						$fieldlist[] = $i;
				if ($i == $link_field)
					$ok = true;
			}
			$strfieldlist = join (', ', $fieldlist);
			if ($ok)
			{
				$sql = "DELETE FROM $t WHERE $link_field = $id";	
				$sql_r .= $sql . '<br>';
				$success &= sql_query($sql);
				
				if (!$success)
				{
					$msg .= "Удаление записей таблицы $t для $link_field $id - ". sql_error();
					break;
				}
				else
				{
					$msg .= "Удаление записей таблицы $t для $link_field $id - успешное выполнение<br/>";
				
					$sql = "INSERT INTO $t ($strfieldlist) SELECT $strfieldlist FROM $target_t WHERE $link_field = $id AND ver = $ver";	
					$sql_r .= $sql . '<br>';
					$success &= sql_query($sql);

					/////////////////////////////////////////////////////////////////////////
					$sql = "DELETE FROM $target_t WHERE $link_field = $id AND ver >= $ver"; 
					$sql_r .= $sql . '<br>';
					$success &= sql_query($sql);
					/////////////////////////////////////////////////////////////////////////
					
					if (!$success)
					{
						$msg .= "Копирование записей таблицы $target_t в таблицу $t для $link_field $id - ". sql_error();
						break;
					}
					else
						$msg .= "Копирование записей таблицы $target_t в таблицу $t для $link_field $id - успешное выполнение<br/>";
				}
			}			
		}
	}
	else
		$msg = sql_error();
	
	/////////////////////////////////////////////////////////////////////////
	$sql = "DELETE FROM $target_table WHERE id = $id AND ver >= $ver"; 
	$sql_r .= $sql . '<br>';
	$success &= sql_query($sql);
	/////////////////////////////////////////////////////////////////////////
	
	//---------------------------------------------------------------------------------
	if ($success) 
		sql_query("COMMIT");
	else
		sql_query("ROLLBACK");	
	return ['success' => $success, 'message' => $msg, 'ver' => $ver, 'sql' => $sql_r];
}

function create_ver($table, $id, $ver_prefix = 'ver_')
{
	$sql_r = '';
	sql_query("START TRANSACTION");
	$table = get_real_tablename($table);
	if (!$ver_prefix) 
		$ver_prefix = 'ver_';
	$target_table = $ver_prefix . $table;
	$link_field = $table . '_id';
	$columns = sql_table_columns($table);
	$target_columns = sql_table_columns($target_table);
	$fieldlist = [];
	$success = 1;
	$msg = '';
	
	$newid = 0;
	//$ver = sql_get_value('ver', $table, 'id = ' . $id);
	$ver = sql_rows("SELECT MAX(ver) AS ver FROM $target_table WHERE id = $id")[0]['ver'];
	$ver++;
	$sql = "UPDATE $table SET ver = $ver WHERE id = $id";	
	$sql_r .= $sql . '<br>';
	$success &= sql_query($sql);
	if (!$success)
		$msg = sql_error();
	
	foreach($columns as $i => $v)
		if ($i != 'ver')
			if (isset($columns[$i]))
				if (isset($target_columns[$i]))
					$fieldlist[] = $i;
					
	$strfieldlist = join (', ', $fieldlist);
	$sql = "INSERT INTO $target_table (ver, $strfieldlist) SELECT $ver, $strfieldlist FROM $table WHERE id = $id";	
	$sql_r .= $sql . '<br>';
	//die($sql);
	$success &= sql_query($sql);
	
	if ($success)
	{
		$msg .= "Создание копии записи $id таблицы $table в таблицу $target_table со ссылкой $link_field - успешное выполнение<br/>";
		$link_field = "{$table}_id";
		$linked_tables = [];
		$newid = $id; //sql_last_id();
		$lt = sql_rows('SHOW TABLES LIKE "'. $table. '_%"');	
		foreach($lt as $v) 
			foreach($v as $vv) 
				$linked_tables[] = $vv;
		foreach($linked_tables as $t)
		{
			$target_t = $ver_prefix . $t;
			$target_link_field = "{$target_table}_id";
			$columns = sql_table_columns($t);
			
			if (!sql_table_exists($target_t))
				continue;
			
			$target_columns = sql_table_columns($target_t);
			//$msg .= "$target_t\n"; 			
			
			$fieldlist = [];
			$ok = false;
			foreach($columns as $i => $v)
			{
				if (isset($columns[$i]))
					if (isset($target_columns[$i]))
						$fieldlist[] = $i;
				if ($i == $link_field)
					$ok = true;
			}
			$strfieldlist = join (', ', $fieldlist);
			if ($ok)
			{
				$sql = "INSERT INTO $target_t (ver, $strfieldlist) SELECT $ver AS ver, $strfieldlist FROM $t WHERE $link_field = $id";	
				$sql_r .= $sql . '<br>';
				$success &= sql_query($sql);
				if (!$success)
				{
					$msg .= "Копирование записей таблицы $t в таблицу $target_t - ". sql_error();
					break;
				}
				else
					$msg .= "Копирование записей таблицы $t в таблицу $target_t - успешное выполнение<br/>";
			}
		}
		$ver++;
		$sql = "UPDATE $table SET ver = $ver WHERE id = $id";	
		$success &= sql_query($sql);
	}
	else 
		$msg = sql_error();
	//-----------------------------------------------
	if ($success) 
		sql_query("COMMIT");
	else
		sql_query("ROLLBACK");	
	/*
	if ($success)
		return $newid;
	else
		$success = 0;
	*/
	if (substr($msg, -5) == '<br/>')
		$msg = substr($msg, 0, strlen($msg) - 5);
	return ['success' => $success, 'message' => $msg, 'sql' => $sql_r, 'newid' => $newid, 'ver' => $ver];
}

function get_log_info($table, $operation_type, $id)
{
	$msg = '';
	$success = true;
	$rr = null;
	$sql = null;
	
	$log_table = 'log_' . $table;;
	
	if (!sql_table_exists($log_table))
		$log_table = $table;

	if (!sql_table_exists($log_table))
	{
		$success = false;
		$msg = "Таблица $log_table не существует";
	}
	else
	{
		if (!$id) $id = 0;
		if (!$operation_type) $operation_type = 0;		
		$sql = "SELECT user_id, operation_type, ts FROM $log_table WHERE id = $id AND operation_type = $operation_type ORDER BY ts DESC LIMIT 1";
		$rr = sql_rows($sql);
	}
	
	return ['success' => $success, 'message' => $msg, 'result' => $rr, 'sql' => $sql];
}

function get_html_doc_info($table, $id)
{
	$msg = '';
	$success = true;
	$rr = null;
	$sql = null;
	$html = '<table border="1" cellpadding="3" cellspacing="1" frame="border" rules="all">';
	
	$log_table = 'log_' . $table;;
	
	if (!sql_table_exists($log_table))
		$log_table = $table;

	if (!sql_table_exists($log_table))
	{
		$success = false;
		$msg = "Таблица $log_table не существует";
	}
	else
	{
		if (!$id) 
			$id = 0;
		$n = 0;
		for ($operation_type = 1; $operation_type <= 2;	$operation_type++)	
		{
			$sql = "
			SELECT lt.user_id, u.fio, lt.operation_type, ot.name operation_type_name, lt.ts 
			FROM $log_table lt
			LEFT JOIN user u ON u.id = lt.user_id
			LEFT JOIN _operation_type ot ON ot.id = lt.operation_type
			WHERE lt.id = $id AND operation_type = $operation_type 
			ORDER BY ts DESC LIMIT 1
			";
			try
			{
				$rr = sql_rows($sql);
				if (count($rr))
				{
					$r = $rr[0];
					$html .= "<tr><td>${r['operation_type_name']}</td><td><b>${r['fio']}</b></td><td>${r['ts']}</td></tr>";
					$n++;
				}
			}
			catch (Exception $e)
			{
				$msg = $e->getMessage();
				$success = false;
			}
		}
		$html .= "</table>";
		if ($n)
			$html = 'Сведения о документе:' . $html;
	}	
	return ['success' => $success, 'message' => $msg, 'html' => $html];
}

$success = true;
$message = '';
$r = null;

if (isset($_REQUEST['fn']))
try
{
	$fn = $_REQUEST['fn'];
	if (isset($_REQUEST['arg1'])) $arg1 = $_REQUEST['arg1']; else $arg1 = null;
	if (isset($_REQUEST['arg2'])) $arg2 = $_REQUEST['arg2']; else $arg2 = null;
	if (isset($_REQUEST['arg3'])) $arg3 = $_REQUEST['arg3']; else $arg3 = null;
	if (isset($_REQUEST['arg4'])) $arg4 = $_REQUEST['arg4']; else $arg4 = null;
	if (isset($_REQUEST['arg5'])) $arg5 = $_REQUEST['arg5']; else $arg5 = null;
	
	$arg1 = sql_escape($arg1);
	$arg2 = sql_escape($arg2);
	$arg3 = sql_escape($arg3);
	$arg4 = sql_escape($arg4);
	$arg5 = sql_escape($arg5);

	if (function_exists($fn))
		$r = $fn($arg1, $arg2, $arg3, $arg4, $arg5);
	else
	{
		require_once $_SERVER['DOCUMENT_ROOT'] . '/thesis/thesis_functions.php';
		if (function_exists($fn))
			$r = $fn($arg1, $arg2, $arg3, $arg3, $arg5);
		else
		{
			$r = null;
			throw new Exception("Функция $fn недоступна.");		
		}
	}
}
catch (Exception $e)
{
	$message = $e->getMessage();
	$success = false;
}
else
{
	$message = 'Необходимо задать fn, arg1, arg2...';
	$success = false;
}

$rr = ['success' => $success, 'message' => $message, $root => $r];

if (isset($_REQUEST['test']))
{
	echo '<HTML><BODY>'; 
	if ($success)
		avd_print_assoc_array($r); 
	else
		echo($message);
	echo '</BODY></HTML>'; 
}	
else 
	if (isset($_REQUEST['dump']))
	{
		echo '<HTML><BODY><PRE>'; 
		var_dump($rr); 
		echo '</PRE></BODY></HTML>'; 
	}	
	else 
		echo json_encode($rr);
?>