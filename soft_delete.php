<?php

require_once($_SERVER['DOCUMENT_ROOT'] . '/modules/save.php');

function del_row($table,$id,&$msg) {
	if (!user_may_edit_table($table)) {
		$msg = 'У вас нет прав редактировать таблицу!';
		return false;
	}
	if (in_array($table,['smeta_files','zayav_files','dogovor_files'])) {
		$author_id = sql_get_value('author_id',$table,"id='$id'");
		if (!is_admin() && $author_id != $_SESSION['user_id']) {
			$msg = 'У вас нет прав удалять файл загруженный другим пользователем!';
			return false;
			
		}
	}

	$ids = is_array($id) ? $id : array($id);	
	// В транзакцию возьмет  getdata\delrecord или gettable
	$_cascade_delete = ['pfhd_ver'=>['pfhd'],'dogovor_sostav'=>['dogovor_sostav_smeta_sostav']];
	if (isset($_cascade_delete[$table])) {
		foreach ($_cascade_delete[$table] as $t) {
			$link_fld = $table.'_id';
			foreach($ids as $_id) {
				sql_query("delete from `$t` WHERE $link_fld='$_id'");
			}
		}
	}
	
	

	$tb_list = array();
	/* получаем список всех таблиц в базе в массив */

	$ref_field = $table . Config::REF_SUF; // имя поля reference
	
	$sql_tables = "select t.table_name
	from information_schema.tables t
	inner join information_schema.COLUMNS c
		  on c.table_schema=t.table_schema
		  and c.table_name=t.table_name
	where t.TABLE_SCHEMA='reestr2015' AND c.COLUMN_NAME='$ref_field' AND t.TABLE_TYPE = 'BASE TABLE'";
	
	$res = sql_query($sql_tables);
	while($row = sql_numeric_array($res)) {
		$_table = $row[0];
		$ignore_prefixes = ['log_','ver_'];
		$ignore = false;
		foreach ($ignore_prefixes as $ignore_prefix) {
			if (substr($_table,0,strlen($ignore_prefix)) == $ignore_prefix) {
				$ignore = true;
				break;
			}
		}
		if ($table == 'smeta' && in_array($_table,['pfhd','zfo_smeta_limit'])) {
			// Разрешаем удалять сметы входящие в ПФХД. 
			// Можно смотреть по версиям
			$ignore = true;
		}
		if (!$ignore) {
			$tb_list[] = $_table;
		}
	}
	/* название поля, соответствующее id удаляемой записи в других таблицах */
	$has_error = false;
	$ref_tables = array();
	foreach ($tb_list as $tb_name) {
		/* если название колонки соответсmвует, ищем сколько записей с таким id */	
		for($i = 0; $i < count($ids); $i++) {
			$cnt = sql_get_value('count(*)',$tb_name,"$ref_field='$ids[$i]'");
			if ($cnt>0) $ref_tables[] = $tb_name;
		}
	}

	
	foreach (Config::referenced_by($table) as $v) {
		for($i = 0; $i < count($ids); $i++) {
			$cnt = sql_get_value('count(*)',$v['table'],$v['field'] . "='" . $ids[$i] ."'");
			if (sql_count_rows($res)>0) $ref_tables[] = $v['table'];
		}
	} 

	//$ref = $_SERVER['HTTP_REFERER'];
	if (count($ref_tables)) {
		$msg = "Данную запись удалять нельзя, т.к. она используется в других таблицах: ";
		$msg .= implode("\n<br>" , $ref_tables);
		return false;
	} else {
		//NB!!!AVD
		$result = true;
		//
		if (!(strpos('s'.$table,'temp'))) {
			$msg = '';
			for($i = 0; $i < count($ids); $i++) {
				SaveData::save_log($_SESSION['user_id'],$table,3,$ids[$i]);
				$key_name = sql_table_key($table);
				$sql = "DELETE FROM `$table` WHERE `$key_name`='".$ids[$i]."'";
				//throw new Exception($sql);
				//NB!!!AVD
				//sql_query($sql)				
				$r =sql_query($sql);
				$result &= $r;
				if (!$r)
				{
					if ($msg) $msg .=  "\\n";
					$msg .= sql_error();
				}
				//
			}
		}
		return $result;
	}
}

?>