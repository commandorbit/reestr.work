<?php 
require SHARED_PHP_PATH."/controls/controls.php";
$controls = new controls;
?>
<div id="stat_738_filter">
<form method="get">
	<label>
		Версия 738:
		<?php 
		$vers = sql_to_assoc("SELECT id, name FROM stat_738_ver ORDER BY tstamp desc ");
		$default_ver_id = sql_get_value("MAX(id)","stat_738_ver");
		$ver_id = request_numeric_val('f_stat_738_ver_id',$default_ver_id);
		$controls->print_select('f_stat_738_ver_id',$vers,$ver_id); 
		?>
	</label>
	<input type="hidden" name="t" value="<?=$table?>">
	<input type="submit" name="change" value="Ок">
	<?php if (is_admin()) : ?>
		<input type="button" name="create_version" value="Создать новую версию">
	<?php endif ?>
</form>
</div>


<div id="create_version_div" style="display: none;">
	<?php 
	$controls->print_input_label('name','','Название');
	$controls->print_input_label('prim','','Примечание');
	$controls->print_button('save_version','Сохранить');
	?>
</div>


<script type="text/javascript">
	$(document).ready(function(){
		$('input[name="create_version"]').click(function(){
			$('#create_version_div').dialog({
				title: 'СОЗДАТЬ ВЕРСИЮ 738',
				width: 450,
				height: 180,
				maxWidth: 450,
				maxHeight: 180,
			});
		});
		
		$('input[name="save_version"]').click(function(){
			var name = $('#create_version_div input[name="name"]').val();
			var prim = $('#create_version_div input[name="prim"]').val();
			if (!name) {
				alert('Не введено название новой версии!');
				return false;
			}
			location.href='/form_738/form_738_save.php?name='+name+'&prim='+prim;
		});
	});
</script>

<style type="text/css">
#stat_738_filter {
	text-align: center;
	margin-top: 20px;
}

#stat_738_filter select[name="f_stat_738_ver_id"] {
	width: 150px;
}

#stat_738_filter select[name="f_stat_738_ver_id"] {
	width: 150px;
}

#stat_738_filter input[name="create_version"] {
	margin-left: 50px;
}

#create_version_div {
	text-align: right;
}

#create_version_div input, #create_version_div select {
	width: 250px;
	font-size: 18px;
	box-sizing: border-box;
	margin: 5px;
}

#create_version_div input[type="button"] {
	margin-right: 10px;
}
</style>