<?php 
	$vers = sql_rows("SELECT id, name FROM stat_738_ver");
?>

<div class="content">
	<form action="/tedit.php" method="get" style="text-align: center">
		<input type="hidden" name="t" value="<?=$_GET['t'] ?>" />
		<p>Выберите версии для сравнения:</p>
		<label>
		Версия 1:
		<select name="ver_1">
			<option value="-1">Не выбрано</option>
			<?php foreach($vers as $ver): ?>
				<?php 
					$selected = (isset($_GET['ver_1']) && $ver['id'] === $_GET['ver_1']) ? 'selected' : '';
				?>
				<option <?= $selected ?> value="<?= $ver['id'] ?>"><?= $ver['name'] ?></option>
			<?php endforeach; ?>
		</select>
		</label>
		<label>
		Версия 2:
		<select name="ver_2">
			<option value="-1">Не выбрано</option>
			<?php foreach($vers as $ver): ?>
				<?php 
					$selected = (isset($_GET['ver_2']) && $ver['id'] === $_GET['ver_2']) ? 'selected' : '';
				?>
				<option <?= $selected ?> value="<?= $ver['id'] ?>"><?= $ver['name'] ?></option>
			<?php endforeach; ?>
		</select>
		</label>
		<input type="submit" value="Сравнить" />
	</form>
</div>