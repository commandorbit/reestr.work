<?php
function zayav_file_upload($id) {
    $path='/DOCS/ZAYAV/';
    $filename=file_upload($path);
    $path=cr_path_year_month($path,date('Y.m.d'));
    $author_id = $_SESSION['user_id'];
    foreach ($filename as $key => $value) {
        $sql = "INSERT INTO zayav_files (zayav_id, author_id, filename, path) VALUES($id, $author_id, '$value', '$path')";
        $res=sql_query($sql);   
    }
}


function zayav_out($T,$data) {
    echo '<div class="form-controls dogovor_fields clearfix">';
    //print_r($T->columns);  

    foreach ($T->columns as $field=>$col) {
        //ПРОВЕРИТЬ НА ДОГОВОР НОМ - ОТКУДА БЕРЕТСЯ
        if($field == 'user' || $field == 'dogovor_sostav_id' || $field == 'dogovor_id' && !is_admin())
            continue;    

        if ($field=='id') {
            echo '<tr style="display: none;"><td><input type="hidden" name="id" value="' .notset($data,$field) .'"></td></tr>';
        } else {
            echo '<div class="form-group" id="'.$field.'">';
            echo '<label>' . $col['label'] .'</label><br />';
            $disabled = !$T->may_edit;
            if($field == 'dogovor_id') $disabled = true;                
            echo $T->control($col,notset($data,$field), is_null($data), $disabled);
            echo '</div>'."\n";
        }
    }
    echo '</div>';
}

function TD_zayav_sostav($may_edit,$id) {
    $T = newTD('v_zayav_sostav',array('f_zayav_id'=>$id));

    if (user_has_zfo()) {
		$zfo = user_zfo_id_str();
        $T->columns['fp_article_id']['edit_options']=sql_to_assoc("select id,name from v_fp_article where id in (select fp_article_id from fp_article_zfo where zfo_id in ($zfo)) order by shifr");
    }

    $T->columns['dogovor_sostav_id']['hidden'] = true;  
    $T->columns['plat_sum']['hidden'] = true;  

    // $TS->fixed_headers=false;
    $T->has_filter=false;
    $T->use_buttons=false;
    $T->fixed_headers=true;
    $T->may_edit = $may_edit;

    unset($T->columns['zayav_id']);

    $T->foreign_key=array('zayav_id'=>$id);

    //if ($may_edit) {
    //    $T->html_after = '<div style="text-align:center;"><a style="display:block; margin-top: 15px;" class="td-add-button" href="#">Новая запись</a></div>';
    //}

    return $T;
}

function TD_plat($id) {
    $T = newTD('plat',array('f_zayav_id'=>$id));
    $T->has_filter=false;
    $T->may_edit=false;
    $T->use_buttons=false;
    $T->fixed_headers=true;    
    $columns=$T->columns;
    $platcolumns=array();
    $platcolumns['bank_acc_id']=$columns['bank_acc_id'];
    $platcolumns['nom']=$columns['nom'];
    $platcolumns['d']=$columns['d'];
    $platcolumns['kosgu']=$columns['kosgu'];
	$platcolumns['kvr']=$columns['kvr'];
    //$platcolumns['fin_source_id']=$columns['fin_source_id'];
   	$platcolumns['booker_fin_source']=$columns['booker_fin_source'];
    $platcolumns['s']=$columns['s'];
    $T->columns=$platcolumns;
    return $T;
}

function TD_history($id) {
    $t=newTD('v_zayav_status',array('f_id'=>$id));
    unset($t->columns['id']);
//  $t->columns['user_id']['options']=$DB::arrayFromTable('user');
    $t->columns['ts']['width']=200;
    $t->may_edit=false;
    $t->has_filter=$t->use_buttons=$t->fixed_headers=false;
    return $t;
}


function notset($array,$key,$default=null) {
    return isset($array[$key]) ? $array[$key] : $default;
}

?>