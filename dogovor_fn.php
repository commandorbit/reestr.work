<?php

function dogovor_file_upload($id) {
    $path='/DOCS/DOG/';
    $filename=file_upload($path);
	$path=cr_path_year_month($path,date('Y.m.d'));
    $author_id = $_SESSION['user_id'];
    foreach ($filename as $key => $value) {
		save('dogovor_files',['dogovor_id'=>$id,'author_id'=>$author_id,'filename'=>$value,'path'=>$path]);
//        $sql = "INSERT INTO dogovor_files (dogovor_id, author_id, filename, path) VALUES($id, $author_id, '$value', '$path')";
//        $res=sql_query($sql);   
    }
    header('Location: ' . $_SERVER['HTTP_REFERER']);
}

function zfo_may_fp_object($zfo_id) {
    return sql_fetch_value ("select count(a.id) from fp_article a inner join fp_article_zfo az on a.id=az.fp_article_id WHERE az.zfo_id='$zfo_id' and a.fp_object_type_id is not null  ");
}

function TD_zayav_dog($id) {
	$t = newTD('virt_zayav_dog',array('f_dogovor_id'=>$id),true);
	unset($t->columns['dogovor_id']);
	$t->may_edit=false;
	$t->use_buttons=false;
    return $t;
}

function TD_dogovor_sostav($may_edit,$id) {
	if(may_create_zayav() && may_zayav_dog_status($id)) {
		echo '<div style="width: 150px;">';		
		echo '<form method="GET" action="/zayav_new.php" class="custom-buttons" id="create-zayav">';
		echo '<div id="hiddens"></div>';
		echo '<p><input type="submit" name="create_zayav" value="Создать заявку" disabled="disabled" title="Не выбрано ни одной заявки"></p>';
		echo '</form>';
		echo '</div>';
	}

	$T = newTD('v_dogovor_sostav',array('f_dogovor_id'=>$id));
	// разрешаем checkbox для выбора
	if(may_create_zayav() && may_zayav_dog_status($id)) {
		$T->show_checkbox = true;
		//if(may_create_zayav() && may_zayav_dog_status($id)) 
	}
	
    if (user_has_zfo()) {
		$zfo = user_zfo_id_str();
        $T->columns['fp_article_id']['edit_options']=sql_to_assoc("select id,name from v_fp_article where id in (select fp_article_id from fp_article_zfo where zfo_id in ($zfo)) order by shifr");
    }

    $dog_zfo_id = sql_get_value('zfo_id','dogovor',"id='$id'");
    $dog_has_fp_object = sql_fetch_value ("select count(dogovor_id) from dogovor_sostav where dogovor_id='$id' and fp_object_id is not null");

    if (!zfo_may_fp_object($dog_zfo_id) && !$dog_has_fp_object) {
        $T->columns['fp_object_id']['hidden'] = true;    
    } else {
        $T->columns['fp_object_id']['hidden'] = false;
    }

   // $TS->fixed_headers=false;
	$T->has_filter=false;
    $T->use_buttons=false;
    $T->fixed_headers=true;
    $T->may_edit = $may_edit;

	unset($T->columns['dogovor_id']);

    $T->foreign_key=array('dogovor_id'=>$id);

    if ($may_edit) {
        $T->html_after = '<div style="text-align:center;"><a style="display:block; margin-top: 15px;" class="td-add-button" href="#">Новая запись</a></div>';
    }

    return $T;
}

function dogovor_status_out($data) {
    if ($data) {
        $status_history_tmp = sql_rows("   SELECT ds.name as status_name, ld.ts, u.fio, ld.dogovor_status_id 
                                        FROM log_dogovor ld 
                                        LEFT JOIN dogovor_status ds ON ds.id = ld.dogovor_status_id 
                                        LEFT JOIN user u ON u.id = ld.user_id
                                        WHERE ld.id = '${data['id']}'
                                        ORDER BY ts ASC");
        $prev_status = -1;
        $status_history = array();
        foreach ($status_history_tmp as $row) {
            if ($row['dogovor_status_id']!= $prev_status) {
                $status_history[] = $row;
                $prev_status=$row['dogovor_status_id'];
            }
        }

        echo '<form method="POST" action="/save_dogovor_status.php">';
            echo '<div class="form-controls dog-status clearfix">';
            echo '<div class="status-group" style="font-size: 17px;">';
                $options = false;
                $status = $data['dogovor_status_id'];
                if (!is_ro()) {
                    if (user_may_any_dogovor_status()) {
                        $options = arrayFromSql2("select id, name from dogovor_status ");
                    } else if (in_array($data['zfo_id'], user_zfo_ids()) && in_array($status,array(1,2))) {
                        // Договор имеет статус редактирование или Включить в ФП и договор принадлжеит данном упользователю
                        $options = arrayFromSql2("select id, name from dogovor_status where id in (1,2)");
                    } else if(is_ku()) {
                        $options = false;
                    }
                }

                echo "<input type='hidden' value='".$data['id']."' name='dogovor_id' />";

                if($options) {
                    $controls = new controls;
                    $controls->print_select_label('dogovor_status', $options, $status, 'Статус договора', array('style'=>'float:left; width: 100%'));
                    echo '<input type="submit" name="save_status" style="float: right; margin-top: 12px;" value="Применить">';
                } else {
                    echo '<b>Статус договора:</b> ' . sql_get_value('name', 'dogovor_status', "id='$status'").'<br />';

                    $is_ku_checked = is_ku_checked($data['id']);

                    if(dogovor_has_file($data['id']) && !$is_ku_checked && $data['dogovor_status_id'] == 3 && is_ku()) {
                        $checked = ($is_ku_checked) ? 'checked="checked"' : '';
                        echo '<br /><input type="checkbox" name="ku" '. $checked .' /> Заключен КУ';
                        echo '<input type="submit" name="save_ku" style="float: right; margin-top: 12px;" value="Применить">';
                    }
                }
            echo '<a href="#" class="status_journal" data-content="status-history">Журнал изменения статусов</a>';
            echo '</div>';
            echo '<div id="status-history" style="margin-left: 8px; display: none;">';
                    echo '<table class="td-table" align="left">';
                        echo '<tr>';
                            echo '<th>Статус</th>';
                            echo '<th>Дата изменения</th>';
                            echo '<th>Изменил</th>';
                        echo '</tr>';
                        for($i = count($status_history)-1; $i>=0; $i--) {
                            echo '<tr>';
                                foreach($status_history[$i] as $key=>$fld) {
                                   if($key !== 'dogovor_status_id') echo '<td>' . $fld . '</td>';
                                }
                            echo '</tr>';
                        }
                    echo '</table>';
            echo '</div>';
            echo '</div>';
        echo '</form>';
    }
}

function TD_plat_out($id) {
    $T = newTD('plat',array('f_zayav_id'=>$id));
    $T->data = vDogovorPlatData($id);
    $T->has_filter=false;
    $T->may_edit=false;
    $T->use_buttons=false;
    $T->fixed_headers=true;    
    return $T;
}

function dogovor_out($T,$data) {
    echo '<div class="form-controls dogovor_fields clearfix">';
    //print_r($T->columns);  

    foreach ($T->columns as $field=>$col) {
        //ПРОВЕРИТЬ НА ДОГОВОР НОМ - ОТКУДА БЕРЕТСЯ
        if($field == 'dogovor_nom' || $field == 'dogovor_status_id')
            continue;   


        if ($field=='id') {
            echo '<tr style="display: none;"><td><input type="hidden" name="id" value="' .notset($data,$field) .'"></td></tr>';
        } else {
            echo '<div class="form-group" id="'.$field.'">';
            echo '<label><span>'. $col['label'] .'</span><br />';

            $disabled = !$T->may_edit;             
            echo $T->control($col,notset($data,$field), is_null($data), $disabled);
            echo '</label>';
            echo '</div>'."\n";
        }
    }
    echo '</div>';
}
function notset($array,$key,$default=null) {
    return isset($array[$key]) ? $array[$key] : $default;
}
//-------------------------конец загрузки файла-------------------------
