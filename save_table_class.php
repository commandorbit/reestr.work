<?php

class save_table{
	public static function save($table, $use_sql_date2rus = true){
		$status = array(
			'saved'=>false,
			'sql'=>'',
			'errors'=>array(),
			'message'=>''
		);
		$data = self::request2array();
		$cols = sql_rows("SHOW COLUMNS FROM $table");

		foreach ($data as $nRow=>$v) {
			if ($errors = self::dataErrors($cols,$v)) $status['errors'][]=$errors;
		}
		if ($status['errors']) {
			$status['message'] = 'Ошибки в данных!';
		} else {
			foreach ($data as $nRow=>$v) {
				$s=array();
				foreach ($cols as $col) {
					$field = $col['Field'];
					if (isset($v[$field])) {
						$value = $v[$field];
						if (self::col_has_null($col) && self::val_is_empty($v,$field)) {
							$s[]="$field=null ";
						} elseif (self::col_is_date($col) && $use_sql_date2rus == true) {
							$s[]="$field='".sql_date2rus($value)."' ";
						} else {
							$s[]="$field='$value' ";
						}
					}
				}
				if (self::primary_key_exists($table,$v)) {
					$sql = "UPDATE $table SET ".implode(",",$s)." WHERE true ".self::where($table,$v);
				} else {
					$sql = "INSERT INTO $table SET ".implode(",",$s);
				}
				$status['sql']=$sql;
				if (sql_query($sql)) {
					$status['message'] = 'Данные успешно сохранены!';
					$status['saved'] = true;
				}
			}
			//USER_ZFO
            if ($table=='user' && isset($data[0]['id']) ) {
                $user_id = $data[0]['id'];
                sql_query("DELETE FROM user_zfo WHERE user_id = '$user_id'");
                if(isset($data[0]['id']) && isset($data[0]['user_zfo'])) {
                    $user_zfo = $data[0]['user_zfo'];
                    foreach($user_zfo as $zfo_id) {
                        sql_query("INSERT INTO user_zfo (user_id, zfo_id) VALUES ('$user_id', '$zfo_id')");
                    }
                }
            }
		}
		return $status;
	}

	private static function primary_key_exists($table,$row) {
		if ($where = self::where($table,$row)) return sql_get_value("count(*)",$table,"true ".$where);
		else return false;
	}

	private static function where($table,$row) {
		$pk = sql_table_key($table);

		$where = '';
		if (is_array($pk)) {
			foreach ($pk as $key) {
				if (isset($row[$key])) $where.= " AND $pk='${row[$key]}' ";
			}
		} else {
			if (isset($row[$pk])) $where.= " AND $pk='${row[$pk]}' ";
		}
		return $where;
	}

	private static function dataErrors($cols,$row){
		$errors = array();
		foreach ($cols as $col) {
			$field = $col['Field'];
			if($field == 'zfo_id') continue;
			if ($col['Type'] !=='timestamp') {
				if (!self::col_has_null($col) && self::val_is_empty($row,$field) && !self::col_has_autoincrement($col)) {
					if (!self::col_has_default($col)) $errors[$field] = 'Не заполнено обязательное поле '.$field.'!';
				} elseif (!self::val_is_empty($row,$field)) {
					if (self::col_is_numeric($col) && !is_numeric($row[$col['Field']])) {
						$errors[$field] = 'Поле '.$field.' может содержать только числовые значения!';
					} elseif (self::col_is_date($col) && !checkdate(substr($row[$field],3,2), substr($row[$field],0,2), substr($row[$field],6,4))) {
						$errors[$field] = 'Недопустимое значение даты в поле '.$field.'!';
					}
				}
			}
		}
		return $errors;
	}

	private static function request2array() {
		$data = array();
		$nRow = 0;
		foreach ($_POST as $key=>$v) {
			if (is_array($v)) {
				if(!isset($data[$nRow][$key])) $data[$nRow][$key] = array();
				foreach ($v as $value) {
					array_push($data[$nRow][$key],sql_escape(trim($value)));
				}
			} else {
				$data[$nRow][$key]=sql_escape(trim($v));
			}
		}
		return $data;
	}

	private static function col_has_default($col) {
		return ($col['Default']=='' || (self::col_is_character($col) && $col['Default']==(string)'')) ? 0 : 1;
	}

	private static function col_has_null($col) {
		return ($col['Null']=='NO') ? 0 : 1;
	}

	private static function val_is_empty($row,$col_name) {
		$is = false;
		if (!isset($row[$col_name])) {
			$is = true;
		} else {
			if ($row[$col_name]=='') $is = true;
			if ($row[$col_name]=='null') $is = true;
			if ($row[$col_name]=='00.00.0000') $is = true;
		}
		return $is;
	}

	private static function col_is_numeric($col) {
		return (strstr($col['Type'],"int")) ? 1 : 0;
	}

	private static function col_has_autoincrement($col) {
		return ($col['Extra']=='auto_increment') ? 1 : 0;
	}

	private static function col_is_character($col) {
		return (strstr($col['Type'],"char")) ? 1 : 0;
	}

	private static function col_is_date($col) {
		return (strstr($col['Type'],"date")) ? 1 : 0;
	}
}