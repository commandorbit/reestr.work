<?php
error_reporting(E_ALL | E_STRICT);
require_once $_SERVER['DOCUMENT_ROOT'] . '/init.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/modules/asu_pfhd_sender.php';

//AVD
$srcxml = '';
if (isset($_POST['srcxml'])) $srcxml = $_POST['srcxml'];

$max_size = 2*1014;
if (isset($_REQUEST['is_partly'])) {
	$max_size = request_val('size_part',$max_size);
}
define('MAX_SIZE',$max_size * 1024);

/*if(isset($_POST['size_part'])) {
	var_dump(MAX_SIZE);
	die();
}*/

$response_key = isset($_POST['key']) ? $_POST['key'] : null;

$use_calc_table = isset($_POST['use_calc_table']) ? true : false;

$loading_types = array(
	array('value' => 'finActivityPlan2017', 'name' => 'ПФХД 2017'), 
	array('value' => 'finActivityPlan', 'name' => 'ПФХД 2016'), 
	array('value' => 'extraValue', 'name' => 'Доп. показатели')
);
$value_types = array(
	'extraValue'=>'extra.xml'
);

//AVD
function createSrcXML($use_calc_table) {
	$method = $_POST['method'];
	$y = (int) $_POST['y'];
	$ver_id = sql_escape($_POST['ver_id']);
	if ($method == 'extraValue') $ver_id = 0;
	$xml = null;
	if ($method == 'extraValue') {
		$xml = Pfhd::extraToXML($ver_id);
	} else if ($method == 'finActivityPlan' && $y == 2016) {
		$xml = Pfhd::toXml2016($ver_id);
	} else if ($method == 'finActivityPlan2017') {
		// Для 2018 и 2019 года название Метода остался старый = "finActivityPlan2017"
		Pfhd::$useCalcTables = $use_calc_table;
		$xml = Pfhd::toXml($ver_id);
	}
	return $xml;
}


$loading_answer = $loading_result = 'Ответ сервера АСУ ПФХД';

if (isset($_POST['send'])) {
	$xml = createSrcXML($use_calc_table);
	$method = $_POST['method'];
	$y = $_POST['y'];
	$answerArray = send_xml($xml,$method,$method.$y.'.xml');
	$loading_answer = implode(' ', $answerArray);	
	$response_key = getKey(end($answerArray));
	
} else if (isset($_POST['create'])) {	
	$srcxml = createSrcXML($use_calc_table);
	
} else if (isset($_POST['sendcreated']) && $srcxml) {
	$method = $_POST['method'];
	
	$answerArray = send_xml($srcxml,$method,$method.'.xml');
	$loading_answer = implode(' ', $answerArray);	
	$response_key = getKey(end($answerArray));	
//
} else if (isset($_POST['get_xml'])) {
	$xml = createSrcXML($use_calc_table);
	header('Content-Type: text/xml');
	header('Content-Disposition: attachment;filename="ПФХД2017.xml"');
	header('Cache-Control: max-age=0');
	echo $xml;	
	exit();
} else if(isset($_POST['get_xls']) && isset($_POST['ver_id'])) {
	$ver_id = sql_escape($_POST['ver_id']);	
	if($_POST['method'] == 'extraValue')  {
		$ver_id = 0;
		//$html = Pfhd::toHTML($ver_id, Pfhd::getExtraRows());
	} else if($_POST['method'] == 'finActivityPlan') {
		//Pfhd::fillTmpPfhdTable($ver_id);
		//$html = Pfhd::toHTML($ver_id, Pfhd::getPfhdRows(2016));
	//AVD
	} else if($_POST['method'] == 'finActivityPlan2017') {
		//Pfhd::fillTmpPfhdTable($ver_id);
		//$html = Pfhd::toHTML($ver_id, Pfhd::getPfhdRows(2017));
	}
	
	header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
	header('Content-Disposition: attachment;filename="'.$_POST['method'] . '_' . date('dmY').'.xls"');
	header('Cache-Control: max-age=0');
	echo $html;
	exit();
}

$key = request_val('key','');
if (isset($_POST['check_key'])) {
	$loading_result = check_key($key);
}

function getKey($xml_str) {
	libxml_use_internal_errors(true);
	$xml = simplexml_load_string($xml_str);
	if ($xml === false) {
		header('Content-Type: application/xml');
		header('Content-Disposition: attachment;filename="'.$_POST['method'] . '_' . date('dmY').'.xml"');
		header('Cache-Control: max-age=0');
		echo $xml_str;
		exit();
	}
	$key = ($xml->Key[0]) ? $xml->Key[0] : null;
	return $key;	
}

$title = "Загрузка в АСУ ПФХД";
require_once $_SERVER['DOCUMENT_ROOT'] . '/template/header.php';
?>

<style>
	.info textarea {
		width: 100%;
		height: 105px;
	}
	
	.info input[type="submit"] {
		margin: 10px 0;
	}
	
	.form-group {
		margin: 0 0 10px 0;
		padding: 0 5px 0 0;
		visibility: visible;
	}
</style>
<link rel="stylesheet" type="text/css" href="/css/background<?=$_SESSION["palette"]?>.css?ver=<?=VER?>">

<!-- NB!!! -->
<div id="ext-body" class="ext-body">
<?php
require $_SERVER['DOCUMENT_ROOT'] . '/include_extjs.php';
?>
<script id="microloader" type="text/javascript" src="/extapp/bootstrap.js"></script>

<script>
	//Ждём загрузки EXTJS
	window.addEventListener("load", function(event)
	{
		var waitExtJSTimer = setInterval(function()
		{
			if (window.extapp && window.extapp.getApplication)
			{
				window.extapp.userId = <?=$user_id?>;
				//alert('USER = ' + window.extapp.userId);
				clearInterval(waitExtJSTimer);
				console.log('Библиотека загружена!');
				//
			}
		},
		100);	
	});
	
	function colseMainDialog()
	{
		//alert('Закрываем!');
		var el = document.getElementById('main_dialog');
		if (el) el.parentNode.removeChild(el);
	}
</script>
<style>
/* AVD 23.08.2017 ------------------------------------------------------------------------------------------------------------- */
.box_colse
{
    background-position: 0px 0px;
    background-image: url(img/png/tool-sprites.png);	
    width: 16px;
    height: 16px;
	display: block;	
	cursor: pointer;
}

select[name="ver_id"] {width: 150px;}
</style>

<!-- /NB!!! -->

<div style="text-align: center;">
<div class="php-dialog content-t" data-moveable="1" style="width:750px" id="main_dialog">
<div align="right">
<div id="box_colse" onclick="colseMainDialog();" data-tooltip="Закрыть" class="box_colse"></div>
</div>
<h1><?=$title?></h1>
<br>
<button onclick="extapp.getApplication().openTable('asu_pfhd_division');">Подразделения</button>
<button onclick="extapp.getApplication().openPFHDParam();">Реквизиты</button>
<button onclick="extapp.getApplication().openPFHDDopPokaz();">Доп. показатели</button>
<button onclick="extapp.getApplication().openPFHDActivityKind();">Виды ФО</button>
<button onclick="extapp.getApplication().openPFHDActivityGoal();">Цели деятельности</button>
<button onclick="extapp.getApplication().openPFHDActivityKindPFHD();">Виды деятельности ПФХД</button>

<br/>
<br/>
<button onclick="extapp.getApplication().openPFHDServiceList();">Перечень услуг</button>
<button onclick="extapp.getApplication().openPFHDFinState();">Показатели ФСУ</button>
<button onclick="extapp.getApplication().openPFHDPerfomanceGoal();">Показатели целей / задач</button>
<button onclick="extapp.getApplication().openPFHDProgramEvent();">Мероприятия</button>
<button onclick="extapp.getApplication().openPFHDActivityDirection();">Направления расходов</button>
<br/>
<br/>
<button onclick="extapp.getApplication().openPFHDInstitutionProgram();">Цели / задачи</button>
<button onclick="extapp.getApplication().openPFHDInstitutionSubProgram();">Напр. 1 уровень</button>
<button onclick="extapp.getApplication().openPFHDCPPKProgram();">Напр. 2 уровень</button>
<button onclick="extapp.getApplication().openPFHDExpenseDirection();">Показатели расходов</button>
<button onclick="extapp.getApplication().openPFHDEventItem();">Мероприятия</button>
<button onclick="extapp.getApplication().openPFHDEnergySavingItem();">Энергосбережение</button>

<br/>
<br/>

<?php 
require_once SHARED_PHP_PATH . '/controls/controls.php';
$controls = new controls;
//options for select
$years = sql_to_assoc("SELECT id, name FROM plan_period ORDER BY id");

$versions = [];
$rows = sql_rows("SELECT plan_period_id y, id, name FROM stat_pfhd_ver WHERE plan_period_id<=2017 ORDER BY id DESC");
foreach($rows as $r) {
	$versions[$r['y']][$r['id']] = $r['name'];
}
$rows = sql_rows("SELECT plan_period_id y, id, name FROM pfhd_ver WHERE plan_period_id>=2018 ORDER BY plan_period_id ASC,id DESC");
foreach($rows as $r) {
	$versions[$r['y']][$r['id']] = $r['name'];
}

//defaults for select
$year_default = 2019;
$year = request_numeric_val('y',$year_default);
$ver_id_default =  isset($versions[$year]) ? max(array_keys($versions[$year])) : '';
$ver_id = request_numeric_val('ver_id',$ver_id_default);
?>

<form method="post">
	<div class="form-group">
		<label>
			Тип загрузки:
			<select name="method" id="method" onchange="verSwitch(this)">
				<?php foreach($loading_types as $type): ?>
					<option value="<?=$type['value']?>"><?=$type['name']?></option>
				<?php endforeach; ?>
			</select>
		</label>
	</div>
	<div class="form-group">
		<!-- defaults and options for ver_id будут установлены javascript-ом -->
		<label>
			Год:
			<?php $controls->print_select('y',$years,''); ?>
		</label>

		<label>
			Версия:
			<?php $controls->print_select('ver_id',[],''); ?>
		</label>
	</div>
	<div style="text-align: left">
		<p>
		<input type="checkbox" name="use_calc_table" value="1"> Передавать обоснование расчётов ПФХД </p>
		<input type="submit" name="send" value="Передать в АСУ ПФХД" />
		<a href="#" class="decor-link" onclick="showLoadParams(); return false;">Дополнительно</a>
		<div class="load-params" <? if (!(isset($_POST['create']) || isset($_POST['sendcreated']))) echo 'hidden'; ?> >
			<br/>
			<input type="submit" name="create" value="Сформировать XML" />
			<input type="submit" name="sendcreated" value="Передать сформированный XML" />
			<input type="checkbox" name="is_partly" onchange="enableSizePart();" value="1"> Передать данные в АСУ ПФХД по <input  name="size_part" type="text" disabled style="width: 70px;text-align:right" value="<?=MAX_SIZE/1024?>"> КБ
			<div class="info answer">
				<p><b>XML:</b></p>
				<textarea name="srcxml"><?=$srcxml?></textarea>
			</div>
		</div>
		<p><b>Скачать:</b></p>
		<input type="submit" name="get_xml" value="Скачать в формате XML" />
		<input type="submit" name="get_xls" value="Скачать в формате XLS" />
	</div>
</form>

<div class="info answer">
	<p><b>Ответ от сервера:</b></p>
	<textarea id="answer-str"><?=$loading_answer ?></textarea>
</div>
<div class="info result">
	<form method="post">
		<!--defaults for selects -->
		<input type="hidden" name="y" value="<?=$year?>">
		<input type="hidden" name="ver_id" value="<?=$ver_id?>">

		<input required type="text" name="key" value="<?php if(!is_null($response_key)) echo $response_key; ?>" placeholder="Введите ключ ответа сервера вида: a99a24be-68b6-4846-990e-1ba0a3cec401">
		<input type="submit" name="check_key" value="Проверить результат загрузки">
	</form>
	<textarea id="result-str"><?=$loading_result?></textarea>
</div>
<!--
<br>
<button onclick="extapp.getApplication().openPFHDLoadLog();">Журнал загрузок</button>
<br>
<h4><a href="/tedit.php?t=asu_pfhd_load_log" target="_blank">Журнал загрузок</a></h4>
-->		
<h4><a href="#" onclick="extapp.getApplication().openPFHDLoadLog();">Журнал загрузок</a></h4>

</div>
</div>
<!-- NB!!! -->
</div>

<script>
	var options = <?=json_encode($versions,JSON_UNESCAPED_UNICODE);?> || [];
	var year = <?=$year?>;
	var verId = <?=$ver_id?>;

	$(document).ready(function(){
		$('select[name="y"]').val(year);
		onChangeYear(year,verId);

		$('select[name="y"]').change(function(){
			var year = $(this).val();
			var defaultVerId = undefined;
			onChangeYear(year,defaultVerId);
		});

		function onChangeYear(year,defaultVerId) {
//			var options = [];
			fillSel('ver_id',options[year],defaultVerId);
		}

		function fillSel(selName,options,defaultVal) {
			clearSel(selName);
			$.each(options,function(id,name){
				selAddOption(selName,id,name);
			});
			if (defaultVal !== undefined) {
				$('select[name="'+selName+'"]').val(defaultVal);
			}
		}

		function clearSel(selName) {
			$('select[name="'+selName+'"] option').each(function(){
				$(this).remove();
			});
		}

		function selAddOption(selName,value,text) {
			$('select[name="'+selName+'"]').append('<option value="'+value+'">'+text+'</option>');
		}
	});

    function enableSizePart() {
        var check = document.querySelector('input[name="is_partly"]');
        var inp = document.querySelector('input[name="size_part"]');
        if (inp && check) {
            inp.disabled = !check.checked;
        }
    }
	function verSwitch(el) {
		var types = {'finActivityPlan': 'visible', 'extraValue': 'hidden'};
		var value = el.value;
		var version_control = document.querySelector("#version");
		version_control.parentNode.parentNode.style.visibility = types[value];
	} 
	
	function showLoadParams() {
		var loadParams = document.querySelector('.load-params');
		$(loadParams).toggle();
	}
</script>