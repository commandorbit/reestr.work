<?php
require 'init.php';
require 'modules/save.php';
require 'soft_delete.php';
require 'modules/attaching_files.php';
require 'modules/controls.php';
require 'modules/option_list.php';
require dirname(__FILE__) . '/zayav_fn.php';
require dirname(__FILE__) . '/upload_file_fn.php';

if (!isset($_GET['id']) ) {
	header ('Location: index.php');
	exit;
} 

$id = sql_escape($_GET['id']);    

$table = 'zayav';
$ref_key = 'zayav_id';

//Start POST handler

if(isset($_POST['zayav_delete']) && user_may('delete_zayav')) {
    sql_query("START TRANSACTION;");
    $msg = '';
    $ok = true;
    $zss = sql_rows("SELECT id FROM zayav_sostav WHERE zayav_id='$id'");
    foreach ($zss as $zs) $ok = $ok && del_row('zayav_sostav',$zs['id'],$msg);
    $ok = $ok && del_row('zayav',$id,$msg);
    if ($ok) {
        sql_query("COMMIT;");
        $_SESSION['USER_MESSAGE']['SUCCESS'] = 'Запись успешно удалена!';
        header('Location: /tedit.php?t=' . $table);
        exit();
    } else {
        sql_query("ROLLBACK;");
        $_SESSION['SYSTEM_MESSAGE'] = $msg;
    }
}

if (isset($_POST['uploaded'])){
    zayav_file_upload($id);
}

if (isset($_POST['save_parent'])) {
	$id = save($table, $_POST);
	$_GET['id'] = $id;

    $_SESSION['USER_MESSAGE']['SUCCESS'] = 'Запись успешно сохранена!';

	refresh_after_save(); // перенавправляем На id
}

if (isset($_POST['save'])) {
	$_POST[$ref_key] = $id;
	save($table_sostav, $_POST);    

	refresh_after_save();
}

//End POST handler

$T = newTD($table, array('f_id'=>$id));

if (!$T->data) {
	echo '<p class="attention">Запись не найдена!</p>';
	exit();
}

$data = $T->data[0];

$zfo = user_zfo_id_str();
if(user_has_zfo()) {
    $T->may_edit = ((!is_ro() && may_dogovor()) && (in_array(check_may_edit_zfo($id, $table), user_zfo_ids())));
} else if($zfo == NULL) {
    $T->may_edit = true;
}

if (user_has_zfo()) {
	$zfo = user_zfo_id_str();
    $T->columns['zfo_id']['edit_options'] = sql_to_assoc("select id,name from zfo where id in ($zfo)");
}

if(!may_dogovor() && may_edit_zayav()) {
    $T->may_edit = may_edit_zayav();
}

$dogovor_id = sql_get_value('dogovor_id', 'zayav', "id='$id'");
$title =  'Заявка ID: ' . $id;
include 'template/header.php';
?>

<script src="/assets/js/multiselect-plugin/modalWindowBody.js"></script>
<script src="/assets/js/multiselect-plugin/multiselect.js"></script>
<script src="/assets/js/tree-plugin/js/treeplugin.js"></script>
<style type="text/css">
h1.zayav_title {
    margin-top: 20px;
    margin-bottom:20px;
}

.zayav_attached_files h1 {
    margin-top: 15px;
    margin-bottom: 15px;
}

h1 {
    margin-top: 20px;
    margin-bottom:-60px;
}
</style>
<?

$dog_num = sql_get_value('dogovor_num', 'dogovor', "id = '$dogovor_id'");
$zayav_num = sql_get_value('nom', 'zayav2014_2015', "id_2015 = '$id'");
$dog_num = ($dog_num) ? ' | № ' . $dog_num : '';
$zayav_num = ($zayav_num) ? ' | (№ '. $zayav_num .') ' : '';

if ($dogovor_id) {
	echo '<h1 class="zayav_title">Заявка ID: ' . $id . $zayav_num . ' (<a href="http://reestr2015.unecon.ru/dogovor.php?id='.$dogovor_id.'" target="_blank">по договору ID: '.$dogovor_id. $dog_num.'</a>)</h1>';
} else {
	echo '<h1 class="zayav_title">Заявка ID: ' . $id  . $zayav_num;
}

$reestr_2014 = sql_get_value('id_2014', 'zayav2014_2015', "id_2015 = '$id'");
if ($reestr_2014) echo '<h1><a href="https://reestr.finec.ru/zayav.php?id='. $reestr_2014 .'" target="_blank">Из реестра 2014</a></h1>';

echo '<div class="parent_edit">';
    //dogovor_status_out($data);
    echo '<form method="post" action="" class="custom-buttons">';
        zayav_out($T,$data);
        if ($T->may_edit) {
            if($id != 'new') {
                echo '<p class="save_parent">';
                echo '<a href="/tedit.php?t=log_zayav_sostav&f_zayav_id='.$id.'" target="_blank">
                        <i class="fa fa-clock-o"></i> Журнал изменений</a>&nbsp;&nbsp;&nbsp;';
                echo '<input type="submit" name="save_parent" value="Сохранить">&nbsp;&nbsp;&nbsp;';
                if (user_may('delete_zayav')) {
                    echo '<input type="submit" class="delete-submit" name="zayav_delete" onclick="return confirmBox(\'Вы действительно хотите удалить заявку?\')" value="Удалить заявку">';
                }
                echo '</p>';
            } else {
                echo '<p class="save_parent"><input type="submit" name="save_parent" value="Сохранить"></p>';
            }
        }

    echo '</form>';
echo '</div>';

//--------------------------------------------------------------------------
//Прикрепленные файлы и поля для загрузки
echo '<div class="zayav_attached_files">'.html_out_attached_files($id, $table).'</div>';

//Подключаем форму для загрузки файлов
html_out_load_form();

echo '<h1>Состав заявки</h1>';

$res = array();
$diff_zs = sql_rows("SELECT zayav_sostav_id FROM v_diff_ds_zs WHERE zayav_id = '$id'");
foreach ($diff_zs as $zs) {
    $res[] = $zs['zayav_sostav_id'];
}

if(!empty($diff_zs)) echo "<p style='color: red;' align='center'><b>Внимание! В строке № \"" . implode(', ', $res) . "\" выявлено несоотвествие строкам состава договора указанного в заявке!</b></p>";

//Прикрепляем состав заявки
$T_sostav = TD_zayav_sostav($T->may_edit, $id);

//var_dump($T_sostav);
$T_sostav->out_js();

//Прикрепляем платежки
echo '<h1 style="margin-top: 40px;">Платежки</h1>';

$T_plat = TD_plat($id);
$T_plat->out_js();

echo '<h1>История</h1>';
TD_history($id)->out_js();

echo '<br /><br />';
?>

<script>
    //In app.js
    createZayav();
    specContragentName();
</script>

<?php
include 'template/footer.php';
?>