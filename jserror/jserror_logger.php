<?php 

include $_SERVER['DOCUMENT_ROOT'] . '/init.php';
$file = $_SERVER['DOCUMENT_ROOT'] . '/../reestr/DOCS/jserror.log';

if(!empty($_POST)) {
	$current = file_get_contents($file);
	$n = "";
	if(strlen($current) > 0) $n = "\n";
	$current .= $n . '---------------------START--------------------' . "\n";
	$current .= '[' . date('d.m.Y H:i:s') . ']' . ' // [Message] ' . sql_escape($_POST['message']) . ' // [Stack trace] ' . sql_escape($_POST['stack']) . "\n";
	$current .= '[DEBUG INFO - USER_AGENT] ';
	$current .= $_SERVER['HTTP_USER_AGENT'];
	$current .= "\n" . '[DEBUG INFO - USER_ID] ';
	$current .= $_SESSION['user_id'];
	$current .= "\n" . '----------------------END---------------------';
	file_put_contents($file, $current);
}

?>