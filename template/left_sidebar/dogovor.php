<?php 
if (!defined('NEW_ID')) define ('NEW_ID','new');
require_once 'thesis/thesis_functions.php';

?>
<? if($id != NEW_ID): ?>
<div class="left_sidebar">
	<ul class="nav nav-tabs" role="tablist">
		<li><a href="/dogovor.php?id=<?php echo $id ?>&page=dogovor"><i class="fa fa-file-text"></i><span>Договор</span></a></li>
		<?php if(is_admin() || user_has_permission('may_edit_dogovor_gz')): ?>
			<li><a href="/dogovor.php?id=<?php echo $id ?>&page=dogovor_gz<?=(gz_44_isset_draft() && !isset_dogovor_gz($id)) ? '&new&action=edit' : '' ?>"><i class="fa fa-file-text"></i><span>Договор (Гос. зак.)</span></a></li>
		<?php endif; ?>
		<li><a href="/dogovor.php?id=<?php echo $id ?>&page=dogovor_sostav"><i class="fa fa-list"></i><span>Состав <?php echo ($count = sql_count_rows_by_id('dogovor_sostav', 'dogovor_id', $id)) ? '(' . $count . ')' : '' ?></span></a></li>
		<li><a href="/dogovor.php?id=<?php echo $id ?>&page=dogovor_zayav"><i class="fa fa-outdent"></i><span>Заявки <?php echo ($count = sql_count_rows_by_id('zayav', 'dogovor_id', $id)) ? '(' . $count . ')' : '' ?></span></a></li>
		<li><a href="/dogovor.php?id=<?php echo $id ?>&page=dogovor_plat"><i class="fa fa-outdent"></i><span>Платежки</span></a></li>
		<li><a href="/dogovor.php?id=<?php echo $id ?>&page=dogovor_files"><i class="fa fa-chain-broken"></i><span>Файлы <?php echo ($count = sql_count_rows_by_id('dogovor_files', 'dogovor_id', $id)) ? '(' . $count . ')' : '' ?></span></a></li>
		
		<div style="border-bottom: 1px dashed #9e9e9e; width: 100%;height: 1px; display: block"></div>
		
		<li><a href="/tedit.php?t=log_dogovor_sostav&f_dogovor_id=<?php echo $id ?>" target="_blank"><i class="fa fa-clock-o"></i> <span>Журнал изменений</span></a></li>
		<li><a href="/ver_comparsion.php?dogovor_id=<?php echo $id ?>" target="_blank"><i class="fa fa-clock-o"></i> <span>Сравнение версий</span></a></li>
		
		<?php if(get_thesis_guid($id)): ?>
			<div style="border-bottom: 1px dashed #9e9e9e; width: 100%;height: 1px; display: block"></div>
			<li><a href="/thesis/thesis_dogovor.php?id=<?php echo $id ?>" target="_blank"><i class="fa fa-file-text"></i> <span>Карточка в "ТЕЗИС"</span></a></li>
		<?php endif; ?>
		
		<li><a href="/onec_dogovor.php?id=<?php echo $id ?>"><i class="fa fa-file-text"></i> <span>Связать с 1С</span></a></li>
	</ul>
</div>
<? endif; ?>