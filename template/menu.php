<div class="menu">

	<?php if(is_admin() || !user_has_zfo()): ?>
		<ul id="sprav" class="sub_menu">
				<li class="menu_separator"><span>Раздел: ЦФО</span></li>
				<li><a href="/tedit_new.php?t=zfo">ЦФО</a></li>
				<li><a href="/tedit_new.php?t=zfo_limit">Лимиты ЦФО</a></li>
				<li><a href="/tedit_new.php?t=kosgu_fp_limit">ПФХД</a></li>
				<li class="menu_separator"><span>Раздел: ФП</span></li>
				<li><a href="/tedit_new.php?t=fp_article">ФП статьи</a></li>
				<!--
				<li><a href="/extapp.php#openTreeFPArticle">ФП-статьи (Новый редактор)</a></li>
				-->
				<li><a href="/tedit_new.php?t=fp_object">ФП объекты</a></li> 
				<li><a href="/tedit_new.php?t=fp_object_type">Виды ФП объектов</a></li> 
				<li><a href="/tedit_new.php?t=fp_object_fin_source_may_smeta">Разрешённые ИФ по зданиям в сметах</a></li> 
				<li><a href="/tedit_new.php?t=fp_article_zfo">ФП статьи - ЦФО</a></li>
				<li><a href="/tedit_new.php?t=fp_article_income">ФП статьи доходов</a></li>				
				<li><a href="/tedit_new.php?t=pfhd_article">Статьи ПФХД</a></li>              
				<li class="menu_separator"><span>Поля договоров</span></li>
				<li><a href="/tedit_new.php?t=period">Периоды</a></li> 	
				<li><a href="/tedit_new.php?t=place_method">Способ размещения</a></li> 
				<li><a href="/tedit_new.php?t=plan_grafic">План-график</a></li>  
				<li><a href="/tedit_new.php?t=unit">Единицы измерения</a></li> 
				<li><a href="/tedit_new.php?t=fin_source">Источники финансирования</a></li> 
				<li><a href="/tedit.php?t=booker_fin_source">ИФ Бух</a></li> 
				<li><a href="/tedit_new.php?t=dogovor_status">Статусы договоров</a></li>
				<li class="menu_separator"><span>Другие справочники</span></li> 	   
				<li><a href="/tedit_new.php?t=prorektor">Проректора</a></li>  
				<li><a href="/tedit_new.php?t=zayav_status">Статусы заявок</a></li>  
				<li><a href="/tedit_new.php?t=bank_acc">Расч. счета</a></li>  
				<li><a href="/tedit.php?t=kosgu">Коды КОСГУ</a></li>  
				<li><a href="/tedit_new.php?t=vid_rashodov">КВР</a></li>
				<li><a href="/tedit_new.php?t=okved">Коды ОКВЕД</a></li>
				<li><a href="/tedit.php?t=contragent">Контрагенты</a></li>
				<li><a href="/extapp.php#openContragent">Контрагенты (Новый редактор)</a></li>
				<li><a href="/tedit.php?t=holiday">Праздничные выходные дни</a></li>
				<li class="menu_separator"><span>Версии</span></li> 	   
				<li><a href="/tedit_new.php?t=pfhd_ver">Версии ПФХД</a></li>
				<li><a href="/tedit_new.php?t=ls_remains">Отстаки на л/с</a></li>
				<li class="menu_separator"><span>Натуральные показатели</span></li> 	   
				<li><a href="/tedit_new.php?t=natural_indicator">Натуральные показатели</a></li>
				<li><a href="/tedit_new.php?t=natural_indicator_category">Категории натур. показателей</a></li>
				
		</ul>

		<ul id="other" class="sub_menu">
				<li><a href="/asu_pfhd_send.php">Загрузка в АСУ ПФХД</a></li>
				<li class="menu_separator"><span>Раздел: Справочники</span></li>
				<li><a href="/tedit.php?t=param">Наши реквизиты</a></li>
				<li><a href="/tedit.php?t=asu_pfhd_dop_pokaz">Перечень доп. показателей</a></li>
				<li><a href="/tedit.php?t=asu_pfhd_activity_kind">Виды финансового обеспечения</a></li>            
				<li><a href="/tedit.php?t=asu_pfhd_activity_direction">Направление расходования</a></li>
				<li><a href="/tedit.php?t=asu_pfhd_expense_direction">Статьи расходов ExpenseDirection</a></li>
				<li><a href="/tedit.php?t=asu_pfhd_institution_program">Цели/Задачи</a></li>
				<li class="menu_separator"><span>Раздел: Обоснования</span></li>
				<li><a href="/asu_pfhd/pfhd_calc_editor_title.php">Расчеты</a></li>
				<li><a href="/tedit_new.php?t=pfhd_calc_table">Таблицы</a></li>
				<li><a href="/tedit_new.php?t=pfhd_calc_table_col">Колонки</a></li>
				<li><a href="/tedit_new.php?t=pfhd_calc_table_row">Строки</a></li>
				<li><a href="/tedit_new.php?t=pfhd_calc_table_dict">Справочники</a></li>
				
		</ul>
	<?php endif ?>
	
	<ul id="limits" class="sub_menu">
		<li class="menu_separator"><span>Лимиты</span></li>
		<li><a href="/tedit_new.php?t=virt_dogovor_limit_compare">Лимиты по КВР</a></li>
		<?php if(is_admin() || !user_has_zfo()): ?>
			<li><a href="/tedit.php?t=virtual_zfo_limit&ff_fp_year=2018">Лимиты 2018</a></li>		
			<li class="menu_separator"><span>Справочники</span></li>
			<li><a href="/tedit.php?t=zfo_limit">Лимиты по ЦФО</a></li>
			<li><a href="/tedit.php?t=fin_source_limit">Лимиты по ИФ</a></li>
			<li><a href="/tedit.php?t=v_zfo_limit">Договоры - Лимиты</a></li>
			<li class="menu_separator"><span>Документы</span></li>
			<li><a href="/doc_lim.php?id=new">Создать документ изменения лимитов</a></li>
			<li><a href="/tedit.php?t=doc_lim_change">Список документов изменения лимитов</a></li>
		<?php endif; ?>
		<li class="menu_separator"><span>История</span></li>
		<li><a href="/tedit.php?t=log_zfo_limit">Журнал изменений лимитов</a></li>
	</ul>

	<ul id="dog" class="sub_menu">
		<li class="menu_separator"><span>Раздел: Договоры</span></li>
		<?php if(may_dogovor() == 1): ?>
			<li><a href="/dogovor.php?t=dogovor&amp;t_sostav=dogovor_sostav&amp;id=new">Новый договор</a></li>
		<?php endif; ?>
		<li><a href="/tedit_old.php?t=v_dogovor">Договоры</a></li>
		<li><a href="/tedit_new.php?t=virt_dogovor">Договоры 2.0</a></li>		
		<li><a href="/tedit_new.php?t=virt_dogovor_sostav_rep">Отчет Договоры</a></li>
	</ul>

	<ul id="docs" class="sub_menu">
			<li class="menu_separator"><span>Раздел: Заявки</span></li>
    		<li><a href="/tedit.php?t=virt_zayav_dog">Все заявки</a></li>
		<?php if(is_admin()): ?>
    		<li><a href="/tedit_new.php?t=virt_zayav_dog">Все заявки 2.0</a></li>
		<?php endif ?>

			<?php if (user_may('rashod_plat')) : ?>
				<li class="menu_separator"><span>Раздел: Платежки</span></li>
				<li><a href="/tedit.php?t=plat">Платежки расходные</a></li>
		    		<?php if(is_admin()): ?>	
				<li><a href="/tedit_new.php?t=v_plat">Платежки расходные 2.0</a></li>
				<?php endif ?>
			<?php endif ?>
			<? if(user_may_smeta()): ?>				
					<li class="menu_separator"><span>Раздел: Сметы</span></li>
					<li><a href="/tedit_new.php?t=smeta">Сметы</a></li>
					<li><a href="/tedit_new.php?t=v_smeta_sostav">Расходы в сметах</a></li>
					<li><a href="/tedit_new.php?t=virt_dogovor_sostav">Строчки договоров</a></li>
					<li><a href="/tedit_new.php?t=virt_smeta_month_rep">Расходы по месяцам</a></li>
					<? if(is_admin() || !user_has_zfo()): ?>	
						<li><a href="/tedit_new.php?t=virt_smeta_dogovor_compare">Сопоставление Договоров и Смет</a></li>
						<li><a href="/tedit_new.php?t=virt_smeta_limit_compare">Сопоставление Лимитов и Расходов в сметах</a></li>
						<li><a href="/tedit_new.php?t=building_percent">Здания % по Источникам</a></li>
						<li><a href="/tedit_new.php?t=building_article_percent">Здания Статьи % по Источникам</a></li>
						<li><a href="/tedit_new.php?t=fin_source_remains">Остатки ЦФР по ИФ на начало года</a></li>
						<li><a href="/tedit_new.php?t=virt_smeta_building">Расходы по зданиям</a></li>
						<li><a href="/tedit_new.php?t=virt_smeta_income">Доходы</a></li>
					<? endif ?>	
			<? endif ?>
    		<?php if(is_admin() || !user_has_zfo()): ?>	
                <li class="menu_separator"><span>Раздел: План доходов</span></li>
                <!--<li><a href="/tedit.php?t=plan_dohod&ff_plan_dohod_ver_id=<?//=plan_dohod_last_ver(); ?>">План доходов</a></li>-->
				<li><a href="/plan_dohod/plan_dohod.php">План доходов</a></li>
			<?php endif; ?>
		</ul>

		<ul id="otchet" class="sub_menu">
			<li class="menu_separator"><span>Раздел: Отчеты</span></li>
			<li><a href="/tedit.php?t=v_reestr&amp;ff_reestr_d=<?=date('Y-m-d') ?>">Реестр</a></li>
			<li><a href="/tedit.php?t=v_report_zayav&amp;ff_month_id=<?=date('n')?>&amp;ff_reestr_year=<?=date('Y')?>">Заявки</a></li>
			<li><a href="/stat_zayav.php">Аналитика заявки</a></li>
			<li><a href="/stat_plat.php">Аналитика платежки</a></li>
			<li><a href="/tedit_new.php?t=virt_smeta_dogovor_compare2">Сравнение смет и договоров</a></li>
			<?php //if(is_admin() || !user_has_zfo()): ?>
				<li><a href="/pivot/pivot.php?type=zayav_summary">Сводный отчет (заявки)</a></li>
				<li><a href="/pivot/pivot.php?type=plat_summary">Сводный отчет (платежки)</a></li>
				<li><a href="/pivot/pivot.php?type=dogovor_summary">Сводный отчет (договоры)</a></li>
			<?php //endif; ?>			
<!-- Можно удалять
			<li><a href="/tedit.php?t=v_reestr_goskontrakt">Реестр госконтрактов</a></li>
			<li><a href="/tedit.php?t=v_reestr_goskontrakt_separated">Реестр госконтрактов (раздельный)</a></li>
-->			
			<? if (is_admin() || !user_has_zfo()): ?>
            <li class="menu_separator"><span>Раздел: ПФХД</span></li>
                <!--<li><a href="/pivot/pivot.php?type=pfhd">Сводный отчет ПФХД текущий</a></li>-->
                <li><a href="/asu_pfhd/pfhd_ver_compare.php">Сводный отчет ПФХД</a></li>
                <!--<li><a href="/pivot/pivot.php?type=pfhd_diff">Сводный отчет ПФХД (отклонения)</a></li>-->
<!--    		<li><a href="/tedit.php?t=virt_pfhd">Текущие расходы ПФХД</a></li> -->
<!--            <li><a href="/asu_pfhd/pfhd_versions.php">Версии ПФХД</a></li>-->
    		<!--<li><a href="/tedit.php?t=v_pfhd_2016">Устаревший отчет ПФХД (Текущая 2016)</a></li>-->
                <li class="menu_separator"><span>Форма 737 </span></li>
				<li><a href="/form_737/form_737_pfhd_compare_new.php?type=1">Форма 737 отклонения от ПФХД c 2018</a></li>
				<li><a href="/form_737/form_737_pfhd_compare.php?type=1">Форма 737 отклонения от ПФХД до 2018</a></li>
				<li class="menu_separator"><span>Форма 738</span></li>
				<!--<li><a href="/form_738/form_738.php?type=1">Форма 738 отклонения от ПФХД</a></li>-->
				<!--<li><a href="/form_738/form_738_ver.php?id=new">Добавить новую версию 738</a></li>-->
				<li><a href="/tedit.php?t=stat_738_ver">Справочник версий 738</a></li>
				<li><a href="/tedit.php?t=v_stat_738">Форма 738 версии</a></li>
                <li><a href="/tedit.php?t=virtual_738_diff">Форма 738 сравнение версий</a></li>
            <li class="menu_separator"><span>Раздел: 1c</span></li>                
                <li><a href="/pivot/pivot_2.php?type=1C_18">[18 счет] Выбытия денежных средств</a></li>
                <li><a href="/alcher/502_11_obligation.php">Обязательства (для 738 формы)</a></li>
                <li><a href="/alcher/502_11_money_obligation.php">Денежные обязательства (для 738 формы)</a></li>
				<li class="menu_separator"><span>Отчет по остаткам</span></li> 
				<li><a href="/lonely_pages/today_report.php">Справка по движению денежных средств</a></li>
				<li><a href="/lonely_pages/cash_dynamics.php">График динамики денежных средств</a></li>
				<li><a href="/gz/gz.php">План-график (44-ФЗ)</a></li>
				<!--AVD 31.10.2016
				<li><a href="/gz/fz_44.php?download">44-ФЗ</a></li>
				-->
			<?php endif; ?>
		</ul>

		<ul id="check" class="sub_menu">
			<li><a href="/tedit.php?t=v_sverka_kosgu_if">Ошибки в плат./заявках соотв. сумм в разрезе КОСГУ, ИФ</a></li>
			<li><a href="/tedit.php?t=v_diff_fpa_fs">Состав заявки/дог. расхождения по ИФ и ФП статьям</a></li>
			<li><a href="/tedit.php?t=v_diff_ds_zs">Расхождения в связях между сост. заяв. и договоров</a></li>
			<li><a href="/tedit.php?t=v_zayav_sostav_bad_dogovor_sostav_id">Строки заявок без привязки к договору</a></li>
			<li><a href="/tedit.php?t=v_zayav_many_plat_y">Заявки с оплатой в разные годы</a></li>
			<li><a href="/tedit.php?t=v_reestr_thesis_diffs">Расхождения в договорах (СУП/ТЕЗИС)</a></li>
		</ul>	
		
		<ul id="nastr" class="sub_menu">
			<li class="menu_separator"><span>Выбор оформления</span></li>
			<li class="theme1"><a href="#" onclick="submitPallette(1)">Тема 1 (Оранжевая)</a></li>
			<li class="theme2"><a href="#" onclick="submitPallette(2)">Тема 2 (Серая)</a></li>
			<li class="theme3"><a href="#" onclick="submitPallette(3)">Тема 3 (Синяя)</a></li>
			<li class="theme4"><a href="#" onclick="submitPallette(4)">Тема 4 (Зелёная)</a></li>
			<li class="menu_separator"><span>Библиотека расширения</span></li>
			<li><a href="#" onclick="localStorageClear()">Сброс настроек всех окон</a></li>
			<? if (is_sysadmin()) :?>	
			<li class="menu_separator"><span>Работа с талицами</span></li>
			<li><a href="/tedit_new.php?t=tab">Таблицы</a></li>
			<li><a href="/tedit_new.php?t=col">Поля</a></li>
			<li class="menu_separator"><span>Работа с пользователями</span></li>
			<li><a href="/admin/user_spisok.php">Список пользователей</a></li>
            <li><a href="/admin/user_edit.php?u=new">Добавление пользователя</a></li>
			<li><a href="/tedit_new.php?t=role_permission">Права групп пользователей</a></li>
            <li class="menu_separator"><span>Другое</span></li>
            <li><a href="/tedit.php?t=dogovor_sostav_restore">Восстановление записей договора</a></li>
            <li><a href="/lonely_pages/load_okei.php">Загрузка справочника ОКЕИ</a></li>
<!--            <li><a href="/lonely_pages/union_xml.php">Слияние XML-форм 6НДФЛ</a></li> -->
            <li><a href="/lonely_pages/1c_dogovor_linking.php">Привязка Договоров 1с</a></li>
			<? endif; ?>
		</ul>
</div>
<div class="cleared"></div>
