<?php
ob_start(); 
define ('VER', '2018.11.21.01');
$bad_title = 'Без заголовка';
//NB!!!
//include 'init.php';
//include_once $_SERVER['DOCUMENT_ROOT'] . '/init.php';
set_time_limit(0);
error_reporting(E_ALL | E_STRICT);
//
if (isset($_SESSION["user_id"])) 
{
	$user_id = $_SESSION["user_id"];
}
else
{
	$url = urlencode($_SERVER['REQUEST_URI']);
    header('Location: /login.php?url=' . $url);	
}	
set_palette();
//if (!isset($_SESSION["palette"])) 
function set_palette() 
{
	global $user_id;
	$r = sql_rows("SELECT palette FROM user WHERE id = '$user_id'");
	$_SESSION["palette"] = $r[0]['palette'];
	if (!isset($_SESSION["palette"])) $_SESSION["palette"] = 1;
	if (!$_SESSION["palette"]) $_SESSION["palette"] = 1;
	if (isset($_REQUEST["palette"])) 
	{	
		if ($_REQUEST["palette"] >= 1 && $_REQUEST["palette"] <= 4)
			$_SESSION["palette"] = $_REQUEST["palette"];
		$user_palette = $_SESSION["palette"];
		sql_query("UPDATE user SET palette = '$user_palette' WHERE id = '$user_id'");
	}
}
if (isset($_REQUEST["exit"])) 
{	
	//unset($_SESSION["user_id"]);
	go_exit();
}
?>
<!DOCTYPE HTML>
<html>
<head>
<!-- Старый дизайн -->
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="Content-Type" content="text/html;charset=utf-8" >
	<!-- <meta http-equiv="pragma" content="no-cache"> -->
	<title><?php if(isset($title)) { echo $title; } else { echo $bad_title; } ?></title>
	<!-- 
	<link rel="stylesheet" type="text/css" href="/assets/css/style.css">
	-->
	<link rel="stylesheet" type="text/css" href="/assets/css/style.css?ver=<?=VER?>">
	
	<script>
		
		window.addEventListener("error", function (e) {
			var data = Object.create(null);
			data.message = e.error.message;
			data.stack = e.error.stack;
			if(e.error.lineNumber) data.line = e.error.lineNumber;
		    $.ajax({
			  url: "/jserror/jserror_logger.php",
			  type: "POST",
			  data: data,
			  success: function(data) {
			  	console.log(data);
				alert(data);
			  },
			  error: function (xhr, ajaxOptions, thrownError) {
			  	console.info("JSError logger is crashed");
		        console.log(xhr.status);
		        console.log(thrownError);
		      }
			});
		});
		
	</script>
<?
$theme_name = 
[
    1 => "orange",
    2 => "gray",
    3 => "blue",
    4 => "green"
];
//echo $theme_name[$_SESSION["palette"]];
?>
	
	<!-- Font icons set (Иконки в меню (иконочный шрифт)) -->
	<link rel="stylesheet" href="/assets/css/font-awesome.min.css?ver=<?=VER?>">

	<!-- RESIZE PLUGIN -->
	<link rel="stylesheet" type="text/css" href="//cdn.unecon.ru/css/libunicorn/v1/resizeplugin.css?ver=<?=VER?>">
	<script src="//cdn.unecon.ru/js/libunicorn/v1/resizeplugin.js?ver=<?=VER?>" type="text/javascript"></script>   

	<!-- FAVICON -->
    <link rel="icon" type="image/png" href="/favicon.png" />		

    <!--jquery date-picker + multi-select-->
 	<script src="//cdn.unecon.ru/js/jquery-2.1.1.min.js?ver=<?=VER?>" type="text/javascript"></script>   
	
	<!-- FLOT JS -->
	<script src="//cdn.unecon.ru/js/flotjs/jquery.flot.js" type="text/javascript"></script>   
	<script src="//cdn.unecon.ru/js/flotjs/jquery.flot.time.js" type="text/javascript"></script>   

	<script src="//cdn.unecon.ru/js/jquery-ui-1.10.3.custom/js/jquery-ui-1.10.3.custom.min.js"></script>
	<script src="//cdn.unecon.ru/js/jquery-ui-1.10.3.custom/development-bundle/ui/minified/jquery.ui.dialog.min.js"></script>
	<script src="//cdn.unecon.ru/js/jquery.maskedinput.min.js"></script>
	<link rel="stylesheet" type="text/css" href="//cdn.unecon.ru/js/jquery-ui-1.10.2.custom/css/dim_theme/jquery-ui-1.10.1.custom.min.css">
 	<script src="//cdn.unecon.ru/js/jquery-ui-1.10.2.custom/js/jquery.ui.datepicker-ru.js" type="text/javascript"></script>   
 	<!-- Не понятно где используется -->
	<!--<script src="/assets/js/dropdownchecklist/ui.dropdownchecklist-1.4-min.js" type="text/javascript"></script>
		<script src="/assets/js/dropdownsticker/dropdownsticker.js?ver=<?=VER?>" type="text/javascript"></script>
		<link rel="stylesheet" type="text/css" href="/assets/js/dropdownsticker/dropdownsticker.css">-->

	<!-- MULTISELECT (Старый мультиселект используюется в аналитике заявки/платежки)-->
	<link rel="stylesheet" type="text/css" href="/assets/js/multiselect-plugin/multiselect.css?ver=<?=VER?>">
	
	<!-- PAGINATOR -->
	<script src="//cdn.unecon.ru/js/libunicorn/v1/paginator.js?ver=<?php VER?>" type="text/javascript"></script>
	<link rel="stylesheet" type="text/css" href="//cdn.unecon.ru/css/libunicorn/v1/paginator.css?ver=<?=VER?>">

	<!-- PIVOT -->
	<script src="//cdn.unecon.ru/js/pivot/v1/pivot.js?ver=<?=VER?>" type="text/javascript"></script>
	<script src="//cdn.unecon.ru/js/pivot/v1/pivot-calc.js?ver=<?=VER?>" type="text/javascript"></script>
	<script src="//cdn.unecon.ru/js/pivot/v1/pivot-settings.js?ver=<?=VER?>" type="text/javascript"></script>
	<script src="//cdn.unecon.ru/js/pivot/v1/pivot-draw.js?ver=<?=VER?>" type="text/javascript"></script>
	<script src="//cdn.unecon.ru/js/pivot/v1/pivot-ev.js?ver=<?=VER?>" type="text/javascript"></script>
	
	<!-- Libunecorn -->
	<script src="//cdn.unecon.ru/js/libunicorn/v1/multiselect.js?ver=<?=VER?>" type="text/javascript"></script>
	<script src="//cdn.unecon.ru/js/libunicorn/v1/favoritesfilter.js?ver=<?=VER?>" type="text/javascript"></script>
	<script src="//cdn.unecon.ru/js/libunicorn/v1/columnsfilter.js?ver=<?=VER?>" type="text/javascript"></script>
	<script src="//cdn.unecon.ru/js/libunicorn/v1/favoriteColorSelect.js?ver=<?=VER?>" type="text/javascript"></script>
	<script src="//cdn.unecon.ru/js/libunicorn/v1/treeplugin.js?ver=<?=VER?>" type="text/javascript"></script>
	<script src="//cdn.unecon.ru/js/libunicorn/v1/table-fix-top.js?ver=<?=VER?>" type="text/javascript"></script>
	<script src="//cdn.unecon.ru/js/libunicorn/v1/table-drag-drop.js?ver=<?=VER?>" type="text/javascript"></script>

	<link rel="stylesheet" type="text/css" href="//cdn.unecon.ru/css/libunicorn/v1/multiselect.css?ver=<?=VER?>">
	<link rel="stylesheet" type="text/css" href="//cdn.unecon.ru/css/libunicorn/v1/treeplugin.css?ver=<?=VER?>">
	<link rel="stylesheet" type="text/css" href="//cdn.unecon.ru/css/pivot/v1/progress-bar.css?ver=<?=VER?>">
	<link rel="stylesheet" type="text/css" href="//cdn.unecon.ru/css/pivot/v1/pivot.css?ver=<?=VER?>">
	<link rel="stylesheet" type="text/css" href="//cdn.unecon.ru/css/pivot/v1/pivot-settings.css?ver=<?=VER?>">
	<link rel="stylesheet" type="text/css" href="//cdn.unecon.ru/css/service-ico.css?ver=<?=VER?>">
	
	<!--td-->
	<script src="/td/js/myfn.js?ver=<?=VER?>" type="text/javascript"></script>
	<script src="/td/js/ajax.js?ver=<?=VER?>" type="text/javascript"></script>
	<!--<script src="/td/js/menu.js?ver=<?=VER?>" type="text/javascript"></script>-->
	<script src="/assets/js/app.js?ver=<?=VER?>" type="text/javascript"></script>
	<script src="/td/js/colmovesize.js?ver=<?=VER?>" type="text/javascript"></script>
	<script src="/td/js/winmove.js?ver=<?=VER?>" type="text/javascript"></script>
	<script src="/td/js/td.js?ver=<?=VER?>" type="text/javascript"></script>
	<script src="/td/js/edit-box.js?ver=<?=VER?>" type="text/javascript"></script>
	<script src="/td_reestr/js/edit-box-init.js?ver=<?=VER?>" type="text/javascript"></script>
	<link rel="stylesheet" type="text/css" href="/td/css/editbox.css?ver=<?=VER?>">
	<link rel="stylesheet" type="text/css" href="/td/css/td.css?ver=<?=VER?>">

	<script type="text/javascript">
    var time_start=new Date();
    function keep_alive()
	{
	    var time = 60000; // 1 mins
	    setTimeout(
	        function ()
	        {
	        $.ajax({
	           url: '/modules/keep_alive_session.php',
	           cache: false,
	           complete: function () {keep_alive();}
	        });
	    },
	    time
	);
	};

	$(document).ready(function()
	{
	//	$('input[type=text]').attr("autocomplete", "off");
	//	$('.date').mask("99.99.9999");
	//	$('.mob_phone').mask("(999)999-99-99");	
	//	$('.date').datepicker();
		//AVD
		avd.holiday = [];
		/*
		var xhr = new XMLHttpRequest();
		xhr.open('GET', '/ListM.php', false);
		xhr.send();
		if (xhr.status == 200) 
		{
			var tmp_holiday = JSON.parse(xhr.responseText);
			for (var h = 0; h < tmp_holiday.length; h++)
			{
				var dh = new Date(tmp_holiday[h].date);
				dh.setHours(0, 0, 0, 0);
				avd.holiday[dh] = {};
				avd.holiday[dh].description = tmp_holiday[h].description;
				avd.holiday[dh].workday = +tmp_holiday[h].workday;
			}
		}
		*/
		//AVD
		$.ajax({
			url: "/ListM.php",
			success: function(result)
			{
				var tmp_holiday = JSON.parse(result);
				//alert(avd.parseObj(tmp_holiday));
				for (var h = 0; h < tmp_holiday.length; h++)
				{
					var dh = new Date(tmp_holiday[h].date);
					dh.setHours(0, 0, 0, 0);
					avd.holiday[dh] = {};
					avd.holiday[dh].description = tmp_holiday[h].description;
					avd.holiday[dh].workday = +tmp_holiday[h].workday;
				}
			}
		});
		//
		avd.highlightDays = function highlightDays(date) 
		{
			var className = ''; 
			var tooltip = '';
			if (avd.holiday[date]) 
			{
				className = avd.holiday[date].workday == 0 ? 'highlight' : 'lowlight';
				tooltip = avd.holiday[date].description;
			}
			return [true, className, tooltip];
		}
		$('.date').datepicker({beforeShowDay: avd.highlightDays});
		//
		keep_alive();
		$('#but_ok , #save').click(function(event) {
			
			//if (has_nulls($(this).parents('form'))) return false;
			var form= $(this).parents('form');
			var ver=form.find('input[name="ver"]').val();
			var min_ver=0;
			if (ver<=min_ver) {
				alert ('Нельзя менять записи для версии <=1');
				return false;
			}
			if (has_nulls(form) ) return false;
		});
       
	});
	</script>

<!-- Новый дизайн -->
<link rel="icon" type="image/png" href="/img/ico/favicon16.ico" />		
<link rel="stylesheet" type="text/css" href="/css/common.css?ver=<?=VER?>">
<link rel="stylesheet" type="text/css" href="/css/editbox.new.css?ver=<?=VER?>">
<link rel="stylesheet" type="text/css" href="/css/theme<?=$_SESSION["palette"]?>.css">
<link rel="stylesheet" type="text/css" href="/css/editbox.new2.css?ver=<?=VER?>">
<link rel="stylesheet" type="text/css" href="/css/avd.css?ver=<?=VER?>">

</head>
<body>
<div class="top-header">
	<!-- title="СУП - система управленческого планирования" -->
	<div class="th-logo-dev">
		<a href="/" data-tooltip="СУП - система управленческого планирования"><img alt="СУП" src="/img/ico/main-logo32.ico" style="position: relative; top: 8px"/>СУП</a>
	</div>
	<div class="th-user">
		<a href="/user_profile.php">
		<div class="user_pic">
			<div class="user_pic2">
				<!--
				<img src="/img/users/AVD.jpg" data-tooltip="Смотреть/редактировать профиль">
				-->
				<img alt="" src="<? echo user_pic(40); ?>" data-tooltip="Смотреть/редактировать профиль">
			</div>
		</div>
		</a>
		<!-- <i class="fa fa-user"></i> -->
		<div class="user_info"><?php echo login_info() ?></div>
		<?php 
			$last_id = (isset($_SESSION['last_user_id'])) ? $_SESSION['last_user_id'] : false; 
			if ($last_id && is_admin_changed() != $_SESSION['user_id']) 
				echo "<div class='user_info'><a data-tooltip='Вернуться под Вашим логином' class='top-header-lnk' href='/change_user.php?newer_user_id=$last_id'><i class='fa fa-arrow-left'></i></a></div>";
		?>
		<!--
		<b>
		<a class="top-header-lnk" href="/login.php?exit">Выйти</a>
		</b>
		-->
		<div class="user_info">
			<a class="top-header-lnk" href="http://get.teamviewer.com/unecon" target="_blank" style="font-size: 20px; vertical-align: -3px;" data-tooltip="Техническая поддержка">
				<i class="fa fa-question-circle"></i>
			</a>
		</div>
		<div class="user_info">
			<a id="exit" class="top-header-lnk" href="#" data-tooltip="Выход из системы">
				<img alt="" src="/img/ico/menu-exit.ico">
			</a>
		</div>
	</div>
	<div class="main_menu_container">
		<ul class="main_menu">
			<li><a href="#docs"><img alt="" src="/img/ico/menu-docs.ico"/>Документы</a></li>
			<li><a href="#dog"><img alt="" src="/img/ico/menu-contracts.ico"/>Договоры</a></li>
			<li><a href="#otchet"><img alt="" src="/img/ico/menu-reports.ico"/>Отчеты</a></li>
			<li><a href="#check"><img alt="" src="/img/ico/menu-cheks.ico"/>Проверки</a></li>
			<li><a href="#limits"><img alt="" src="/img/ico/menu-limits.ico"/>Лимиты</a></li>
			<?php if(is_admin() || !user_has_zfo()): ?>
			<li><a href="#sprav"><img alt="" src="/img/ico/menu-reference.ico"/>Справочники</a></li>
			<li><a href="#other"><img alt="" src="/img/ico/menu-cloud.ico"/>АСУ ПФХД</a></li>
			<?php endif ?>
			<li><a href="#nastr"><img alt="" src="/img/ico/menu-settings.ico"/>Настройки</a></li>
		</ul>
	</div>
</div>
<div class="th-menu">
	<?php require __DIR__ . '/menu.php' ?>
</div>

<!-- Новый дизайн -->
<script src="/js/avd.js?ver=<?=VER?>">"></script>
<script src="/js/Name.js?ver=<?=VER?>">"></script>
<script src="/js/menu.js?ver=<?=VER?>">"></script>
<!-- Переключатель оформления -->
<form id="paletteForm" method="post"><input id="paletteFormValue" type="hidden" name="palette" value="777"></form>
<!-- Выход -->
<form id="exitForm" method="post"><input type="hidden" name="exit" value="1"></form>
<script>
	//alert('Welcome!');
	//avd.makeExplosion(document.getElementsByClassName('top-header')[0], 'A', 'mouseover');
	Menu('.main_menu a', '.sub_menu');
	function submitPallette(e)
	{
		//alert(e);
		//alert(avd.parseObj(e));
		var form = document.getElementById("paletteForm");
		if (form) 
		if (avd.isNumeric(e))
		{
			var paletteFormValue = document.getElementById("paletteFormValue");
			if (paletteFormValue)
				paletteFormValue.setAttribute('value', e);
		}
		form.submit();
	}
	
	function submitExit()
	{
		var form = document.getElementById("exitForm");
		if (form) form.submit();
	}
	
	function localStorageClear() {
		var form = document.getElementById("paletteForm");
		for (var key in window.localStorage) 
			if (window.localStorage.getItem(key)) {
				window.localStorage.removeItem(key)
				console.log('Ключ', key, 'удаляем!!!');
			}
		form.submit();
	}
	
	function onWinfowLoad()
	{
		avd.init();
		window.addEventListener("resize", function(event)
		{
			//alert(document.width);
		});
		//$("th").css("color", "green");
		//alert(avd.parseObj($("th")));
	}
	
	//CHAV {
	var f = window.onload;
	window.onload = function()
	{
		onWinfowLoad();
		if (typeof f === "function") f();
	}
	//} CHAV
	//window.onload = onWinfowLoad();

	var el = document.getElementById("palette");
	if (el) el.onclick = submitPallette;

	var el = document.getElementById("exit");
	if (el) el.onclick = submitExit;
	
	//TEST
	$(document).ready(function()
	{
		//$("li a").css("color", "lightblue");  
		//$(".top-header a").css("color", "lightblue");  
		//alert(document.width);
		$("div").click(function(event)
		{
			//alert(avd.parseObj(event.target));
			//event.stopPropagation();
		});
	});	

	//Ждём загрузки EXTJS
	window.addEventListener("load", function(event)
	{
		var waitExtJSTimer = setInterval(function()
		{
			if (window.extapp && window.extapp.getApplication)
			{
				window.extapp.userId = <?=$user_id?>;
				/*
				$('.menu a[href^="/tedit_new.php"]').click( function(ev) {
					var found =  ev.target.href.match(/\/tedit_new\.php\?t=([a-zA-Z_0-9]+)$/);
					if (found) {
						ev.stopPropagation();
						ev.preventDefault();
						var tname = found[1];
						console.log(tname);
						extapp.getApplication().openTable(tname);
						return false;	
					}
					//console.log(found);
				});
				*/
				//alert('USER = ' + window.extapp.userId);
				clearInterval(waitExtJSTimer);
			}
		},
		100);	
	});

</script>


<?
//echo '<pre><code>'; 
//echo $_SERVER['SCRIPT_NAME'];
//var_dump($_SERVER); 
//echo '</code></pre>';
?>

<?php if(array_key_exists('SYSTEM_MESSAGE', $_SESSION)): ?>

	<div class="system-message" style="width: calc(100% - 20px); padding: 10px; background: #bd6666; color: #fff; font-weight: bold; text-align: center; position: relative;">
		<div style="color: #fff;font-size: 22px;position: absolute;top: 5px;right: 35px;"><i onclick="delete_system_message()" style="cursor: pointer;" class="fa fa-times"></i></div>
		<?php 
			echo $_SESSION['SYSTEM_MESSAGE'];
			unset($_SESSION['SYSTEM_MESSAGE']);
		?>
	</div>

<?php endif; ?>

<?php if(array_key_exists('SYSTEM_MESSAGE_SUCCESS', $_SESSION)): ?>

	<div class="system-message" style="width: calc(100% - 20px); padding: 10px; background: #66bd66; color: #fff; font-weight: bold; text-align: center; position: relative;">
		<div style="color: #fff;font-size: 22px;position: absolute;top: 5px;right: 35px;"><i onclick="delete_system_message()" style="cursor: pointer;" class="fa fa-times"></i></div>
		<?php 
			echo $_SESSION['SYSTEM_MESSAGE_SUCCESS'];
			unset($_SESSION['SYSTEM_MESSAGE_SUCCESS']);
		?>
	</div>

<?php endif; ?>
