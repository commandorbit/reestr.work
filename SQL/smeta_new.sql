﻿--
-- Установка базы данных по умолчанию
--
USE reestr2015;

--
-- Создать внешний ключ
--
ALTER TABLE smeta_fin_param 
  ADD CONSTRAINT FK_smeta_smeta_fin_param_smeta_id FOREIGN KEY (smeta_id)
    REFERENCES smeta(id) ON DELETE NO ACTION ON UPDATE NO ACTION;