﻿DROP VIEW IF EXISTS v_smeta_sostav;
CREATE VIEW v_smeta_sostav AS
SELECT
  smeta_sostav.id,
  smeta.fp_year_id,
  IFNULL(smeta_sostav.zfo_id, smeta.zfo_id) AS zfo_id,
  smeta_sostav.fp_article_id,
  smeta_sostav.fin_source_id,
  smeta_sostav.smeta_id,
  CONCAT(IFNULL(CONCAT('', smeta.description), ''), IFNULL(CONCAT(' --> ', smeta_sostav.description), ''), IFNULL(CONCAT(' ---> ', natural_indicator.name), '')) AS name,
  smeta_sostav.amount,
  SUM(dogovor_sostav.amount) AS dogovor_sostav_amount,
  IFNULL(smeta_sostav.amount, 0) - IFNULL(SUM(dogovor_sostav.amount), 0) AS unallocated,
  smeta_sostav.fp_object_id,
  natural_indicator.name AS natural_indicator_name,
  natural_indicator.unit_id,
  smeta_sostav.price,
  smeta_sostav.quantity
FROM smeta_sostav
  INNER JOIN smeta
    ON smeta_sostav.smeta_id = smeta.id
  LEFT OUTER JOIN natural_indicator
    ON smeta_sostav.natural_indicator_id = natural_indicator.id
  LEFT OUTER JOIN dogovor_sostav_smeta_sostav
    ON dogovor_sostav_smeta_sostav.smeta_sostav_id = smeta_sostav.id
  LEFT OUTER JOIN dogovor_sostav
    ON dogovor_sostav.id = dogovor_sostav_smeta_sostav.dogovor_sostav_id
GROUP BY 
  smeta_sostav.id,
  smeta_sostav.fp_object_id,
  natural_indicator.name,
  natural_indicator.unit_id,
  smeta_sostav.price,
  smeta_sostav.quantity
ORDER BY smeta.fp_year_id DESC, zfo_id, smeta_sostav.fp_article_id, smeta_sostav.fin_source_id, smeta_sostav.smeta_id;

SELECT * FROM v_smeta_sostav 
