﻿SELECT smeta_id, zfo_id, fin_source_id, kvr, 
  MAX(ver_old) AS ver_old, MAX(ver_new) AS ver_new, 
  SUM(amount_old) AS amount_old, SUM(amount_new) AS amount_new, 
  SUM(amount_new) - SUM(amount_old) AS amount_delta FROM
( SELECT * FROM
  ( SELECT
      ss.smeta_id,
      IFNULL(ss.zfo_id, s.zfo_id) AS zfo_id,
      ss.fin_source_id,
      fpa.kvr,
      0 AS ver_old,
      s.ver AS ver_new,
      0 AS amount_old,
      SUM(ss.amount) AS amount_new,
      COUNT(ss.amount) AS c
    FROM smeta_sostav ss
      INNER JOIN smeta s
        ON ss.smeta_id = s.id
      LEFT OUTER JOIN fp_article AS fpa
        ON ss.fp_article_id = fpa.id
    GROUP BY ss.smeta_id, s.ver, IFNULL(ss.zfo_id, s.zfo_id), ss.fin_source_id, fpa.kvr
  ) AS T1
  UNION ALL
  SELECT * FROM
  ( SELECT
      ss.smeta_id,
      IFNULL(ss.zfo_id, s.zfo_id) AS zfo_id,
      ss.fin_source_id,
      fpa.kvr,
      ss.ver AS ver_old,
      0 AS ver_new,      
      SUM(ss.amount) AS amount_old,
      0 AS amount_new,
      COUNT(ss.amount) AS c
    FROM ver_smeta_sostav ss
      INNER JOIN ver_smeta s
        ON ss.smeta_id = s.id AND ss.ver = s.ver
      LEFT OUTER JOIN fp_article AS fpa
        ON ss.fp_article_id = fpa.id
      WHERE s.ver = (SELECT MAX(ver) FROM ver_smeta WHERE id = s.id) 
      GROUP BY ss.smeta_id, IFNULL(ss.zfo_id, s.zfo_id), ss.fin_source_id, fpa.kvr, s.ver
  ) AS T2  
) T
GROUP BY smeta_id, zfo_id, fin_source_id, kvr
ORDER BY smeta_id, zfo_id, fin_source_id, kvr