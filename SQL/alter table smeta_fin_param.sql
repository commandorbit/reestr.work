﻿ALTER TABLE `smeta_fin_param`  
  ADD `sum_credit1` DOUBLE NOT NULL,  
  ADD `sum_credit2` DOUBLE NOT NULL,  
  ADD `sum_credit3` DOUBLE NOT NULL,  
  ADD `sum_credit4` DOUBLE NOT NULL,  
  ADD `sum_credit5` DOUBLE NOT NULL,  
  ADD `sum_credit6` DOUBLE NOT NULL,  
  ADD `sum_credit7` DOUBLE NOT NULL,  
  ADD `sum_credit8` DOUBLE NOT NULL,  
  ADD `sum_credit9` DOUBLE NOT NULL,  
  ADD `sum_credit10` DOUBLE NOT NULL, 
  ADD `sum_credit11` DOUBLE NOT NULL,  
  ADD `sum_credit12` DOUBLE NOT NULL

ALTER TABLE `log_smeta_fin_param`  
  ADD `sum_credit1` DOUBLE NOT NULL,  
  ADD `sum_credit2` DOUBLE NOT NULL,  
  ADD `sum_credit3` DOUBLE NOT NULL,  
  ADD `sum_credit4` DOUBLE NOT NULL,  
  ADD `sum_credit5` DOUBLE NOT NULL,  
  ADD `sum_credit6` DOUBLE NOT NULL,  
  ADD `sum_credit7` DOUBLE NOT NULL,  
  ADD `sum_credit8` DOUBLE NOT NULL,  
  ADD `sum_credit9` DOUBLE NOT NULL,  
  ADD `sum_credit10` DOUBLE NOT NULL, 
  ADD `sum_credit11` DOUBLE NOT NULL,  
  ADD `sum_credit12` DOUBLE NOT NULL

ALTER TABLE `ver_smeta_fin_param`  
  ADD `sum_credit1` DOUBLE NOT NULL,  
  ADD `sum_credit2` DOUBLE NOT NULL,  
  ADD `sum_credit3` DOUBLE NOT NULL,  
  ADD `sum_credit4` DOUBLE NOT NULL,  
  ADD `sum_credit5` DOUBLE NOT NULL,  
  ADD `sum_credit6` DOUBLE NOT NULL,  
  ADD `sum_credit7` DOUBLE NOT NULL,  
  ADD `sum_credit8` DOUBLE NOT NULL,  
  ADD `sum_credit9` DOUBLE NOT NULL,  
  ADD `sum_credit10` DOUBLE NOT NULL, 
  ADD `sum_credit11` DOUBLE NOT NULL,  
  ADD `sum_credit12` DOUBLE NOT NULL
