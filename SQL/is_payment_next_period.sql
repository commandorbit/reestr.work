﻿--
-- Скрипт сгенерирован Devart dbForge Studio for MySQL, Версия 8.0.124.0
-- Домашняя страница продукта: http://www.devart.com/ru/dbforge/mysql/studio
-- Дата скрипта: 29.11.2018 18:26:03
-- Версия сервера: 5.6.31
-- Версия клиента: 4.1
-- Пожалуйста, сохраните резервную копию вашей базы перед запуском этого скрипта
--


SET NAMES 'utf8';

--
-- Установка базы данных по умолчанию
--
USE reestr2015;

-- is_payment_next_period 
ALTER TABLE     smeta_sostav ADD COLUMN is_payment_next_period  TINYINT(4) DEFAULT 1 AFTER amount_next;
ALTER TABLE log_smeta_sostav ADD COLUMN is_payment_next_period TINYINT(4) AFTER amount_next;
ALTER TABLE ver_smeta_sostav ADD COLUMN is_payment_next_period TINYINT(4) AFTER amount_next;

/*
ALTER TABLE     smeta_income ADD COLUMN is_payment_next_period TINYINT(4) DEFAULT 1;
ALTER TABLE log_smeta_income ADD COLUMN is_payment_next_period TINYINT(4);
ALTER TABLE ver_smeta_income ADD COLUMN is_payment_next_period TINYINT(4);
*/
