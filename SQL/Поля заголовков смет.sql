﻿--
-- Скрипт сгенерирован Devart dbForge Studio for MySQL, Версия 8.0.80.0
-- Домашняя страница продукта: http://www.devart.com/ru/dbforge/mysql/studio
-- Дата скрипта: 13.09.2018 12:04:17
-- Версия сервера: 5.6.31
-- Версия клиента: 4.1
-- Пожалуйста, сохраните резервную копию вашей базы перед запуском этого скрипта
--


SET NAMES 'utf8';

--
-- Установка базы данных по умолчанию
--
USE reestr2015;

--
-- Удалить столбец `fin_source_id` из таблицы `log_smeta`
--
-- ALTER TABLE log_smeta 
--  DROP COLUMN fin_source_id;

--
-- Установка базы данных по умолчанию
--
USE reestr2015;

--
-- Удалить столбец `amount_limit` из таблицы `smeta`
--
ALTER TABLE smeta 
  DROP COLUMN amount_limit;

--
-- Удалить столбец `amount_limit_budjet` из таблицы `smeta`
--
ALTER TABLE smeta 
  DROP COLUMN amount_limit_budjet;

--
-- Удалить столбец `amount_limit_exbudjet` из таблицы `smeta`
--
ALTER TABLE smeta 
  DROP COLUMN amount_limit_exbudjet;
