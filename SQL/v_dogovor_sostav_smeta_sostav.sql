﻿DROP VIEW IF EXISTS v_dogovor_sostav_smeta_sostav;
CREATE VIEW v_dogovor_sostav_smeta_sostav AS
SELECT
  v_smeta_sostav.id,
  v_smeta_sostav.fp_year_id,
  v_smeta_sostav.zfo_id,
  v_smeta_sostav.fp_article_id,
  v_smeta_sostav.fin_source_id,
  dogovor_sostav.dogovor_id,
  dogovor_sostav.description AS dogovor_description,
  dogovor_sostav.amount AS dogovor_amount,
  v_smeta_sostav.smeta_id,
  v_smeta_sostav.description AS smeta_description,
  v_smeta_sostav.description,
  v_smeta_sostav.price,
  v_smeta_sostav.quantity,
  v_smeta_sostav.amount AS smeta_amount,
  v_smeta_sostav.natural_indicator_id
FROM dogovor_sostav
  INNER JOIN dogovor
    ON dogovor_sostav.dogovor_id = dogovor.id
  INNER JOIN v_smeta_sostav
    ON dogovor.fp_year = v_smeta_sostav.fp_year_id
    AND dogovor_sostav.fp_article_id = v_smeta_sostav.fp_article_id
    AND dogovor_sostav.fin_source_id = v_smeta_sostav.fin_source_id
    AND dogovor.zfo_id = v_smeta_sostav.zfo_id
ORDER BY v_smeta_sostav.fp_year_id, v_smeta_sostav.zfo_id, v_smeta_sostav.fp_article_id, v_smeta_sostav.fin_source_id, dogovor_sostav.dogovor_id;

SELECT * FROM v_dogovor_sostav_smeta_sostav;