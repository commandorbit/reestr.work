﻿DROP VIEW IF EXISTS virt_dogovor_sostav;
CREATE VIEW virt_dogovor_sostav AS
SELECT
  dogovor_sostav.id,
  dogovor.fp_year fp_year_id,
  dogovor.zfo_id,
  dogovor_sostav.fp_article_id,
  dogovor_sostav.fin_source_id,
  dogovor_sostav.dogovor_id,
  CONCAT(
    IFNULL(CONCAT('', dogovor.subject), ''), 
    IFNULL(CONCAT(' - ', dogovor_sostav.description), '')
  ) AS name,
  dogovor.subject,
  dogovor_sostav.description,
  dogovor_sostav.amount,
  dogovor_sostav_smeta_sostav.smeta_sostav_id,
  smeta_sostav.smeta_id, 
  smeta_sostav.amount AS smeta_sostav_amount
FROM dogovor_sostav
  INNER JOIN dogovor
    ON dogovor_sostav.dogovor_id = dogovor.id
  LEFT OUTER JOIN dogovor_sostav_smeta_sostav
    ON dogovor_sostav.id = dogovor_sostav_smeta_sostav.dogovor_sostav_id
  LEFT OUTER JOIN smeta_sostav
    ON smeta_sostav.id = dogovor_sostav_smeta_sostav.smeta_sostav_id
ORDER BY 
  dogovor.fp_year DESC, 
  dogovor.zfo_id, 
  dogovor_sostav.fp_article_id, 
  dogovor_sostav.fin_source_id, 
  dogovor_sostav.dogovor_id;

SELECT * FROM virt_dogovor_sostav;