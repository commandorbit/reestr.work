﻿--
-- Скрипт сгенерирован Devart dbForge Studio for MySQL, Версия 8.0.40.0
-- Домашняя страница продукта: http://www.devart.com/ru/dbforge/mysql/studio
-- Дата скрипта: 15.08.2018 10:19:04
-- Версия сервера: 5.6.31
-- Версия клиента: 4.1
-- Пожалуйста, сохраните резервную копию вашей базы перед запуском этого скрипта
--

SET NAMES 'utf8';

--
-- Установка базы данных по умолчанию
--
USE reestr2015;

--
-- Создать столбец `protected` для таблицы `smeta_sostav`
--
ALTER TABLE smeta_sostav 
  ADD COLUMN protected TINYINT(4) NOT NULL DEFAULT 0;