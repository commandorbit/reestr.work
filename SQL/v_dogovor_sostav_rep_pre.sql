﻿-- Final view structure for view `v_dogovor_sostav_rep_pre`
DROP VIEW IF EXISTS `v_dogovor_sostav_rep_pre`;

CREATE VIEW `v_dogovor_sostav_rep_pre` AS 
select 
  `d`.`id` AS `id`,
  `d`.`dogovor_status_id` AS `dogovor_status_id`,
  `d`.`fp_year` AS `fp_year`,
  `d`.`zfo_id` AS `zfo_id`,
  `d`.`document_type_id` AS `document_type_id`,
  `d`.`d` AS `d`,
  `d`.`contragent_id` AS `contragent_id`,
  `ctgnt`.`inn` AS `inn`,`d`.`dogovor_num` AS `dogovor_num`,
  `d`.`subject` AS `subject`,
  `d`.`demand` AS `demand`,
  `d`.`basis` AS `basis`,
  `d`.`plan_grafic_id` AS `plan_grafic_id`,
  `d`.`place_method_id` AS `place_method_id`,
  `ds`.`fp_object_id` AS `fp_object_id`,
  `ds`.`fp_article_id` AS `fp_article_id`,
  `ds`.`fin_source_id` AS `fin_source_id`,
  `fs`.`booker_fin_source` AS `booker_fin_source`,
  `ds`.`period_id` AS `period_id`,
  `d`.`date_start` AS `date_start`,
  `d`.`date_end` AS `date_end`,
  `a`.`kosgu` AS `kosgu`,
  `a`.`kvr` AS `kvr`,
  `ds`.`unit_id` AS `unit_id`,
  `ds`.`quantity` AS `quantity`,`ds`.`amount` AS `amount`,
  ( select sum(`zs`.`sum`) 
    from ((`zayav_sostav` `zs` join `zayav` `z` on((`zs`.`zayav_id` = `z`.`id`))) join `zayav_status` on((`z`.`zayav_status_id` = `zayav_status`.`id`))) 
    where ((`zs`.`dogovor_sostav_id` = `ds`.`id`) and (`zayav_status`.`is_good` = 1))
  ) AS `sum_zayav`,
  ( select max(`plat`.`d`) 
    from (`zayav_sostav` `zs` 
    join `plat` on((`zs`.`zayav_id` = `plat`.`zayav_id`))) 
    where (`zs`.`dogovor_sostav_id` = `ds`.`id`)) AS `max_plat_d`,
  `d`.`period_place_id` AS `period_place_id`,
  `d`.`period_finish_id` AS `period_finish_id`
from ((((`dogovor_sostav` `ds` join `dogovor` `d` on((`ds`.`dogovor_id` = `d`.`id`))) 
join `fp_article` `a` on((`ds`.`fp_article_id` = `a`.`id`))) 
left join `fin_source` `fs` on((`ds`.`fin_source_id` = `fs`.`id`))) 
left join `contragent` `ctgnt` on((`d`.`contragent_id` = `ctgnt`.`id`))) ;

DROP VIEW IF EXISTS `v_dogovor_sostav_rep`;

CREATE VIEW `v_dogovor_sostav_rep` AS 
select 
  `id` AS `id`,
  `dogovor_status_id` AS `dogovor_status_id`,
  `fp_year` AS `fp_year`,
  `zfo_id` AS `zfo_id`,
  `document_type_id` AS `document_type_id`,
  `d` AS `d`,`contragent_id` AS `contragent_id`,
  `inn` AS `inn`,
  `dogovor_num` AS `dogovor_num`,
  `subject` AS `subject`,`demand` AS `demand`,
  `basis` AS `basis`,`plan_grafic_id` AS `plan_grafic_id`,
  `place_method_id` AS `place_method_id`,
  `fp_object_id` AS `fp_object_id`,
  `fp_article_id` AS `fp_article_id`,
  `fin_source_id` AS `fin_source_id`,`booker_fin_source` AS `booker_fin_source`,
  `period_id` AS `period_id`,
  `kosgu` AS `kosgu`,`kvr` AS `kvr`,
  `unit_id` AS `unit_id`,
  `quantity` AS `quantity`,
  `amount` AS `amount`,
  `sum_zayav` AS `sum_zayav`,
  (`amount` - ifnull(`sum_zayav`,0)) AS `sum_ost`,
  `max_plat_d` AS `max_plat_d`,
  `date_start` AS `date_start`,`date_end` AS `date_end`,
  `period_place_id` AS `period_place_id`,
  `period_finish_id` AS `period_finish_id` 
from `v_dogovor_sostav_rep_pre`;
