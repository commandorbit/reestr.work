﻿--
-- Скрипт сгенерирован Devart dbForge Studio 2019 for MySQL, Версия 8.1.22.0
-- Домашняя страница продукта: http://www.devart.com/ru/dbforge/mysql/studio
-- Дата скрипта: 06.02.2019 21:20:36
-- Версия сервера: 5.6.31
-- Версия клиента: 4.1
-- Пожалуйста, сохраните резервную копию вашей базы перед запуском этого скрипта
--


SET NAMES 'utf8';

--
-- Установка базы данных по умолчанию
--
USE reestr2015;

--
-- Изменить представление `virt_dogovor_sostav`
--
CREATE OR REPLACE
DEFINER = 'reestr2015'@'localhost'
VIEW virt_dogovor_sostav
AS
SELECT
  dogovor_sostav.id AS id,
  period.smeta_y,
  dogovor.zfo_id AS zfo_id,
  dogovor_sostav.fp_article_id AS fp_article_id,
  dogovor_sostav.fin_source_id AS fin_source_id,
  dogovor_sostav.dogovor_id AS dogovor_id,
  CONCAT(IFNULL(CONCAT('', dogovor.subject), ''), IFNULL(CONCAT(' - ', dogovor_sostav.description), '')) AS name,
  dogovor.subject AS subject,
  dogovor_sostav.description AS description,
  dogovor_sostav.amount AS amount,
  dogovor_sostav_smeta_sostav.smeta_sostav_id AS smeta_sostav_id,
  smeta_sostav.smeta_id AS smeta_id,
  smeta_sostav.amount AS smeta_sostav_amount
FROM dogovor_sostav
  INNER JOIN dogovor
    ON dogovor_sostav.dogovor_id = dogovor.id
  LEFT OUTER JOIN dogovor_sostav_smeta_sostav
    ON dogovor_sostav.id = dogovor_sostav_smeta_sostav.dogovor_sostav_id
  LEFT OUTER JOIN smeta_sostav
    ON smeta_sostav.id = dogovor_sostav_smeta_sostav.smeta_sostav_id
  LEFT OUTER JOIN period
    ON dogovor_sostav.period_id = period.id

WHERE dogovor.fp_year <> period.smeta_y

ORDER BY dogovor.fp_year DESC, dogovor.zfo_id, dogovor_sostav.fp_article_id, dogovor_sostav.fin_source_id, dogovor_sostav.dogovor_id;