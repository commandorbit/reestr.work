﻿--
-- Скрипт сгенерирован Devart dbForge Studio for MySQL, Версия 8.0.80.0
-- Домашняя страница продукта: http://www.devart.com/ru/dbforge/mysql/studio
-- Дата скрипта: 12.11.2018 11:49:48
-- Версия сервера: 5.6.31
-- Версия клиента: 4.1
-- Пожалуйста, сохраните резервную копию вашей базы перед запуском этого скрипта
--


SET NAMES 'utf8';

--
-- Установка базы данных по умолчанию
--
USE reestr2015;

--
-- Создать столбец `approved` для таблицы `smeta_sostav`
--
ALTER TABLE smeta_sostav 
  ADD COLUMN approved TINYINT(4) DEFAULT NULL;

--
-- Создать столбец `approved` для таблицы `log_smeta_sostav`
--
ALTER TABLE log_smeta_sostav 
  ADD COLUMN approved TINYINT(4) DEFAULT NULL;

--
-- Создать столбец `approved` для таблицы `ver_smeta_sostav`
--
ALTER TABLE ver_smeta_sostav 
  ADD COLUMN approved TINYINT(4) DEFAULT NULL;
