﻿--
-- Скрипт сгенерирован Devart dbForge Studio for MySQL, Версия 8.0.108.0
-- Домашняя страница продукта: http://www.devart.com/ru/dbforge/mysql/studio
-- Дата скрипта: 20.11.2018 19:54:50
-- Версия сервера: 5.6.31
-- Версия клиента: 4.1
-- Пожалуйста, сохраните резервную копию вашей базы перед запуском этого скрипта
--


SET NAMES 'utf8';

--
-- Установка базы данных по умолчанию
--
USE reestr2015;

--
-- Создать столбцы для таблицы `smeta_sostav`
--
ALTER TABLE smeta_sostav ADD COLUMN approved_user_id INT(11) DEFAULT NULL;
ALTER TABLE smeta_sostav ADD COLUMN approved_time DATETIME DEFAULT NULL;
ALTER TABLE smeta_sostav ADD COLUMN approved_comment VARCHAR(80) DEFAULT NULL;

--
-- Создать столбцы для таблицы `log_smeta_sostav`
--
ALTER TABLE log_smeta_sostav ADD COLUMN approved_user_id INT(11) DEFAULT NULL;
ALTER TABLE log_smeta_sostav ADD COLUMN approved_time DATETIME DEFAULT NULL;
ALTER TABLE log_smeta_sostav ADD COLUMN approved_comment VARCHAR(80) DEFAULT NULL;

--
-- Создать столбцы для таблицы `ver_smeta_sostav`
--
ALTER TABLE ver_smeta_sostav ADD COLUMN approved_user_id INT(11) DEFAULT NULL;
ALTER TABLE ver_smeta_sostav ADD COLUMN approved_time DATETIME DEFAULT NULL;
ALTER TABLE ver_smeta_sostav ADD COLUMN approved_comment VARCHAR(80) DEFAULT NULL;
