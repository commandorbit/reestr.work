﻿--
-- Скрипт сгенерирован Devart dbForge Studio for MySQL, Версия 8.0.40.0
-- Домашняя страница продукта: http://www.devart.com/ru/dbforge/mysql/studio
-- Дата скрипта: 15.08.2018 10:19:04
-- Версия сервера: 5.6.31
-- Версия клиента: 4.1
-- Пожалуйста, сохраните резервную копию вашей базы перед запуском этого скрипта
--

-- 
-- Установка кодировки, с использованием которой клиент будет посылать запросы на сервер
--
SET NAMES 'utf8';

--
-- Установка базы данных по умолчанию
--
USE reestr2015;


--
-- Создать столбец `fin_source_id` для таблицы `smeta_sostav`
--
ALTER TABLE smeta_sostav 
  ADD COLUMN fin_source_id INT(11) DEFAULT NULL;

--
-- Изменение порядка колонок в таблице `smeta_sostav`
--
ALTER TABLE smeta_sostav 
 MODIFY fin_source_id INT(11) DEFAULT NULL AFTER zfo_id;

--
-- Создать столбец `fin_source_id` для таблицы `ver_smeta_sostav`
--
ALTER TABLE ver_smeta_sostav 
  ADD COLUMN fin_source_id INT(11) DEFAULT NULL;

--
-- Изменение порядка колонок в таблице `ver_smeta_sostav`
--
ALTER TABLE ver_smeta_sostav 
 MODIFY fin_source_id INT(11) DEFAULT NULL AFTER zfo_id;

--
-- Создать таблицу `smeta_fin_source`
--

-- DROP TABLE smeta_fin_source;
CREATE TABLE smeta_fin_source 
(
  id int(11) NOT NULL AUTO_INCREMENT,
  smeta_id int(11) DEFAULT NULL,
  fin_source_id int(11) DEFAULT NULL,
  amount_limit decimal(20, 2) DEFAULT NULL,
  PRIMARY KEY (id)
)
ENGINE = INNODB,
CHARACTER SET utf8,
COLLATE utf8_general_ci;

--
-- Создать таблицу `ver_smeta_fin_source`
--
-- DROP TABLE ver_smeta_fin_source;
CREATE TABLE ver_smeta_fin_source 
(
  id int(11) NOT NULL AUTO_INCREMENT,
  ver int(11) NOT NULL DEFAULT 1,
  smeta_id int(11) DEFAULT NULL,
  fin_source_id int(11) DEFAULT NULL,
  amount_limit decimal(20, 2) DEFAULT NULL,
  PRIMARY KEY (id, ver)
)
ENGINE = INNODB,
CHARACTER SET utf8,
COLLATE utf8_general_ci;

--
-- Импортируем источники финансирования, указанные в заголовках смет
--
INSERT INTO smeta_fin_source (smeta_id, fin_source_id)
SELECT
  s.id,
  s.fin_source_id
FROM smeta s
  LEFT OUTER JOIN smeta_sostav ss
    ON ss.smeta_id = s.id
WHERE s.fin_source_id IS NOT NULL
GROUP BY s.id,
         s.fin_source_id;

--
-- Импортируем бюджет
--
INSERT INTO smeta_fin_source (smeta_id, fin_source_id)
SELECT
  s.id,
  1 AS fin_source_id
FROM smeta s
  LEFT OUTER JOIN smeta_sostav ss
    ON ss.smeta_id = s.id
WHERE ss.percent_budjet > 0
GROUP BY s.id,
         s.fin_source_id;

--
-- Импортируем внебюджет
--
INSERT INTO smeta_fin_source (smeta_id, fin_source_id)
SELECT
  s.id,
  2 AS fin_source_id
FROM smeta s
  LEFT OUTER JOIN smeta_sostav ss
    ON ss.smeta_id = s.id
WHERE ss.percent_exbudjet > 0
GROUP BY s.id,
         s.fin_source_id;

-- SELECT * FROM smeta_fin_source;
-- SELECT COUNT(*) FROM smeta_fin_source;

-- Заполняем ИФ расхода для смет, где ИФ был указан в заголовке.
UPDATE smeta_sostav ss LEFT JOIN smeta s ON ss.smeta_id = s.id SET ss.fin_source_id = s.fin_source_id;

-- --------------------------------------------------------------------------------------------------------------------------

-- Заполняем ИФ расхода для смет, где указан только бюджет
UPDATE smeta_sostav SET  
  fin_source_id = 1
WHERE percent_budjet > 0 AND percent_exbudjet = 0 AND fin_source_id IS NULL;

-- Заполняем ИФ расхода для смет, где указан только внебюджет
UPDATE smeta_sostav SET  
  fin_source_id = 2
WHERE percent_budjet = 0 AND percent_exbudjet > 0 AND fin_source_id IS NULL;

-- Сохраняем строки, где указаны оба процента
CREATE TEMPORARY TABLE smeta_sostav_tmp AS ( SELECT * FROM smeta_sostav WHERE percent_budjet > 0 AND percent_exbudjet > 0 AND fin_source_id IS NULL ); 
-- SELECT * FROM smeta_sostav_tmp;

-- Строки, где указаны оба процента обновляются на бюджетные
UPDATE smeta_sostav SET  
  fin_source_id = 1,
  amount = amount * percent_budjet / 100,
  amount1 = amount1 * percent_budjet / 100,
  amount2 = amount2 * percent_budjet / 100,
  amount3 = amount3 * percent_budjet / 100,
  amount4 = amount4 * percent_budjet / 100,
  amount5 = amount5 * percent_budjet / 100,
  amount6 = amount6 * percent_budjet / 100,
  amount7 = amount7 * percent_budjet / 100,
  amount8 = amount8 * percent_budjet / 100,
  amount9 = amount9 * percent_budjet / 100,
  amount10 = amount10 * percent_budjet / 100,
  amount11 = amount11 * percent_budjet / 100,
  amount12 = amount12 * percent_budjet / 100,
  amount_next = amount_next * percent_budjet / 100,
  quantity = IF (quantity = 1, 1, quantity * percent_budjet / 100),
  price = IF (quantity = 1, price * percent_budjet / 100, price)
WHERE percent_budjet > 0 AND percent_exbudjet > 0 AND fin_source_id IS NULL;
-- SELECT * FROM smeta_sostav WHERE percent_budjet > 0 AND percent_exbudjet > 0 ;

-- Внебюджета пока нет
DELETE FROM smeta_sostav WHERE percent_budjet > 0 AND percent_exbudjet > 0 AND fin_source_id = 2;

-- Строки, где указаны оба процента дополняются строками внебюджета
INSERT INTO smeta_sostav (
  smeta_id,
  plan_grafic_id,
  not_in_pfhd,
  natural_indicator_id,
  fp_article_id,
  fp_object_id,
  zfo_id,
  fin_source_id,
  description,
  price,
  quantity,
  amount,
  percent_budjet,
  percent_exbudjet,
  amount1,
  amount2,
  amount3,
  amount4,
  amount5,
  amount6,
  amount7,
  amount8,
  amount9,
  amount10,
  amount11,
  amount12,
  amount_next,
  protected
)
SELECT
  ss.smeta_id,
  ss.plan_grafic_id,
  ss.not_in_pfhd,
  ss.natural_indicator_id,
  ss.fp_article_id,
  ss.fp_object_id,
  ss.zfo_id,
  2, -- fin_source_id,
  IF (ss.description IS NULL, CONCAT('Внебюджет строки ', ss.id), CONCAT(ss.description, ' - внебюджет')), -- description
  IF (ss.quantity = 1, sst.price - ss.price, ss.price), -- price 
  IF (ss.quantity = 1, 1, sst.quantity - sst.quantity), -- quantity 
  sst.amount - ss.amount,
  ss.percent_budjet,
  ss.percent_exbudjet,
  sst.amount1 - ss.amount1,
  sst.amount2 - ss.amount2,
  sst.amount3 - ss.amount3,
  sst.amount4 - ss.amount4,
  sst.amount5 - ss.amount5,
  sst.amount6 - ss.amount6,
  sst.amount7 - ss.amount7,
  sst.amount8 - ss.amount8,
  sst.amount9 - ss.amount9,
  sst.amount10 - ss.amount10,
  sst.amount11 - ss.amount11,
  sst.amount12 - ss.amount12,
  sst.amount_next - ss.amount_next,
  ss.protected
FROM smeta_sostav ss 
LEFT JOIN smeta_sostav_tmp sst ON sst.id = ss.id
WHERE ss.percent_budjet > 0 AND ss.percent_exbudjet > 0 AND ss.fin_source_id = 1;
-- SELECT * FROM smeta_sostav WHERE percent_budjet > 0 AND percent_exbudjet > 0;

DROP TABLE IF EXISTS smeta_sostav_tmp;

-- -------------------------------------------------------------------------------------------------------------------
--
-- Скрипт сгенерирован Devart dbForge Studio for MySQL, Версия 8.0.80.0
-- Домашняя страница продукта: http://www.devart.com/ru/dbforge/mysql/studio
-- Дата скрипта: 24.08.2018 18:00:03
-- Версия сервера: 5.6.31
-- Версия клиента: 4.1
-- Пожалуйста, сохраните резервную копию вашей базы перед запуском этого скрипта
--

--
-- Удалить столбец `fin_source_id` из таблицы `smeta`
--
ALTER TABLE smeta 
  DROP COLUMN fin_source_id;
ALTER TABLE ver_smeta 
  DROP COLUMN fin_source_id;

--
-- Удалить столбец `percent_budjet` из таблицы `smeta_sostav`
--
ALTER TABLE smeta_sostav 
  DROP COLUMN percent_budjet;
ALTER TABLE ver_smeta_sostav 
  DROP COLUMN percent_budjet;

--
-- Удалить столбец `percent_exbudjet` из таблицы `smeta_sostav`
--
ALTER TABLE smeta_sostav 
  DROP COLUMN percent_exbudjet;
ALTER TABLE ver_smeta_sostav 
  DROP COLUMN percent_exbudjet;