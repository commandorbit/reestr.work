﻿--
-- Скрипт сгенерирован Devart dbForge Studio for MySQL, Версия 8.0.80.0
-- Домашняя страница продукта: http://www.devart.com/ru/dbforge/mysql/studio
-- Дата скрипта: 07.09.2018 19:09:49
-- Версия сервера: 5.7.22-log
-- Версия клиента: 4.1
-- Пожалуйста, сохраните резервную копию вашей базы перед запуском этого скрипта
--


SET NAMES 'utf8';

--
-- Установка базы данных по умолчанию
--
USE reestr2015;

--
-- Удалить столбец `percent_budjet` из таблицы `log_smeta_sostav`
--
ALTER TABLE log_smeta_sostav 
  DROP COLUMN percent_budjet;

--
-- Удалить столбец `percent_exbudjet` из таблицы `log_smeta_sostav`
--
ALTER TABLE log_smeta_sostav 
  DROP COLUMN percent_exbudjet;

--
-- Создать столбец `fin_source_id` для таблицы `log_smeta_sostav`
--
ALTER TABLE log_smeta_sostav 
  ADD COLUMN fin_source_id INT(11) DEFAULT NULL;

--
-- Создать столбец `protected` для таблицы `log_smeta_sostav`
--
ALTER TABLE log_smeta_sostav 
  ADD COLUMN protected TINYINT(4) DEFAULT NULL;

--
-- Изменение порядка колонок в таблице `log_smeta_sostav`
--
ALTER TABLE log_smeta_sostav 
 MODIFY fin_source_id INT(11) DEFAULT NULL AFTER zfo_id;
ALTER TABLE log_smeta_sostav 
 MODIFY protected TINYINT(4) DEFAULT NULL AFTER amount_next;

-- ---------------------------------------------------------------------------------------------------------------

--
-- Удалить столбец `amount_limit` из таблицы `smeta`
--
ALTER TABLE smeta 
  DROP COLUMN amount_limit;

--
-- Удалить столбец `amount_limit_budjet` из таблицы `smeta`
--
ALTER TABLE smeta 
  DROP COLUMN amount_limit_budjet;

--
-- Удалить столбец `amount_limit_exbudjet` из таблицы `smeta`
--
ALTER TABLE smeta 
  DROP COLUMN amount_limit_exbudjet;

-- ---------------------------------------------------------------------------------------------------------------

--
-- Удалить столбец `fin_source_id` из таблицы `log_smeta`
--
ALTER TABLE log_smeta 
  DROP COLUMN fin_source_id;

--
-- Удалить столбец `user_created` из таблицы `log_smeta`
--
ALTER TABLE log_smeta 
  DROP COLUMN user_created;

--
-- Удалить столбец `user_modified` из таблицы `log_smeta`
--
ALTER TABLE log_smeta 
  DROP COLUMN user_modified;

--
-- Удалить столбец `ts_created` из таблицы `log_smeta`
--
ALTER TABLE log_smeta 
  DROP COLUMN ts_created;

--
-- Удалить столбец `ts_modified` из таблицы `log_smeta`
--
ALTER TABLE log_smeta 
  DROP COLUMN ts_modified;

-- ---------------------------------------------------------------------------------------------------------------

--
-- Создать таблицу `log_smeta_fin_source`
--

DROP TABLE IF EXISTS `log_smeta_fin_source`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE log_smeta_fin_source (
  id int(11) DEFAULT NULL,
  smeta_id int(11) DEFAULT NULL,
  fin_source_id int(11) DEFAULT NULL,
  amount_limit decimal(20, 2) DEFAULT NULL,
  user_id int(11) DEFAULT NULL,
  operation_type int(11) DEFAULT NULL,
  ts timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ---------------------------------------------------------------------------------------------------------------

--
-- Создать столбец `protected` для таблицы `ver_smeta_sostav`
--
ALTER TABLE ver_smeta_sostav 
  ADD COLUMN protected TINYINT(4) DEFAULT NULL;

-- ---------------------------------------------------------------------------------------------------------------

--
-- Удалить столбец `amount_limit` из таблицы `ver_smeta`
--
ALTER TABLE ver_smeta 
  DROP COLUMN amount_limit;

--
-- Удалить столбец `amount_limit_budjet` из таблицы `ver_smeta`
--
ALTER TABLE ver_smeta 
  DROP COLUMN amount_limit_budjet;

--
-- Удалить столбец `amount_limit_exbudjet` из таблицы `ver_smeta`
--
ALTER TABLE ver_smeta 
  DROP COLUMN amount_limit_exbudjet;

-- ---------------------------------------------------------------------------------------------------------------


