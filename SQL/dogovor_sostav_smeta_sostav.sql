﻿--
-- Скрипт сгенерирован Devart dbForge Studio for MySQL, Версия 8.0.80.0
-- Домашняя страница продукта: http://www.devart.com/ru/dbforge/mysql/studio
-- Дата скрипта: 10.09.2018 16:19:36
-- Версия сервера: 5.6.31
-- Версия клиента: 4.1
-- Пожалуйста, сохраните резервную копию вашей базы перед запуском этого скрипта
--


SET NAMES 'utf8';

--
-- Установка базы данных по умолчанию
--
USE reestr2015;

--
-- Создать таблицу `dogovor_sostav_smeta_sostav`
--
DROP TABLE IF EXISTS dogovor_sostav_smeta_sostav;
CREATE TABLE dogovor_sostav_smeta_sostav (
  id INT(11) NOT NULL AUTO_INCREMENT,
  dogovor_sostav_id INT(11) NOT NULL,
  smeta_sostav_id INT(11) NOT NULL,
  PRIMARY KEY (id)
)
ENGINE = INNODB;