﻿--
-- Скрипт сгенерирован Devart dbForge Studio for MySQL, Версия 8.0.80.0
-- Домашняя страница продукта: http://www.devart.com/ru/dbforge/mysql/studio
-- Дата скрипта: 17.08.2018 16:57:15
-- Версия сервера: 5.6.31
-- Версия клиента: 4.1
-- Пожалуйста, сохраните резервную копию вашей базы перед запуском этого скрипта
--


SET NAMES 'utf8';

--
-- Установка базы данных по умолчанию
--
USE reestr2015;

--
-- Создать столбец `fin_source_id` для таблицы `smeta_sostav`
--
ALTER TABLE smeta_sostav 
  ADD COLUMN fin_source_id INT(11) DEFAULT NULL;

--
-- Изменение порядка колонок в таблице `smeta_sostav`
--
ALTER TABLE smeta_sostav 
 MODIFY fin_source_id INT(11) DEFAULT NULL AFTER zfo_id;

--
-- Создать столбец `fin_source_id` для таблицы `ver_smeta_sostav`
--
ALTER TABLE ver_smeta_sostav 
  ADD COLUMN fin_source_id INT(11) DEFAULT NULL;

--
-- Изменение порядка колонок в таблице `ver_smeta_sostav`
--
ALTER TABLE ver_smeta_sostav 
 MODIFY fin_source_id INT(11) DEFAULT NULL AFTER zfo_id;