﻿--
-- Скрипт сгенерирован Devart dbForge Studio for MySQL, Версия 8.0.80.0
-- Домашняя страница продукта: http://www.devart.com/ru/dbforge/mysql/studio
-- Дата скрипта: 25.09.2018 16:20:27
-- Версия сервера: 5.6.31
-- Версия клиента: 4.1
-- Пожалуйста, сохраните резервную копию вашей базы перед запуском этого скрипта
--


SET NAMES 'utf8';

--
-- Установка базы данных по умолчанию
--
USE reestr2015;

--
-- Изменить столбец `description` для таблицы `smeta_income`
--
ALTER TABLE smeta_income 
  CHANGE COLUMN description description varchar(512) DEFAULT NULL;
ALTER TABLE log_smeta_income 
  CHANGE COLUMN description description varchar(512) DEFAULT NULL;
ALTER TABLE ver_smeta_income 
  CHANGE COLUMN description description varchar(512) DEFAULT NULL;