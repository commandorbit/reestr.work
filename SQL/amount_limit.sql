﻿--
-- Скрипт сгенерирован Devart dbForge Studio for MySQL, Версия 8.0.40.0
-- Домашняя страница продукта: http://www.devart.com/ru/dbforge/mysql/studio
-- Дата скрипта: 07.08.2018 12:35:37
-- Версия сервера: 5.6.31
-- Версия клиента: 4.1
-- Пожалуйста, сохраните резервную копию вашей базы перед запуском этого скрипта
--


SET NAMES 'utf8';

--
-- Установка базы данных по умолчанию
--
USE reestr2015;

--
-- Создать столбец `amount_limit` для таблицы `smeta`
--
ALTER TABLE smeta 
  ADD COLUMN amount_limit DECIMAL(20, 2) DEFAULT NULL;

--
-- Создать столбец `amount_limit` для таблицы `ver_smeta`
--
ALTER TABLE ver_smeta
  ADD COLUMN amount_limit DECIMAL(20, 2) DEFAULT NULL;

--
-- Лимиты по бюджету - внебюджету
--
ALTER TABLE smeta     ADD COLUMN amount_limit_budjet DECIMAL(20, 2) DEFAULT NULL;
ALTER TABLE ver_smeta ADD COLUMN amount_limit_budjet DECIMAL(20, 2) DEFAULT NULL;
ALTER TABLE smeta     ADD COLUMN amount_limit_exbudjet DECIMAL(20, 2) DEFAULT NULL;
ALTER TABLE ver_smeta ADD COLUMN amount_limit_exbudjet DECIMAL(20, 2) DEFAULT NULL;
