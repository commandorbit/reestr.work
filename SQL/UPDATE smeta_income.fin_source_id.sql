﻿CREATE TEMPORARY TABLE T AS
SELECT
  smeta_income.id,
  smeta_fin_source.fin_source_id  
FROM smeta_income 
  INNER JOIN smeta
    ON smeta_income.smeta_id = smeta.id
  INNER JOIN smeta_fin_source
    ON smeta.id = smeta_fin_source.smeta_id
GROUP BY smeta_income.id HAVING COUNT(smeta_fin_source.fin_source_id) = 1;
 
UPDATE smeta_income JOIN T ON T.id = smeta_income.id
  SET smeta_income.fin_source_id = T.fin_source_id;  

DROP TABLE IF EXISTS T;