﻿-- 
-- Установка кодировки, с использованием которой клиент будет посылать запросы на сервер
--
SET NAMES 'utf8';

--
-- Установка базы данных по умолчанию
--
USE reestr2015;

--
-- Создать таблицу `smeta_fin_source`
--

 DROP TABLE smeta_fin_source;
CREATE TABLE smeta_fin_source 
(
  id int(11) NOT NULL AUTO_INCREMENT,
  smeta_id int(11) DEFAULT NULL,
  fin_source_id int(11) DEFAULT NULL,
  amount decimal(20, 2) DEFAULT NULL,
  amount_limit decimal(20, 2) DEFAULT NULL,
  PRIMARY KEY (id)
)
ENGINE = INNODB,
CHARACTER SET utf8,
COLLATE utf8_general_ci;

--
-- Создать таблицу `ver_smeta_fin_source`
--
 DROP TABLE ver_smeta_fin_source;
CREATE TABLE ver_smeta_fin_source 
(
  id int(11) NOT NULL AUTO_INCREMENT,
  ver int(11) NOT NULL DEFAULT 1,
  smeta_id int(11) DEFAULT NULL,
  fin_source_id int(11) DEFAULT NULL,
  amount decimal(20, 2) DEFAULT NULL,
  amount_limit decimal(20, 2) DEFAULT NULL,
  PRIMARY KEY (id, ver)
)
ENGINE = INNODB,
CHARACTER SET utf8,
COLLATE utf8_general_ci;

INSERT INTO smeta_fin_source (smeta_id, fin_source_id, amount_limit, amount)
SELECT
  s.id,
  s.fin_source_id,
  s.amount_limit,
  SUM(ss.amount) AS amount
FROM smeta s
  LEFT OUTER JOIN smeta_sostav ss
    ON ss.smeta_id = s.id
WHERE s.fin_source_id IS NOT NULL
GROUP BY s.id,
         s.fin_source_id,
         s.amount_limit;

INSERT INTO smeta_fin_source (smeta_id, fin_source_id, amount_limit, amount)
SELECT
  s.id,
  1 AS fin_source_id,
  s.amount_limit,
  SUM(ss.amount * ss.percent_budjet / 100) AS amount
FROM smeta s
  LEFT OUTER JOIN smeta_sostav ss
    ON ss.smeta_id = s.id
WHERE ss.percent_budjet > 0
GROUP BY s.id,
         s.fin_source_id,
         s.amount_limit;

INSERT INTO smeta_fin_source (smeta_id, fin_source_id, amount_limit, amount)
SELECT
  s.id,
  2 AS fin_source_id,
  s.amount_limit,
  SUM(ss.amount * ss.percent_exbudjet / 100) AS amount  
FROM smeta s
  LEFT OUTER JOIN smeta_sostav ss
    ON ss.smeta_id = s.id
WHERE ss.percent_exbudjet > 0
GROUP BY s.id,
         s.fin_source_id,
         s.amount_limit;

-- SELECT * FROM smeta_fin_source;
-- SELECT COUNT(*) FROM smeta_fin_source;

-- Для смет, где ИФ был указан в заголовке.
UPDATE smeta_sostav ss LEFT JOIN smeta s ON ss.smeta_id = s.id SET ss.fin_source_id = s.fin_source_id;
