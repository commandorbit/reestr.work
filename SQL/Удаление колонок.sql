﻿--
-- Скрипт сгенерирован Devart dbForge Studio for MySQL, Версия 8.0.80.0
-- Домашняя страница продукта: http://www.devart.com/ru/dbforge/mysql/studio
-- Дата скрипта: 24.08.2018 18:00:03
-- Версия сервера: 5.6.31
-- Версия клиента: 4.1
-- Пожалуйста, сохраните резервную копию вашей базы перед запуском этого скрипта
--


SET NAMES 'utf8';

--
-- Установка базы данных по умолчанию
--
USE reestr2015;

--
-- Удалить столбец `fin_source_id` из таблицы `smeta`
--
ALTER TABLE smeta 
  DROP COLUMN fin_source_id;

--
-- Удалить столбец `percent_budjet` из таблицы `smeta_sostav`
--
ALTER TABLE smeta_sostav 
  DROP COLUMN percent_budjet;

--
-- Удалить столбец `percent_exbudjet` из таблицы `smeta_sostav`
--
ALTER TABLE smeta_sostav 
  DROP COLUMN percent_exbudjet;