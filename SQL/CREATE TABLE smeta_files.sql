﻿-- 
-- Установка кодировки, с использованием которой клиент будет посылать запросы на сервер
--
SET NAMES 'utf8';

--
-- Установка базы данных по умолчанию
--
USE reestr2015;

--
-- Создать таблицу `smeta_files`
--
CREATE TABLE smeta_files (
  id int(10) NOT NULL AUTO_INCREMENT,
  smeta_id int(10) NOT NULL,
  author_id int(5) NOT NULL,
  filename varchar(255) NOT NULL,
  description varchar(255) DEFAULT NULL,
  path varchar(100) NOT NULL,
  timestamp timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (id)
)
ENGINE = INNODB; 

--
-- Создать индекс `smeta_id` для объекта типа таблица `smeta_files`
--
ALTER TABLE smeta_files
ADD INDEX smeta_id (smeta_id);

--
-- Создать таблицу `log_smeta_files`
--
CREATE TABLE log_smeta_files (
  id int(10) NOT NULL,
  smeta_id int(10) NOT NULL,
  author_id int(5) NOT NULL,
  filename varchar(255) NOT NULL,
  description varchar(255) DEFAULT NULL,
  path varchar(100) NOT NULL,
  timestamp timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  user_id int(11) DEFAULT NULL,
  operation_type int(11) DEFAULT NULL,
  ts timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
)
ENGINE = INNODB; 

--
-- Создать таблицу `ver_smeta_files`
--
CREATE TABLE ver_smeta_files (
  id int(10) NOT NULL,
  ver int(11) NOT NULL DEFAULT 1,
  smeta_id int(10) NOT NULL,
  author_id int(5) NOT NULL,
  filename varchar(255) NOT NULL,
  description varchar(255) DEFAULT NULL,
  path varchar(100) NOT NULL,
  timestamp timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
)
ENGINE = INNODB; 
