﻿-- UPDATE smeta_sostav SET 
UPDATE smeta_sostav SET quantity = 1 WHERE quantity = 0;
UPDATE smeta_sostav SET price = amount / quantity WHERE quantity > 0 AND price = 0;
UPDATE smeta_sostav SET price = amount / quantity WHERE ABS(price * quantity - amount) > 1; 
-- SELECT ss.price * ss.quantity - ss.amount, ss.* FROM smeta_sostav ss WHERE ABS(ss.price * ss.quantity - ss.amount) > 1

UPDATE smeta_sostav SET fin_source_id = 1 WHERE percent_budjet > 0 AND percent_exbudjet = 0;
UPDATE smeta_sostav SET fin_source_id = 2 WHERE percent_budjet = 0 AND percent_exbudjet > 0;
-- SELECT * FROM smeta_sostav WHERE (percent_budjet > 0 AND percent_exbudjet = 0) OR (percent_budjet = 0 AND percent_exbudjet > 0);

UPDATE smeta_sostav SET  
  fin_source_id = 1,
  amount = amount * percent_budjet / 100,
  amount1 = amount1 * percent_budjet / 100,
  amount2 = amount2 * percent_budjet / 100,
  amount3 = amount3 * percent_budjet / 100,
  amount4 = amount4 * percent_budjet / 100,
  amount5 = amount5 * percent_budjet / 100,
  amount6 = amount6 * percent_budjet / 100,
  amount7 = amount7 * percent_budjet / 100,
  amount8 = amount8 * percent_budjet / 100,
  amount9 = amount9 * percent_budjet / 100,
  amount10 = amount10 * percent_budjet / 100,
  amount11 = amount11 * percent_budjet / 100,
  amount12 = amount12 * percent_budjet / 100,
  amount_next = amount_next * percent_budjet / 100,
  quantity = IF (quantity = 1, 1, quantity * percent_budjet / 100),
  price = IF (quantity = 1, price * percent_budjet / 100, price)
WHERE percent_budjet > 0 AND percent_exbudjet > 0 AND fin_source_id IS NULL;
-- SELECT * FROM smeta_sostav WHERE percent_budjet > 0 AND percent_exbudjet > 0 ;

DELETE FROM smeta_sostav WHERE percent_budjet > 0 AND percent_exbudjet > 0 AND fin_source_id = 2;

INSERT INTO smeta_sostav (
  smeta_id,
  plan_grafic_id,
  not_in_pfhd,
  natural_indicator_id,
  fp_article_id,
  fp_object_id,
  zfo_id,
  fin_source_id,
  description,
  price,
  quantity,
  amount,
  percent_budjet,
  percent_exbudjet,
  amount1,
  amount2,
  amount3,
  amount4,
  amount5,
  amount6,
  amount7,
  amount8,
  amount9,
  amount10,
  amount11,
  amount12,
  amount_next,
  protected
)
SELECT
  smeta_id,
  plan_grafic_id,
  not_in_pfhd,
  natural_indicator_id,
  fp_article_id,
  fp_object_id,
  zfo_id,
  2, -- fin_source_id,
  IF (description IS NULL, CONCAT('Внебюджет строки ', id), CONCAT(description, ' - внебюджет')), -- description
  IF (quantity = 1, price * percent_exbudjet / percent_budjet, price), -- price 
  IF (quantity = 1, 1, quantity * percent_exbudjet / percent_budjet), -- quantity 
  amount * percent_exbudjet / percent_budjet,
  percent_budjet,
  percent_exbudjet,
  amount1 * percent_exbudjet / percent_budjet,
  amount2 * percent_exbudjet / percent_budjet,
  amount3 * percent_exbudjet / percent_budjet,
  amount4 * percent_exbudjet / percent_budjet,
  amount5 * percent_exbudjet / percent_budjet,
  amount6 * percent_exbudjet / percent_budjet,
  amount7 * percent_exbudjet / percent_budjet,
  amount8 * percent_exbudjet / percent_budjet,
  amount9 * percent_exbudjet / percent_budjet,
  amount10 * percent_exbudjet / percent_budjet,
  amount11 * percent_exbudjet / percent_budjet,
  amount12 * percent_exbudjet / percent_budjet,
  amount_next * percent_exbudjet / percent_budjet,
  protected
FROM smeta_sostav WHERE percent_budjet > 0 AND percent_exbudjet > 0 AND fin_source_id = 1;

-- SELECT * FROM smeta_sostav WHERE percent_budjet > 0 AND percent_exbudjet > 0;