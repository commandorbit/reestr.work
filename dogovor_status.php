<?php

include $_SERVER['DOCUMENT_ROOT'] . '/init.php';
include $_SERVER['DOCUMENT_ROOT'] . '/modules/save.php';
$dogovor_id = $_GET['id'];
$title = 'Статус договора ID: ' . $dogovor_id;
include $_SERVER['DOCUMENT_ROOT'] . '/template/header.php';
echo '<h1>Договор ID: '. $dogovor_id .'</h1>';
$status_options = sql_rows("SELECT * FROM dogovor_status");
$dogovor_status_now = sql_get_value('name', 'dogovor_status', "id IN (SELECT dogovor_status_id FROM dogovor WHERE id = '$dogovor_id')");
if(isset($_POST['dogovor_status'])) {
	$dogovor_status = $_POST['dogovor_status'];
}

?>

<br /><h3>Статус договора: <?php echo $dogovor_status_now; ?></h3>

<form method="POST">
	<?php if(user_has_zfo()): ?>
		<label>Включен в ФП:</label>
		<input type="checkbox" name="dogovor_status" value="2" />
	<?php else: ?>
		<label>Выберите статус договора:</label>
		<select name="dogovor_status">
			<?php foreach($status_options as $option): ?>
				<option value="<?php echo $option['id'] ?>"><?php echo $option['name'] ?></option>
			<?php endforeach; ?>
		</select> 
	<?php endif; ?>

	<input type="submit" value="Сохранить" />
</form>