<?php
error_reporting(E_ALL);
include 'init.php';
//session_start();

$title = 'Вход в систему';
$url = urldecode(request_val('url', 'index.php'));
$is_login_page = true;
$bad_msg = '';

if (isset($_GET['exit'])) 
{
	$UID = isset($_SESSION['user_id']) ? $_SESSION['user_id'] : 0;
	session_unset();
	//setcookie('last_user_id', $UID, time() + 3600 /* 1час */ * 24 * 10 /*10 дней*/);
}

if (isset($_COOKIE['last_user_id']))
	$last_user_id = $_COOKIE['last_user_id'];
else 
	$last_user_id = 0;

if ($last_user_id)
{
	$r = sql_rows("SELECT * FROM user WHERE id = '$last_user_id'");
	$_SESSION["palette"] = $r[0]['palette'];
}
if (!isset($_SESSION["palette"])) $_SESSION["palette"] = 1;
if (!$_SESSION["palette"]) $_SESSION["palette"] = 1;

if (isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) === 'xmlhttprequest') 
{
	echo json_encode(array('status'=>'notok', 'result'=>'Время сессии истекло, либо вы не авторизированны в системе'));
	die();
}

if (isset($_POST['enter'])) 
{
	$login = sql_escape($_POST['login']);
	$pass = sql_escape($_POST['pass']);

	$pass_md5 = md5($pass);

	if (empty($login) or empty($pass)) 
	{
		$bad_msg = 'Не введено имя пользователя и (или) пароль!';
	} 
	else
	{
		if (login($login,trim($pass, ' ') )) 
		{
			header("Location: $url");
			exit;
		}
		else 
		{
			$bad_msg = 'Неверное имя пользователя и (или) пароль!';
		}
	}
} 
else 
{
	$login = '';
	$pass = '';
}
?>
<!DOCTYPE HTML>
<html>
<head>
	<meta charset="UTF-8" />
	<title><?=$title?></title>
	<link rel="icon" type="image/png" href="/img/ico/favicon16.ico" />		
	<link rel="stylesheet" type="text/css" href="/css/common.css">
	<meta http-equiv="Pragma" content="no-cache">
</head>
<link rel="stylesheet" type="text/css" href="/css/background<?=$_SESSION["palette"]?>.css">
<link rel="stylesheet" type="text/css" href="/css/theme<?=$_SESSION["palette"]?>.css">
<style>
.user_pic
{
	width: 108px;
	height: 108px;
	overflow: hidden;
}
.user_pic2
{
	width: 100px;
	height: 100px;
	margin: 4px;
	overflow: hidden;
	border-radius: 50px;
}
</style>
<body>
<div class="main"align="center">
<form method="post" action="">
	<div class="login-card" data-moveable="true">
		<div align="center">
			<img src="img/png/main-logo2.png"/>
		</div>
		<h1>СУП</h1>
		Система управленческого планирования 
		<br>
		<br>
		<!-- -->
		<center>
		<div class="user_pic">
			<div class="user_pic2">
				<img src="<? echo "/userpic.php?id=$last_user_id&width=100"; ?>">
			</div>
		</div>
		</center>
		<!-- -->
		<h2>Вход в систему</h2><br>
		<form>
			<input type="text" name="login" placeholder="Имя пользователя" >
			<input type="password" name="pass" placeholder="Пароль">
			<input type="submit" name="enter" class="login login-submit" value="Войти">
		</form>
		<div align="center"><a style="color:white" href="/lost_pass.php" style="color: #444; opacity: 1">Забыли пароль?</a></div>
		<div class="logotype-img">
			<img style="width:250px" src="img/png/logo-site2.png"/>
		</div>
	</div>
</form>

<h3 class="bad_msg"><?=$bad_msg ?></h3>

</div>

</body>
</html>
<script src="/js/avd.js"></script>
<script>
	avd.init();
</script>
