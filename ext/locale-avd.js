Ext.onReady(function()
{
	// Дополнение к локализации
	Ext.define("My.override.Date", 
	{
		override: "Ext.form.field.Date",
		format: "d.m.Y",
		startDay: 1
	});

	Ext.define("My.override.DatePicker", 
	{
		override: "Ext.picker.Date",
		getDayInitial: function(value) {
			var dayNames = {
				'Понедельник':'Пн',
				'Вторник':'Вт',
				'Среда':'Ср',
				'Четверг':'Чт',
				'Пятница':'Пт',
				'Суббота':'Сб',
				'Воскресенье':'Вс',
			};
			return dayNames[value];
		}
	});
	
	/*
	var button = Ext.create('Ext.Button', {
    text: 'Click me',
    handler: function() {
        alert('You clicked the button!');
    }
	});
	*/
	
	window.alert = function(msg)
	{
		Ext.MessageBox.alert('СУП', msg);
	}	

	window.showToast = function(msg)
	{
        Ext.toast({
            html: msg,
            closable: false,
            align: 't',
			autoCloseDelay: 5000,
            slideInDuration: 300,
			//minWidth: 400,
        });
	}	
	
	//alert('Библиотека подключена!');
	
});
