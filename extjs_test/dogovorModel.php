<?php

require $_SERVER['DOCUMENT_ROOT'] . '/init.php';

if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) && !empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
	$dogovor_rows = sql_rows("SELECT * FROM v_dogovor ORDER BY id DESC");
	echo json_encode($dogovor_rows);
}