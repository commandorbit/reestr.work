Ext.Loader.setPath('Ext.ux', './js');
Ext.onReady(function () {	
	var dogovor_store = Ext.create('Ext.data.Store', {
		fields: ['id', 'subject', 'basis', 'amount'],
		proxy: {
			method: 'GET',
			type: 'ajax',
			url : '/extjs_test/dogovorModel.php'
		},
		pageSize: 4,
	});

    // create the Grid //
    // Ext.ux.LiveSearchGridPanel - Ext.grid.Panel
    //var grid = new Ext.create('Ext.grid.Panel', {
    var grid = Ext.create('Ext.ux.LiveSearchGridPanel', {
        autoScroll: false,
        collapsible: false,
        region: 'center',
        split: false,
        cls: 'grid',
        title: 'Договоры',
        store: dogovor_store,
        loadMask: true,
        //multiSelect: true,
        columns: [
		{
            dataIndex: 'id',
			width: '50',
            text: 'ID'
        },
		{
            dataIndex: 'subject',
			width: '300',
            text: 'Предмет договора'
        },
		{
            dataIndex: 'basis',
			width: '300',
            text: 'Мин. требования'
        },
		{
            dataIndex: 'amount',
			width: '100',
            text: 'Сумма'
        },
        ],
        dockedItems: [{
            dock: 'top',
            xtype: 'toolbar',
            items: [{
                text: '<b>Сформировать</b>',
                iconCls: 'proc',
                handler: function () {
                    //dogovor_store.load();
//                    store.clearFilter(true);
//??                    store.setFilters();
//                    store.remoteFilter = true;
                    //console.log(grid.columns);
                    dogovor_store.load({
						callback : function(r, options, success) {
							console.log(r);
							console.log(r.data);
							console.log(options);
							console.log(success);
						}
					});
                }
            }, '->', {
                region: 'center',
                iconCls: 'xls',
                text: '<b>Открыть в Excel</b>',
                handler: function (b) {
                    b.up(grid).downloadExcelXml();
                }
            }]
        }, {
            xtype: 'toolbar',
            dock: 'bottom',
            items: []
        }]
    });
    Ext.create('Ext.container.Viewport', {
        layout: 'border',
        items: [grid]
    });

    grid.on('edit', function () {
        store.sync();
    });
});