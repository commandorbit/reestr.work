Ext.Loader.setPath('Ext.ux', './js');
Ext.onReady(function () {
	var mainLayout = Ext.create('Ext.panel.Panel', {
			width: '100%',
			height: window.innerHeight,
			layout:'border',
			defaults: {
				collapsible: true,
				split: true,
				bodyStyle: 'padding:5px'
			},
			items: [{
				title: 'Навигация',
				region:'west',
				margins: '5 0 0 0',
				cmargins: '5 5 0 0',
				width: 175,
				minSize: 100,
				maxSize: 250,
				html: '<div id="menu"></div>',
				bodyStyle: 'padding:0px'
			},{
				title: 'Контент',
				collapsible: false,
				region:'center',
				margins: '5 0 0 0',
				html: '<div id="content"></div>',
				bodyStyle: 'padding:0px'
			},{
				title: 'Служебная информация',
				region: 'south',
				height: 150,
				minSize: 75,
				maxSize: 250,
				cmargins: '5 0 0 0'
			}],
			renderTo: Ext.getBody()
		});
	
	var treeMenu =  Ext.create('Ext.tree.Panel', {
		renderTo: 'menu',
		width: '100%',
		root: {
			text: 'Договор',
			expanded: true,
			children: [
				{
					text: 'Важные',
					leaf: true
				},
				{
					text: 'Заключенные',
					leaf: true
				},
				{
					text: 'Отклоненные',
					expanded: true,
					children: [
						{
							text: 'Архив',
							leaf: true
						},
						{
							text: 'Пересмотр',
							leaf: true
						}
					]
				}
			]
		}
	});
	
	var dogovor_store = Ext.create('Ext.data.Store', {
		fields: ['id', 'subject', 'basis', 'amount'],
		proxy: {
			method: 'GET',
			type: 'ajax',
			url : '/extjs_test/dogovorModel.php'
		},
		pageSize: 4,
	});

	var columns = [
		{
            dataIndex: 'id',
			width: '50',
            text: 'ID',
			fieldLabel: 'ID',
			name: 'id'
        },
		{
            dataIndex: 'subject',
			width: '300',
            text: 'Предмет договора',
			fieldLabel: 'Предмет договора',
			name: 'subject'
        },
		{
            dataIndex: 'basis',
			width: '300',
            text: 'Мин. требования',
			fieldLabel: 'Мин. требования',
			name: 'basis'
        },
		{
            dataIndex: 'amount',
			width: '100',
            text: 'Сумма',
			fieldLabel: 'Сумма',
			name: 'amount'
        },
		{
			header: 'Drag',
			dataIndex: 'movecol',
			flex: true,
			tdCls: 'myDraggable'
		}
    ];
	
    // create the Grid //
    // Ext.ux.LiveSearchGridPanel - Ext.grid.Panel
    //var grid = new Ext.create('Ext.grid.Panel', {
    var grid = Ext.create('Ext.grid.Panel', {
		renderTo	: 'content',
        autoScroll	: true,
        collapsible	: false,
        region		: 'center',
        split		: false,
        cls			: 'grid',
        title		: 'Договоры',
        store		: dogovor_store,
        loadMask	: true,
		width		: '100%',
		height		: 700,
		listeners: {
            itemdblclick: function(record, rowIndex){
				//console.log(record, rowIndex);
				var form = Ext.create('Ext.form.Panel', {
					region      : 'center',
					width		: '100%',
					bodyPadding : 10,
					defaultType : 'textfield',
					items		: columns,
					buttons: [{
						text: 'Сохранить',
						type: 'submit',
					}]
				});
				var win = Ext.create("Ext.Window",{
					renderTo	: Ext.getBody(),
					title		: 'Редактировать строку',
					width		: 400, 
					id			: 'drag',
					dockedItems: [{
						dock	: 'top',
						xtype	: 'toolbar',
						items	: [form]
					}]
				});
				win.show();  
			}
        },
        //multiSelect: true,
        columns: columns,
		viewConfig: {
                plugins: {
                    ptype: 'gridviewdragdrop',
                    dragText: 'Переместить',
                    dragZone: {
                        onBeforeDrag: function(data, e) {
                            console.log(data, e);
                            draggedCell = Ext.get(e.target.parentNode);
                            if (draggedCell.hasCls('myDraggable')) {
                                console.log('yes i can be dragged');
                            } else {
                                console.log('no I cannot be dragged');
                                return false;
                            }
                        }
                    }
                }
            },
        dockedItems: [{
            dock: 'top',
            xtype: 'toolbar',
            items: [{
                text: '<b>Сформировать</b>',
                iconCls: 'proc',
                handler: function () {
                    //dogovor_store.load();
//                    store.clearFilter(true);
//??                    store.setFilters();
//                    store.remoteFilter = true;
                    //console.log(grid.columns);
                    dogovor_store.load({
						callback : function(r, options, success) {
							//console.log(r);
							//console.log(r.data);
							//console.log(options);
							//console.log(success);
						}
					});
                }
            }, '->', {
                region: 'center',
                iconCls: 'xls',
                text: '<b>Открыть в Excel</b>',
                handler: function (b) {
                    b.up(grid).downloadExcelXml();
                }
            }]
        }, {
            xtype: 'toolbar',
            dock: 'bottom',
            items: []
        }]
    });

    grid.on('edit', function () {
        store.sync();
    });
});
