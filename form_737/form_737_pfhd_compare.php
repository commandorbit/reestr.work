<?php
$title = 'Форма 737';
define('KOSGU', 1);
define('KVR', 2);
require $_SERVER['DOCUMENT_ROOT'] . '/init.php';
require $_SERVER['DOCUMENT_ROOT'] . '/template/header.php';

//echo '<pre>'; var_dump($_SERVER); echo '</pre>';

$pfhd_versions = sql_rows("SELECT id, name, plan_period_id as y FROM stat_pfhd_ver ORDER BY tstamp DESC");

if (isset($_GET['type']) && $_GET['type'] == KVR) 
	$type = 'kvr';
else
	$type = 'kosgu';

$types = sql_rows("select distinct $type from fp_article order by $type");

//$fss = sql_rows("SELECT distinct stat_pfhd.booker_fin_source FROM stat_pfhd UNION SELECT NULL ORDER BY booker_fin_source");
$fss = sql_rows("
SELECT distinct stat_pfhd.booker_fin_source FROM stat_pfhd 
UNION SELECT booker_fin_source FROM plat
UNION SELECT NULL 
ORDER BY booker_fin_source");

$dfilter = isset($_GET['dfilter']) ? $_GET['dfilter'] : date("Y-m-d", time());
$dfilter = sql_escape($dfilter);

function getSumTable($pfhd_ver_id, $type = 'kosgu', $year = 2016) {
    $next_year = $year + 1;
	
	global $dfilter;
	
	$w = "";
	if ($filter = request_numeric_val('filter','')) $w .= " AND $type = $filter";
	if ($fsfilter = request_numeric_val('fsfilter','')) $w .= " AND booker_fin_source = $fsfilter";

	$sql = " SELECT $type, booker_fin_source, 
		sum(plan) as plan, sum(fact) as fact,
		sum(diff) as diff, 100 * sum(fact) / sum(plan) as percent
		FROM (
			SELECT $type, booker_fin_source, 0 as plan, s as fact, -s as diff 
				FROM plat
				WHERE plat.d>='$year' AND plat.d<'$next_year' AND plat.d <= '$dfilter' 
			UNION ALL
			SELECT fpa.$type as $type, sp.booker_fin_source as booker_fin_source, sp.sum as plan, 0 as fact, sp.sum as diff 
				FROM stat_pfhd sp 
				LEFT JOIN fp_article fpa ON sp.fp_article_id = fpa.id 
				WHERE sp.stat_pfhd_ver_id = '$pfhd_ver_id' AND sp.pfhd_y='$year' 
		) pre WHERE true $w 
		GROUP BY $type, booker_fin_source";
	$rows = sql_rows($sql);
	return $rows;
}

?>

<div class="header-link">
<h1 style="color: #fff;"><?=$title?></h1>
</div>
<div class="container" style="text-align: center; margin: 20px auto;">
	<form id="reportform" method="get">
		<label>
			Версия ПФХД:
			<select id="versionselect" name="pfhd_ver_id">
				<?php foreach($pfhd_versions as $ver): ?>
					<option value="<?=$ver['id']?>" <?= (isset($_GET['pfhd_ver_id']) && $_GET['pfhd_ver_id'] == $ver['id']) ? 'selected' : '' ?>><?=$ver['name']?></option>
				<?php endforeach; ?>
			</select>
		</label>
		<label>
			сформировать по:
			<select id="typeselect" name="type">
				<option value="<?=KOSGU?>" <?= (isset($_GET['type']) && $_GET['type'] == KOSGU) ? 'selected' : '' ?>>КОСГУ</option>
				<option value="<?=KVR?>" <?= (isset($_GET['type']) && $_GET['type'] == KVR) ? 'selected' : '' ?>>КВР</option>
			</select>
		</label>
		<label>
			равному:
			<select id="filterselect" name="filter" style="min-width: 4em;" >
				<?php foreach($types as $t): ?>
					<option value="<?=$t[$type]?>" <?= (isset($_GET['filter']) && $_GET['filter'] == $t[$type]) ? 'selected' : '' ?>><?= $t[$type] ? $t[$type] : '&nbsp;' ?></option>
				<?php endforeach; ?>
			</select>
		</label>
		<label>
			ИФ:
			<select id="fsselect" name="fsfilter" style="min-width: 4em;" >
				<?php foreach($fss as $fs): ?>
					<option value="<?=$fs['booker_fin_source']?>" <?= (isset($_GET['fsfilter']) && $_GET['fsfilter'] == $fs['booker_fin_source']) ? 'selected' : '' ?>>
					<?= $fs['booker_fin_source'] ? $fs['booker_fin_source'] : '&nbsp;' ?></option>
				<?php endforeach; ?>
			</select>
		</label>
		<br><br>
		<label>			
			Учитывать факт по дату: <input type="date" name="dfilter" value="<?= $dfilter ?>">			
		</label>
		<!-- -->
		<input type="submit" value="Обновить">
		<!-- -->
	</form>
	<?php if(isset($_GET['pfhd_ver_id'])): ?>
		<br><a href="#" class="pseudo-link" onclick="toExcel(); return false;">Скачать в формате XLS</a>
	<?php endif; ?>
</div>

<?php 

$labels = array(
	'kosgu'=>['label'=>'КОСГУ'],
	'booker_fin_source'=>['label'=>'ИФ бух.'],
	'plan'=>['label'=>'План','format'=>'sum'],
	'fact'=>['label'=>'Факт','format'=>'sum'],
	'diff'=>['label'=>'Разница','format'=>'sum'],
	'kvr'=>['label'=>'КВР'],
	'percent'=>['label'=>'% вып.','format'=>'sum']
);

if (isset($_GET['type']) && isset($_GET['pfhd_ver_id'])) 
{
	$form_type = sql_escape($_GET['type']);
	$pfhd_ver_id = sql_escape($_GET['pfhd_ver_id']);
	$plan_period_id = sql_get_value('plan_period_id', 'stat_pfhd_ver', "id = '$pfhd_ver_id'");
	$year = sql_get_value("y_from","plan_period","id='$plan_period_id'");
	echo '<div id="table">';
	if($form_type == KOSGU) {
		echo Utils::toHTML(getSumTable($pfhd_ver_id, 'kosgu', $year), $labels);
	} else if($form_type == KVR) {
		echo Utils::toHTML(getSumTable($pfhd_ver_id , 'kvr', $year), $labels);
	}
	echo '</div>';
}
else
	echo '<script>document.getElementById("reportform").submit();</script>';	

?>

<script>
	function toExcel() 
	{
		var table = document.querySelector('#table');
        download('form_737' + '_' + new Date().getTime() + '.xls', table.outerHTML);
    }
	
	function download(filename, text) 
	{
        var URL = window.URL || window.webkitURL || window.mozURL || window.msURL;
        var blob = new Blob([text]);
        var url = URL.createObjectURL(blob);
        var pom = document.createElement('a');
        pom.setAttribute('href', url);
        pom.setAttribute('download', filename);
        if (document.createEvent) 
		{
            var event = document.createEvent('MouseEvents');
            event.initEvent('click', true, true);
            pom.dispatchEvent(event);
        }
        else 
		{
            pom.click();
        }
    }
	
	//AVD	
	var f1 = function() { document.getElementById("reportform").submit(); };
	var f2 = f1;
	var f3 = f1;
	var f4 = f1;
	document.getElementById('versionselect').addEventListener("change", f1); 
	document.getElementById('typeselect').addEventListener("change", f2); 	
	document.getElementById('filterselect').addEventListener("change", f3); 	
	document.getElementById('fsselect').addEventListener("change", f4); 	
	//
	
</script>