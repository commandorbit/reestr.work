<?php
require $_SERVER['DOCUMENT_ROOT'].'/td/db.php';

class ReestrDB extends ConfigDB {
    const ColumnSuffix1C = '_Ref';
    const ColumnSuffixEnum1C = '_Enum';
//	$virtual_tables = ['virtual_zfo_limit'];

//	Описание структуры виртуальных таблиц
	private static $priv_virtual_table_columns = [
		'virtual_zfo_limit'=>[
			'fp_year'=>['type'=>'int']
			,'zfo_id'=>['type'=>'int']
			,'fin_source_id'=>['type'=>'int']
			,'sum_limit'=>['type'=>'decimal']
			,'sum_amount'=>['type'=>'decimal']
			,'sum_diff'=>['type'=>'decimal']
		],
		'virt_dogovor'=>[
			'favorites'=>'int'
			,'files'=>'int'
			,'id'=>'int'
			,'fp_year'=>'int'
			,'dogovor_status_id'=>'int'
			,'thesis_state'=>'int'
			,'thesis_state_ts'=>'timestamp'
			,'concluded_ku'=>'tinyint'
			,'zfo_id'=>'int'
			,'document_type_id'=>'int'
			,'demand'=>'varchar'
			,'amount'=>'decimal'
		],
		'virt_dogovor_status_history'=>[
			 'dogovor_id'=>'int'
			,'dogovor_status_id'=>'int'
			,'ts'=>'timesamp'
			,'user_id'=>'int'
		],
		
		'virt_smeta_limit_compare'=>[
			 'smeta_id'=>'int'
			,'fp_year_id'=>'int'
			,'zfo_id'=>'int'
			,'fin_source_id'=>'int'
			,'sum_limit'=>'decimal'
			,'sum_amount'=>'decimal'
			,'sum_diff'=>'decimal'
		],
		'обязательства'=>[
			'Договор'=>'varchar'
			,'Контрагент'=>'varchar'
			,'ИНН'=>'varchar'
			,'КВР'=>'varchar'
			,'dog_ids'=>'varchar'
			,'Сумма_1С'=>'decimal'
			,'Сумма_СУП'=>'decimal'
			,'Разница'=>'decimal'
			,'Сумма_1С_Год0'=>'decimal'
			,'Сумма_СУП_Год0'=>'decimal'
			,'Разница_Год0'=>'decimal'
			,'Сумма_1С_Год1'=>'decimal'
			,'Сумма_СУП_Год1'=>'decimal'
			,'Разница_Год1'=>'decimal'
		],
		'денежные_обязательства'=>[
			'Договор'=>'varchar'
			,'Контрагент'=>'varchar'
			,'ИНН'=>'varchar'
			,'КВР'=>'varchar'
			,'dog_ids'=>'varchar'
			,'Сумма_1С'=>'decimal'
			,'СУП_Заявки'=>'decimal'
			,'Разница'=>'decimal'
			,'СУП_Платежки'=>'decimal'
		],
		
		'virtual_738_diff'=>[
			'dogovor_id'=>'int'
			,'fp_year'=>'int'
			//,'is_new'=>'int'
			,'zfo_id'=>'int'
			,'dogovor_num'=>'varchar'
			,'d'=>'date'
			,'booker_fin_source'=>'int'
			,'fin_source_type_id'=>'int'
			,'kosgu'=>'varchar'
			,'kvr'=>'varchar'
			,'sum1'=>'decimal'
			,'sum2'=>'decimal'
			,'sum_dif'=>'decimal'
			,'date_start'=>'date'
			,'date_end'=>'date'			
			,'subject'=>'varchar'
			,'name'=>'varchar'
			,'inn'=>'varchar'
		],
		
		'virt_pfhd'=>[
			'dogovor_id'=>'int'
			,'pfhd_y'=>'int'
			,'dogovor_status_id'=>'int'
			,'fp_year'=>'int'
			,'zfo_id'=>'int'
			,'document_type_id'=>'int'
			,'subject'=>'varchar'
			,'contragent_id'=>'varchar'
			,'inn'=>'varchar'
			,'dogovor_num'=>'varchar'
			,'d'=>'date'
			,'booker_fin_source'=>'int'
			,'fp_article_id'=>'int'
			,'plan_grafic_id'=>'int'
			,'kosgu'=>'varchar'
			,'kvr'=>'varchar'
			,'sum'=>'decimal'
			,'date_start'=>'date'
			,'date_end'=>'date'			
		],
		'v_fp_article_select'=>[
			'id'=>'int'
			,'parent_id'=>'int'
			,'name'=>'varchar'
			,'leaf'=>'tinyint'
		],
		'v_fp_article_limited'=>[
			'id'=>'int'
			,'parent_id'=>'int'
			,'shifr'=>'varchar'
			,'name'=>'varchar'
			,'leaf'=>'tinyint'
			//
			,'kosgu'=>'int'
			,'prim'=>'varchar'
			,'okved_id'=>'int'
			,'vid_rashodov_id'=>'int'
			,'kvr'=>'int'
			,'fp_object_type_id'=>'int'
			,'pfhd_article_2016_id'=>'int'
			,'pfhd_article_2017_id'=>'int'
			,'asu_pfhd_expense_direction_id'=>'int'
			,'asu_pfhd_institution_program_id'=>'int'
			
		],
		'virt_zayav_dog'=> [
			'id'=>'int'
			,'zfo_id'=>'int'
			,'dogovor_id'=>'int'
			,'plat_type_id'=>'int'
			,'period_y_m'=>'varchar'
			,'sum'=>'decimal'
			,'d'=>'date'
			,'y'=>'int'
			,'reestr_d'=>'date' 
			,'contragent_id'=>'int'
			,'contragent_fio'=>'varchar'
			,'notes'=>'varchar'
			,'zayav_status_id'=>'int'
		],
		'virt_dogovor_sostav_rep'=>[
			'id'=>'int',
			'dogovor_status_id'=>'int',
			'fp_year'=>'int',
			'zfo_id'=>'int',
			'document_type_id'=>'int',
			'd'=>'date',
			'contragent_id'=>'int',
			'inn'=>'varchar',
			'dogovor_num'=>'varchar',
			'subject'=>'varchar',
			'demand'=>'varchar',
			'basis'=>'varchar',
			'plan_grafic_id'=>'int',
			'place_method_id'=>'int',
			'fp_object_id'=>'int',
			'fp_article_id'=>'int',
			'fin_source_id'=>'int',
			'booker_fin_source'=>'int',
			'period_id'=>'int',
			'kosgu'=>'int',
			'kvr'=>'int',
			'unit_id'=>'int',
			'quantity'=>'int',
			'amount'=>'decimal',
			'sum_zayav'=>'decimal',
			'sum_ost'=>'decimal',
			'max_plat_d'=>'date',
			'date_start'=>'date',
			'date_end'=>'date',
			'period_place_id'=>'int',
			'period_finish_id'=>'int',
			'description'=>'varchar'
		],
		
		'virt_smeta_month_rep'=>[
			'id'=>'int'
			,'sostavid'=>'int'
			,'description'=>'varchar'
			,'zfo_id'=>'int'
			,'fp_year_id'=>'int'
			,'not_in_pfhd'=>'tinyint'
			,'fin_source_id'=>'int'
			,'booker_fin_source'=>'int'
			,'fp_article_id'=>'int'
			,'fp_object_id'=>'int'
			,'natural_indicator_id'=>'int'
			,'unit_id'=>'int'
			,'quantity'=>'decimal'
			,'price'=>'decimal'			
			,'itogo'=>'decimal'
			,'amount0'=>'decimal'
			,'amount1'=>'decimal'
			,'amount2'=>'decimal'
			,'amount3'=>'decimal'
			,'amount4'=>'decimal'
			,'amount5'=>'decimal'
			,'amount6'=>'decimal'
			,'amount7'=>'decimal'
			,'amount8'=>'decimal'
			,'amount9'=>'decimal'
			,'amount10'=>'decimal'
			,'amount11'=>'decimal'
			,'amount12'=>'decimal'
		],
		
		'virt_smeta_income'=>[
			'smeta_id'=>'int'
			,'zfo_id'=>'int'
			,'fp_year_id'=>'int'
			,'fin_source_id'=>'int'
			,'fp_article_income_id'=>'int'
			,'amount'=>'decimal'
			,'nds'=>'int'
			,'amount_nds'=>'decimal'
			,'amount_wo_nds'=>'decimal'
			,'amount1'=>'decimal'
			,'amount2'=>'decimal'
			,'amount3'=>'decimal'
			,'amount4'=>'decimal'
			,'amount5'=>'decimal'
			,'amount6'=>'decimal'
			,'amount7'=>'decimal'
			,'amount8'=>'decimal'
			,'amount9'=>'decimal'
			,'amount10'=>'decimal'
			,'amount11'=>'decimal'
			,'amount12'=>'decimal'
			,'amount_next'=>'decimal'			
		],
		
		'virt_smeta_building_itog'=>[
			'smeta_id'=>'int',
			'building_id'=>'int',
			'amount'=>'decimal'
		],
		
		'virt_smeta_building'=>[
			 'fp_year_id'=>'int'
			,'fp_object_id'=>'int'
			,'smeta_id'=>'int'
			,'zfo_id'=>'int'
			,'fin_source_id'=>'int'
			,'fp_article_id'=>'int'
			,'natural_indicator_id'=>'int'
			,'price'=>'decimal'
			,'quantity'=>'decimal'			
			,'amount'=>'decimal'
			,'percent'=>'decimal'
			,'itogo'=>'decimal'
			,'description'=>'varchar'
		],
		
		'virt_smeta_result'=>[
			 'id'=>'varchar'
			,'smeta_id'=>'int'
			,'may_edit'=>'tinyint'
			,'kvr'=>'varchar'
			,'article_type'=>'varchar'
			,'article_name'=>'varchar'
			,'v'=>'decimal'
			,'v0'=>'decimal'
			,'v1'=>'decimal'
			,'v2'=>'decimal'
			,'v3'=>'decimal'
			,'v4'=>'decimal'
			,'v5'=>'decimal'
			,'v6'=>'decimal'
			,'v7'=>'decimal'
			,'v8'=>'decimal'
			,'v9'=>'decimal'
			,'v10'=>'decimal'
			,'v11'=>'decimal'
			,'v12'=>'decimal'
		],
		
		'virt_smeta_sostav_compare'=>[
			'smeta_id'=>'int'
			,'zfo_id'=>'int'
			,'fin_source_id'=>'int'
			,'kvr'=>'varchar'
			,'ver_old'=>'int'
			,'ver_new'=>'int'			
			,'amount_old'=>'decimal'
			,'amount_new'=>'decimal'
			,'amount_delta'=>'decimal'
		],
		
		'virt_smeta_dogovor_compare'=>[
			 'fp_year_id'=>'int'
			,'zfo_id'=>'int'
			,'fin_source_id'=>'int'
			,'dogovor_amount'=>'decimal'
			,'smeta_amount'=>'decimal'
			,'delta'=>'decimal'
		],
		
		'virt_smeta_dogovor_compare2'=>[
			 'fp_year_id'=>'int'
			,'zfo_id'=>'int'
			,'fin_source_id'=>'int'
			,'fp_article_id'=>'int'
			,'fp_object_id'=>'int'
			,'smeta_amount'=>'decimal'
			,'dogovor_amount'=>'decimal'
			,'delta'=>'decimal'
		],
		
		'virt_dogovor_limit_compare'=>[
			 'y'=>'int'
			,'zfo_id'=>'int'
			,'fin_source_id'=>'int'
			,'kvr'=>'int'
			,'limit_amount'=>'decimal'
			,'dogovor_amount'=>'decimal'
			,'delta'=>'decimal'
		],
		
		'virt_smeta_limit_change'=>[
			'doc_lim_change_id'=>'int'
			,'smeta_id'=>'int'
			,'zfo_id'=>'int'
			,'fin_source_id'=>'int'
			,'y'=>'int'
			,'kvr'=>'int'
			,'sum'=>'decimal'
			,'d'=>'date'
			,'description'=>'varchar'
			,'files'=>'varchar'
		],
	];
	
	public static function query_virt_dogovor_status_history($filter) {
        $sql = "SELECT id dogovor_id, ts, dogovor_status_id, user_id 
					FROM log_dogovor 
					WHERE true ";
		if (isset($filter['dogovor_id'])) {
			$sql .= " AND id = " . (int) $filter['dogovor_id'];
		}
		$sql .= " ORDER BY id, ts ";
		$res = sql_query($sql);
		$tmp_rows = sql_rows_fix_numeric($res);
        $prev_status_id = -1;
		$rows = [];
        foreach ($tmp_rows as $row) {
            if ($row['dogovor_status_id'] != $prev_status_id) {
                $rows[] = $row;
                $prev_status_id = $row['dogovor_status_id'];
            }
        }
		// Вернем в обратном порядке = послденяя запись - первой
		return array_reverse($rows);
	}
	
    private static function query1Ctable($table) {
        if ($table == 'РазделыЛицевыхСчетов') {
            $q = OneC::select('Р.Ссылка','Л.Наименование')->from(['Справочник.РазделыЛицевыхСчетов', 'Р'])
            ->join(['Справочник.ЛицевыеСчета','Л'])->on('Р._OwnerIDRRef','=','Л.Ссылка')
            ->where('Р._Marked','=','0x00');
            
        } else if ($table == 'КОСГУ')   {
            $q = OneC::select('КОСГУ.Ссылка',['КОСГУ._Code', 'Наименование'])->from(['Справочник.КОСГУ','КОСГУ'])
            ->where('_Marked','=','0x00');
        
        } else if ($table == 'Контрагенты')   {
            $q = OneC::select('К.Ссылка','К.Наименование')->from(['Справочник.Контрагенты','К'])
            ->where('_Marked','=','0x00')
		//	->where('К.ВидКонтрагента','=',OneC::enum('ВидыКонтрагентов.ФизЛицо'))
			->order_by('Наименование');
        
        } else {
            $q = OneC::select()->from('Справочник.'.$table)
                ->where('_Marked','=','0x00')
                //->where('_Folder','<>','0x00')                
                ->order_by('Наименование');
            //echo (string) $q;
        } 
        return $q->execute();
    }
	
	// Виртуальные таблицы для них поля определены фиксированно
	public static function is_virtual_table($table) {
		$v = static::$priv_virtual_table_columns;
		return isset($v[$table]);
	}
	
	public static function virtual_table_columns($table) {
		$cols = self::$priv_virtual_table_columns[$table];
		foreach ($cols as $name=>$data) {
			if (is_string($data)) {
				// если строка, то указан тип данных
				$cols[$name] = [];
				$cols[$name]['type'] = $data;
			}
			$cols[$name]['name'] = $name;
			$cols[$name]['null'] = 1;
			$cols[$name]['length'] = 0;
		}
		//var_dump($cols);		die();
		return $cols;
	}	

	
	public static function query_virt_dogovor($filter) {
		$sql = "SELECT NULL as favorites, (select count(df.id) from dogovor_files df where (df.dogovor_id = d.id)) AS files, d.id AS id, d.fp_year, d.dogovor_status_id, thesis_dogovor.state AS thesis_state,  thesis_dogovor.ts AS thesis_state_ts,d.concluded_ku, d.zfo_id,d.document_type_id ,d.subject,d.demand,sum(d_sostav.amount) AS amount, d.contragent_id, d.d, d.dogovor_num,sum(d_sostav.quantity) AS quantity, d.basis,d.plan_grafic_id ,d.place_method_id ,d.period_place_id ,d.period_finish_id
				FROM  dogovor d LEFT JOIN dogovor_sostav d_sostav ON d.id = d_sostav.dogovor_id
				LEFT JOIN thesis_dogovor on thesis_dogovor.dogovor_id = d.id
				WHERE true ";
		$has_zfo = user_has_zfo();		
		if ($has_zfo) {
			$zfo = user_zfo_id_str();
			$fin_source_list = array_to_sql_string(user_own_fin_sources());
			$sql .= " AND (d.zfo_id in ($zfo) OR d_sostav.fin_source_id IN ($fin_source_list)) ";
		}
		if (isset($_GET['fp_year'])) {
			$fp_year = (int) $_GET['fp_year'];
			$sql .= " AND d.fp_year = '$fp_year' ";
		}
		$sql .= " GROUP BY d.id ";
		$sql .= " ORDER BY d.id DESC ";

		$res = sql_query($sql);
	
		$rows = sql_rows_fix_numeric($res);
		$zfos = $has_zfo ? user_zfo_ids() : [];
		foreach($rows as &$row) {
			$row['_may_edit'] = !$has_zfo || in_array($row['zfo_id'],$zfos);
		//	$row['_may_approve_smeta_sostav'] = !$has_zfo;
		}

		return $rows;
	}
	
	public static function query_virt_smeta_result($filter) {
		$w = " true ";
		require_once __DIR__  . '/smeta_virt_result.php';
		if (isset($filter['smeta_id'])) {
			$smeta_id = (int) $filter['smeta_id'];
			$rows = SmetaVirtResult::getRows($smeta_id);
		} else {
			$smeta_rows = sql_rows("select id from smeta");
			$rows = [];
			foreach($smeta_rows as $r) {
				$smeta_id=$r['id'];
				$rows= array_merge($rows,SmetaVirtResult::getRows($smeta_id));
			}
		}
		return $rows;
	
	}

	public static function query_virt_dogovor_limit_compare($filter) {
		$sql = " 
		SELECT y,zfo_id,fin_source_id,kvr,SUM(limit_amount) limit_amount, SUM(dogovor_amount) dogovor_amount,SUM(limit_amount)-SUM(dogovor_amount) delta
		FROM (
		SELECT y,zfo_id,fin_source_id,kvr,amount limit_amount, 0 dogovor_amount
		FROM zfo_smeta_limit 
		UNION ALL 
		SELECT p.smeta_y,d.zfo_id,ds.fin_source_id,kvr,0 limit_amount,amount  dogovor_amount
		FROM dogovor_sostav ds 
		INNER JOIN dogovor d ON ds.dogovor_id = d.id 
		INNER JOIN dogovor_status status ON d.dogovor_status_id = status.id 
		INNER JOIN fp_article a ON ds.fp_article_id = a.id
		INNER JOIN period p ON ds.period_id = p.id 
		WHERE status.use_limit = 1
		) pre 
		WHERE y>=2019
		GROUP BY y,zfo_id,fin_source_id,kvr
		";
		
		if (user_has_zfo()) {
			$zfo_str = user_zfo_id_str();
			$sql .= " HAVING zfo_id in ( " . $zfo_str . ")";
		}
		
		$res = sql_query($sql);
		$rows = sql_rows_fix_numeric($res);
		return $rows;
	
	}	

	public static function query_virt_smeta_dogovor_compare2($filter) {
		$sql = "SELECT fp_year_id,zfo_id,fin_source_id,fp_article_id,fin_source_id,fp_object_id,
					sum(smeta_amount) smeta_amount, sum(dogovor_amount) dogovor_amount, sum(smeta_amount)-sum(dogovor_amount) delta
			FROM (
			select s.fp_year_id,ifnull(ss.zfo_id,s.zfo_id) zfo_id, ss.fin_source_id, ss.fp_article_id, ss.fp_object_id, ss.amount smeta_amount,0 dogovor_amount
			from smeta_sostav ss inner join smeta s ON ss.smeta_id = s.id
			where s.fp_year_id>=2019
			UNION ALL
			select period.smeta_y,d.zfo_id,ds.fin_source_id, ds.fp_article_id, ds.fp_object_id, 0 as smeta_smount,ds.amount as dogovor_amount
			FROM dogovor_sostav ds INNER JOIN dogovor d ON ds.dogovor_id = d.id
			INNER JOIN dogovor_status ON d.dogovor_status_id = dogovor_status.id 
			INNER JOIN period ON ds.period_id = period.id
			WHERE period.smeta_y>=2019 AND dogovor_status.use_limit = 1
			) pre
			GROUP BY fp_year_id,zfo_id,fin_source_id,fp_article_id, fp_object_id";
			
		if (user_has_zfo()) {
			$zfo_str = user_zfo_id_str();
			$sql .= " HAVING zfo_id in ( " . $zfo_str . ")";
		}
		$res = sql_query($sql);
		$rows = sql_rows_fix_numeric($res);
		return $rows;	
	}
	
	public static function query_virt_smeta_limit_change($filter) {
		$sql = " 
		SELECT s.doc_lim_change_id, s.sum, s.zfo_id, s.fin_source_id, s.kvr, s.smeta_id, d.d, d.description
		FROM doc_lim_change_sostav s 
		INNER JOIN doc_lim_change d ON s.doc_lim_change_id = d.id
		WHERE s.smeta_id IS NOT NULL
		";
		if (isset($filter['smeta_id'])) {
			$sql .= ' AND s.smeta_id = ' . $filter['smeta_id'];
		}
		$res = sql_query($sql);
		$rows = sql_rows_fix_numeric($res);
		foreach($rows as &$r) {
			$files = [];
			$file_rows = sql_rows("select id,filename FROM doc_lim_change_files WHERE doc_lim_change_id=".$r['doc_lim_change_id']);
			foreach($file_rows as $fr) {
				$files[] = '<a href="/download.php?id='.$r['doc_lim_change_id'].'&table=doc_lim_change_files&id='.$fr['id'].'">'.$fr['filename']."</a>";
			}
			$r['files'] = implode(', ' ,$files);
		}
		return $rows;
	
	}

	public static function query_virt_smeta_dogovor_compare($filter) {
		$sql = " 
		SELECT fp_year_id,zfo_id,fin_source_id,SUM(smeta_amount) smeta_amount, SUM(dogovor_amount) dogovor_amount,SUM(smeta_amount)-SUM(dogovor_amount) delta
		FROM (
		SELECT s.fp_year_id,ifnull(ss.zfo_id,s.zfo_id) zfo_id,fin_source_id,amount smeta_amount, 0 dogovor_amount
		FROM smeta_sostav ss inner join smeta s ON ss.smeta_id = s.id
		UNION ALL 
		SELECT p.smeta_y,d.zfo_id,ds.fin_source_id,0,amount 
		FROM dogovor_sostav ds 
		INNER JOIN dogovor d ON ds.dogovor_id = d.id 
		INNER JOIN dogovor_status status ON d.dogovor_status_id = status.id 
		INNER JOIN period p ON ds.period_id = p.id 
		WHERE status.use_limit = 1
		) pre 
		GROUP BY fp_year_id,zfo_id,fin_source_id
		";

		$res = sql_query($sql);
		$rows = sql_rows_fix_numeric($res);
		return $rows;
	
	}
			
	public static function query_virt_smeta_sostav_compare($filter) {
		$w = " true ";
		if (isset($filter['smeta_id'])) {
			$w .= " AND smeta_id = " . (int)$filter['smeta_id'];
		}
		$ver = 'null';
		//var_dump($filter); die();
		if (isset($filter['ver_old']))
			if ($filter['ver_old'])
				$ver = $filter['ver_old'];		
		$sql = "
			SELECT smeta_id, zfo_id, fin_source_id, kvr, 
			  MAX(ver_old) AS ver_old, MAX(ver_new) AS ver_new, 
			  SUM(amount_old) AS amount_old, SUM(amount_new) AS amount_new, 
			  SUM(amount_new) - SUM(amount_old) AS amount_delta FROM
			( SELECT * FROM
			  ( SELECT
				  ss.smeta_id,
				  IFNULL(ss.zfo_id, s.zfo_id) AS zfo_id,
				  ss.fin_source_id,
				  fpa.kvr,
				  0 AS ver_old,
				  s.ver AS ver_new,
				  0 AS amount_old,
				  SUM(ss.amount) AS amount_new,
				  COUNT(ss.amount) AS c
				FROM smeta_sostav ss
				  INNER JOIN smeta s
					ON ss.smeta_id = s.id
				  LEFT OUTER JOIN fp_article AS fpa
					ON ss.fp_article_id = fpa.id
				GROUP BY ss.smeta_id, s.ver, IFNULL(ss.zfo_id, s.zfo_id), ss.fin_source_id, fpa.kvr
			  ) AS T1
			  UNION ALL
			  SELECT * FROM
			  ( SELECT
				  ss.smeta_id,
				  IFNULL(ss.zfo_id, s.zfo_id) AS zfo_id,
				  ss.fin_source_id,
				  fpa.kvr,
				  ss.ver AS ver_old,
				  0 AS ver_new,      
				  SUM(ss.amount) AS amount_old,
				  0 AS amount_new,
				  COUNT(ss.amount) AS c
				FROM ver_smeta_sostav ss
				  INNER JOIN ver_smeta s
					ON ss.smeta_id = s.id AND ss.ver = s.ver
				  LEFT OUTER JOIN fp_article AS fpa
					ON ss.fp_article_id = fpa.id
				  WHERE s.ver = IFNULL($ver, (SELECT MAX(ver) FROM ver_smeta WHERE id = s.id))
				  GROUP BY ss.smeta_id, IFNULL(ss.zfo_id, s.zfo_id), ss.fin_source_id, fpa.kvr, s.ver
			  ) AS T2  
			) T
			WHERE $w			
			GROUP BY smeta_id, zfo_id, fin_source_id, kvr
			ORDER BY smeta_id, zfo_id, fin_source_id, kvr";
		//echo("<html><pre>$sql"); die();	
		$res = sql_query($sql);
		$rows = sql_rows_fix_numeric($res);
		return $rows;
	}	
	
	public static function query_virt_smeta_building_itog($filter) {
		if (isset($filter['smeta_id'])) {
			$smeta_id = (int) $filter['smeta_id'];
		} else {
			var_dump($filter);
			die("ERROR no smeta_id in filter");
		}
		$smeta_row = sql_get_array("smeta","id=$smeta_id");
		$zfo_id = $smeta_row['zfo_id'];
		$zfo_type_id = sql_get_value("zfo_type_id","zfo","id=$zfo_id");
		// Если не ЦФР - не считаем
		if ($zfo_type_id != ZFO_TYPE_ZFR) {
			return [];
		}
		//
		$result = [];
		$smeta_fin_source_rows = sql_rows("SELECT * FROM smeta_fin_source WHERE smeta_id = $smeta_id");
		//echo "<pre>"; var_dump($smeta_fin_source_rows); die;

		foreach ($smeta_fin_source_rows as &$smeta_fin_source_row) {
			//
			$fin_source_id = $smeta_fin_source_row['fin_source_id'];
			$fp_year = $smeta_row['fp_year_id'];
			// c 2019 года берутся расходы только по источнику указанному в building_percent
			if ($fin_source_id) {
					$sql = "SELECT $smeta_id AS smeta_id, sbp.fp_object_id, 
			  ( SELECT SUM(round(ss.amount*IFNULL(sabp.percent,sbp.percent)/100,2))  
			    FROM smeta_sostav ss JOIN smeta s ON s.id = ss.smeta_id 
				LEFT JOIN building_article_percent sabp ON ss.fp_object_id = sabp.fp_object_id AND s.fp_year_id = sabp.fp_year AND ss.fp_article_id = sabp.fp_article_id AND sabp.fin_source_id = $fin_source_id
				WHERE ss.fp_object_id = sbp.fp_object_id AND s.fp_year_id = $fp_year 
				AND (s.fp_year_id <= 2018 OR ss.fin_source_id = $fin_source_id)
			  ) AS itogo
				FROM building_percent sbp WHERE  sbp.fp_year = $fp_year AND  sbp.fin_source_id = $fin_source_id
				";
				//	echo $sql;
					$res = sql_query($sql);
					$rows = sql_rows_fix_numeric($res);
			} else {
				$rows = [];
			}
			//
			foreach ($rows as &$row) {
				$result[] = $row;
			}
			//
		}
		return $result;
	}



	public static function query_virt_smeta_building($filter) {
		$w =[];
		if (isset($filter['smeta_id'])) {
			$w[] = 'ss.smeta_id = ' . (int) $filter['smeta_id'];
		}
		
		if (isset($filter['fp_object_id'])) {
			$w[] = 'ss.fp_object_id = ' . (int) $filter['fp_object_id'];
		}
		if (isset($filter['fp_year_id'])) {
			$w[] = 's.fp_year_id = ' . (int) $filter['fp_year_id'];
		}
		if (isset($filter['fin_source_id'])) {
			$w[] = ' bp.fin_source_id  = ' . (int) $filter['fin_source_id'] ;
		}
		
		$sql = "
		SELECT s.fp_year_id,s.zfo_id, bp.fin_source_id,ss.smeta_id,ss.fp_article_id,ss.fp_object_id,ss.natural_indicator_id,ss.quantity,ss.price,
			ss.amount,ss.amount1,ss.amount2,ss.amount3,ss.amount4,ss.amount5,ss.amount6,ss.amount7,ss.amount8,ss.amount9,ss.amount10,ss.amount11,ss.amount12, 
			IFNULL(bap.percent,bp.percent) percent, 
			IF(s.fp_year_id<=2018,round(ss.amount*IFNULL(bap.percent,bp.percent)/100,2),ss.amount)  itogo,			
			ss.description
		FROM smeta_sostav ss INNER JOIN smeta s ON s.id = ss.smeta_id 
		INNER JOIN building_percent bp ON s.fp_year_id = bp.fp_year AND ss.fp_object_id = bp.fp_object_id
		LEFT JOIN building_article_percent bap ON ss.fp_object_id = bap.fp_object_id AND s.fp_year_id = bap.fp_year AND ss.fp_article_id = bap.fp_article_id AND bp.fin_source_id = bap.fin_source_id
		WHERE (s.fp_year_id<=2018 OR (ss.fin_source_id = bp.fin_source_id)) ";
		if ($w) {
			$sql .=  " AND " . implode(' AND ' ,$w );
		}

	
		//echo $sql;
		$res = sql_query($sql);
		$rows = sql_rows_fix_numeric($res);
		return $rows;
	}
	
/*	
	public static function query_virt_smeta_building($filter) {
		$w =[];
		if (isset($filter['smeta_id'])) {
			$w[] = 'ss.smeta_id = ' . (int) $filter['smeta_id'];
		}
		
		if (isset($filter['fp_object_id'])) {
			$w[] = 'ss.fp_object_id = ' . (int) $filter['fp_object_id'];
		}
		if (isset($filter['fp_year_id'])) {
			$w[] = 's.fp_year_id = ' . (int) $filter['fp_year_id'];
		}
		if (isset($filter['fin_source_id'])) {
			$w[] = ' bp.fin_source_id  = ' . (int) $filter['fin_source_id'] ;
		}
		
		$sql = "
		SELECT s.fp_year_id,s.zfo_id, bp.fin_source_id,ss.smeta_id,ss.fp_article_id,ss.fp_object_id,ss.natural_indicator_id,ss.quantity,ss.price,
			ss.amount,ss.amount1,ss.amount2,ss.amount3,ss.amount4,ss.amount5,ss.amount6,ss.amount7,ss.amount8,ss.amount9,ss.amount10,ss.amount11,ss.amount12, 
			IFNULL(bap.percent,bp.percent) percent, 
			round(ss.amount*IFNULL(bap.percent,bp.percent)/100,2)  itogo,			
			ss.description
		FROM smeta_sostav ss INNER JOIN smeta s ON s.id = ss.smeta_id 
		INNER JOIN building_percent bp ON s.fp_year_id = bp.fp_year AND ss.fp_object_id = bp.fp_object_id
		LEFT JOIN building_article_percent bap ON ss.fp_object_id = bap.fp_object_id AND s.fp_year_id = bap.fp_year AND ss.fp_article_id = bap.fp_article_id AND bp.fin_source_id = bap.fin_source_id
		";
		if ($w) {
			$sql .=  " WHERE " . implode(' AND ' ,$w );
		}

	
		//echo $sql;
		$res = sql_query($sql);
		$rows = sql_rows_fix_numeric($res);
		return $rows;
	}
*/	
	private static function smeta_building_result() {
		
		
	}

	public static function query_обязательства($filter) {
		require_once($_SERVER['DOCUMENT_ROOT'] . '/modules/c1.php');
		$kvd =  (isset($filter['KVD'])) ? $filter['KVD'] : null;
		$y = (isset($filter['y'])) ? $filter['y'] : 2016;
		return C1::обязательства($kvd,$y);
	}
	
	public static function query_v_fp_article_select($filter) {
		/* Подводим итоги	*/
		$sql = "SELECT id, concat('/',shifr,'/ ',name) as name, parent_id  from fp_article ";
		if (user_has_zfo()) {
			$zfo_str = user_zfo_id_str();

			$rows = sql_rows("select fp_article_id from fp_article_zfo where zfo_id in ($zfo_str)");
			$fp_ids = [];
			foreach($rows as $row) {
				$fp_ids[] = $row['fp_article_id'];
			}
			if (!$fp_ids) {
				$fp_ids = [-10];
			}
			while (true) {
				$in_str = implode(",",$fp_ids);
				$tsql = "select distinct parent_id as id from fp_article where parent_id is not null and parent_id not in ($in_str) and id in ($in_str)";
				$rows = sql_rows($tsql);
				if (count($rows)==0) break;
				foreach($rows as $row) {
					$fp_ids[] = $row['id'];
				}
			}
			$sql .=  " where id in (".implode(",",$fp_ids).")";
		}
		$rows = sql_rows_fix_numeric(sql_query($sql));
		foreach ($rows as &$row) {
			$row['leaf'] = (sql_get_value('count(id)', 'fp_article', "parent_id='" . $row['id'] ."'") == 0);
		}		
		return $rows;		
	}

	public static function query_virt_dogovor_sostav_rep($filter) {
		$sql = "SELECT d.id,d.dogovor_status_id,d.fp_year,d.zfo_id,d.document_type_id,d.d,d.contragent_id,ctgnt.inn,
		d.dogovor_num,d.subject,d.demand,d.basis,d.plan_grafic_id,d.place_method_id,
		ds.fp_object_id,ds.fp_article_id ,ds.fin_source_id, fs.booker_fin_source,
		ds.period_id ,d.date_start, d.date_end ,
		a.kosgu,a.kvr,
		ds.unit_id,
		ds.quantity,
		ds.amount,
		ds.description,
		(select sum(zs.sum) from zayav_sostav zs join zayav z on zs.zayav_id = z.id join zayav_status on z.zayav_status_id = zayav_status.id where zs.dogovor_sostav_id = ds.id and zayav_status.is_good = 1) AS sum_zayav,
		(select max(plat.d) from zayav_sostav zs join plat on zs.zayav_id = plat.zayav_id where zs.dogovor_sostav_id = ds.id) AS max_plat_d,
		d.period_place_id AS period_place_id,
		d.period_finish_id AS period_finish_id 
		FROM 
		dogovor_sostav ds 
		join dogovor d on ds.dogovor_id = d.id 
		join fp_article a on ds.fp_article_id = a.id
		left join fin_source fs on ds.fin_source_id = fs.id
		left join contragent ctgnt on d.contragent_id = ctgnt.id
		";
		if (user_has_zfo()) {
			$zfo_sql = user_zfo_id_str();
			$fin_source_list_sql = array_to_sql_string(user_own_fin_sources());
			$sql .= " WHERE (d.zfo_id in ($zfo_sql) OR ds.fin_source_id IN ($fin_source_list_sql)) ";
		}
		
		$res = sql_query($sql);
		$rows = sql_rows_fix_numeric($res);
		foreach ($rows as &$row) {
			$sum_zayav = 0;
			if (!empty($row['sum_zayav'])) {
				$sum_zayav = $row['sum_zayav'];
			}
			$row['sum_ost'] = $row['amount'] - $sum_zayav;
		}
		return $rows;
	}

	public static function query_virt_zayav_dog($filter) {
		
		$period_formula = "min(CONCAT(p.year, if(p.m is not null, CONCAT('-' , lpad(p.m,2,'0')),'')))";
	
		$sql = "select z.id AS id, z.zfo_id AS zfo_id, z.dogovor_id AS dogovor_id, z.plat_type_id AS plat_type_id, 
		$period_formula period_y_m,
		sum(zs.sum) AS sum, z.d AS d, YEAR(z.d) AS y,
		z.reestr_d AS reestr_d, z.contragent_id AS contragent_id, z.contragent_fio AS contragent_fio, z.notes AS notes, z.zayav_status_id AS zayav_status_id
		from zayav z 
		left join zayav_sostav zs on z.id = zs.zayav_id 
		left join period p on zs.period_id = p.id 
		WHERE true ";
		
		if (user_has_zfo()) {
			$zfo_sql = user_zfo_id_str();
			$fin_source_list_sql = array_to_sql_string(user_own_fin_sources());
			$sql .= " AND (z.zfo_id in ($zfo_sql) OR zs.fin_source_id IN ($fin_source_list_sql)) ";
		}
		
		if (array_key_exists('dogovor_id',$filter)) {
			$sql .=  " AND z.dogovor_id = '". sql_escape($filter['dogovor_id']) . "'";
		}

		if (array_key_exists('y',$filter)) {
			$sql .=  " AND YEAR(z.d) = '". sql_escape($filter['y']) . "'";
		}
		
		$sql .= " group by z.id order by z.id desc";
		$res = sql_query($sql);
		return sql_rows_fix_numeric($res);		
	}

	public static function query_virt_smeta_income($filter) {
		// ПЕРЕДЕЛАТЬ ДОБАВИТЬ fin_source_id в smeta_income!!!!
		/*
		$sum_nds =  "round(si.amount /(100+nds) *nds,2)";
		$sql = "SELECT si.smeta_id, smeta.zfo_id, smeta.fp_year_id, sf.fin_source_id, si.fp_article_income_id,  si.amount,
					si.nds,  $sum_nds  amount_nds,
					si.amount -IFNULL($sum_nds,0) amount_wo_nds
					FROM smeta_income si 
					INNER JOIN smeta on si.smeta_id = smeta.id
					INNER JOIN smeta_fin_source sf ON si.smeta_id = sf.smeta_id
					";
		*/
		$sql = "SELECT
			  si.smeta_id,
			  smeta.zfo_id,
			  smeta.fp_year_id,
			  si.fin_source_id,
			  si.fp_article_income_id,
			  si.amount,
			  si.nds,
			  ROUND(si.amount / (100 + si.nds) * si.nds, 2) AS amount_nds,
			  si.amount - IFNULL(ROUND(si.amount / (100 + si.nds) * si.nds, 2), 0) AS amount_wo_nds,
			  si.amount1,si.amount2,si.amount3,si.amount4,si.amount5,
			  si.amount6,si.amount7,si.amount8,si.amount9,si.amount10,
			  si.amount11,si.amount12,si.amount_next
			FROM smeta_income si
			  INNER JOIN smeta
			    ON si.smeta_id = smeta.id
			";

		$res = sql_query($sql);
		$rows = sql_rows_fix_numeric($res);
		return $rows;
	}
	
	public static function query_virt_smeta_limit($filter) {
		$flds = ['smeta_id','fp_year_id', 'zfo_id', 'fin_source_id','fp_article_id','fp_object_id','itogo'];
		$new_rows = [];
		$building_rows = query_virt_smeta_building($filter);
		$smeta_rows = query_virt_smeta_month_rep($filter);
		
		foreach($building_rows as $row) {
			$new_row = [];
			foreach($flds as $fld) {
				if ($fld == 'itogo') {
					$new_row['amount'] = $row[$fld];
				} else {					
					$new_row[$fld] = $row[$fld];
				}
			}
			$new_rows[] = $new_row;
		}
		foreach ($rows as $row) {
			
		}
	}
	


	

	
	public static function query_virt_smeta_month_rep($filter) {
		$w =[];
		if (isset($filter['smeta_id'])) {
			$w[] = 'ss.smeta_id = ' . (int) $filter['smeta_id'];
		}
		$sql = "SELECT ss.smeta_id id,ss.id sostavid, smeta.description, ifnull(ss.zfo_id,smeta.zfo_id) zfo_id, not_in_pfhd, smeta.fp_year_id, 
			ss.fin_source_id,fs.booker_fin_source, ss.fp_article_id, ss.fp_object_id, 
					ss.amount itogo,
					ss.amount - (ss.amount1+ss.amount2+ss.amount3+ss.amount4+ss.amount5+ss.amount6+ss.amount7+ss.amount8+ss.amount9+ss.amount10+ss.amount11+ss.amount12) as amount0,
					ss.amount1,ss.amount2,ss.amount3,ss.amount4,ss.amount5,ss.amount6,ss.amount7,ss.amount8,ss.amount9,ss.amount10,ss.amount11,ss.amount12,
					ss.natural_indicator_id,
					ss.quantity,
					ss.price,
					ni.unit_id,
					ss.description
					FROM smeta_sostav ss 
					INNER JOIN smeta on ss.smeta_id = smeta.id
					LEFT JOIN fin_source fs ON ss.fin_source_id = fs.id	
					LEFT JOIN natural_indicator ni ON ss.natural_indicator_id = ni.id
					WHERE true ";
		if ($w) {
			$sql .=  " AND " . implode(' AND ' ,$w );
		}
		
		if (user_has_zfo()) {
			$zfo_sql = user_zfo_id_str();
			$sql .= " AND ifnull(ss.zfo_id,smeta.zfo_id)  in ($zfo_sql)  ";
		}

		$res = sql_query($sql);
		$pre_rows = sql_rows_fix_numeric($res);
		$rows = [];
		foreach ($pre_rows as $r) {
			if ($r['fin_source_id']) {
				$rows[] = $r;
			} else {
				/*
				if ($r['percent_budjet']) {
					$row = self::_smeta_multiple($r,$r['percent_budjet']);
					$row['fin_source_id'] = 1; // БЮДЖЕТ
					$rows[] = $row;
				}
				
				if ($r['percent_exbudjet']) {
					$row = self::_smeta_multiple($r,$r['percent_exbudjet']);
					$row['fin_source_id'] = 2; // ВНЕБЮДЖЕТ
					$rows[] = $row;
				}				
				
				if (100-$r['percent_budjet']-$r['percent_exbudjet']) {					
					$row = self::_smeta_multiple($r,100-$r['percent_budjet']-$r['percent_exbudjet']);
					// fin_sourc оставляем пустым
					$rows[] = $row;
				}
				*/
			}
		}
		return $rows;
	}
	
	
	private static function _smeta_multiple($row,$percent) {
		$row['itogo'] = round($row['itogo'] * $percent/100,2);
		for ($i=0; $i<=12; $i++) {
			$fld = 'amount' . $i;
			$row[$fld] =  round($row[$fld] * $percent/100,2);;
		}
		return $row;
	}
	
	public static function query_v_fp_article_limited($filter) {
		/* Подводим итоги	*/
		$sql = "SELECT id, name, parent_id, shifr, kosgu, prim, okved_id, vid_rashodov_id, kvr, fp_object_type_id, pfhd_article_2016_id, pfhd_article_2017_id, asu_pfhd_expense_direction_id, asu_pfhd_institution_program_id from fp_article";
		if (user_has_zfo()) {
			$zfo_str = user_zfo_id_str();

			$rows = sql_rows("select fp_article_id from fp_article_zfo where zfo_id in ($zfo_str)");
			$fp_ids = [];
			foreach($rows as $row) {
				$fp_ids[] = $row['fp_article_id'];
			}
			if (!$fp_ids) {
				$fp_ids = [-10];
			}
			while (true) {
				$in_str = implode(",",$fp_ids);
				$tsql = "select distinct parent_id as id from fp_article where parent_id is not null and parent_id not in ($in_str) and id in ($in_str)";
				$rows = sql_rows($tsql);
				if (count($rows)==0) break;
				foreach($rows as $row) {
					$fp_ids[] = $row['id'];
				}
			}
			$sql .=  " where id in (".implode(",",$fp_ids).")";
		}
		$rows = sql_rows_fix_numeric(sql_query($sql));
		foreach ($rows as &$row) {
			$row['leaf'] = (sql_get_value('count(id)', 'fp_article', "parent_id='" . $row['id'] ."'") == 0);
		}		
		return $rows;		
	}
	
	public static function query_денежные_обязательства($filter) {
		require_once($_SERVER['DOCUMENT_ROOT'] . '/modules/c1.php');
		$kvd =  (isset($filter['KVD'])) ? $filter['KVD'] : null;
		$y = (isset($filter['y'])) ? $filter['y'] : 2016;
		return C1::денежные_обязательства($kvd,$y);
	}
	
	public static function pfhd_rashod_sql($plan_period_id) {
		// Переделать на pfhd_period
		$plan_period = sql_get_array('plan_period',"id='$plan_period_id'");
		$pfhd_article_fld  = 'pfhd_article_' . $plan_period['fld_suffix'] . '_id' ;
		if (!array_key_exists($pfhd_article_fld,sql_table_columns('fp_article'))) {
			die("В таблице fp_article нет поля $pfhd_article_fld");
		}
		$years = [];
		for ($y=$plan_period['y_from']; $y<=$plan_period['y_to']; $y++) {
			$years[] = $y;
		}
		$years_in_sql = implode(',',$years);

		
		$where = ' true ';
		
		if (user_has_zfo()) {
			$fin_sources_sql = array_to_sql_string(user_own_fin_sources());
			$zfo_sql = user_zfo_id_str();
			$where = " (d.zfo_id in ($zfo_sql) OR ds.fin_source_id in ($fin_sources_sql)) ";
		}
		
		$sql = "SELECT ds.dogovor_id,
		period.year as pfhd_y,
		d.dogovor_status_id, d.plan_grafic_id, 
		d.fp_year,
		d.zfo_id,
		d.document_type_id,
		d.subject,
		d.contragent_id,contragent.inn,d.dogovor_num,d.d,
		sum(ds.amount) as sum,
		d.date_start,d.date_end,
		ds.fp_article_id,
		fp_article.kvr,
		fp_article.kosgu,
		fp_article.$pfhd_article_fld AS pfhd_article_id,
		fs.booker_fin_source
		FROM dogovor_sostav ds 		
			join dogovor d on ds.dogovor_id = d.id 
			join document_type dt on d.document_type_id = dt.id 
			join period on ds.period_id = period.id 
			join fin_source fs on ds.fin_source_id = fs.id 
			join dogovor_status status ON d.dogovor_status_id = status.id 
			join fp_article ON ds.fp_article_id = fp_article.id
			left join contragent on d.contragent_id = contragent.id 
		WHERE status.use_pfhd = 1 AND dt.use_pfhd = 1 AND period.year in ($years_in_sql) AND $where
			
		GROUP BY dogovor_id,ds.fp_article_id,fs.booker_fin_source,period.year
		";
		return $sql;
		
	}

	public static function query_virt_pfhd($y = null) {
		// параметры для вызова из pivot - Вероятно переделать pivot!
		$y = isset($_GET['plan_period']) ? (int) $_GET['plan_period'] : 2017 ;
		
		$sql = static::pfhd_rashod_sql($y);
		$rows = sql_rows_fix_numeric(sql_query($sql));
		return $rows;
	}
	
	
	public static function query_virtual_zfo_limit() {
		$DOG_STATUS_ON_EDIT = 1;
		$where = " true ";
		if (user_has_zfo()) {
			$fin_sources_sql = array_to_sql_string(user_own_fin_sources());
			$zfo_sql = user_zfo_id_str();
			$where = " (zfo_id in ($zfo_sql) OR fin_source_id in ($fin_sources_sql)) ";
		}
		$sql = "SELECT d.fp_year,d.zfo_id, ds.fin_source_id,
			0 as sum_limit, sum(ds.amount) AS sum_amount
				FROM dogovor_sostav ds join dogovor d on ds.dogovor_id = d.id join dogovor_status status ON d.dogovor_status_id = status.id 
				WHERE (status.use_limit = 1 OR status.id = $DOG_STATUS_ON_EDIT) AND $where
				GROUP BY d.fp_year,d.zfo_id,ds.fin_source_id
			UNION ALL 
			SELECT fp_year, zfo_id,fin_source_id,
					sum as sum_limit, 0 as sum_amount
			FROM zfo_limit
			WHERE $where ";
			
		/* Подводим итоги	*/
		$sql = "SELECT fp_year,zfo_id,fin_source_id,sum(sum_limit) sum_limit, sum(sum_amount) sum_amount, sum(sum_limit) - sum(sum_amount) sum_diff
		FROM ($sql) pre 
		GROUP by fp_year,zfo_id,fin_source_id";
		
		$rows = sql_rows_fix_numeric(sql_query($sql));
		return $rows;
	}
	
	public static function query_virt_smeta_limit_compare() {
		$sql = "
			SELECT sf.smeta_id,sf.fin_source_id,sf.amount_limit as sum_limit, 0 as sum_amount 
			FROM smeta_fin_source sf 
			UNION ALL 
			SELECT ss.smeta_id,ss.fin_source_id, 0 as sum_limit, amount as sum_amount 
				FROM smeta_sostav ss
			";
			
		/* Подводим итоги	*/
		$sql = "SELECT pre.smeta_id,s.fp_year_id,s.zfo_id,pre.fin_source_id,
		sum(pre.sum_limit) sum_limit, 
		sum(pre.sum_amount) sum_amount,
		sum(pre.sum_limit)- sum(pre.sum_amount) sum_diff
		FROM ($sql) pre 
		INNER JOIN smeta s ON pre.smeta_id = s.id
		GROUP by pre.smeta_id,pre.fin_source_id";
		$rows = sql_rows_fix_numeric(sql_query($sql));
		return $rows;
	}
	
	
	public static function query_virtual_738_diff() {
		$first_ver = request_numeric_val('ver_1',-1);
		$second_ver = request_numeric_val('ver_2',-1);
		// убрал временно kvr
		$sql_pre = "select * from 
		(select dogovor_id,kosgu,kvr,booker_fin_source,fin_source_type_id, sum AS sum_was, 0 AS sum_now 
		from stat_738 where (stat_738_ver_id = '$first_ver') 
		union all 
		select dogovor_id,kosgu,kvr,booker_fin_source,fin_source_type_id, 0 AS sum_was, sum AS sum_now 
		from stat_738 where (stat_738_ver_id = '$second_ver') 
		) union_pre";
		/*(select if((count(stat_738.dogovor_id) = 0),1,0) 
				from stat_738 where ((stat_738.stat_738_ver_id = '$first_ver') 
				and (dogovor.id = stat_738.dogovor_id))) AS is_new
				*/
	// убрал временно pre.kvr из select и group by
		$sql = "select pre.dogovor_id,dogovor.fp_year AS fp_year,
				dogovor.zfo_id AS zfo_id,
				dogovor.dogovor_num,dogovor.d AS d,
				pre.booker_fin_source,
				pre.kosgu,
				pre.kvr,
				pre.fin_source_type_id,
				sum(pre.sum_was) AS sum1,
				sum(pre.sum_now) AS sum2,
				(ifnull(sum(pre.sum_now),0) - ifnull(sum(pre.sum_was),0)) AS sum_dif,
				dogovor.date_start,
				dogovor.date_end,
				dogovor.subject AS subject,
				contragent.name AS name,
				contragent.inn AS inn 
				from ((($sql_pre) pre left join dogovor on((pre.dogovor_id = dogovor.id))) 
				left join contragent on((dogovor.contragent_id = contragent.id))) 
				group by pre.dogovor_id,pre.booker_fin_source,pre.kosgu,pre.kvr,pre.fin_source_type_id
				having (ifnull(sum(pre.sum_was),0) <> ifnull(sum(pre.sum_now),0))";
		$res=sql_query($sql);
		return sql_rows_fix_numeric($res);
	}   
	
	public static function table_columns($table) {
		$cols=parent::table_columns($table);
		foreach ($cols as $name=>$vals) {
			$is = (substr($name,-4)=='code');
			$cols[$name]['is_select_code'] = $is;
			if ($is) {
				$cols[$name]['select_code_table'] = substr($name,0,strlen($name)-5);
			}
		}
        
        foreach ($cols as $name=>$dummy) {
            if ($table_1c = self::isRef1c($name)) {
                $opts = [];                
                $res = self::query1Ctable($table_1c);
                while($row = mssql_fetch_array($res,MSSQL_ASSOC)) {
                    $key = bin2hex($row['Ссылка']);
                    $opts[$key] = $row['Наименование'];
                }
                $cols[$name]['options'] = $cols[$name]['edit_options'] = $opts;
            } elseif ($enum = self::isEnum1c($name)) {
				$cols[$name]['options']  = $cols[$name]['edit_options'] = OneC::enum_options($enum);
				//var_dump($cols[$name]);
			}
        }
        
		return $cols;
	}

    private static function isEnum1c($name) {
		return self::hasSuffix($name,self::ColumnSuffixEnum1C);
    }	
    
    private static function isRef1c($name) {		
		return self::hasSuffix($name,self::ColumnSuffix1C);
    }
	
	private static function hasSuffix($name,$suffix) {
	    if (substr($name,-strlen($suffix)) == $suffix) {
            return substr($name,0,-strlen($suffix));
        };
        return null;
	}
	
	protected static function arrayFromTable($table,$actual=false) {
		if ($table=='fp_issue') {	
            $res=static::arrayFromSql("SELECT id,concat(shifr,' /',short_name,'/')  FROM fp_issue order by shifr ");
		} elseif ($table=='month') {
			$res=static::arrayFromSql("select id, name from month order by id");
		} elseif ($table=='place_period' || $table=='exec_period' ) {
			$res=static::arrayFromSql("select id, name from period order by name");
		} else {
			$res=parent::arrayFromTable($table,$actual);
		}
		return $res;
	}
	
	public static function query($table,$filter=array(),$order_by='',$order_desc=false) {
		/*
		if ($table == 'user' && !is_admin()) {
			throw new Exception('Нет прав просмотра таблицы!');
		}
		*/
		$rows = parent::query($table,$filter,$order_by,$order_desc);
		foreach ($rows as &$row) {
			if ($table == 'zfo') {
				$use = in_array($row['id'],user_zfos()) ? 1 : 0;
			} else {
				$use = 1;
			}
			$row['use_in_editor'] = $use; 
		}
		if (array_key_exists('use_in_editor',$filter)) {
			$new_rows = [];
			foreach($rows as $r) {
				if ($r['use_in_editor'] == $filter['use_in_editor']) {
					$new_rows[] = $r;
				}
			}
			$rows = $new_rows;
		}
		self::_post_process_rows($table,$rows);
		return $rows;
	}
	
	private static function _post_process_rows($table,&$rows) {
		$has_zfo = user_has_zfo();
		$zfos = $has_zfo ? user_zfo_ids() : [];
		if (in_array($table,['smeta','dogovor'])) {
			// Наличие флага _may_Edit или _may_approve позволяет видеть вкладки "Доход", "Фин Итоги", "Файлы", "Здания"
			foreach($rows as &$row) {
				$row['_may_edit'] = !$has_zfo || in_array($row['zfo_id'],$zfos);
				if ($table == 'smeta') {
					$row['_may_approve'] = !$has_zfo;
				}
			//	$row['_may_approve_smeta_sostav'] = !$has_zfo;
			}
		}
	}
	
	protected static function sql_where($table,$filter) {
		$w = parent::sql_where($table,$filter);
        $wa = array();        
		$c = sql_table_columns($table);
		$fin_sources = user_own_fin_sources();
		$fin_source_list = $fin_sources ? array_to_sql_string($fin_sources) : '-1';
		if (user_has_zfo()) {
			$zfo_list = user_zfo_id_str();
			if ( $table == 'dogovor' ) {
				$wa[] = "(zfo_id in ($zfo_list) or id in (select dogovor_id from dogovor_sostav WHERE fin_source_id in ($fin_source_list)))";
			} elseif ( $table == 'smeta' ) {
				$wa[] = "(zfo_id in ($zfo_list) or id in (select smeta_id from smeta_sostav WHERE zfo_id in ($zfo_list)))";
			} elseif ( $table == 'smeta_sostav' || $table == 'ver_smeta_sostav') {
				$wa[] = "(zfo_id in ($zfo_list) or smeta_id in (select id from smeta WHERE zfo_id in ($zfo_list)))";
			} elseif ( $table == 'smeta_income' || $table == 'ver_smeta_income') {
				$wa[] = "(smeta_id in (select id from smeta WHERE zfo_id in ($zfo_list)))";
	        } else if($table == 'zayav' ) {
	        	$wa[] = "(zfo_id in ($zfo_list) or id in (select zayav_id from zayav_sostav WHERE fin_source_id in ($fin_source_list)))";
	        } else if (isset($c['zfo_id']) && isset($c['fin_source_id'])) {
	        	$wa[] = "(zfo_id in ($zfo_list) or fin_source_id in ($fin_source_list))";
			} else if ( $table == 'dogovor_sostav' ) {
				$wa[] = " (dogovor_id in (select id from dogovor where zfo_id in ($zfo_list)) or fin_source_id in ($fin_source_list)) ";
			} else if (isset($c['zfo_id']) && $table !== 'fin_source') {
				// просмотр источников доступен весь
				$wa[]="zfo_id in ($zfo_list)";	
			}
		}
        $w_add=implode(' and ',$wa); 
        if(strlen($w) >0) {
            if (strlen($w_add)>0) $w.=' and '. $w_add;
        } else {
            $w = $w_add;
        }
		return $w;
	}
}

$DB =  'ReestrDB'; // имя класс для работы с базой