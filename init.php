<?php
//die("Сервис временно не работает! ");
if (isset($_SERVER['SERVER_ADDR']) && $_SERVER['SERVER_ADDR'] == '127.0.0.1') 
	define ('SHARED_PHP_PATH','home/dekanat/modules');
else
	define ('SHARED_PHP_PATH','/home/dekanat/modules');

error_reporting(E_ALL);
date_default_timezone_set('Europe/Moscow');
require_once SHARED_PHP_PATH . '/sess/session.php';
start_session_reestr();
$path = $_SERVER['DOCUMENT_ROOT'];
set_include_path(get_include_path() . PATH_SEPARATOR . $path);
require 'config.php';
require 'modules/SystemMessage.php';
require 'modules/error_init.php';
require 'modules/sql_connect.php';
require 'modules/sql.php';
//require 'modules/mssql.php'; // MSSQL используется для ТЕЗИСа и 1с(?)
require 'modules/functions.php';
require 'modules/reestr_functions.php';
require 'modules/utils.php';
require 'modules/pfhd.php';
require 'mailing/mailer.php';
require 'td/newtd.php';
require 'reestr_db.php'; //$DB='ReestrDB';
if (isset($_SERVER['SERVER_ADDR']) && $_SERVER['SERVER_ADDR'] == '127.0.0.1') 
	require_once SHARED_PHP_PATH . '/mssql/mssql_srv.php';
else
	require SHARED_PHP_PATH .'/OneC/init.php'; // Инициализация свзя с 1с

/* 
Permission сейчас 5.11.2016 используется только в 
save.php (функция заглушка UserMaySave (всегда возвращает True) и 
cash_dynamics.php 
*/
require 'classes/Users/PermissionBase.php';
require 'classes/Users/Permission.php';
/*
Не используется ЧАВ 05.11.2016
require 'models/BaseModel.php';
require 'models/ContragentModel.php';
*/
//Таки используется (АВД)
//require 'classes/Users/User.php';

/// ????
//require '/td/layout.php';

db_connect();
//db_connect_bank();


$url = $_SERVER['REQUEST_URI'];
current_user_kod();
if (substr($url,0,strlen('/login.php')) <> '/login.php' && substr($url,0,strlen('/lost_pass.php')) <> '/lost_pass.php') {
	if (!isset($_SESSION['user_id']) or time_out()) {
		go_exit();
	}
	else
		//AVD
		setcookie('last_user_id', $_SESSION['user_id'], time() + 3600 /* 1час */ * 24 * 10 /*10 дней*/);
}

if (is_layout_request()) {
    layout_save();
	exit();	
}
?>