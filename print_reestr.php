<?php
require 'init.php';
//ini_set('include_path', ini_get('include_path').';'.dirname(__FILE__) . '/PHPExcel/');
date_default_timezone_set('UTC');
include_once __DIR__ . '/PHPExcel/Classes/PHPExcel.php';
//include_once 'functions.php';

//Для вытаскивания только валидной даты
//Посколку в GET из фильтра может попасть пэрент, если в нем лежит только один чаилд
function validDateMatch($dates) {
	$validDates = array();
	for($i = 0; $i < count($dates); $i++) {
		$date = $dates[$i];
		if (preg_match("/\d{4}\-\d{2}-\d{2}/", $date)) {
			$validDates[] = $date;
		}
	}
	return $validDates;
}

if (!isset($_GET['f_reestr_d'])) {
    require 'template/header.php';
    echo "<div style='font-size: 20px; font-weight: bold; text-align: center; margin-top: 30px; color: red;'>Ошибка! Для печати реестра необходимо установить фильтр на дату!</div>";
    die();
}

$reestr_d = validDateMatch($_GET['f_reestr_d']);
if(count($reestr_d) > 1) {
	require 'template/header.php';
    echo "<div style='font-size: 20px; font-weight: bold; text-align: center; margin-top: 30px; color: red;'>Ошибка! Для печати реестра необходимо выбрать только одну дату!</div>";
    die();
}

// исправить !!!
//AVD
$dd = is_array($_GET['f_reestr_d']) ? $_GET['f_reestr_d'][0]: $_GET['f_reestr_d']; //было [0]
$d = date_parse($dd);

$templatename=__DIR__.'/reestr_plat.xlsx';

$objPHPExcel = PHPExcel_IOFactory::load( $templatename );
$objPHPExcel->setActiveSheetIndex(0);
$sheet = $objPHPExcel->getActiveSheet();
//echo'<pre>'; var_dump($d); die();
$rtext='Реестр безналичных платежей СПбГЭУ на '.$d['day'].' '.sql_get_value('name_rod_p','month','id='.$d['month']).' '.$d['year'].' г.';

//echo'<pre>'; var_dump($rtext); die();

$sheet->setCellValueByColumnAndRow(0,6,$rtext);
/*
$objPHPExcel->getProperties()->setCreator("Alex Vasiliev");
$objPHPExcel->getProperties()->setLastModifiedBy("Alex Vasiliev");
$objPHPExcel->getProperties()->setTitle("Office 2007 XLSX Test Document");
$objPHPExcel->getProperties()->setSubject("Office 2007 XLSX Test Document");
$objPHPExcel->getProperties()->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.");
*/
$i=10;

$cols=array('id', 'contragent_id', 's', 'kosgu', 'kvr', 'zfo_id', 'fp_article_id', 'booker_fin_source');
$table='v_reestr';
$T = newTD($table);
$data=$T->data;
$columns=$T->columns;

foreach($data as $row ){
	$i++;
	$sheet->insertNewRowBefore($i, 1);
	$sheet->setCellValueByColumnAndRow(0,$i,$i-10);
	$j=0;
	foreach($cols as $col){
		$j++;
		$sheet->setCellValueByColumnAndRow($j, $i, ($columns[$col]['type']=='double') ? $row[$col] : $T->col_formated($columns[$col],$row[$col]));
	}
}

$sheet->removeRow(10,1);
//$sheet->setCellValueByColumnAndRow(4,$i,'=СУММ((E10:E10))');
$sheet->setCellValue("D$i", "=SUM(D10:D".($i-1).")");

//$sheet->setCellValueByColumnAndRow(5, 6, $ostatok1);
//$sheet->setCellValueByColumnAndRow(5, 7, $ostatok2);
$filename=sys_get_temp_dir() .'/'. 'plat'.time().'.xlsx';
 
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

//echo'<pre>'; var_dump($objWriter); die();

$objWriter->save($filename);

header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="'.basename($filename).'"');
header('Cache-Control: max-age=0');

//print_r($rtext);

/*header('Content-Description: File Transfer');
header('Content-Type: application/vnd.openxmlformats-officedocument.wordprocessingml.document');
header('Content-Disposition: attachment; filename='.basename($filename));
header('Content-Transfer-Encoding: binary');
header('Expires: 0');
header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
header('Pragma: public');
header('Content-Length: '.filesize($filename));*/
@readfile($filename);
?>