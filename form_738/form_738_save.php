<?php 
include $_SERVER['DOCUMENT_ROOT'] . '/init.php';

$ver_table = 'stat_738_ver';
$data_table = 'stat_738';
$errors = array();

$name = request_val('name',null);
$prim = request_val('prim','');

if (!$name) {
	$errors[] = "Версия не сохранена. Не введено имя версии.";
} else {
	sql_query("START TRANSACTION");
	sql_query("INSERT INTO $ver_table (name, prim) VALUES ('$name','$prim')");
	$last_insert_id = sql_last_id();
	$sql = "INSERT INTO stat_738 (
		stat_738_ver_id, pfhd_y, dogovor_id, dogovor_status_id, nomer, 
		document_type_id, contragent_id, sum, fp_year, booker_fin_source, fin_source_type_id, kosgu, kvr, d)  
	SELECT '$last_insert_id', 
	p.year, 
	ds.dogovor_id, 
	d.dogovor_status_id, 
	d.dogovor_num as nomer,
	d.document_type_id as dogovor_type_id,
	d.contragent_id, sum(ds.amount) as sum,
	d.fp_year, 
	fs.booker_fin_source, 
	fs.fin_source_type_id,
	fpa.kosgu, fpa.kvr, 
	d.d 
	FROM dogovor_sostav ds 
	INNER JOIN dogovor d ON ds.dogovor_id = d.id 
	INNER JOIN period p ON ds.period_id = p.id 
	LEFT JOIN fp_article fpa ON ds.fp_article_id = fpa.id 
	LEFT JOIN fin_source fs ON ds.fin_source_id = fs.id 
	WHERE d.dogovor_status_id in (SELECT id FROM dogovor_status WHERE use_stat_738 = 1) 
	GROUP BY ds.dogovor_id,p.year, fpa.kosgu,fpa.kvr,fs.booker_fin_source,fs.fin_source_type_id
	";
	sql_query($sql);
	sql_query("COMMIT");
}

if($cnt = count($errors)) {
	$_SESSION['SYSTEM_MESSAGE'] = '';
	for($i = 0; $i < $cnt; $i++) {
		$_SESSION['SYSTEM_MESSAGE'] .= $errors[$i] . "<br />";
	}
} else {
	$_SESSION['SYSTEM_MESSAGE_SUCCESS'] = "Версия успешно сохранена";
	header("Location: " . $_SERVER['HTTP_REFERER']);
}

?>