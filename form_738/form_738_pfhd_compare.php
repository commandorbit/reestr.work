<?php
$title = 'Форма 738';
define('KOSGU', 1);
define('KVR', 2);
require $_SERVER['DOCUMENT_ROOT'] . '/init.php';
require $_SERVER['DOCUMENT_ROOT'] . '/template/header.php';
require_once '/home/dekanat/modules/controls/controls.php';

$controls = new controls;

$ver_id = (isset($_GET['ver_id'])) ? $_GET['ver_id'] : false;
$versions = sql_to_assoc("SELECT id, name FROM stat_738_ver ORDER BY tstamp DESC");

$type = (isset($_GET['type']) && $_GET['type'] == KVR) ? 'kvr' : 'kosgu';
$types = ['kosgu'=>'КОСГУ','kvr'=>'КВР'];

$fp_article = (isset($_GET['fp_article'])) ? $_GET['fp_article'] : false;
$fp_articles = sql_to_assoc("select distinct $type,$type from fp_article order by $type");

$booker_fin_source = (isset($_GET['booker_fin_source'])) ? $_GET['booker_fin_source'] : false;
$booker_fin_sources = sql_to_assoc("
SELECT distinct 
	v.booker_fin_source, v.booker_fin_source as name FROM v_stat_738 v
UNION SELECT 
	p.booker_fin_source, p.booker_fin_source as name FROM plat p 
ORDER BY name ");

function getSumTable($ver_id,$type,$fp_article,$booker_fin_source) {
	// не понятно что хотели с годом.
	$year = 2016;
    $next_year = 2016 + 1;
	$w = "";
	if ($fp_article) $w .= " AND $type = '".sql_escape($fp_article)."'";
	if ($booker_fin_source) $w .= " AND booker_fin_source = '".sql_escape($booker_fin_source)."'";
	
	$sql = " 
	SELECT $type, booker_fin_source, sum(plan) as plan, sum(fact) as fact, sum(diff) as diff, 100 * sum(fact) / sum(plan) as percent
	FROM (
		SELECT $type, booker_fin_source, 0 as plan, s as fact, -s as diff 
			FROM plat
			WHERE plat.d>='$year' and plat.d<'$next_year'
		UNION ALL 
		SELECT v.$type as $type, v.booker_fin_source, v.sum as plan, 0 as fact, v.sum as diff 
			FROM v_stat_738 v 
			WHERE v.stat_738_ver_id = '$ver_id'
		) pre 
	WHERE true $w 
	GROUP BY $type, booker_fin_source";
	$rows = sql_rows($sql);
	return $rows;
}

?>

<div class="header-link">
<h1 style="color: #fff;"><?=$title?></h1>
</div>
<div class="container" style="text-align: center; margin: 20px auto;">
	<form id="reportform" method="get">
		<?php 
		$controls->print_label('ver_id','Версия 738');
		$controls->print_select('ver_id',$versions,$ver_id,['onchange'=>'submit();']);
		/*
		$controls->print_label('pfhd_ver_id','Версия ПФХД');
		$controls->print_select('pfhd_ver_id',$pfhd_versions,$pfhd_ver_id,['onchange'=>'submit();']);
		*/
		$controls->print_label('type','сформировать по');
		$controls->print_select('type',$types,$type,['onchange'=>'submit();']);
		$controls->print_label('fp_article','равному');
		$controls->print_select('fp_article',$fp_articles,$fp_article,['onchange'=>'submit();']);
		$controls->print_label('booker_fin_source','ИФ');
		$controls->print_select('booker_fin_source',$booker_fin_sources,$booker_fin_source,['onchange'=>'submit();']);
		echo '<br><br>';
		$controls->print_submit('refresh','Обновить')
		?>
	</form>
	<?php if($ver_id): ?>
		<br><a href="#" class="pseudo-link" onclick="toExcel(); return false;">Скачать в формате XLS</a>
	<?php endif; ?>
</div>

<?php 

$labels = array(
	'kosgu'=>['label'=>'КОСГУ'],
	'booker_fin_source'=>['label'=>'ИФ бух.'],
	'plan'=>['label'=>'План','format'=>'sum'],
	'fact'=>['label'=>'Факт','format'=>'sum'],
	'diff'=>['label'=>'Разница','format'=>'sum'],
	'kvr'=>['label'=>'КВР'],
	'percent'=>['label'=>'% вып.','format'=>'sum']
);

if ($type && $ver_id) {
	$ver_id = sql_escape($ver_id);
	echo '<div id="table">';
	if($type == 'kosgu') {
		echo Utils::toHTML(getSumTable($ver_id, 'kosgu', $fp_article, $booker_fin_source), $labels);
	} else if($type == 'kvr') {
		echo Utils::toHTML(getSumTable($ver_id , 'kvr', $fp_article, $booker_fin_source), $labels);
	}
	echo '</div>';
}

?>

<script>
	function toExcel() 
	{
		var table = document.querySelector('#table');
        download('form_738' + '_' + new Date().getTime() + '.xls', table.outerHTML);
    }
	
	function download(filename, text) 
	{
        var URL = window.URL || window.webkitURL || window.mozURL || window.msURL;
        var blob = new Blob([text]);
        var url = URL.createObjectURL(blob);
        var pom = document.createElement('a');
        pom.setAttribute('href', url);
        pom.setAttribute('download', filename);
        if (document.createEvent) 
		{
            var event = document.createEvent('MouseEvents');
            event.initEvent('click', true, true);
            pom.dispatchEvent(event);
        }
        else 
		{
            pom.click();
        }
    }
</script>