<?php

include_once $_SERVER['DOCUMENT_ROOT'] . '/init.php';

$requisits_rows = sql_rows("SELECT * FROM param");
$requisits = array();
foreach($requisits_rows as $row) {
	$requisits[$row['name']] = $row['value'];
}

if(isset($_GET['download'])) 
{
	$year = NULL;
	$ver = NULL;
	$where = "";
	if (isset($_GET['year'])) $year = sql_escape($_GET['year']);
	if (isset($_GET['ver'])) $ver = sql_escape($_GET['ver']);
	if (!$year || !$ver)
		die('Необходимо указать год и версию.');
}

$where = "WHERE gz.year = '$year' AND gz.ver = '$ver'";

//NB!!!
//echo "<hr>$where<hr>";
//

/*
$dogovor_gz_rows = sql_rows("
SELECT dgz.dogovor_id, dgz.pos_num, dgz.contract_max_price, dgz.zayav_guarantee, dgz.contract_guarantee, dgz.contract_execution_stages, dgz.periodicity, 
dgz.change_reason_info, gpw.name as placing_way_name, dgzo2.okved2_code as okved, dogovor.subject, dogovor.demand, fpa.kvr, dgp.okpd, p_start.name as period_place,
 p_finish.name as period_finish 
 FROM dogovor_gz dgz 
 LEFT JOIN dogovor_gz_okved2 dgzo2 ON dgzo2.dogovor_gz_id = dgz.id 
 LEFT JOIN dogovor_gz_placing_way dgpw on dgpw.dogovor_gz_id = dgz.id 
 LEFT JOIN gz_placing_way gpw on dgpw.gz_placing_way = gpw.code 
 LEFT JOIN dogovor on dgz.dogovor_id = dogovor.id 
 LEFT JOIN dogovor_sostav ds on dgz.dogovor_id = ds.dogovor_id 
 LEFT JOIN fp_article fpa on ds.fp_article_id = fpa.id 
 LEFT JOIN dogovor_gz_position dgp ON dgz.id = dgp.dogovor_gz_id 
 LEFT JOIN period p_start on dogovor.period_place_id = p_start.id 
 LEFT JOIN period p_finish on dogovor.period_finish_id = p_finish.id 
$where
GROUP BY dgz.dogovor_id ORDER BY dgz.pos_num");
*/

$dogovor_gz_rows = sql_rows("
SELECT
  dgzs.id,
  gz.year,
  gz.ver,
  dgz.ver AS dgz_ver,
  gz.description,
  dgz.dogovor_id,
  dgz.pos_num,
  dgz.contract_max_price,
  dgz.zayav_guarantee,
  dgz.contract_guarantee,
  dgz.contract_execution_stages,
  dgz.periodicity,
  dgz.change_reason_info,
  GROUP_CONCAT(DISTINCT gpw.name SEPARATOR ', ') AS placing_way_name,
  GROUP_CONCAT(DISTINCT dgzo2.okved2_code SEPARATOR ', ') AS okved,
  GROUP_CONCAT(DISTINCT dogovor.subject SEPARATOR ', ') AS subject,
  GROUP_CONCAT(DISTINCT dogovor.demand SEPARATOR ', ') AS demand,
  (SELECT
      fpa.kvr
    FROM dogovor_sostav ds
      LEFT OUTER JOIN fp_article fpa
        ON ds.fp_article_id = fpa.id
    WHERE dgz.dogovor_id = ds.dogovor_id
    ORDER BY ds.amount DESC
    LIMIT 1) AS kvr,
  GROUP_CONCAT(DISTINCT dgp.okpd SEPARATOR ', ') AS okpd,
  p_start.name AS period_place,
  p_finish.name AS period_finish,
  GROUP_CONCAT(DISTINCT gz_okei.localName SEPARATOR ', ') AS localName,
  GROUP_CONCAT(DISTINCT gz_okei.localSymbol SEPARATOR ', ') AS localSymbol,
  SUM(dgp.quantity) AS quantity,
  SUM(dgp.sum) AS sum
FROM dogovor_gz dgz
  INNER JOIN gz_44_sostav dgzs
    ON dgzs.dogovor_gz_id = dgz.id
  INNER JOIN gz_44 gz
    ON gz.id = dgzs.gz_44_id
  LEFT OUTER JOIN dogovor_gz_okved2 dgzo2
    ON dgzo2.dogovor_gz_id = dgz.id
  LEFT OUTER JOIN gz_placing_way gpw
    ON gpw.code = dgz.gz_placing_way_code
  LEFT OUTER JOIN dogovor
    ON dgz.dogovor_id = dogovor.id
  LEFT OUTER JOIN dogovor_gz_position dgp
    ON dgz.id = dgp.dogovor_gz_id
  LEFT OUTER JOIN period p_start
    ON dogovor.period_place_id = p_start.id
  LEFT OUTER JOIN period p_finish
    ON dogovor.period_finish_id = p_finish.id
  LEFT OUTER JOIN gz_okei
    ON dgp.gz_okei = gz_okei.code
/*WHERE gz.year = 2016 AND gz.ver = 1*/
$where
GROUP BY dgzs.id,
         dgz.dogovor_id
ORDER BY gz.year, gz.ver, dgz.dogovor_id, dgz.pos_num 
");

//AVD
$final_sql = "
SELECT
  gz_44.id,
  gz_44.year,
  gz_44.ver,
  gz_44.date_sign,
  gz_44.date_publish,
  gz_44_final_positions.less100,
  gz_44_final_positions.less400,
  gz_44.description,
  MAX(dogovor_gz.contract_max_price) AS contract_max_price
FROM gz_44
  INNER JOIN gz_44_final_positions
    ON gz_44_final_positions.gz_44_id = gz_44.id
  INNER JOIN gz_44_sostav
    ON gz_44_sostav.gz_44_id = gz_44.id
  INNER JOIN dogovor_gz
    ON gz_44_sostav.dogovor_gz_id = dogovor_gz.id
WHERE gz_44.ver = $ver AND gz_44.year = $year
GROUP BY gz_44.year,
         gz_44.ver
";
$finals = sql_rows($final_sql);

//echo '<hr>'. count($dogovor_gz_rows) . '<hr>';

function splitDeadlineDate($str) {
	$pattern = '(\d{4}\-\d{2})';
	$match = preg_match($pattern,$str,$matches);
	if($match) {
		$match_result = $matches[0];
		$result = explode('-', $match_result);
		return $result[1] . '.' . $result[0];
	}	
	return $str;
}

ob_start();
require dirname(__FILE__) . '/fz_44_template.html';
$output = ob_get_clean();

if(isset($_GET['download'])) {
	/**/
	header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
	header('Content-Disposition: attachment;filename="fz_44_'.date('dm').'.xls"');
	header('Cache-Control: max-age=0');
	/**/
	//echo '<html xmlns="http://www.w3.org/1999/xhtml">';
	echo $output;
} else {
	return $output;
}
?>