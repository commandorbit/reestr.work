<?php 
	$ver = (isset($_GET['ver']) && sql_get_value('id', 'dogovor_gz', "dogovor_id = '$id' AND ver = '".(int)sql_escape($_GET['ver'])."'")) ? (int)sql_escape($_GET['ver']) : NULL;
    $dogovor_gz = get_dogovor_gz($id, $ver); 
    $dogovor_gz_positions = (isset($dogovor_gz['id'])) ? get_dogovor_gz_positions($dogovor_gz['id']) : array(); 
	$dogovor_gz_vers = dogovor_gz_vers($id);
	
	//Если договор гз ид входит в какую-либо версию gz_44, то смотрим на статус gz_44 и если редактировать нельзя, то disable все кнопки с пометкой
	$dogovor_gz_may_edit = (isset($dogovor_gz['id'])) ? dogovor_gz_may_edit($dogovor_gz['id']) : false;
	
	if(!$dogovor_gz_may_edit && !isset($_GET['new'])) {
		header("Location: /dogovor.php?id=".$id."&page=dogovor_gz&ver=".$ver);
	}
	
    //echo '<hr>'; var_dump($dogovor_gz_vers); echo '<hr>'; 
    //die();
	$disabled = (!isset($_GET['new']) && !$dogovor_gz_may_edit) ? 'disabled' : '';
	$title = (!$dogovor_gz_may_edit) ? "Действие невозможно, т.к. не найдено версии план-графика в статусе 'Черновик' или вы пыаетесь редактировать не актуальную версию" : "";
?>

<script>
    var dogovor_gz_values = <?= json_encode($dogovor_gz) ?>;
    var positions_values = <?= json_encode($dogovor_gz_positions) ?>;
</script>

<!-- Вкладка гос. закупки -->
    <h1>Договор (Гос. закупки)</h1>
	<?php if(!empty($dogovor_gz_vers)): ?>
    <div style="margin: 10px 0">
		<a href="#" id="show-ver-select" class="pseudo-link">Версия № <?=$dogovor_gz['ver']?> от <?= $dogovor_gz['create_ts'] ?></a>
		<div id="ver-select" class="hidden-box">
			<form method="GET">
				<!-- Передаю id и page, чтобы уже существующие параметры GET не слетали -->
				<input type="hidden" name="id" value="<?=$id?>">
				<input type="hidden" name="page" value="<?=$page?>">
				<input type="hidden" name="ver" value="<?=$ver?>">
				<label>
					Выбрать версию:
					<input type="hidden" name="action" value="edit">
					<select name="ver">
						<?php foreach($dogovor_gz_vers as $ver): ?>
							<?php $selected = (isset($_GET['ver']) && $_GET['ver'] == $ver['ver']) ? "selected" : ""; ?>
							<option <?=$selected?> value="<?= $ver['ver'] ?>"><?=$ver['ver']?></option>
						<?php endforeach; ?>
					</select>
					<input type="submit" value="OK">
				</label>
			</form>
		</div>
    </div>
	<div style="margin: 10px 0">
		<form method="POST" action="/gz/dogovor_gz_ver.php">
			<input type="hidden" name="dogovor_id" value="<?=$id?>">
			<input type="submit" <?=$disabled?> title="<?=$title?>" value="Удалить последнюю версию" name="delete_ver" onclick="return confirm('Вы действительно хотите удалить версию?');">
			<input type="submit" <?=$disabled?> title="<?=$title?>" value="Создать новую версию" name="create_ver">
		</form>
	</div>
	<?php endif; ?>
    <form method="POST" action="/save_dogovor_gz.php" id="dogovor-gz-form" class="custom-buttons">
        <input type="hidden" name="dogovor_id" value="<?=$id?>">
		<input type="hidden" name="ver" value="<?=(isset($_GET['ver'])) ? $_GET['ver'] : 1?>">
        <h3 style="text-align: left">Общие сведения:</h3>
        <div class="gz-fields">
            <div class="form-controls dogovor_fields clearfix" style="background: #E5E7EA; ">
				<div class="form-groups cust-group">
					<label>№ позиции: <input type="text" data-required="true" style="width: 100px" name="pos_num"  value="<?=(isset($dogovor_gz['pos_num'])) ? $dogovor_gz['pos_num'] : NULL ?>"></label>
				</div>
                <div class="form-groups cust-group">
                    <label>
                        Классификация по ОКВЭД2 (<a href="#" onclick="addOkvedField(this)" class="pseudo-link">Добавить</a>): 
                        <div class="clearfix">
							<div data-name="okved" data-required="true" data-control-of="treeplugin" class="cust-select half">
							</div>
                        </div>
                    </label>
                </div>
                <div class="form-groups cust-group">
                    <label>
                        Способ определения поставщика: 
                        <div>
                            <div data-name="gz_placing_way" data-control-of="treeplugin" class="cust-select" >
                            </div>
                        </div>
                    </label>
                </div>
                <div class="form-groups cust-group">
                    <label>Ориентировочная начальная стоимость контракта: <input type="text" data-required="true" name="contract_max_price" value="<?=(isset($dogovor_gz['contract_max_price'])) ? $dogovor_gz['contract_max_price'] : $contract_max_pice ?>"></label>
                </div> 
				<div class="form-groups cust-group">
                    <label>Размер обеспечения заявки: <input type="text" data-required="true" name="zayav_guarantee" value="<?=(isset($dogovor_gz['zayav_guarantee'])) ? $dogovor_gz['zayav_guarantee'] : NULL ?>"></label>
                </div> 
				<div class="form-groups cust-group">
                    <label>Размер обеспечения исполнения контракта: <input type="text" data-required="true" name="contract_guarantee" value="<?=(isset($dogovor_gz['contract_guarantee'])) ? $dogovor_gz['contract_guarantee'] : NULL ?>"></label>
                </div> 
                <div class="form-groups cust-group">
                    <label>Сроки исполнения отдельных этапов контракта: <input type="text" data-required="true" name="contract_execution_stages" placeholder="ежеквартально" value="<?=(isset($dogovor_gz['contract_execution_stages'])) ? $dogovor_gz['contract_execution_stages'] : NULL ?>"></label>
                </div>
                <div class="form-groups cust-group">
                    <label>Переодичность поставки товаров (выполнения работ, оказания услуг): <input name="periodicity" data-required="true" type="text" placeholder="ежеквартально" value="<?=(isset($dogovor_gz['periodicity'])) ? $dogovor_gz['periodicity'] : NULL ?>"></label>
                </div>
                <div class="form-groups cust-group">
                    <label>Преимущества: 
                        <div>
                            <div data-name="gz_preferences" data-control-of="treeplugin" class="cust-select">
                            </div>
                        </div>
                    </label>
                </div>
				<div class="form-groups cust-group">
                    <label>Основание для внесения изменений: <? //echo '<hr>'; var_dump($dogovor_gz); '<hr>'; ?>
                        <div>
                            <div data-name="gz_change_reason" data-control-of="treeplugin" class="cust-select">
                            </div>
                        </div>
                    </label>
                </div>
				<div class="form-groups cust-group">
					<label>Дополнительная инф-я по внесению изменений: <input type="text" name="change_reason_info" value="<?=(isset($dogovor_gz['change_reason_info'])) ? $dogovor_gz['change_reason_info'] : NULL ?>"></label>
				</div>
            </div>
        </div>

        <h3 style="text-align: left">Позиции план-графика: <button type='button' class="btn add-btn" onclick="addPosition(this); return false;" style="width: 150px; margin-left: 0;"><i class="fa fa-plus-square" aria-hidden="true"></i> Добавить позицию</button></h3>
        
        <? if(empty($dogovor_gz_positions)): ?>
        <div class="gz-position">
            <div class="form-controls dogovor_fields clearfix" style="background: #E5E7EA; ">
                    <div class="form-groups cust-group">
                        <label>
                            ОКПД2 
                            <div>
                            <div data-required="true" data-name="okpd" data-control-of="treeplugin" class="cust-select">
                            </div>
                            </div>
                        </label>
                    </div>
                    <div class="form-groups cust-group">
                        <label>
                            ОКЕИ 
                            <div>
                            <div data-required="true" data-name="gz_okei" data-control-of="treeplugin" class="cust-select">
                            </div>
                            </div>
                        </label>
                    </div>
                    <div class="form-groups cust-group">
                        <label>Шт. <input type="text" name="quantity[]" value="<?=(isset($position['quantity'])) ? $position['quantity'] : 1 ?>"></label>
                    </div>
                    <div class="form-groups cust-group">
                        <label>Сумма <input type="text" name="sum[]" value="<?=(isset($position['sum'])) ? $position['sum'] : NULL ?>"></label>
                    </div>
                </div>
                <div class="delete"></div>
            </div>
            <div class="action_sidebar">
                <p class="save_parent clearfix"><input style="position:relative; left:-20px;" <?=$disabled?> title="<?=$title?>" type="button" name="save_dogovor_gz" onclick="sendGzForm(); return false;" value="Сохранить"></p>
            </div>  
        </div>    
        <?php else: ?>
            <?php foreach($dogovor_gz_positions as $position): ?>
                <div class="gz-position">
                    <div class="form-controls dogovor_fields clearfix" style="background: #E5E7EA; ">
                            <div class="form-groups cust-group">
                                <label>
                                    ОКПД2 
                                    <div>
                                    <div data-required="true" data-name="okpd" data-control-of="treeplugin" class="cust-select">
                                    </div>
                                    </div>
                                </label>
                            </div>
                            <div class="form-groups cust-group">
                                <label>
                                    ОКЕИ 
                                    <div>
                                    <div data-required="true" data-name="gz_okei" data-control-of="treeplugin" class="cust-select">
                                    </div>
                                    </div>
                                </label>
                            </div>
                            <div class="form-groups cust-group">
                                <label>Кол-во <input type="text" name="quantity[]" value="<?=(isset($position['quantity'])) ? $position['quantity'] : 1 ?>"></label>
                            </div>
                            <div class="form-groups cust-group">
                                <label>Сумма <input type="text" name="sum[]" value="<?=(isset($position['sum'])) ? $position['sum'] : NULL ?>"></label>
                            </div>
                        </div>
                        <div class="delete"></div>
                    </div> 
            <?php endforeach; ?>
            <div class="action_sidebar">
                        <p class="save_parent clearfix"><input <?=$disabled?> title="<?=$title?>" style="margin-right: 20px;" type="button" name="save_dogovor_gz" onclick="sendGzForm(); return false;" value="Сохранить"></p>
                    </div>  

        <?php endif; ?> 
    </form>

<script>
    var validateErrors = [];
    var wrap = document.querySelector('.content-pane');

    function initTreeplugin(wrap) {
        var controls = wrap.querySelectorAll('[data-control-of="treeplugin"]');
        for(var i = 0; i < controls.length; i++) {
            var control = controls[i];
            _initSingleTreeplugin(control);
        }
    }

    function _initSingleTreeplugin(control) {
        var dataTable = control.getAttribute('data-name');
        var obj = {};
        var checkboxes = (dataTable == 'gz_preferences') ? true : false;
        obj = libUnicorn.TreePlugin.create(control, false, {opened: false, checkboxes: checkboxes});
        obj.container.style.width = (control.offsetWidth) + 'px';
        obj.container.style.height = (obj.container.offsetHeight * 1.5) + 'px';
        //var controlValue = control.getAttribute('data-value');
        new resizable(obj.container, {directions: ["southeast"]});
        getOptions(obj, dataTable);
    }

    function sendGzForm() {
        var form = document.querySelector('#dogovor-gz-form');
		var has_errors = false;
        var treePlugins = document.querySelectorAll('[data-control-of="treeplugin"]');
        for(var i = 0; i < treePlugins.length; i++) {
            var trp = treePlugins[i];
            var obj = libUnicorn.getControl(trp);
            var selected = obj.getSelected();
			var dataTable = trp.getAttribute('data-name'); //
			//alert(selected.length); 
            for(var j = 0; j < selected.length; j++) {                
                var hidden_input = document.createElement('input');
                hidden_input.type = 'hidden';
                hidden_input.name = dataTable + '[]';
                hidden_input.value = selected[j];
                form.appendChild(hidden_input);
            }
			if (trp.dataset.required) {
				trp.classList.toggle("attention",selected.length==0);
				if (selected.length==0) {
					has_errors = true;
				}
			}
			//var filled_trp = document.querySelector('input[name="'+dataTable+'[]"]');
			//if (0) //AVD
			/*
			if (dataTable != 'gz_change_reason' && selected.length == 0) {
				alert('Не все поля заполнены ' + dataTable);
				return false;
			}
			*/
		
			
        }
		var inps = form.querySelectorAll("input[type='text']");
		for (var j = 0; j < inps.length; j++) {
			if (inps[j].dataset.required) {
				inps[j].classList.toggle("attention",inps[j].value=='');
				if (inps[j].value=='') {
					has_errors = true;
				}
			}
		}
		if (!has_errors) {
			//console.log(form);
			form.submit();
		} 	else {
			console.log(has_errors);
			alert('Не все обязательные поля заполнены.')
		}
        return false;
    }

    function addPosition(el) {
		var contentPanel = document.querySelector('.content-pane');
        var gzPosition = document.querySelector('.gz-position');
        var newPos = gzPosition.cloneNode(true);
        var deleteIcon = newPos.querySelector('.delete');
        deleteIcon.style.display = 'block';
        console.log(newPos);
        contentPanel.appendChild(newPos);
		$('html,body').animate({ scrollTop: newPos.offsetTop }, 'slow');
        initTreeplugin(newPos);
        addDeletePositionHandler(newPos);
    } 

    function addDeletePositionHandler(position) {
        position.addEventListener('click', function(e) {
            var target = e.target || e.srcElement;
            if(target.classList.contains('delete')) {
                target.parentNode.parentNode.removeChild(target.parentNode);
            }
        });
    }

    function getOptions(obj, table) {
        var url = '/gz/get_options.php';

        $.ajax({
            type: "POST",
            url: url,
            data: {table: table},
            async: true,
            success: function(data) {
                //console.log(data);
                data = JSON.parse(data);
                obj.setOptions(data);

                //Сетим для dogovor_gz
                var dogovor_gz_controls = wrap.querySelectorAll('[data-name="'+table+'"]');
                dogovorSetSelected(dogovor_gz_controls, table);
				
                //Сетим для позиций
                var positions = wrap.querySelectorAll('.gz-position');
                for(var i = 0; i < positions.length; i++) {
                    var position = positions[i];
                    var controls = position.querySelectorAll('[data-name="'+table+'"]');
					for(var j = 0; j < controls.length; j++) {
						var object = libUnicorn.getControl(controls[j]);
						if (positions_values) // AVD
						if (positions_values[0]) // AVD
						var values = positions_values[0][table];
						if (values) // AVD
						if(values != undefined) {
							var vals_array = [];
							for(var key in values) {
								if (values[key]) // AVD
								vals_array.push(values[key].code);
							}
							object.setSelected(vals_array);
						}
					}
                }
            }
        });
    }

    function dogovorSetSelected(controls, table) {
		if(!controls) return false;
        for(var i = 0; i < controls.length; i++) {
            var control = controls[i];
			//Объект триплагина
            var object = libUnicorn.getControl(control);
			//Значения полей (сетим в 16-й строке файла)
            var values = dogovor_gz_values[table];
            if (values) //AVD
            if (values != undefined) {				
				var vals_array = [];
				for(var key in values) {
					if (values[key]) //AVD
					vals_array.push(values[key].code);
				}
				object.setSelected(vals_array);
            }
        }
    }

    function addOkvedField(el) {
        var selectsBox = el.parentNode.querySelector('div');
        var control = document.createElement('div');
        control.setAttribute('data-name', 'okved');
        control.setAttribute('data-control-of', 'treeplugin');
        control.classList.add('cust-select', 'half');
        if(selectsBox) selectsBox.appendChild(control);
        _initSingleTreeplugin(control);
    }

	(function() {
		var showVerSelect = document.querySelector('#show-ver-select');
		var verSelect = document.querySelector('#ver-select');
		if(showVerSelect) {
			showVerSelect.addEventListener('click', function() {
				verSelect.classList.toggle('hidden-box');
			});
		}
	})();
	
    initTreeplugin(document.querySelector('.content-pane'));
</script>

<style>
    #dogovor-gz-form .mst-tag {
        max-width: 30%;
    }
</style>