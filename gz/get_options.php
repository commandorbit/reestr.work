<?php

require $_SERVER['DOCUMENT_ROOT'] . '/init.php';

if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) && !empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
	if(isset($_POST['table'])) {
		$table = $_POST['table'];
		$columns = sql_table_columns($table);
		//Строка, которая в sql запросе тянет код пэрэнта или вместо пэрэнта ставит NULL
		$parent_col = (isset($columns['parent_code'])) ? 'parent_code' : 'NULL';
		switch($table) {
			case 'okpd': 
			case 'okved':
			case 'gz_okei':
			case 'gz_placing_way':
				echo json_encode(sql_rows("select code as id,concat('/',`code`,'/ ',`name`) AS `name`, $parent_col as parent_id from $table"));
				break;
			case 'gz_preferences':
				echo json_encode(sql_rows("select code as id,name, $parent_col as parent_id from gz_preferences"));
				break;
			case 'gz_change_reason':
				echo json_encode(sql_rows("select id,name, $parent_col as parent_id from gz_change_reason"));
				break;
			default:
				echo json_encode(array('status'=>'NOTOK', 'message'=>'Произошла ошибка загрузкки данных, обратитесь к разработчикам'));
				break;
		}
	}
}