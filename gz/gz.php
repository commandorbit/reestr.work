<?php 
$title = 'План-график (44-ФЗ)';
include $_SERVER['DOCUMENT_ROOT'] . '/init.php';
include $_SERVER['DOCUMENT_ROOT'] . '/template/header.php';
//$gz_table_html = include dirname(__FILE__) . '/fz_44.php';

$ver = NULL;
$year = NULL;
$gz_44 = NULL;

//var_dump($_REQUEST); echo '<hr>' . $_SERVER['HTTP_REFERER'] . '<hr>';

/*
if (isset($_GET['show_ver']) && isset($_GET['year']) && isset($_GET['ver'])) 
{
	$year = sql_escape($_GET['year']);
	$ver = sql_escape($_GET['ver']);
	$gz_44 = get_gz_44($year, $ver);
}
*/

if (isset($_GET['year']))
	$year = sql_escape($_GET['year']);
if (isset($_GET['ver']))
	$ver = sql_escape($_GET['ver']);

/* AVD
$ver_rows = sql_rows("SELECT ver FROM gz_44");
$ver_year_rows = sql_rows("SELECT year FROM gz_44 GROUP BY year");
*/

$y = date("Y"); $ver_year_rows = sql_rows("SELECT DISTINCT year FROM gz_44 UNION SELECT $y ORDER BY year");
//$ver_year_rows = sql_rows("SELECT DISTINCT year FROM gz_44 ORDER BY year");

$year_is_ok = false;	
foreach($ver_year_rows as $row)		
    if ($year == $row['year'])
		$year_is_ok = true;

if (!$year_is_ok)
	foreach($ver_year_rows as $row)
		$year = $row['year'];

$ver_rows = sql_rows("SELECT DISTINCT ver FROM gz_44 WHERE YEAR = '$year' UNION SELECT 1 AS ver ORDER BY ver");
//$ver_rows = sql_rows("SELECT DISTINCT ver FROM gz_44 WHERE YEAR = '$year' ORDER BY ver");

$ver_is_ok = false;	
foreach($ver_rows as $row)		
    if ($ver == $row['ver'])
		$ver_is_ok = true;

if (!$ver_is_ok)
	foreach($ver_rows as $row)		
		$ver = $row['ver'];

$gz_44 = get_gz_44($year, $ver);

//echo "<hr>$year $ver<hr>";
//echo "<hr>"; var_dump($gz_44); echo "<hr>";

//AVD
//update_final
if (isset($_REQUEST['update_final'])) 
{
	//echo "<hr>$year $ver<hr>";
	//echo "<hr>"; var_dump($gz_44); echo "<hr>";
	//echo "<hr>"; var_dump($_REQUEST); echo "<hr>";
	$gz_44_id = $gz_44['id'];
	$gz_44_status_id = sql_get_value('gz_44_status_id', 'gz_44', "id = '$gz_44_id'");
	$may_edit = sql_get_value('may_edit', 'gz_44_status', "id = '$gz_44_status_id'");
	$status_name = mb_strtolower(sql_get_value('name', 'gz_44_status', "id = '$gz_44_status_id'"));
	if ($may_edit)
	{		
		if (isset($_REQUEST['sum_less100'])) 	
		{
			sql_query("UPDATE gz_44_final_positions SET less100 = '${_REQUEST['sum_less100']}' WHERE gz_44_id = '$gz_44_id'");
			//header("Location: " . $_SERVER['SCRIPT_NAME'] . "?year=$year&ver=$ver#less100");
		}
		if (isset($_REQUEST['sum_less400'])) 	
		{
			sql_query("UPDATE gz_44_final_positions SET less400 = '${_REQUEST['sum_less400']}' WHERE gz_44_id = '$gz_44_id'");
			//header("Location: " . $_SERVER['SCRIPT_NAME'] . "?year=$year&ver=$ver#less400");
		}
	}
	else
	{
		$_SESSION['SYSTEM_MESSAGE'] = "Нельзя редактировать финальные позиции, так как версия план-графика (id = $gz_44_id) $status_name";
		header("Location: " . $_SERVER['SCRIPT_NAME'] . "?year=$year&ver=$ver");
	}
}

//AVD
//Удаляем запись
if (isset($_REQUEST['deleteone'])) 
{
	$id = $_REQUEST['delete_id'];
	$gz_44_id = sql_get_value('gz_44_id', 'gz_44_sostav', "id = '$id'");
	$gz_44_status_id = sql_get_value('gz_44_status_id', 'gz_44', "id = '$gz_44_id'");
	$may_edit = sql_get_value('may_edit', 'gz_44_status', "id = '$gz_44_status_id'");
	$status_name = mb_strtolower(sql_get_value('name', 'gz_44_status', "id = '$gz_44_status_id'"));
	//echo "$id --> $gz_44_id --> $gz_44_status_id --> $may_edit --> $status_name <hr>";
	if ($may_edit)
	{		
		$sql = "DELETE FROM gz_44_sostav WHERE id = '$id'";
		sql_query($sql);	
		$_SESSION['SYSTEM_MESSAGE_SUCCESS'] = "Запись ${_REQUEST['delete_id']} успешно удалена";
	}
	else
		$_SESSION['SYSTEM_MESSAGE'] = "Запись ${_REQUEST['delete_id']} удалить нельзя так как версия план-графика (id = $gz_44_id) $status_name";
	header("Location: " . $_SERVER['HTTP_REFERER']);
}	

//Создаем версию
if(isset($_POST['create_ver'])) 
{
	$year = sql_escape($_POST['year']);
	$description = (trim($_POST['description']) != '') ? sql_escape($_POST['description']) : NULL;
	$ver = sql_get_value('ver', 'gz_44', "year = '$year' ORDER BY create_ts DESC LIMIT 1");
	$ver = (!$ver) ? 1 : $ver + 1;
	//sql_query('START TRANSACTION;');
	sql_query("INSERT INTO gz_44 (year, ver, description) VALUES ('$year', '$ver', '$description')");
	$gz_44_id = sql_last_id();
	if($gz_44_id) 
	{
		//$gz_rows = sql_rows("SELECT id FROM dogovor_gz");
 		//$gz_rows = sql_rows("SELECT id FROM dogovor_gz WHERE id NOT IN (SELECT dogovor_gz_id FROM gz_44_sostav)");
		$gz_rows = sql_rows("
			SELECT gz.dogovor_id, gz.ver, gz.id FROM dogovor_gz gz
			JOIN (
			SELECT
			  dogovor_id,
			  MAX(ver) mver
			FROM dogovor_gz
			GROUP BY dogovor_id
			ORDER BY dogovor_id, ver
			) mv ON mv.dogovor_id = gz.dogovor_id AND mv.mver = gz.ver
			JOIN dogovor d ON d.id = gz.dogovor_id
			WHERE d.concluded_ku = 0
			ORDER BY gz.dogovor_id
		");
		foreach($gz_rows as $row) 
		{
			$gz_id = $row['id'];
			sql_query("INSERT INTO gz_44_sostav (gz_44_id, dogovor_gz_id) VALUES ('$gz_44_id', '$gz_id')");
		}
		sql_query("INSERT INTO gz_44_final_positions (gz_44_id) VALUES ('$gz_44_id')");
		$_SESSION['SYSTEM_MESSAGE_SUCCESS'] = 'Версия успешно создана';
		//header("Location: " . $_SERVER['HTTP_REFERER']);
		header("Location: " . $_SERVER['SCRIPT_NAME'] . "?year=$year&ver=$ver");
	}
	//sql_query('COMMIT;');
} 
//Подписываем версию
else if(isset($_POST['date_sign'])) 
{
	$date = sql_escape($_POST['date_sign']);
	if(!is_null($ver)) 
	{
		if($gz_44['date_sign'] != 'NULL') 
		{
			$gz_44_id = $gz_44['id'];
			sql_query("UPDATE gz_44 SET date_sign = '$date', gz_44_status_id = '2' WHERE id = '$gz_44_id'");
			$_SESSION['SYSTEM_MESSAGE_SUCCESS'] = 'Версия успешно подписана';
			header("Location: " . $_SERVER['HTTP_REFERER']);
		}
	}
} 
//Публикуем версию
else if(isset($_POST['date_publish'])) 
{
	$date = sql_escape($_POST['date_publish']);
	if(!is_null($ver)) 
	{
		if($gz_44['date_publish'] != 'NULL') 
		{
			$gz_44_id = $gz_44['id'];
			sql_query("UPDATE gz_44 SET date_publish = '$date', gz_44_status_id = '3' WHERE id = '$gz_44_id'");
			$_SESSION['SYSTEM_MESSAGE_SUCCESS'] = 'Версия успешно опубликована';
			header("Location: " . $_SERVER['HTTP_REFERER']);
		}
	}
} 
//Удаляем версию
else if(isset($_POST['delete_ver'])) 
{
	if(!is_null($ver) && $ver > 0) 
	{
		$gz_44_id = $gz_44['id'];
		sql_query("DELETE FROM gz_44_final_positions WHERE gz_44_id = '$gz_44_id'");
		sql_query("DELETE FROM gz_44_sostav WHERE gz_44_id = '$gz_44_id'");
		sql_query("DELETE FROM gz_44 WHERE id = '$gz_44_id'");
		$_SESSION['SYSTEM_MESSAGE_SUCCESS'] = 'Версия успешно удалена';
		//header("Location: " . $_SERVER['HTTP_REFERER']);
		header("Location: " . $_SERVER['SCRIPT_NAME']);
	} else {
		$_SESSION['SYSTEM_MESSAGE'] = "Вы не можете удалить единственную версию $ver";
		header("Location: " . $_SERVER['HTTP_REFERER']);
		//header("Location: " . $_SERVER['SCRIPT_NAME']);
	}
} 
//Отменяем подписание
else if(isset($_POST['rollback_sign_ver'])) 
{
	$gz_44_id = $gz_44['id'];
	sql_query("UPDATE gz_44 SET date_sign = NULL, gz_44_status_id = 1 WHERE id = '$gz_44_id'");
	$_SESSION['SYSTEM_MESSAGE_SUCCESS'] = 'Подписание успешно отменено';
	header("Location: " . $_SERVER['HTTP_REFERER']);
}
//Отменяем публикацию
else if(isset($_POST['rollback_publish'])) 
{
	$gz_44_id = $gz_44['id'];
	sql_query("UPDATE gz_44 SET date_publish = NULL, gz_44_status_id = 2 WHERE id = '$gz_44_id'");
	$_SESSION['SYSTEM_MESSAGE_SUCCESS'] = 'Публикация успешно отменена';
	header("Location: " . $_SERVER['HTTP_REFERER']);
}

?>

<h1><?= $title ?></h1>
<!--
<h3>Настройки</h3>
-->
<div class="container" style="background-color: #fff; margin-top: 20px; padding: 20px;">
	<div class="row">
		<div class="col-md-8">
			<form name="plan_version" method="GET">
				<label>
					План-график по <b>44-ФЗ</b> на
					<select name="year" onchange = "document.forms['plan_version'].submit();">
						<?php 
						foreach($ver_year_rows as $row): 						
							//$selected = (isset($_GET['year']) && $row['year'] == $_GET['year']) ? "selected" : ""; 
							$selected = ($year && $row['year'] == $year) ? "selected" : ""; 
							?>
							<option <?=$selected?> value="<?=$row['year']?>"><?=$row['year']?></option>
						<?php endforeach; ?>
					</select>
					год, по версии:
					<select name="ver" onchange = "document.forms['plan_version'].submit();">
						<?php 
						foreach($ver_rows as $row): 
							$selected = ($ver && $row['ver'] == $ver) ? "selected" : ""; 
						?>
							<option <?=$selected?> value="<?=$row['ver']?>"><?=$row['ver']?></option>
						<?php endforeach; ?>
					</select>
					<? //echo "<hr>$year $ver<hr>" ?>
					<input type="hidden" name="show_ver" value="Выбрать">
					<input type="submit" name="submit_button" value="ОК"> 
					<?php if(!gz_44_isset_draft()): ?>
						<button type='button' id="create-new-ver">Создать новую версию</button>
					<?php endif; ?>
				</label>
			</form>
		</div>
	</div>
	<?php 
	$gz_44 = get_gz_44($year, $ver);
	//echo '<hr>'; var_dump($gz_44); echo '<hr>';
	//if(isset($_GET['show_ver']) && !is_null($gz_44)): 
	//if($year && $ver && !is_null($gz_44)): 
	if(true): 
	?>
	<div class="row" style="margin: 10px 0 0 0">
		<div class="col-md-8">
			<? if(!is_null($gz_44)): //if($year && $ver && !is_null($gz_44)): ?>
			<form method="POST" >			
				<b>Статус выбранной версии :</b> <? echo gz_44_status_name($year, $ver); 
				//AVD NB!!!
				//echo '<hr>'; var_dump($gz_44); echo '<hr>';				
				?>
				<?php if($gz_44['date_sign'] == NULL && $gz_44['date_publish'] == NULL): ?>
				<!-- <input type="submit" name="delete_ver" onclick="retrurn confirm("Вы действительно хотите..."); " value="Удалить версию"> -->
				<button type='button' id="delete-ver-btn">Удалить версию</button>
				<?php endif; ?>
				<?php if($gz_44['date_sign'] != NULL /*&& $gz_44['date_publish'] == NULL*/): ?>
				<!-- <input type="submit" name="rollback_sign_ver" value="Отменить подписание"> -->
				<button type='button' id="ver-unsign-btn">Отменить подписание</button>
				<?php endif; ?>
				<?php if(/*$gz_44['date_sign'] != NULL &&*/ $gz_44['date_publish'] != NULL): ?>
				<!--<input type="submit" name="rollback_publish" value="Отменить публикацию"> -->
				<button type='button' id="ver-unpubl-btn">Отменить публикацию</button>
				<?php endif; ?>
				<?php if($gz_44['date_sign'] == NULL): ?>
				<button type='button' id="ver-sign-btn">Подписать</button>
				<?php endif; ?>
				<?php if($gz_44['date_sign'] != NULL && $gz_44['date_publish'] == NULL): ?>
				<button type='button' id="ver-publish-btn" >Опубликовать</button>
				<!--<button type='button' id="ver-publish-btn">Опубликовать</button> -->
				<?php endif; ?>
			</form>
			<?php endif; ?>
		</div>
		<!--
		-->
	</div>
	<?php endif; ?>
</div><br/>

<?php 
//if(isset($_GET['ver'])): 
//if ($ver && $year): 
if ($gz_44): 
?>

<?php 
$hrefxml = '/gz/create_gz_44_xml.php?download' . "&year=$year&ver=$ver"; 
$hrefxls = '/gz/fz_44.php?download' . "&year=$year&ver=$ver"; 
//NB!!!
//echo "<hr>${_GET['show_ver']} ${_GET['year']} ${_GET['ver']} $href<hr>" 
//
?>

<h1><a href="/gz/gz.php">44-ФЗ <? echo "$year год, версия $ver " ?>(<?=$gz_44['description']?>)</a></h1>
<h4><a class="pseudo-link" href="<?php echo $hrefxls ?>">Скачать в формате XLS</a> | <a class="pseudo-link" href="<?php echo $hrefxml ?>">Скачать в формате XML</a> | <a class="pseudo-link" href="load_okei.php">Загрузка справочника ОКЕИ</a></h4><br/>
<? //=$gz_table_html;
echo include dirname(__FILE__) . '/fz_44.php';
?>
<?php endif; ?>

<!-- Создание новой версии 44-ФЗ -->
<div id="new-ver" title="Создание новой версии" style="display: none;">
	<form method="POST">		
		<div class="form-groups cust-group">
		<label>
			Год: <br/>
			<input type="text" maxlength="4" name="year" value="<?=date('Y')?>">
		</label>
		</div>
		<div class="form-groups cust-group">
			<label>
				Краткое описание: <br/>
				<input type="text" name="description" maxlength="255" placeholder="Описание (необязательно)">
			</label>
		</div>
		<div align="center">
		<input type="submit" name="create_ver" value="Создать">
		<button type='button' id="cancel-create-new-ver">Отмена</button>
		</div>
	</form>
</div>

<!-- Подписание версии 44-ФЗ -->
<div id="ver-sign" title="Подписание" style="display: none;">
	<form method="POST">
		<div class="form-groups cust-group" width=400>
		<!-- <label> -->
			Дата подписания: <input type="text" name="date_sign" value="<?=date('Y-m-d')?>" required>
		<!-- </label> -->
		</div>
		<div align="center">
		<input type="submit" name="ver_sign" value="Подписать">
		<button type='button' id="cancel-ver-sign">Не подписывать</button>
		</div>
	</form>
</div>

<!-- Опубликование версии 44-ФЗ -->
<div id="ver-publish" title="Опубликование" style="display: none;">
	<form method="POST">
		<div class="form-groups cust-group">
		<label>
			Дата опубликования: <br/>
			<input type="text" name="date_publish" value="<?=date('Y-m-d')?>" required>
		</label>
		</div>
		<div align="center">
		<input type="submit" name="ver_publish" value="Опубликовать">
		<button type='button' id="cancel-publish-btn">Отмена</button>
		</div>
	</form>
</div>

<!-- Отмена подписания версии 44-ФЗ (AVD)-->
<div id="ver-unsign" title="Отмена подписания" style="display: none;">
	<form method="POST">
		<div class="form-groups cust-group">
		<label>
			Отменить подписание версии?
		</label>
		</div>
		<div align="center">
		<input type="submit" name="rollback_sign_ver" value="Отменить">
		<button type='button' id="cancel-ver-unsign">Оставить</button>
		</div>
	</form>
</div>

<!-- Отмена публикации версии 44-ФЗ (AVD)-->
<div id="ver-rollback-publish" title="Отмена публикации" style="display: none;">
	<form method="POST">
		<div class="form-groups cust-group">
		<label>
			Отменить публикацию версии?
		</label>
		</div>
		<div align="center">
		<input type="submit" name="rollback_publish" value="Отменить">
		<button type='button' id="cancel-rollback-publish">Оставить</button>
		</div>
	</form>
</div>

<!-- Удаление версии 44-ФЗ (AVD)-->
<div id="delete-ver" title="Удаление версии" style="display: none;">
	<form method="POST">
		<div class="form-groups cust-group">
		<label>
			Удалить версию?
		</label>
		</div>
		<div align="center">
		<input type="submit" name="delete_ver" value="Удалить">
		<button type='button' id="cancel-delete-ver">Оставить</button>
		</div>
	</form>
</div>

<script>
	//alert( 'Привет, Мир!' );
	$(function() 
	{
		//new-ver
		$('[name="year"]').mask('9999', {placeholder:"ГГГГ"});
		$( "#new-ver" ).dialog({ autoOpen: false, modal: true });
		var newVerBtn = document.querySelector('#create-new-ver');
		if (newVerBtn) { 
			newVerBtn.addEventListener('click', function() {
				$( "#new-ver" ).dialog("open"); 
			}); 
		}
		var cancelCreateVerBtn = document.querySelector('#cancel-create-new-ver');
		cancelCreateVerBtn.addEventListener('click', function() {$( "#new-ver" ).dialog("close"); });
		//ver-publish
		$('[name="date_publish"], [name="date_sign"]').mask('9999-99-99', {placeholder:"ГГГГ-ММ-ДД"});
		$("#ver-publish, #ver-sign").dialog({ autoOpen: false, modal: true });
		var verSignBtn = document.querySelector('#ver-sign-btn');
		var verPublishBtn = document.querySelector('#ver-publish-btn');
		if (verPublishBtn) {
			verPublishBtn.addEventListener('click', function() {				
				$( "#ver-publish" ).dialog("open");				
			});
		}
		var cancelPublBtn = document.querySelector('#cancel-publish-btn');
		cancelPublBtn.addEventListener('click', function() { $( "#ver-publish" ).dialog("close"); });
		//ver-sign
		if(verSignBtn) {
			verSignBtn.addEventListener('click', function() {
				$( "#ver-sign" ).dialog("open");
			});
		}
		var cancelVerSignBtn = document.querySelector('#cancel-ver-sign');
		cancelVerSignBtn.addEventListener('click', function() {	$( "#ver-sign" ).dialog("close"); });
		//#ver-unsign
		$( "#ver-unsign" ).dialog({ autoOpen: false, modal: true });
		var verUnSignBtn = document.querySelector('#ver-unsign-btn');
		if (verUnSignBtn) { 
			verUnSignBtn.addEventListener('click', function() {
				$( "#ver-unsign" ).dialog("open"); 
				}); 
		}
		var cancelVerUnSignBtn = document.querySelector('#cancel-ver-unsign');
		cancelVerUnSignBtn.addEventListener('click', function() {$( "#ver-unsign" ).dialog("close"); });
		//ver-rollback-publish
		$( "#ver-rollback-publish" ).dialog({ autoOpen: false, modal: true });
		var verUnPublBtn = document.querySelector('#ver-unpubl-btn');
		if (verUnPublBtn) { 
			verUnPublBtn.addEventListener('click', function() {
				$( "#ver-rollback-publish" ).dialog("open"); 
				}); 
		}
		var cancelVerUnPublBtn = document.querySelector('#cancel-rollback-publish');
		cancelVerUnPublBtn.addEventListener('click', function() {$( "#ver-rollback-publish" ).dialog("close"); });
		//delete-ver
		$( "#delete-ver" ).dialog({ autoOpen: false, modal: true });
		var verUnPublBtn = document.querySelector('#delete-ver-btn');
		if (verUnPublBtn) { 
			verUnPublBtn.addEventListener('click', function() {
				$( "#delete-ver" ).dialog("open"); 
				}); 
		}
		var cancelDelVerBtn = document.querySelector('#cancel-delete-ver');
		cancelDelVerBtn.addEventListener('click', function() {$( "#delete-ver" ).dialog("close"); });
	});
</script>																																																									
