<?php 
	$ver = (isset($_GET['ver']) && sql_get_value('id', 'dogovor_gz', "dogovor_id = '$id' AND ver = '".(int)sql_escape($_GET['ver'])."'")) ? (int)sql_escape($_GET['ver']) : NULL;
    $dogovor_gz = get_dogovor_gz($id, $ver); 
    $dogovor_gz_positions = (isset($dogovor_gz['id'])) ? get_dogovor_gz_positions($dogovor_gz['id']) : array(); 
	$dogovor_gz_vers = dogovor_gz_vers($id);
	
	//Если договор гз ид входит в какую-либо версию gz_44, то смотрим на статус gz_44 и если редактировать нельзя, то disable все кнопки с пометкой
	$dogovor_gz_may_edit = (isset($dogovor_gz['id'])) ? dogovor_gz_may_edit($dogovor_gz['id']) : true;
	
    //var_dump($dogovor_gz_vers);
    //die();
	$disabled = (!$dogovor_gz_may_edit) ? 'disabled' : '';
	$title = (!$dogovor_gz_may_edit) ? "Действие невозможно, т.к. не найдено версии план-графика в статусе 'Черновик' или вы пыаетесь редактировать не актуальную версию" : "";
	/*echo "<pre>";
	var_dump($dogovor_gz);
	echo "</pre>";
	die();*/
?>

<h1>Договор (Гос. закупки)</h1>
	<?php if(!empty($dogovor_gz_vers)): ?>
    <div style="margin: 10px 0">
		<a href="#" id="show-ver-select" class="pseudo-link">Версия № <?=$dogovor_gz['ver']?> от <?= $dogovor_gz['create_ts'] ?></a>
		<div id="ver-select" class="hidden-box">
			<form method="GET">
				<!-- Передаю id и page, чтобы уже существующие параметры GET не слетали -->
				<input type="hidden" name="id" value="<?=$id?>">
				<input type="hidden" name="page" value="<?=$page?>">
				<input type="hidden" name="ver" value="<?=$ver?>">
				<label>
					Выбрать версию:
					<select name="ver">
						<?php foreach($dogovor_gz_vers as $ver): ?>
							<?php $selected = (isset($_GET['ver']) && $_GET['ver'] == $ver['ver']) ? "selected" : ""; ?>
							<option <?=$selected?> value="<?= $ver['ver'] ?>"><?=$ver['ver']?></option>
						<?php endforeach; ?>
					</select>
					<input type="submit" value="OK">
				</label>
			</form>
		</div>
    </div>
	<div style="margin: 10px 0">
		<a href="/dogovor.php?id=<?=$id?>&page=dogovor_gz&action=edit<?=(isset($_GET['ver'])) ? '&ver=' . $_GET['ver'] : '' ?>"><button <?=$disabled?> title="<?=$title?>">Редактировать</button></a>
		<form method="POST" action="/gz/dogovor_gz_ver.php" style="float: left; margin-right: 5px;">
			<input type="hidden" name="dogovor_id" value="<?=$id?>">
			<button name="create_ver" <?= (!gz_44_draft_id()) ? 'disabled' : '' ?> title="<?= (!gz_44_draft_id()) ? 'Не найдено версии в статусе Черновик' : '' ?>">Создать новую версию</button>
		</form>
	</div>
	<?php endif; ?>

<!-- Вкладка гос. закупки -->
<form method="POST" action="/save_dogovor_gz.php" id="dogovor-gz-form" class="custom-buttons">
        <input type="hidden" name="dogovor_id" value="<?=$id?>">
		<input type="hidden" name="ver" value="<?=(isset($_GET['ver'])) ? $_GET['ver'] : 1?>">
        <h3 style="text-align: left">Общие сведения:</h3>
        <div class="gz-fields">
            <div class="form-controls dogovor_fields clearfix" style="background: #E5E7EA; ">
				<div class="form-groups cust-group">
					<label>№ позиции:</label>
					<div>
						<?=(isset($dogovor_gz['pos_num'])) ? $dogovor_gz['pos_num'] : NULL ?>
					</div>
				</div>
                <div class="form-groups cust-group">
                    <label>
                        Классификация по ОКВЭД2: 
                    </label>
					<?php if(isset($dogovor_gz['okved'])): ?>
						<?php foreach($dogovor_gz['okved'] as $item): ?>
						<div>
							<?=$item['name']?>
						</div>
						<?php endforeach; ?>
					<?php else: ?>
						<div>Не указано</div>
					<?php endif; ?>
                </div>
                <div class="form-groups cust-group">
                    <label>
                        Способ определения поставщика: 
                    </label>
					<?php if(isset($dogovor_gz['gz_placing_way'])): ?>
						<?php foreach($dogovor_gz['gz_placing_way'] as $item): ?>
						<div>
							<?=$item['name']?>
						</div>
						<?php endforeach; ?>
					<?php else: ?>
						<div>Не указано</div>
					<?php endif; ?>
                </div>
                <div class="form-groups cust-group">
                    <label>Ориентировочная начальная стоимость контракта:</label>
					<div>
						<?=(isset($dogovor_gz['contract_max_price'])) ? $dogovor_gz['contract_max_price'] : $contract_max_pice ?>
					</div>
                </div> 
				<div class="form-groups cust-group">
                    <label>Размер обеспечения заявки: </label>
					<div><?=(isset($dogovor_gz['zayav_guarantee'])) ? $dogovor_gz['zayav_guarantee'] : NULL ?></div>
                </div> 
				<div class="form-groups cust-group">
                    <label>Размер обеспечения исполнения контракта:</label>
					<div><?=(isset($dogovor_gz['contract_guarantee'])) ? $dogovor_gz['contract_guarantee'] : NULL ?></div>
                </div> 
                <div class="form-groups cust-group">
                    <label>Сроки исполнения отдельных этапов контракта:</label>
					<div>
						<?=(isset($dogovor_gz['contract_execution_stages'])) ? $dogovor_gz['contract_execution_stages'] : NULL ?>
					</div>
                </div>
                <div class="form-groups cust-group">
                    <label>Переодичность поставки товаров (выполнения работ, оказания услуг):</label>
					<div><?=(isset($dogovor_gz['periodicity'])) ? $dogovor_gz['periodicity'] : NULL ?></div>
                </div>
                <div class="form-groups cust-group">
                    <label>Преимущества: 
                    </label>
					<?php if(isset($dogovor_gz['gz_preferences'])): ?>
						<?php foreach($dogovor_gz['gz_preferences'] as $item): ?>
						<div>
							<?=$item['name']?>
						</div>
						<?php endforeach; ?>
					<?php else: ?>
						<div>Не указано</div>
					<?php endif; ?>
                </div>
				<div class="form-groups cust-group">
                    <label>Основание для внесения изменений: 
                    </label>
					<?php if(isset($dogovor_gz['gz_change_reason'])): ?>
						<?php foreach($dogovor_gz['gz_change_reason'] as $item): ?>
						<div>
							<? if ($item !== NULL) echo $item['name']; else echo 'Не указано'; /*var_dump($item);*/ ?>
						</div>
						<?php endforeach; ?>
					<?php else: ?>
						<div>Не указано</div>
					<?php endif; ?>
                </div>
				<div class="form-groups cust-group">
					<label>Дополнительная инф-я по внесению изменений:</label>
					<div>
						<?=(isset($dogovor_gz['change_reason_info'])) ? $dogovor_gz['change_reason_info'] : NULL ?>
					</div>
				</div>
            </div>
        </div>

        <h3 style="text-align: left">Позиции план-графика:</h3>
        
        <? if(empty($dogovor_gz_positions)): ?>
        <div class="gz-position">
            <div class="form-controls dogovor_fields clearfix" style="background: #E5E7EA; ">
                    <div class="form-groups cust-group">
                        <label>
                            ОКПД2 
                        </label>
						<?php if(isset($dogovor_gz_positions['okpd'])): ?>
							<?php foreach($dogovor_gz_positions['okpd'] as $item): ?>
							<div>
								<?=$item['name']?>
							</div>
							<?php endforeach; ?>
						<?php endif; ?>
                    </div>
                    <div class="form-groups cust-group">
                        <label>
                            ОКЕИ 
                        </label>
						<?php if(isset($dogovor_gz_positions['gz_okei'])): ?>
							<?php foreach($dogovor_gz_positions['gz_okei'] as $item): ?>
							<div>
								<?=$item['name']?>
							</div>
							<?php endforeach; ?>
						<?php endif; ?>
                    </div>
                    <div class="form-groups cust-group">
                        <label>Шт.</label>
						<div>
							<?=(isset($position['quantity'])) ? $position['quantity'] : 1 ?>
						</div>
                    </div>
                    <div class="form-groups cust-group">
                        <label>Сумма</label>
						<div>
							<?=(isset($position['sum'])) ? $position['sum'] : NULL ?>
						</div>
                    </div>
                </div>
                <div class="delete"></div>
            </div>  
        </div>    
        <?php else: ?>
            <?php foreach($dogovor_gz_positions as $position): ?>
                <div class="gz-position">
					<div class="form-controls dogovor_fields clearfix" style="background: #E5E7EA; ">
                    <div class="form-groups cust-group">
                        <label>
                            ОКПД2 
                        </label>
						<?php if(isset($position['okpd'])): ?>
							<?php foreach($position['okpd'] as $item): ?>
							<div>
								<?=$item['name']?>
							</div>
							<?php endforeach; ?>
						<?php endif; ?>
                    </div>
                    <div class="form-groups cust-group">
                        <label>
                            ОКЕИ 
                        </label>
						<?php if(isset($position['gz_okei'])): ?>
							<?php foreach($position['gz_okei'] as $item): ?>
							<div>
								<?=$item['name']?>
							</div>
							<?php endforeach; ?>
						<?php endif; ?>
                    </div>
                    <div class="form-groups cust-group">
                        <label>Шт.</label>
						<div>
							<?=(isset($position['quantity'])) ? $position['quantity'] : 1 ?>
						</div>
                    </div>
                    <div class="form-groups cust-group">
                        <label>Сумма</label>
						<div>
							<?=(isset($position['sum'])) ? $position['sum'] : NULL ?>
						</div>
                    </div>
                </div>
                <div class="delete"></div>
                </div>
            <?php endforeach; ?>
        <?php endif; ?> 
    </form>
	
	<script>
		(function() {
			var showVerSelect = document.querySelector('#show-ver-select');
			var verSelect = document.querySelector('#ver-select');
			if(showVerSelect) {
				showVerSelect.addEventListener('click', function() {
					verSelect.classList.toggle('hidden-box');
				});
			}
		})();
	</script>