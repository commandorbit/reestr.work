<?php

/*
OKPD не подготовлено
*/

require $_SERVER['DOCUMENT_ROOT'] . '/init.php';
$table = 'okpd';

function gz_parse($path, $table) {
	sql_query("DELETE FROM $table");
	if(is_file($path)) {
		$xml = xmlObject($path);
		parse_run($xml->nsiOKPD2List->nsiOKPD2, $table);
	} else if(is_dir($path)) {
		$files = scandir($path);
		foreach($files as $file) {
			if($file == '.' || $file == '..') continue;
			if(substr($file, -4) == '.xml') {
				$filepath = $path . DIRECTORY_SEPARATOR . $file;
				$xml = xmlObject($filepath);
				parse_run($xml->nsiOKPD2List->nsiOKPD2, $table);
			}
		}
	}
}

function xmlObject($path) {
	$content = file_get_contents($path);
	$xml = new SimpleXMLElement($content);
	return $xml;
}

function parse_run($xmlList, $table) {
	for ($i=0; $i<count($xmlList); $i++) {
		$children = $xmlList[$i]->children('http://zakupki.gov.ru/oos/types/1');	
		$comment = (isset($children->comment[0])) ? $children->comment : NULL;
		sql_query("INSERT INTO $table (code, parent_code, name, comment) VALUES('$children->code', '$children->parentCode', '$children->name', '$comment')");
	}
}

gz_parse(dirname(__FILE__) . '/xml/okpd', $table);