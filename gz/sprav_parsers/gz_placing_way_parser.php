<?php

require $_SERVER['DOCUMENT_ROOT'] . '/init.php';
$table = 'gz_placing_way';

function gz_parse($filePath, $table) {
	$content = file_get_contents($filePath);
	$xml = new SimpleXMLElement($content);
	sql_query("DELETE FROM $table");
	parse_run($xml->nsiPlacingWayList->nsiPlacingWay, $table);
}

function parse_run($xmlList, $table) {
	for ($i=0; $i<count($xmlList); $i++) {
		$children = $xmlList[$i]->children('http://zakupki.gov.ru/oos/types/1');	
		if($children->actual == 'true') {
			sql_query("INSERT INTO $table (code, name, subsystemType) VALUES('$children->code', '$children->name', '$children->subsystemType')");
		}
	}
}

sql_query("DELETE FROM $table");
gz_parse(dirname(__FILE__) . '/xml/placing_way.xml', $table);
echo "Ok";