<?php

require $_SERVER['DOCUMENT_ROOT'] . '/init.php';
$table = 'okved';

function gz_parse($filePath, $table) {
	$content = file_get_contents($filePath);
	$xml = new SimpleXMLElement($content);
	sql_query("DELETE FROM $table");
	parse_run($xml->nsiOKVED2List->nsiOKVED2, $table);
}

function parse_run($xmlList, $table) {
	for ($i=0; $i<count($xmlList); $i++) {
		$children = $xmlList[$i]->children('http://zakupki.gov.ru/oos/types/1');	
		sql_query("INSERT INTO $table (name, localName, code) VALUES('$children->fullName', '$children->localName', '$children->code')");
	}
}

gz_parse(dirname(__FILE__) . '/xml/okved2.xml', $table);