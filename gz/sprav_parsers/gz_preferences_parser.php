<?php

require $_SERVER['DOCUMENT_ROOT'] . '/init.php';
$table = 'gz_preferences';

function gz_parse($filePath, $table) {
	$content = file_get_contents($filePath);
	$xml = new SimpleXMLElement($content);
	sql_query("DELETE FROM $table");
	parse_run($xml->nsiPurchasePreferenceList->nsiPurchasePreference, $table);
}

function parse_run($xmlList, $table) {
	for ($i=0; $i<count($xmlList); $i++) {
		$children = $xmlList[$i]->children('http://zakupki.gov.ru/oos/types/1');	
		if($children->actual == 'true') {
			sql_query("INSERT INTO $table (name, code) VALUES('$children->name', '$children->id')");
		}
	}
}

sql_query("DELETE FROM $table");
gz_parse(dirname(__FILE__) . '/xml/prefs.xml', $table);
echo "Ok";