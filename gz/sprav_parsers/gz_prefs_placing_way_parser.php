<?php

require $_SERVER['DOCUMENT_ROOT'] . '/init.php';
$table = 'gz_prefs_placing_way';

function gz_parse($filePath, $table) {
	$content = file_get_contents($filePath);
	$xml = new SimpleXMLElement($content);
	sql_query("DELETE FROM $table");
	parse_run($xml->nsiPurchasePreferenceList->nsiPurchasePreference, $table);
}

function parse_run($xmlList, $table) {
	for ($i=0; $i<count($xmlList); $i++) {
		$children = $xmlList[$i]->children('http://zakupki.gov.ru/oos/types/1');	
		if($children->actual == 'true') {
			$id = $children->id;
			$placingWays = $children->placingWays;
			for($j = 0; $j < count($placingWays); $j++) {
				$placingWayCodes = $placingWays[$j]->code;
				foreach ($placingWayCodes as $code) {
					sql_query("INSERT INTO $table (gz_preferences_id, code) VALUES('$id', '$code')");
				}
				//sql_query("INSERT INTO $table (gz_preferences_id, code) VALUES('$id', '$placingWayCode')");
			}
		}
	}
}

sql_query("DELETE FROM $table");
gz_parse(dirname(__FILE__) . '/xml/prefs.xml', $table);
echo "Ok";