﻿<?php
//AVD Функция получения строкаи текушей даты и времени
function avd_getcurrentdate()
{
	return date("d.m.Y H:i:s", time() + 60 * 60);
}

//AVD Функция вывода элементов массива
function avd_printarrayvalues($arr, $arrname)
{
	$name = ""; //arrayname($arr);
		echo "<b>Значения массива $arrname</b><br>";
	while (list($key, $value) = each($arr))
		echo $key . " -> " . $value . "<br>";
	echo "<br>";
}

//AVD Функция вывода элементов массива рекурсивная
function avd_printarrayvalues_r(&$arr, $l = 0)
{
	@$arr = (array)$arr;
	foreach($arr as $i => $v) 
	{
		for ($n = 0; $n < $l; $n++) echo '&nbsp&nbsp&nbsp&nbsp';
		if (is_array($v))
		{
			echo $i . ': Массив [' . count($v) . ' элементов] = <br>'; 
			avd_printarrayvalues_r($v, $l + 1);
		}
		else if (is_object($v))
		{
			echo $i . ': Объект (' . count($v) . ' свойств) = <br>'; 
			avd_printarrayvalues_r($v, $l + 1);
		}
		else
			echo $i . ' = ' . $v . "<br>"; 
	}
}

//AVD Получение указанной части даты
function avd_getdatepart($time, $partname)
{
	$d = getdate($time);
	$r = $d[$partname];
	return $r;
}

//AVD Разбор строки и выделение в ней ссылок на документы
function avd_parsedoclinks($s)
{
	$a = explode(' ', $s);
	foreach ($a as $i => $v) 
		if (! stristr($v, '.') === FALSE)
		{
			$f = 'doc/eri/' . $v;
			if (file_exists($f))
				$a[$i] = '<a href = "' . $f . '">' . $v . '</a>';
		}
		else 
			$a[$i] = $v;
	$r = implode(' ', $a);
	return $r;
}

//AVD Чтение строк XML формата ЕД-108
function avd_read_ed108($filename)
{
	// simplexml_load_file
	// http://php.net/manual/ru/function.simplexml-load-file.php
	//$r = $filename; return array('filename' => $filename);
	//	
	$xml = simplexml_load_file($filename);
	$root = $xml->DataArea->Header->Report;
	//var_dump($root);
	$plat = [];
	$rows = [];
	$plat['GUID_FK'] = (string) $root->GUID_FK;
	$plat['DATE_PP'] = (string) $root->DATE_PP;
	$plat['SUM_PP'] = (string) $root->SUM_PP;
	$plat['INN_PLAT_PP'] = (string) $root->INN_PLAT_PP;
	$plat['KPP_PLAT_PP'] = (string) $root->KPP_PLAT_PP;
	$plat['PURPOSE_PP'] = (string) $root->PURPOSE_PP;
	$plat['FIO_ISP'] = (string) $root->FIO_ISP;
	$plat['TEL_ISP'] = (string) $root->TEL_ISP;
	// explode 
	foreach ($root->RegPP as $el)  {
		$row = [];
		foreach ($el as $key=>$val) {
			if ($key == 'PURPOSE')
			{
				$p = explode(';', $val);
				$pp = [];
				foreach ($p as $v) {
					$d = explode(':', $v);
					if (count($d) == 2) {
						$pp[$d[0]] = $d[1];				
					}
				}
				$row[$key] = $pp;
			}
			else if ($key == 'FIO_PLAT')
				$row[$key] = substr($val, 0, strlen($val) / 2);
			else 
				$row[$key] = (string) $val;
		}
		$rows[] = $row;
	}
	$r = [];
	$r ['HEADER'] = $plat;
	$r ['DETAIL'] = $rows;
	return $r;
	//return $xml;
}

//AVD Вывод ассоциативного массива в виде таблицы
function avd_print_assoc_array($r)
{
	if ($r)
	{
		//foreach($r as $i => $v) echo $i . ' = ' . $v . "<br>";
		echo '<table border = "1" cellpadding = "3" cellspacing = "1" frame = "border" rules = "all">';
		$ii = 0;
		foreach ($r as $rr)
		{
			if ($ii == 0) 
			{
				echo '<tr>';
				foreach ($rr as $i => $v) echo '<th>' . $i . '</th>';
				echo '</tr>';
			}
			echo '<tr>';
			foreach ($rr as $i => $v) echo '<td>' . $v . '</td>';
			echo '</tr>';
			$ii++;
		}
		echo '</table>';
	}
	else
		echo "Нет результатов.";	
}

//AVD Выкладывает результаты запроса в массив;
function sql_result_array($r)
{
	$a = array();
	if ($r)
		while ($rr = $r->fetch_assoc())
			$a[] = $rr;
	return $a;
}

//AVD Вывод ассоциативного результата запроса в виде таблицы
function avd_print_assoc_result($r)
{
	if ($r)
	{
		//foreach($r as $i => $v) echo $i . ' = ' . $v . "<br>";
		echo '<table border = "1" cellpadding = "3" cellspacing = "1" frame = "border" rules = "all">';
		$ii = 0;
		while ($rr = $r->fetch_assoc())
		{
			if ($ii == 0) 
			{
				echo '<tr>';
				foreach ($rr as $i => $v) echo '<th>' . $i . '</th>';
				echo '</tr>';
			}
			echo '<tr>';
			foreach ($rr as $i => $v) echo '<td>' . $v . '</td>';
			echo '</tr>';
			$ii++;
		}
		echo '</table>';
	}
	else
		echo "Нет результатов.";	
}

//AVD Преобразование массива вложенных ассоциативных массивов в плоский массив
function avd_flat_assoc_array($in, $prefix = '')
{
	$in = (array)$in;
	$out = array();
	foreach ($in as $i => $v)
	{
		if (is_array($v) || is_object($v)) 
		{
			$out += avd_flat_assoc_array($v, $i);
		}			
		else
			$out[$prefix ? $prefix . '_' . $i : $i] = $v;
	}
	return $out;
}

//AVD Вывод значений ассоциативного массива в виде таблицы
function avd_print_assoc_values($r)
{
	if ($r)
	{
		//foreach($r as $i => $v) echo $i . ' = ' . $v . "<br>";
		echo '<table border = "1" cellpadding = "3" cellspacing = "1" frame = "border" rules = "all">';
		//while ($rr = $r->fetch_assoc())
		foreach($r as $k => $v)
		{
			echo '<tr>';
			echo '<td>' . $k . '</td>';
			echo '<td>' . $v . '</td>';
			echo '</tr>';
		}
		echo '</table>';
	}
	else
		echo "Нет результатов.";	
}

?>
