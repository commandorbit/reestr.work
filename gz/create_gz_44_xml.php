<?php 
require $_SERVER['DOCUMENT_ROOT'] . '/init.php';

//AVD Функция вывода элементов массива рекурсивная
function avd_printarrayvalues_r($arr, $l = 0)
{
	@$arr = (array)$arr;
	foreach($arr as $i => $v) 
	{
		for ($n = 0; $n < $l; $n++) echo '&nbsp&nbsp&nbsp&nbsp';
		if (is_array($v))
		{
			echo $i . ': Массив [' . count($v) . ' элементов] = <br>'; 
			avd_printarrayvalues_r($v, $l + 1);
		}
		else if (is_object($v))
		{
			echo $i . ': Объект (' . count($v) . ' свойств) = <br>'; 
			avd_printarrayvalues_r($v, $l + 1);
		}
		else
			echo $i . ' = ' . $v . "<br>"; 
	}
}

$xml_path = dirname(__FILE__) . '/xml_parts';
//$dogovors_gz = sql_rows("SELECT dogovor_id FROM dogovor_gz");

//AVD
if (isset($_GET['year'])) $year = $_GET['year']; else $year = 0;
if (isset($_GET['ver'])) $ver = $_GET['ver']; else $ver = 0;
$gz_44 = sql_rows("select * from gz_44 where year = $year and ver = $ver");
if (count($gz_44) == 1)
	$gz_44 = $gz_44[0];
else
{
	$gz_44['date_sign'] = 0;
	$gz_44['date_publish'] = 0;
}
	
//$dogovors_gz = sql_rows("SELECT dogovor_id FROM dogovor_gz ORDER BY pos_num");
$dogovors_gz = sql_rows("
	SELECT dogovor_gz.id
	FROM dogovor_gz 
	  LEFT JOIN gz_44_sostav ON gz_44_sostav.dogovor_gz_id = dogovor_gz.id
	  LEFT JOIN gz_44 ON gz_44.id = gz_44_sostav.gz_44_id
	WHERE gz_44.ver = $ver AND gz_44.year = $year
	ORDER BY dogovor_gz.pos_num");

//NB!!!
//echo '<html><head><title>Выгрузка XML</title></head><body>';

//NB!!!
/*
echo '<hr>';
avd_printarrayvalues_r($gz_44);
echo '<hr>';
echo date("Y-m-d\TG:i:s\Z", strtotime($gz_44['date_sign']));
echo '<hr>';
echo date("Y-m-d\TG:i:s\Z", strtotime($gz_44['date_publish']));
echo '<hr>';
*/
//

//param - таблица с реквизитами организации
$params = sql_rows("SELECT * FROM param");

//NB!!!
//echo '<hr>'; avd_printarrayvalues_r($params); echo '<hr>';

$requisites = array();
foreach ($params as $param) {
	$requisites[$param['name']] = $param['value'];
}

//NB!!!
//echo '<hr>'; avd_printarrayvalues_r($requisites); echo '<hr>';
//echo '<hr>' . $requisites['FullName'] . '<hr>';

$org_name = mb_strtoupper($requisites['FullName'], 'UTF-8');

ob_start();
echo '<?xml version="1.0" encoding="UTF-8"?>' . "\r\n"; 
//echo "<hr>$org_name<hr>"

//PlanNumber
//  44201603721000535001
//$PlanNumber = '44201603721000535001';
$PlanNumber = '44' . date('Y') . $requisites['gzRegNum'] . '001';

//positionNumber
// П44201603721000535001000004
$positionNumber = 1;

require ($xml_path . '/header.xml');	
foreach($dogovors_gz as $dogovor_gz) {
	$dogovor_gz_id = $dogovor_gz['id'];
	$position_sql = "
	SELECT
	  dogovor_gz.id dogovor_gz_id,
	  dogovor.subject AS subject,
	  dogovor.demand AS min_requirement,
	  dogovor_gz.dogovor_id,
	  dogovor_gz.contract_max_price,
	  dogovor_gz.contract_execution_stages,
	  dogovor_gz.periodicity,
	  gz_placing_way.code AS pw_code,
	  gz_placing_way.NAME AS pw_name,
	  (SELECT
		  fpa.kvr
		FROM dogovor_sostav ds
		  LEFT JOIN fp_article fpa
			ON ds.fp_article_id = fpa.id
		WHERE ds.dogovor_id = '$dogovor_gz_id'
		ORDER BY ds.amount DESC
		LIMIT 1) kvr,
	  gz_change_reason.id gz_change_reason_id,
	  gz_change_reason.NAME gz_change_reason_name,
	  dogovor_gz.change_reason_info
	FROM dogovor_gz
	  /*left join dogovor_gz_placing_way dgpw on dogovor_gz.id = dgpw.dogovor_gz_id*/
	  LEFT JOIN gz_placing_way
		ON gz_placing_way.code = dogovor_gz.gz_placing_way_code
	  LEFT JOIN dogovor
		ON dogovor.id = dogovor_gz.dogovor_id
	  LEFT JOIN gz_change_reason
		ON dogovor_gz.gz_change_reason_id = gz_change_reason.id						
	WHERE dogovor_gz.id = '$dogovor_gz_id'
	";
					    //ORDER BY dogovor_gz.pos_num
	$position = sql_array(sql_query($position_sql));
	
//	$dogovor_gz_id = $position['dogovor_gz_id'];
	$position['okveds'] = sql_rows("SELECT code, name FROM okved WHERE code in (SELECT okved2_code FROM dogovor_gz_okved2 WHERE dogovor_gz_id = '$dogovor_gz_id')");
	//NB!!!!!!
	//echo "<hr> $dogovor_id "; var_dump($position['okveds']); echo '<hr>';
	//
	//products

	$products_sql = "select 
						dgp.quantity,
						dgp.sum,
						okpd.code as okdp_code,
						okpd.name as okdp_name,
						gz_okei.code as gz_okei_code,
						gz_okei.name as gz_okei_name
						FROM dogovor_gz_position dgp
						LEFT JOIN okpd on okpd.code = dgp.okpd
						LEFT JOIN gz_okei on gz_okei.code = dgp.gz_okei
						WHERE dgp.dogovor_gz_id = '$dogovor_gz_id'
					";
	$products = sql_rows($products_sql);
	//NB!!!!!!
	//echo "<hr> $dogovor_id "; var_dump($products); echo '<hr>';
	//
	if (isset($position['okveds']) && count($position['okveds']) > 0)
		require ($xml_path . '/position.xml');	
}

//AVD
$final_sql = "
SELECT
  gz_44.id,
  gz_44.year,
  gz_44.ver,
  gz_44.date_sign,
  gz_44.date_publish,
  gz_44_final_positions.less100,
  gz_44_final_positions.less400,
  gz_44.description,
  MAX(dogovor_gz.contract_max_price) AS contract_max_price
FROM gz_44
  INNER JOIN gz_44_final_positions
    ON gz_44_final_positions.gz_44_id = gz_44.id
  INNER JOIN gz_44_sostav
    ON gz_44_sostav.gz_44_id = gz_44.id
  INNER JOIN dogovor_gz
    ON gz_44_sostav.dogovor_gz_id = dogovor_gz.id
WHERE gz_44.ver = $ver AND gz_44.year = $year
GROUP BY gz_44.year,
         gz_44.ver
";
$finals = sql_rows($final_sql);
if (count($finals) == 1)
{
	$final = $finals[0];
	require ($xml_path . '/footer.xml');		
}

$xml = ob_get_clean();	

if (isset($_GET['download'])) {
	//NB!!!!!!
	//var_dump($_GET['download']);
	//echo $xml_path;
	//echo '<html><head><title>Выгрузка XML</title></head><body>'; echo $xml; echo '<hr>'; exit;
	//
	/**/
	header('Content-Type: text/xml');
	header('Content-Disposition: attachment;filename="gz44_'.date('d-m-Y_H:i:s').'.xml"');
	header('Cache-Control: max-age=0');
	/**/
	echo $xml;
} else {
	header("Location: /index.php");
}