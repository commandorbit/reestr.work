<?php

include $_SERVER['DOCUMENT_ROOT'] . '/init.php';

if(isset($_POST['create_ver']) && isset($_POST['dogovor_id'])) {
	$dogovor_id = sql_escape($_POST['dogovor_id']);
	$dogovor_gz = sql_array(sql_query("SELECT * FROM dogovor_gz WHERE dogovor_id='$dogovor_id' ORDER BY ver DESC LIMIT 1"));
	$dogovor_gz_id = $dogovor_gz['id'];
	$new_ver = $dogovor_gz['ver'] + 1;
	if($gz_44_draft_id = gz_44_draft_id()) {
		sql_query('START TRANSACTION;');
		sql_query("INSERT INTO dogovor_gz SELECT NULL as id, dogovor_id, pos_num, $new_ver as ver, contract_max_price, zayav_guarantee, contract_guarantee, contract_execution_stages, periodicity, gz_change_reason_id, change_reason_info, NULL as create_ts, gz_placing_way_code FROM dogovor_gz WHERE id='$dogovor_gz_id'");
		$dogovor_gz_new_id = sql_last_id();
		sql_query("INSERT INTO dogovor_gz_okved2 SELECT NULL as id, $dogovor_gz_new_id as dogovor_gz_id, okved2_code FROM dogovor_gz_okved2 WHERE dogovor_gz_id = $dogovor_gz_id");
		//AVD commented
		//sql_query("INSERT INTO dogovor_gz_placing_way SELECT NULL as id, $dogovor_gz_new_id as dogovor_gz_id, gz_placing_way FROM dogovor_gz_placing_way WHERE dogovor_gz_id = $dogovor_gz_id");
		//
		sql_query("INSERT INTO dogovor_gz_position SELECT NULL as id, $dogovor_gz_new_id as dogovor_gz_id, okved, okpd, gz_okei, quantity, sum FROM dogovor_gz_position WHERE dogovor_gz_id = $dogovor_gz_id");
		sql_query("INSERT INTO dogovor_gz_preferences SELECT NULL as id, $dogovor_gz_new_id as dogovor_gz_id, pref_code FROM dogovor_gz_preferences WHERE dogovor_gz_id = $dogovor_gz_id");
		if($gz_44_draft_id) {
			$isset_sostav = sql_get_value('id', 'gz_44_sostav', "dogovor_gz_id = '$dogovor_gz_id' AND gz_44_id = '$gz_44_draft_id'");
			if($isset_sostav) {
				sql_query("UPDATE gz_44_sostav SET dogovor_gz_id = '$dogovor_gz_new_id' WHERE gz_44_id = '$gz_44_draft_id' AND dogovor_gz_id = '$dogovor_gz_id'");
			} else {
				sql_query("INSERT INTO gz_44_sostav (gz_44_id, dogovor_gz_id) VALUES ('$gz_44_draft_id', '$dogovor_gz_new_id')");
			}
		}
		sql_query("COMMIT;");
		header("Location: " . $_SERVER['HTTP_REFERER'] . '&ver=' . $new_ver);
	} else {
		$_SESSION['SYSTEM_MESSAGE'] = 'Не найдена версия ФЗ-44 в статусе "Черновик"';
		header('Location: ' . $_SERVER['HTTP_REFERER']);
		exit();
	}
} else if(isset($_POST['delete_ver']) && isset($_POST['dogovor_id'])) {
	if(!gz_44_draft_id()) {
		$_SESSION['SYSTEM_MESSAGE'] = 'Не найдена версия ФЗ-44 в статусе "Черновик"';
		header('Location: ' . $_SERVER['HTTP_REFERER']);
		exit();
	}
	$dogovor_id = sql_escape($_POST['dogovor_id']);
	$dogovor_gz = sql_array(sql_query("SELECT * FROM dogovor_gz WHERE dogovor_id='$dogovor_id' ORDER BY ver DESC LIMIT 1"));
	$dogovor_gz_id = $dogovor_gz['id'];
	if($dogovor_gz['ver'] <= 1) {
		$_SESSION['SYSTEM_MESSAGE'] = 'Вы не можете удалить единственную версию';
		header('Location: ' . $_SERVER['HTTP_REFERER']);
		exit();
	}
	sql_query("DELETE FROM dogovor_gz_okved2 WHERE dogovor_gz_id = '$dogovor_gz_id'");
	//AVD commented
	//sql_query("DELETE FROM dogovor_gz_placing_way WHERE dogovor_gz_id = '$dogovor_gz_id'");
	//
	sql_query("DELETE FROM dogovor_gz_position WHERE dogovor_gz_id = '$dogovor_gz_id'");
	sql_query("DELETE FROM dogovor_gz_preferences WHERE dogovor_gz_id = '$dogovor_gz_id'");
	$dogovor_gz_prev_ver = sql_get_value('id', 'dogovor_gz', "ver = '" . (int) ($dogovor_gz['ver'] - 1) . "'");
	if($dogovor_gz_prev_ver) {
		sql_query("UPDATE gz_44_sostav SET dogovor_gz_id = '$dogovor_gz_prev_ver' WHERE dogovor_gz_id = '$dogovor_gz_id'");
	}
	$referer_array = explode('&', $_SERVER['HTTP_REFERER']);
	foreach($referer_array as $key=>$part) {
		if(preg_match("/^ver=(\d+)/", $part)) unset($referer_array[$key]);
	}
	//AVD перенесено вниз
	sql_query("DELETE FROM dogovor_gz WHERE id = '$dogovor_gz_id'");
	//
	$referer = implode('&',$referer_array);
	header("Location: " . $referer);
}

?>

