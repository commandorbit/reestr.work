<?php if(isset($_GET['status'])) $statuses = $_GET['status'] ?>

<div class="center">
	<select name="limits_by_status" class="select_status" multiple="multiple">
		<?php 

			foreach($statusSet as $status) {
				$selected = '';

				if(isset($statuses)) {
					for($i = 0; $i < count($statuses); $i++) {
						if($status['id'] == $statuses[$i]) $selected = 'selected';
					}
				}

				$status_id = $status['id'];
				$status_name = $status['name'];

				echo '<option value="'.$status_id.'" '.$selected.'>' . $status['name'] . '</option>';
			}

		?>
	</select>
</div>

<link rel="stylesheet" type="text/css" href="/assets/js/multiselect-plugin/multiselect.css">
<script src="/assets/js/multiselect-plugin/modalWindowBody.js" type="text/javascript"></script>
<script src="/assets/js/multiselect-plugin/multiselect.js" type="text/javascript"></script>

<script>
	var select = document.querySelector('.select_status');
	multiSelectPlugin.selectStart(select);

	var control = document.querySelector('.default-control');

	function urlApply() {
		multiselectDiv = this.parentNode;
		var checked = multiselectDiv.querySelectorAll('input[type="checkbox"]:checked');
		
		var url = document.location.pathname + '?t=' + '<?php echo $table ?>';

		for(var i = 0; i < checked.length; i++) {
			if(checked[i].value != '_all_')
				url += '&status[]=' + encodeURIComponent(checked[i].value);
		}

		window.location.assign(url);
	}
</script>