<?php

$zfo = user_zfo_id_str();
$fin_sources = user_own_fin_sources();

function vDogovorData($zfo, $fin_source_list) {
	$sql = "SELECT NULL as favorites, (select count(df.id) from dogovor_files df where (df.dogovor_id = d.id)) AS files, d.id AS id, d.fp_year, d.dogovor_status_id, thesis_dogovor.state AS thesis_state,  thesis_dogovor.ts AS thesis_state_ts,d.concluded_ku, d.zfo_id,d.document_type_id ,d.subject,d.demand,sum(d_sostav.amount) AS amount, d.contragent_id, d.d, d.dogovor_num,sum(d_sostav.quantity) AS quantity, d.basis,d.plan_grafic_id ,d.place_method_id ,d.period_place_id ,d.period_finish_id
			FROM  dogovor d LEFT JOIN dogovor_sostav d_sostav ON d.id = d_sostav.dogovor_id
			LEFT JOIN thesis_dogovor on thesis_dogovor.dogovor_id = d.id
			WHERE (d.zfo_id in ($zfo) OR d_sostav.fin_source_id IN ($fin_source_list))
			GROUP by d.id";

	$res=sql_query($sql);

	return sql_rows_fix_numeric($res);
}

function vReestrData() {
	$zfo_str = user_zfo_id_str();
	$zfo = '';
	if(user_has_zfo())
		$zfo = " AND (z.zfo_id in ($zfo_str)) ";

	$sql = "select z.id AS id, z.zfo_id, zs.fp_article_id as fp_article_id, z.zayav_status_id AS zayav_status_id,z.reestr_d AS reestr_d,z.plat_type_id AS plat_type_id,z.contragent_id AS contragent_id,zs.sum AS s,zs.fin_source_id AS fin_source_id,fs.booker_fin_source as booker_fin_source,art.kosgu AS kosgu, art.kvr
			from ((zayav_sostav zs join zayav z on((zs.zayav_id = z.id))) 
			left join fp_article art on((zs.fp_article_id = art.id)) left join fin_source fs ON fs.id = zs.fin_source_id) 
			where (z.zayav_status_id > 1)" . $zfo;

	$res=sql_query($sql);
	return sql_rows_fix_numeric($res);
}


/* deprecated оставлена пока вместе с пунктом меню для тестирования */
function vLimitsData($statuses = null, $zfo, $fin_source_list) {
	$statuses_sql = 'true';

	if(!is_null($statuses))
		$statuses_sql = "dogovor.dogovor_status_id IN ($statuses)";

	$where = "WHERE ($statuses_sql)";

	if(user_has_zfo()) {
		$where = "WHERE (dogovor.zfo_id in ($zfo) OR dogovor_sostav.fin_source_id in ($fin_source_list)) AND $statuses_sql";
	}

	$sql = "SELECT dogovor.fp_year,dogovor.zfo_id, dogovor_sostav.fin_source_id,sum(dogovor_sostav.amount) AS amount, zfo_limit.sum AS limit_sum,zfo_limit.id as zfo_limit_id
			FROM dogovor_sostav join dogovor on dogovor_sostav.dogovor_id = dogovor.id
			LEFT JOIN zfo_limit on dogovor.zfo_id = zfo_limit.zfo_id and dogovor_sostav.fin_source_id = zfo_limit.fin_source_id and dogovor.fp_year=zfo_limit.fp_year
			$where
			GROUP BY dogovor.zfo_id,dogovor_sostav.fin_source_id,dogovor.fp_year,zfo_limit_id";

	$res=sql_query($sql);
	$zfo_limit_ids=array();
	while ($row = sql_array($res)) {
		if ($row['zfo_limit_id']) $zfo_limit_ids[] = $row['zfo_limit_id'];
	}
	$where = ' true ';
	if (count($zfo_limit_ids)) {
		$where .= " and id not in (" . implode(',', $zfo_limit_ids). ")";
	}	
	if(user_has_zfo()) {
		$where .= " and (zfo_id in ($zfo) or fin_source_id in ($fin_source_list))";
	}
	$sql .= " UNION ALL SELECT fp_year, zfo_id,fin_source_id, 0 as amount,sum as limit_sum,id from zfo_limit WHERE $where "; 
	
	$res=sql_query($sql);
	$sql_rows = sql_rows_fix_numeric($res);

	if(count($sql_rows)==0) die('<div align="center" style="margin-top: 30px; font-weight: bold; color: red">Не найдено договоров с выбранными статусами!</div>');
	return $sql_rows;
}



function vDocLimChangeData($zfo, $fin_source_list) {
	$sql = "select dlc.id, dlc.d, dlc.description, dlc.is_approved from doc_lim_change dlc 
			left join doc_lim_change_sostav dlcs on dlcs.doc_lim_change_id = dlc.id
			where dlcs.zfo_id in ($zfo) or fin_source_id in ($fin_source_list)";
	$res=sql_query($sql);
	return sql_rows_fix_numeric($res);
}

function vDogovorPlatData($dogovor_id) {
	$sql = "SELECT * FROM plat WHERE zayav_id in (SELECT id FROM zayav WHERE dogovor_id = '$dogovor_id')";
	$res=sql_query($sql);
	return sql_rows_fix_numeric($res);
}

?>
