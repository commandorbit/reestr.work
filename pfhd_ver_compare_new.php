<?php

$title = 'Выберите версию ПФХД';

include $_SERVER['DOCUMENT_ROOT'] . '/init.php';
include $_SERVER['DOCUMENT_ROOT'] . '/template/header.php';

$years = sql_to_assoc("SELECT id, name FROM plan_period ORDER BY id");

$versions_2016 = sql_to_assoc("SELECT id, name FROM stat_pfhd_ver WHERE plan_period_id=2016 ORDER BY id DESC");
$versions_2016_v2 = $versions_2016;
$versions_2016 = [Pfhd::CURRENT_VERSION_2016=>'Текущая 2016']+$versions_2016;

$versions_2017 = sql_to_assoc("SELECT id, name FROM stat_pfhd_ver WHERE plan_period_id=2017 ORDER BY id DESC");
$versions_2017_v2 = $versions_2017;
$versions_2017 = [Pfhd::CURRENT_VERSION_2017=>'Текущая 2017']+$versions_2017;

$versions_2018 = sql_to_assoc("SELECT id, name FROM pfhd_ver ORDER BY id DESC");

require_once SHARED_PHP_PATH . '/controls/controls.php';
$controls = new controls;
?>
<div id="ext-body" class="ext-body">
<script type="text/javascript">
	var Ext = Ext || {};
	Ext.manifest = '/extapp/<?=$theme_name[$_SESSION["palette"]]?>.json';
</script>
<script id="microloader" type="text/javascript" src="/extapp/bootstrap.js"></script>
<script type="text/javascript">
	window.addEventListener("load", function(event){
		var waitExtJSTimer = setInterval(function(){
			if (window.extapp && window.extapp.getApplication){
				window.extapp.userId = <?=$user_id?>;
				clearInterval(waitExtJSTimer);
				console.log('Библиотека загружена!');
				$('.admin_buttons').css('visibility','visible');
			}
		},
		100);	
	});
</script>

<div class="ver_check">
	<h3>Для продолжения необходимо выбрать верси<span id="suffix">ю</span> ПФХД:</h3>
	<form method="GET" action="/pivot/pivot.php">
		<input type="hidden" name="type" value="pfhd">
		<label class="ver_select">
			<p>Период:</p>
			<?php $controls->print_select('y',$years,2017); ?>
		</label>
		
		<label class="ver_select">
			<p>Первая версия:</p>
			<?php $controls->print_select('first_ver',[],''); ?>
		</label>
		<label class="ver_select hidden_field">
			<p>Вторая версия:</p>
			<?php $controls->print_select('second_ver',[],''); ?>
		</label>

		<input type="submit" name="submit" value="Продолжить">
		<p>
			<input type="checkbox" id="compare" onchange="showCompareControl()"> Сравнить версии
			<input type="checkbox" id="rashod_sign" name="rashod_sign" checked="checked"> Расходы со знаком минус
			<input type="checkbox" name="pfhd_article_y" value="2017">		Использовать  статьи ПФХД 2017
		</p>
	</form>
	<?php if (is_admin()) : ?>
		<p class="admin_buttons">
			<button id="create_version_2017_btn">Создать версию 2017</button>
			<button id="create_version_2018_btn">Создать версию 2018</button>
			<br>
			<button id="versions_2017_btn">Версии 2017</button>
			<button id="versions_2018_btn">Версии 2018</button>
		</p>
	<?php endif ?>
</div>
</div><!--ext body-->

<?php if (is_admin()) : ?>

<div id="create_version_2017" class="create_version_div">
	<?php 
	$plan_dohod_vers = sql_to_assoc("SELECT id, name FROM plan_dohod_ver WHERE y='2017' ORDER BY name");
	$plan_dohod_ver_id_default = sql_get_value("MAX(id)",'plan_dohod_ver',"y='2017'");
	$controls->print_input_hidden('plan_period_id',2017);
	$controls->print_select_label('plan_dohod_ver_id',$plan_dohod_vers,$plan_dohod_ver_id_default,'Версия плана доходов');
	$controls->print_input_label('ver_name','','Название версии');
	$controls->print_input_label('prim','','Примечание');
	?>
</div>

<div id="create_version_2018" class="create_version_div">
	<?php 
	$controls->print_input_hidden('plan_period_id',2018);
	$controls->print_input_label('ver_name','','Название версии');
	$controls->print_input_label('prim','','Примечание');
	?>
</div>

<script type="text/javascript">
	$(document).ready(function(){
		$('#create_version_2017_btn').click(function(){createVersionDialog(2017);});
		$('#create_version_2018_btn').click(function(){createVersionDialog(2018);});
		$('#versions_2017_btn').click(function(){
			if (window.extapp && window.extapp.getApplication) {
				extapp.getApplication().openTable('stat_pfhd_ver');
			} else {
				alert('Пожалуйста, подождите! Загружаю библиотеки...');
			}
		});
		$('#versions_2018_btn').click(function(){
			if (window.extapp && window.extapp.getApplication) {
				extapp.getApplication().openTable('pfhd_ver');
			} else {
				alert('Пожалуйста, подождите! Загружаю библиотеки...');
			}
		});
		$('select[name="plan_period_id"]').change(function(){
			var planPeriodId = $(this).val()
			$.ajax({
				url: '/modules/ajax.php?func=get_plan_dohod_options&arg1='+planPeriodId
			}).done(function(htmlOptions){
				$('select[name="plan_dohod_ver_id"]').html(htmlOptions);
			});
		});

		function createVersionDialog(year) {
			if (year == 2017) {
				var div = $('#create_version_2017');
			} else {
				var div = $('#create_version_2018');
			}
			
			div.dialog({
				title: 'СОЗДАТЬ ВЕРСИЮ ПФХД '+year,
				minWidth: 500,
				maxWidth: 500,
				minHeight: 100,
				modal: true,
				close: function() {
					// code onclose
					$('.create_version_div input[type="text"]').val('');
				},
				autoOpen: true,
				buttons : [
					{
						text: 'Отмена',
						id: 'dialogCancelBtn',
						click: function() {
							$(this).dialog("close");
						}
					},
					{
						text: 'Сохранить',
						click: function() {
							// code for saving

							var planPeriodId = div.find('input[name="plan_period_id"]').val();
							var planDohodVer = div.find('select[name="plan_dohod_ver_id"]').val();
							var verName = div.find('input[name="ver_name"]').val();
							var prim = div.find('input[name="prim"]').val();

							if (!verName) {
								alert('Не введено название версии!');
							} else {
								if (year == 2017) {
									var saveUrl = '/asu_pfhd/save_pfhd_ver_2017.php?ver_name='+verName+'&plan_period_id='+planPeriodId+'&plan_dohod_ver_id='+planDohodVer+'&prim='+prim;
								} else {
									var saveUrl = '/asu_pfhd/save_pfhd_ver.php?ver_name='+verName+'&plan_period_id='+planPeriodId+'&prim='+prim;
								}
								
								location.href=saveUrl;
							}
						}
					}
				]
			});
		}
	});
</script>

<?php endif ?>

<script type="text/javascript">
	var options2016 = <?=json_encode($versions_2016,JSON_UNESCAPED_UNICODE);?> || [];
	var options2016_v2 = <?=json_encode($versions_2016_v2,JSON_UNESCAPED_UNICODE);?> || [];
	var options2017 = <?=json_encode($versions_2017,JSON_UNESCAPED_UNICODE);?> || [];
	var options2017_v2 = <?=json_encode($versions_2017_v2,JSON_UNESCAPED_UNICODE);?> || [];
	var options2018 = <?=json_encode($versions_2018,JSON_UNESCAPED_UNICODE);?> || [];

	$(document).ready(function(){
		var year = $('select[name="y"]').val();
		onChangeYear(year);

		$('select[name="y"]').change(function(){
			var year = $(this).val();
			onChangeYear(year);
		});

		function onChangeYear(year) {
			if (year >= 2018) {
				// одинаковые опции в select
				fillSel('first_ver',options2018);
				fillSel('second_ver',options2018);
			} else  {
				// 2016 и 2017 разные наборы опций
				if (year == 2016) {
					fillSel('first_ver',options2016,'-2016');
					fillSel('second_ver',options2016_v2);
				} else if (year == 2017) {
					fillSel('first_ver',options2017,'-2017');
					fillSel('second_ver',options2017_v2);
				}
			}
		} 

		function fillSel(selName,options,defaultVal) {
			clearSel(selName);
			$.each(options,function(id,name){
				selAddOption(selName,id,name);
			});
			if (defaultVal !== undefined) {
				$('select[name="'+selName+'"]').val(defaultVal);
			}
		}

		function clearSel(selName) {
			$('select[name="'+selName+'"] option').each(function(){
				$(this).remove();
			});
		}

		function selAddOption(selName,value,text) {
			$('select[name="'+selName+'"]').append('<option value="'+value+'">'+text+'</option>');
		}
	});

	var links = ["/pivot/pivot.php?type=pfhd", "/pivot/pivot.php?type=pfhd_diff"];
	function showCompareControl() {
		var suffixes = ["ю", "и"];
		var displays = ["none", "inline-block"];
		var ver_check = document.querySelector(".ver_check");
		var form = ver_check.querySelector('form');
		var hidden_fields = ver_check.querySelectorAll(".hidden_field");
		var suffix = ver_check.querySelector("#suffix");
		var showState = 0;
		ver_check.showState = !ver_check.showState;
		showState = (ver_check.showState) ? 1 : 0;
		suffix.innerHTML = suffixes[showState];
		form.action = links[showState];
		for(var i = 0; i < hidden_fields.length; i++) {
			hidden_fields[i].style.display = displays[showState];
		}
	}
	
	window.onload = function() {
		var checkbox = document.querySelector("#compare");
		if(checkbox) {
			if(checkbox.checked) showCompareControl();
		}
	}
</script>
<style type="text/css">
	#ext-body {position: absolute; top: 0; left: 0;}
	select[name="first_ver"] {width: 250px;}
	select[name="second_ver"] {width: 250px;}

	.create_version_div {display: none; text-align: right;}
	.create_version_div input, .create_version_div select {width: 250px; font-size: 18px; box-sizing: border-box; margin: 5px;}

	.admin_buttons {padding-top: 50px;}
	.admin_buttons button {padding: 6px; width: 230px; margin: 0 50px 35px 0; font-size: 20px; cursor: pointer;}

	/* 1dp = 8px */
	.ver_check {
		width: 900px;
		height: auto;
		margin: 16px auto; /* 2dp */
		text-align: center;
	}

	.ver_check form {
		margin: 16px 0; /* 2dp */
	}

	.ver_check select {
		font-size: 20px; /* 3dp */
	}

	.ver_check input[type="submit"], .ver_check input[type="button"] {
		font-size: 22px; /* 2dp - 2px default borders */
	}

	.ver_check input[type="button"] {
		margin-left: 30px;
	}

	.ver_select {
		display: inline-block;
		margin-right: 24px;
	}

	.ver_select p {
		text-align: left;
		margin: 8px 0;
		color: #999;
		font-size: 16px; /* 2dp */
	}

	.hidden_field {display: none;}
</style>