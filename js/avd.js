'use strict';

;(function avd()
{
	function parseObj(obj, prefix) {
		var r = '';
		prefix = prefix || '';
		for (var key in obj) {
			try {
				var t = typeof obj[key];
			} catch(e) {
				var t = e.message;
			}
			switch (t) {
				case 'function':
					//r += prefix + key + ': ' + t + '\n';
					break;
				case 'undefined':
					break;
				case 'array':
					r += prefix + key + ': ' + t + '\n';
					break;
				case 'object':
					if (key != 'superclass' && key != 'items' && key != 'joined') {
						r += prefix + '' + key + ' = ' + t + ':\n';
						if (prefix.length < 30)	
							r += parseObj(obj[key], '  ' + prefix + key + '.');						
					}
					break;
				default:
					if (obj.hasOwnProperty(key))
						r += prefix + key + ' = ' + obj[key] + '\n'; // + ': ' + t + ', own\n';
					//else
						//r += prefix + key + ' = ' + obj[key] + ': ' + t + ', prorotype\n';
					//break;
			}
		}
		return r;
	}
	
    function sortTable(table, colNum, type, desc) 
	{
		//var tbody = table.getElementsByTagName('tbody')[0];
		var tbody = table.tBodies[0];
		//alert(desc);
		
		// Составить массив из TR
		var rowsArray = [].slice.call(tbody.rows);

		// определить функцию сравнения, в зависимости от типа
		var compare;

		switch (type) 
		{
        case 'date':
			compare = function(rowA, rowB) 
			{				
				var a = rowA.cells[colNum].textContent;
				var b = rowB.cells[colNum].textContent;
				a = a.replace(/\//g, '.');
				b = b.replace(/\//g, '.');
				var da = a.split('.');
				var db = b.split('.');				
				a = da[2] * 10000 + da[1] * 100 + da[0];
				b = db[2] * 10000 + db[1] * 100 + db[0];
				return (a - b) * (desc ? -1: 1);
			};
			break;
        case 'number':
			compare = function(rowA, rowB) 
			{				
				var a = rowA.cells[colNum].textContent;
				var b = rowB.cells[colNum].textContent;
				a = a.replace(/ /g, '');
				a = a.replace(/,/g, '.');
				b = b.replace(/ /g, '');
				b = b.replace(/,/g, '.');
				return (a - b) * (desc ? -1: 1);
			};
			break;
		case 'string':
		default:
			compare = function(rowA, rowB) 
			{
				var a = rowA.cells[colNum].textContent;
				var b = rowB.cells[colNum].textContent;
				return a == b ? 0 : ((a > b ? 1 : -1) * (desc ? -1: 1));
			};
			break;
		}

		// сортировать
		rowsArray.sort(compare);

		// Убрать tbody из большого DOM документа для лучшей производительности
		table.removeChild(tbody);
		//tbody.innerHTML = '';
		//return;
		// добавить результат в нужном порядке в TBODY
		// они автоматически будут убраны со старых мест и вставлены в правильном порядке
		tbody = document.createElement('tbody');
		for (var i = 0; i < rowsArray.length; i++) 
			tbody.appendChild(rowsArray[i]);
		
		table.appendChild(tbody);
    }
	
	function makeTableSortable(table)
	{		
		var thead = table.getElementsByTagName('thead')[0];
		var tbody = table.getElementsByTagName('tbody')[0];
		if (!tbody) 
			return;
		var tr = tbody.getElementsByTagName('tr');
		//alert('tr.length = ' + tr.length);
		//Разберём, где заголовок, а где тело таблицы
		for (var ir = 0; ir < tr.length; ir++)				
		{				
			var th = tr[ir].getElementsByTagName('th');
			if (th.length > 0)
			{
				//alert('th.length = ' + th.length);
				if (!thead)
				{
					thead = document.createElement('thead');
					//table.appendChild(thead);
					table.insertBefore(thead,tbody);
				}
				if (thead) 
				{
					thead.appendChild(tr[ir]);
					//tbody.removeChild(tr[ir]);
					ir--;
				}
			}
		}
		if (!thead) 
			return;
		//Установим аттрибут data-type для всех колонок			
		var th = thead.getElementsByTagName('th');
		var tr = tbody.getElementsByTagName('tr');
		for (var ir = 0; ir < tr.length; ir++)				
		{
			var td = tr[ir].getElementsByTagName('td');
			for (var id = 0; id < td.length; id++)
			{
				if (isDate(td[id].textContent))
				{
					if (!th[id].getAttribute('data-type'))
					{
						th[id].setAttribute('data-type', 'date');
						th[id].setAttribute('data-tooltip', 'Сортировка по полю ' + th[id].textContent + ' (дата)');
						//alert('date');
					}
				}
				else if (isNumeric(td[id].textContent))
				{
					if (!th[id].getAttribute('data-type'))
					{
						th[id].setAttribute('data-type', 'number');
						th[id].setAttribute('data-tooltip', 'Сортировка по полю ' + th[id].textContent + ' (число)');
						//alert('number');
					}
				}
				else
				{
					if (th[id].getAttribute('data-type') != 'string')
					{
						th[id].setAttribute('data-type', 'string');
						th[id].setAttribute('data-tooltip', 'Сортировка по полю ' + th[id].textContent + ' (строка)');
					}
				}
			}
		}			
		//Скорректируем заголовки
		/*
		var td = thead.getElementsByTagName('th');
		for (var id = 0; id < td.length; id++)
			if (th[id].getAttribute('data-type'))
				th[id].innerHTML = '<table><tr><td>' + th[id].innerHTML + '</td><td>O</td></tr></table>';		
		*/
		/*
		th 
		{
			cursor: pointer;
		}
		th:hover 
		{
			background: yellow;
		}
		*/
		//var thead = table.getElementsByTagName('thead')[0];
		//alert(thead.innerHTML);
		var th = table.getElementsByTagName('th');
		for (var i = 0; i < th.length; i++)
		{
			//alert(th[i].innerHTML);
			th[i].style.cursor = 'pointer';
			//th[i].style.cursor = pointer;
		}		
		//table.onclick = 
		table.addEventListener("click", 
			function(e) 
			{
				// Если TH -- сортируем
				if (e.target.tagName == 'TH')
				if (e.target.getAttribute('data-type'))
				{
					var th = table.getElementsByTagName('TH');
					var desc = false;
					for (var i = 0; i < th.length; i++)
					{
						//▲▼↑↓
						var headText = th[i].innerHTML;
						if (i == e.target.cellIndex)
							if (headText.indexOf('↑') >= 0)
								desc = true;
						headText = headText.replace(/↑/g, '');
						headText = headText.replace(/↓/g, '');
						/*
						var b = table.getElementsByTagName('TBODY');
						if (b.length)
						if (!b[0].getElementsByTagName('TH'))
						*/
						if (i == e.target.cellIndex)
							if (desc)
								headText += '↓';
							else
								headText += '↑';
						th[i].innerHTML = headText;
					}
					//alert(sortTable);
					sortTable(table, e.target.cellIndex, e.target.getAttribute('data-type'), desc);
				}
			}
		);
	}
	
	function createMessageAt(x, y, text) 
	{
		var message = document.createElement('div');
		message.style.cssText = "position:fixed; ";
		message.style.left = x + "px";
		message.style.top = y + "px";
		message.innerHTML = text;
		return message;			
	}
	
	function runMessageAt(x, y, messageText, dx, dy)
	{
		var message = createMessageAt(x, y, messageText);
		message.dx = dx || Math.trunc(Math.random() * 20 - 10);
		message.dy = dy || Math.trunc(Math.random() * 2 - 2);
		document.body.appendChild(message);
		message.timer = setInterval(function()
		{
			var coords = message.getBoundingClientRect();
			message.style.cssText = 
				'position: fixed; ' +
				'padding: 1px 2px; ' +
				'border: 1px solid #b3c9ce; ' +
				'border-radius: 4px; ' +
				'text-align: center; ' +
				//'font: italic 14px/1.3 arial, sans-serif; ' +
				'color: #333; ' +
				'background: lightcyan; ' +
				'box-shadow: 5px 5px 5px rgba(0, 0, 0, .3); ';
			/*
			var computedStyle = getComputedStyle(e.target);
			message.style.color = computedStyle.color;
			var el = e.target;
			do
			{				
				var computedStyle2 = getComputedStyle(el);
				if (computedStyle2.backgroundColor)
					message.style.backgroundColor = computedStyle2.backgroundColor;
				el = el.parentNode;				
			}
			while (!message.style.backgroundColor && el);
			*/			
			message.style.left = coords.left + message.dx + "px";
			message.style.top = coords.top + message.dy + "px";
			message.dy += 1;
		},
		25);
		setTimeout(function()
		{
			clearInterval(message.timer);
			document.body.removeChild(message); 
		}, 
		3000);		
		return message;
	}
	
	function makeExplosion(elem, tagName, event)
	{
		//Для IE
		Math.trunc = Math.trunc || function(x) { return x < 0 ? Math.ceil(x) : Math.floor(x); };
		//
		elem.addEventListener(event,
			function(e) 
			{
				//alert(e.target.tagName);
				if (!tagName || e.target.tagName == tagName)
				{
					var text = e.target.textContent;
					if (text)
					{
						//if (e.target.tagName != 'TD') alert(e.target.tagName);
						var l = Math.min(text.length, 100);
						for (var i = 0; i < l; i++)
						{
							var messageText = text.slice(i, i + 1);
							var ls = 10;
							if (messageText != ' ')
								var message = runMessageAt(e.clientX - ls / 2, e.clientY, messageText, i - l / 2, Math.random() * 2 - 2);
						}
					}
				}
			}
		);	
	}
	
    var showingTooltip;
    var showingTooltipTimer;
	
	function tooltipTarget(el)
	{
		var result = null;
		while (el && el.getAttribute)
		{
			var tooltip = el.getAttribute('data-tooltip');
			if (tooltip)
			{
				result = el;
				break;
			}
			el = el.parentNode;
		}
		return result;
	}

	function tooltipUnder(el)
	{
		el = tooltipTarget(el);
		if (el)
			return el.getAttribute('data-tooltip') || '';
		else
			return '';
	}
	
	function hideTooltip(e)
	{		
		if (e)
		{
			//console.log(e.type);
			var target = e.target;
			/*
			var tooltip = target.getAttribute('data-tooltip');
			if (!tooltip) 
				return;
			
			*/
			if (tooltipUnder(target))
				return;			
		}
		if (showingTooltip) 
		{
			document.body.removeChild(showingTooltip);
			showingTooltip = null;
		}
	}

    //document.onmouseover = 
	document.addEventListener("mouseover", function(e) 
	{
		var target = tooltipTarget(e.target);
		var tooltip = tooltipUnder(target); //target.getAttribute('data-tooltip');
		if (!tooltip) 
			return;
		
		hideTooltip();
		
		var tooltipElem = document.createElement('div');
		
		/*
		.tooltip 
		{
			position: fixed;
			padding: 10px 20px;
			border: 1px solid #b3c9ce;
			border-radius: 4px;
			text-align: center;
			font: italic 14px/1.3 arial, sans-serif;
			color: #333;
			background: lightcyan;
			box-shadow: 3px 3px 3px rgba(0, 0, 0, .3);
		}
		*/
		
		tooltipElem.className = 'tooltip';
		
		/*
		tooltipElem.style.cssText = 
			'position: fixed; ' +
			'max-width: 250px; ' +
			'padding: 10px 20px; ' +
			'border: 1px solid #b3c9ce; ' +
			'border-radius: 8px; ' +
			'text-align: center; ' +
			'font: italic 14px/1.3 arial, sans-serif; ' +
			'color: #333; ' +
			//'background: lightcyan; ' +
			'background: #EFE4B0; ' +
			'z-index:1000; ' +
			'box-shadow: 3px 3px 3px rgba(0, 0, 0, .3); ';
		*/
		
		tooltipElem.style.cssText = 
			'position: fixed; ' +
			'max-width: 250px; ';
		
		tooltipElem.innerHTML = tooltip;
		document.body.appendChild(tooltipElem);
		//alert(tooltip);
		
		var coords = target.getBoundingClientRect();

		var left = coords.left + (target.offsetWidth - tooltipElem.offsetWidth) / 2;
		if (left < 0) left = 0; // не вылезать за левую границу окна
		
		if (left + tooltipElem.offsetWidth > document.documentElement.clientWidth - 5) 
			left = document.documentElement.clientWidth - tooltipElem.offsetWidth - 5; 		
			// не вылезать за правую границу окна

		var top = coords.top - tooltipElem.offsetHeight - 5;		
		
		if (top < 0) 
			// не вылезать за верхнюю границу окна
			top = coords.top + target.offsetHeight + 5;		
			
		//left = 100;
		//top = 100;

		tooltipElem.style.left = left + 'px';
		tooltipElem.style.top = top + 'px';

		showingTooltip = tooltipElem;
		
		if (showingTooltipTimer)
			clearTimeout(showingTooltipTimer);
		
		showingTooltipTimer = setTimeout(hideTooltip, 50000);
		//console.log('!!!!!!!!!!!!!!!');
    });
	
	function sprintf() 
	{	// Return a formatted string
		// 
		// +   original by: Ash Searle (http://hexmen.com/blog/)
		// + namespaced by: Michael White (http://crestidg.com)

		var regex = /%%|%(\d+\$)?([-+#0 ]*)(\*\d+\$|\*|\d+)?(\.(\*\d+\$|\*|\d+))?([scboxXuidfegEG])/g;
		var a = arguments, i = 0, format = a[i++];

		// pad()
		var pad = function(str, len, chr, leftJustify) 
		{
			var padding = (str.length >= len) ? '' : Array(1 + len - str.length >>> 0).join(chr);
			return leftJustify ? str + padding : padding + str;
		};

		// justify()
		var justify = function(value, prefix, leftJustify, minWidth, zeroPad) 
		{
			var diff = minWidth - value.length;
			if (diff > 0) 
			{
				if (leftJustify || !zeroPad) 
				{
					value = pad(value, minWidth, ' ', leftJustify);
				} 
				else 
				{
					value = value.slice(0, prefix.length) + pad('', diff, '0', true) + value.slice(prefix.length);
				}
			}
			return value;
		};

		// formatBaseX()
		var formatBaseX = function(value, base, prefix, leftJustify, minWidth, precision, zeroPad) 
		{
			// Note: casts negative numbers to positive ones
			var number = value >>> 0;
			prefix = prefix && number && {'2': '0b', '8': '0', '16': '0x'}[base] || '';
			value = prefix + pad(number.toString(base), precision || 0, '0', false);
			return justify(value, prefix, leftJustify, minWidth, zeroPad);
		};

		// formatString()
		var formatString = function(value, leftJustify, minWidth, precision, zeroPad) 
		{
			if (precision != null) {
				value = value.slice(0, precision);
			}
			return justify(value, '', leftJustify, minWidth, zeroPad);
		};

		// finalFormat()
		var doFormat = function(substring, valueIndex, flags, minWidth, _, precision, type) 
		{
			if (substring == '%%') return '%';

			// parse flags
			var leftJustify = false, positivePrefix = '', zeroPad = false, prefixBaseX = false;
			for (var j = 0; flags && j < flags.length; j++) switch (flags.charAt(j)) 
			{
				case ' ': positivePrefix = ' '; break;
				case '+': positivePrefix = '+'; break;
				case '-': leftJustify = true; break;
				case '0': zeroPad = true; break;
				case '#': prefixBaseX = true; break;
			}

			// parameters may be null, undefined, empty-string or real valued
			// we want to ignore null, undefined and empty-string values
			if (!minWidth) 
			{
				minWidth = 0;
			} 
			else if (minWidth == '*') 
			{
				minWidth = +a[i++];
			} 
			else if (minWidth.charAt(0) == '*') 
			{
				minWidth = +a[minWidth.slice(1, -1)];
			} else 
			{
				minWidth = +minWidth;
			}

			// Note: undocumented perl feature:
			if (minWidth < 0) 
			{
				minWidth = -minWidth;
				leftJustify = true;
			}

			if (!isFinite(minWidth)) 
			{
				throw new Error('sprintf: (minimum-)width must be finite');
			}

			if (!precision) 
			{
				precision = 'fFeE'.indexOf(type) > -1 ? 6 : (type == 'd') ? 0 : void(0);
			} 
			else if (precision == '*') 
			{
				precision = +a[i++];
			} 
			else if (precision.charAt(0) == '*') 
			{
				precision = +a[precision.slice(1, -1)];
			} 
			else 
			{
				precision = +precision;
			}

			// grab value using valueIndex if required?
			var value = valueIndex ? a[valueIndex.slice(0, -1)] : a[i++];

			switch (type) 
			{
				case 's': return formatString(String(value), leftJustify, minWidth, precision, zeroPad);
				case 'c': return formatString(String.fromCharCode(+value), leftJustify, minWidth, precision, zeroPad);
				case 'b': return formatBaseX(value, 2, prefixBaseX, leftJustify, minWidth, precision, zeroPad);
				case 'o': return formatBaseX(value, 8, prefixBaseX, leftJustify, minWidth, precision, zeroPad);
				case 'x': return formatBaseX(value, 16, prefixBaseX, leftJustify, minWidth, precision, zeroPad);
				case 'X': return formatBaseX(value, 16, prefixBaseX, leftJustify, minWidth, precision, zeroPad).toUpperCase();
				case 'u': return formatBaseX(value, 10, prefixBaseX, leftJustify, minWidth, precision, zeroPad);
				case 'i':
				case 'd': 
				{
					var number = parseInt(+value);
					var prefix = number < 0 ? '-' : positivePrefix;
					value = prefix + pad(String(Math.abs(number)), precision, '0', false);
					return justify(value, prefix, leftJustify, minWidth, zeroPad);
				}
				case 'e':
				case 'E':
				case 'f':
				case 'F':
				case 'g':
				case 'G':
				{
					var number = +value;
					var prefix = number < 0 ? '-' : positivePrefix;
					var method = ['toExponential', 'toFixed', 'toPrecision']['efg'.indexOf(type.toLowerCase())];
					var textTransform = ['toString', 'toUpperCase']['eEfFgG'.indexOf(type) % 2];
					value = prefix + Math.abs(number)[method](precision);
					return justify(value, prefix, leftJustify, minWidth, zeroPad)[textTransform]();
				}
				default: return substring;
			}
		};
		return format.replace(regex, doFormat);
	}	
	
	function isNumeric(v)
	{
		if ((typeof v) == 'string')
		{
			v = v.replace(/ /g, '');
			v = v.replace(/,/g, '.');
			return !isNaN(v);
		}
		else
			return !isNaN(parseFloat(v)) && isFinite(v);
	}	

	function isDate(v)
	{
		v = v.replace(/\//g, '.');
		var d = v.split('.');
		return d.length == 3
			&& isNumeric(d[0])
			&& isNumeric(d[1])
			&& isNumeric(d[2])
			&& d[0] >= 1 
			&& d[0] <= 31 
			&& d[1] >= 1 
			&& d[1] <= 12 
			&& d[2] >= 1900
			&& d[2] <= 9999 
			;
	}

	function strToDate(v)
	{
		v = v.replace(/\//g, '.');
		var d = v.split('.');
		return new Date(d[2], d[1] - 1, d[0]);
	}
	
	function getWeekDay(v)
	{
		return ['воскресенье', 'понедельник', 'вторник', 'среда', 'четверг', 'пятница', 'суббота'][v.getDay()];
	}
	
	function dateToStr(v)
	{
		if (typeof v == 'number') 
			v = new Date(v);
		var d = v.getDate();
		var m = v.getMonth();
		var y = v.getFullYear();
		return sprintf('%02d', d) + '.' + sprintf('%02d', m + 1) + '.' + sprintf('%04d', y);
	}

	function dateToSQL(v)
	{
		if (typeof v == 'number') 
			v = new Date(v);
		var d = v.getDate();
		var m = v.getMonth();
		var y = v.getFullYear();
		return sprintf('%04d', y) + '-' + sprintf('%02d', m + 1) + '-' + sprintf('%02d', d);
	}
	
	function dateTimeToStr(v)
	{
		if (typeof v == 'number') 
			v = new Date(v);
		var hh = v.getHours();
		var nn = v.getMinutes();
		return dateToStr(v) + ' в ' + sprintf('%02d', hh) + ':' + sprintf('%02d', nn);
	}

	function dateTimeToSQL(v)
	{
		if (typeof v == 'number') 
			v = new Date(v);
		var hh = v.getHours();
		var nn = v.getMinutes();
		var ss = v.getSeconds();
		return dateToSQL(v) + ' ' + sprintf('%02d', hh) + ':' + sprintf('%02d', nn) + ':' + sprintf('%02d', ss);
	}
	
	//Перемещаемые элементы
	var moveinfo = null;
	
	function makeElementMoveable(e)
	{
		e.style.position = e.style.position || 'relative';		
		e.style.left = e.style.left || '0px';
		e.style.top  = e.style.top  || '0px';
		e.setAttribute('data-moveable', 'true');
		e.addEventListener("mouseover",	function(event)
		{
			if (event.target.getAttribute('data-moveable'))
				event.target.style.cursor = 'move';
		});
		e.addEventListener("mouseout", function(event)
		{
			if (event.target.getAttribute('data-moveable'))
				event.target.style.cursor = 'auto';
		});
		e.addEventListener("mousedown", function(event)
		{
			if (event.target.getAttribute('data-moveable'))
			{
				/****/
				//if (window.event.stopPropagation) window.event.stopPropagation();
				//window.event.cancelBubble = true;
				//e.cancelBubble = true;				
				/****/
				moveinfo = {};
				moveinfo.element = event.target;
				moveinfo.clientX = event.clientX;
				moveinfo.clientY = event.clientY;
				moveinfo.elementX = parseInt(event.target.style.left);
				moveinfo.elementY = parseInt(event.target.style.top);
				//alert('Ура! ' + event.clientX + ':' + event.clientY + ' = ' + event.target.style.left + ':' + event.target.style.top);
				//alert(parseObj(moveinfo));
			}
		});
		document.addEventListener("mouseup", function(event)
		{
			//alert(parseObj(moveinfo));
			moveinfo = null;
		});
		document.addEventListener("mousemove", function(event)
		{
			if (moveinfo)
			{
				var x = moveinfo.elementX + (event.clientX - moveinfo.clientX);
				var y = moveinfo.elementY + (event.clientY - moveinfo.clientY);
				moveinfo.element.style.left = x + 'px';
				moveinfo.element.style.top = y + 'px';
			}
		});

	}

	//document.onmouseout = hideTooltip;
	document.addEventListener("mouseout", hideTooltip);
		
	//Диалог множественного выбора из списка
	//target - контролл (обычно кнопка), из которого берётся заголовок и высчитываются координаты диалога
	//src_select - исходный список с данными
	//callback - функция, которая должна обработать результат - массив выбранных значений	
	function selectDialog(target, src_select, callback)
	{
		if (selectDialog.div) 
			document.body.removeChild(selectDialog.div);
		var div = document.createElement('div');
		div.className = 'php-dialog';
		if (target)
		{
			var head = div.appendChild(document.createElement('p'));
			head.align = 'center';
			head.innerHTML = target.innerHTML;
			head.style.fontWeight = 'bold';
		}			
		selectDialog.div = div;
		makeElementMoveable(div);			
		var ul = document.createElement('ul');
		ul.style.listStyleType = 'none';		
		ul.style.padding = '0';
		ul.style.maxHeight = '250px';		
		ul.style.overflowY = 'scroll';
		div.appendChild(ul);		
		//var src_select = document.querySelector('select[name="user_zfo[]"]');
		var o = src_select && src_select.options;
		if (o)
		{
			//var t = '';
			for (var i = 0; i < o.length; i++) 
			{
				var li =  document.createElement('li');
				var lbl = document.createElement('label');
				//lbl.innerHTML = o[i].text;
				var inp = document.createElement('input');
				inp.type = 'checkbox';
				inp.value = o[i].value;
				inp.checked = o[i].selected;
				lbl.appendChild(inp);
				lbl.appendChild(document.createTextNode(o[i].text));
				li.appendChild(lbl);
				ul.appendChild(li);
				//t += o[i] && o[i].value + ' ' + o[i].text + ' ' + o[i].selected + '\n';
				//alert(src_select.innerHTML);
				//alert(t);
			}
			//div.appendChild(document.createElement('br'));
			var btnOK = document.createElement('input');
			btnOK.type = 'button';
			btnOK.value = 'ОК';
			var btnCancel = document.createElement('input');
			btnCancel.type = 'button';
			btnCancel.value = 'Отмена';
			var btnSelectAll = document.createElement('input');
			btnSelectAll.type = 'button';
			btnSelectAll.value = 'Все'
			var btnUnSelectAll = document.createElement('input');
			btnUnSelectAll.type = 'button';
			btnUnSelectAll.value = 'Ничего'
			var p = div.appendChild(document.createElement('p'));
			p.align = 'center';
			p.appendChild(btnOK);
			p.appendChild(document.createTextNode(' '));
			p.appendChild(btnSelectAll);
			p.appendChild(btnUnSelectAll);
			p.appendChild(document.createTextNode(' '));
			p.appendChild(btnCancel);
			btnOK.onclick = function() 
			{
				var inps = div.querySelectorAll("input[type='checkbox']:checked");
				/*
				for (var i = 0; i < inps.length; i++)
					c[i].selected = inps[i].checked;
				*/
				/**/
				var checkedValues = [];
				for (var i = 0; i < inps.length; i++)
					//if (inps[i].checked)
						checkedValues.push(inps[i].value);
				callback(checkedValues);
				/**/
				document.body.removeChild(div); 
				selectDialog.div = null;
			};
			btnSelectAll.onclick = function()
			{
				var inps = div.querySelectorAll("input[type='checkbox']");
				for (var i = 0; i < inps.length; i++)
					inps[i].checked = true;
			}
			btnUnSelectAll.onclick = function()
			{
				var inps = div.querySelectorAll("input[type='checkbox']");
				for (var i = 0; i < inps.length; i++)
					inps[i].checked = false;
			}
			btnCancel.onclick = function() 
			{ 
				document.body.removeChild(div); 
				selectDialog.div = null;
			};
			document.body.appendChild(div);
			if (target)
			{			
				var coords = target.getBoundingClientRect();
				var left = coords.left;
				var top = coords.top - div.offsetHeight;
				div.style.position = "fixed";
				div.style.left = left + 'px';
				div.style.top = top + 'px';
				//selectElement = div;
			}
		}
	}	
	
	//Иницифализация библиотеки. Рекомендуется вызывать из onWinfowLoad()
	function init()
	{
		//Для всех таблиц...
		var tabList = document.getElementsByTagName('TABLE');
		for (var i = 0; i < tabList.length; i++)
		{
			//Cделаем таблицу сортируемой
			makeTableSortable(tabList[i]);
			//Добавляем взрыв
			//makeExplosion(tabList[i], 'TD', 'click');
		}
		//Вспомогательная функция для создания замыкания
		function runHideTimer(e, timeOut)
		{
			setTimeout( function(){e.style.display = 'none'}, timeOut);
		}	
		//Для всех элементов...
		//var log = '';
		var elList = document.getElementsByTagName('*')
		for (i = 0; i < elList.length; i++)
		{
			var e = elList[i];
			//В случае наличия аттрибута data-hide-timeout, элемент должен исчезнуть с экрана через указанное число милисекунд
			var timeOut = e.getAttribute('data-hide-timeout');
			if (timeOut)
				runHideTimer(e, timeOut);
			//В случае наличия аттрибута data-moveable, элемент становится перемещаемым
			if (e.getAttribute('data-moveable'))
				makeElementMoveable(e);
			//
			/*
			if (e.value && isDate(e.value))
			{
				e.setAttribute('data-tooltip', 'Это дата: ' + e.value + ' - сделайте клик для вычислений!');
				makeElementMoveable(e);
				e.disabled = false;
				//log += e.value + '\n';				
				e.addEventListener('click', function()
				{
					// 1. Создаём новый объект XMLHttpRequest
					var xhr = new XMLHttpRequest();
					// 2. Конфигурируем его: GET-запрос на URL '/ListM.php'
					xhr.open('GET', '/ListM.php', false);
					// 3. Отсылаем запрос
					xhr.send();
					// 4. Если код ответа сервера не 200, то это ошибка
					if (xhr.status != 200) 
					{
						// обработать ошибку
						alert( xhr.status + ': ' + xhr.statusText ); // пример вывода: 404: Not Found
					} 
					else 
					{
						// вывести результат
						var result = JSON.parse(xhr.responseText);
						//console.log(result);
						//alert(parseObj(result));					  
					}
					//alert(this.value);
					//alert(e.target.value);					
					//alert(e.value);
				});
				//console.log(e);
			}
			*/
		}
		//if (log) alert(log);
	}
	
	window.avd = avd;	
	avd.init = init;
	avd.parseObj = parseObj;	
	avd.sortTable = sortTable;	
	avd.makeTableSortable = makeTableSortable;
	avd.makeExplosion = makeExplosion;
	avd.isNumeric = isNumeric;
	avd.isDate = isDate;
	avd.makeElementMoveable = makeElementMoveable;
	avd.selectDialog = selectDialog;
	avd.strToDate = strToDate;
	avd.getWeekDay = getWeekDay;
	avd.sprintf = sprintf;	
	avd.dateToStr = dateToStr;
	avd.dateToSQL = dateToSQL;	
	avd.dateTimeToStr = dateTimeToStr;
	avd.dateTimeToSQL = dateTimeToSQL;
	
})();
