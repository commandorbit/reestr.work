<?php
// stat_functions включить в класс наверное
//require __DIR__ .'/stat_functions.php';
function makeStat($filters,$colrows) {
	$s = new Stat($filters,$colrows);
	return $s;
}
Class Stat {
	const ALL='_all_';
	const NULL='_null_';
	private $col,$row;  
	private $cols,$rows; // массивы для выбора колонок и строк
	private $filters;
	private $f; //условия фильтров
	private $has_d; //предлагать вывод на дату
	private $d; // на дату
	private $func_data; //функция для данных
	private $url;
	
	public function __construct($filters,$colrows) {		
		$this->has_d=true;
		$this->rows = $this->cols =	$this->filters = array();
		foreach ($colrows as $id=>$v) {
			$this->cols[$id] = $this->rows[$id] = $v['label'];
		}
		/* вытаскиваем имеющиеся аттрибуты и создаем массив столбцов, колонок и фильтров */
		//$tmp_cols = array_merge($colrows,$filters);
		$tmp_cols = array_merge($colrows,$filters);
		foreach ($tmp_cols as $id=>$v) {
			$default = (!empty($v['default_val'])) ? $v['default_val'] : self::ALL;

			if (isset($v['func_option_array']) && !empty($v['func_option_array'])) {
				$list=call_user_func($v['func_option_array']);
			} elseif (isset($v['options']) && !empty($v['options'])) {
				$list=$v['options'];
			} else if(isset($v['date_options']) && !empty($v['date_options'])) {
				$list=$v['date_options'];
			} else {
				$list=array();
			}
			$list = $this->_array_merge(array(self::NULL=>'(Пустые)'),$list);

			if(isset($v['date_options']) && !empty($v['date_options']))
				$this->filters[$id] = array('label'=>$v['label'],'list'=>$list, 'type'=>'date', 'default'=>array($default));
			else
				$this->filters[$id] = array('label'=>$v['label'],'list'=>$list, 'default'=>array($default));
		}
	}
	public function set_has_d($b) {
		$this->has_d=$b;
		return $this;
	}
	public function set_row_col($row,$col) {
		$this->row = $row;
		$this->col = $col;
		return $this;
	}
	private function _array_merge($a1,$a2) {
		$a=array();
		foreach ($a1 as $key=>$val) $a[$key]=$val;
		foreach ($a2 as $key=>$val) $a[$key]=$val;
		return $a;
	}
	public function set_url($url){
	$this->url=$url;
	return $this;
	
	}
	
	// $params-$_request
	// устанвливает значения фильтров и полей для условий
	// prefix w_
	public function set_f($params,$prefix='') {
		$this->f = array();
		foreach ($params as $id=>$value) {	
			if (substr($id,0,strlen($prefix))==$prefix) {
				$id = substr($id,strlen($prefix));
				if (!self::value_is_all($value)) {
					$this->f[$id]=$value;
				}
				$this->filters[$id]['default'] = $value; // переменные для defaults фильтров обновляем даже если all!
			}
		}
		return $this;		
	}
	
	public function set_d($d) {
		$this->d=$d;
		return $this;		
	}
	
	public static function value_is_all($v) {
		if (is_array($v)) {
			return (count($v)==1 && $v[0]==self::ALL);
		} else return ($v==self::ALL);
	}
	
	public function opisanie() {
		$opisanie = '';
		foreach ($this->f as $id=>$value) {
			$flt=$this->filters[$id];

			$opisanie .= $flt['label'] . ': ';
			$op = array();
			if (!is_array($value)) $value=array($value);
			foreach ($value as $v)
				if ($v == self::NULL) $op[] = 'нет значения';
				elseif (is_numeric($v) || array_key_exists('type', $flt) && $flt['type'] == 'date')  {
					$simpleDate = substr($v, 6);

					if(substr($v, 0, 6) == '_LIKE_') {
						$dateSlice = explode('-', $simpleDate);

						foreach ($dateSlice as $key => $value) {
							if($value == '__') {
								unset($dateSlice[$key]);
							}
						}

						$v = implode('-', $dateSlice);
					}

					$op[] = isset($flt['list'][$v]) ? $flt['list'][$v] : $v;
				}


			$opisanie .= implode($op,', ') .';<br>';
		}
		if (!empty($this->d)) $opisanie .= ('На дату: ' . $this->d .'<br>');
		return $opisanie;
	}
	
	public function to_excel($filename="stat.xls") {
		$cross=$this->get_data();
		header('Content-type: application/ms-excel');
		header('Content-Disposition: attachment; filename='.$filename);		
		foreach ($cross as $n_tr => $tds) {
			foreach ($tds as $n_td => $data) {
				$data = strip_tags($data);
				if ($data == self::NULL) $data = '';
				echo iconv("UTF-8","cp1251",$data) . "\t";
			}
			echo "\n";
		}
	}
	
	public  function print_cross() {
		$cross=$this->get_data();

		echo '<table summary="">';
		foreach ($cross as $n_tr => $tds) {
			echo "<tr>";
			foreach ($tds as $n_td => $data) {
				if ($data == 'null') $data = '';
				if ($n_tr==0) {
					echo '<th></th><th colspan="'.(count($cross[1])).'">' . $data . '</th>';
				} elseif ($n_td==1 or $n_tr==1) {
					echo '<th>' . $data . '</th>';
				} else echo '<td style="text-align: right; white-space: nowrap;">'  . $data . '</td>';
			}
			echo "</tr>\n";
		}
		echo "</table>";
	}
	public function set_func_data($func) {
		$this->func_data=$func;
		return $this;
	}
	public function get_data() {
		$field1=$this->col;
		$field2=$this->row;
		$f=array($field1,$field2);
		$sql_pre = call_user_func($this->func_data,$f,$this->f,$this->d);
		$sql = " select $field1,$field2,sum(s) as v from ($sql_pre) pre group by $field1,$field2 ";
		$res = sql_query($sql);
		$cross = $this->to_cross($res,$this->col,$this->row);
		$cross = $this->make_cross ($cross,$field1,$field2,$this->f,$this->d);
		return $cross;
	}
	
	public function out_form($form_php='stat_form.php') {
		//ob_start();
		require  __DIR__ .'/'.$form_php;
		//$view=ob_get_clean();
		//echo $view;
		//echo '<pre>';
		//print_r($this->filters);
		//echo '</pre>';

	}
	
	private function to_cross($res,$row_name,$col_name) {
		$a=array();
		while ($row=sql_array($res)) {
			$i=$row[$row_name];
			$j=$row[$col_name];
			if (!isset($a[$i][$j])) $a[$i][$j]=0;
			$a[$i][$j]+=$row['v'];
		}
		return $a;
	}
	private function td_url($a_td=array()) {
		
		//$url='/stat/stat_spisok.php?d='.$this->d.'&amp;';
		$url=$this->url;
		$prefix='f_';
		$w=$this->f;
		$p=array();
		foreach ($w as $key=>$val) {
			$p[$prefix.$key]= $val; // Переименовываем
		};
		
		foreach ($a_td as $key=>$val) {
			$p[$prefix.$key]=((string)$val=='') ? self::NULL : $val; // перекрываем если были
		};
		$url .= http_build_query($p);
		return $url;
	}
		// вынести в какой-то другой модуль 
	// нужны ли условия на поле типа date??
	// определить константу null
	public static function where($w) {
		$where='';
		foreach ($w as $field=>$val) {
			if (is_array($val)) {
				$has_null = false;
				$has_like = false;
				foreach ($val as $key=>$v) {
					$like = substr($v, 0, 6);

					if ($v==self::NULL) {
						$has_null = true;
                        $val[$key]="''";
					} else if($like == '_LIKE_') {
						$has_like = true;
						$val[$key] = "'" . sql_escape(substr($v, 6, strlen($v))) . "'";
					} else {
						$val[$key] = "'" . sql_escape($v) ."'";
					}
				}

				if($has_like) {
					$w_like = '';
					foreach ($val as $k => $v) {						 
						$w_like .=" OR `$field` LIKE " . $val[$k] . "";
					}
					$where .= " AND  (" . substr($w_like,3) .")";
				} else {
					$s = implode(",",$val);

					if (!$has_null) {
						$where .=" AND `$field` in ($s) ";
					} else {
						$where .=" AND (`$field` in ($s) or `$field` is null)";
					}
				}
				
			} else {
				if ($val == self::NULL) $where .=" AND (`$field`='' or `$field` is null)";
				else $where .=" AND `$field`='" .sql_escape($val) ."'";
			}
		}
		if (strlen($where)>0) $where=substr($where,4);

		return $where;
	}
	
	private function make_cross($cross,$row_name,$col_name) {
		
		//$attrs = stat_attrs();
		$cols=array();
		//print_r($this->filters);
		foreach($cross as $data) {
			foreach ($data as $id=>$val) {
				if (!array_key_exists($id,$cols)) {
			//		echo $id .'<br>';

					$list=isset($this->filters[$col_name]) ?  $this->filters[$col_name]['list'] : false;
					$cols[$id] = isset($list[$id])? $list[$id]:$id;
				}
			}
		}
		$it=array();
		foreach($cols as $col=>$dummy) 
			$it[$col]=0;
		asort($cols);
		
		$a = array();
		/// !!!!!!!!!!!!!!!!!!!!!!!!!!!! ВЕРНУТЬ
		$a[0][1] = $this->filters[$col_name]['label'];
		$n_td = 1;
		$a[1][$n_td] = $this->filters[$row_name]['label'];
		foreach($cols as $col) {
			$n_td++;
			$a[1][$n_td] = $col;
		}
		$n_td++;
		$a[1][$n_td] = 'Итого';
		$n_tr = 2;
		foreach($cross as $id=>$data) {
			$n_tr++;
			$list=$this->filters[$row_name]['list'];
			$a[$n_tr][1] =  isset($list[$id])? $list[$id]:$id;
			$s = 0;
			$n_td = 1;
			foreach ($cols as $col=>$dummy) {
				$n_td++;
				if (isset($data[$col])) {
					$s+=$data[$col];
					$it[$col]+=$data[$col];
					$a[$n_tr][$n_td] = '<a href="' .$this->td_url(array($row_name=>$id,$col_name=>$col)) .'" target="_blank">'.number_format(floatval($data[$col]),2,',',' ').'</a>';
				} else $a[$n_tr][$n_td] = '';
			}
			$n_td++;
			$a[$n_tr][$n_td] = '<a href="' . $this->td_url(array($row_name=>$id)).'" target="_blank">'.number_format(floatval($s),2,',',' ').'</a>';
		}
		$n_tr++;
		$n_td = 1;
		$a[$n_tr][$n_td] = 'ВСЕГО';
		$s = 0;
		foreach ($cols as $col=>$dummy) {
			$n_td++;
			$s += $it[$col];
			$a[$n_tr][$n_td] = '<a href="' . $this->td_url(array($col_name=>$col)).'" target="_blank">' . number_format(floatval($it[$col]),2,',',' ') . '</a>';
		}
		$n_td++;
		$a[$n_tr][$n_td]='<a href="' . $this->td_url().'" target="_blank">'.number_format(floatval($s),2,',',' ').'</a>';
		return $a;
	}
}

	


