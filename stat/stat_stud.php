<?php

// вынести в sql ??
function sql_age($field_name,$d) {
	$d = empty($d) ? 'CURRENT_DATE' : sql_rus2date($d);
	return "((YEAR($d) - YEAR(`$field_name`)) - (DAYOFYEAR($d) < DAYOFYEAR(`$field_name`)))";
}


function make_stud_sql($f,$w,$d) {
	if	(empty($d))
		$from_sql='decanat_stud' ;
	else {
		$hist_fields=array_unique(array_merge(array_keys($w),$f));
		$from_sql=  '(' . decanat_stud_d_sql($hist_fields,$d) .')';
	}
	
	$attrs = stat_attrs();
	foreach ($f as $key=>$val) {
		if (!empty($attrs[$val]['alias_name'])) 
			$f[$key]= $attrs[$val]['alias_name'] . '.' .$val;
		if ($val=='age') $f[$key]= __age($d);
	}
	$where=Stat::where($w);
	if (strstr($where,'age')) $where = str_replace("age",__age_in_where($d),$where);
	$sql = "select " . implode(",",$f) . "	from  " . $from_sql . "  ds
		inner join abitur a on ds.stud_kod=a.abitur_kod 
		";
	if ($where)  $sql.= ' WHERE ' .$where;
	return $sql;
}

function decanat_stud_d_sql($fields,$d) {
	$sql='select stud_kod  ';
	$attrs = stat_attrs();
	$dd=sql_rus2date($d);
	foreach ($fields as $field) {
		if (isset($attrs[$field]['alias_name']) && $attrs[$field]['alias_name']=='ds') {
			if (! $attrs[$field]['has_history'] ) {
				$sql.=" ,$field";
			} else {
				$sql_from =" prikaz_sostav ps inner join prikaz_nomer pn on ps.prikaz_nomer_kod=pn.prikaz_nomer_kod ";
				$field_w_alias='ps.'.$field;
				if ($field=='status_kod') {
					$sql_from .= ' inner join s_prikaz_type pt on ps.prikaz_type_kod=pt.prikaz_type_kod ';	
					$field_w_alias='pt.'.$field;	
				}
				$sql.=" , (select $field_w_alias from $sql_from
						where abitur_kod=stud_kod and pn.prikaz_data<$dd and $field_w_alias is not null 
						order by pn.prikaz_data desc limit 1) $field ";
			}
		}
	}
	$sql.=' from decanat_stud ';
	return $sql;
}

function __age($d='') {
	return (sql_age('data_rogdenia',$d) ." as age");
}

function __age_in_where($d='') {
	return sql_age('data_rogdenia',$d);
}


function age_array() {
	$age = __age();
	$s = "SELECT DISTINCT $age, $age FROM abitur a INNER JOIN decanat_stud ds ON a.abitur_kod=ds.stud_kod WHERE data_rogdenia is not null ORDER BY age";
	return sql_to_assoc($s);
}

function stat_attrs() {
	$a = array();
	$s = "SELECT field_name, alias_name, label, has_history, has_func, func_name, func_option_array, default_val, ord 
	FROM s_field_attributes ORDER BY ord";
	$res = sql_query($s);
	while ($row = sql_array($res)) {
		$a[$row['field_name']] = $row;
	}
	return $a;
}