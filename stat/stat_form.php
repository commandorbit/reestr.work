<!-- MULTISELECT PLUGIN -->
<link rel="stylesheet" type="text/css" href="/assets/js/tree-plugin/css/treeplugin.css">
<script src="/assets/js/tree-plugin/js/treeplugin.js" type="text/javascript"></script>
<script src="/assets/js/multiselect-plugin/modalWindowBody.js" type="text/javascript"></script>
<script src="/assets/js/multiselect-plugin/multiselect.js" type="text/javascript"></script>  

<style type="text/css">
	body{overflow: scroll;}
</style>

	<script type="text/javascript">
		$(function() {
			$(".stat_filter").show();
		});
	</script>
	<form method="get" action="" style="margin-top: 10px;">
		<div class="stat_filter" style="display: none;">
			<span>
				<label for="row_id">В строках:</label>
				<select name="row" id="row_id">
					<? 	print_options($this->cols,$this->col); ?>
				</select>
				<label for="col_id">В столбцах:</label>
				<select name="col" id="col_id">
					<?	print_options($this->rows,$this->row); ?>
				</select>
				<? if ($this->has_d) : ?>
					<label for="d_id">На дату:</label>
					<input name="d" value="<?=$this->d ?>" class="date" id="d_id">
				<? endif ?>
			</span>
			<h3 style="margin: 20px 0;">Условия:</h3>
			<table class="ms-table" summary="">
	<?
	$n = 0;
	foreach ($this->filters as $field=>$v) {
		$n++;
		if ($n%3==1) echo '<tr>';
		echo '<td><label class="select_label" for="'.$field.'_id">'.$v['label'].': </label></td>';
		if(isset($v['type']))
			echo '<td style="padding: 10px"><select class="multiselect" data-type="'.$v['type'].'" size="1" multiple name="w_'.$field.'[]" id="'.$field.'_id">';
		else
			echo '<td style="padding: 10px"><select class="multiselect" size="1" multiple name="w_'.$field.'[]" id="'.$field.'_id">';
		//	print_r($v['list']);
		print_options($v['list'],$v['default']);
		echo '</select></td>';
		if ($n%3==0) echo "</tr>\n";
	}
	?>

			<tr><td></td>
				<td style="padding: 10px;"><input type="button" class="input_button" value="Очистить фильтры" onClick="location.href='stat_zayav.php'"></td>
				<td></td>
				<td style="padding: 10px;"><input type="button" class="input_button" name="show" onClick="send()" value="Показать"></td>	
				<td></td>
				<td style="padding: 10px;"><input type="button" class="input_button" onClick="this.setAttribute('name', 'to_excel'); send(); this.removeAttribute('name')" value="в Excel"></td>
			</tr>
			</table>
		</div>
	</form>

	<form id="send_form" method="POST">
		
	</form>

<script type="text/javascript">
		var start = new Date();
        var elements = document.querySelectorAll(".multiselect");

        for (var i = 0; i < elements.length; i++) {
            multiSelectPlugin.selectStart(elements[i]); 
        }

        function send() {
        	var form = document.querySelector('#send_form');

        	var con = document.querySelector('.stat_filter');
        	var controls = con.parentNode.elements;

        	var dataSubmit = {};

        	for (var i=0;i<controls.length;i++) {
        		if (controls[i].name && controls[i].value.length) {
        			dataSubmit[controls[i].name] = controls[i].value;
        		}
        	}

        	var mults = document.querySelectorAll(".default-control");

        	for (var i = 0; i < mults.length; i++) {
        		if(mults[i].hasOwnProperty('newTree')) {
        			mults[i].newTree.getValuesForPHP();

        			console.log(mults[i].newTree.selected);

        			for(var key in mults[i].newTree.selected) {
        				if(!dataSubmit[mults[i].newTree.dataName])
        					dataSubmit[mults[i].newTree.dataName] = [];
        				
        				dataSubmit[mults[i].newTree.dataName].push(mults[i].newTree.selected[key]);
        			}
        		} else {
        			if(mults[i].fieldValue()) {
	        			var fields = mults[i].fieldValue();

	        			for(var key in fields) {
	        				if(!dataSubmit[key])
	        					dataSubmit[key] = fields[key];
	        			}
	        		}
        		}
        	} 

        	if(dataSubmit.hasOwnProperty('to_excel')) {
        		var input = '';

        		for(var key in dataSubmit) {
        			if(Array.isArray(dataSubmit[key])) {
        				for(var i = 0; i < dataSubmit[key].length; i++) {
        					input += '<input type="hidden" name="'+key+'" value="'+ dataSubmit[key][i] +'" />';
        				}
        			} else {
        				input += '<input type="hidden" name="'+key+'" value="'+ dataSubmit[key] +'" />';
        			}
        		}

        		form.innerHTML = input;

        		//console.log(form);

        		form.submit();
        	} else {
        		//console.log(dataSubmit);

        		ajax_request(dataSubmit);
        	}

        }

        function ajax_request(dataSubmit) {
        	$.ajax({
	            type:'POST',
	            url: document.URL,
	            data: dataSubmit,
	            beforeSend: function() {
	            	var loader = '<div class="load" style="position: relative; margin-top: 30px; height: 100px; width: 300px"><div class="loader loader-quart"></div><span style="font-size: 18px;"> ЗАГРУЗКА...</span></div>';
	            	document.querySelector('.stat').innerHTML = loader;
	            },
	            success:function(data){
	                document.querySelector('.stat').innerHTML = data;
	            },
	            error: function(data){
	                console.log(data);
	            }
	        });
        }

        var end = new Date();

		console.log(end - start + 'ms');
</script>
