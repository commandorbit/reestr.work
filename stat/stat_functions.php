<?php

function sql_age($field_name,$d) {
	$d = empty($d) ? 'CURRENT_DATE' : sql_rus2date($d);
	return "((YEAR($d) - YEAR(`$field_name`)) - (DAYOFYEAR($d) < DAYOFYEAR(`$field_name`)))";
}


function age_array() {
	$age = __age();
	$s = "SELECT DISTINCT $age, $age FROM abitur a INNER JOIN decanat_stud ds ON a.abitur_kod=ds.stud_kod WHERE data_rogdenia is not null ORDER BY age";
	return sql_to_assoc($s);
}

function stat_attrs() {
	$a = array();
	$s = "SELECT field_name, alias_name, label, has_history, has_func, func_name, func_option_array, default_val, ord 
	FROM s_field_attributes ORDER BY ord";
	$res = sql_query($s);
	while ($row = sql_array($res)) {
		$a[$row['field_name']] = $row;
	}
	return $a;
}

