<?php
$path='/home/dekanat/';
set_include_path  (get_include_path() . PATH_SEPARATOR . $path);

include 'init.php';
$time_start = microtime(true);
include 'spisok.php';
require 'stat/stat.php';
require 'stat/stat_stud.php';

$fields = read_field_list($_SESSION['user_kod']);//������ ����� ��������� ������
$attrs = stat_attrs();

$w = array();
foreach ($_REQUEST as $id=>$value) {
	if (!Stat::value_is_all($value) && in_array($id,array_keys($attrs))) {
		$w[$id]=$value;
	}
}
$d = request_val('d','');
$in_sql= make_stud_sql(array('stud_kod'),$w,$d);
$s = "SELECT stud_kod, concat(fam, ' ', im, ' ', otc) as fio, ds.nomer_zach, ds.kurs,
	ds.status_kod, ds.grp_spec_kod, ds.ob_forma_kod, ds.ob_osnova_kod, ds.srok_kod, ds.student_type_kod, ds.target_org_kod, ds.osn_zach_kod, 
	ds.is_sps, ds.inostr_lang_kod, ds.is_starosta, ds.contract_nom, ds.contract_date, ds.grp_kod, ds.fakultet_kod as fak, 
	status_name, grp_spec_name, ob_forma_name, ob_osnova_name, srok_name, student_type_name, target_name, osn_zach_name, 
	a.obraz_kod, a.data_rogdenia, a.pol_kod, pol_name, obraz_short_name, a.is_interdecanat, s_lang.lang_name as inostr_name, fg.grp_nomer, fg.uch_plan_kod
	FROM decanat_stud ds
	INNER JOIN abitur a ON ds.stud_kod=a.abitur_kod 
	LEFT JOIN decanat_fakultet_grp fg ON ds.grp_kod=fg.grp_kod 
	LEFT JOIN s_student_status ON ds.status_kod=s_student_status.status_kod
	LEFT JOIN ob_forma ON ds.ob_forma_kod=ob_forma.ob_forma_kod
	LEFT JOIN grp_spec ON ds.grp_spec_kod=grp_spec.grp_spec_kod
	LEFT JOIN s_obraz ON a.obraz_kod=s_obraz.obraz_kod
	LEFT JOIN s_ob_osnova on ds.ob_osnova_kod=s_ob_osnova.ob_osnova_kod
	LEFT JOIN s_lang on ds.inostr_lang_kod= s_lang.lang_kod 
	LEFT JOIN s_srok on ds.srok_kod= s_srok.srok_kod
	LEFT JOIN s_student_type ON ds.student_type_kod=s_student_type.student_type_kod 
	LEFT JOIN s_target ON ds.target_org_kod=s_target.target_kod 
	LEFT JOIN s_pol ON a.pol_kod=s_pol.pol_kod 
	LEFT JOIN s_osn_zach ON ds.osn_zach_kod=s_osn_zach.osn_zach_kod 
	WHERE stud_kod in ( $in_sql ) ";
$ord = request_val('ord','counter');
if (strstr($ord,'counter')) {
	$sort_is_used = true;
	$s .= " ORDER BY fak, grp_nomer, fio ";
} else {
	$sort_is_used = false;
	if ($ord == 'fio') $s .= " ORDER BY fio ";
	else $s .= " ORDER BY $ord, fio ";
}
if (isset($_GET['to_excel'])) {
	toExcel($s,$fields);
	exit();
}
$page_head = '������ �� ��������';
include 'template/header.php';
?>
<script type="text/javascript">
	$(document).ready(function(){
		$('td.ord').click(function() {
			var sort_by = $(this).attr("id");
			var sort_now = $('#ord_id').val();
			if (sort_by == sort_now) {
				sort_by = sort_by + ' desc ';
			}
			$('#ord_id').val(sort_by);
			window.location.href='stat_spisok.php?<?= filter_params_2string($w,array(),'&') ?>&ord='+sort_by;
		});
	});
</script>
<form method="get" action="" id="sort_form">
	<span><input type="hidden" id="ord_id" name="ord" value="<?=$ord ?>"></span>
</form>
<p style="text-align: center;"><a href="<? if (isset($_SESSION['last_stat'])) echo $_SESSION['last_stat']; else echo 'stat.php'?>">��������� � ����������</a></p>
<div class="stat">
	<? $opisanie=makeStat($attrs)->set_f($w)->set_d($d)->opisanie() ?>
	<h4><?=$opisanie; ?></h4>
</div>
<form method="get" action="">
	<p>
		<input type="button" value="������� �������" onclick="location.href='tune_spisok.php'">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		<input type="button" value="� Excel" onclick="location.href='stat_spisok.php?<?= filter_params_2string($w,array(),'&') ?>&ord=<?=$ord?>&to_excel'">
	</p>
</form>
<?php
print_spisok($fields,$s,$sort_is_used);
echo '<p style="text-align: center;font-size:8pt;">����� ����������: ' . round((microtime(true) - $time_start),2) . ' ���.</p>';
include 'template/footer.php';
?>