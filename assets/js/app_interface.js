function showInfoMessage(msg) {
    if(showTimeout !== null) {
        clearTimeout(showTimeout);
    }
    $('.infoMessage').remove();
    $(document.body).append('<div class="infoMessage">' + msg + '</div>');
    $('.infoMessage').slideDown(0);
    showTimeout = setTimeout(function(){
        $('.infoMessage').slideUp(0, function(){
            this.remove();
        });
    }, 3000);
}

function _dispalyBySelector(el, selector, display) {
	var items = el.querySelectorAll(selector);

	if(items) {
		for(var i = 0; i < items.length; i++) {
			items[i].style.display = display;
		}
	}
}

function delete_system_message() {
	var message = document.body.querySelector('.system-message');

	if(message) {
		message.style.display = 'none';
	}

	return false;
}

//MessageBox - ����� ��������� ��� �������� �� �����
function messageBox(message, type) {
	var body = document.body,
		messageContainer = document.createElement('div'),
		effectShow = 'animated fadeInUp',
		effectHide = 'animated fadeOutDown';

	messageContainer.innerHTML = '<p>' + message + '</p>';

	switch(type) {
		case 'SUCCESS':
			messageContainer.className = 'u_message success_message ' + effectShow;
			break;
		case 'ERROR':
			messageContainer.className = 'u_message error_message ' + effectShow;
			break;
		default:
			messageContainer.className = 'u_message default_message ' + effectShow;
	}

	body.appendChild(messageContainer);

	setTimeout(function() {
		body.removeChild(messageContainer);
	}, 7000);
}

//����� ������������� ��������
function confirmBox(message) {
	if(confirm(message)) {
		return true;
	}

	return false;
}