window.onload = function() {
	var left_sidebar = document.querySelector('.left_sidebar');
	var tab_pane = document.querySelector('.tab-pane');

	if(left_sidebar) {
		var header = document.querySelector('.top-header'),
			LS_MINWIDTH = 60;

		function hideTextLs() {
			if(left_sidebar.offsetWidth == LS_MINWIDTH) {
				tab_pane.style.left = LS_MINWIDTH + 10 + 'px';
				left_sidebar.style.textAlign = 'center';
				_dispalyBySelector(left_sidebar, 'span', 'none');
			} else {
				if(tab_pane) tab_pane.style.left = 14 + '%';
				left_sidebar.style.textAlign = 'left';
				_dispalyBySelector(left_sidebar, 'span', 'inline');
			}
		}

		window.addEventListener('scroll', function(e) {
			var pageY = window.pageYOffset || document.documentElement.scrollTop;

			if(pageY > header.offsetHeight) {
				left_sidebar.style.top = '0';
			} else {
				left_sidebar.style.top = header.offsetHeight - pageY + 'px';
			}
		}, false);

		window.addEventListener('resize', function(e) {
			hideTextLs();
		}, false);	

		hideTextLs();	
	}
	
	var related_links = document.querySelectorAll('[data-content]');
    for(var i = 0; i < related_links.length; i++) {
        related_links[i].addEventListener('click', toggleRelatedContent);
    }
	
	var user_zfo = document.querySelector('select[name="user_zfo[]"]');
	var role_id = document.querySelector('select[name="role_id"]');
	if(user_zfo && role_id) {
		zfoChange(role_id);
	}
}

function specContragentName() {
    var contragent_fld = document.querySelector('.dogovor_fields select[name="contragent_id"]');
    contragent_fld.addEventListener('change', getShowFio);
    var evt = document.createEvent("HTMLEvents");
    evt.initEvent("change", false, true);
    contragent_fld.dispatchEvent(evt);
}

function getShowFio() {
    var url = '/modules/ajax.php';

    var that = this;

    var value = that.value;

    $.get(
      url,
      {
        func: "spec_contragent_name",
        arg1: value
      },
      onAjaxSuccess
    );
     
    function onAjaxSuccess(data)
    {
        var d;

        try {
            d = JSON.parse(data);

            var contragent_fio_fld = document.querySelector('.dogovor_fields #contragent_fio');

            if(d == 1) {
                contragent_fio_fld.style.display = 'block';
            } else {
                contragent_fio_fld.style.display = 'none';
                contragent_fio_fld.querySelector('input').value = '';
            }
        }   catch (e) {
            alert ('Error in server respons!');
        // Save ot server error log?
            console.log('error json parse');
            console.log(data);
        }  
    }
}

function createZayav(rows) {
    var form = document.querySelector('#create-zayav');

    if(!form) return false;

    var submit = form.querySelector('input[type="submit"]');
    var hiddens = form.querySelector('#hiddens');

    if(form && rows.length > 0) {
        hiddens.innerHTML = '<input type="hidden" name="id" value="new" />';

        submit.removeAttribute('disabled');
        submit.setAttribute('title', '������� ����� ������ �� ��������� ���������');

        for(var key in rows) {
            var hidden = '<input type="hidden" name="ds[]" value="'+ rows[key].id +'" />';

            hiddens.innerHTML += hidden;
        }
    } else {
        submit.setAttribute('disabled', 'disabled');
        submit.setAttribute('title', '������ �� �������');
    }
}

function dogovorControlsOperation() {
	var period_place = document.querySelector('[name=period_place_id]');
    var period_finish = document.querySelector('[name=period_finish_id]');
    var period = document.querySelector('.modal-editbox [name=period_id]');

    var save_status = document.querySelector('[name=save_status]');

    var dogovor_fields_selects = document.querySelectorAll('.dogovor_fields select');
    var dogovor_fields_textareas = document.querySelectorAll('.dogovor_fields textarea');

    function addAttr(node) {
		if(!node) return;
        node.setAttribute('disabled', 'disabled');
        node.setAttribute('title', '�������� ���� ��������, ���������� ��������� �������, ���� �������� ��������');
    }

    for(var i = 0; i < dogovor_fields_selects.length; i++) {
        dogovor_fields_selects[i].onchange = function() {
            addAttr(save_status);
        }
    }

    for(var i = 0; i < dogovor_fields_textareas.length; i++) {
        dogovor_fields_textareas[i].onkeyup = function() {
            addAttr(save_status);
        }
    }
    
    //������ ���������
    var submit = document.querySelector('[name=save_parent]');

    //������������ ����
    var textareas = document.querySelectorAll('.form-controls textarea');

    //������ � ���������� ������
    if(period != null) 
       removeOptions(period_place, period);

    removeOptions(period_place, period_finish);

    function removeOptions(el, from_remove) {
        var options = from_remove.querySelectorAll('option');

        if(parseInt(from_remove.value) < parseInt(el.value)) {
            from_remove.value = el.value;
        }

        for(var i = 0; i < options.length; i++) {
            options[i].style.display = '';

            if(parseInt(options[i].value) < parseInt(el.value)) {
                options[i].setAttribute('disabled', 'disabled');
            } else {
            	options[i].removeAttribute('disabled', 'disabled');
            }
        }
    }

    period_place.onchange = function(options) {
        removeOptions(this, period_finish);

        if(period != null) 
            removeOptions(this, period);
    }

    //����� ������ � ���������� ������
    
    submit.onclick = function() {
        var error_fields = [];

        for(var i = 0; i < textareas.length; i++) {
        	if(textareas[i].name == 'dogovor_num') continue;
        	
            if(textareas[i].value.trim() == '') {
                error_fields.push(textareas[i]);
            }
        }

        if(error_fields.length > 0) {
            var name='<p><b>�� ��������� ����: </b></p>';

            var error_message = document.createElement('div');


            for(var i = 0; i < error_fields.length; i++) {
                name += '<p>' + error_fields[i].parentNode.querySelector('span').innerHTML + '</p>';
            }

            error_message.innerHTML = name;
            error_message.className = 'u_message error_message';

            document.body.appendChild(error_message);


            setTimeout(function(){
                    document.body.removeChild(error_message);
                }, 3000);

            return false;
        }

        return true;
    }
}

function toggleRelatedContent(e) {
    var target = e.target || e.srcElement,
        content_id = target.getAttribute('data-content'),
        content = document.querySelector('#'+content_id),
        content_display = (content && content.style.display === 'none') ? '' : 'none';
    content.style.display = content_display;
}



function savePfhdVer(el) {
    var href = el.href;
    var ver_name = prompt("������� �������� ����� ������:");
    if(ver_name && ver_name.trim() != '') {
        href += '?ver_name=' + encodeURIComponent(ver_name);
        window.location.assign(href); 
    } else {
        alert("�������� ������ �� ����� ���� ������!");
    }
}

function openCanvas(id) {
    var canvas = document.querySelector('#dog-'+id);
    if(canvas) {
        canvas.classList.toggle('active');
    }
	return false;
}

function zfoChange(el) {
	var zfo_select = document.querySelector('select[name="user_zfo[]"]');
	var parent = zfo_select.parentNode;
	if(!parent) return;
	if(el.value == 6) {
		parent.style.display = 'block';
		zfo_select.removeAttribute('disabled');
	} else {
		parent.style.display = 'none';
		zfo_select.value = 'null';
		zfo_select.setAttribute('disabled', 'disabled');
	}
}