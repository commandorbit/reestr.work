var ModalWindowBody = {
    modalDiv: null,
    handler:null,
    //control: null,
    bodyHandler: function (e) {
        var modalDiv = ModalWindowBody.modalDiv;
        if (!modalDiv) return;
        e = e || window.event;

        var target = e.target || e.srcElement;

        var found = false;

        while(target) {
            if(target === modalDiv) {
                found = true;
            }
            target = target.parentNode;
        }

        if(!found) {
            if (!ModalWindowBody.handler || ModalWindowBody.handler(e,modalDiv)) {
                //var evt = document.createEvent("HTMLEvents");
                //evt.initEvent("change", false, true);
                //ModalWindowBody.control.dispatchEvent(evt);

                modalDiv.style.visibility = 'hidden';
                ModalWindowBody.modalDiv = null;
                ModalWindowBody.handler = null;      
            }    
        }
    }
}

var body = document.getElementsByTagName('body')[0];

body.addEventListener("click", ModalWindowBody.bodyHandler, true);