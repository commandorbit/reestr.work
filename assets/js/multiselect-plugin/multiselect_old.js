"use strict";

if(!NodeList.prototype.forEach) {
    NodeList.prototype.forEach = Array.prototype.forEach;
}

var multiSelectPlugin = {
    selectStart: function(select) {
        if (select.tagName != 'SELECT') {
            // error!
        }

        var control = document.createElement('span');
        control.className = 'multiselect-control';
        var elementName = select.name || select.dataset.name;
        var elementType = select.getAttribute('data-type') || '';
        control.setAttribute("data-name", elementName);
        control.setAttribute("data-type", elementType);
        multiSelectPlugin._multiSelectCreator(control, select.options);
        select.parentNode.replaceChild(control, select);
    },

    //Start plugin
    start: function(control) {
        multiSelectPlugin._multiSelectCreator(control);
    },

    html: '<div class="selection"></ul><ul class="filter-dropdown-ul"><li class="filter-item-menu asc"><label><i class="fa fa-sort-alpha-asc"></i><span>Сортировка от А до Я</span></label></li><li class="filter-item-menu desc"><label><i class="fa fa-sort-alpha-desc"></i><span>Сортировка от Я до А</span></label></li><li></li><li class="filter-item-menu clean-filter"><label><i class="fa fa-filter"><i class="fa fa-times extra"></i></i><span>Снять фильтр</span></label></li><li><div class="filter-item-pointer"></div></li><li class="filter-item-menu parent-li"><label class="parent"><i class="fa fa-file-text"></i><span>Текстовые фильтры</span></label><ul class="filter-item-menu-submenu"></ul></li></li></ul><div class="speed-search"><input type="text" placeholder="Поиск.." /></div><div class="container-box"><ul class="container"></ul></div><button class="btn filter-dropdown-apply">ОК</button><button class="btn filter-dropdown-cancel">Отмена</button><div class="filter-item-footer"><div class="filter-resize-dotts"></div></div></div>',

    submenu_text: '<li data="~" data-contain="false"><i></i>Содержит..</li><li data="!~" data-contain="true"><i></i>Не содержит..</li>',

    submenu_int: '<li data="="><i></i>Равно..</li><li data="!="><i></i>Не равно..</li><li data=">"><i></i>Больше..</li><li data="<"><i></i>Меньше..</li><li data="range"><i></i>Диапазон..</li>',

    submenu_date: '<li data="date" value="="><i></i>Равно..</li><li data="date" value="!="><i></i>Не равно..</li><li data="date" value="<"><i></i>До..</li><li data="date" value=">"><i></i>После..</li><li data="date" value="date_range"><i></i>Между..</li>',

    childHtml: '<li><label class="%selected%"><input name="%name%" type="checkbox" value="%value%" %checked%><span>%text%</span></label></li>',

    props: ['submenu','submenu_text','submenu_int','submenu_date','multiselectDiv',
        'setOptions','fieldValue','selected','onClone','onclick','openFilter', 'setTree', 'getSelected', 'setSelected', 'clearSelected', 'closeFilter'],
    //Redefine <select> 
    _multiSelectCreator: function(control, options) {        
        

        //Create items list <div>
        var options = options;
        var dateOptions;

        var multiselectDiv = document.createElement('div');
        multiselectDiv.className = 'multiselect';
        multiselectDiv.style.position = "absolute";
        multiselectDiv.style.visibility = "hidden";
      
        control.multiselectDiv = multiselectDiv;

        multiselectDiv.innerHTML = multiSelectPlugin.html;

        var speedSearch = multiselectDiv.querySelector('input[type="text"]');

        var submenu = multiselectDiv.querySelector('.filter-item-menu-submenu');

        control.submenu = submenu;
        control.submenu_text = multiSelectPlugin.submenu_text;
        control.submenu_int = multiSelectPlugin.submenu_int;
        control.submenu_date = multiSelectPlugin.submenu_date;

        var ul = multiselectDiv.querySelector(".container");


        var btn = multiselectDiv.querySelectorAll(".btn");

        control.setOptions = function(opt) {
            options = opt;
            initOptions(options);
        }


        //Start: Манипуляции с контролом
        var _selected = [];

        control.getSelected = function() {
            // return copy of array
            return _selected.slice(0);
        }

        control.clearSelected = function() {
            _selected = [];
        }

        control.setSelected = function(defaultFilter) {
            var checks = this.multiselectDiv.querySelectorAll('input[type="checkbox"]:checked');

            _selected = [];

            for (var j = 0; j < checks.length; j++) {
                //on cross browser empty value fix
                if (checks[j].value != 'on') {
                    _selected.push(checks[j].value);
                }
            }

            if(defaultFilter) {
                if(Array.isArray(defaultFilter))
                    _selected = defaultFilter;
                else
                    _selected.push(defaultFilter);
            }
        };

        control.fieldValue = function() {
            var str = "";
            if (this.multiselectDiv) {
                var checks = this.multiselectDiv.querySelectorAll('input');
                for (i = 0; i <checks.length; i++) {
                    if (checks[i].checked && checks[i].value != 'on') {
                        str += '<input type="hidden" name="'+ checks[i].name +'" value="' + checks[i].value + '" />';
                    }
                }
            }
            return str;
        };


        control.onClone = function (to) {
            var that = this;
            multiSelectPlugin.props.forEach(function(prop){
                to[prop]=that[prop];                
            });
        }        

        control.prevStateAction = function() {
            var selected = control.getSelected();

            [].forEach.call(multiselectDiv.checks, function(option) {
                option.checked = false;
                checkColorChange(option);

                for(var i = 0; i < selected.length; i++) {
                    if(option.value == selected[i]) {
                        option.checked = true;
                        checkColorChange(option);
                    }
                }
            });
        }

        control.setTree = function(data) {
            ul.innerHTML = '';
            new treePlugin(ul, data, this.getAttribute('data-name'), {opened:false, checkboxes:true});
        }

                control.onclick = function() {
            this.openFilter();
        }



        control.openFilter = function() {
            var div;
            div = this.multiselectDiv;
            if (div && div.style.visibility == 'hidden') {
                controlOpened = this;

                var of = offset(this);
                div.style.left = of.left + "px";
                div.style.top = of.top+this.offsetHeight + "px";
                div.style.visibility = 'visible';
                div.className = 'multiselect';

                var apply = multiselectDiv.querySelector('.filter-dropdown-apply');
                var cancel = multiselectDiv.querySelector('.filter-dropdown-cancel');

                apply.onclick = function() {
                    setFilterIcon();

                    control.setSelected();

                    control.closeFilter(div);
                }

                cancel.onclick = function() {
                    control.closeFilter(div);

                    control.prevStateAction();
                }

                ModalWindowBody.modalDiv = div;
                ModalWindowBody.handler = bodyClick;
            }

            new resizable(div.querySelector('.selection'), {directions: ['southeast']});
        }

        control.closeFilter = function(div) {
            div.style.visibility = 'hidden';
            div = null;
            ModalWindowBody.handler = null;
        }

        control.treePluginRunup = function(dateSet, selectedArray) {
            var dates = {},
                months = {'01':'Январь', '02':'Февраль', '03':'Март', '04':'Апрель', '05':'Май', '06':'Июнь', '07':'Июль', '08':'Август', '09':'Сентябрь', '10':'Октябрь', '11':'Ноябрь', '12':'Декабрь'},
                datesArray = [],
                selectedDates = [];

            for(var key in dateSet) {
                if(dateSet[key].nodeType === 1 && dateSet[key].hasAttribute('selected')) {
                    selectedDates.push(dateSet[key].value);
                }
            }

            if(selectedArray == undefined) {
                control.setSelected(selectedDates);
            } else {
                control.setSelected(selectedArray);
            }
            
            var selected = control.getSelected();

            for(var i = 0; i < dateSet.length; i++) {
                if(dateSet[i].nodeType === 1) {
                    datesArray.push(dateSet[i].value);
                } else {
                    datesArray.push(dateSet[i]);
                }
            }

            datesArray.sort();

            for(var i = 0; i < datesArray.length; i++) {
                var dt = datesArray[i].split('-');

                if(datesArray[i] == '_null_') { 
                    if(!dates[' (Пустые)']) dates[' (Пустые)'] = {};
                    if(!dates[' (Пустые)']['value']) dates[' (Пустые)']['value'] = '_null_';
                }

                    if(!dates[' (Выделить все)']) dates[' (Выделить все)'] = {};
                    if(!dates[' (Выделить все)']['value']) dates[' (Выделить все)']['value'] = '_all_';
                

                for(var key in months) {
                    if(key == dt[1]) dt[1] = months[key];
                }

                dt.forEach(function(item,i) {dt[i]=' '+item;});

                if(!dates[dt[0]]) dates[dt[0]] = {};
                if(!dates[dt[0]][dt[1]]) dates[dt[0]][dt[1]] = {};
                if(!dates[dt[0]][dt[1]][dt[2]]) dates[dt[0]][dt[1]][dt[2]] = {};
                if(!dates[dt[0]][dt[1]][dt[2]]['value']) dates[dt[0]][dt[1]][dt[2]]['value'] = datesArray[i];
                if (selected.indexOf(datesArray[i])>=0) dates[dt[0]][dt[1]][dt[2]]['selected'] = true;
            }

            delete dates[' _all_'];
            delete dates[' _null_'];

            control.setTree(dates);
        }

                //Хэлперы
        function setFilterIcon() {
            if(ul.querySelectorAll('input[type="checkbox"]:checked').length > 0) {    
                var checks = ul.querySelectorAll('input[type="checkbox"]:checked');

                for(var i = 0; i < checks.length; i++) {
                    if(checks.length == 1) {
                        if(control.getAttribute('data-type') == 'date') {
                            control.innerHTML = checks[i].parentNode.querySelector('input').value;
                        } else {
                            control.innerHTML = checks[i].parentNode.querySelector('span').innerHTML;
                        }
                    } else {
                        control.innerHTML = 'Значений выбрано: ' + checks.length; 
                    }
                }

                if(control.innerHTML.length > 20) {
                    control.innerHTML = control.innerHTML.substring(20, 20 - control.innerHTML.length - 1) + '..';
                }

                addClass(control, 'filtered-control');
            } else {
                removeClass(control, 'filtered-control');
                control.innerHTML = '';
            }
        }


        if(options) {
            var selectAll = document.createElement('option');
            selectAll.innerHTML = '(Выделить все)';
            selectAll.value = '_all_';

            var opt = [];

            for(var i = 0; i < options.length; i++) opt.push(options[i]);

            opt.unshift(selectAll);

            if(control.getAttribute('data-type') == 'date') {
                control.treePluginRunup(opt);
            } else {
                initOptions(opt);
            }

            $('.filter-dropdown-ul').remove();

            multiselectDiv.querySelector('.selection').style.minHeight = 315 + 'px';

            setFilterIcon();
        }

        //Вспомогательные функции надо перенести в отдельный файл
        function hasClass(node, className) {
            var cl = node.className.split(/\s/);

            var result = false;

            for (var i = 0; i < cl.length; i++) {
                if (cl[i] == className) {
                    result = true;
                    break;
                }
            }

            return result;
        }

        function addClass(node, className) {
            if (!hasClass(node,className)) {
                if (node.className) node.className += ' ';
                node.className += className;
            }
        }

        function removeClass(node, className) {
            var cl = node.className.split(/\s/);

            var result = false;

            var str='';

            for (var i = 0; i < cl.length; i++) {
                if (cl[i] == className) {
                    cl.splice(i, 1);
                }
            }

            node.className= cl.join(' ');
        }
        //Конец вспомогательных функций

        var selectedOptions = [];

        //Быстрый поиск по опшинсам
        speedSearch.onkeyup = function(e) {
            var result = [],
                optionText,
                searchText;

            for(var i = 0; i < options.length; i++) {
                optionText = options[i].text.toLowerCase();
                searchText = this.value.toLowerCase();

                if(optionText.indexOf(searchText) !== -1) {
                    result.push(options[i]);
                } else if(this.value == '') {
                    result = options;
                }
            }

            if(selectedOptions.length > 0) {
                for(var key in result) {
                    for(var i = 0; i < selectedOptions.length; i++) {
                        if(result[key].value == selectedOptions[i]) {
                            result[key].selected = true;
                        }
                    }
                }
            }

            initOptions(result);
        }

        function initOptions(options) {
            var html = '';
            var elementName= control.getAttribute("data-name");
            
            for(var i = 0; i < options.length; i++) {
                var val = options[i].value;
                var checked = options[i].selected;
                var text = options[i].text;

                var h = multiSelectPlugin.childHtml.replace('%value%',val).replace('%name%',elementName).replace('%text%',text);
                if (checked) {
                    h=h.replace('%checked%','checked="true"').replace('%selected%','selected');
                } else {
                    h = h.replace('%checked%','').replace('%selected%','');
                }

                html += h;
            }

            ul.innerHTML = html;

            //multiselectDiv.checks = ul.querySelectorAll('input');
        }

        var controlOpened = null;

        function bodyClick(e) {
            if (e.target == controlOpened) {
                e.stopPropagation();
            } else {
                control.prevStateAction();
            }

            return true;
        };

        //Выделяем цветом выбранный чекбокс && активируем "(Выделиьть все)"
        multiselectDiv.addEventListener('change', function(e) {

            e = e || window.event;

            var target = e.target || e.srcElement,
                allChecked = true;
            var checks = ul.querySelectorAll('input');

            if(target.value == '_all_') {
                for(var i = 0; i < checks.length; i++) {
                    checks[i].checked = target.checked;

                    checkColorChange(checks[i]);
                }
            }

            for(var i = 1; i < checks.length; i++) {
                if(!checks[i].checked) {
                    allChecked = false;
                }
            }

            ul.querySelector('input').checked = allChecked;

            checkColorChange(ul.querySelector('input'));

            checkColorChange(target);

            control.allChecked = allChecked;

            //console.log('clicked');
        });

        function checkColorChange(target) {
            if (target.checked)
                addClass(target.parentNode, 'selected');
            else
                removeClass(target.parentNode, 'selected');
        }

        var checks = multiselectDiv.querySelectorAll('.container input');

        body.appendChild(multiselectDiv);

        //EndOf: манипуляции с контролом
        
        return true;
    }
};

//determine the position of items list <div>
function offset(elem) {
    var pos;

    function searchUp(element) {
        if(element.tagName == 'TABLE' || element.tagName == 'BODY') {
            pos = element.style.position;
            return element;
        } else  {
            searchUp(element.parentNode);
        }

        return false;
    }

    searchUp(elem);

    var offset={top:2,left:0};

    //var submenu = elem.multiselectDiv.querySelector('.filter-item-menu-submenu');

    while (elem) {
        offset.top += elem.offsetTop;
        offset.left += elem.offsetLeft;
        elem = elem.offsetParent;
    }

    var divEnd = offset.left + 234;

    if(divEnd > body.scrollWidth) {
        offset.left = offset.left - 220;
    }

    if(body.scrollTop > 100 && body.scrollLeft > 0) {
        offset.left = offset.left + body.scrollLeft;
    }

    if(pos == 'fixed') {
        //console.log(body.scrollTop);
        offset.top = offset.top + body.scrollTop;
    }

    return offset;
}
