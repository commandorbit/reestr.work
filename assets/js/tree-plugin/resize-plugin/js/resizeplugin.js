function resizable(element, options) {

	//Определяем дефолтные направления
	this._defaults = {
		directions: ['north', 'south', 'west', 'east', 'southeast', 'southwest', 'northeast', 'northwest']
	};
	this.element = element;

	//Функция для определения направлений определенных пользователем
	var extend = function (a, b) {
		for(var key in b) {
			if (b.hasOwnProperty(key)) {
				a[key] = b[key];
			}
		}
		return a;
	}

	this._options = extend(this._defaults, options);

	this._directions = this._options.directions;

	this._create();
}

//Вешает обработчики
resizable.prototype._createHandle = function (direction) {
	var container = this.element.querySelector('.container');

	if(container != null) {
		containerHeight = container.offsetHeight;
	}

	var ghost,

	boxSize = {},

	conSize,

	resizable = this,

		//Создает прозрачный див с направлениями
		rh = document.createElement('div'),

		//Ресайзер
		resize = function (e) {
			e.preventDefault();

			if (direction.indexOf('south') !== -1) {
				ghost.style.height = (resizable._startH + (e.pageY - resizable._startY)) + "px";

				if(container !== null) {
					conSize = container.offsetHeight + (e.pageY - resizable._startY) + 'px';
				}
			}

			if (direction.indexOf('east') !== -1) {
				ghost.style.width = (resizable._startW + (e.pageX - resizable._startX)) + "px";
			}
		},

		//Удаляем эвент если кнопка мыши отпущена
		stop = function () {
			boxSize = {'width':ghost.style.width, 'height':ghost.style.height};

			resizable.element.style.width = boxSize.width;
			resizable.element.style.height = boxSize.height;

			if(container !== null) {
				container.style.height = conSize;
			}

			[].forEach.call(resizable.element.parentNode.querySelectorAll('.ghost'), function (ghost) {
				ghost.remove();
			});

			window.removeEventListener("mousemove", resize, true);
		},

		//Определяем позицию элемента, вешаем обработчики
		start = function (e) {
			e.stopPropagation();
			resizable._startX = e.pageX;
			resizable._startY = e.pageY;
			resizable._startW = resizable.element.offsetWidth;
			resizable._startH = resizable.element.offsetHeight;	
			window.addEventListener("mousemove", resize, true);
			window.addEventListener("mouseup", stop, true);

			ghost = document.createElement('div');
			ghost.setAttribute('class', 'ghost');
			ghost.style.width = resizable.element.offsetWidth + 'px';
			ghost.style.height = resizable.element.offsetHeight + 'px';
			ghost.style.left = resizable.element.offsetLeft + 'px';
			ghost.style.top = resizable.element.offsetTop + 'px';

			resizable.element.parentNode.appendChild(ghost);
		};

	rh.className = rh.className + ' rsize-d rsize-d-' + direction;
	rh.setAttribute('data-rsize-d', direction);
	resizable.element.appendChild(rh);
	rh.addEventListener("mousedown", start, false); //Запускаем эвент, если кнопка мыши нажата
}

//Инициализируем направления для заданных элементов
resizable.prototype._create = function () {
	var resizable = this;
	resizable.element.className = resizable.element.className + ' rsizable';
	resizable._directions.forEach(function (direction) {
		resizable._createHandle(direction);
	});
}