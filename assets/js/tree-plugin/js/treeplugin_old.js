/*
	Плагин: JavaScprit-дерево
	Описание: JavaScprit-дерево создается автоматически из
	          переданного объекта, с поддержкой множественного
	          выбора.
	Пример использования: new treePlugin(el, data, options);
						  -el - элемент куда будет встроено дерево
						  -data - объект из которого будет строится дерево
						  -options - {opened: true, checkboxes: true}
						  -options.opened - открыты/закрыты "ветки" дерева
						  при инициализации
						  -options.checkboxes - дерево с чекбоксами/без них
						  По дефолту дерево всегда развернуто и с чекбоксами
	Автор: Борисов Денис 
*/

if(!NodeList.prototype.forEach) {
    NodeList.prototype.forEach = Array.prototype.forEach;
}

function hasClass(node, className) {
    var cl = node.className.split(/\s/);

    var result = false;

    for (var i = 0; i < cl.length; i++) {
        if (cl[i] == className) {
            result = true;
            break;
        }
    }

    return result;
}

function addClass(node, className) {
    if (!hasClass(node,className)) {
        if (node.className) node.className += ' ';
        node.className += className;
    }
}

function removeClass(node, className) {
    var cl = node.className.split(/\s/);

    var result = false;

    var str='';

    for (var i = 0; i < cl.length; i++) {
        if (cl[i] == className) {
            cl.splice(i, 1);
        }
    }

    node.className= cl.join(' ');
}

function isObjectEmpty(obj) {
    for (var key in obj) {
    	return false;
    }

    return true;
}

function treePlugin(element, data, options) {
	this.element = element;

	this._defaults = {
		opened: true,
		checkboxes: true
	};

	//Функция для определения пользовательских опций
	var extend = function (a, b) {
		for(var key in b) {
			if (b.hasOwnProperty(key)) {
				a[key] = b[key];
			}
		}
		return a;
	}

	this.data = data;

	this._options = extend(this._defaults, options);

	this._init();
}

treePlugin.prototype._drawTree = function(data) {
	if (isObjectEmpty(data)) return;

	var treePlugin = this,
		checkBox,
		expand = '<div class="Expand"></div>',
		ul = document.createElement('ul');

	treePlugin._options.opened ? expandState = 'Open' : expandState = 'Closed';
	treePlugin._options.checkboxes ? checkBox = '<input type="checkbox" />' : checkBox = '';

	ul.setAttribute('class', 'Container');

	for (var key in data) {
		var li = document.createElement('li');

		if(!isObjectEmpty(data[key].children)) {
			li.setAttribute('class', 'Node Expand' + expandState);
			li.innerHTML = expand + '<div class="Content Fat">' + checkBox + data[key].name + '</div>';
		} else {
			li.setAttribute('class', 'Node ExpandLeaf IsLast');
			li.innerHTML = expand + '<div class="Content">' + checkBox + data[key].name + '</div>';
		}

		var childrenUL = treePlugin._drawTree(data[key].children);

		if (childrenUL) {
			li.appendChild(childrenUL);
		}

		ul.appendChild(li);
    }

    return ul;
}

treePlugin.prototype._createHandle = function() {
	var treePlugin = this,
		checkBoxes = treePlugin.element.querySelectorAll('input[type="checkbox"]'),
		toggle = treePlugin.element.querySelectorAll('.Expand');

	checkChange = function(e) {
		e = e || window.event;

		var checked = this.checked,
			container = this.parentNode.parentNode,
			containerChecks = container.querySelectorAll('input[type="checkbox"]');

		for(var i = 0; i < containerChecks.length; i++) {
			containerChecks[i].checked = checked;
			containerChecks[i].indeterminate = false;
		}

		function getSiblings(elem) {
			var sibList = Array.prototype.filter.call(elem.parentNode.children, function(child){
				return child !== elem;
			});

			return sibList;
		}

		function parentsMy(elem, tagName, stopTagName) {
			if(elem == null) return;

			stopTagName = stopTagName || 'body';

			var set = [];

			while(elem.tagName.toLowerCase() != stopTagName.toLowerCase()) {
				if(elem.tagName.toLowerCase() == tagName.toLowerCase()) {
					set.push(elem);
				}

				elem = elem.parentNode;
			}	

			return set;
		}

		function checkSiblings(el) {
			var parent = el.parentNode.parentNode,
            	all = true,
            	parentCheck = parent.querySelector('input[type="checkbox"]'),
            	checkedLength = (parent.parentNode.querySelectorAll('input[type="checkbox"]:checked').length > 0);

            if(parent.nodeName != 'LI') return;

            [].forEach.call(getSiblings(el), function(sib) {
            	var sibChecks = sib.querySelectorAll('input[type="checkbox"]');

            	for(var i = 0; i < sibChecks.length; i++) {
            		if(!sibChecks[i].checked) {
            			all = false;
            		}
            	}
            });

        	if(all && checked) {
        		parentCheck.indeterminate = false;
        		parentCheck.checked = checked;
        		checkSiblings(parent);
        	} else if(all && !checked) {
        		parentCheck.checked = checked;
            	parentCheck.indeterminate = (parent.querySelectorAll('input[type="checkbox"]:checked').length > 0);
            	checkSiblings(parent);
        	} else {
        		[].forEach.call(parentsMy(parent, 'li', 'div'), function(rootNode) {
        			rootNode.querySelector('input[type="checkbox"]').indeterminate = (rootNode.querySelectorAll('input[type="checkbox"]:checked').length > 0);
        			rootNode.querySelector('input[type="checkbox"]').checked = false;
        		});
        	}
		}

		checkSiblings(container);
	},

	toggleClick = function(e) {
		e = e || window.event;

		var target = e.target || e.srcElement,
			targetParent = target.parentNode;

		if(hasClass(targetParent, 'ExpandOpen')) {
			removeClass(targetParent, 'ExpandOpen');
			addClass(targetParent, 'ExpandClosed');
		} else {
			removeClass(targetParent, 'ExpandClosed');
			addClass(targetParent, 'ExpandOpen');
		}
	};

	if(treePlugin._options.checkboxes) {
		for(var i = 0, l = checkBoxes.length; i < l; i++) {
			checkBoxes[i].addEventListener('change', checkChange);
		}
	}

	for(var i = 0, l = toggle.length; i < l; i++) {
		toggle[i].addEventListener('click', toggleClick);
	}
}

treePlugin.prototype._init = function() {
	var treePlugin = this;

	treePlugin.element.appendChild(treePlugin._drawTree(treePlugin.data));

	treePlugin._createHandle();

	console.log('treePlugin.js init');
}

