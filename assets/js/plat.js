'use strict';
// included by tedit.php
// sets zayac conttol options based on contragent and orgainzsation selected
var plat={
	// устанавливает option list заявкам
	// zayav_id - optional если установлен то ставит value
	zayav_list:function(contragent_id,zayav_id) {
			Ajax({
				type: "GET",
				url: "/modules/ajax.php?func=get_zayav_options&arg1="+contragent_id,
                done: function(data) {
                    var z=document.querySelector('select[name="zayav_id"]')
                    z.innerHTML=data;
                    if(zayav_id) z.value=zayav_id;
                }
			});
		},
	change:function() {
			var contragent_id=document.querySelector('select[name="contragent_id"]').value
			plat.zayav_list(contragent_id);
	},
};
if (TDplat && TDplat.editBox) TDplat.editBox.onload = function() { 
    var data=this.row,box=this.box;
    box.querySelector('select[name="contragent_id"]').onchange=plat.change;
	if (data) plat.zayav_list(data.contragent_id,data.zayav_id);
};


