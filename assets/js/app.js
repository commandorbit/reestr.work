var showTimeout = null;
/*function getMainMenu() {
	var sidebar = document.querySelector('[data-type="main-sidebar"]');
	if(sidebar) {
		sidebar.classList.add("animated", "fadeInLeft");
		sidebar.style.display = 'block';

		sidebar.addEventListener('mouseout', function(e) {
			var _this = this;
			_this.classList.remove('fadeInLeft');

			e = event.toElement || event.relatedTarget;

		    // check for all children levels (checking from bottom up)
		    while (e && e.parentNode && e.parentNode != window) {
		        if (e.parentNode == this||  e == this) {
		            if(e.preventDefault) {
		                e.preventDefault();
		            }
		            return false;
		        } 
		        e = e.parentNode;
		    }

		    _this.classList.add('fadeOutLeft');
			setTimeout(rmClass, 400);

			function rmClass() {
				_this.classList.remove('fadeOutLeft');
				_this.style.display = 'none';
			};
		});
	}
}*/

//Форма подтверждения действия
function confirmBox(message) {
	if(confirm(message)) {
		return true;
	}

	return false;
}

//MessageBox - вывод сообщений для действий на сайте
function messageBox(message, type) {
	var body = document.body,
		messageContainer = document.createElement('div'),
		effectShow = 'animated fadeInUp',
		effectHide = 'animated fadeOutDown';

	messageContainer.innerHTML = '<p>' + message + '</p>';

	switch(type) {
		case 'SUCCESS':
			messageContainer.className = 'u_message success_message ' + effectShow;
			break;
		case 'ERROR':
			messageContainer.className = 'u_message error_message ' + effectShow;
			break;
		default:
			messageContainer.className = 'u_message default_message ' + effectShow;
	}

	body.appendChild(messageContainer);

	setTimeout(function() {
		body.removeChild(messageContainer);
	}, 7000);
}

function delete_system_message() {
	var message = document.body.querySelector('.system-message');

	if(message) {
		message.style.display = 'none';
	}

	return false;
}

function dogovorControlsOperation() {
	var period_place = document.querySelector('[name=period_place_id]');
    var period_finish = document.querySelector('[name=period_finish_id]');
    var period = document.querySelector('.modal-editbox [name=period_id]');

    var save_status = document.querySelector('[name=save_status]');

    var dogovor_fields_selects = document.querySelectorAll('.dogovor_fields select');
    var dogovor_fields_textareas = document.querySelectorAll('.dogovor_fields textarea');

    function addAttr(node) {
		if(!node) return;
        node.setAttribute('disabled', 'disabled');
        node.setAttribute('title', 'Изменены поля договора, необходимо сохранить договор, либо обновить страницу');
    }

    for(var i = 0; i < dogovor_fields_selects.length; i++) {
        dogovor_fields_selects[i].onchange = function() {
            addAttr(save_status);
        }
    }

    for(var i = 0; i < dogovor_fields_textareas.length; i++) {
        dogovor_fields_textareas[i].onkeyup = function() {
            addAttr(save_status);
        }
    }
    
    //Кнопка сохранить
    var submit = document.querySelector('[name=save_parent]');

    //Обязательные поля
    var textareas = document.querySelectorAll('.form-controls textarea');

    //Работа с выбранными датами
    if(period != null) 
       removeOptions(period_place, period);

    removeOptions(period_place, period_finish);

    function removeOptions(el, from_remove) {
        var options = from_remove.querySelectorAll('option');

        if(parseInt(from_remove.value) < parseInt(el.value)) {
            from_remove.value = el.value;
        }

        for(var i = 0; i < options.length; i++) {
            options[i].style.display = '';

            if(parseInt(options[i].value) < parseInt(el.value)) {
                options[i].setAttribute('disabled', 'disabled');
            } else {
            	options[i].removeAttribute('disabled', 'disabled');
            }
        }
    }

    period_place.onchange = function(options) {
        removeOptions(this, period_finish);

        if(period != null) 
            removeOptions(this, period);
    }

    //конец работы с выбранными датами
    
    submit.onclick = function() {
        var error_fields = [];

        for(var i = 0; i < textareas.length; i++) {
        	if(textareas[i].name == 'dogovor_num') continue;
        	
            if(textareas[i].value.trim() == '') {
                error_fields.push(textareas[i]);
            }
        }

        if(error_fields.length > 0) {
            var name='<p><b>Не заполнены поля: </b></p>';

            var error_message = document.createElement('div');


            for(var i = 0; i < error_fields.length; i++) {
                name += '<p>' + error_fields[i].parentNode.querySelector('span').innerHTML + '</p>';
            }

            error_message.innerHTML = name;
            error_message.className = 'u_message error_message';

            document.body.appendChild(error_message);


            setTimeout(function(){
                    document.body.removeChild(error_message);
                }, 3000);

            return false;
        }

        return true;
    }
}

function createZayav(rows) {
    var form = document.querySelector('#create-zayav');

    if(!form) return false;

    var submit = form.querySelector('input[type="submit"]');
    var hiddens = form.querySelector('#hiddens');

    if(form && rows.length > 0) {
        hiddens.innerHTML = '<input type="hidden" name="id" value="new" />';

        submit.removeAttribute('disabled');
        submit.setAttribute('title', 'Создать новую заявку из выбранных элементов');

        for(var key in rows) {
            var hidden = '<input type="hidden" name="ds[]" value="'+ rows[key].id +'" />';

            hiddens.innerHTML += hidden;
        }
    } else {
        submit.setAttribute('disabled', 'disabled');
        submit.setAttribute('title', 'Ничего не выбрано');
    }
}

function specContragentName() {
    var contragent_fld = document.querySelector('.dogovor_fields select[name="contragent_id"]');

    contragent_fld.addEventListener('change', getShowFio);

    var evt = document.createEvent("HTMLEvents");
    evt.initEvent("change", false, true);
    contragent_fld.dispatchEvent(evt);
}

function getShowFio() {
    var url = '/modules/ajax.php';

    var that = this;

    var value = that.value;

    $.get(
      url,
      {
        func: "spec_contragent_name",
        arg1: value
      },
      onAjaxSuccess
    );
     
    function onAjaxSuccess(data)
    {
        var d;

        try {
            d = JSON.parse(data);

            var contragent_fio_fld = document.querySelector('.dogovor_fields #contragent_fio');

            if(d == 1) {
                contragent_fio_fld.style.display = 'block';
            } else {
                contragent_fio_fld.style.display = 'none';
                contragent_fio_fld.querySelector('input').value = '';
            }
        }   catch (e) {
            alert ('Error in server respons!');
        // Save ot server error log?
            console.log('error json parse');
            console.log(data);
        }  
    }
}

function _dispalyBySelector(el, selector, display) {
	var items = el.querySelectorAll(selector);

	if(items) {
		for(var i = 0; i < items.length; i++) {
			items[i].style.display = display;
		}
	}
}

window.onload = function() {
	var left_sidebar = document.querySelector('.left_sidebar');
	var tab_pane = document.querySelector('.tab-pane');

	if(left_sidebar) {
		var header = document.querySelector('.top-header'),
			LS_MINWIDTH = 60;

		function hideTextLs() {
			if(left_sidebar.offsetWidth == LS_MINWIDTH) {
				tab_pane.style.left = LS_MINWIDTH + 10 + 'px';
				left_sidebar.style.textAlign = 'center';
				_dispalyBySelector(left_sidebar, 'span', 'none');
			} else {
				if(tab_pane) tab_pane.style.left = 14 + '%';
				left_sidebar.style.textAlign = 'left';
				_dispalyBySelector(left_sidebar, 'span', 'inline');
			}
		}

		window.addEventListener('scroll', function(e) {
			var pageY = window.pageYOffset || document.documentElement.scrollTop;

			if(pageY > header.offsetHeight) {
				left_sidebar.style.top = '0';
			} else {
				left_sidebar.style.top = header.offsetHeight - pageY + 'px';
			}
		}, false);

		window.addEventListener('resize', function(e) {
			hideTextLs();
		}, false);	

		hideTextLs();	
	}
	
	var related_links = document.querySelectorAll('[data-content]');
    for(var i = 0; i < related_links.length; i++) {
        related_links[i].addEventListener('click', toggleRelatedContent);
    }
	
	var user_zfo = document.querySelector('select[name="zfo_id"]');
	var role_id = document.querySelector('select[name="role_id"]');
	if(user_zfo && role_id) {
		zfoChange(role_id);
	}
}

function toggleRelatedContent(e) {
    var target = e.target || e.srcElement,
        content_id = target.getAttribute('data-content'),
        content = document.querySelector('#'+content_id),
        content_display = (content && content.style.display === 'none') ? '' : 'none';
    content.style.display = content_display;
}

function showInfoMessage(msg) {
    if(showTimeout !== null) {
        clearTimeout(showTimeout);
    }
    $('.infoMessage').remove();
    $(document.body).append('<div class="infoMessage">' + msg + '</div>');
    $('.infoMessage').slideDown(0);
    showTimeout = setTimeout(function(){
        $('.infoMessage').slideUp(0, function(){
            this.remove();
        });
    }, 3000);
}

function zfoChange(el) {
	var zfo_select = document.querySelector('select[name="zfo_id"]');
	if(el.value == 6) {
		zfo_select.removeAttribute('disabled');
	} else {
		zfo_select.value = 'null';
		zfo_select.setAttribute('disabled', 'disabled');
	}
}

function openCanvas(id) {
    var canvas = document.querySelector('#dog-'+id);
    if(canvas) {
        canvas.classList.toggle('active');
    }
	return false;
}

function savePfhdVer(el) {
    var href = el.href;
    var ver_name = prompt("Введите название новой версии:");
    if(ver_name && ver_name.trim() != '') {
        href += '?ver_name=' + encodeURIComponent(ver_name);
        window.location.assign(href); 
    } else {
        alert("Название версии не может быть пустым!");
    }
}