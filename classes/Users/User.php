<?php

/**
 * Класс User - для работы с пользователями (Создание/Изменение/Удаление)
 * Получение инормации о пользователе
 **/
class User 
{
	/**
	 * Возвращает id текущего пользователя
	 * @return integer 
	 */
	public static function getCurrent()
	{
		return $_SESSION['user_id'];
	}

	/**
	 * Логинит пользователя
	 * @param  string $login
	 * @param  string $pass
	 * @return boolean
	 */
	public static function login($login, $pass) {
		$sql = "SELECT id, pass FROM user WHERE login='$login'";
		$row = sql_array(sql_query($sql));
		if ($row == null || $row['pass'] != md5($pass)) {
			return false;
		} else {
			$_SESSION['user_id'] = $row['id'];
			$_SESSION['last_time'] = time();	
			return true;
		}
	}

	/**
	 * Разлогинивает пользователя
	 * @return void
	 */
	public static function logout() 
	{
		session_unset();
		$url = urlencode($_SERVER['REQUEST_URI']);
	    header('Location: /login.php?url='.$url);
	}

	/**
	 * Возвращаяет id роли по id юзера
	 * @return itnteger
	 */
	public static function getRoleId($userId)
	{
		return sql_get_value('role_id', 'user', "id = '$userId'");
	}
}