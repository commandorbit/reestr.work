<?php

/**
 * Класс Permission - предназначен для ассоциации с юзером прав доступа
 */
class PermissionBase 
{
	/**
	 * Возвращает id доступа по коду
	 * @param  string $code
	 * @return integer
	 */
	public static function getIdByCode($permissionCode) 
	{
		return sql_get_value('id', 'permission', "code = '$permissionCode'");
	}

	/**
	 * Проверяет есть ли указанные права у роли
	 * По умолчанию текущий юзер, если не указан id другого
	 * @param  string $code [наименование правила]
	 * @return boolean
	 */
	public static function userHasPermission($permissionCode, $userId = null) {
		$userId = (!is_null($userId)) ? $userId : User::getCurrent();
		//Петров
		if ($permissionCode == "may_edit_dogovor_gz" && $userId == 34) return true;
		$roleId = User::getRoleId($userId);
		$permissionId = self::getIdByCode($permissionCode);
		
		$sql = "SELECT * FROM role_permission WHERE role_id = '$roleId' AND permission_id = '$permissionId'";
		$permissionArray = sql_array(sql_query($sql));
		$result = (!empty($permissionArray)) ? true : false;
		return $result;
	}

	public static function userMaySave($table,$row) {
		return true;
	}

	/**
	 * Добаляет новый код доступа
	 * @param string $code
	 * @param string $description
	 * @return integer [MySQLi last insert id]
	 */
	public static function add($permissionCode, $description)
	{
		array_map("sql_escape",$argv);
		sql_query("INSERT INTO permission (`code`, `description`) VALUES ('$permissionCode', '$description')");
		return sql_last_id(); 
	}
}