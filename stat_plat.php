<?php
require 'init.php';
include 'stat/stat.php';
require_once 'stat/stat_stud.php';

$time_start = microtime(true);


$col = request_val('row','month_id');
$row = request_val('col','kosgu_id');
$d = request_val('d','');

function stat_sql($f,$w,$d='') {
	$s=	'select * from v_stat_plat WHERE true ';
	$sql_w=	 Stat::where($w);
	if ($sql_w) $s.= ' and ' .$sql_w;
	return $s;
}

$attrs = $DB::table_columns('v_stat_plat');
//unset ($attrs['d']);
unset ($attrs['s']);
unset ($attrs['nom']);

//$attrs['month_id']['options']=array(1=>'Январь',2=>'Февраль',3=>'Март',4=>'Апрель',5=>'Май',6=>'Июнь',7=>'Июль',8=>'Август',9=>'Сентябрь',10=>'Октябрь',11=>'Ноябрь',12=>'Декабрь');
$attrs['d_year']['options']=array(2012=>2012,2013=>2013,2014=>2014);
//print_r($attrs);

$cr=$attrs;
$cr["date_format(d,'%Y-%m')"]=array('label'=>'Год, месяц');

$Stat = makeStat($attrs, $cr)->set_row_col($row,$col)->set_f($_REQUEST,'w_')->set_func_data('stat_sql')->set_has_d(false)->set_url("/view_stat.php?t=v_stat_plat&stat&");


if (isset($_POST['to_excel'])) {	
	$Stat->to_excel();
	exit();
}

$is_ajax=(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest');

if( !$is_ajax ) {
	$title = 'Аналитика платежки';
	include 'template/header.php';

	echo "<h1>$title</h1>";
	echo '<div class="stat_filter_block">';
	echo $Stat->out_form();
	echo '</div>';
	echo '<div class="stat"></div>';
	include 'template/footer.php';
} else {
	if (isset($_POST['show']))  {
		$_SESSION['last_stat']=$_SERVER['REQUEST_URI'];
		echo '<h4>' . $Stat->opisanie() . '</h4>';
		echo $Stat->print_cross();
	}
	exit();
}

?>