<?php

class ContragentModel extends BaseModel 
{
	/**
	 * Таблица по умолчанию для данной модели
	 * @var string
	 */
	private $table = 'contragent';
	
	/**
	 * Реализация абстрактной функции родителя
	 * @param  array $row
	 * @return void
	 */
	public function beforeSave($row) 
	{
		if(isset($row['is_jur']) && $row['is_jur']) {
			$inn = $row['inn'];
			if(is_null($inn) || trim($inn) == '') {
				throw new Exception('Поле ИНН для контрагента с признаком юр. лица не может быть пустым');
			} else {
				if($this->innExists($inn, $row['id'])) {
					throw new Exception('Контрагент с указанным ИНН уже существует');
				}
			}
		}
	}
	
	/**
	 * Проверка на существование ИНН
	 * @param  string $inn 
	 * @return integer
	 */
	protected function innExists($inn, $rowId) 
	{
		if($rowId && trim($rowId) != '') {
			$result = sql_get_value('id', $this->table, "inn = '$inn' and id<>$rowId");
		} else {
			$result = sql_get_value('id', $this->table, "inn = '$inn'");
		}
		return $result;
	}
}