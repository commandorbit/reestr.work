<?php
require_once __DIR__ . "/define.php";

function is_ro() {
	$user_id = $_SESSION['user_id'];
	return sql_get_value("is_ro","user","id='$user_id'");
}

function is_ku() {
    $user_id = $_SESSION['user_id'];
    return sql_get_value("is_ku","user","id='$user_id'");
}

function is_ku_checked($dogovor_id) {
    return sql_get_value('concluded_ku', 'dogovor', "id = '$dogovor_id'");
}

function dogovor_has_file($id) {
    return sql_get_value('id', 'dogovor_files', "dogovor_id = '$id'");
}

/*
function user_org_id() {
	$user_id = $_SESSION['user_id'];
	return sql_get_value("org_id","user","id='$user_id'");
}

function user_may_status() {
	$user_id = $_SESSION['user_id'];
	return sql_get_value("may_status","user","id='$user_id'");
}
*/

function user_may_edit_plat() {
	$user_id = $_SESSION['user_id'];
	return sql_get_value("may_edit_plat","user","id='$user_id'");
}



function user_may_approve_limits() {
	$user_id = $_SESSION['user_id'];
	return sql_get_value("may_approve_limit","user","id='$user_id'");
}

function user_may_edit_many() {
	$user_id = $_SESSION['user_id'];
	return sql_get_value("may_edit_many","user","id='$user_id'");
}

function user_may_smeta() {
	if (is_admin() || !user_has_zfo()) return true;
	$user_id = $_SESSION['user_id'];
	return sql_get_value("may_smeta","user","id='$user_id'");
}

function may_dogovor() {
	$user_id = $_SESSION['user_id'];
	return sql_get_value("may_dogovor","user","id='$user_id'");
}

function may_merge() {
	$user_id = $_SESSION['user_id'];
    return sql_get_value('may_sprav_union','user',"id='$user_id'");
}

function may_create_zayav() {
	$user_id = $_SESSION['user_id'];
    return sql_get_value('may_create_zayav','user',"id='$user_id'");
}

function may_edit_zayav() {
    $user_id = $_SESSION['user_id'];
    return sql_get_value('may_edit_zayav','user',"id='$user_id'");
}

function may_zayav_dog_status($dogovor_id) {
	$may_zayav = sql_get_value('may_zayav', 'dogovor_status', "id IN(SELECT dogovor_status_id FROM dogovor WHERE id='$dogovor_id')");

	return $may_zayav;
}

//ZFO
function check_may_edit_zfo($id, $table) {
    return sql_get_value('zfo_id', $table, "id='$id'");
}

function user_role() {
	$user_id = (int) $_SESSION['user_id'];
	return sql_get_value('role_id','user',"id=$user_id");
}

function user_has_zfo() {	
	return user_role() == USER_ROLE_ZFO;
}

/*
function user_has_zfo() {	
	$zfo_rows = sql_rows("SELECT zfo_id FROM user_zfo WHERE user_id = '$user_id'");
	$has = (count($zfo_rows)) ? true : false;
	return $has;
}
*/

function user_zfo_id_str() {
	$user_id = $_SESSION['user_id'];
	if (count(user_zfo_ids($user_id))) {
		return implode(',',user_zfo_ids($user_id));
	}
	return 'NULL';
}

function user_zfo_ids($user_id = 0) {
	if(!$user_id) $user_id = $_SESSION['user_id'];
	$zfo = array();
	$zfo_rows = sql_rows("SELECT zfo_id FROM user_zfo WHERE user_id = '$user_id'");
	foreach($zfo_rows as $row) {
		array_push($zfo, 0 + $row['zfo_id']);
	}
	return $zfo;
}

function user_zfo_id() {
	$user_id = $_SESSION['user_id'];
	return sql_get_value("zfo_id","user","id='$user_id'");
}
//END ZFO

//Вытаскивает все строчки (не забыть!)
function dogovorCheckLimit($dogovor_id) {
    $dogovor_zfo = sql_get_value('zfo_id', 'dogovor', "id='$dogovor_id'");
    $zfo_name = sql_get_value('name', 'zfo', "id='$dogovor_zfo'");
    $dogovor_year = sql_get_value('fp_year', 'dogovor', "id = '$dogovor_id'");
    if($dogovor_year == 2014) return true;
	if ($dogovor_year <= 2018) {
		$s = ("SELECT fin_source_id, sum(amount) AS sum FROM dogovor_sostav 
				INNER JOIN dogovor ON dogovor_sostav.dogovor_id = dogovor.id 
				WHERE dogovor.zfo_id = '$dogovor_zfo'  AND dogovor.fp_year = '$dogovor_year'
				AND (dogovor.dogovor_status_id IN (SELECT id FROM dogovor_status WHERE use_limit = 1)
				OR dogovor.id = '$dogovor_id')
				GROUP BY dogovor_sostav.fin_source_id");
		$rows = sql_rows($s);
		foreach($rows as $row) {
			$fin_source_id = $row['fin_source_id'];
			if ($row['sum']) {
				$limit = sql_get_value('sum', 'zfo_limit', "zfo_id='$dogovor_zfo' AND fin_source_id='$fin_source_id' AND fp_year='$dogovor_year'");
				$fin_source_name = sql_get_value('name', 'fin_source', "id='$fin_source_id'");
				if(!$limit) {
					SystemMessage::set('По источнику финансирования за '.$dogovor_year.' год: "' . $fin_source_name . '" не установлен лимит для ЦФО ('.$zfo_name.')');
					return false;
				} elseif($row['sum'] > $limit) {
					SystemMessage::set('По источнику финансирования за '.$dogovor_year.' год: "' . $fin_source_name . '" превышен лимит для ЦФО ('.$zfo_name.'). <br /><br /> На: ' . ($row['sum'] - $limit) . ' руб.');
					return false;
				}
			}
		}		
	} 
	$s = "SELECT DISTINCT ds.fin_source_id,a.kvr,p.smeta_y y
	FROM dogovor_sostav ds
	INNER JOIN fp_article a ON ds.fp_article_id = a.id 
	INNER JOIN period p ON ds.period_id = p.id
	WHERE dogovor_id = '$dogovor_id' AND p.smeta_y>=2019";
	$rows = sql_rows($s);
	foreach($rows as $row) {
		
		if (!_checkLimitYZfoFinSourceKvr($dogovor_id,$row['y'],$dogovor_zfo,$row['fin_source_id'],$row['kvr'])) {
			//echo "BAD";
			return false;
		} else {
			//echo "OK";
		}
		
	}
    return true;
}

function _checkLimitYZfoFinSourceKvr($dogovor_id,$y,$zfo_id,$fin_source_id,$kvr) {
	$s = "SELECT SUM(ds.amount)
	FROM dogovor_sostav ds
	INNER JOIN dogovor d ON ds.dogovor_id = d.id
	INNER JOIN dogovor_status status ON d.dogovor_status_id = status.id
	INNER JOIN fp_article a ON ds.fp_article_id = a.id 
	INNER JOIN period p ON ds.period_id = p.id
	WHERE (status.use_limit=1 OR d.id=$dogovor_id) AND d.zfo_id=$zfo_id AND ds.fin_source_id = $fin_source_id AND a.kvr ='$kvr' AND p.smeta_y=$y";
	
	$amount = sql_fetch_value($s);
	if ($amount) {
		$s = "select sum(amount) from zfo_smeta_limit where y=$y  and zfo_id=$zfo_id and fin_source_id=$fin_source_id and kvr=$kvr";
		$limit = sql_fetch_value($s);
		if ($limit<$amount) {
			$fin_source_name = sql_get_value('name', 'fin_source', "id='$fin_source_id'");
			$zfo_name = sql_get_value('name', 'zfo', "id='$zfo_id'");
			SystemMessage::set("По источнику финансирования $fin_source_name за период $y год  превышен лимит для ЦФО ($zfo_name)<br /><br /> На: " . ($amount - $limit) . ' руб.');
			return false;
		}
	}
	return true;
}

function ds_zayav_ost($dogovor_sostav_id) {
    $sum = NULL;

    $zs_sum = ds_zs_sum($dogovor_sostav_id);
    $ds_sum = sql_get_value('amount', 'dogovor_sostav', "id = '$dogovor_sostav_id'");

    if($zs_sum) $sum = round($ds_sum - $zs_sum, 2);

    return $sum;
}

function ds_zs_check_sum($zayav_sostav_id, $zs_current_sum) {

    $zs_current_sum = str_replace(',','.',$zs_current_sum);
    $zs_current_sum = str_replace(' ','',$zs_current_sum);
    $ds_id =   sql_get_value("dogovor_sostav_id","zayav_sostav", "id = '$zayav_sostav_id'");
    $zs_sum = ds_zs_sum($ds_id);
    //sql_get_value('sum(sum)', 'zayav_sostav', "dogovor_sostav_id in (select dogovor_sostav_id from zayav_sostav where id = '$zayav_sostav_id') and id not in ($zayav_sostav_id)");

    $ds_sum = sql_get_value('amount', 'dogovor_sostav', "id = '$ds_id'");
    $old_sum = sql_get_value('sum', 'zayav_sostav', " id = '$zayav_sostav_id'");
    $sum = round($ds_sum - ($zs_current_sum - $old_sum + $zs_sum),2);

    return $sum;
}

function ds_zs_sum($dogovor_sostav_id) {
    $sum = sql_get_value('sum(sum)', 'zayav_sostav', "dogovor_sostav_id = '$dogovor_sostav_id' AND zayav_id in (select id from zayav where zayav_status_id in (select id from zayav_status where is_good = '1'))");
    return $sum;
}

function insert_row($table, $row) {
    if(is_array($row)) {
        foreach($row as $key => $value) {
            if($value == '') $row[$key] = NULL;
            else $row[$key] = "'" . sql_escape($value) . "'";
        }

        $sql_fields = implode(',', array_keys($row));
        $sql_values = implode(',', $row);

        sql_query("INSERT INTO $table ($sql_fields) VALUES ($sql_values)");
    }
}

function zfo_limit() {
    $zfo = user_zfo_id_str();
    $user_id = $_SESSION['user_id'];
    return sql_get_value("sum","zfo_limit","zfo_id in ($zfo)");
}


function may_view_plat() {
	$user_id = $_SESSION['user_id'];
	return sql_get_value("may_view_plat","user","id='$user_id'");
}

function user_may_any_dogovor_status() {
    if(!user_has_zfo() && !is_ro()) {
        return true;
    }
    return false;
}

//key for subtable - dogovor_id and etc
function last_table_log($table,$key,$id) {
    $res = sql_query("select * from log_$table where $key='$id' order by ts desc limit 1 "); 
    return sql_array($res);
}

function out_log_link($table,$id,$key=false) {
    if (!$key) $key=sql_table_key($table);   
    if ($log=last_table_log($table,$key,$id)) {
        echo '<a href="/tedit.php?t=log_'.$table.'&f_'.$key.'='.$id .'">История: ' . sql_get_value('name','operation_type',"id=".$log['operation_type']) .' ' .sql_get_value('fio','user',"id=".$log['user_id']) . ' '. $log['ts'] .'</a>';
    }
}

function da_net_name($flag) {
    $a = array_da_net();
    return (isset($a[$flag])) ? $a[$flag] : '?';
}

function array_da_net() {
    return array(0=>'Нет',1=>'Да');
}

function request_numeric_val($name,$def) {
    return (isset($_REQUEST[$name]) && is_numeric($_REQUEST[$name])) ? $_REQUEST[$name] : $def;
}

//Возвращает массив id источников финансирования(fin_source_id) для текущего пользователя
//По ЦФО id
function user_own_fin_sources() {
	$fin_sources = array();
	$zfo = user_zfo_id_str();
	$fin_sources_assoc = sql_rows("select id from fin_source where zfo_id in ($zfo)");
	foreach ($fin_sources_assoc as $fin_source) {
		$fin_sources[] = $fin_source['id'];
	}

	return $fin_sources;
}

//Перегоняем массив в строку для вставки в SQL запрос
function array_to_sql_string($set) {
	if (count($set)) {
		$array_list = implode(',', $set);
	} else {
		$array_list = "null";
	}

	return $array_list;
}

function sql_select_row($table,$where) {    
    $cols = sql_table_columns($table);
    $data = array();
    foreach ($cols as $col=>$props) {
        $data[$col]=$props['default'];
    }
    $s = "SELECT * FROM  $table WHERE $where ";
    $rows = sql_rows($s);
    foreach ($rows as $row) {
        foreach ($cols as $col=>$props) {
            $data[$col] = ($props['type']=='date') ? sql_date2rus($row[$col]) : $row[$col];
        }
    }
    return $data;
}

function fill_array_with_post($array){
    $new_array = array();
    foreach ($array as $key=>$value) {
        if (isset($_POST[$key])) $new_array[$key] = $_POST[$key];
        else $new_array[$key] = $value;
    }
    return $new_array;
}

function passgen() {
    $length=7;
    $strength=0;
    $vowels = 'aeuy';
    $consonants = 'bdghjmnpqrstvz';
    if ($strength & 1) {
        $consonants .= 'BDGHJLMNPQRSTVWXZ';
    }
    if ($strength & 2) {
        $vowels .= "AEUY";
    }
    if ($strength & 4) {
        $consonants .= '23456789';
    }
    if ($strength & 8) {
        $consonants .= '@#$%';
    }
    $password = '';
    $alt = time() % 2;
    for ($i = 0; $i < $length; $i++) {
        if ($alt == 1) {
            $password .= $consonants[(rand() % strlen($consonants))];
            $alt = 0;
        } else {
            $password .= $vowels[(rand() % strlen($vowels))];
            $alt = 1;
        }
    }
    return $password;
}

function plat_zayav_sum_update($plat_id, $plat_sum) {
    //Получаем id заявки
    $zayav_id = sql_get_value('zayav_id', 'plat', "id = '$plat_id'");

    //обнуляем оплаченную сумму
    sql_query("UPDATE zayav_sostav SET plat_sum = 0 WHERE zayav_id = '$zayav_id'");

    $plat_sum_all = sql_fetch_array("SELECT Sum(s) as sum FROM plat WHERE zayav_id = '$zayav_id' AND id != '$plat_id' GROUP BY zayav_id");

    $plat_sum_all = (float)$plat_sum_all['sum'];

    $plat_sum += $plat_sum_all;

    $sum_zayav = sql_fetch_array("SELECT Sum(sum) as sum FROM zayav_sostav WHERE zayav_id = '$zayav_id' GROUP BY zayav_id");

    $sum_zayav = (float)$sum_zayav['sum'];

    $zayav_rows = sql_rows("SELECT * FROM zayav_sostav WHERE zayav_id='$zayav_id'");

    $n = count($zayav_rows);

    $sum = 0;

    for($i = 0; $i < $n; $i++) {
        if($i + 1 == $n) {
            $sum_oplat = round(($plat_sum - $sum), 2);
        } else {
            $sum_oplat = $sum_zayav==0 ? 0 : round((($zayav_rows[$i]['sum'] / $sum_zayav) * $plat_sum), 2);
        }

        $sum += $sum_oplat;

        $row_id = $zayav_rows[$i]['id'];

        sql_query("UPDATE zayav_sostav SET plat_sum = $sum_oplat WHERE id = $row_id");
    }
}


function get_saved_pivot_reports($pivot_type) {
    $user_id = $_SESSION['user_id'];
    return sql_rows("select id, name from pivot_reports where user_id = '$user_id' and pivot_type='$pivot_type' order by name");
}

function plan_dohod_last_ver($y=2016) {
	if ($y>2017) { //Пока 2018 и 2019 заносятся под 2017
		$y=2017;
	}
	$last_id = sql_get_value('id', 'plan_dohod_ver', "y=$y ORDER BY ts DESC LIMIT 1");
	
	
	return $last_id;
	
}

function get_gz_44($year, $ver) {
	//$year = sql_escape($year);
	$ver = sql_escape($ver);
	return sql_array(sql_query("SELECT * FROM gz_44 WHERE year = '$year' AND ver = '$ver'"));
}

function current_gz_44_id() {
	return sql_get_value('id', 'gz_44', "true ORDER BY create_ts DESC LIMIT 1");
}

function gz_44_may_edit($id = false) {
	$id = (!$id) ? current_gz_44_id() : sql_escape($id);
	return sql_get_value('may_edit', 'gz_44_sostav', "id in (SELECT gz_44_status_id FROM gz_44 WHERE id = '$id')");
}

function isset_dogovor_gz($dogovor_id) {
	return sql_get_value('id', 'dogovor_gz', "dogovor_id = '$dogovor_id'");
}

function gz_44_isset_draft() {
	//return sql_get_value('id', 'gz_44', "gz_44_status_id = 1 OR gz_44_status_id = 2");
	return sql_get_value('id', 'gz_44', "gz_44_status_id = 1 LIMIT 1");
}

function gz_44_draft_id() {
	return sql_get_value('id', 'gz_44', "gz_44_status_id = 1");
}

function gz_44_status() {
	
}

function gz_44_sostav($id) {
	$id = sql_escape($id);
	return sql_get_value('id', 'gz_44_sostav', "dogovor_gz_id = '$id'");
}

function dogovor_gz_may_edit($dogovor_gz_id) {
	$gz_44_sostav_id = gz_44_sostav($dogovor_gz_id);
	if($gz_44_sostav_id) {
		$gz_44_id = sql_get_value('gz_44_id', 'gz_44_sostav', "id = '$gz_44_sostav_id'");
		return (boolean) sql_get_value('may_edit', 'gz_44_status', "id in (SELECT gz_44_status_id FROM gz_44 WHERE id='$gz_44_id')");
	}
	return false;
}

function dogovor_gz_may_create_ver($dogovor_gz_id) {
	$in_draft = false;
	$gz_44_sostav_id = gz_44_sostav($dogovor_gz_id);
	if($gz_44_sostav_id) {
		
	}
}

function gz_44_status_name($year, $ver) {
	$year = sql_escape($year);
	$ver = sql_escape($ver);
	return sql_get_value('name', 'gz_44_status', "id in (SELECT gz_44_status_id FROM gz_44 WHERE ver = '$ver' AND year = '$year')");
}

function dogovor_gz_vers($dogovor_id) {
	return sql_rows("SELECT id,ver FROM dogovor_gz WHERE dogovor_id = '$dogovor_id' ORDER BY ver");
}