<?php

class Utils {
	public static function TDfromRows($rows,$columns,$params) {
		global $DB;
		//$DB = new ReestrDB();
		$TD = new TD($params['t']);
		$TD->show_checkbox=false;

		$TD->primary_key=null;
		$TD->columns=$columns;
		$TD->filter=null;
		$TD->dataFilter=null;
		$TD->params=$params;
		$TD->data = &$rows;
		//$TD->init($res,'sql_array');
		return $TD;
	}

	public static function td_column($fld_name,$params) {
		if (empty($label)) $label=$fld_name;
		$param_names = ['label','format','has_sum','width','description','editable','url','date_options','options','edit_options','tree_options','default_value','align','type','null','is_select_code'];
		
		foreach($param_names as $p) {
			$col[$p] = (isset($params[$p])) ? $params[$p] : null; 
		}
		if (empty($col['label'])) $col['label'] = $fld_name;
		if (empty($col['width'])) $col['width'] = strlen($col['label'])*8;
		if (empty($col['align'])) $col['align'] = 'center';
		$col['hidden'] = false;
		$col['name'] = $fld_name;
		$col['length'] = 20;
		return $col;
	}

	public static function cross($res,$row_fld,$col_fld,$sum_fld) {
		$r = [];
		$cols = [];
		while($row = sql_array($res)) {
			$grp_col = $row[$col_fld];
			$cols[$grp_col] = true;
			ksort($cols);
			$grp_row = $row[$row_fld];
			if (!isset($r[$grp_row])) $r[$grp_row] = [];
			if (!isset($r[$grp_row][$grp_col])) $r[$grp_row][$grp_col] = 0;
			$r[$grp_row][$grp_col] += $row[$sum_fld];
		}
		$rows = [];

		foreach ($r as $grp_row=>$dummy) {
			$row = [];
			$row[$row_fld] = $grp_row;
			$itogo = 0;
			
			foreach ($cols as $col=>$dummy) {
				$itogo += $row[$col] = isset($r[$grp_row][$col]) ? $r[$grp_row][$col] : null;
			}
			$row['itogo'] = $itogo;
			$rows[] = $row;
		}
		return $rows;
	}

	public static function nf($v) {
		return number_format($v, 2, ',', ' ');
	}
	
	public static function date_format($date,$new_format) {
		// добавлено dim 31.01.17
		// date может быть в форматах YYYY-mm-dd или dd.mm.YYYY

		$regexp_sql_d = "/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])/";
		$regexp_rus_d = "/^(0[1-9]|[1-2][0-9]|3[0-1])\.(0[1-9]|1[0-2])\.[0-9]{4}/";

		$new_date = '';
		if ($date && (preg_match($regexp_sql_d,$date) || preg_match($regexp_rus_d,$date))) {
			$date = new DateTime($date);
			$new_date = $date->format($new_format);
		}
		return $new_date ? $new_date : $date;
	} 
	
	public static function toHTML($rows, $labels = array()) {
		if(!count($rows)) return false;
	
		$html = "";
		$cols = (isset($rows[0])) ? array_keys($rows[0]) : array();
		if(count($labels)) {
			foreach($cols as &$col) {
				// добавил Dim 31.01.17
				// для дополнительных аттрибутов label[col] может быть массивом
				if (isset($labels[$col]['label'])) $col = $labels[$col]['label'];
				// оставлено пока для совместимости label[col] строка, содержащая только label
				elseif (isset($labels[$col])) $col = $labels[$col];
			}
		}
		$html .= "<table class='td-table' border='1'>";
		$html .= "<tr>";
		for($i = 0; $i < count($cols); $i++) {
			$html .= "<th nowrap>" . $cols[$i] . "</th>";
		}
		$html .= "</tr>";
		foreach($rows as $row) {
			foreach($row as $col=>$col_value) {
				$align = '';
				$nowrap = '';
				if (isset($labels[$col]['format'])) {
					// 31.01.17 dim добавил форматы
					if ($labels[$col]['format']=='sum') {
						$align = 'right';
						$nowrap = 'nowrap';
						$col_value = self::nf($col_value);
					} elseif ($labels[$col]['format']=='date') {
						$align = 'center';
						$nowrap = 'nowrap';
						$col_value = utils::date_format($col_value,'d.m.Y');
					}
				}
				$html .= "<td align='$align' $nowrap>" . $col_value . "</td>";
			}
			$html .= "</tr>";
		}
		$html .= "</table>";
		return $html;
	}
}
