<?php
class mail {
	public static function send($email,$subject,$body,$from) {
		$headers = 'MIME-Version: 1.0' . "\r\n" ;
		$headers .= "From: $from\r\n";
		$headers .= "Reply-To: $from\r\n";
		$headers .= 'Content-type: text/html; charset="utf-8"';
		mail($email,self::utf_encode($subject),$body,$headers,"-f$from");
	}
	
	public static function is_valid_email($email) {
		if (!preg_match("/^([a-zA-Z0-9])+([a-zA-Z0-9\._-])*@([a-zA-Z0-9_-])+([a-zA-Z0-9\._-]+)+$/" , $email)) {
			return false;
		} else {
			return self::is_mx_exists($email);
		}
	}
	
	public static function is_mx_exists($email) {
		$hosts=array();
		list($user,$domain)=explode('@',$email);
		return getmxrr($domain,$hosts);
	}
	
	public static function utf_encode($s) {
		//$s = iconv("cp1251","utf-8",$s);
		return '=?utf-8?B?'.base64_encode($s).'?=';
	}
}
?>