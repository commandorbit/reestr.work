<?php

function error_die($msg) {
	echo $msg;
	echo error_backtrace();
	die();
}

// 3.12. start 2 private functions added
function error_backtrace($traces_to_ignore = 2){
    $traces = debug_backtrace();
    $ret = array();
    foreach($traces as $i => $call){
        if ($i < $traces_to_ignore ) {
            continue;
        }

        $object = '';
        if (isset($call['class'])) {
            $object = $call['class'].$call['type'];
            if (is_array($call['args'])) {
                foreach ($call['args'] as &$arg) {
                    backtrace_get_arg($arg);
                }
            }
        }        

        
		$r='#'.str_pad($i - $traces_to_ignore, 3, ' ');
        $r.=$object.$call['function'].'('.implode(', ', $call['args']);
        $r.=') called at ['.$call['file'].':'.$call['line'].']';
		$ret[] =$r;
    }
    return implode("<br>\n",$ret);
}

function backtrace_get_arg(&$arg) {
    if (is_object($arg)) {
        $arr = (array)$arg;
        $args = array();
        foreach($arr as $key => $value) {
            if (strpos($key, chr(0)) !== false) {
                $key = '';    // Private variable found
            }
            $args[] =  '['.$key.'] => '.backtrace_get_arg($value);
        }

        $arg = get_class($arg) . ' Object ('.implode(',', $args).')';
    }
}
