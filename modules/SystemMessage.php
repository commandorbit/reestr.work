<?php

class SystemMessage {
	protected static $systemMessage = "";

	public static function set($message) {
		if (self::$systemMessage) self::$systemMessage .= "\n<br>";

		self::$systemMessage .= $message;
	}

	public static function get() {
		return self::$systemMessage;
	}
}

?>