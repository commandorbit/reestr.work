<?php

function options_yes_no($val,$show_all=false) {
	$s='';
	if ($show_all) {
		$sel = ($val=='_all') ? 'selected' :'';
		$s.='<option '. $sel .' value="_all">Все</option>';
	}
	$sel_1 = ($val===1) ? 'selected' :'';
	$s.='<option ' . $sel_1. ' value="1">Да</option>';
	$sel_2 = ($val===0) ? 'selected' :'';
	$s.='<option ' . $sel_2. ' value="0">Нет</option>';
	return $s;
}
function arr_2options($a,$value,$show_all=false) {
	$s="";
	if ($show_all)
		$s.='<option value="_all">Все</option>';
	$s.='<option value="">Нет</option>';		
	foreach ($a as $key=>$val) {
		$sel=$key==$value ? 'selected' :'';
		$s.= '<option ' . $sel . ' value="' . $key . '">' . $val. "</option>\n";
	}
	return $s;
}

function arrayFromTable($table,$fields=array()) {
	if (count($fields)==0) {
		$cols=table_columns($table);	
		$col=current($cols);
		$fields[0]=$col['name'];
		$col=next($cols);
		$fields[1]=$col['name'];
	}
	$sql='select ' .$fields[0] .',' . $fields[1] . ' from ' . $table . ' order by ' . $fields[1];
	return arrayFromSql($sql);
}

function buildTree($elements, $parentId = 0) {
    $branch = array();

    foreach ($elements as $element) {
        if ($element['parent_id'] == $parentId) {
            $children = buildTree($elements, $element['id']);
            if ($children) {
                $element['children'] = $children;
            }
            $branch[$element['id']] = $element;
            unset($elements[$element['id']]);
        }
    }
    
    return $branch;
}

function treeFromTable($table, $fields_array) {
	$sql_fields = implode(',', $fields_array);

	$result = sql_query("SELECT $sql_fields FROM $table");

	$set = array();

	while ($row = $result->fetch_assoc()) {
	    $set[$row['id']] = array('id'=>$row['id'], 'name'=>$row['name'], 'shift'=>$row['shifr'], 'is_folder'=>$row['is_folder'], 'parent_id'=>$row['parent']);
	}

	return buildTree($set);
}

/*

function arrayFromSql($sql) {
	$r=sql_query($sql);
	$a=array();
	while ($row=sql_array($r)) {
		$a[$row[0]]=$row[1];
	}
	return $a;	
}
*/
?>