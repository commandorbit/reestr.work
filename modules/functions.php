<?php

function staff_user_id_user_kod($staff_user_id) {
	if (!$staff_user_id) return null;
	return sql_get_value("id","user","staff_user_id='$staff_user_id'");
}

function current_user_kod() {
	if ($user_kod = session_val('user_id')) {
		$_SESSION['last_time'] = time();
		return $user_kod;
	} elseif ($user_kod = staff_user_id_user_kod(session_val('staff_user_id'))) {
		$_SESSION['last_time'] = time();
		$_SESSION['user_id'] = $user_kod;
		return $user_kod;
	} else {
		return false;
	}
}

function current_user_id() {
	return current_user_kod();
}

function user_may_edit_table($table) {
// временная заглущка переделать на базе ролей
	$PETROFF_UID = 34;
	
	
	$user_id = current_user_id();
	// Хак Для Петррова
	if ( $user_id == $PETROFF_UID && in_array($table,['natural_indicator','natural_indicator_category']) ) {
		return true;
	}
	$tables_1 =	[
	 'zfo'
	,'zfo_limit'
	,'kosgu_fp_limit'
	,'fin_source'
	,'fp_article'
	,'fp_object'
	,'fp_object_type'
	,'fp_article_zfo'
	,'fp_article_income'
	,'fp_years'
	,'document_type'
	,'period'
	,'place_method'
	,'plan_grafic'
	,'booker_fin_source'
	,'dogovor_status'
	,'prorektor'
	,'zayav_status'
	,'bank_acc'
	,'kosgu'
	,'pfhd_article'
	,'vid_rashodov'
	,'okved'
	,'contragent'
	,'holiday'
	,'stat_pfhd_ver'
	,'unit'
	,'ls_remains'
	,'natural_indicator'
	,'natural_indicator_category'
	,'plat_type'
	,'zayav_status'
	,'bank_acc'
	];
	if (substr($table,0, strlen('virt_'))=='virt_') {
		return false;
	}
	if (in_array($table,$tables_1)) {
		return (is_admin() || !user_has_zfo());
	}
	if ($table == 'plat' && !user_may_edit_plat()) {
		return false;
	}
	return true;
}

function user_has_permission($flag)  {
	return user_may($flag);
}

function user_may($flag) {
	$user_id = current_user_id();
	$permission_id = sql_get_value("id","permission","code='$flag'");
	$user = sql_get_array("user","id='$user_id'");
	$role_id = $user['role_id'];

	/*
	
	dim:: предлагаю след.механизм работы
	1. проверили флаг в ролях
	2. только если 1 false: проверили флаг пользователя 
	
	*/

	$may = 0;
	if ($role_id && $permission_id) {
		$may = sql_get_value("count(*)","role_permission","role_id='$role_id' and permission_id='$permission_id'");
	}
	if (!$may && array_key_exists($flag,$user)) {
		$may = $user[$flag] == 1 ? true : false;
	}
	return $may;
}

function user_zfos() {
	$user_id = current_user_id();
	$user = sql_get_array("`user`","id='$user_id'");
	$role = sql_get_array("`role`","id='".$user['role_id']."'");
	$zfos = [-1];
	// не описана роль проректора
	if ($role['is_zfo']) {
		$rows = sql_rows("SELECT zfo_id FROM user_zfo WHERE user_id='$user_id'");
		foreach ($rows as $row) $zfos[] = $row['zfo_id'];
	} else {
		$rows = sql_rows("SELECT id FROM zfo ");
		foreach ($rows as $row) $zfos[] = $row['id'];
	}
	return $zfos;
}


function is_dim() {
	return (isset($_SERVER) && array_key_exists('REMOTE_ADDR',$_SERVER)) ? $_SERVER['REMOTE_ADDR']=='10.123.124.210' : false;
}

function session_val($name,$def=0) {
	return (isset($_SESSION[$name])) ? $_SESSION[$name] : $def;
}

function login($login,$pass) {
	$s = "SELECT id, pass FROM user WHERE login='$login'";

	$res = sql_query($s);
	$row = sql_array($res);

	if ($row==null || $row['pass'] != md5($pass)) {
		return false;
	} else {
		$_SESSION['user_id'] = $row['id'];
		$_SESSION['last_time'] = time();	

		return true;
	}
}

function go_exit() 
{
	//$UID = isset($_SESSION['user_id']) ? $_SESSION['user_id'] : 0;
	session_unset();
        //unset($_SESSION['user_id']);
	//setcookie('last_user_id', $UID, time() - 3600 /* 1час */ * 24 * 10 /*10 дней*/);
	$url = urlencode($_SERVER['REQUEST_URI']);
	header('Location: /login.php?url=' . $url);
	exit;
}

function time_out() {
	$time_tek = time();
	$time_login = $_SESSION['last_time'];
	$time_zapas = 60*60*10; /* 5 hours */	
	$t = ($time_login + $time_zapas) - $time_tek;
	$_SESSION['last_time'] = time();
	return ($t < 0);
}

function change_user($id) {
	if(is_admin() || is_admin_changed()) {
		if(!isset($_SESSION['last_user_id'])) $_SESSION['last_user_id'] = $_SESSION['user_id'];
		$_SESSION['user_id'] = $id;
	}
}

function login_info() {
	$result = null;
	$user_id = $_SESSION['user_id'];
	if(is_sysadmin()) {
		$all_users = sql_rows('SELECT * FROM user ORDER BY fio ASC');
		$result = '<form method="GET" action="/change_user.php" class="user_change_form"><select name="newer_user_id" class="user_select" data-tooltip="Вы можете войти под другим пользователем">';
			foreach ($all_users as $user) {
				$selected = ($_SESSION['user_id'] == $user['id']) ? 'selected' : '';
				$result .= '<option '.$selected.' value="'. $user['id'] .'">' . $user['fio'] . ' (' . $user['login'] . ')' . '</option>';
			}
		$result .= '</select><input type="submit" data-tooltip="Подтвердите выбор" value="OK" /></form>';
	} else {
		//$result = sql_get_value("login","user","id='$user_id'");
		$result = '<div class="user_info2">' . sql_get_value("fio", "user", "id = '$user_id'") . '</div>';
	}
	return $result;
}

//AVD
function user_pic($width = 200) 
{
	$user_id = $_SESSION['user_id'];
	$user_profile = sql_rows("SELECT * FROM user WHERE id = $user_id");	
	//$picture_url = $user_profile[0]['picture'] ? $user_profile[0]['picture'] : '/img/users/default-user.jpg';
	//return "/imagecut.php?filename=$picture_url&width=$width";
	return "/userpic.php?id=$user_id&width=$width";
}

function user_login($user_id) {
	return sql_get_value("login","user","id='$user_id'");
}

function is_admin_changed() {
	if (!isset($_SESSION['last_user_id'])) return false;
	$user_id = $_SESSION['last_user_id'];
	return sql_get_value("is_admin","user","id='$user_id'");
}

function is_admin() {
	$user_id = $_SESSION['user_id'];
	$role_id = sql_get_value("role_id","user","id='$user_id'");
	return $role_id && role_is_admin($role_id) || is_sysadmin();
}

function role_is_admin($role_id) {
	return $role_id==1;
}

function is_sysadmin() {
	$user_id = $_SESSION['user_id'];
	return sql_get_value("is_admin","user","id='$user_id'");
}


function refresh_after_save(){
	$url=parse_url($_SERVER['REQUEST_URI'],PHP_URL_PATH);
	$url .= '?' . http_build_query($_GET); //в Get Меняем id для new;
	//echo $url;
	header('Location:'.$url);
	exit();
}

function int_param($name,$default,$arr=null) {
	if (is_null($arr)) $arr=$_REQUEST;
	return isset($arr[$name]) && is_numeric($arr[$name]) ?  $arr[$name] : $default;
}
// not escaped
function str_param($name,$default,$arr=null) {
	if (is_null($arr)) $arr=$_REQUEST;
	return isset($arr[$name])  ?  $arr[$name] : $default;
}

function get_header($table) {
	$h =  sql_get_value("short_name","tab","`table`='$table'");
	return ($h) ? $h : $table;
}

function get_table_description($table) {
	$h =  sql_get_value("full_name","tab","`table`='$table'");
	return ($h) ? $h : '';
}

function request_val($name,$default) {
	$arr=$_REQUEST;
	return isset($arr[$name])  ?  $arr[$name] : $default;
}

function print_options($option_array,$selected_val,$need_return = false) {
	$return = '';
	foreach ($option_array as $key=>$value) {
		// if Added by chernov
		if (is_array($selected_val))
			$selected = in_array($key,$selected_val) ? 'selected' : '';
		else
			$selected = ((string)$key == (string)$selected_val) ? 'selected' : ''; 
		$option = '<option ' . $selected . ' value="' . $key . '">' . $value . "</option>\n";
		if ($need_return) $return .= $option;
		else echo $option;
	}
	if ($need_return) return $return;
}

function arrayFromSql2($sql) {
    $r=sql_query($sql);
    $a=array();
    while ($row=sql_numeric_array($r)) {
        // for js processing as numeric field
        $key=(is_numeric($row[0])) ? $row[0]+0 :$row[0];
        $a[$key]=$row[1];
    }
    return $a;	
}	


function scan_dir($dir) {
    $a = array();
    $di = new RecursiveDirectoryIterator($dir);
    foreach (new RecursiveIteratorIterator($di) as $filename => $file) {
        $a[] = $filename;
    }
    return $a;
}

function is_date($date) {
	// date должно быть в формате YYYY-mm-dd
	return preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/",$date);
}

function file_ext($file) {
	return pathinfo($file, PATHINFO_EXTENSION);
}

function unichr($u) {
    return json_decode('"\u'.$u.'"');
    //return mb_convert_encoding('&#' . intval($u) . ';', 'UTF-8', 'HTML-ENTITIES');
}

function fixedUserMessage($message, $type = '') {
	if($message) {
		echo "<div class='fixed-message $type'>";

		echo $message;

		echo "</div>";
	}
}

function checkUserMessage() {
	if(array_key_exists('USER_MESSAGE', $_SESSION)) {
		return json_encode($_SESSION['USER_MESSAGE']);
	}

	return json_encode(false);
}

?>