<?php

function getPrefix() {
	$table_prefix = '_files';
	return $table_prefix;
}

function html_out_attached_files($id, $table, $field = 'filename') {

	$prefix = getPrefix();

	$files_set = sql_rows("SELECT * FROM " . "$table"."$prefix" . " WHERE " . "$table" . "_id=$id");

	$html  = '<h1>-- Не найдено прикрепленных файлов --</h1>';

	if (!empty($files_set)) {
		$html  = '<h1>Прикрепленные файлы</h1>';

		for($i = 0; $i < count($files_set); $i++) {

			$f_id = $files_set[$i]['id'];
			$f_decription = $files_set[$i]['description'];
			$f_name = $files_set[$i]['filename'];
			$f_author = $files_set[$i]['author_id'];
			$f_timestamp = $files_set[$i]['timestamp'];

			$html .= '<div class="attached-file">';

			$html .= '<div class="action-buttons">';

			$html .= '<a href="/download.php?id='.$id.'&table='.$table.'_files&id='.$f_id.'" class="download"><i class="fa fa-download"></i> Скачать</a>';

			if(!is_null($f_decription)) {
				$html .= '<a onclick="MyFn.addFileDescription(this, '.$f_id.', \''.$table.'\', true)" class="description"><i class="fa fa-pencil-square-o"></i> Редактировать описание</a>';
			} else {
				$html .= '<a onclick="MyFn.addFileDescription(this, '.$f_id.', \''.$table.'\', false)" class="description"><i class="fa fa-pencil-square-o"></i> Добавить описание</a>';
			}

			if(is_admin() || $f_author == $_SESSION['user_id'])
				$html .= '<a href="/delete_files.php?id='.$f_id.'&table='.$table.'" onclick="return MyFn.deleteFileConfirmation()" class="delete"><i class="fa fa-trash-o"></i> Удалить</a>';
			
			$html .= '</div>';
			
			$html .= '<span class="s-title">Название: </span><span>'.$f_name.'</span><br />';

			if(!is_null($f_author)) {
				$html .= '<span class="s-title">Загрузил(а): </span><span>'.sql_get_value("fio","user","id=$f_author").'</span><br />';
			}

			$html .= '<span class="s-title">Дата: </span><span>'.$f_timestamp.'</span><br />';

			if(!is_null($f_decription)) 
				$html .= '<span class="s-title">Описание: </span><span class="descript">'.$f_decription.'</span>';

			$html .= '</div>';
		}
	}

	return $html;
}

function html_out_load_form() {
	require $_SERVER['DOCUMENT_ROOT'] . '/template/module_templates/attaching_files/form.php';
}