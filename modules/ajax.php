<?php 
include '../init.php';
$func= sql_escape($_GET['func']);
$arg1 = isset($_GET['arg1']) ? sql_escape($_GET['arg1']) : null;
$arg2 = isset($_GET['arg2']) ? sql_escape($_GET['arg2']) : null;
$arg3 = isset($_GET['arg3']) ? sql_escape($_GET['arg3']) : null;

$functions = array('get_zayav_options', 'get_description', 'add_file_description', 'spec_contragent_name', 'toggle_favorite',
	'get_plan_dohod_options',
	'get_plan_dohod_options_by_period_id',
	'get_stat_pfhd_ver_options_by_period_id');
if ( in_array($func,$functions )) {
		echo $func($arg1,$arg2,$arg3);
}

function get_stat_pfhd_ver_options_by_period_id($plan_period_id=null) {
	$w = '';
	$max = '';
	if (is_numeric($plan_period_id)) {
		$w = " and plan_period_id='$plan_period_id' ";
		$max = sql_get_value("MAX(id)",'stat_pfhd_ver',"true $w");
	}
	$sql = "SELECT id, name FROM stat_pfhd_ver WHERE true $w ";
	$res = sql_query($sql);
	$s = '';
	while ($row=sql_array($res)){
		$selected = ($row['id']==$max) ? 'selected="selected" ' : '';
		$s.='<option '.$selected.'value="'.$row['id'].'">';
		$s.=$row['name'];
		$s.='</option>';
	}
	return $s;
}

function get_plan_dohod_options_by_period_id($plan_period_id=null) {
	$w = '';
	$max = '';
	if (is_numeric($plan_period_id)) {
		$w = " and plan_period_id='$plan_period_id' ";
		$max = sql_get_value("MAX(id)",'plan_dohod_ver',"true $w");
	}
	$sql = "SELECT id, name FROM plan_dohod_ver WHERE true $w ";
	$res = sql_query($sql);
	$s = '';
	while ($row=sql_array($res)){
		$selected = ($row['id']==$max) ? 'selected="selected" ' : '';
		$s.='<option '.$selected.'value="'.$row['id'].'">';
		$s.=$row['name'];
		$s.='</option>';
	}
	return $s;
}

function get_plan_dohod_options($year=null) {
	$w = '';
	$max = '';
	if (is_numeric($year)) {
		$w = " and y='$year'";
		$max = sql_get_value("MAX(id)",'plan_dohod_ver',"y='$year'");
	}
	$sql = "SELECT id, name FROM plan_dohod_ver WHERE true $w ";
	$res = sql_query($sql);
	$s = '';
	while ($row=sql_array($res)){
		$selected = ($row['id']==$max) ? 'selected="selected" ' : '';
		$s.='<option '.$selected.'value="'.$row['id'].'">';
		$s.=$row['name'];
		$s.='</option>';
	}
	return $s;
}

function get_zayav_options($contragent_id=null){
	$w1='true';
	if (!empty($contragent_id)) $w1="contragent_id=$contragent_id";
	$sql="select z.id, z.d, z.contragent_fio, zs.id as zs_id from zayav z left join zayav_status zs on z.zayav_status_id=zs.id where $w1 order by zs_id, d desc";
	$res=sql_query($sql);
	$s='<option value>Нет</option>';
	
	while ($row=sql_array($res)){
		$s.='<option value="'.$row['id'].'">';
		$s.=$row['id'].' от '.$row['d'] . ' ' . $row['contragent_fio'];
		$s.='</option>';
	}

	return $s;
}

function get_description($table,$code) {
	$table = sql_escape($table);
	$code = sql_escape($code);
	echo sql_get_value('descript',"`$table`","code='$code'");
}

function add_file_description($value, $id, $update_table) {
	$prefix = '_files';

	$sql = "UPDATE $update_table". $prefix ." SET description='$value' WHERE id=$id";

	sql_query($sql);

	echo $value;
}

function spec_contragent_name($contragent_id) {
	$show_fio = sql_get_value('show_fio', 'contragent', "id = '$contragent_id'");

	return json_encode($show_fio);
}

function toggle_favorite($table, $row_id, $color_key) {
	$table = sql_escape($table);
	$row_id = sql_escape($row_id);
	$exist = sql_get_value('id', 'favorites', "user_id='${_SESSION['user_id']}' and tab='$table' and row_id='$row_id'");
	if($exist) {
		sql_query("UPDATE favorites SET color_key = '$color_key' WHERE user_id='${_SESSION['user_id']}' and tab='$table' and row_id='$row_id'");
		//sql_query("DELETE FROM favorites WHERE user_id='${_SESSION['user_id']}' and tab='$table' and row_id='$row_id'");
	} else {
		sql_query("INSERT INTO favorites (user_id, tab, row_id, color_key) VALUES ('${_SESSION['user_id']}', '$table', '$row_id', '$color_key')");
	}
	return json_encode(array('success'=>'OK'));
}
?>