<?php
require_once __DIR__ . "/define.php";

// $p = $_POST
function save($table,$p) {
	$save = new SaveData($table);
	return $save->save($p);
}

function restore($table,$p) {
	$save = new SaveData ($table);
    $save->is_restore=true;
	return $save->save($p);
}
/*	USES:
	Config
	Config::view_table
	Config::after_update
	functions:
	table_columns

*/	
Class SaveData {
	public static $source_tname;
	public static $source_tid;
    public  $is_restore=false;
	private $table;
	private $key_name;
	private $fields;
	private $row;
    private $is_insert=false;
    private $last_id;
	private $model = null;
	

	function __construct($table) {
		// при редактировании View сохраняем запись в таблицу
		if (Config::view_table($table))
			$table=Config::view_table($table);
		$this->table=$table;
		$m=sql_table_columns($table);
		$this->key_name=sql_table_key($table);
        if (empty($this->key_name) || is_array($this->key_name)) {
            die("Bad primary key in table $table");
        }
		$this->fields=$m;
		$this->findModel($table);
	}

	function findModel($modelName) {
		$modelName = ucfirst($modelName);
		$className = $modelName.'Model';
		if(class_exists($className) && $className instanceof BaseModel) {
			$this->model = new $className;
		}
	}


	/* protected??? */
	private function _testBeforeSave($row) {
		if ($this->table == 'dogovor') {
			if (isset($row['id']) && isset($row['dogovor_status_id'])) {
				if (count($row)>2) {
					throw new Exception ('При изменении статуса нельзя одновременно обновлять другие поля');
				}
			}
		} elseif ($this->table == 'dogovor_sostav') {
			if (isset($row['dogovor_id'])) {
				$dogovor_id = (int) $row['dogovor_id'];
			}	elseif (isset($row['id'])) {
				$dogovor_id = sql_get_value('dogovor_id','dogovor_sostav','id='.(int)$row['id']);
			} else {
				throw new Exception('Не указан ни id строки, ни dogovor_id!');
			}
			$dogovor_status_id = sql_get_value("dogovor_status_id","dogovor","id=$dogovor_id");
			if ($dogovor_status_id != DOGOVOR_STATUS_TYPE_EDIT) {
				throw new Exception ('Статус договора не позволяет его редактировать! ' );
			}
			$use_limit = sql_get_value('use_limit', 'dogovor_status', "id='$dogovor_status_id'");
			// проверим лимиты
			if ($use_limit == 1 && !dogovorCheckLimit($dogovor_id)) {
				$errMesage = SystemMessage::get();
				throw new Exception ($errMesage);
			}
		}
	}
	/* p -array POST */	
	function save($p) {
		if (isset($p[$this->key_name]) && is_array($p[$this->key_name])) 
            die('cannot save array');

		if (!user_may_edit_table($this->table)) {
			throw new Exception ('У вас недостаточно прав для сохранения записи');
		}
		
		
		
		if(!is_null($this->model)) {
			$this->model->beforeSave($p);
		}
		$this->_testBeforeSave($p);// кинет оишбку если что
	
        // возможно перекроет при Exec на last_id
        $this->last_id=isset($p[$this->key_name]) ? $p[$this->key_name] : null;
		$this->fill_row($p);
		
		
        $this->exec();

		$this->after_save();

       	if (!(strstr($this->table,'temp'))) {
            $op_type = $this->is_restore ? 4 : ($this->is_insert ? 1 : 2);
            static::save_log($_SESSION['user_id'],$this->table,$op_type,$this->last_id);
        }

        return $this->last_id;
	}
	
   
	
	private function after_save() {
	
		// Проверка для smeta Обновляем zfo_smeta_limit
		if ($this->table == 'smeta') {
			// Обновляем zfo_smeta_limit
			$smeta_status_id = sql_get_value('smeta_status_id','smeta','id='.$this->last_id);
			define('SMETA_SATUS_APPROVED',3);
			if ($smeta_status_id == SMETA_SATUS_APPROVED) {
				$sql = "DELETE FROM zfo_smeta_limit where smeta_id=".$this->last_id;
				sql_execute($sql); 
				$sql = "INSERT INTO zfo_smeta_limit(smeta_id,y,zfo_id,fin_source_id,kvr,amount)
						SELECT ss.smeta_id,s.fp_year_id,ifnull(ss.zfo_id,s.zfo_id) zfo_id,ss.fin_source_id,a.kvr,sum(amount) amount
						FROM smeta_sostav ss 
						INNER JOIN smeta s ON ss.smeta_id = s.id
						LEFT JOIN fp_article a ON ss.fp_article_id = a.id
						WHERE s.fp_year_id >= 2019 AND ss.smeta_id = ".$this->last_id."
						GROUP BY s.fp_year_id,ifnull(ss.zfo_id,s.zfo_id),ss.fin_source_id,a.kvr";
				sql_execute($sql);
			}
		} else if ($this->table == 'dogovor_files' && user_role() == USER_ROLE_KU) {
			$dogovor_id = sql_get_value('dogovor_id','dogovor_files','id='.$this->last_id);
			$dogovor_status_id = sql_get_value('dogovor_status_id','dogovor','id='.$dogovor_id);
			if ($dogovor_status_id == 9) { 
				//Заключен делаем рассылку
				// Просьба Селезневой А.В.
				$from_email = 'noreply@unecon.ru';
				$subject = 'Загружен файл для заключенного договора';				
				$emails = sql_to_assoc("select id,email from user WHERE is_actual=1 AND email is not null AND may_receive_mail=1 AND (role_id=".USER_ROLE_FEU_OUU." OR email='Kovaleva.m@unecon.ru' OR email='alex_tche@mail.ru' )");
				$email_text = "Загружен новый файл для договора <a href='https://reestr2015.unecon.ru/dogovor.php?id=%ID%'>ID:%ID%</a> в статусе Заключен";
				$email_text = str_replace('%ID%',$dogovor_id,$email_text);
				foreach($emails as $email) {
					send_mail($email, $subject,$email_text, $from_email);
				}
			}
		}
		
		foreach (Config::after_update($this->table) as $query) {
			sql_query($query);
		}
	}
	
	private function fill_row($vals) {
		$this->row=array();
		foreach ($this->fields as $c) {
			$column_name=$c['name'];
		//	if (array_key_exists($column_name,$vals)) {
			if (isset($vals[$column_name])) {
				$val = $vals[$column_name];
				if ($val === false) {
					$val = 0;
				} 
				// sql_escape(true) === "1"
				$val = sql_escape($val);
				if ($val == '' || $val==TD::$option_none) {
					$val='null';
				} else {
					if ($c['type']=='date' &&  preg_match('/[0-9]{2}\.[0-9]{2}\.[0-9]{4}/',$val) ) {
						$val=sql_rus2date($val);
					} elseif ($c['type']=='double' || $c['type']=='decimal') {
						$val='"'.str_replace(',','.',$val).'"';
					} elseif ($c['type']=='binary') {
						if (!preg_match('/^[0-9a-f]*$/',$val)) {
							$val = 'null';
						} else {
							$val='0x'.$val;
						}
					} else {
						$val='"' . $val . '"';						
					}
				}
				//$column_name='`'.$column_name.'`';
				$this->row[$column_name] = $val;
			}
		}
		//var_dump($this->row);
	}
	
	private function exec() {
		//$keyname='`'.$this->key_name.'`';
		$keyname=$this->key_name;
			
		if (!isset($this->row[$keyname]) || $this->row[$keyname]=='null') {
			$this->is_insert=true;
		} else {
			$this->is_insert = (0 == sql_get_value('count(*)', $this->table,"`$keyname`=".$this->row[$keyname]));
		}

        if ($this->is_insert) {
			$flds = array_map([$this,'escape_sql_column_name'],array_keys($this->row));
			$sql = 'INSERT INTO `'.$this->table.'` ('.implode(', ',$flds).') VALUES ('.implode(', ',$this->row).')';
		} else {
			$updarr=array();
			foreach ($this->row as $updkey=>$updval) {
				$updarr[$updkey]="`$updkey` = $updval";
			}
			unset ($updarr[$keyname]);
			$sql = 'UPDATE `' . $this->table . '` SET '.implode (', ',$updarr).' WHERE '."`$keyname`".'='.$this->row[$keyname];
		}
		$result = sql_execute($sql);
//		echo sql_error();

		if (!$result && defined('SQL_NO_TRIGGER_ERRROR') && SQL_NO_TRIGGER_ERRROR) {
//			echo sql_error();
//			die("error!");
			throw new Exception('Ошибка: ' . sql_error());
		}
        $this->last_id = $this->is_insert ? sql_last_id() : $this->last_id;
	}
	
	
	// возарщает i-й срез POSTa
	public static function split_post($a,$i) {
		$v=array();
		foreach($p as $key=>$val) {
			if  (is_array($val) && $i<count($val)){
				$v[$key]=$val[$i];
			}
		}
		return $v;
	}

	//Проверяем наличие таблицы с логами, если не существует, то создаем
	public static function check_log_table($table) {
        /*if(count(sql_array(sql_query("SHOW TABLES LIKE 'log_$table'"))) == 0) {
        	$tableStructure = sql_table_columns($table);

        	foreach($tableStructure as $key => $item) {

        	}
        }

		$sql_create_log_table = "CREATE TABLE log_$table LIKE $table";

        	sql_query($sql_create_log_table);

        	//Узнаем PRIMARY_KEY у созданного лога
        	$primary_key = sql_table_key('log_' . $table);

        	$sql_alter_log_table   =   "ALTER TABLE log_$table 
        								MODIFY $primary_key INT,
        								DROP PRIMARY KEY,
										ADD COLUMN `user_id` int(11) NOT NULL,
										ADD COLUMN `operation_type` int(11) NOT NULL,
										ADD COLUMN `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP";

			sql_query($sql_alter_log_table);*/
	}
    
    public static function save_log($user,$table,$op_type,$id) {
    	$id = (int)$id;
    	$key_name = sql_table_key($table);
		
/*
        $cols=sql_table_columns($table);        
        foreach ($cols as $columns){
            $colnames[]='`'.$columns['name'].'`';
        }
*/        
        $sql="select * from $table where `" . $key_name."`=".$id."";
        $row=sql_array(sql_query($sql));
        $row['user_id'] = $user;
        $row['operation_type'] = $op_type;
        static::add_log_row_sys_params_info($table, $row);

        foreach ($row as &$s) { 
        	$s=sql_escape($s);
        }
		$cols=array();
		foreach($row as $key=>$dummy) {
			$cols[] = '`' . $key . '`';
		}
        $colsstr=implode(', ',$cols);
        $valstr='"'.implode('", "', $row).'"';

		//AVD
		if (!sql_table_exists('log_' . $table)) return;
		
       	$sql1="insert into log_$table ($colsstr) values ($valstr)";
        return sql_execute($sql1);
    }
    
    protected static function add_log_row_sys_params_info($table,&$row) {
		
		//AVD
		if (!sql_table_exists('log_' . $table)) return;
		
    	$log_columns = sql_table_columns('log_'.$table);
		
    	$sys_params = array('source_tname', 'source_tid');
    	foreach ($sys_params as $param) {
    		if (isset($log_columns[$param])) {
    			$val = static::$$param;
    			if (!empty($val)) $row[$param] = $val;
    		}
    	}
   	}
	
	private function escape_sql_column_name($fld) {
		return "`$fld`";
	}
    
}

//operation type 1=add 2=edit 3=delete
