<?php
class C1 {
	private static function booker_fin_sources_by_KVD($KVD) {
		$booker_fin_ss = [];
		$rows = sql_rows("select code from booker_fin_source where КВД_Enum=".'0x'.$KVD);
		foreach($rows as $row) {
			$booker_fin_ss[]=$row['code'];
		}
		return $booker_fin_ss;
	}
	
	public static function денежные_обязательства($KVD,$y) {
		return self::_обязательства_oborot(1,$KVD,$y);
	}
	
	public static function обязательства($KVD,$y) {
		//var_dump(self::_обязательства_oborot(0,$KVD));
		$rows = self::_обязательства_oborot(0,$KVD,$y);
		return $rows;
	}
	
	private static function _обязательства_tmp_оборот($tmp_name,$ACC,$KVD,$PERIOD_START,$PERIOD_END) {
	
		$query = OneC::select(['Д.Ссылка','Дог_Ссылка'],'Оборот.КБК',['sum("Оборот.Сумма")', 'Сумма_1С'])
			->into($tmp_name)
			->from(['РегистрБухгалтерии.ЕПСБУ', 'Оборот'])
			->oborot_credit($ACC,$PERIOD_START,$PERIOD_END,['КЭК','Принятые обязательства','Разделы Лицевых Счетов'])
/*			->join('Справочник.КОСГУ','LEFT')->
				on(OneC::cast('Оборот.Субконто1','Справочник.КОСГУ'),'=','КОСГУ.Ссылка')
			->join(['Справочник.РазделыЛицевыхСчетов', 'Разделы'],'LEFT')->
				on(OneC::cast('Оборот.Субконто3','Справочник.РазделыЛицевыхСчетов'),'=','Разделы.Ссылка')*/
			->join(['Справочник.Договоры', 'Д'],'LEFT')->
				on(OneC::cast('Оборот.Субконто2','Справочник.Договоры'),'=','Д.Ссылка')
			;

		if ($KVD) {
			$query->where('Оборот.КВД','=', '0x'.$KVD);
		}
		$query->group_by('Д.Ссылка','Оборот.КБК');
		$query->execute();
	}
	
	private static function _обязательства_oborot($is_money,$KVD,$CURRENT_YEAR) {
		$booker_fin_ss = []; //
		if ($KVD) {
			$booker_fin_ss = self::booker_fin_sources_by_KVD($KVD);
		}
			
		$PERIOD_START =  "$CURRENT_YEAR-01-01";
		$PERIOD_END =  "$CURRENT_YEAR-12-31";
		
		$rows = [];
		$ACC = $is_money ? '502.12' : '502.11';

		if ($is_money) {
			self::_обязательства_tmp_оборот('#ОБОРОТ','502.12',$KVD,$PERIOD_START,$PERIOD_END);
		} else {
			self::_обязательства_tmp_оборот('#ОБОРОТ',['502.11','502.21','502.31'],$KVD,$PERIOD_START,$PERIOD_END);
			self::_обязательства_tmp_оборот('#ОБОРОТ_1','502.11',$KVD,$PERIOD_START,$PERIOD_END);
			self::_обязательства_tmp_оборот('#ОБОРОТ_2','502.21',$KVD,$PERIOD_START,$PERIOD_END);
		}
		$query = OneC::select(['Д.Ссылка','Дог_Ссылка'],['Д.Наименование','Договор'],['К.Наименование', 'Контрагент'],
			'К.Фамилия','К.Имя','К.ИНН',['КБК.Наименование','КВР'],'#ОБОРОТ.Сумма_1С')
			->from('#ОБОРОТ')
			->join(['Справочник.Договоры', 'Д'],'LEFT')->
				on('#ОБОРОТ.Дог_Ссылка','=','Д.Ссылка')
			->join(['Справочник.КБК', 'КБК'],'LEFT')->
				on('#ОБОРОТ.КБК','=','КБК.Ссылка')
			->join(['Справочник.Контрагенты', 'К'],'LEFT')->
				on('Д.Контрагент','=','К.Ссылка');
		if (!$is_money) {
			$query->select(['#ОБОРОТ_1.Сумма_1С', 'Сумма_1С_Год0'],['#ОБОРОТ_2.Сумма_1С', 'Сумма_1С_Год1'])
				->join('#ОБОРОТ_1','LEFT')->
					on('#ОБОРОТ.Дог_Ссылка','=','#ОБОРОТ_1.Дог_Ссылка')->
					on('#ОБОРОТ.КБК','=','#ОБОРОТ_1.КБК')
				->join('#ОБОРОТ_2','LEFT')->
					on('#ОБОРОТ.Дог_Ссылка','=','#ОБОРОТ_2.Дог_Ссылка')->
					on('#ОБОРОТ.КБК','=','#ОБОРОТ_2.КБК');
		}

/*				
		if ($is_money) {
			$query = OneC::select('*')->from('#ОБОРОТ');
		} else {
			
		}
*/
		$res = $query->execute();
		
		$sql_w = '';
		if ($KVD) {
			if ($booker_fin_ss) {
				$sql_w = " and booker_fin_source in (" . implode(",",$booker_fin_ss) . ")";
			} else { //на случай для КВД не заполнен ИФ
				$sql_w = ' and false ';
			}
		}
		if ($_SERVER['REMOTE_ADDR'] == '10.123.124.215') {
			for ($i = 0; $i<2; $i++) {
				//echo '\'' . mssql_field_name($res, $i) . '\' типа ' . 	strtoupper(mssql_field_type($res, $i)) . 				'(' . mssql_field_length($res, $i) . ')<br>';				
			}
		}		
		while ($row =  mssql_fetch_array($res,MSSQL_ASSOC)) {		
			$row['КВР'] = $kvr = substr($row['КВР'],-3);
			$dogovor_sum_y1 = $dogovor_sum_y0 = $dogovor_sum = $sum_plat = $sum_zayav = 0;
//			if ($row['Дог_Ссылка']) {
			$ref= '0x'.bin2hex($row['Дог_Ссылка']);
			$dogovor_ids = self::dogovor_ids_by_1c_Ref($ref);
			$dogovor_in_sql =  implode(",",$dogovor_ids); 
/*			if ($_SERVER['REMOTE_ADDR'] == '10.123.124.215') {
				if ($row['Об_Дог_Ссылка']!==$row['Дог_Ссылка']) {
					
					echo strlen($row['Об_Дог_Ссылка']) . ' ' . bin2hex($row['Об_Дог_Ссылка']) .' '. strlen($row['Дог_Ссылка']). ' ' .	bin2hex($row['Дог_Ссылка']) . '<br>';
				}
				if ($row['Договор'] == 'Государственный контракт 472/16 от 23.09.2016') {
				
				}
			}
*/			
			if ($dogovor_ids) {
				if ($is_money) {
					$sum_plat = sql_fetch_value("select sum(plat.s) as plat_s from plat inner join zayav on plat.zayav_id=zayav.id where 
						zayav.dogovor_id in ($dogovor_in_sql) and plat.d>='$PERIOD_START' and plat.d<='$PERIOD_END' and plat.kvr='$kvr'" . $sql_w);
					$sum_zayav = sql_fetch_value("select sum(zs.sum)  
							from zayav_sostav zs 
							inner join zayav z on zs.zayav_id = z.id 
							inner join fp_article fpa on zs.fp_article_id = fpa.id
							inner join fin_source fs on zs.fin_source_id = fs.id 
							inner join zayav_status on z.zayav_status_id = zayav_status.id 							
						where z.dogovor_id  in ($dogovor_in_sql) and z.d>='$PERIOD_START' and z.d<='$PERIOD_END' and 
							  zayav_status.is_good = 1 and zayav_status.id not in (1,2,11) and 
							  fpa.kvr='$kvr'" . $sql_w);	
				}	else {
					$dogovor_sum =  sql_fetch_value("select sum(amount) from dogovor_sostav ds
						left join fin_source fs on ds.fin_source_id = fs.id 
						inner join fp_article fa on ds.fp_article_id = fa.id
						WHERE dogovor_id  in ($dogovor_in_sql)  and fa.kvr='$kvr'" . $sql_w);
					$plat_before = sql_fetch_value("select sum(plat.s) as plat_s from plat inner join zayav on plat.zayav_id=zayav.id where 
					zayav.dogovor_id  in ($dogovor_in_sql)  and plat.d<'$PERIOD_START' and plat.kvr='$kvr'" . $sql_w);
					$plat_before +=0;
					$dogovor_sum -=  $plat_before;					
					$dogovor_sum_y0 =  sql_fetch_value("select sum(amount) from dogovor_sostav ds
						left join fin_source fs on ds.fin_source_id = fs.id 
						inner join fp_article fa on ds.fp_article_id = fa.id
						inner join period on ds.period_id = period.id 
						WHERE dogovor_id in ($dogovor_in_sql) and period.year = $CURRENT_YEAR and fa.kvr='$kvr'" . $sql_w);
					$dogovor_sum_y1 =  sql_fetch_value("select sum(amount) from dogovor_sostav ds
						left join fin_source fs on ds.fin_source_id = fs.id 
						inner join fp_article fa on ds.fp_article_id = fa.id
						inner join period on ds.period_id = period.id 
						WHERE dogovor_id in ($dogovor_in_sql) and period.year = $CURRENT_YEAR+1 and fa.kvr='$kvr'" . $sql_w);
						
				}
			}
			// Кредит  возвращает отрицательную сумму - пПЕДЕЛЕАТЬ (?)
			$row['Сумма_1С'] = -$row['Сумма_1С'];  
			if (!$is_money) {
				$row['Сумма_1С_Год0'] = -$row['Сумма_1С_Год0'];  // Кредит  возвращает отрицательную
				$row['Сумма_1С_Год1'] = -$row['Сумма_1С_Год1'];  // Кредит  возвращает отрицательную сумму 
			}
			
			if ($is_money) {				
				$row['СУП_Заявки'] = +$sum_zayav;
				$row['СУП_Платежки'] = +$sum_plat;
				$row['Разница'] = $row['Сумма_1С'] - $row['СУП_Заявки'];
			} else {
				$row['Сумма_СУП'] = +$dogovor_sum;
				$row['Разница'] = round($row['Сумма_1С'] - $row['Сумма_СУП'] ,2);
				$row['Сумма_СУП_Год0'] = +$dogovor_sum_y0;
				$row['Разница_Год0'] = round($row['Сумма_1С_Год0'] - $row['Сумма_СУП_Год0'],2);
				$row['Сумма_СУП_Год1'] = +$dogovor_sum_y1;
				$row['Разница_Год1'] = round($row['Сумма_1С_Год1'] - $row['Сумма_СУП_Год1'],2);
			}
			unset($row['Дог_Ссылка']);
			$hrefs = [];
			foreach ($dogovor_ids as $dog_id) {
				$hrefs[] = "<a target='_blank' href='/dogovor.php?id=$dog_id&page=dogovor_sostav'>$dog_id</a>";
			}
			$row['dog_ids'] = $hrefs ? implode(", ",$hrefs) : null;
			$rows[] = $row;
		}
		//var_dump($rows);
		return $rows;
		
	}
	
	private static function dogovor_ids_by_1c_Ref($ref) {
		$ids = [];
		$rows = sql_rows("select d1c.dogovor_id from 
		dogovor_link_1c d1c join dogovor d on d1c.dogovor_id = d.id 
		join dogovor_status status on d.dogovor_status_id = status.id 
		where Договоры_Ref=$ref and status.use_stat_738 = 1 		
		order by d1c.dogovor_id");
		foreach ($rows as $row) {
			$ids[] = $row['dogovor_id'];
		}
		return $ids;
	}
}