<?php 
define ('PFHD_PUBLIC_VOLUME','00086'); //объем публинчых обязательств
// средства во временном распоряжении

class Pfhd {
	const SQL_PATH =  '/asu_pfhd/sql/';
	const CURRENT_VERSION_2016 = -2016; 
	const CURRENT_VERSION_2017 = -2017; 
	const CURRENT_VERSION = -1; // 2018 и послед годы 
	
	const PFHD_ARTILCE_REMAINS_START_ID = 92;
	const TMP_PFHD_TABLE = 'tmp_asu_pfhd';
	static $pfhd_article_y = null;
	static $useCalcTables = false; // Формировать xml c обоснованиями расчетов


	// $n_version null - нет сравнения версия, 1 -первая 2 -вторая
	private static function _pfhd_sql($ver_id, $n_version=null, $rashod_is_minus=false) {
		$sql_file = $_SERVER['DOCUMENT_ROOT'] . self::SQL_PATH ;
		$sql_file .= (self::$useCalcTables) ?  '/pfhd_use_delta_2018.sql' : '/pfhd_2018.sql';
		
		$pfhd_y = sql_get_value('plan_period_id','pfhd_ver',"id=$ver_id");
		$pfhd_article_remains_id = sql_get_value("id","pfhd_article","y=$pfhd_y and is_remains_start=1");

		$replace = ['{ver_id}'=>$ver_id,'{pfhd_article_remains_id}'=>$pfhd_article_remains_id];
		
		
/*


		if ($n_version==1) {
			$replace['{sign_sum} as sum']= '{sign_sum} as sum_1, CAST(0 AS DECIMAL(19,2)) as sum_2,{sign_sum} as sum_diff';
		} else if ($n_version==2) {
			$replace['{sign_sum} as sum']= 'CAST(0 AS DECIMAL(19,2)) as sum_1,{sign_sum} as sum_2,-{sign_sum} as sum_diff';
		}
*/		
		if(is_file($sql_file)) {
			$sql = file_get_contents($sql_file);
			$sql = str_replace(array_keys($replace), array_values($replace), $sql);
            if ($rashod_is_minus) {
                $sql = str_replace('{sign_sum}','if(pfhd_article.sign<0 or pfhd_article.is_rashod,-sum,sum)',$sql);
            } else {
                $sql = str_replace('{sign_sum}','if(pfhd_article.sign<0,-sum,sum)',$sql);
            }
		} else {
			//Выбрасываем исключение, если вдруг sql файл не найден или переименован
			throw new Exception("Невозможно подключить файл");
		}
		return $sql;
		
	}
	// $n_version null - нет сравнения версия, 1 -первая 2 -вторая
	private static function _pfhd_sql_2017($ver_id,$n_version=null,$rashod_is_minus=false) {
		$sql_file = $_SERVER['DOCUMENT_ROOT'] . self::SQL_PATH . '/pfhd_2017.sql';
		
		// pfhd_article_fld для доходов
		$plan_period_row = self::_plan_period_row_2017($ver_id);
		$pfhd_article_fld = 'pfhd_article_'.$plan_period_row['fld_suffix'].'_id';
		
		$replace = array(
			'{plan_dohod_ver_id}'=>self::_sql_pfhd_dohod_ver_2017($ver_id)
			,'{sql_rashod}'=>self::_sql_pfhd_rashod_ver_2017($ver_id)
			,'{pfhd_article_fld}'=>$pfhd_article_fld
			,'{sql_remains}'=>self::_sql_pfhd_remains_ver_2017($ver_id)
		);
		if ($n_version==1) {
			$replace['{sign_sum} as sum']= '{sign_sum} as sum_1, CAST(0 AS DECIMAL(19,2)) as sum_2,{sign_sum} as sum_diff';
		} else if ($n_version==2) {
			$replace['{sign_sum} as sum']= 'CAST(0 AS DECIMAL(19,2)) as sum_1,{sign_sum} as sum_2,-{sign_sum} as sum_diff';
		}
		
		if(is_file($sql_file)) {		
			$sql = file_get_contents($sql_file);
			$sql = str_replace(array_keys($replace), array_values($replace), $sql);
            if ($rashod_is_minus) {
                $sql = str_replace('{sign_sum}','if(pfhd_article.sign<0 or pfhd_article.is_rashod,-sum,sum)',$sql);
            } else {
                $sql = str_replace('{sign_sum}','if(pfhd_article.sign<0,-sum,sum)',$sql);
            }
		} else {
			//Выбрасываем исключение, если вдруг sql файл не найден или переименован
			throw new Exception("Невозможно подключить файл");
		}
		return $sql;
		
	}
	
	private static function _is_current_version($ver_id) {
		return ($ver_id == self::CURRENT_VERSION_2016 || $ver_id == self::CURRENT_VERSION_2017 || $ver_id == self::CURRENT_VERSION);
		
	}
	
	private static function _plan_period_row_2017($ver_id) {
		if ($ver_id == self::CURRENT_VERSION_2016) {
			$plan_period_id = 2016;
		} else if ($ver_id == self::CURRENT_VERSION_2017) {
			$plan_period_id = 2017;
		} else {
			$plan_period_id = sql_get_value("plan_period_id","stat_pfhd_ver","id=$ver_id");
		}
		return sql_get_array("plan_period","id=$plan_period_id");
	}



	private static function _sql_pfhd_remains_ver_2017($ver_id) {
		$plan_period_row = self::_plan_period_row_2017($ver_id);
		$y = $plan_period_row['y_from'];
		$pfhd_article_y = $plan_period_row['fld_suffix'];
		$pfhd_article_remains_id = sql_get_value("id","pfhd_article","y=$pfhd_article_y and is_remains_start=1");
		
		if ($y ==2016) {
			$from = "ls_remains where y=$y";				
		} else {
			$from = "tmp_pfhd_remains";
		}
		$sql = "select y pfhd_y,null fp_article_id,$pfhd_article_remains_id pfhd_article_id,null kvr,booker_fin_source,null zfo_id,null as dogovor_id,y as fp_year,null plan_grafic_id,sum
			from $from ";
		return $sql;
	}

	
	private static function _sql_pfhd_dohod_ver_2017($ver_id) {
		if (self::_is_current_version($ver_id)) {
			$plan_period_id = self::_plan_period_row_2017($ver_id)['id'];
			$v = sql_get_value("MAX(id)","plan_dohod_ver","plan_period_id=$plan_period_id");
		} else {
			$v = sql_get_value("plan_dohod_ver_id", "stat_pfhd_ver","id = '$ver_id'");
		}
		return $v;
	}

	private static function _sql_pfhd_rashod_ver_2017($ver_id) {
		if (self::_is_current_version($ver_id)) {
			// Было для текущей 2016 
			//if ($ver_id == self::CURRENT_VERSION_2016)
			//$sql = ' v_pfhd_2016 v';
			$plan_period_id = self::_plan_period_row_2017($ver_id)['id'];
			$sql = "(" . ReestrDB::pfhd_rashod_sql($plan_period_id) . ") v";
		} else {
			$sql =  " v_stat_pfhd v where v.stat_pfhd_ver_id = '$ver_id'";
		}
		return $sql;
	}
	
	private static function _fill_amounts($rows) {
		$amounts = [];
		foreach($rows as $row) {
			$amounts[$row['pfhd_article_id']] = $row['sum'];
		}
		return $amounts;
	}

	private static function _set_has_childs(&$rows) {
		$parent_ids = [];
		foreach ($rows as $r) {
			if ($p_id = $r['parent_id']) {
				$parent_ids[$p_id] = 1;
			}
		}
		foreach ($rows as &$r) {
			$r['has_childs'] = isset($parent_ids[$r['id']]);
		}
	}

	private static function _fill_sum($pfhd_y,$amounts,$fin_source) {
		$USER_CHERNOV  = 12;

		$article_rows = sql_rows("SELECT * FROM pfhd_article WHERE y=$pfhd_y order by ord"); 
		self::_set_has_childs($article_rows);
		$codeSums = [];
		foreach ($article_rows as &$r) {
			$id = $r['id'];
			$s  = isset($amounts[$id]) ? $amounts[$id] : 0;
			if ($r['has_childs']) {
				$s +=self::_sum_childs($id,$article_rows, $amounts);
			}
			$codeSums[$r['code_asu_pfhd']] = $r['sum'] = $s;
			$r['fin_source'] = $fin_source;
		}
		
		foreach ($article_rows as &$r) {
			if ($formula=$r['formula']) {
				$formula_rus = iconv('utf-8','cp1251',$formula);
				$short_name_regex =  iconv('utf-8','cp1251','(?P<name>[а-яА-ЯA-z0-9_]+)');
				if (preg_match_all('/\['.$short_name_regex.'\]/',$formula_rus,$matches)) {
					foreach ($matches['name'] as $name_rus) {
						$name = iconv('cp1251','utf-8',$name_rus);
						//echo $name;						
						if (isset($codeSums[$name])) {
							$formula = str_replace('['.$name.']','('.$codeSums[$name].')',$formula);
						} else {
							die ("Unknown name $name in fomula");
						}
					}
					$sum = 0;
					eval ('$sum =' . substr($formula,1).';');
					if (current_user_id()==$USER_CHERNOV) {
		//					echo $r['code_asu_pfhd'] . ' ' . $sum .'=' . $formula . " " . "<br>";		
					}
				
					$codeSums[$r['code_asu_pfhd']] = $r['sum'] = $sum;
				}
			}
		}
		
		return  $article_rows;
	}


	private static function _save_to_table($rows) {
		foreach($rows as $row) {
			if ($row['sum'] && $row['code_asu_pfhd']) {
				sql_query("insert into tmp_asu_pfhd(code_asu_pfhd,booker_fin_source,sum) 
				values('${row['code_asu_pfhd']}','${row['fin_source']}','${row['sum']}')");
			}
		}
	}

	private static function _sum_childs($parent_id,$articles,$amounts) {
		$sum = 0;
		foreach ($articles as $article) {
			$id = $article['id'];
			if ($article['parent_id'] == $parent_id) {
				if (isset($amounts[$id])) {
					$sum += $amounts[$id];					
				}
				$sum += self::_sum_childs($id,$articles,$amounts);
			}
		}
		return $sum;
	}	
	
	private static function _get_org_rekvizits() {
		$org = array();
		$rows = sql_rows("select * from param");
		foreach ($rows as $row) {
			$org[$row['name']]=$row['value'];
		}
		return $org;
	}


	public static function  getGUID(){
        mt_srand((double)microtime()*10000);//optional for php 4.2.0 and up.
        $charid = strtoupper(md5(uniqid(rand(), true)));
        $hyphen = chr(45);// "-"
        $uuid = //chr(123)// "{"
             substr($charid, 0, 8).$hyphen
            .substr($charid, 8, 4).$hyphen
            .substr($charid,12, 4).$hyphen
            .substr($charid,16, 4).$hyphen
            .substr($charid,20,12);
            //.chr(125);// "}"
        return $uuid;
	}
	
	public static function getPfhdRows($pfhd_article_y) {
	
		$sql = "select  name,code_asu_pfhd from pfhd_article 
		where code_asu_pfhd is not null and y=$pfhd_article_y order by ord ";
		
		$rows = sql_rows($sql);

		$fin_sources = array(
			'1'=>'Unknown1',
			'4'=>'GosSubsidy',
			'5'=>'TargetSubsidy',
			'2'=>'IncomeActivityValue',
			'3'=>'Unknown3',
		);

		foreach($rows as &$row) {
			$row_sum = 0;
			$code  = $row['code_asu_pfhd'];
			if ($code == PFHD_PUBLIC_VOLUME) {
				// Для публичных обязательств передаетсся только сумма по 1-му источнику в поле Sum
				foreach ($fin_sources as $b_id=>$tagName) {					
					$row[$tagName]= 0;
				}
				$row['Sum'] = sql_get_value('sum', self::TMP_PFHD_TABLE ,"code_asu_pfhd='$code' and booker_fin_source = '1'");
			} else {
				foreach ($fin_sources as $b_id=>$tagName) {					
					$sum = sql_get_value('sum', self::TMP_PFHD_TABLE ,"code_asu_pfhd='$code' and booker_fin_source = '$b_id'");
					$row_sum += $row[$tagName] = (($sum) ? $sum : 0);
				}
				$row['Sum'] = $row_sum;
			}
		}
		
		return $rows;
	}
	
	public static function toHTML($ver_id, $rows) {
		if(!count($rows)) return false;
		$sum_pattern = '(\d+\.\d{2})';
		$html = "";
		$cols = (isset($rows[0])) ? array_keys($rows[0]) : array();
		$html .= "<table border='1'>";
		$html .= "<tr>";
		for($i = 0; $i < count($cols); $i++) {
			$html .= "<th>" . $cols[$i] . "</th>";
		}
		$html .= "</tr>";
		foreach($rows as $row) {
			$html .= "<tr>";
			foreach($row as $col_value) {
				if(preg_match($sum_pattern, $col_value)) $col_value = str_replace(".", ",", $col_value);
				$html .= "<td>" . $col_value . "</td>";
			}
			$html .= "</tr>";
		}
		$html .= "</table>";
		return $html;
	}
	
	public static function extraToXML($ver_id,$download=false) {
		$year = date('Y');
		$rows = self::getExtraRows();
		$org = self::_get_org_rekvizits();
		$xml_path = $_SERVER['DOCUMENT_ROOT'] . '/asu_pfhd/extra';
		ob_start();
		require ($xml_path . '/extra_header.xml');	
		foreach ($rows as $row) {
			require ($xml_path . '/extra_body.xml');	
		}	
		require ($xml_path . '/extra_footer.xml');	
		$xml = ob_get_clean();		
		if ($download) {
			header('Content-Type: text/xml');
			header('Content-Disposition: attachment;filename="Extra.xml"');
			header('Cache-Control: max-age=0');
			echo $xml;
		}
		return $xml;
	}
	
	public static function getExtraRows() {
		$sql = "SELECT ex.* 
		FROM asu_pfhd_dop_pokaz ex 
		ORDER BY ex.id";
		$rows = sql_rows($sql);
		return $rows;
	}
	
	// Перименовать toXml2016
	public static function toXml2016($ver_id,$download=false) {
		$year = 2016;//date('Y');
		self::_fillTmpPfhdTable(2016,$ver_id,$year);
		
		$rows = self::getPfhdRows($year);
		$org = self::_get_org_rekvizits();
		$xml_path = $_SERVER['DOCUMENT_ROOT'] . '/asu_pfhd/pfhd';
		ob_start();
		require ($xml_path . '/pfhd_header.xml');	
		foreach ($rows as $fld) {
			require ($xml_path . '/pfhd_body.xml');	
		}	
		require ($xml_path . '/pfhd_footer.xml');	
		$xml = ob_get_clean();		
		if ($download) {
			header('Content-Type: text/xml');
			header('Content-Disposition: attachment;filename="ПФХД.xml"');
			header('Cache-Control: max-age=0');
			echo $xml;
		}
		return $xml;
	}
	
	private static function pfhd_years_array($plan_period_id) {
		if ($plan_period_id == -1) {
			$years = [2017,2018,2019]	; // 
		} else {
			$row = sql_get_array("plan_period","id=$plan_period_id");
			$years = [];
			for ($y=$row['y_from'];$y<=$row['y_to'];$y++) {
				$years[] = $y;
			}
		}
		return $years;
	}

	private static function _fillYearRemains($pfhd_y, $ver_id) {	
		$years = self::pfhd_years_array($pfhd_y);
//		$pfhd_y = $years[0]; // первый год ПФХД
		// расчет для первого года
		sql_query ("create temporary table tmp_pfhd_remains(y int,booker_fin_source int, sum decimal(20,2)) engine=memory");
		sql_query ("insert into tmp_pfhd_remains(y,booker_fin_source,sum) select y,booker_fin_source,sum from ls_remains where y=$pfhd_y");
		$finish_code = sql_get_value("code_asu_pfhd","pfhd_article","y=$pfhd_y and is_remains_finish=1");
		
		array_pop($years); // последний год не нужен так как вставляем расчет остатки на конец = $year+1
		foreach ($years as $year) {
			self::_fillTmpPfhdTable($pfhd_y,$ver_id,$year);			
			// Сохраняем остатки на конец года
			sql_query ("insert into tmp_pfhd_remains(y,booker_fin_source,sum) 
			select $year+1,booker_fin_source,sum from tmp_asu_pfhd where code_asu_pfhd='$finish_code'");
		}
		
	}
	
	//AVD
	private static function _fillXmlData($pfhd_y,$ver_id) {
		$data = [];
		self::$pfhd_article_y = $pfhd_y;
		self::_fillYearRemains($pfhd_y,$ver_id);
		// на год и 2 следующих
		$years = self::pfhd_years_array($pfhd_y);
		
		foreach ($years as $year) {
			self::_fillTmpPfhdTable($pfhd_y,$ver_id,$year);
			//$tmp_rows= sql_rows("select * from tmp_asu_pfhd");

			$rows = self::getPfhdRows($pfhd_y);

			foreach ($rows as $row) {
				$article = sql_get_array("pfhd_article","code_asu_pfhd=${row['code_asu_pfhd']}");				
				$row['FinanceActivityPlanCode'] = substr($row['code_asu_pfhd'],2);
				$row['TempOrderFunds'] = $article['is_temp_order_funds'] ? 'true' : 'false';
				$row['InformationCode'] = $article['is_information_code'] ? 'true' : 'false';
				$row['year'] = $year;
				$row['Sum'] += 0;
				if ($article['is_temp_order_funds']) {
					if ($year == $pfhd_y) {
						$data['TempFunds'][] = $row;
					}
				} else if ($article['is_information_code']) {
					if ($year == $pfhd_y) {
						$data['HelpInfo'][] = $row;
					}
				} else {
					$data['IncomePayment'][] = $row;
				}
			}
		}
		return $data;
	}

	//-------------------------------------------------------------------
	// AVD 17.08.2017
	//-------------------------------------------------------------------
	private static function _echoActivityGoal($xml_path)
	{
		echo "    <ActivityGoal>\r\n";
		$sql = "SELECT * FROM asu_pfhd_activity_goal";
		$rows = sql_rows($sql);	
		$i = 1;
		foreach ($rows as $fld) 
		{
			require ($xml_path . '/ActivityGoalItem.xml');
			$i++;
		}
		echo "    </ActivityGoal>\r\n";
	}

	//-------------------------------------------------------------------
	// AVD 17.08.2017
	//-------------------------------------------------------------------
	private static function _echoActivityKind($xml_path)
	{
		echo "    <ActivityKind>\r\n";
		$sql = "SELECT * FROM asu_pfhd_activity_kind_pfhd";
		$rows = sql_rows($sql);	
		$i = 1;
		foreach ($rows as $fld) 
		{
			require ($xml_path . '/ActivityKindItem.xml');
			$i++;
		}
		echo "    </ActivityKind>\r\n";
	}
	
	//-------------------------------------------------------------------
	// AVD 19.08.2017
	//-------------------------------------------------------------------
	private static function _echoFinanceStateItem($xml_path)
	{
		echo "    <FinanceStates>\r\n";
		echo "      <FinStateValueDate>" . date("Y-m-d") . "</FinStateValueDate>\r\n";
		echo "      <FinanceStateItems>\r\n";	  
		$sql = "SELECT * FROM asu_pfhd_fin_state";
		$rows = sql_rows($sql);	
		$i = 1;
		foreach ($rows as $fld) 
		{
			require ($xml_path . '/FinanceStateItem.xml');
			$i++;
		}
		echo "      </FinanceStateItems>\r\n";
		echo "    </FinanceStates>\r\n";
	}
	
	//-------------------------------------------------------------------
	// AVD 17.08.2017
	//-------------------------------------------------------------------
	private static function _echoServices($xml_path)
	{
		echo "    <Services>\r\n";
		$sql = "SELECT * FROM asu_pfhd_service_list";
		$rows = sql_rows($sql);	
		$i = 1;
		foreach ($rows as $fld) 
		{
			require ($xml_path . '/ServicesItem.xml');
			$i++;
		}
		echo "    </Services>\r\n";
	}

	//-------------------------------------------------------------------
	// AVD 18.08.2017
	//-------------------------------------------------------------------
	private static function _echoEvent($xml_path)
	{
		echo "    <Event>\r\n";
		$sql = "
			SELECT
			  asu_pfhd_expense_direction.code AS ExpenseDirectionCode,
			  asu_pfhd_expense_direction.short_name AS ExpenseDirectionName,
			  asu_pfhd_expense_direction.full_name AS ExpenseDirectionAltText,
			  pfhd_article.id AS ExpenseDirectionVkCode,
			  pfhd_article.name AS ExpenseDirectionVkName,
			  pfhd_article.code_asu_pfhd AS ExpenseDirectionVkFinanceActivityPlanCode,
			  asu_pfhd_activity_direction.code AS ExpenseDirectionActivityDirectionCode,
			  asu_pfhd_activity_direction.full_name AS ExpenseDirectionActivityDirectionName,
			  asu_pfhd_program_event.code AS ExpenseDirectionProgramEventCode,
			  asu_pfhd_program_event.name AS ExpenseDirectionProgramEventName,
			  asu_pfhd_program_event.is_strategic AS ExpenseDirectionProgramEventIsStrategic,
			  asu_pfhd_program_event.is_energy_saving AS ExpenseDirectionProgramEventIsEnergySaving,
			  asu_pfhd_program_event.name AS ExpenseDirectionProgramEventAltText,
			  asu_pfhd_institution_program.code AS ExpenseDirectionInstitutionProgramCode,
			  asu_pfhd_institution_program.short_name AS ExpenseDirectionInstitutionProgramName,
			  asu_pfhd_institution_program.full_name AS ExpenseDirectionInstitutionProgramAltText,
			  asu_pfhd_perfomance_goal.code AS ExpenseDirectionInstitutionProgramPerfomanceGoals1Code,
			  asu_pfhd_perfomance_goal.name AS ExpenseDirectionInstitutionProgramPerfomanceGoals1Text,
			  asu_pfhd_institution_subprogram.code AS ExpenseDirectionInstitutionSubprogramCode,
			  asu_pfhd_institution_subprogram.short_name AS ExpenseDirectionInstitutionSubprogramName,
			  asu_pfhd_cppk_program.code AS ExpenseDirectionCPPKProgramCode,
			  asu_pfhd_cppk_program.short_name AS ExpenseDirectionCPPKProgramName,
			  asu_pfhd_event_item.plan_expense AS PlanExpense,
			  asu_pfhd_event_item.execution_time AS ExecutionTime,
			  asu_pfhd_event_item.execution_time_end AS ExecutionTimeEnd,
			  asu_pfhd_event_item.PlanResultY0P1,
			  asu_pfhd_event_item.PlanResultY1P1,
			  asu_pfhd_event_item.PlanResultY2P1
			FROM asu_pfhd_event_item
			  LEFT OUTER JOIN asu_pfhd_expense_direction
				ON asu_pfhd_event_item.expense_direction = asu_pfhd_expense_direction.code
			  LEFT OUTER JOIN pfhd_article
				ON asu_pfhd_expense_direction.vk = pfhd_article.code_asu_pfhd
			  LEFT OUTER JOIN asu_pfhd_activity_direction
				ON asu_pfhd_expense_direction.activity_direction = asu_pfhd_activity_direction.code
			  LEFT OUTER JOIN asu_pfhd_program_event
				ON asu_pfhd_expense_direction.program_event = asu_pfhd_program_event.code
			  LEFT OUTER JOIN asu_pfhd_institution_program
				ON asu_pfhd_expense_direction.institution_program = asu_pfhd_institution_program.code
			  LEFT OUTER JOIN asu_pfhd_perfomance_goal
				ON asu_pfhd_institution_program.perfomance_goal1 = asu_pfhd_perfomance_goal.code
			  LEFT OUTER JOIN asu_pfhd_institution_subprogram
				ON asu_pfhd_expense_direction.institution_subprogram = asu_pfhd_institution_subprogram.code
			  LEFT OUTER JOIN asu_pfhd_cppk_program
				ON asu_pfhd_expense_direction.cppk_program = asu_pfhd_cppk_program.code
		";
		$rows = sql_rows($sql);	
		$i = 1;
		foreach ($rows as $fld) 
		{
			require ($xml_path . '/EventItem.xml');
			$i++;
		}
		echo "    </Event>\r\n";
	}

	//-------------------------------------------------------------------
	// AVD 19.08.2017
	//-------------------------------------------------------------------
	private static function _echoEnergySaving($xml_path)
	{
		echo "    <EnergySaving>\r\n";
		$sql = "
			SELECT
			  asu_pfhd_expense_direction.code AS ExpenseDirectionCode,
			  asu_pfhd_expense_direction.short_name AS ExpenseDirectionName,
			  asu_pfhd_expense_direction.full_name AS ExpenseDirectionAltText,
			  pfhd_article.id AS ExpenseDirectionVkCode,
			  pfhd_article.name AS ExpenseDirectionVkName,
			  pfhd_article.code_asu_pfhd AS ExpenseDirectionVkFinanceActivityPlanCode,
			  asu_pfhd_activity_direction.code AS ExpenseDirectionActivityDirectionCode,
			  asu_pfhd_activity_direction.full_name AS ExpenseDirectionActivityDirectionName,
			  asu_pfhd_program_event.code AS ExpenseDirectionProgramEventCode,
			  asu_pfhd_program_event.name AS ExpenseDirectionProgramEventName,
			  asu_pfhd_program_event.is_strategic AS ExpenseDirectionProgramEventIsStrategic,
			  asu_pfhd_program_event.is_energy_saving AS ExpenseDirectionProgramEventIsEnergySaving,
			  asu_pfhd_program_event.name AS ExpenseDirectionProgramEventAltText,
			  asu_pfhd_institution_program.code AS ExpenseDirectionInstitutionProgramCode,
			  asu_pfhd_institution_program.short_name AS ExpenseDirectionInstitutionProgramName,
			  asu_pfhd_institution_program.full_name AS ExpenseDirectionInstitutionProgramAltText,
			  asu_pfhd_perfomance_goal.code AS ExpenseDirectionInstitutionProgramPerfomanceGoals1Code,
			  asu_pfhd_perfomance_goal.name AS ExpenseDirectionInstitutionProgramPerfomanceGoals1Text,
			  asu_pfhd_institution_subprogram.code AS ExpenseDirectionInstitutionSubprogramCode,
			  asu_pfhd_institution_subprogram.short_name AS ExpenseDirectionInstitutionSubprogramName,
			  asu_pfhd_cppk_program.code AS ExpenseDirectionCPPKProgramCode,
			  asu_pfhd_cppk_program.short_name AS ExpenseDirectionCPPKProgramName,
			  asu_pfhd_energy_saving_item.plan_expense AS PlanExpense,
			  asu_pfhd_energy_saving_item.execution_time AS ExecutionTime,
			  asu_pfhd_energy_saving_item.execution_time_end AS ExecutionTimeEnd,
			  asu_pfhd_energy_saving_item.PlanResultY0P1,
			  asu_pfhd_energy_saving_item.PlanResultY1P1,
			  asu_pfhd_energy_saving_item.PlanResultY2P1
			FROM asu_pfhd_energy_saving_item
			  LEFT OUTER JOIN asu_pfhd_expense_direction
				ON asu_pfhd_energy_saving_item.expense_direction = asu_pfhd_expense_direction.code
			  LEFT OUTER JOIN pfhd_article
				ON asu_pfhd_expense_direction.vk = pfhd_article.code_asu_pfhd
			  LEFT OUTER JOIN asu_pfhd_activity_direction
				ON asu_pfhd_expense_direction.activity_direction = asu_pfhd_activity_direction.code
			  LEFT OUTER JOIN asu_pfhd_program_event
				ON asu_pfhd_expense_direction.program_event = asu_pfhd_program_event.code
			  LEFT OUTER JOIN asu_pfhd_institution_program
				ON asu_pfhd_expense_direction.institution_program = asu_pfhd_institution_program.code
			  LEFT OUTER JOIN asu_pfhd_perfomance_goal
				ON asu_pfhd_institution_program.perfomance_goal1 = asu_pfhd_perfomance_goal.code
			  LEFT OUTER JOIN asu_pfhd_institution_subprogram
				ON asu_pfhd_expense_direction.institution_subprogram = asu_pfhd_institution_subprogram.code
			  LEFT OUTER JOIN asu_pfhd_cppk_program
				ON asu_pfhd_expense_direction.cppk_program = asu_pfhd_cppk_program.code
		";
		$rows = sql_rows($sql);	
		$i = 1;
		foreach ($rows as $fld) 
		{
			require ($xml_path . '/EnergySavingItem.xml');
			$i++;
		}
		echo "    </EnergySaving>\r\n";
	}
	
	//-------------------------------------------------------------------
	// AVD 02.02.2017
	//-------------------------------------------------------------------
	private static function _echoPaymentIndex($ver_id, $xml_path)
	{
		echo "    <PaymentIndex>\r\n";
		$sql = "
			SELECT
			  stat_pfhd.pfhd_y,
			  SUM(stat_pfhd.sum) AS 'sum',
			  stat_pfhd.plan_grafic_id,
			  stat_pfhd.stat_pfhd_ver_id,
			  stat_pfhd.id,
			  stat_pfhd_ver.plan_period_id,
			  plan_period.y_from
			FROM stat_pfhd
			  INNER JOIN dogovor
				ON stat_pfhd.dogovor_id = dogovor.id
			  INNER JOIN stat_pfhd_ver
				ON stat_pfhd_ver.id = stat_pfhd.stat_pfhd_ver_id
			  INNER JOIN plan_period
				ON stat_pfhd_ver.plan_period_id = plan_period.id
			WHERE (stat_pfhd.plan_grafic_id = 1
			OR stat_pfhd.plan_grafic_id = 2)
			AND stat_pfhd.stat_pfhd_ver_id = $ver_id
			GROUP BY stat_pfhd.pfhd_y,
					 stat_pfhd.plan_grafic_id,
					 stat_pfhd.stat_pfhd_ver_id,
					 stat_pfhd_ver.plan_period_id,
					 plan_period.y_from;	
		";
		$Code = '0001';
		$ValueName = 'Выплаты по расходам на закупку товаров, работ, услуг всего:';
		$OveralFinYear = 0;
		$OveralFirstYear = 0;
		$OveralSecondYear = 0;
		$Fz44FinYear = 0;
		$Fz44FirstYear = 0;
		$Fz44SecondYear = 0;
		$Fz223FinYear = 0;
		$Fz223FirstYear = 0;
		$Fz223SecondYear = 0;
		$rows = sql_rows($sql);		
		foreach ($rows as $fld) 
		{
			$y_num = $fld['pfhd_y'] - $fld['y_from'];
			$sum = $fld['sum'];
			switch ($y_num) 
			{
				case 0:
					$OveralFinYear += $sum;
					switch ($fld['plan_grafic_id']) 
					{
						case 1:
							$Fz44FinYear += $sum;
							break;
						case 2:
							$Fz223FinYear += $sum;
							break;
						default:
					}
					break;
				case 1:
					$OveralFirstYear += $sum;
					switch ($fld['plan_grafic_id']) 
					{
						case 1:
							$Fz44FirstYear += $sum;
							break;
						case 2:
							$Fz223FirstYear += $sum;
							break;
						default:
					}
					break;
				case 2:
					$OveralSecondYear += $sum;
					switch ($fld['plan_grafic_id']) 
					{
						case 1:
							$Fz44SecondYear += $sum;
							break;
						case 2:
							$Fz223SecondYear += $sum;
							break;
						default:
					}
					break;
				default:
			}
		}
		require ($xml_path . '/PaymentIndexItem.xml');				
		//
		$Code = '1001';
		$ValueName = 'в том числе: на оплату контрактов заключенных до начала очередного финансового года:в том числе: на оплату контрактов заключенных до начала очередного финансового года:';
		$OveralFinYear = 0;
		$OveralFirstYear = 0;
		$OveralSecondYear = 0;
		$Fz44FinYear = 0;
		$Fz44FirstYear = 0;
		$Fz44SecondYear = 0;
		$Fz223FinYear = 0;
		$Fz223FirstYear = 0;
		$Fz223SecondYear = 0;
		require ($xml_path . '/PaymentIndexItem.xml');				
		//
		$Code = '2001';
		$ValueName = 'на закупку товаров работ, услуг по году начала закупки:';
		require ($xml_path . '/PaymentIndexItem.xml');				
		echo "    </PaymentIndex>\r\n";
	}
	//-------------------------------------------------------------------
	private static function _toXml($y,$ver_id) {
		
		//$year = date('Y');
		if ($y != 2017 && $y != 2018 && $y!=2019) {
			die("unknown year=$y in pfhd::_toXml");
		}
		$data = self::_fillXmlData($y,$ver_id);
		$org = self::_get_org_rekvizits();
		
		$year = $y; // используется в xml
		$xml_path = $_SERVER['DOCUMENT_ROOT'] . '/asu_pfhd/pfhd2017';
		ob_start();
		require ($xml_path . '/pfhd_header.xml');
		
		// AVD 17.08.2017		
		self::_echoActivityGoal($xml_path);
		
		// AVD 17.08.2017		
		self::_echoActivityKind($xml_path);
		
		// AVD 17.08.2017		
		self::_echoServices($xml_path);		
		
		/////////////////////////////////////////////////////////
		// IncomePayment
		/////////////////////////////////////////////////////////
		foreach ($data as $key => $articles) 
		{
			if ($key == 'IncomePayment')
			{
				echo "	<IncomePayment>\n";
				foreach ($articles as $row) 
				{
					if (self::$useCalcTables) {
						$row['xml_calcuation'] = self::_incomeActivityCalcXml($ver_id,$row,$year,$row['year']);
					}
				
					require ($xml_path . '/IncomePaymentItem.xml');
				}
				echo "    </IncomePayment>\n";
			}
		}
		/////////////////////////////////////////////////////////
		
		// AVD 18.08.2017		
		self::_echoEvent($xml_path);		

		// AVD 19.08.2017		
		self::_echoEnergySaving($xml_path);		
		
		// AVD 19.08.2017		
		self::_echoFinanceStateItem($xml_path);				

		/////////////////////////////////////////////////////////
		// TempFunds
		/////////////////////////////////////////////////////////
		foreach ($data as $key => $articles) 
		{
			if ($key == 'TempFunds')
			{
				echo '	<'.$key.">\n";
				foreach ($articles as $row) 
				{
					require ($xml_path . '/' . $key . 'Item.xml');
				}
				echo '    </'.$key.">\n";
			}
		}
		/////////////////////////////////////////////////////////

		/////////////////////////////////////////////////////////
		// HelpInfo
		/////////////////////////////////////////////////////////
		foreach ($data as $key => $articles) 
		{
			if ($key == 'HelpInfo')
			{
				echo '	<'.$key.">\n";
				foreach ($articles as $row) 
				{
					require ($xml_path . '/' . $key . 'Item.xml');
				}
				echo '    </'.$key.">\n";
			}
		}
		/////////////////////////////////////////////////////////
		
		// AVD 02.02.2017
		self::_echoPaymentIndex($ver_id, $xml_path);
		
		require ($xml_path . '/pfhd_footer.xml');	
		$xml = ob_get_clean();		
		return $xml;
		
	}
	
	private static function _calcCodeOkei($code) {
		$str_code = str_pad($code,3,'0',STR_PAD_LEFT); // Данные gz_okei
		$row = sql_get_array('gz_okei',"code='$code'");
		$val = "<c:Code>$code</c:Code><c:Text>".$row['name']."</c:Text>";
		return $val;
	}
	
	private static function _calcColIsOkei($tableCode, $col) {
		// мат запасы
		return ($tableCode == 'PaymentPurchaseTangibleAssets' && $col == 'C3' || $tableCode == 'PaymentCommunal' && $col == 'C2_1');
		
	}
	
	public static function _incomeActivityCalcXml($pfhd_ver_id,$pfhd_row,$pfhd_y,$y) {
//		var_dump($pfhd_row);
//		die("12");
		$_skipCodes = ['PaymentInsurance'=>['SH1.C3_R8','SH1.C3_R9','SH1.C3_R13','SH1.C3_R14','SH1.C3_R18','SH1.C3_R15']];
		
		$code_asu_pfhd = $pfhd_row['code_asu_pfhd'];
		
		
		$fld = 'pfhd_article_'.$pfhd_y.'_id';
		
		$table_rows = sql_rows("select c.* FROM pfhd_calc_table c inner join pfhd_article pa ON c.$fld = pa.id WHERE c.need_send_pfhd=1 and code_asu_pfhd='$code_asu_pfhd'");
		if (!$table_rows) {
			return '';
		}
		// Если дельта превышает лимит то записей не заносим - это признак что не надо передавать расчет!
		$cnt_delta = sql_get_value("count(d.id)","pfhd_delta d inner join pfhd_article a ON d.pfhd_article_id=a.id", "d.pfhd_ver_id=$pfhd_ver_id AND d.y=$y AND a.code_asu_pfhd = '$code_asu_pfhd' ");
		if ($cnt_delta == 0) return '';
		
		$table = $table_rows[0];
		$calcCode = $table['code'];
		$skipTags = isset($_skipCodes[$calcCode]) ? $_skipCodes[$calcCode] : [];
		
		
		$xml = "<c:Code>$calcCode</c:Code>\n";
		$xml .="\t\t\t\t<c:data>\n";
		// Обычные
		$sql = "select v.*, r.code, r.section,r.pfhd_calc_table_row_type_id from pfhd_calc_table_value v inner join  pfhd_calc_table_row r on v.pfhd_calc_table_row_id = r.id where r.pfhd_calc_table_id=${table['id']} AND y=$y and r.pfhd_calc_table_row_type_id=2";
		$rows = sql_rows($sql);
		foreach($rows as $row) {
			$r_code = $row['code'];
			foreach ($row as $col=>$val) {
				
				if (preg_match('/^C[0-9]+$/',$col) && ($val || $val==='0')) {
					$tag =  "SH1.".$col."_". $r_code;
					if (!in_array($tag,$skipTags))
						$xml .= "\t\t\t\t<c:$tag>$val</c:$tag>\n";
				}
			}
		}
		
		// c детализацией
		$l1_fld = 'pfhd_calc_table_level1_dict_val_id';
		$sql = "select distinct r.code, r.$l1_fld from pfhd_calc_table_row r where r.pfhd_calc_table_id=${table['id']}  AND pfhd_calc_table_row_type_id=3";
		$detail_rows = sql_rows($sql);
		foreach($detail_rows as $d_row) {
			$v_level1 = $d_row[$l1_fld];
			$w_level1 =  $v_level1 ? "$l1_fld='$v_level1'"  :  "$l1_fld is null";
			if ($v_level1) {
				if (!preg_match('/^(.*)\./',$d_row['code'],$matches)) {
					die("bad format detail code no dot ".$d_row['code']);
				}
				// Детализация 1-го уровня (если есть)
				$detail_level1_code = $matches[1];
				$xml .="\t\t\t\t<c:SH1.$detail_level1_code>\n\t\t\t\t<c:Item>\n";
				// Элемент детадизации 1-го уровня
				$tag = "SH1.$detail_level1_code.C2_R$detail_level1_code";
				$dict = sql_get_array("pfhd_calc_table_dict_val","id=".$v_level1);
				$dict_val = "<c:Code>".$dict['code']."</c:Code>"."<c:Text>".$dict['name']."</c:Text>";
				$xml .= "\t\t\t\t\t\t<c:$tag>$dict_val</c:$tag>\n";
			}
			
			$detail_code = $d_row['code'];
			$detail_end_code = $detail_code;
			if (preg_match('/\.(.*$)/',$detail_code,$matches)) {
				// если в коде точка - код многоуровневый сохраняем окончание
				$detail_end_code = $matches[1];
			}
			
			//$xml .="\t\t\t\t<c:SH1.$detail_code>\n";
			$found = false;
			$rows = sql_rows("select r.id row_id,d.code dict_code,d.name dict_name, d.type dict_type from pfhd_calc_table_row r inner join pfhd_calc_table_dict_val d ON r.pfhd_calc_table_dict_val_id = d.id where r.pfhd_calc_table_id=${table['id']} AND r.code='$detail_code'");
			foreach($rows as $row) {
				$vals = sql_get_array("pfhd_calc_table_value","pfhd_calc_table_row_id=".$row['row_id']. " AND y=$y");
				if (!$vals) continue;
				// Открываем тег на первом найжденном
				if (!$found) {
					$found = true;
					$xml .="\t\t\t\t<c:SH1.$detail_code>\n";
				}
				$xml .="\t\t\t\t\t<c:Item>\n";
				// Элемент справлочника
				$tag =  "SH1.$detail_code.C2_R$detail_end_code";
				//$dict = sql_get_array("pfhd_calc_table_dict","id=".$row['pfhd_calc_table_dict_id']);
				$dict_val = "<c:Code>".$row['dict_code']."</c:Code>"."<c:Text>".$row['dict_name']."</c:Text>";
				if ($row['dict_type']) {
					$dict_val .= "<c:Type>".$row['dict_type']."</c:Type>";
				}
				$xml .= "\t\t\t\t\t\t<c:$tag>$dict_val</c:$tag>\n";
				
				foreach ($vals as $col=>$val) {					
					if ((preg_match('/^C[0-9]+$/',$col) || $col == 'C2_1') && ($val || $val==='0')) { //C2_1 Коммунадка ед.изм.
						if (self::_calcColIsOkei($calcCode,$col)) {
							$val = self::_calcCodeOkei($val);
						}
						$tag =  "SH1.$detail_code.$col"."_R$detail_end_code";
						$xml .= "\t\t\t\t\t\t<c:$tag>$val</c:$tag>\n";
					}
				}
				$xml .="\t\t\t\t\t</c:Item>\n";
			}
			
			if ($found) {
				$xml .="\t\t\t\t</c:SH1.$detail_code>\n";
				if ($v_level1) {
					$xml .="\t\t\t\t</c:Item>\n\t\t\t\t</c:SH1.$detail_level1_code>\n";
				}
			}
		}
		$cols = ['budget','budget_special','budget_capital','oms','vne_budget','vne_budget_grant'];
		$itogo = sql_get_array("pfhd_calc_table_itogo","pfhd_calc_table_id=${table['id']} AND y=$y");
		
		if ($itogo) {
			$r_i = $table['itogo_row'];
			$c_i = $table['itogo_col'];
			foreach ($cols as $i=>$col) {
				if ($table['has_'.$col]) {
					$tag = $tag =  "SH1.C".($c_i+$i+1)."_R".$r_i;
					if ($col =='vne_budget') {
						$itogo[$col] = $pfhd_row['IncomeActivityValue'];					}
					$xml .= "\t\t\t\t<c:$tag>".$itogo[$col]."</c:$tag>\n";
				}
			}
			// Неизвестная Госсубсидия
			$tag =  "SH1.C".($c_i+1)."_2_R".$r_i;
			$xml .= "\t\t\t\t<c:$tag>".'0'."</c:$tag>\n";
		}
		$xml .= "\t\t\t\t</c:data>\n";
		$xml = "<Calc.$calcCode>".$xml."\t\t\t\t</Calc.$calcCode>\n";
		return $xml;
	}
	
	public static function toXml2017($ver_idse) {
		return self::_toXml(2017,$ver_id);
	}

	public static function toXml2018($ver_id) {
		return self::_toXml(2018,$ver_id);
	}
	
	public static function toXml($ver_id) {
		$y = sql_get_value("plan_period_id","pfhd_ver","id=". (int)$ver_id);
		return self::_toXml($y,$ver_id);
	}
	
	private static function _fillTmpPfhdTable($pfhd_y,$pfhd_ver_id,$year) {
		$MAX_DELTA = 10000;
		if (self::$useCalcTables) {
			sql_query("DELETE from pfhd_delta WHERE pfhd_ver_id=$pfhd_ver_id and y=$year");
			require_once($_SERVER['DOCUMENT_ROOT'].'/asu_pfhd/pfhd_calc_table_func.php');
			$fld = 'pfhd_article_'.$pfhd_y.'_id';
			$rows = sql_rows("select id,$fld from  pfhd_calc_table WHERE need_send_pfhd = 1 AND $fld is not null");
			foreach($rows as $row) {
				$r = pfhd_calc_table_itogo_row($row['id'],$year);
				$pfhd_article_id = $row[$fld];
				if ($r) {
					$sum_calc = $r['sum'] - $r['delta']; // РАсчетная ИТОГО (она в разных столбцах чтобы не тащить номер столбца )
					$sum_pfhd = sql_get_value('sum(sum)','pfhd',"pfhd_ver_id=$pfhd_ver_id and pfhd_article_id = $pfhd_article_id and y=$year");					
					$delta = $sum_calc - $sum_pfhd  ;//$r['delta'];
					if (abs($delta)<$MAX_DELTA) {
						sql_query("INSERT INTO pfhd_delta(pfhd_ver_id,pfhd_article_id,booker_fin_source,y,sum) VALUES($pfhd_ver_id,$pfhd_article_id,2,$year,$delta)");
					}
				}
			}
		}
		
		sql_query("delete from tmp_asu_pfhd");
		$fn_sql = ($pfhd_y<=2017) ? '_pfhd_sql_2017' : '_pfhd_sql' ;
		$artices_amounts_rows = array(); //Массив в котором ключи являются pfhd_article_id, а значение данные из $pfhd_article_data
		$articles_hier = array(); //Иерархия
		$stat_pfhd_table = 'stat_pfhd'; //Таблица с суммами
//		$pfhd_article_table = 'pfhd_article'; //(Таблица с иерархией)
		$fin_source_rows = sql_rows("select id from asu_pfhd_activity_kind");


		foreach($fin_source_rows as $row ) {
			$fin_source = $row['id'];
			$fin_sources = self::_fin_source_codes($fin_source);
			if(empty($fin_sources)) continue;
			$fin_sources_str = implode(',', $fin_sources);
			
			// переделать аргументы!!!
			$sql = self::$fn_sql($pfhd_ver_id);
			$sql2 = "SELECT pfhd_article_id, sum(sum) as sum
				FROM ($sql) as pre 
				WHERE booker_fin_source in ($fin_sources_str) AND pfhd_y=$year  
			 GROUP BY pfhd_article_id";
			$pfhd_rows = sql_rows($sql2); //берем суммы сгруппированные по pfhd_article_id
			
			// вспомогательное действие превращаем строки в ассоц массив
			$amounts = self::_fill_amounts($pfhd_rows); 	
			// заполняем сумм (для родитлей и по формулам)
			
			$rs = self::_fill_sum($pfhd_y,$amounts,$fin_source);
		
			// сохраняем во временную таблицу
			self::_save_to_table($rs);
		}
		
	}
	
	private static function _fin_source_codes($fin_source) {
		$codes = [];
		$rows = sql_rows("select code from booker_fin_source where asu_pfhd_activity_kind_id = ' $fin_source'");
		foreach($rows as $row) {
			$codes[] = $row['code'];
		}
		return $codes;
	}
	
	private static function _getSql($pfhd_y,$ver1_id,$ver2_id=null,$rashod_is_minus=false) {
		
		// проверить по годам !!!!!!!!!!!!
		self::$pfhd_article_y = $pfhd_y;
		//$fn_sql =  '_pfhd_sql_' . $pfhd_y ;
		
		$fn_sql = ($pfhd_y <= 2017) ? '_pfhd_sql_2017' : '_pfhd_sql' ;

		
		if ($ver2_id) {
			self::_fillYearRemains($pfhd_y,$ver1_id);
			$sql = self::$fn_sql($ver1_id,1,$rashod_is_minus);
			sql_query("CREATE TEMPORARY TABLE tmp_pfhd as  $sql") ;
			sql_query ("drop  temporary table if exists tmp_pfhd_remains ");
			self::_fillYearRemains($pfhd_y,$ver1_id);
			$sql = self::$fn_sql($ver2_id,2,$rashod_is_minus);
			sql_query("INSERT INTO tmp_pfhd  $sql") ;
			$sql = "select * from tmp_pfhd";
			
		} else {
			self::_fillYearRemains($pfhd_y,$ver1_id);
			$sql = self::$fn_sql($ver1_id,null,$rashod_is_minus);
		}
		return $sql;
	
	}

	/* Удалить? */
	public static function getSql2017($ver1_id,$ver2_id=null,$rashod_is_minus=false) {
		$pfhd_y = 2017;
		return self::_getSql($pfhd_y,$ver1_id,$ver2_id,$rashod_is_minus);
	}

	/* Удалить? */
	public static function getSql2018($ver1_id,$ver2_id=null,$rashod_is_minus=false) {
		$pfhd_y = 2018;
		return self::_getSql($pfhd_y,$ver1_id,$ver2_id,$rashod_is_minus);
	}
	
	public static function getSql($y, $ver1_id,$ver2_id=null,$rashod_is_minus=false) {
		return self::_getSql($y,$ver1_id,$ver2_id,$rashod_is_minus);
	}
	
	
	public static function verName($ver_id) {
		if (self::_is_current_version($ver_id)) $name = 'Текущая';
		else $name = sql_get_value('name','stat_pfhd_ver',"id = '$ver_id' ");
		return $name;
	}
	
	public static function getVersions() {
		$rows = sql_rows("SELECT id, name FROM stat_pfhd_ver order by tstamp DESC");
		return $rows;
	}
}
