<?php
//AVD Функция получения строки текушей даты и времени
function avd_getcurrentdate()
{
	return date("d.m.Y H:i:s", time() + 60 * 60);
}

//AVD Функция вывода элементов массива
function avd_printarrayvalues($arr, $arrname)
{
	$name = ""; //arrayname($arr);
	echo "<b>Значения массива $arrname</b><br>";
	while (list($key, $value) = each($arr))
		echo $key . " -> " . $value . "<br>";
	echo "<br>";
}

//AVD Функция вывода элементов массива рекурсивная
function avd_printarrayvalues_r(&$arr, $l = 0)
{
	@$arr = (array)$arr;
	foreach($arr as $i => $v) 
	{
		for ($n = 0; $n < $l; $n++) echo '&nbsp&nbsp&nbsp&nbsp';
		if (is_array($v))
		{
			echo $i . ': Массив [' . count($v) . ' элементов] = <br>'; 
			avd_printarrayvalues_r($v, $l + 1);
		}
		else if (is_object($v))
		{
			echo $i . ': Объект (' . count($v) . ' свойств) = <br>'; 
			avd_printarrayvalues_r($v, $l + 1);
		}
		else
			echo $i . ' = ' . $v . "<br>"; 
	}
}

//AVD Получение указанной части даты
function avd_getdatepart($time, $partname)
{
	$d = getdate($time);
	$r = $d[$partname];
	return $r;
}

//AVD Разбор строки и выделение в ней ссылок на документы
function avd_parsedoclinks($s)
{
	$a = explode(' ', $s);
	foreach ($a as $i => $v) 
		if (! stristr($v, '.') === FALSE)
		{
			$f = 'doc/eri/' . $v;
			if (file_exists($f))
				$a[$i] = '<a href = "' . $f . '">' . $v . '</a>';
		}
		else 
			$a[$i] = $v;
	$r = implode(' ', $a);
	return $r;
}

//AVD Чтение строк XML формата ЕД-108
function avd_read_ed108($filename)
{
	// simplexml_load_file
	// http://php.net/manual/ru/function.simplexml-load-file.php
	//$r = $filename; return array('filename' => $filename);
	//	
	$xml = simplexml_load_file($filename);
	$root = $xml->DataArea->Header->Report;
	//var_dump($root);
	$plat = [];
	$rows = [];
	$plat['GUID_FK'] = (string) $root->GUID_FK;
	$plat['DATE_PP'] = (string) $root->DATE_PP;
	$plat['SUM_PP'] = (string) $root->SUM_PP;
	$plat['INN_PLAT_PP'] = (string) $root->INN_PLAT_PP;
	$plat['KPP_PLAT_PP'] = (string) $root->KPP_PLAT_PP;
	$plat['PURPOSE_PP'] = (string) $root->PURPOSE_PP;
	$plat['FIO_ISP'] = (string) $root->FIO_ISP;
	$plat['TEL_ISP'] = (string) $root->TEL_ISP;
	// explode 
	foreach ($root->RegPP as $el)  {
		$row = [];
		foreach ($el as $key=>$val) {
			if ($key == 'PURPOSE')
			{
				$p = explode(';', $val);
				$pp = [];
				foreach ($p as $v) {
					$d = explode(':', $v);
					if (count($d) == 2) {
						$pp[$d[0]] = $d[1];				
					}
				}
				$row[$key] = $pp;
			}
			else if ($key == 'FIO_PLAT')
				$row[$key] = substr($val, 0, strlen($val) / 2);
			else 
				$row[$key] = (string) $val;
		}
		$rows[] = $row;
	}
	$r = [];
	$r ['HEADER'] = $plat;
	$r ['DETAIL'] = $rows;
	return $r;
	//return $xml;
}

//---------------------------------------------------------------------------------
//AVD Вывод ассоциативного массива в виде таблицы
function avd_print_assoc_array($r, $tittle = '', $num = false)
{
	if ($r)
	{
		//foreach($r as $i => $v) echo $i . ' = ' . $v . "<br>";
		//echo '<p>';
		if ($tittle) echo "<b>$tittle</b>";
		//echo '<table border="1" cellpadding="3" cellspacing="1" frame="border" rules="all">';
		echo '<table class="td-table">';
		$ii = 0;
		foreach ($r as $rr)
		{
			if ($ii == 0) 
			{
				//if ($tittle) echo '<tr><th colspan="' . count($rr) . '">' . $tittle . '</th></tr>';
				echo '<tr>';
				if ($num)
					echo '<th>№</th>';
				foreach ($rr as $i => $v) 
					echo '<th>' . $i . '</th>';
				echo '</tr>';
			}
			echo '<tr>';
			if ($num)
				echo '<td>' . ($ii + 1) . '</td>';
			foreach ($rr as $v) 
				echo '<td>' . $v . '</td>';
			echo '</tr>';
			$ii++;
		}
		echo '</table>';
		echo '<br/>';
		//echo '</p>';
	}
}

//AVD Выкладывает результаты запроса в массив;
function sql_result_array($r)
{
	$a = array();
	if ($r)
		while ($rr = $r->fetch_assoc())
			$a[] = $rr;
	return $a;
}

//AVD Получает название колонки 
function avd_get_column_name($column, $table = '')
{
	$r1 = sql_get_value('short_name', 'col', "col_name='$column'");
	$r2 = sql_get_value('short_name', 'col', "col_name='$column' AND `table`='$table'");
	//$r = $r2 || $r1 || $column;
	//var_dump($r); die();
	if ($r2)
		return $r2;
	else if ($r1)
		return $r1;
	else 
		return $column;
}

//AVD Получить значение в зависимости от значения $v и имени колонки #i
function avd_render_value($i, $v)
{
	if (substr($i, -3) == '_id') 
	try
	{					
		$maybetable = sql_get_value('view_name', 'col', "col_name='$i'");
		if ($maybetable && avd_table_exists($maybetable))
			$tablename = $maybetable;
		else
			$tablename = substr($i, 0, strlen($i) - 3);
		$vv = '';
		if (avd_table_exists($tablename)) 
			foreach (['name', 'description', 'fio', 'type'] as $f)
				if ($vv == '') {
					if (isset(sql_table_columns($tablename)[$f]))
						$vv = sql_get_value($f, $tablename, "id='$v'");
				}
					
		if ($vv)
			$v = $vv;
	} 
	catch (Exception $e) 
	{
		$v = 'Ошибка: ' + $e->getMessage();
	}
	return $v;
}

//AVD Преобразование ассоциативного результата запроса в таблицу
function avd_assoc_result($r, $tittle = '', $num = false)
{
	$result = '';
	if ($r)
	{
		//foreach($r as $i => $v) $result .=  $i . ' = ' . $v . "<br>";
		if ($tittle) $result .=  "<b>$tittle</b>";
		$result .=  '<table border = "1" cellpadding = "3" cellspacing = "1" frame = "border" rules = "all">';
		$ii = 0;
		//while ($rr = $r->fetch_assoc())
		$stoplist = ['use_in_editor'];
		foreach($r as $rr)
		{
			if ($ii == 0) 
			{
				$result .= '<tr>';
				if ($num)
					$result .=  '<th>№</th>';
				foreach ($rr as $i => $v) 
					if (!in_array($i, $stoplist))
						$result .=  '<th>' . avd_get_column_name($i) . '</th>';
				$result .=  '</tr>';
			}
			$result .=  '<tr>';
			if ($num)
				$result .=  '<td>' . ($ii + 1) . '</td>';			
			foreach ($rr as $i => $v) 
				if (!in_array($i, $stoplist))
				{
					$v = avd_render_value($i, $v);
					$result .=  '<td>' . $v . '</td>';
				}
			$result .=  '</tr>';
			$ii++;
		}
		$result .=  '</table>';
	}
	//else
	//	$result .=  "Нет результатов.";	
	return $result;
}

//AVD Вывод ассоциативного результата запроса в виде таблицы
function avd_print_assoc_result($r, $tittle = '', $num = false)
{
	//echo avd_assoc_result($r, $tittle, $num);
	/**/
	if ($r)
	{
		//foreach($r as $i => $v) echo $i . ' = ' . $v . "<br>";
		if ($tittle) echo "<b>$tittle</b>";
		echo '<table border = "1" cellpadding = "3" cellspacing = "1" frame = "border" rules = "all">';
		$ii = 0;
		while ($rr = $r->fetch_assoc())
		{
			if ($ii == 0) 
			{
				echo '<tr>';
				if ($num)
					echo '<th>№</th>';
				foreach ($rr as $i => $v) echo '<th>' . $i . '</th>';
				echo '</tr>';
			}
			echo '<tr>';
			if ($num)
				echo '<td>' . ($ii + 1) . '</td>';
			foreach ($rr as $i => $v) echo '<td>' . $v . '</td>';
			echo '</tr>';
			$ii++;
		}
		echo '</table>';
	}
	else
		echo "Нет результатов.";	
	/**/
}

//AVD Преобразование массива вложенных ассоциативных массивов в плоский массив
function avd_flat_assoc_array($in, $prefix = '')
{
	$in = (array)$in;
	$out = array();
	foreach ($in as $i => $v)
	{
		if (is_array($v) || is_object($v)) 
		{
			$out += avd_flat_assoc_array($v, $i);
		}			
		else
			$out[$prefix ? $prefix . '_' . $i : $i] = $v;
	}
	return $out;
}

//AVD Вывод значений ассоциативного массива в виде таблицы
function avd_print_assoc_values($r)
{
	if ($r)
	{
		//foreach($r as $i => $v) echo $i . ' = ' . $v . "<br>";
		//echo '<table border = "1" cellpadding = "3" cellspacing = "1" frame = "border" rules = "all">';
		echo '<table>';
		//while ($rr = $r->fetch_assoc())
		foreach($r as $k => $v)
		{
			echo '<tr>';
			echo '<th>' . $k . '</th>';
			echo '<td>' . $v . '</td>';
			echo '</tr>';
		}
		echo '</table>';
	}
	//else
		//echo "Нет результатов.";	
}

?>
