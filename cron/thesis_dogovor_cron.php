<?php
error_reporting(E_ALL | E_STRICT);

require dirname(__FILE__) . '/../modules/sql_connect.php';
require dirname(__FILE__) . '/../modules/sql.php';
require dirname(__FILE__) . '/../thesis/thesis_functions.php';

db_connect();


sql_query("CREATE TEMPORARY TABLE thesis_dogovor_tmp (
			dogovor_id INTEGER(11) NOT NULL
			, state VARCHAR(255) NULL DEFAULT NULL
			, guid VARCHAR(36) NOT NULL
			, amount DECIMAL(20,2) NULL DEFAULT NULL
			, ts datetime
)  ENGINE = MEMORY");


sql_query("START TRANSACTION");

$bad_guids = get_old_resstr_guids();

$sql = "SELECT dd.CARD_ID, dsav.INTEGER_VALUE as DOGOVOR_ID, dc.AMOUNT as AMOUNT 
FROM dbo.DF_DOC dd INNER JOIN dbo.SYS_ATTR_VALUE dsav ON dsav.ENTITY_ID = dd.CARD_ID 
INNER JOIN dbo.DF_CONTRACT dc ON dc.CARD_ID = dd.CARD_ID 
WHERE dsav.CATEGORY_ATTR_ID in (".thesis_reestr_guids_sql().") AND dsav.INTEGER_VALUE is not null 
AND dsav.ENTITY_ID not in ('$bad_guids') 
AND dd.VERSION_OF_ID is null 
ORDER BY  dsav.INTEGER_VALUE, dd.CREATE_TS desc";


$query = mssql_query($sql,$thesis_connect);

$dog_cards = [];
while ($row = mssql_fetch_array($query) ) {
	/*
	if ($row['DOGOVOR_ID'] == $dogovor_id) continue;
	$dogovor_id = $row['DOGOVOR_ID'];	
	$rows[] = $row;
	*/
	$dogovor_id = $row['DOGOVOR_ID'];
	if (!isset($dog_cards[$dogovor_id])) {
		$dog_cards[$dogovor_id] = [];
	}
	$dog_cards[$dogovor_id][] = "'" . mssql_guid_string($row['CARD_ID']) . "'";
}	

foreach ($dog_cards as $dogovor_id=>$cards) {
	$guids_str = implode(',',$cards);
	$sql = "SELECT wcp.STATE,wa.CREATE_TS,wa.CARD_ID FROM dbo.WF_CARD_PROC wcp  INNER JOIN dbo.WF_ASSIGNMENT wa ON wcp.CARD_ID = wa.CARD_ID WHERE wcp.CARD_ID in ($guids_str) ORDER BY wa.CREATE_TS desc ";
	//echo $sql;
	$query2 = mssql_query($sql,$thesis_connect);
	$row2 = mssql_fetch_array($query2);
	if ($row2) {
		$state = (!is_null($row2['STATE'])) ? explode(',', $row2['STATE'])[1] : $row2['STATE'];
		$card_guid = mssql_guid_string($row2['CARD_ID']);
		$ts = $row2['CREATE_TS'];
		sql_query("INSERT INTO thesis_dogovor_tmp(dogovor_id, state, guid, ts) VALUES ('$dogovor_id', '$state', '$card_guid', '$ts')");
	}
}

sql_query("DELETE FROM thesis_dogovor");
sql_query("INSERT INTO thesis_dogovor (guid, dogovor_id, state, ts) SELECT guid, dogovor_id, state,ts FROM thesis_dogovor_tmp t where dogovor_id  not in (select dogovor_id from thesis_dogovor)");
//sql_query("UPDATE thesis_dogovor d INNER JOIN thesis_dogovor_tmp t ON d.guid = t.guid SET d.state = t.state,d.ts = t.ts WHERE ifnull(d.state,0)<>ifnull(t.state,0) OR d.ts<>t.ts");
sql_query("COMMIT");