<?php

/*
function max_date($rows) {
	$dates = array();
	foreach($rows as $row) {
		$dates[] = $row['date_otch'];
	}
	return max($dates);
}
*/

function private_ls_labels($fld_names) {
	$labels = [];
	$labels['name'] = 'Название строк';			
	foreach($fld_names as $label) {
		if (!in_array($label,['name','fp_article_id','_FILTER_'])) $labels[$label] = $label;
	}
	$labels['itogo']='Всего';
	return $labels;
}

/*
function private_b_red_wrap($v) {
	return '<b style="color: #ff0000;">'.$v.'</b>';
}
*/

function fk_vp_normalize($query_result) {
	$row = array();
	$row['date_otch'] = $query_result['date_otch'];
	$row['ost_day_start'] = $query_result['sum_lbo_n'] - $query_result['sum_out_n'];
	$row['sum_itog_post'] = $query_result['sum_lbo_k']-$query_result['sum_lbo_n'];
	$row['sum_itog_vipl'] = $query_result['total_out_bud'];
	$row['ost_day_end'] = $query_result['sum_lbo_k'] - $query_result['sum_out_k'];
	return $row;
}

/*
function plat_dates_range() {
	$sql = "SELECT z.reestr_d FROM v_zayav z LEFT JOIN v_zayav_sostav zs ON zs.zayav_id = z.id LEFT JOIN fp_article fpa ON fpa.id = zs.fp_article_id WHERE z.zayav_status_id=" . ZAYAV_STATUS_IN_REESTR . " AND zs.booker_fin_source in (2, 4, 40, 400) GROUP BY fpa.id,z.reestr_d";
	$dates = sql_rows($sql);
	return array('min'=>min(array_column($dates, 'reestr_d')), 'max'=>max(array_column($dates, 'reestr_d')));
}
*/

function get_plat_sum($bank_acc_id) {
	//$range_d = plat_dates_range();
	$sum_sql = "SELECT sum(zs.sum) as sum 
	FROM v_zayav_sostav zs 
	LEFT JOIN booker_fin_source bfs on bfs.code = zs.booker_fin_source 
	LEFT JOIN v_zayav z on z.id = zs.zayav_id 
	WHERE bfs.bank_acc_id = '$bank_acc_id' 
		and z.zayav_status_id = '" . ZAYAV_STATUS_IN_REESTR . "'";
	// and z.reestr_d between '${range_d['min']}' and '${range_d['max']}'
	$sum = sql_fetch_value($sum_sql);
	return (!is_null($sum)) ? $sum : null;
}

function get_plat_sum_neraznes($bank_acc_id) {
	$sql = "SELECT SUM(b.sum_vipl) as sum 
	FROM `bank`.bank b 
	INNER JOIN bank_acc a ON b.ls=a.ls_name 
	WHERE a.id='$bank_acc_id' 
		AND b.zayav_id IS NULL 
		AND b.d>'2016-01-01'
		AND b.sum_vipl<>b.sum_post 
		AND b.sum_vipl<>0 ";
	return sql_fetch_value($sql);
}

//Остатки на лицевых счетах
function remains() {
	$bank_db = db_connect_bank();
	$bank_db->set_charset("utf8");
	$rows = [];
	
	//
	$fk_vj = $bank_db->query("select fk_vj.date_otch, fk_vj.ost_day_start, fki.sum_itog_post, fki.sum_itog_vipl, fk_vj.ost_day_end from fk_vj LEFT JOIN fk_iobu fki ON fki.fk_vj_id = fk_vj.id ORDER BY fk_vj.date_otch DESC LIMIT 1"); //20 bank_acc = 1
	$fk_vj_res = $fk_vj->fetch_assoc();
	$fk_vj_plat = get_plat_sum(BANK_ACC_LS_20);
	$fk_vj_plat_neraznes = get_plat_sum_neraznes(BANK_ACC_LS_20);
	$fk_vj_ost = $fk_vj_res['ost_day_end'] - $fk_vj_plat + $fk_vj_plat_neraznes;
	//if ($fk_vj_plat_neraznes) $fk_vj_plat_neraznes = private_b_red_wrap($fk_vj_plat_neraznes);
	$rows[] = array_merge(array('ls'=>'20 '), $fk_vj_res, 
		array('plat'=>$fk_vj_plat, 'plat_neraznes'=>$fk_vj_plat_neraznes, 'ostatok'=>$fk_vj_ost));
	
	//
	$fk_vk = $bank_db->query("select  fk_vk.date_otch, fk_vk.ost_day_start, fki.sum_itog_post, fki.sum_itog_vipl, fk_vk.ost_day_end from fk_vk LEFT JOIN fk_iobu fki ON fki.fk_vk_id = fk_vk.id ORDER BY fk_vk.date_otch DESC LIMIT 1"); //21 bank_acc = 2
	$fk_vk_res = $fk_vk->fetch_assoc();
	$fk_vk_plat = get_plat_sum(BANK_ACC_LS_21);
	$fk_vk_plat_neraznes = get_plat_sum_neraznes(BANK_ACC_LS_21);
	$fk_vk_ost = $fk_vk_res['ost_day_end'] - $fk_vk_plat + $fk_vk_plat_neraznes;
	//if ($fk_vk_plat_neraznes) $fk_vk_plat_neraznes = private_b_red_wrap($fk_vk_plat_neraznes);
	$rows[] = array_merge(array('ls'=>'21 '), $fk_vk_res, 
		array('plat'=>$fk_vk_plat, 'plat_neraznes'=>$fk_vk_plat_neraznes, 'ostatok'=>$fk_vk_ost));
	
	//
	$fk_vp = $bank_db->query("select date_otch, sum_lbo_n, sum_lbo_k, sum_of_n, sum_of_k, sum_pbo_n, sum_pbo_k, sum_out_n, sum_out_k, total_out_bud from fk_vp ORDER BY date_otch DESC LIMIT 1");//14 bank_acc = 3
	$fk_vp_res = fk_vp_normalize($fk_vp->fetch_assoc());
	$fk_vp_plat = get_plat_sum(BANK_ACC_LS_14);
	$fk_vp_plat_neraznes = get_plat_sum_neraznes(BANK_ACC_LS_14);
	$fk_vp_ost = number_format(($fk_vp_res['ost_day_end'] - $fk_vp_plat + $fk_vp_plat_neraznes),2,'.','');
	//if ($fk_vp_plat_neraznes) $fk_vp_plat_neraznes = private_b_red_wrap($fk_vp_plat_neraznes);
	$rows[] = array_merge(array('ls'=>'14 '), $fk_vp_res, 
		array('plat'=>$fk_vp_plat, 'plat_neraznes'=>$fk_vp_plat_neraznes, 'ostatok'=>$fk_vp_ost));
	
	//$max_date = max_date($rows);
	/*
	foreach($rows as &$row) {
		if($row['date_otch'] < $max_date) {
			//$row['ost_day_start'] = $row['ost_day_end'];
			//$row['sum_itog_post'] = $row['sum_itog_vipl'] = 0;
			$row['date_otch'] = $max_date;
		}
	}
	*/
	return $rows;
}

//Расшифровка реестра по 20 л/с - массив со строками
function ls_rows($bank_acc_id) {
	$rows = array();
	$sql = "SELECT zs.fp_article_id, fpa.name, sum(zs.sum) as sum, z.reestr_d 
	FROM v_zayav z LEFT JOIN v_zayav_sostav zs ON zs.zayav_id = z.id 
	LEFT JOIN fp_article fpa ON fpa.id = zs.fp_article_id
	WHERE z.zayav_status_id=" . ZAYAV_STATUS_IN_REESTR . " AND 
	zs.booker_fin_source in (SELECT code FROM booker_fin_source WHERE bank_acc_id = '$bank_acc_id') 
	GROUP BY zs.fp_article_id,z.reestr_d";
	$rows = Utils::cross(sql_query($sql),"fp_article_id",'reestr_d','sum');
	usort($rows,function($a,$b) {return $a['itogo']<$b['itogo'] ? 1 : ($a['itogo']==$b['itogo'] ? 0 : -1);});

	$itogo_row = ['name'=>'Общий итог','fp_article_id'=>'ITOGO_ROW'];
	$less_html = '<a onclick="toggleInvisible(this); return false;" href="#">+</a> Прочие (до 100000 тыс.)';
	$less_row = ['name'=>$less_html,'fp_article_id'=>'LESS_ROW'];
	$less_sum = 0;
	$itogo_sum = 0;
	
	foreach($rows as &$row) {
		$row['name'] = sql_get_value("name","fp_article","id=".$row['fp_article_id']);
		foreach($row as $k=>$v) {
			if($k == 'name' || $k == 'itogo' || $k == 'fp_article_id' ) continue;
	
			if(!isset($itogo_row[$k])) $itogo_row[$k] = NULL;
			$itogo_sum += $v;
			$itogo_row[$k] += $v;	
			if($row['itogo'] < 100000) {
				if(!isset($less_row[$k])) $less_row[$k] = NULL;
				$less_sum += $v;
				$less_row[$k] += $v;
			}
			$row['_FILTER_'] = '&amp;ff_bank_account_id='.$bank_acc_id . "&amp;ff_fp_article_id=".$row['fp_article_id'];
		}
	}
	$itogo_row['itogo'] = $itogo_sum;
	$itogo_row['_FILTER_'] =  '&amp;ff_bank_account_id='.$bank_acc_id . "&amp";
	$less_row['itogo'] = $less_sum;
	
	if($bank_acc_id == BANK_ACC_LS_20) {
		$rows[] = $less_row;
	}
	$rows[] = $itogo_row;
	return $rows;
}

function lsResourceToHTML($rows, $labels = array(), $less = false, $bank_acc_id = BANK_ACC_LS_20) {
	if(!count($rows)) return false;

	$html = "";
	$html .= "<table class='td-table' border='1'>";
	
	$html .= "<tr>";
	foreach ($labels as $label) {
		$html .= "<th nowrap>" . $label . "</th>";
	}
	$html .= "</tr>";
	foreach($rows as $row) {
		if($less && isset($row['itogo']) && $row['itogo'] < 100000) {
			$html .= "<tr class='invis' style='display: none'>";
		} else {
			$html .= "<tr>";
		}

		$fpa_id = $row['fp_article_id'];
		foreach($labels as $fld_name=>$label) {
			$col_value = $row[$fld_name];
			$align = '';
			$nowrap = '';
			$url = '';
			if(is_numeric($col_value)) {
				$align = 'right';
				$nowrap = 'nowrap';
				$col_value = Utils::nf($col_value);
				if (isset($row['_FILTER_']) && $row['_FILTER_']) {
					$url = 'https://' . $_SERVER['SERVER_NAME'] .'/tedit.php?t=v_report_zayav&amp;ff_zayav_status_id='.ZAYAV_STATUS_IN_REESTR.$row['_FILTER_'];
					if (is_date($fld_name)) $url.="&amp;ff_reestr_d=$fld_name";
				}

			}
			$a = ($url) ? '<a href="'.$url.'" style="text-decoration: none;" target="_blank" class="pseudo-link">'.$col_value.'</a>' : $col_value;
			$html .= '<td align="'.$align.'"'.$nowrap.'">'.$a.'</td>';
		}
		$html .= "</tr>";
	}
	$html .= "</table>";
	return $html;
}

?>