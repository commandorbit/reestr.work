<?php
$title = 'Справка по движению денежных средств по счетам СПбГЭУ';
require_once $_SERVER['DOCUMENT_ROOT'] . '/init.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/template/header.php';

define('ZAYAV_STATUS_IN_REESTR',7);
define('BANK_ACC_LS_20', 1);
define('BANK_ACC_LS_21', 2);
define('BANK_ACC_LS_14', 3);

require_once dirname(__FILE__).'/today_report_func.php';
?>

<div class="header-link">
    <div class="pivot-icons">
        <div class="service-ico to-excel" title="Выгрузить отчет в формате XLS"><a href="#" class="button"></a></div>
    </div>
    <div class="pivot-report-title"><h1 style="color: #fff; margin-top: 0; padding: 12px 0"><?=$title?></h1></div>
</div>

<div class="xls-html">
	<h1>Остаток на счетах</h1>
	<div class="container" style="padding: 20px 0">
	<?
		$labels = array(
			'ls'=>['label'=>'ЛС'],
			'date_otch'=>['label'=>'Дата выписки','format'=>'date'],
			'ost_day_start'=>['label'=>'Остаток на начало','format'=>'sum'],
			'sum_itog_post'=>['label'=>'Приход','format'=>'sum'],
			'sum_itog_vipl'=>['label'=>'Расход','format'=>'sum'],
			'ost_day_end'=>['label'=>'Остаток на конец','format'=>'sum'],
			'plat'=>['label'=>'Платежи в реестрах','format'=>'sum'],
			'plat_neraznes'=>['label'=>'Неразнесенные платежи','format'=>'sum'],
			'ostatok'=>['label'=>'Остаток','format'=>'sum']
		);
		echo Utils::toHTML(remains(), $labels);
	?>
	</div>
	<h1>Расшифровка реестра по 20 л/с</h1>
	<?php $rows = ls_rows(BANK_ACC_LS_20); ?>
	<?php if($rows): ?>
	<div class="container" style="padding: 20px 0;">
		<?
		$labels = private_ls_labels(array_keys($rows[0]));
		echo lsResourceToHTML($rows, $labels, true, BANK_ACC_LS_20);
		?>
	</div>
	<?php else: ?>
		<p style="text-align: center">За текущую дату движений не обнаружено</p>
	<?php endif; ?>
	
	<h1>Расшифровка реестра по 21 л/с</h1>
	<?php $rows = ls_rows(BANK_ACC_LS_21); ?>
	<?php if($rows): ?>
	<div class="container" style="padding: 20px 0;">	
		<?
		$labels = private_ls_labels(array_keys($rows[0]));
		echo lsResourceToHTML($rows, $labels, false, BANK_ACC_LS_21);
		?>
	</div>
	<?php else: ?>
		<p style="text-align: center">За текущую дату движений не обнаружено</p>
	<?php endif; ?>
</div>

<script>
	var ZAYAV_STATUS_IN_REESTR = 7;
	var toExcelBtn = document.querySelector('.to-excel');
	toExcelBtn.addEventListener('click', function(e) {
		var html = document.querySelector('.xls-html');
		toExcel(html.outerHTML);
	});
	
	function toExcel(html) {
        download('today_report' + '_' + new Date().getTime() + '.xls', html);
    }
	
	function download(filename, text) {
        var URL = window.URL || window.webkitURL || window.mozURL || window.msURL;
        var blob = new Blob([text]);
        var url = URL.createObjectURL(blob);
        var pom = document.createElement('a');
        pom.setAttribute('href', url);
        pom.setAttribute('download', filename);
        if (document.createEvent) {
            var event = document.createEvent('MouseEvents');
            event.initEvent('click', true, true);
            pom.dispatchEvent(event);
        }
        else {
            pom.click();
        }
    }
	
	var invisible_tr = document.querySelectorAll('.invis');
	function toggleInvisible(el) {
		if(el.invisible == undefined) el.invisible = true;
		el.invisible = !el.invisible;
		var display = (el.invisible) ? 'none' : 'table-row';
		var sign = (el.invisible) ? '+' : '-';
		for(var i =0; i < invisible_tr.length; i++) {
			invisible_tr[i].style.display = display;
		}
		el.innerHTML = sign;
		return false;
	}
	
	/*
	function goToFilterListener() {
		var tables = document.querySelectorAll('table');
		var filter_table = 'v_report_zayav';
		for(var i = 0; i < tables.length; i++) {
			tables[i].addEventListener('click', function(e) {
				var filter_table_url = window.location.protocol + '//' + window.location.host + '/tedit.php?t=' + filter_table;
				var target = e.target || e.srcElement;
				if(target.hasAttribute('data-filter') && target.getAttribute('data-filter') !== 'none') {
					var filter_str = target.getAttribute('data-filter');
					filter_table_url += filter_str + '&ff_zayav_status_id='+ZAYAV_STATUS_IN_REESTR;
					window.open(filter_table_url,'_blank');
				}
			});
		}
	}
	
	goToFilterListener();
	*/
</script>