<?php
define('REESTR', FALSE);
ob_start();
$title = 'Слияние XML-форм 6НДФЛ';
IF (REESTR):
	include $_SERVER['DOCUMENT_ROOT'] . '/init.php';
	include $_SERVER['DOCUMENT_ROOT'] . '/template/header.php';
ELSE:
	echo '<HTML>';	
	session_start();
ENDIF;
//require_once($_SERVER['DOCUMENT_ROOT'] . "/modules/avdfunctions.php");

//---------------------------------------------------------------------------------
//AVD Вывод ассоциативного массива в виде таблицы
function avd_print_assoc_array($r, $tittle = '', $num = false)
{
	if ($r)
	{
		//foreach($r as $i => $v) echo $i . ' = ' . $v . "<br>";
		//echo '<p>';
		if ($tittle) echo "<b>$tittle</b>";
		echo '<table border="1" cellpadding="3" cellspacing="1" frame="border" rules="all">';
		$ii = 0;
		foreach ($r as $rr)
		{
			if ($ii == 0) 
			{
				//if ($tittle) echo '<tr><th colspan="' . count($rr) . '">' . $tittle . '</th></tr>';
				echo '<tr>';
				if ($num)
					echo '<th>№</th>';
				foreach ($rr as $i => $v) 
					echo '<th>' . $i . '</th>';
				echo '</tr>';
			}
			echo '<tr>';
			if ($num)
				echo '<td>' . ($ii + 1) . '</td>';
			foreach ($rr as $v) 
				echo '<td>' . $v . '</td>';
			echo '</tr>';
			$ii++;
		}
		echo '</table>';
		echo '<br/>';
		//echo '</p>';
	}
}

/*
function union_node($source, $target, $childname, $groupby = [], $sumby = [])
{
	foreach($source->$childname as $v)
	{
		// Ищем такой же узел
		$node = null;
		if (count($groupby))
		{
			foreach($target->$childname as $t)
			{
				$found = true;
				foreach($groupby as $f)
				{
					if ((string)$t->attributes()[$f] != (string)$v->attributes()[$f])
					{
						//echo 'Стоп! <br>'; echo $t->attributes()[$f]; echo " != "; echo $v->attributes()[$f]; echo '<br>';
						$found = false;
						break;
					}
				}
				if ($found)
				{
					$node = $t;
					break;
				}
			}
		}
		if ($node)
		{
			//Если узел был найден, то суммирование
			foreach($sumby as $f)
				$node->attributes()[$f] += $v->attributes()[$f];
		}
		else
		{
			//Иначе создаём узел и копируем в него все аттрибуты
			$node = $target->addChild($childname);
			foreach ($v->attributes() as $aname => $avalue)
				$node->addAttribute($aname, $avalue);
		}
	}
	return $target;
}
*/

function union_assoc_arrays($source, $target, $groupby = [], $sumby = [])
{
	/*
	$result = Array(); 
	foreach ($target as $t)
	{
		$r = Array(); 
		foreach	($t as $i => $v)
			$r[$i] = $v;
		$result[] = $r;
	}
	*/
	//avd_print_assoc_array($source, 'Исходный');
	//avd_print_assoc_array($result, 'Целевой');

	foreach($source as $v)
	{
		// Ищем такой же узел
		$ii = 0;
		if (count($groupby))
		{
			$ii = -1;
			foreach($target as $i => $t)
			{
				$found = true;
				foreach($groupby as $f)
				{
					if ($t[$f] != $v[$f])
					{
						//echo 'Стоп! <br>'; echo $t[$f]; echo " != "; echo $v[$f]; echo '<br>';
						$found = false;
						break;
					}
				}
				if ($found)
				{
					$ii = $i;
					break;
				}
			}
		}
		if ($ii >= 0)
		{
			//Если узел был найден, то суммирование
			//var_dump($node); echo '<br>';
			//var_dump($v); echo '<hr>';			
			foreach($sumby as $f)
				$target[$ii][$f] += $v[$f];
			//echo '<hr>';			
		}
		else
		{
			//Иначе создаём узел и копируем в него все аттрибуты
			$node = Array();
			foreach ($v as $aname => $avalue)
				$node[$aname] = $avalue;
			$target[] = $node;
		}
	}
	return $target;
}

function get_assoc_array_from_xml_node($node)
{
	$result = Array();
	if ($node)
	foreach($node as $v)
	{
		$row = Array();
		foreach ($v->attributes() as $aname => $avalue)
		{
			$row[$aname] = (string) $avalue;
			//var_dump($avalue); echo '<hr>';
		}
		$result[] = $row;
	}
	return $result;
}

function put_assoc_array_into_xml_node($arr, $target, $childname)
{
	unset($target->$childname);
	if ($target)
	foreach ($arr as $a)
	{
		$node = $target->addChild($childname);
		foreach ($a as $aname => $avalue)
			$node->addAttribute($aname, $avalue);
	}
}

function union_xml($source, $target)
{
	/*
	union_node($source->Документ->НДФЛ6->ОбобщПоказ, $target->Документ->НДФЛ6->ОбобщПоказ, 'СумСтавка', 
		['Ставка'], 
		['НачислДох', 'НачислДохДив', 'ВычетНал', 'ИсчислНал', 'ИсчислНалДив', 'АвансПлат']);
	*/
	
	$src_arr0 = get_assoc_array_from_xml_node($source->Документ->НДФЛ6->ОбобщПоказ);
	$trg_arr0 = get_assoc_array_from_xml_node($target->Документ->НДФЛ6->ОбобщПоказ);
	$res_arr0 = union_assoc_arrays($src_arr0, $trg_arr0, 
		[], 
		['КолФЛДоход', 'УдержНалИт', 'НеУдержНалИт', 'ВозврНалИт']);
	
	$src_arr1 = get_assoc_array_from_xml_node($source->Документ->НДФЛ6->ОбобщПоказ->СумСтавка);
	$trg_arr1 = get_assoc_array_from_xml_node($target->Документ->НДФЛ6->ОбобщПоказ->СумСтавка);	
	
	$trg_arr1 = union_assoc_arrays($trg_arr1, [], 
		['Ставка'], 
		['НачислДох', 'НачислДохДив', 'ВычетНал', 'ИсчислНал', 'ИсчислНалДив', 'АвансПлат']);
	$res_arr1 = union_assoc_arrays($src_arr1, $trg_arr1, 
		['Ставка'], 
		['НачислДох', 'НачислДохДив', 'ВычетНал', 'ИсчислНал', 'ИсчислНалДив', 'АвансПлат']);

	/*
	union_node($source->Документ->НДФЛ6->ДохНал, $target->Документ->НДФЛ6->ДохНал, 'СумДата',
		['ДатаФактДох', 'ДатаУдержНал', 'СрокПрчслНал'], 
		['ФактДоход', 'УдержНал']);
	*/
	
	$src_arr2 = get_assoc_array_from_xml_node($source->Документ->НДФЛ6->ДохНал->СумДата);
	$trg_arr2 = get_assoc_array_from_xml_node($target->Документ->НДФЛ6->ДохНал->СумДата);
	
	$trg_arr2 = union_assoc_arrays($trg_arr2, [], 
		['ДатаФактДох', 'ДатаУдержНал', 'СрокПрчслНал'], 
		['ФактДоход', 'УдержНал']);	
	$res_arr2 = union_assoc_arrays($src_arr2, $trg_arr2, 
		['ДатаФактДох', 'ДатаУдержНал', 'СрокПрчслНал'], 
		['ФактДоход', 'УдержНал']);
	
	//echo "<pre>"; var_dump($res_arr); echo "</pre>";
	usort($res_arr2, 'cmp_date');
	
	put_assoc_array_into_xml_node($res_arr0, $target->Документ->НДФЛ6, 'ОбобщПоказ');
	put_assoc_array_into_xml_node($res_arr1, $target->Документ->НДФЛ6->ОбобщПоказ, 'СумСтавка');
	put_assoc_array_into_xml_node([['СумДата' => 'V']], $target->Документ->НДФЛ6, 'ДохНал');
	put_assoc_array_into_xml_node($res_arr2, $target->Документ->НДФЛ6->ДохНал, 'СумДата');

	return $target;
}

function date_hash($date)
{
	$a = explode('.', $date);
	return $a[2] . $a[1] . $a[0];
}

function cmp_date($a, $b) 
{ 
	if (date_hash($a['ДатаФактДох']) == date_hash($b['ДатаФактДох']))
	{
		if (date_hash($a['ДатаУдержНал']) == date_hash($b['ДатаУдержНал']))		
		{
			if (date_hash($a['СрокПрчслНал']) == date_hash($b['СрокПрчслНал']))		
				return  0;
			else
				return date_hash($a['СрокПрчслНал']) < date_hash($b['СрокПрчслНал']) ? -1 : 1;			
		}
		else
			return date_hash($a['ДатаУдержНал']) < date_hash($b['ДатаУдержНал']) ? -1 : 1;			
	}
	else
		return date_hash($a['ДатаФактДох']) < date_hash($b['ДатаФактДох']) ? -1 : 1;	
}

// Нормализуем сессию
if (!isset($_SESSION["output"])) $_SESSION["output"] = '';
if (!isset($_SESSION["filecount"])) $_SESSION["filecount"] = 0;

//Кнопка "Сохранить"
if (isset($_REQUEST['save']))
{
	if ($_SESSION["output"])
	{
		ob_clean();
		echo iconv('UTF-8', 'windows-1251', $_SESSION["output"]);	
		//echo $_SESSION["output"];	
		header('Content-Type: text/xml');
		header('Content-Disposition: attachment;filename="6NDFL_'.date('d-m-Y_H:i:s').'.xml"');
		header('Cache-Control: max-age=0');
		die();
	}
	else
		echo '<script>alertMessage = "Вы ещё не загрузили ни одного файла.";</script>';
}

echo ob_get_clean();	

//Кнопка "Очистить"
if (isset($_REQUEST['clear']))
{
	$_SESSION["output"] = '';
	$_SESSION["filecount"] = 0;
}	

//В сессии у нас хранится XML от предыдущей загрузки в кодировке UTF-8, поэтому...
if ($_SESSION["output"])
	$xml_trg = new SimpleXMLElement(iconv('UTF-8', 'windows-1251', $_SESSION["output"]));
else
	$xml_trg = null;

//Кнопка "Загрузить"
if (isset($_REQUEST['run']))
{
	if ($_FILES['userfile'])
	{
		$xml_name = $_FILES['userfile']['tmp_name'];
		if ($xml_name)
		{
			$xml = file_get_contents($xml_name); 	//Исходное содержимое файла
			$xml_src = new SimpleXMLElement($xml); 	//Преобразуем в объект
			
			/*
			if ($xml_trg)
				union_xml($xml_src, $xml_trg);
			else
				$xml_trg = $xml_src;
			*/
			
			if (!$xml_trg)
			{
				$xml_trg = new SimpleXMLElement($xml);
				//put_assoc_array_into_xml_node([], 	$xml_trg->Документ->НДФЛ6, 'ОбобщПоказ');
				//var_dump($xml_trg->Документ->НДФЛ6->ОбобщПоказ); die();
				try 
				{
					$n = $xml_trg->Документ->НДФЛ6->ОбобщПоказ;
				} 
				catch (Exception $e) 
				{
					echo 'Неверный формат файла: ',  $e->getMessage(), "<br/>";
				}				
				//var_dump($n); die();
				if ($n)
					foreach ($n->attributes() as $aname => $avalue)
						$n->attributes()[$aname] = 0;
				else
					die();
				put_assoc_array_into_xml_node([], $xml_trg->Документ->НДФЛ6->ОбобщПоказ, 'СумСтавка');
				put_assoc_array_into_xml_node([], $xml_trg->Документ->НДФЛ6->ДохНал, 'СумДата');
			}
			union_xml($xml_src, $xml_trg);
			
			$_SESSION["output"] = iconv('windows-1251', 'UTF-8', $xml_trg->asXML());
			$_SESSION["filecount"]++;
		}
		else
			echo "<script>alertMessage = 'Нажмите кнопку \"Обзор...\" для выбора файла.';</script>";
	}
}

?>

<div class="header-link"><h1><?=$title?></h1></div>
<?IF (REESTR):?>
<div class="body">
<?ENDIF?>

<p>
<form method="post" enctype="multipart/form-data">
	Загружено файлов: <?=$_SESSION["filecount"]?><br>
	Загрузите <? if (isset($_SESSION["output"])) echo 'ещё один ' ?>XML-файл:
	<input name = "userfile" type = "file" width = "300">
	<input name="run" type="submit" value="Загрузить">
	<input name="clear" type="submit" value="Очистить">
	<input name="save" type="submit" value="Сохранить">
</form>
</p>

<?php if ($_SESSION["output"]): ?>
<p>
<textarea rows="15" cols="100" style="width:100%" readonly>
<?php 
	$xml = new DOMDocument('1.0');
	$xml->formatOutput = true;
	$xml->loadXML(iconv('UTF-8', 'windows-1251', $_SESSION["output"]));
	echo iconv('windows-1251', 'UTF-8', $xml->saveXML());
?>
</textarea>
</p>
<?php endif; ?>

<?php 
	if ($xml_trg)
	{
		//avd_print_assoc_array(get_assoc_array_from_xml_node($xml_trg->Документ->СвНП->НПЮЛ), 'НПЮЛ');	
		//avd_print_assoc_array(get_assoc_array_from_xml_node($xml_trg->Документ->Подписант->ФИО), 'Подписант, ФИО');
		avd_print_assoc_array(get_assoc_array_from_xml_node($xml_trg->Документ->НДФЛ6->ОбобщПоказ), 'Обобщенный показ', true); 
		avd_print_assoc_array(get_assoc_array_from_xml_node($xml_trg->Документ->НДФЛ6->ОбобщПоказ->СумСтавка), 'Суммы по ставкам', true); 
		avd_print_assoc_array(get_assoc_array_from_xml_node($xml_trg->Документ->НДФЛ6->ДохНал->СумДата), 'Сумы по датам', true);
	}
?>

<script>
	var alertMessage;
	if (alertMessage) 
		alert(alertMessage);
</script>

<?php
IF (REESTR):
	include $_SERVER['DOCUMENT_ROOT'] . '/template/footer.php';
ELSE:
	echo '</HTML>';
ENDIF;	
?>
<?IF (REESTR):?>
</div>
<?ENDIF?>
