<?php
$title = 'График динамики денежных средств';

require $_SERVER['DOCUMENT_ROOT'] . '/init.php';
require $_SERVER['DOCUMENT_ROOT'] . '/template/header.php';

if (!Permission::userHasPermission('may_watch_cash_dynamic')) {
	echo '<h1>У вас недостаточно прав для просмотра страницы</h1>';
	exit();
}

$bank_db = db_connect_bank();
$bank_db->set_charset("utf8");
$fk_vj = $bank_db->query("select ost_day_end, date_otch from fk_vj ORDER BY date_otch"); //20 bank_acc = 1
$fk_vk = $bank_db->query("select ost_day_end, date_otch from fk_vk ORDER BY date_otch"); //21 bank_acc = 2

$fk_vp = $bank_db->query("select sum_lbo_n, sum_lbo_k, sum_of_n, sum_of_k, sum_pbo_n, sum_pbo_k, sum_out_n, sum_out_k, total_out_bud, date_otch from fk_vp ORDER BY date_otch DESC");//14 bank_acc = 3
$fk_vp_data = array();
while($row = $fk_vp->fetch_assoc()) {
	$row = fk_vp_normalize($row);
	$d = new DateTime($row['date_otch']);
	$ts = $d->getTimestamp();
	$fk_vp_data[] = [$ts * 1000,($row['ost_day_end'])/1000];
}
//$fk_vp_res = fk_vp_normalize($fk_vp->fetch_assoc());

function fk_vp_normalize($query_result) {
	$row = array();
	$row['ost_day_start'] = number_format(($query_result['sum_lbo_n'] - $query_result['sum_out_n']), 2, '.', '');
	$row['sum_itog_post'] = number_format(($query_result['sum_lbo_k']-$query_result['sum_lbo_n']), 2, '.', '');
	$row['sum_itog_vipl'] = $query_result['total_out_bud'];
	$row['ost_day_end'] = number_format(($query_result['sum_lbo_k'] - $query_result['sum_out_k']), 2, '.', '');
	$row['date_otch'] = $query_result['date_otch'];
	return $row;
}

function getLSData($resource) {
	$plotdata = [];
	$i = 0;
	while ($row = $resource->fetch_assoc()) {
		$d = new DateTime($row['date_otch']);
		$ts = $d->getTimestamp();
		$plotdata[] = [$ts * 1000,($row['ost_day_end'])/1000];
		$i++;
	}
	return $plotdata;
}
?>
<div class="header-link">
<h1 style="color: #fff;"><?=$title?></h1>
</div>
<div id="plot" style="height:400px;width:80%;margin:0 auto; margin-top: 20px;"></div>

<script>
	var placeholder = document.querySelector('#plot');
	var fk_vj_data=<?=json_encode(getLSData($fk_vj))?>;
	var fk_vk_data=<?=json_encode(getLSData($fk_vk))?>;
	var fk_vp_data=<?=json_encode($fk_vp_data)?>;
	var plotinfo = fk_vj_data;
	var dataset = [
					{label:'20 счет' , data: fk_vj_data, hoverable:true}, 
					{label:'21 счет' , data: fk_vk_data, hoverable:true}, 
					{label:'14 счет' , data: fk_vp_data, hoverable:true}
				];
	var options = {
		series: {
		lines: { show: true },
		points: { show: true }
		},
		grid: { hoverable:true, clickable: true },
		xaxis: {mode: "time"},
		legend:{         
            backgroundOpacity: 0.5,
            noColumns: 0,
            backgroundColor: "#fff",   
            position: "ne"
        }
 	};
	
	var plot = $.plot($('#plot'), dataset, options);
	$("#plot").bind("plothover", function (event, pos, item) {
		if (item) {
			
			if (prevPoint!=item.dataIndex) {
				prevPoint=item.dataIndex;
				var info = normalizeData(item.datapoint);
				showTooltip(item.pageX,item.pageY,info);
			}
		} else {
			$('#tooltip').remove();
			prevPoint=null;
		}
 	});
	
	function normalizeData(datapoint) {
		var timestamp = datapoint[0];
		var sum = datapoint[1] * 1000;
		var date = new Date();
		date.setTime(timestamp);
		var dt = 'Дата: <b>' + formattedDate(date) + '</b>';
		sum = 'Сумма: <b>' + sum.toFixed(2).toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1 ") + '</b>';
		return dt + '<br />' + sum;
	}
	
	function formattedDate(d) {
		var month = '' + (d.getMonth() + 1),
			day = '' + d.getDate(),
			year = d.getFullYear();
		if (month.length < 2) month = '0' + month;
		if (day.length < 2) day = '0' + day;
		return [day, month, year].join('.');
	}
	
 	function showTooltip(x, y, contents) {
		$('#tooltip').remove();
		$('<div id="tooltip">' + contents + '</div>').css({
			position: 'absolute',
			display: 'none',
			top: y + 5,
			left: x + 5,
			border: '1px solid #fdd',
			padding: '2px',
			'background-color': '#fff',
			opacity: 1
		}).appendTo("body").fadeIn(200);
 	}
</script>