<?php
require_once $_SERVER['DOCUMENT_ROOT'] . '/init.php';
if (!defined('KOMANDIR_RASHODY_ZFO')) define('KOMANDIR_RASHODY_ZFO',67);

require $_SERVER['DOCUMENT_ROOT'] . '/template/header.php';

// 1-договор, 4-приказ
$dogovors = sql_rows("SELECT * FROM dogovor 
	WHERE document_type_id in (1,4) 
		AND id NOT IN (SELECT dogovor_id FROM dogovor_link_1c)");
$cnt = 0;	
foreach ($dogovors as $d) {
	$dogovor_id = $d['id'];
	$ref = dogovor_1c_relate($dogovor_id);
	
	if ($ref) {
		$sql = "INSERT INTO dogovor_link_1c (dogovor_id, Договоры_Ref, is_auto_relate) 
			VALUES ($dogovor_id, $ref, 2) ";
		//echo $sql.'<br>';
		sql_query($sql);
		$cnt++;
	}
	
}

echo "<h1>Привязано $cnt Договоров</h1>";

function dogovor_1c_relate($dogovor_id) {
	$d = sql_get_array("dogovor","id='$dogovor_id'");
	$nomer = $d['dogovor_num'];
	$date = $d['d'];
	$contragent_id = $d['contragent_id'];
	$contragent_info = sql_get_array("contragent","id='$contragent_id'");
	$contr_1c_ref = $contragent_info['Контрагенты_Ref'];
	if ($contr_1c_ref) $contr_1c_ref = '0x'.bin2hex($contr_1c_ref);
	$inn = $contragent_info['inn'];

	$dogovor_ref = null;
	$query = null;

	$is_komandir_rashody = ($d['zfo_id']==KOMANDIR_RASHODY_ZFO);
	if ($is_komandir_rashody) {
		if ($contr_1c_ref) {
			$query = OneC::select('Д.Ссылка')
					->from(['Справочник.Договоры','Д'])
					->where('Д.Контрагент', '=', $contr_1c_ref)
					->and_where('Д.Наименование','=','командировочные расходы  от -');
		}
	} elseif ($nomer && $date) {
		$o = OneC::select('К.Ссылка')
			->from(['Справочник.Контрагенты','К'])
			->where('К.ИНН', '=', $inn);
		if ($contr_1c_ref) {
			$o->or_where('К.Ссылка', '=', $contr_1c_ref);
		}
		$res = $o->execute();
		$contragent_refs = array();
		while ($row = mssql_fetch_array($res,MSSQL_ASSOC)) {
			$contragent_refs[] = '0x' . bin2hex($row['Ссылка']);
		}	
		if ($contragent_refs) {
			$dogovor_like = '% '.$nomer.' от '.sql_date2rus($date).'%';
			$query = OneC::select('Д.Ссылка')
					->from(['Справочник.Договоры','Д'])
					->where('Д.Контрагент', 'in', $contragent_refs)
					->and_where('Д.Наименование','like',$dogovor_like);
		}
	}

	if ($query) {
		$res = $query->execute();
		$row = mssql_fetch_array($res,MSSQL_ASSOC);	
		$dogovor_ref = $row ? '0x' . bin2hex($row['Ссылка'])  : null;
	}
	
	return $dogovor_ref;
}