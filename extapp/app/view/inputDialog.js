/*
 * File: app/view/inputDialog.js
 *
 * This file was generated by Sencha Architect version 4.2.4.
 * http://www.sencha.com/products/architect/
 *
 * This file requires use of the Ext JS 6.2.x Classic library, under independent license.
 * License of Sencha Architect does not include license for Ext JS 6.2.x Classic. For more
 * details see http://www.sencha.com/license or contact license@sencha.com.
 *
 * This file will be auto-generated each and everytime you save your project.
 *
 * Do NOT hand edit this file.
 */

Ext.define('extapp.view.inputDialog', {
    extend: 'Ext.window.Window',
    alias: 'widget.inputdialog',

    requires: [
        'extapp.view.inputDialogViewModel',
        'extapp.view.inputDialogViewController',
        'Ext.form.field.Text',
        'Ext.toolbar.Toolbar',
        'Ext.button.Button'
    ],

    controller: 'inputdialog',
    viewModel: {
        type: 'inputdialog'
    },
    modal: true,
    height: 111,
    width: 400,
    layout: 'form',
    title: 'Введите значение',

    items: [
        {
            xtype: 'textfield',
            reference: 'textField',
            fieldLabel: 'Значение'
        }
    ],
    dockedItems: [
        {
            xtype: 'toolbar',
            dock: 'bottom',
            ui: 'footer',
            layout: {
                type: 'hbox',
                pack: 'center'
            },
            items: [
                {
                    xtype: 'button',
                    text: 'ОК',
                    listeners: {
                        click: 'onButtonOKClick'
                    }
                },
                {
                    xtype: 'button',
                    text: 'Отмена',
                    listeners: {
                        click: 'onButtonCancelClick'
                    }
                }
            ]
        }
    ],
    listeners: {
        show: 'onWindowShow'
    }

});