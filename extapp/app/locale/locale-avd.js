Ext.onReady(function()
{
    // Дополнение к локализации
    Ext.define("My.override.Date", {
        override: "Ext.form.field.Date",
        format: "d.m.Y",
        startDay: 1
    });

    Ext.define("My.override.DatePicker",  {
        override: "Ext.picker.Date",
        getDayInitial: function(value) {
            var dayNames = {
                'Понедельник': 'Пн',
                'Вторник': 'Вт',
                'Среда': 'Ср',
                'Четверг': 'Чт',
                'Пятница': 'Пт',
                'Суббота': 'Сб',
                'Воскресенье': 'Вс',
            };
            return dayNames[value];
        }
    });

    Ext.define("My.override.util.Format", {
	   override: "Ext.util.Format",
	   decimalSeparator: ',',
	   thousandSeparator: ' '
    });
    
    /*
        var button = Ext.create('Ext.Button', {
        text: 'Click me',
        handler: function() {
            alert('You clicked the button!');
        }
        });
    */

    //alert('Библиотека подключена!');

});
