/*
 * File: app/model/modelZayav.js
 *
 * This file was generated by Sencha Architect version 4.2.4.
 * http://www.sencha.com/products/architect/
 *
 * This file requires use of the Ext JS 6.2.x Classic library, under independent license.
 * License of Sencha Architect does not include license for Ext JS 6.2.x Classic. For more
 * details see http://www.sencha.com/license or contact license@sencha.com.
 *
 * This file will be auto-generated each and everytime you save your project.
 *
 * Do NOT hand edit this file.
 */

Ext.define('extapp.model.modelZayav', {
    extend: 'Ext.data.Model',
    alias: 'model.modelzayav',

    requires: [
        'Ext.data.field.Number',
        'Ext.data.field.Date',
        'Ext.data.field.String'
    ],

    fields: [
        {
            type: 'int',
            name: 'id'
        },
        {
            type: 'int',
            name: 'zfo_id'
        },
        {
            type: 'int',
            name: 'dogovor_id'
        },
        {
            type: 'int',
            name: 'plat_type_id'
        },
        {
            type: 'float',
            name: 'sum',
            persist: false
        },
        {
            type: 'date',
            name: 'd'
        },
        {
            type: 'int',
            name: 'y'
        },
        {
            type: 'date',
            name: 'reestr_d'
        },
        {
            type: 'int',
            name: 'contragent_id'
        },
        {
            type: 'string',
            name: 'contragent_fio'
        },
        {
            type: 'string',
            name: 'notes'
        },
        {
            type: 'string',
            name: 'zayav_status_id'
        }
    ]
});