/*
 * File: app/model/modelSmetaFinSource.js
 *
 * This file was generated by Sencha Architect version 4.2.4.
 * http://www.sencha.com/products/architect/
 *
 * This file requires use of the Ext JS 6.2.x Classic library, under independent license.
 * License of Sencha Architect does not include license for Ext JS 6.2.x Classic. For more
 * details see http://www.sencha.com/license or contact license@sencha.com.
 *
 * This file will be auto-generated each and everytime you save your project.
 *
 * Do NOT hand edit this file.
 */

Ext.define('extapp.model.modelSmetaFinSource', {
    extend: 'Ext.data.Model',

    requires: [
        'Ext.data.field.Number'
    ],

    fields: [
        {
            type: 'int',
            name: 'id'
        },
        {
            type: 'int',
            name: 'smeta_id'
        },
        {
            type: 'int',
            name: 'fin_source_id'
        },
        {
            type: 'float',
            calculate: function(data) {
                var sum = 0;
                var r = storeFind('storeSmetaSostavList', [
                {
                    field: 'smeta_id',
                    value: data.smeta_id
                },
                {
                    field: 'fin_source_id',
                    value: data.fin_source_id
                },
                ]);
                r.forEach(function(rr)
                {
                    sum += rr.get('amount');
                });
                return sum;
            },
            name: 'amount',
            persist: false
        },
        {
            type: 'float',
            name: 'amount_limit'
        }
    ]
});