/*
 * File: app/model/modelSmetaSostavSelect.js
 *
 * This file was generated by Sencha Architect version 4.2.4.
 * http://www.sencha.com/products/architect/
 *
 * This file requires use of the Ext JS 6.2.x Classic library, under independent license.
 * License of Sencha Architect does not include license for Ext JS 6.2.x Classic. For more
 * details see http://www.sencha.com/license or contact license@sencha.com.
 *
 * This file will be auto-generated each and everytime you save your project.
 *
 * Do NOT hand edit this file.
 */

Ext.define('extapp.model.modelSmetaSostavSelect', {
    extend: 'Ext.data.Model',

    requires: [
        'Ext.data.field.String',
        'Ext.data.field.Number',
        'Ext.data.field.Boolean'
    ],

    fields: [
        {
            type: 'int',
            name: 'id'
        },
        {
            type: 'int',
            allowNull: true,
            name: 'fp_year_id'
        },
        {
            type: 'int',
            name: 'zfo_id'
        },
        {
            type: 'int',
            name: 'fp_article_id'
        },
        {
            type: 'int',
            name: 'fin_source_id'
        },
        {
            type: 'int',
            name: 'fp_object_id'
        },
        {
            type: 'int',
            name: 'smeta_id'
        },
        {
            type: 'int',
            name: 'unit_id'
        },
        {
            type: 'string',
            name: 'name'
        },
        {
            type: 'string',
            name: 'natural_indicator_name'
        },
        {
            type: 'float',
            name: 'price'
        },
        {
            type: 'float',
            name: 'quantity'
        },
        {
            type: 'float',
            name: 'amount'
        },
        {
            type: 'float',
            name: 'dogovor_sostav_amount'
        },
        {
            type: 'float',
            name: 'unallocated'
        },
        {
            type: 'boolean',
            defaultValue: false,
            name: 'selection',
            persist: false
        }
    ]
});