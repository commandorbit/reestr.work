/*
 * File: app/model/modelContragent.js
 *
 * This file was generated by Sencha Architect version 4.2.4.
 * http://www.sencha.com/products/architect/
 *
 * This file requires use of the Ext JS 6.2.x Classic library, under independent license.
 * License of Sencha Architect does not include license for Ext JS 6.2.x Classic. For more
 * details see http://www.sencha.com/license or contact license@sencha.com.
 *
 * This file will be auto-generated each and everytime you save your project.
 *
 * Do NOT hand edit this file.
 */

Ext.define('extapp.model.modelContragent', {
    extend: 'Ext.data.Model',
    alias: 'model.modelcontragen',

    requires: [
        'Ext.data.field.Integer',
        'Ext.data.field.String',
        'Ext.data.field.Boolean'
    ],

    fields: [
        {
            type: 'int',
            name: 'id'
        },
        {
            type: 'string',
            name: 'name'
        },
        {
            type: 'boolean',
            name: 'is_jur'
        },
        {
            type: 'string',
            name: 'inn'
        },
        {
            type: 'string',
            name: 'address'
        },
        {
            type: 'boolean',
            name: 'show_fio'
        },
        {
            type: 'boolean',
            name: 'not_actual'
        }
    ]
});