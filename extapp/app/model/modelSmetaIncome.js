/*
 * File: app/model/modelSmetaIncome.js
 *
 * This file was generated by Sencha Architect version 4.2.4.
 * http://www.sencha.com/products/architect/
 *
 * This file requires use of the Ext JS 6.2.x Classic library, under independent license.
 * License of Sencha Architect does not include license for Ext JS 6.2.x Classic. For more
 * details see http://www.sencha.com/license or contact license@sencha.com.
 *
 * This file will be auto-generated each and everytime you save your project.
 *
 * Do NOT hand edit this file.
 */

Ext.define('extapp.model.modelSmetaIncome', {
    extend: 'Ext.data.Model',

    requires: [
        'Ext.data.field.String',
        'Ext.data.field.Number',
        'Ext.data.field.Boolean'
    ],

    fields: [
        {
            type: 'int',
            name: 'id'
        },
        {
            type: 'int',
            name: 'ver',
            persist: false
        },
        {
            type: 'int',
            name: 'smeta_id'
        },
        {
            type: 'int',
            name: 'fp_article_income_id'
        },
        {
            type: 'int',
            name: 'fin_source_id'
        },
        {
            type: 'string',
            name: 'description'
        },
        {
            type: 'float',
            name: 'amount'
        },
        {
            type: 'float',
            name: 'amount1'
        },
        {
            type: 'float',
            name: 'amount2'
        },
        {
            type: 'float',
            name: 'amount3'
        },
        {
            type: 'float',
            name: 'amount4'
        },
        {
            type: 'float',
            name: 'amount5'
        },
        {
            type: 'float',
            name: 'amount6'
        },
        {
            type: 'float',
            name: 'amount7'
        },
        {
            type: 'float',
            name: 'amount8'
        },
        {
            type: 'float',
            name: 'amount9'
        },
        {
            type: 'float',
            name: 'amount10'
        },
        {
            type: 'float',
            name: 'amount11'
        },
        {
            type: 'float',
            name: 'amount12'
        },
        {
            type: 'float',
            name: 'amount_next'
        },
        {
            type: 'float',
            defaultValue: 20,
            name: 'nds'
        },
        {
            type: 'float',
            calculate: function(data) {
                return data.amount / ((100 + data.nds) / 100) * (data.nds / 100);
            },
            name: 'amount_nds',
            persist: false
        },
        {
            type: 'float',
            calculate: function(data) {
                return data.amount - data.amount / ((100 + data.nds) / 100) * (data.nds / 100);
            },
            name: 'amount_nonds',
            persist: false
        },
        {
            type: 'boolean',
            name: 'is_payment_next_period'
        },
        {
            type: 'boolean',
            calculate: function(data) {
                var month_sum = 0;
                for (var i = 1; i <= 12; i++)
                month_sum += data['amount' + i];
                month_sum += data.amount_next;
                var diff = Math.abs(data.amount - month_sum);
                //console.log(data.amount, month_sum, diff);
                return (diff <= 0.12) ? 1 : 0;
            },
            depends: [
                'amount',
                'amount_next',
                'amount1',
                'amount2',
                'amount3',
                'amount4',
                'amount5',
                'amount6',
                'amount7',
                'amount8',
                'amount9',
                'amount10',
                'amount11',
                'amount12'
            ],
            name: 'month_distributed',
            persist: false
        }
    ]
});