Ext.define('extapp.override.grid.header.Container', {
    override: 'Ext.grid.header.Container',
    
    /**
     * Returns an array of menu items to be placed into the shared menu
     * across all headers in this header container.
     * @return {Array} menuItems
     */
    getMenuItems: function() {
        //alert('extapp.override.grid.header.Container');

        var me = this,
            menuItems = [],
            hideableColumns = me.enableColumnHide ? me.getColumnMenu(me) : null;
 
        if (me.sortable) {
            menuItems = [{
                itemId: 'ascItem',
                text: me.sortAscText,
                iconCls: me.menuSortAscCls,
                handler: me.onSortAscClick,
                scope: me
            }, {
                itemId: 'descItem',
                text: me.sortDescText,
                iconCls: me.menuSortDescCls,
                handler: me.onSortDescClick,
                scope: me
            }];
        }
        
        /*//AVD Мы убрали меню для показа/скрытия колонок
        if (hideableColumns && hideableColumns.length) {
            
            if (me.sortable) {
                menuItems.push({
                    itemId: 'columnItemSeparator',
                    xtype: 'menuseparator'
                });
            }
 
            menuItems.push({
                itemId: 'columnItem',
                text: me.columnsText,
                iconCls: me.menuColsIcon,
                menu: hideableColumns,
                hideOnClick: false
            });
        }
        */
        
        //console.log('me.filters', me.filters);        
        //console.log('me', me);
        /*
        menuItems.push({
            //glyph: 'xf002@FontAwesome', 
            xtype: 'panel',

            //width: 400,
            //height: 150,
            align: 'stretch',
            hideOnClick: false,
            //html: 'Отсель грозить мы будем шведу, <br/>' +
            //    'здесь будет город заложён <br/>назло надменному соседу!',
            items:
            [
                {
                    xtype: 'textfield',
                    reference: 'filtersearch',
                    width: 200,
                    //fieldLabel: 'Поиск',
                    emptyText: 'Поиск',
                    fieldLabel: '',
                    //labelWidth: 45,
                    inputType: 'search',
                    listeners: {
                        change: function(field, newValue, oldValue, eOpts) { //'onFilterChange'
                            console.log(field, newValue, oldValue, eOpts);
                        }
                    }
                    
                },
                
                {
                    xtype: 'checkboxgroup',
                    height: 150,
                    scrollable: 'both',
                    width: 200,
                    fieldLabel: '',
                    labelAlign: 'top',
                    layout: {
                        type: 'vbox',
                        align: 'stretch'
                    },
                    items: [
                        {
                            xtype: 'checkboxfield',
                            height: 16,
                            boxLabel: 'ID',
                            checked: true
                        },
                        {
                            xtype: 'checkboxfield',
                            height: 16,
                            boxLabel: 'Наименование',
                            checked: true
                        },
                        {
                            xtype: 'checkboxfield',
                            height: 32,
                            boxLabel: 'Источник финансирования'
                        },
                        {
                            xtype: 'checkboxfield',
                            height: 16,
                            boxLabel: 'ЦФО',
                            checked: true
                        },
                        {
                            xtype: 'checkboxfield',
                            height: 48,
                            boxLabel: 'Источник финансирования бухгалтерии'
                        },
                        {
                            xtype: 'checkboxfield',
                            height: 16,
                            boxLabel: 'Box Label'
                        },
                        {
                            xtype: 'checkboxfield',
                            height: 16,
                            boxLabel: 'Box Label'
                        },
                        {
                            xtype: 'checkboxfield',
                            height: 16,
                            boxLabel: 'Box Label'
                        }
                    ]
                },
                
            ],            
        });
        */
        return menuItems;
    },
    
    // Render our menus to the first enclosing scrolling element so that they scroll with the grid
    beforeMenuShow: function(menu) {
        var me = this,
            columnItem = menu.child('#columnItem'),
            hideableColumns,
            insertPoint;
 
        // If a change of column structure caused destruction of the column menu item
        // or the main menu was created without the column menu item because it began
        // with no hideable headers. Then create it and its menu now.
        if (!columnItem) {
            hideableColumns = me.enableColumnHide ? me.getColumnMenu(me) : null;
 
            // Insert after the "Sort Ascending", "Sort Descending" menu items if they are present.
            insertPoint = me.sortable ? 2 : 0;
 
            /* // AVD
            if (hideableColumns && hideableColumns.length) {
                menu.insert(insertPoint, [{
                    itemId: 'columnItemSeparator',
                    xtype: 'menuseparator'
                }, {
                    itemId: 'columnItem',
                    text: me.columnsText,
                    iconCls: me.menuColsIcon,
                    menu: {
                        items: hideableColumns
                    },
                    hideOnClick: false
                }]);
            }
            */
        }
 
        //AVD при каждом открытии меню
        /*
        menu.insert(insertPoint, [{
            itemId: 'columnItemSeparator',
            xtype: 'menuseparator'
        }, {
            xtype: 'panel',
            width: 100,
            height: 100,
            hideOnClick: false
        }, {
            itemId: 'columnItemSeparator',
            xtype: 'menuseparator'
        }]);
        */
 
        //NB!!!
        //me.updateMenuDisabledState(me.menu);
        
        // TODO: rendering the menu to the nearest overlfowing ancestor has been disabled
        // since DomQuery is no longer available by default in 5.0
        //        if (!menu.rendered) {
        //            menu.render(this.el.up('{overflow=auto}') || document.body);
        //        }
    },    
    
});