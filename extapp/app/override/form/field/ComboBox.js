Ext.define('extapp.override.form.field.ComboBox', {
    override: 'Ext.form.field.ComboBox',
    
    //matchFieldWidth: false,
    minChars: 1,
    autoLoadOnValue: true,
    valueField: 'id',
    displayField: 'name',
    forceSelection: true,    
    queryMode: 'local',
    anyMatch: true,
    listeners: 
    {
        expand: function(field, eOpts) 
        {
            //alert('expand');
        }
    }    
});