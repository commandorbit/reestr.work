Ext.define('extapp.override.data.reader.Json', {
    override: 'Ext.data.reader.Json',
    messageProperty: 'message',
    rootProperty: 'records'    
});