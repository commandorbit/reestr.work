/*
 * File: app/store/storeZFO1.js
 *
 * This file was generated by Sencha Architect version 4.2.4.
 * http://www.sencha.com/products/architect/
 *
 * This file requires use of the Ext JS 6.2.x Classic library, under independent license.
 * License of Sencha Architect does not include license for Ext JS 6.2.x Classic. For more
 * details see http://www.sencha.com/license or contact license@sencha.com.
 *
 * This file will be auto-generated each and everytime you save your project.
 *
 * Do NOT hand edit this file.
 */

Ext.define('extapp.store.storeZFO1', {
    extend: 'Ext.data.Store',

    requires: [
        'extapp.model.modelZFO',
        'Ext.data.proxy.Ajax',
        'Ext.data.reader.Json'
    ],

    constructor: function(cfg) {
        var me = this;
        cfg = cfg || {};
        me.callParent([Ext.apply({
            storeId: 'storeZFO1',
            model: 'extapp.model.modelZFO',
            proxy: {
                type: 'ajax',
                url: '/ext_php/gettable.php?table=zfo&use_in_editor=1',
                reader: {
                    type: 'json'
                }
            }
        }, cfg)]);
    }
});