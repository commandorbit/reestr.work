# theme4/sass/etc

This folder contains miscellaneous SASS files. Unlike `"theme4/sass/etc"`, these files
need to be used explicitly.
