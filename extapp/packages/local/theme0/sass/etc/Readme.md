# theme0/sass/etc

This folder contains miscellaneous SASS files. Unlike `"theme0/sass/etc"`, these files
need to be used explicitly.
