# theme1/sass

This folder contains SASS files of various kinds, organized in sub-folders:

    theme1/sass/etc
    theme1/sass/src
    theme1/sass/var
