# theme-triton-f2b8e3d5-e213-43c5-9e33-064e484d26c8/sass

This folder contains SASS files of various kinds, organized in sub-folders:

    theme-triton-f2b8e3d5-e213-43c5-9e33-064e484d26c8/sass/etc
    theme-triton-f2b8e3d5-e213-43c5-9e33-064e484d26c8/sass/src
    theme-triton-f2b8e3d5-e213-43c5-9e33-064e484d26c8/sass/var
