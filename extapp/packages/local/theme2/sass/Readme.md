# theme2/sass

This folder contains SASS files of various kinds, organized in sub-folders:

    theme2/sass/etc
    theme2/sass/src
    theme2/sass/var
