
Ext.define('Ext.grid.filters.filter.myList', {
    extend: 'Ext.grid.filters.filter.List',
    alias: 'grid.filter.mylist',
 
    createMenuItems: function(store) {
        var me = this,
            menu = me.menu,
            len = store.getCount(),
            contains = Ext.Array.contains,
            itemDefaults, record, gid, idValue, idField, labelValue, labelField,
            i, processed;
 
        // B/c we're listening to datachanged event, we need to make sure there's a menu.
        if (len && menu) {
            itemDefaults = me.getItemDefaults();
            menu.suspendLayouts();
            menu.removeAll(true);
            gid = me.single ? Ext.id() : null;
            idField = me.idField;
            labelField = me.labelField; 
            processed = [];
            var menuItems = [];
            var panelItems = [];
            for (i = 0; i < len; i++) {
                record = store.getAt(i);
                idValue = record.get(idField);
                labelValue = record.get(labelField);
 
                // Only allow unique values.
                if (labelValue === null || contains(processed, idValue)) {
                    continue;
                }
 
                panelItems.push({
                    xtype: 'checkboxfield',
                    height: 16,
                    inputValue: idValue,
                    boxLabel: labelValue,
                    checked: false,
                    handler: me.onCheckChange,
                    scope: me
                });
            }
            menuItems.push({
                //glyph: 'xf002@FontAwesome', 
                xtype: 'panel',
                //width: 400,
                //height: 150,
                //resizable:true,
                align: 'begin',
                layout: {
                    type: 'vbox',
                    align: 'stretch'
                },
                hideOnClick: false,
                //html: 'Отсель грозить мы будем шведу, <br/>' +
                //    'здесь будет город заложён <br/>назло надменному соседу!',
                items: 
                [
                    {
                        xtype: 'textfield',
                        reference: 'filtersearch',
                        //height: 30,
                        //minHeight: 30,
                        //flex:1,
                        emptyText: 'Поиск фильтров',
                        //glyph: 'xf002@FontAwesome', 
                        fieldLabel: '',
                        //labelWidth: 45,
                        inputType: 'search',
                        listeners: {
                            change: function(field, newValue, oldValue, eOpts) { //'onFilterChange'
                                //console.log(field, newValue, oldValue, eOpts);
                                var searchString = newValue.toLowerCase();
                                var panel = me.menu.query("#checkboxgroup")[0];
                                var items = panel.items.items;
                                //console.log(items);
                                //menu.suspendLayout = true;
                                Ext.suspendLayouts();
                                for (var i = 0; i < items.length; i++) {
                                    //console.log(items[i]);
                                    value = items[i].boxLabel.toString().toLowerCase();
                                    items[i].setHidden(searchString && value.indexOf(searchString) < 0);
                                }
                                //menu.suspendLayout = false;
                                //menu.doLayout();
                                Ext.resumeLayouts();
                            }
                        }                    
                    },
                    {
                        xtype: 'checkboxfield',
                        itemId: 'checkboxall',
                        //height: 16,
                        margin: '-4 0 0 4',
//                        padding: '0 0 0 10',
                        inputValue: 0,
                        boxLabel: 'Выбрать все',
                        checked: false,
                        listeners: {
                            change: function(field, newValue, oldValue, eOpts) {
                                var panel = me.menu.query("#checkboxgroup")[0];
                                var items = panel.items.items;
                                Ext.suspendLayouts();
                                for (var i = 0; i < items.length; i++) {
                                    //console.log(items[i]);
                                    items[i].setValue(newValue);
                                }
                                Ext.resumeLayouts();
                            }
                        },                    
                        scope: me
                    },
                    
                    { 
                        xtype: 'menuseparator'
                    },
                    
                    {
                        xtype: 'checkboxgroup',
                        itemId: 'checkboxgroup',
                        flex:1,
                        //height: 250,
                        margin:"-3 0 4 0",
                        maxHeight: Math.max(200, document.body.clientHeight - 200), //170, //350,
                        scrollable: 'both',
                        //width: 200,
                        fieldLabel: '',
                        labelAlign: 'top',
                        layout: {
                            type: 'vbox',
                            align: 'stretch'
                        },
                        items: panelItems
                    },

                ],            
            });
            menu.add(menuItems);
            //menu.resizable = true;
            menu.resumeLayouts(true);
            /*
            for (i = 0; i < len; i++) {
                record = store.getAt(i);
                idValue = record.get(idField);
                labelValue = record.get(labelField);
 
                // Only allow unique values.
                if (labelValue == null || contains(processed, idValue)) {
                    continue;
                }
 
                processed.push(labelValue);
 
                // Note that the menu items will be set checked in filter#activate()
                // if the value of the menu item is in the cfg.value array.
                menu.add(Ext.apply({
                    text: labelValue,
                    group: gid,
                    value: idValue,
                    checkHandler: me.onCheckChange,
                    scope: me
                }, itemDefaults));
            }
 
            menu.resumeLayouts(true);
            */
        }
    },
 
    /**
     * @private
     * Template method that is to set the value of the filter.
     */
    setValue: function() {
        var me = this,
            //items = me.menu.items,
            value = [],
            i, len, checkItem;
        
        var panel = me.menu.query("#checkboxgroup")[0];
        var items = panel.items;
        
        // The store filter will be updated, but we don't want to recreate the list store
        // or the menu items in the onDataChanged listener so we need to set this flag.
        // It will be reset in the onDatachanged listener when the store has filtered.
        me.preventDefault = true;
 
        for (i = 0, len = items.length; i < len; i++) {
            checkItem = items.getAt(i); 
            if (checkItem.checked) {
                value.push(checkItem.inputValue);
                if (i === 0 && !checkItem.inputValue.length) { // для пустой строки добавим null
                    value.push(null);
                }
            }
        }
 
        // Only update the store if the value has changed
        if (!Ext.Array.equals(value, me.filter.getValue())) {
            me.filter.setValue(value);
            len = value.length; 
            if (len && me.active) {
                me.updateStoreFilter();
            }
            else {
                me.setActive(!!len);
            }
        }
    }, 
});