<?php
error_reporting(E_ALL | E_STRICT);
require_once $_SERVER['DOCUMENT_ROOT'] . '/init.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/modules/asu_pfhd_sender.php';

//AVD
$srcxml = '';
if (isset($_POST['srcxml'])) $srcxml = $_POST['srcxml'];

$max_size = 2*1014;
if (isset($_REQUEST['is_partly'])) {
	$max_size = request_val('size_part',$max_size);
}
define('MAX_SIZE',$max_size * 1024);

/*if(isset($_POST['size_part'])) {
	var_dump(MAX_SIZE);
	die();
}*/

$response_key = isset($_POST['key']) ? $_POST['key'] : null;
$loading_types = array(
	array('value' => 'finActivityPlan2017', 'name' => 'ПФХД 2017'), 
	array('value' => 'finActivityPlan', 'name' => 'ПФХД'), 
	array('value' => 'extraValue', 'name' => 'Доп. показатели')
);
$value_types = array(
	'extraValue'=>'extra.xml'
);

//AVD
function creaateSrcXML($vid, $method) {
	global $srcxml;
	$ver_id = sql_escape($vid);
	if($method == 'extraValue') {
		$xml = Pfhd::extraToXML($ver_id);
	} else if($method == 'finActivityPlan') {
		$xml = Pfhd::toXml2016($ver_id);
	} else if($method == 'finActivityPlan2017') {
		$xml = Pfhd::toXml2017($ver_id);
	}
	$srcxml = $xml;
}

//AVD
function sendCreated($method) {
	global $srcxml;
	return send_xml($srcxml,$method,$method.'.xml');
}

function pfhdRequest($vid, $method) {
	//AVD
	global $srcxml;
	creaateSrcXML($vid, $method);
	$xml = $srcxml;
	//
	return send_xml($xml,$method,$method.'.xml');
}

$loading_answer = $loading_result = 'Ответ сервера АСУ ПФХД';
if (isset($_POST['send']) && isset($_POST['ver_id'])) {
	$ver_id = $_POST['ver_id'];
	if($_POST['method'] == 'extraValue')  $ver_id = 0;
	$answerArray = pfhdRequest($_POST['ver_id'], $_POST['method']);
	$loading_answer = implode(' ', $answerArray);	
	$response_key = getKey(end($answerArray));
//AVD
} else if (isset($_POST['create']) && isset($_POST['ver_id'])) {
	$ver_id = $_POST['ver_id'];
	if($_POST['method'] == 'extraValue')  $ver_id = 0;
	creaateSrcXML($_POST['ver_id'], $_POST['method']);
} else if (isset($_POST['sendcreated']) && $srcxml) {
	$answerArray = sendCreated($_POST['method']);
	$loading_answer = implode(' ', $answerArray);	
	$response_key = getKey(end($answerArray));	
//
} else if(isset($_POST['get_xml']) && isset($_POST['ver_id'])) {
	$ver_id = sql_escape($_POST['ver_id']);
	if($_POST['method'] == 'extraValue')  {
		$ver_id = 0;
		Pfhd::extraToXML2016($ver_id, true);
	} else if($_POST['method'] == 'finActivityPlan') {
		Pfhd::toXml($ver_id, true);
	//AVD
	} else if($_POST['method'] == 'finActivityPlan2017') {
		Pfhd::toXml2017($ver_id, true);
	}
	exit();
} else if(isset($_POST['get_xls']) && isset($_POST['ver_id'])) {
	$ver_id = sql_escape($_POST['ver_id']);	
	if($_POST['method'] == 'extraValue')  {
		$ver_id = 0;
		//$html = Pfhd::toHTML($ver_id, Pfhd::getExtraRows());
	} else if($_POST['method'] == 'finActivityPlan') {
		//Pfhd::fillTmpPfhdTable($ver_id);
		//$html = Pfhd::toHTML($ver_id, Pfhd::getPfhdRows(2016));
	//AVD
	} else if($_POST['method'] == 'finActivityPlan2017') {
		//Pfhd::fillTmpPfhdTable($ver_id);
		//$html = Pfhd::toHTML($ver_id, Pfhd::getPfhdRows(2017));
	}
	
	header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
	header('Content-Disposition: attachment;filename="'.$_POST['method'] . '_' . date('dmY').'.xls"');
	header('Cache-Control: max-age=0');
	echo $html;
	exit();
}

$key = request_val('key','');
if (isset($_POST['check_key'])) {
	$loading_result = check_key($key);
}

function getKey($xml_str) {
	libxml_use_internal_errors(true);
	$xml = simplexml_load_string($xml_str);
	if ($xml === false) {
		header('Content-Type: application/xml');
		header('Content-Disposition: attachment;filename="'.$_POST['method'] . '_' . date('dmY').'.xml"');
		header('Cache-Control: max-age=0');
		echo $xml_str;
		exit();
	}
	$key = ($xml->Key[0]) ? $xml->Key[0] : null;
	return $key;	
}

$title = "Загрузка в АСУ ПФХД";
require_once $_SERVER['DOCUMENT_ROOT'] . '/template/header.php';
?>

<style>
	.info textarea {
		width: 100%;
		height: 105px;
	}
	
	.info input[type="submit"] {
		margin: 10px 0;
	}
	
	.form-group {
		margin: 0 0 10px 0;
		padding: 0 5px 0 0;
		visibility: visible;
	}
</style>
<link rel="stylesheet" type="text/css" href="/css/background<?=$_SESSION["palette"]?>.css">
<div style="text-align: center;">
<div class="php-dialog content-t" data-moveable="1" style="width:700px">
<h1><?=$title?></h1>
<h4><a href="/tedit.php?t=asu_pfhd_load_log" target="_blank">Журнал загрузок</a></h4>
<br>
	<form method="post">
		<div class="form-group">
			<label>
				Тип загрузки:
				<select name="method" id="method" onchange="verSwitch(this)">
					<?php foreach($loading_types as $type): ?>
						<option value="<?=$type['value']?>"><?=$type['name']?></option>
					<?php endforeach; ?>
				</select>
			</label>
		</div>
		<div class="form-group">
			<label>
				Выбрать версию:
				<select name="ver_id" id="version">
					<?php foreach(Pfhd::getVersions() as $version): ?>
						<option value="<?=$version['id']?>"><?=$version['name']?></option>
					<?php endforeach; ?>
				</select>
			</label>
		</div>
		<div style="text-align: left">
			<input type="submit" name="send" value="Передать в АСУ ПФХД" />
			<a href="#" class="decor-link" onclick="showLoadParams(); return false;">Дополнительно</a>
			<div class="load-params" <? if (!(isset($_POST['create']) || isset($_POST['sendcreated']))) echo 'hidden'; ?> >
				<input type="submit" name="create" value="Сформировать XML" />
				<input type="submit" name="sendcreated" value="Передать сформированный XML" />
				<input type="checkbox" name="is_partly" onchange="enableSizePart();" value="1"> Передать данные в АСУ ПФХД по <input  name="size_part" type="text" disabled style="width: 70px;text-align:right" value="<?=MAX_SIZE/1024?>"> КБ
				<div class="info answer">
					<p><b>XML:</b></p>
					<textarea name="srcxml"><?=$srcxml?></textarea>
				</div>
			</div>
			<p><b>Скачать:</b></p>
			<input type="submit" name="get_xml" value="Скачать в формате XML" />
			<input type="submit" name="get_xls" value="Скачать в формате XLS" />
		</div>
	</form>
	
	<div class="info answer">
		<p><b>Ответ от сервера:</b></p>
		<textarea id="answer-str"><?=$loading_answer ?></textarea>
	</div>
	<div class="info result">
		<form method="post">
			<input required type="text" name="key" value="<?php if(!is_null($response_key)) echo $response_key; ?>" placeholder="Введите ключ ответа сервера вида: a99a24be-68b6-4846-990e-1ba0a3cec401">
			<input type="submit" name="check_key" value="Проверить результат загрузки">
		</form>
		<textarea id="result-str"><?=$loading_result?></textarea>
	</div>
		
</div>
</div>
<script>
    function enableSizePart() {
        var check = document.querySelector('input[name="is_partly"]');
        var inp = document.querySelector('input[name="size_part"]');
        if (inp && check) {
            inp.disabled = !check.checked;
        }
    }
	function verSwitch(el) {
		var types = {'finActivityPlan': 'visible', 'extraValue': 'hidden'};
		var value = el.value;
		var version_control = document.querySelector("#version");
		version_control.parentNode.parentNode.style.visibility = types[value];
	} 
	
	function showLoadParams() {
		var loadParams = document.querySelector('.load-params');
		$(loadParams).toggle();
	}
</script>