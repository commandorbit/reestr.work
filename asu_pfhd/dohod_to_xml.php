<?php 
// Сейчас не используется
include $_SERVER['DOCUMENT_ROOT'] . '/init.php';

$year = date('Y');
$beg_date = date('Y-m-d',mktime(0,0,0,1,1,$year));
$end_date = date('Y-m-d',mktime(0,0,0,12,31,$year));
// Переделать pfhd_article в зависимости от года
$sql = "select pfhd.asu_pfhd_code as pfhd_code,pfhd.name as pfhd_name,kind.code activity_code,kind.full_name activity_name, sum(s) as sum from 
plan_dohod p 
inner join fp_article a on p.fp_article_id = a.id
inner join pfhd_article pfhd on a.pfhd_article_id = pfhd.id
inner join fin_source fs on p.fin_source_id = fs.id
inner join booker_fin_source bfs on fs.booker_fin_source = bfs.code
inner join asu_pfhd_activity_kind kind on bfs.asu_pfhd_activity_kind_id = kind.id
group by pfhd.asu_pfhd_code, kind.code";



header('Content-Type: text/xml');
header('Content-Disposition: attachment;filename="План доходов.xml"');
header('Cache-Control: max-age=0');

$org = get_org_rekvizits();
$rows = sql_rows($sql);

echo '<?xml version="1.0" encoding="UTF-8"?>'."\n";
require (dirname(__FILE__) . '/dohod/dohod_header.xml');	
foreach ($rows as $row) {
	require (dirname(__FILE__) . '/dohod/dohod_item.xml');	
}	
require (dirname(__FILE__) . '/dohod/dohod_footer.xml');	

function get_org_rekvizits() {
    $org = array();
    $rows = sql_rows("select * from param");
    foreach ($rows as $row) {
        $org[$row['name']]=$row['value'];
    }
    return $org;
}
?>