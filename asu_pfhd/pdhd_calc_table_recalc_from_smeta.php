<?php
define('UTF-8',1);
error_reporting(-1);

include $_SERVER['DOCUMENT_ROOT'].'/init.php';
require_once $_SERVER['DOCUMENT_ROOT'].'/modules/save.php';
require_once __DIR__ .'/pfhd_calc_table_func.php';

$YEARS = [2019,2020,2021];
//$table_id = request_numeric_val('t_id',16);
//$table_ids = [16,18,33,17];

define('PFHD_ARTICLE_ID','pfhd_article_2019_id');

$table_ids = _table_ids();


function _table_ids() {
	$table_ids = [];
	$rows = sql_rows("select id from pfhd_calc_table where need_send_pfhd = 1");

	foreach($rows as $tr) {
		$table_ids[] = $tr['id'];
	}
	return $table_ids;
		
}

foreach ($table_ids as $table_id) {
	foreach($YEARS as $y){
		$rows = sql_rows("select * from pfhd_calc_table_row where pfhd_calc_table_id=$table_id AND fp_article_ids is not null");
		foreach($rows as $r) {
			recalc_rows($table_id,$y,$r['id'],$r['fp_article_ids'],$r['natural_indicator_id']);
		}
		/*
		$rows = sql_rows("select distinct d.id from pfhd_calc_table_detail d inner join pfhd_calc_table_detail_dict_show ds on d.id=pfhd_calc_table_detail_id 
				where d.pfhd_calc_table_id=$table_id AND ds.fp_article_id is not null");

		foreach($rows as $r) {
			recalc_details($table_id,$r['id'],$y);
		}
		*/
		recalc_itogo($table_id,$y);
	}
}
echo "OK!";

function recalc_itogo($table_id,$y) {
	$pfhd_article_id = sql_get_value(PFHD_ARTICLE_ID,'pfhd_calc_table',"id=$table_id");
	$sql = 	"SELECT asu_pfhd_activity_kind_id kind, SUM(ss.amount) amount 
		FROM smeta_sostav ss INNER JOIN  smeta s ON ss.smeta_id = s.id 
		INNER JOIN fp_article fp ON ss.fp_article_id = fp.id 
		INNER JOIN fin_source fs ON ss.fin_source_id = fs.id 
		INNER JOIN booker_fin_source bf ON  fs.booker_fin_source = bf.code
		WHERE s.fp_year_id=$y AND fp.".PFHD_ARTICLE_ID."=$pfhd_article_id
		GROUP BY bf.asu_pfhd_activity_kind_id
		";
	$rows = sql_rows($sql);
	sql_query("DELETE FROM pfhd_calc_table_itogo WHERE pfhd_calc_table_id=$table_id and y=$y");
	
	$calc_fin_sources = [2=>'vne_budget',4=>'budget',6=>'budget_capital',7=>'oms',5=>'budget_special'];
	$vals = ['pfhd_calc_table_id'=>$table_id,'y'=>$y];
	foreach($rows as $r) {
		$kind = $r['kind'];
		if (isset($calc_fin_sources[$kind])) {
			$vals[$calc_fin_sources[$kind]] = $r['amount'];
		}
	}
	$sql = "INSERT INTO pfhd_calc_table_itogo(" .implode(',',array_keys($vals)) . ") VALUES (" . implode(',',$vals) .")";
	sql_query($sql);
}
function my_throw_error_handler($errno, $message){ 
	throw new ErrorException($message); 
}

function recalc_rows($table_id,$y,$row_id,$fp_article_ids,$natural_indicator_id) {
	$sql = 	"SELECT SUM(ss.amount) amount ,SUM(ss.quantity) quantity, SUM(ss.price) price
		FROM smeta_sostav ss INNER JOIN  smeta s ON ss.smeta_id = s.id
		WHERE ss.fp_article_id IN ($fp_article_ids) AND s.fp_year_id=$y";
	if ($natural_indicator_id)
		$sql .= " AND ss.natural_indicator_id IN ($natural_indicator_id)";
	$smeta_vals = _smeta_vals($sql);
	
	foreach($smeta_vals as $key=>$val) {
		$$key = $val;
	}
	$vals = pfhd_calc_table_data_row($table_id,$y,$row_id);
//	$vals = ['r'=>$n_row,'table_id'=>$table_id,'y'=>$y];
	if ($vals) {
		foreach($vals as $fld=>$v) {
			$$fld = $v;
		}
		$formula = sql_get_value('recalc_formula','pfhd_calc_table',"id=$table_id");
		if ($formula) $formula=json_decode($formula,true);
		foreach($formula as $fld=>$formula) {
			$val = null;
			$error = false;
			if ($smeta_vals['amount']>0) {
				try {
					set_error_handler('my_throw_error_handler');
					$str = "\$val=round($formula,2);";
					eval ($str);
				} catch (ErrorException $e) {
					echo "" . $e->getMessage(). " error in formula: '$str' <br>\n";
					$error = true;
				} 
			}
			$vals[$fld]=$val;
		}
		save_pfhd_calc_table_val($vals);
	}
}

function _smeta_vals($sql) {
	return sql_rows($sql)[0];
}


function recalc_details($table_id,$detail_id,$y) {
	$detail_code = sql_get_value('code','pfhd_calc_table_detail',"id=$detail_id");
	$rows = sql_rows("select * from pfhd_calc_table_detail_dict_show WHERE pfhd_calc_table_detail_id=$detail_id");
	foreach($rows as $r) {
		$fp_article_id = $r['fp_article_id'];
		$dict_id = $r['pfhd_calc_table_dict_id'];
		$fp_object_id = $r['fp_object_id'];
		$natural_indicator_id = $r['natural_indicator_id'];
		$sql = "SELECT SUM(ss.amount) amount ,SUM(ss.quantity) quantity, SUM(ss.price) price
			FROM smeta_sostav ss INNER JOIN  smeta s ON ss.smeta_id = s.id
			WHERE ss.fp_article_id IN ($fp_article_id) AND s.fp_year_id=$y";
			
		if ($fp_object_id) $sql .= " AND ss.fp_object_id=$fp_object_id";
		if ($natural_indicator_id) $sql .= " AND ss.natural_indicator_id=$natural_indicator_id"; 
		
		$smeta_vals = sql_rows($sql)[0];
		foreach($smeta_vals as $key=>$val) {
			$$key = $val;
		}
		
		$vals = ['dict_id'=>$dict_id,'detail_code'=>$detail_code,'table_id'=>$table_id,'y'=>$y,'level1_dict_id'=>$r['detail_level1_id']];
		$formula = sql_get_value('recalc_formula','pfhd_calc_table',"id=$table_id");
		if ($formula) $formula = json_decode($formula,true);
		foreach($formula as $fld=>$formula) {
			$val = null;
			$error = false;
			if ($smeta_vals['amount']>0) {
				try {
					$str = "\$val=round($formula,2);";
					eval ($str);
				} catch (Exception $e) {
					echo "error in formula '$str'";
					$error = true;
				}
			}	
			$vals[$fld]=$val;
		}
		save_pfhd_calc_table_val($vals);
	}
}