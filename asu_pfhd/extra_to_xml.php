<?php
include $_SERVER['DOCUMENT_ROOT'] . '/init.php';

$year = date('Y');

$sql = "SELECT ex.* 
FROM asu_pfhd_dop_pokaz ex 
ORDER BY ex.id";
$rows = sql_rows($sql);

header('Content-Type: text/xml');
header('Content-Disposition: attachment;filename="extra.xml"');
header('Cache-Control: max-age=0');
echo '<?xml version="1.0"?>'."\r\n";

$org = get_org_rekvizits();

require (dirname(__FILE__) . '/extra/extra_header.xml');	
foreach ($rows as $row) {
	require (dirname(__FILE__) . '/extra/extra_body.xml');	
}	
require (dirname(__FILE__) . '/extra/extra_footer.xml');	

function get_org_rekvizits() {
    $org = array();
    $rows = sql_rows("select * from param");
    foreach ($rows as $row) {
        $org[$row['name']]=$row['value'];
    }
    return $org;
}
?>