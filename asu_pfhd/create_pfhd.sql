TRUNCATE TABLE pfhd;

INSERT INTO pfhd (y,sum,fp_article_id,pfhd_article_id,zfo_id,pfhd_ver_id,booker_fin_source )
SELECT smeta.fp_year_id, Sum(amount*percent_exbudjet/100) AS sum, ss.fp_article_id, fp_article.pfhd_article_2018_id, smeta.zfo_id, 1 AS pfd_ver_id, 2 AS booker_fin_source
FROM smeta_sostav ss INNER JOIN smeta ON ss.smeta_id = smeta.id 
INNER JOIN fp_article ON ss.fp_article_id = fp_article.id
WHERE (smeta.fp_year_id>=2018 And smeta.fp_year_id<=2020) AND percent_exbudjet<>0 AND 
fp_article.pfhd_article_2018_id Is Not Null
GROUP BY smeta.fp_year_id, ss.fp_article_id, fp_article.pfhd_article_2018_id, smeta.zfo_id;


INSERT INTO pfhd (y,sum,fp_article_id,pfhd_article_id,zfo_id,pfhd_ver_id,booker_fin_source )
SELECT smeta.fp_year_id, Sum(amount*percent_budjet/100) AS sum, ss.fp_article_id, fp_article.pfhd_article_2018_id, smeta.zfo_id, 1 AS pfd_ver_id, 4 AS booker_fin_source
FROM smeta_sostav ss INNER JOIN smeta ON ss.smeta_id = smeta.id 
INNER JOIN fp_article ON ss.fp_article_id = fp_article.id
WHERE (smeta.fp_year_id>=2018 And smeta.fp_year_id<=2020) AND percent_budjet<>0 AND 
fp_article.pfhd_article_2018_id Is Not Null
GROUP BY smeta.fp_year_id, ss.fp_article_id, fp_article.pfhd_article_2018_id, smeta.zfo_id;


INSERT INTO pfhd (y,sum,fp_article_id,pfhd_article_id,zfo_id,booker_fin_source,pfhd_ver_id )
SELECT smeta.fp_year_id, sum(ss.amount), ss.fp_article_id, fp_article.pfhd_article_2018_id, smeta.zfo_id,fin_source.booker_fin_source,  1 AS pfhd_ver_id
FROM smeta_sostav ss INNER JOIN smeta ON ss.smeta_id = smeta.id
INNER JOIN fp_article ON ss.fp_article_id = fp_article.id
INNER JOIN fin_source ON smeta.fin_source_id = fin_source.id
WHERE (smeta.fp_year_id>=2018 And smeta.fp_year_id<=2020) AND fp_article.pfhd_article_2018_id Is Not Null
GROUP BY smeta.fp_year_id, ss.fp_article_id, fp_article.pfhd_article_2018_id, fin_source.booker_fin_source, smeta.zfo_id;


INSERT INTO pfhd (y,sum,fp_article_id,pfhd_article_id,zfo_id,booker_fin_source,pfhd_ver_id )
SELECT smeta.fp_year_id, sum(ss.amount), ss.fp_article_id, fp_article.pfhd_article_2018_id, smeta.zfo_id,fin_source.booker_fin_source,  1 AS pfhd_ver_id
FROM smeta_income si INNER JOIN smeta ON si.smeta_id = smeta.id
INNER JOIN fp_article_income fp ON si.fp_article_id = fp.id
INNER JOIN fin_source ON smeta.fin_source_id = fin_source.id

