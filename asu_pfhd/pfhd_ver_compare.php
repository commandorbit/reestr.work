<?php

$title = 'Выберите версию ПФХД';

include $_SERVER['DOCUMENT_ROOT'] . '/init.php';
include $_SERVER['DOCUMENT_ROOT'] . '/template/header.php';

$years = sql_to_assoc("SELECT id, name FROM plan_period ORDER BY id");

$versions_2016 = sql_to_assoc("SELECT id, name FROM stat_pfhd_ver WHERE plan_period_id=2016 ORDER BY id DESC");
$versions_2016_v2 = $versions_2016;
$versions_2016 = [Pfhd::CURRENT_VERSION_2016=>'Текущая 2016']+$versions_2016;

$versions_2017 = sql_to_assoc("SELECT id, name FROM stat_pfhd_ver WHERE plan_period_id=2017 ORDER BY id DESC");
$versions_2017_v2 = $versions_2017;
$versions_2017 = [Pfhd::CURRENT_VERSION_2017=>'Текущая 2017']+$versions_2017;

//$versions_2018 = sql_to_assoc("SELECT id, name FROM pfhd_ver WHERE plan_period_id=2018 ORDER BY id DESC");
$versions = [];
$rows = sql_rows("SELECT plan_period_id y, id, name FROM pfhd_ver WHERE plan_period_id>=2018 ORDER BY plan_period_id ASC,id DESC");
foreach($rows as $r) {
	$versions[$r['y']][$r['id']] = $r['name'];
}


require_once SHARED_PHP_PATH . '/controls/controls.php';
$controls = new controls;
?>

<div class="ver_check">
	<h3>Для продолжения необходимо выбрать верси<span id="suffix">ю</span> ПФХД:</h3>
	<form method="GET" action="/pivot/pivot.php">
		<input type="hidden" name="type" value="pfhd">
		<label class="ver_select">
			<p>Период:</p>
			<?php $controls->print_select('y',$years,2018); ?>
		</label>
		
		<label class="ver_select">
			<p>Первая версия:</p>
			<?php $controls->print_select('first_ver',[],''); ?>
		</label>
		<label class="ver_select hidden_field">
			<p>Вторая версия:</p>
			<?php $controls->print_select('second_ver',[],''); ?>
		</label>

		<input type="submit" name="submit" value="Продолжить">
		<p>
			<input type="checkbox" id="compare" onchange="showCompareControl()"> Сравнить версии
			<input type="checkbox" id="rashod_sign" name="rashod_sign" checked="checked"> Расходы со знаком минус
			<input type="checkbox" name="pfhd_article_y" value="2017">		Использовать  статьи ПФХД 2017
		</p>
	</form>
	<?php if (is_admin()) : ?>
		<p style="padding-top: 50px;">
<!--			<input type="button" id="create_version_2017_btn" value="Создать версию 2017"> -->
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			<input type="button" id="new_version" value="Создать версию">
		</p>
	<?php endif ?>
</div>

<?php if (is_admin()) : ?>
<!--
<div id="create_version_2017" class="create_version_div">
	<?php 
	/*
	$plan_dohod_vers = sql_to_assoc("SELECT id, name FROM plan_dohod_ver WHERE y='2017' ORDER BY name");
	$plan_dohod_ver_id_default = sql_get_value("MAX(id)",'plan_dohod_ver',"y='2017'");
	$controls->print_input_hidden('plan_period_id',2017);
	$controls->print_select_label('plan_dohod_ver_id',$plan_dohod_vers,$plan_dohod_ver_id_default,'Версия плана доходов');
	$controls->print_input_label('ver_name','','Название версии');
	$controls->print_input_label('prim','','Примечание');
	*/
	?>
</div>
-->

<div id="create_version" class="create_version_div">
	<?php 
	$controls->print_input_label('ver_name','','Название версии');
	$controls->print_input_label('prim','','Примечание');
	?>
</div>

<script type="text/javascript">
	$(document).ready(function(){
//		$('#create_version_2017_btn').click(function(){createVersionDialog(2017);});
		$('#new_version').click(function(){createVersionDialog();});
/*
		$('select[name="plan_period_id"]').change(function(){
			var planPeriodId = $(this).val()
			$.ajax({
				url: '/modules/ajax.php?func=get_plan_dohod_options&arg1='+planPeriodId
			}).done(function(htmlOptions){
				$('select[name="plan_dohod_ver_id"]').html(htmlOptions);
			});
		});
*/		

		function createVersionDialog() {
			var div = $('#create_version');
			var year = $('#id_y').val(); 
			div.dialog({
				title: 'СОЗДАТЬ ВЕРСИЮ ПФХД '+year,
				minWidth: 500,
				maxWidth: 500,
				minHeight: 100,
				modal: true,
				close: function() {
					// code onclose
					$('.create_version_div input[type="text"]').val('');
				},
				autoOpen: true,
				buttons : [
					{
						text: 'Отмена',
						id: 'dialogCancelBtn',
						click: function() {
							$(this).dialog("close");
						}
					},
					{
						text: 'Сохранить',
						click: function() {
							// code for saving

							var planPeriodId = year; //div.find('input[name="plan_period_id"]').val();
							var planDohodVer = div.find('select[name="plan_dohod_ver_id"]').val();
							var verName = div.find('input[name="ver_name"]').val();
							var prim = div.find('input[name="prim"]').val();

							if (!verName) {
								alert('Не введено название версии!');
							} else {
								var saveUrl = '/asu_pfhd/save_pfhd_ver.php?ver_name='+verName+'&plan_period_id='+planPeriodId+'&prim='+prim;								
								location.href=saveUrl;
							}
						}
					}
				]
			});
		}
	});
</script>

<?php endif ?>
	

<script type="text/javascript">
	var options2016 = <?=json_encode($versions_2016,JSON_UNESCAPED_UNICODE);?> || [];
	var options2016_v2 = <?=json_encode($versions_2016_v2,JSON_UNESCAPED_UNICODE);?> || [];
	var options2017 = <?=json_encode($versions_2017,JSON_UNESCAPED_UNICODE);?> || [];
	var options2017_v2 = <?=json_encode($versions_2017_v2,JSON_UNESCAPED_UNICODE);?> || [];

	var options = <?=json_encode($versions,JSON_UNESCAPED_UNICODE);?> || [];

	$(document).ready(function(){
		var year = $('select[name="y"]').val();
		onChangeYear(year);

		$('select[name="y"]').change(function(){
			var year = $(this).val();
			onChangeYear(year);
		});

		function onChangeYear(year) {
			if (year >= 2018) {
				// одинаковые опции в select
				fillSel('first_ver',options[year]);
				fillSel('second_ver',options[year]);
			} else  {
				// 2016 и 2017 разные наборы опций
				if (year == 2016) {
					fillSel('first_ver',options2016,'-2016');
					fillSel('second_ver',options2016_v2);
				} else if (year == 2017) {
					fillSel('first_ver',options2017,'-2017');
					fillSel('second_ver',options2017_v2);
				}
			}
		} 

		function fillSel(selName,options,defaultVal) {
			clearSel(selName);
			$.each(options,function(id,name){
				selAddOption(selName,id,name);
			});
			if (defaultVal !== undefined) {
				$('select[name="'+selName+'"]').val(defaultVal);
			}
		}

		function clearSel(selName) {
			$('select[name="'+selName+'"] option').each(function(){
				$(this).remove();
			});
		}

		function selAddOption(selName,value,text) {
			$('select[name="'+selName+'"]').append('<option value="'+value+'">'+text+'</option>');
		}
	});

	//var links = ["/pivot/pivot.php?type=pfhd", "/pivot/pivot.php?type=pfhd_diff"];
	function showCompareControl() {
		var suffixes = ["ю", "и"];
		var displays = ["none", "inline-block"];
		var ver_check = document.querySelector(".ver_check");
		var form = ver_check.querySelector('form');
		var hidden_fields = ver_check.querySelectorAll(".hidden_field");
		var suffix = ver_check.querySelector("#suffix");
		var showState = 0;
		ver_check.showState = !ver_check.showState;
		showState = (ver_check.showState) ? 1 : 0;
		suffix.innerHTML = suffixes[showState];		
		
		document.querySelector('[name="type"]').value = (ver_check.showState) ? 'pfhd_diff': 'pfhd';
		for(var i = 0; i < hidden_fields.length; i++) {
			hidden_fields[i].style.display = displays[showState];
		}
	}
	
	window.onload = function() {
		var checkbox = document.querySelector("#compare");
		if(checkbox) {
			if(checkbox.checked) showCompareControl();
		}
	}
</script>
<style type="text/css">
	select[name="first_ver"] {width: 250px;}
	select[name="second_ver"] {width: 250px;}

	.create_version_div {display: none; text-align: right;}
	.create_version_div input, .create_version_div select {width: 250px; font-size: 18px; box-sizing: border-box; margin: 5px;}
	#create_version_2017_btn {padding: 6px;}
	#create_version_2018_btn {padding: 6px;}

	/* 1dp = 8px */
	.ver_check {
		width: 900px;
		height: auto;
		margin: 16px auto; /* 2dp */
		text-align: center;
	}

	.ver_check form {
		margin: 16px 0; /* 2dp */
	}

	.ver_check select {
		font-size: 20px; /* 3dp */
	}

	.ver_check input[type="submit"], .ver_check input[type="button"] {
		font-size: 22px; /* 2dp - 2px default borders */
	}

	.ver_check input[type="button"] {
		margin-left: 30px;
	}

	.ver_select {
		display: inline-block;
		margin-right: 24px;
	}

	.ver_select p {
		text-align: left;
		margin: 8px 0;
		color: #999;
		font-size: 16px; /* 2dp */
	}

	.hidden_field {display: none;}
</style>