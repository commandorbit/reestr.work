select pre.y pfhd_y,pre.fp_article_id,pre.pfhd_article_id,pfhd_article.kosgu kvr,pre.booker_fin_source,pre.zfo_id,{sign_sum} as sum
from  
(
select pfhd.y ,pfhd.fp_article_id,pfhd.pfhd_article_id,
pfhd.booker_fin_source,pfhd.zfo_id,pfhd.sum
from  pfhd  where pfhd_ver_id = {ver_id}
union all

select y ,null ,pfhd_article_id,
booker_fin_source,null,sum
from  pfhd_delta  where pfhd_ver_id = {ver_id}

union all 

select y pfhd_y,null fp_article_id,{pfhd_article_remains_id} pfhd_article_id,
booker_fin_source,null zfo_id,sum
			from tmp_pfhd_remains
) pre left join booker_fin_source bf on pre.booker_fin_source=bf.code
	 left join pfhd_article on pre.pfhd_article_id = pfhd_article.id 
