select pfhd_y,fp_article_id,pfhd_article_id,kvr,booker_fin_source,zfo_id, dogovor_id,fp_year,plan_grafic_id,{sign_sum} as sum
from  
(
select plan.y pfhd_y,plan.fp_article_id,fp_article.{pfhd_article_fld} pfhd_article_id,
fp_article.kvr,fs.booker_fin_source,plan.zfo_id,null as dogovor_id,plan.y fp_year,null as plan_grafic_id, sum
    from plan_dohod plan 
    inner join plan_dohod_ver  v on plan.plan_dohod_ver_id = v.id 
    left join fin_source fs on plan.fin_source_id = fs.id
    left join fp_article on plan.fp_article_id = fp_article.id
    where plan_dohod_ver_id = {plan_dohod_ver_id}
union all     
select v.pfhd_y,v.fp_article_id,v.pfhd_article_id,
v.kvr,v.booker_fin_source,v.zfo_id,v.dogovor_id,v.fp_year,v.plan_grafic_id,v.sum 
from  {sql_rashod}
union all
{sql_remains}
) pre left join booker_fin_source bf on pre.booker_fin_source=bf.code
	 left join pfhd_article on pre.pfhd_article_id = pfhd_article.id 
