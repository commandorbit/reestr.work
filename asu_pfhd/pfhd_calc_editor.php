<?php
define('UTF-8',1);
include $_SERVER['DOCUMENT_ROOT'].'/init.php';

define ("DEKANAT_PATH",$_SERVER['DOCUMENT_ROOT'] .'/../dekanat');

include DEKANAT_PATH . '/modules/data_table/data_table.php';
include DEKANAT_PATH . '/modules/controls/controls.php';
require_once __DIR__ . '/pfhd_calc_table_func.php';
/* include __DIR__.'/editor_func.php'; */
$YEARS = [2019,2020,2021];
$table_id = request_numeric_val('t_id',null);
$table_col_id = sql_get_value('col_pfhd_calc_table_id','pfhd_calc_table',"id=$table_id");
$cols =  pfhd_calc_table_cols($table_col_id);

//$title = $d['title'];
//print_header();

$pre_filter = '';
/*$pre_filter = data_table_filter($data_table_id);*/
$rowsData = pfhd_calc_table_data($table_id,$YEARS);


$title = "АСУ ПФХД Обоснования расходов <br> " . sql_get_value('name','pfhd_calc_table','id='.$table_id);
require 'template/header.php';

load_data_table('v_reestr');

?>

<script type="text/javascript">
$(document).ready(function(){
	
	
	(function (L) {
		
		var div = document.getElementById("data_wrapper");
		var params = {
			rows: <?=json_encode($rowsData,JSON_UNESCAPED_UNICODE); ?>,
			cols: <?=json_encode($cols,JSON_UNESCAPED_UNICODE); ?>,
			div: div,
			url:'/asu_pfhd/pfhd_calc_save_ajax.php',
			editor:L.ModalWindowEditor,
			db_table_name: '<?=$table_id ?>'
		}
		var dTable = new L.DataTable(params);
		dTable.draw();
        dTable.applyFilter();        
        $('#addNewRec').click(function() {dTable.openEditor();});
	})(libUnicorn);
	$('.data-table-editbox button[name="save"]').html('Сохранить');
	$('.data-table-editbox button[name="del"]').html('Удалить');
	$('.data-table-editbox button[name="saveAs"]').html('Сохранить как');
	$('.data-table-editbox').on('show',function(){
		$('body').append('<div id="layer"></div>');
		$('#layer').css('height',$(document).height()+'px');
		$('#layer').fadeIn('fast');
	});
	$('.data-table-editbox .close_button, .data-table-editbox .buttons button').click(function(){	
		$('#layer').fadeOut('fast');
		$('#layer').remove();
	});
});
</script>
<link rel="stylesheet" type="text/css" href="editor.css?ver=<?=VER?>">
<h2 class="kartochka_header"><?=$title?></h2>
<form method="get">
<div id="data_table_filter" class="cover">
	<?=$pre_filter ?>
</div>
</form>
<!--button id="addNewRec">Добавить</button>-->
<div id="data_wrapper" class="data-table-wrap">

</div>
<?php
//print_footer();
?>