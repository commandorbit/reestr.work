<?php 

include $_SERVER['DOCUMENT_ROOT'] . '/init.php';

$year = date('Y');

$sql = "select sum(apa.amount) as sum, apa.asu_pfhd_codes_id,  apc.code, apc.name from asu_pfhd_amounts apa left join asu_pfhd_codes apc on apa.asu_pfhd_codes_id = apc.id 
group by asu_pfhd_codes_id order by asu_pfhd_codes_id";
$rows = sql_rows($sql);

$fin_sources = array(
	'4'=>'GosSubsidy',
	'5'=>'TargetSubsidy',
	'2'=>'IncomeActivityValue'
);

foreach($rows as &$row) {
	foreach ($fin_sources as $b_id=>$tagName) {
		$code_id  = $row['asu_pfhd_codes_id'];
		$sum = sql_get_value('amount',"asu_pfhd_amounts","asu_pfhd_codes_id=$code_id and booker_fin_source_id = $b_id");
		$row[$tagName]=($sum) ? $sum : 0;
	}
}

header('Content-Type: text/xml');
header('Content-Disposition: attachment;filename="ПФХД.xml"');
header('Cache-Control: max-age=0');

$org = get_org_rekvizits();

require (dirname(__FILE__) . '/pfhd/pfhd_header.xml');	
foreach ($rows as $fld) {
	require (dirname(__FILE__) . '/pfhd/pfhd_body.xml');	
}	
require (dirname(__FILE__) . '/pfhd/pfhd_footer.xml');	

function get_org_rekvizits() {
    $org = array();
    $rows = sql_rows("select * from param");
    foreach ($rows as $row) {
        $org[$row['name']]=$row['value'];
    }
    return $org;
}
?>