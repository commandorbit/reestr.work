<?php

include $_SERVER['DOCUMENT_ROOT'] . '/init.php';

$errors = array();

$ver_name = request_val('ver_name', '');
$plan_period_id = request_numeric_val('plan_period_id',0);
$prim = request_val('prim', '');

if (!$ver_name) $errors[] = "Не задано название версии!";
if (!$plan_period_id) $errors[] = "Не задан плановый период!";


if (count($errors) == 0) {
	$has_error = false;
	sql_query("START TRANSACTION");  
    // 3х летним планам год не ставим
	sql_query("INSERT INTO pfhd_ver (name,plan_period_id,prim) VALUES ('$ver_name','$plan_period_id','$prim')");
	$pfhd_ver_id = sql_last_id();
	if ($pfhd_ver_id) {
		$plan_period = sql_get_array("plan_period","id='$plan_period_id'");
		$y_from = $plan_period['y_from'];
		$y_to = $plan_period['y_to'];
		$pfhd_article_nds_id = sql_get_value("id","pfhd_article","is_nds=1 AND y=$y_from");
		
		$pfhd_table = 'pfhd';
		$pfhd_article_fld_id = 'pfhd_article_' . $plan_period_id . '_id';
		/*
		$q1 = "INSERT INTO pfhd (smeta_id,y,sum,fp_article_id,pfhd_article_id,zfo_id,pfhd_ver_id,booker_fin_source )
				SELECT 
					 ss.smeta_id
					,smeta.fp_year_id
					,Sum(amount*percent_exbudjet/100) AS sum
					,ss.fp_article_id
					,fp_article.pfhd_article_2018_id
					,IFNULL(ss.zfo_id,smeta.zfo_id) zfo_id
					,$pfhd_ver_id AS pfd_ver_id
					,2 AS booker_fin_source
				FROM smeta_sostav ss 
				INNER JOIN smeta ON ss.smeta_id = smeta.id 
				INNER JOIN fp_article ON ss.fp_article_id = fp_article.id
				WHERE (smeta.fp_year_id>=$y_from And smeta.fp_year_id<=$y_to) 
						AND percent_exbudjet<>0 
						AND not_in_pfhd = 0
						AND fp_article.pfhd_article_2018_id Is Not Null
						AND smeta.fin_source_id is null
				GROUP BY ss.smeta_id,ss.fp_article_id, fp_article.pfhd_article_2018_id";

		$q2 = "INSERT INTO pfhd (smeta_id,y,sum,fp_article_id,pfhd_article_id,zfo_id,pfhd_ver_id,booker_fin_source)
				SELECT 
					 ss.smeta_id
					,smeta.fp_year_id
					,Sum(amount*percent_budjet/100) AS sum
					,ss.fp_article_id
					,fp_article.pfhd_article_2018_id
					,IFNULL(ss.zfo_id,smeta.zfo_id) zfo_id
					,$pfhd_ver_id AS pfd_ver_id
					,4 AS booker_fin_source
				FROM smeta_sostav ss 
				INNER JOIN smeta ON ss.smeta_id = smeta.id 
				INNER JOIN fp_article ON ss.fp_article_id = fp_article.id
				WHERE (smeta.fp_year_id>=$y_from And smeta.fp_year_id<=$y_to) 
						AND percent_budjet<>0 
						AND not_in_pfhd = 0
						AND fp_article.pfhd_article_2018_id Is Not Null
						AND smeta.fin_source_id is null
				GROUP BY ss.smeta_id,ss.fp_article_id, fp_article.pfhd_article_2018_id";
		*/		
		$q3 = "INSERT INTO pfhd(smeta_id,y,sum,fp_article_id,pfhd_article_id,zfo_id,booker_fin_source,pfhd_ver_id)
				SELECT 
					 ss.smeta_id
					,smeta.fp_year_id
					,sum(ss.amount)
					,ss.fp_article_id
					,fp_article.$pfhd_article_fld_id
					,IFNULL(ss.zfo_id,smeta.zfo_id) zfo_id
					,fin_source.booker_fin_source
					,$pfhd_ver_id AS pfhd_ver_id
				FROM smeta_sostav ss 
				INNER JOIN smeta ON ss.smeta_id = smeta.id
				INNER JOIN fp_article ON ss.fp_article_id = fp_article.id
				INNER JOIN fin_source ON ss.fin_source_id = fin_source.id
				WHERE (smeta.fp_year_id>=$y_from And smeta.fp_year_id<=$y_to) 
						AND fp_article.$pfhd_article_fld_id Is Not Null
						AND not_in_pfhd = 0
				GROUP BY ss.smeta_id,ss.fp_article_id, fp_article.$pfhd_article_fld_id, fin_source.booker_fin_source";

				
// Доходы				
		$q4 = "INSERT INTO pfhd (smeta_id,y,sum,fp_article_income_id,pfhd_article_id,zfo_id,booker_fin_source,pfhd_ver_id )
				SELECT 
					 si.smeta_id
					,smeta.fp_year_id
					,sum(si.amount)
					,si.fp_article_income_id
					,fp.$pfhd_article_fld_id					
					,smeta.zfo_id
					,fin_source.booker_fin_source
					,$pfhd_ver_id AS pfhd_ver_id
				FROM smeta_income si 
				INNER JOIN smeta ON si.smeta_id = smeta.id
				INNER JOIN fp_article_income fp ON si.fp_article_income_id = fp.id
				INNER JOIN fin_source ON si.fin_source_id = fin_source.id
				WHERE (smeta.fp_year_id>=$y_from And smeta.fp_year_id<=$y_to) 
				GROUP BY si.smeta_id,si.fp_article_income_id, fp.$pfhd_article_fld_id, fin_source.booker_fin_source";
// Доходы НДС
		$q5 = "INSERT INTO pfhd (smeta_id,y,sum,fp_article_income_id,pfhd_article_id,zfo_id,booker_fin_source,pfhd_ver_id )
				SELECT 
					 si.smeta_id
					,smeta.fp_year_id
					,-sum(round(si.amount/(100+nds)*nds,2))
					,si.fp_article_income_id
					,$pfhd_article_nds_id					
					,smeta.zfo_id
					,fin_source.booker_fin_source
					,$pfhd_ver_id AS pfhd_ver_id
				FROM smeta_income si 
				INNER JOIN smeta ON si.smeta_id = smeta.id
				INNER JOIN fp_article_income fp ON si.fp_article_income_id = fp.id
				INNER JOIN fin_source ON si.fin_source_id = fin_source.id
				WHERE si.nds<>0 AND (smeta.fp_year_id>=$y_from And smeta.fp_year_id<=$y_to) 
				GROUP BY si.smeta_id,si.fp_article_income_id, fp.$pfhd_article_fld_id, fin_source.booker_fin_source";
				
		$queries = [$q3,$q4];
		if ($pfhd_article_nds_id) {
			$queries[] = $q5;
		}
		foreach ($queries as $q) {
			if (sql_query($q)) {
				// do notning
			} else {
				$has_error = true;
				break;
			}
		}
	} else {
		$has_error = true;
	}

	if ($has_error) {
		$errors[] = "Возникла ошибка при сохранении версии. Обратитесь к разработчикам.";
		sql_query("ROLLBACK");
	} else {
		sql_query("COMMIT");
	}
}

if($cnt = count($errors)) {
	$_SESSION['SYSTEM_MESSAGE'] = '';
	for($i = 0; $i < $cnt; $i++) {
		$_SESSION['SYSTEM_MESSAGE'] .= $errors[$i] . "<br />";
	}
} else {
	$_SESSION['SYSTEM_MESSAGE_SUCCESS'] = "Версия успешно сохранена";
	header("Location: " . $_SERVER['HTTP_REFERER']);
}
