<?php
require_once $_SERVER['DOCUMENT_ROOT'] . '/init.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/modules/asu_pfhd_sender.php';
define("PROCESSING_STATUS", "Processing");

$log_table = 'asu_pfhd_load_log';
$log_rows = sql_rows("SELECT `id`, `key` FROM `$log_table` WHERE status = '" . PROCESSING_STATUS . "'");

foreach($log_rows as $row) {
	$row_id = $row['id'];
	$key = $row['key'];
	$answer = check_key($key);
	if($status = answer_status($answer) !== PROCESSING_STATUS) {
		sql_query("UPDATE `$log_table` SET status = '$status' WHERE id = '$row_id'");
	}
}

$_SESSION['SYSTEM_MESSAGE_SUCCESS'] = "Статусы успешно обновлены";
header("Location: " . $_SERVER['HTTP_REFERER']);