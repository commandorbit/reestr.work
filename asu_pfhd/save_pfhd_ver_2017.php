<?php

include $_SERVER['DOCUMENT_ROOT'] . '/init.php';
$ver_table = 'stat_pfhd_ver';
$stat_table = 'stat_pfhd';
$last_insert_id = 0;
$errors = array();

$ver_name = request_val('ver_name', '');
$prim = request_val('prim','');
$plan_dohod_ver_id = request_numeric_val('plan_dohod_ver_id',0);
$plan_period_id = request_numeric_val('plan_period_id',0);

if (!$ver_name) $errors[] = "Не задано название версии!";
if (!$plan_dohod_ver_id) $errors[] = "Не задана версия плана доходов!";
if (!$plan_period_id) $errors[] = "Не задан плановый период!";


if (count($errors) == 0) {
	sql_query("START TRANSACTION");  
    // 3х летним планам год не ставим
	sql_query("INSERT INTO $ver_table (name,plan_dohod_ver_id,plan_period_id,prim) VALUES ('$ver_name',$plan_dohod_ver_id,'$plan_period_id','$prim')");
	$last_insert_id = sql_last_id();
	if ($last_insert_id) {
		sql_query("INSERT INTO $stat_table (stat_pfhd_ver_id, pfhd_y, dogovor_id, dogovor_status_id, contragent_id, d, fp_year, zfo_id, document_type_id, fp_article_id, pfhd_article_id, booker_fin_source, y, sum, plan_grafic_id) 
		 SELECT '$last_insert_id',pfhd_y,dogovor_id, dogovor_status_id, contragent_id, d, fp_year, zfo_id, document_type_id, fp_article_id, pfhd_article_id, booker_fin_source, pfhd_y, sum, plan_grafic_id 
		 FROM (".ReestrDB::pfhd_rashod_sql($plan_period_id).") pre  ");
	} else {
		$errors[] = "Возникла ошибка при сохранении версии. Обратитесь к разработчикам.";
		sql_query("ROLLBACK");
	}
	sql_query("COMMIT");
}

if($cnt = count($errors)) {
	$_SESSION['SYSTEM_MESSAGE'] = '';
	for($i = 0; $i < $cnt; $i++) {
		$_SESSION['SYSTEM_MESSAGE'] .= $errors[$i] . "<br />";
	}
} else {
	$_SESSION['SYSTEM_MESSAGE_SUCCESS'] = "Версия успешно сохранена";
	header("Location: " . $_SERVER['HTTP_REFERER']);
}