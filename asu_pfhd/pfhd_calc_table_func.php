<?php
define ('CALC_PREFIX','pfhd_calc_table');

function save_pfhd_calc_table_val($_DATA) {
	$row_id = isset($_DATA['row_id']) && $_DATA['row_id'] ? $_DATA['row_id'] : null;
	$table_id = (int) $_DATA['table_id'];
	$table_col_id = sql_get_value('col_pfhd_calc_table_id','pfhd_calc_table','id='.$table_id);
	$y = (int) $_DATA['y'];


	$vals =['pfhd_calc_table_row_id'=>$row_id,'pfhd_calc_table_id'=>$table_id,'y'=>$y,'C3'=>null,'C4'=>null,'C5'=>null,'C6'=>null,'C7'=>null,'C7'=>null,'C9'=>null];
	$has_data = false;
	foreach($_DATA as $col=>$val) {
		if (preg_match('/^C[0-9]+$/',$col)) {
			$n = substr($col,1);
			$params = sql_get_array('pfhd_calc_table_col',"pfhd_calc_table_id=$table_col_id  AND n=$n");
			if ($n>2 && !$params['formula'] && ($val || $val==='0')) { // ѕервые две колонки всегда пропускаем (name и номер)
				$val = str_replace(' ','',$val);
				$val = str_replace(',','.',$val);
				$vals[$col] = $val;
				$has_data = true;
			}
		}
	}
	$w_arr = ['pfhd_calc_table_id'=>$table_id, 'y'=>$y, 'pfhd_calc_table_row_id'=>$row_id ];
	$w = _make_where($w_arr);


	$id = sql_get_value('id','pfhd_calc_table_value',$w);
	if ($id) $vals['id']=$id;
	if ($has_data) save('pfhd_calc_table_value',$vals);

	$has_itogo = request_numeric_val('has_itogo',false);
	if ($has_itogo) {
		$id = sql_get_value('id','pfhd_calc_table_itogo',"pfhd_calc_table_id='$table_id' AND y='$y'");
		$vals = ['pfhd_calc_table_id'=>$table_id,'y'=>$y];
		if ($id) {
			$vals['id'] = $id;
		}
		$cols = ['budget','budget_special','budget_capital','oms','vne_budget','vne_budget_grant'];
		$has = sql_get_array("pfhd_calc_table","id='$table_id'");
		foreach($cols as $col) {
			$vals[$col] = null;
			if ($has['has_'.$col] && isset($_DATA[$col])) {
				$v = $_DATA[$col];
				$v = str_replace(' ','',$v);
				$v = str_replace(',','.',$v);
				$v = floatval($v);
				$vals[$col] = $v;
			}
		}
		save('pfhd_calc_table_itogo',$vals);

	}
}

function _make_where($vals) {
	$w = [];
	foreach($vals as $fld=>$val) {
		if ($val === null) 
			$w[] = " $fld is null";
		else 
			$w[] = " $fld='".sql_escape($val)."'";
	}
	return implode(" AND ",$w);
}



/*     ************************       */

function pfhd_calc_table_data_row($table_id,$y,$row_id) {
	$rows = pfhd_calc_table_data($table_id,[$y]);
	foreach($rows as $r) {
		if ($r['row_id']==$row_id) {
			return $r;
		}
	}
	return null;
}

function pfhd_calc_table_itogo_row($table_id,$y) {
	$rows = pfhd_calc_table_data($table_id,[$y]);
	foreach($rows as $r) {
		if ($r['has_itogo']) {
			return $r;
		}
	}
	return null;
}


function pfhd_calc_table_data($table_id,$YEARS) {
	$table_col_id = sql_get_value('col_pfhd_calc_table_id','pfhd_calc_table',"id=$table_id");	
	$cols =  pfhd_calc_table_cols($table_col_id);

	$rowsData = pfhd_calc_table_rows($table_id,$YEARS);
	$itogo_col = 'C' . sql_get_value('itogo_col','pfhd_calc_table',"id=$table_id");
	$rowsData = calc_formulas($rowsData,$cols);
	return $rowsData;
}

function calc_formulas($rows,$cols) {
	$y = 0;
	$itogo = 0;
	$sub_itogs = [];
	
	$itogo_fld = null;
	foreach($cols as $fld=>$c) {
		if (isset($c['is_itogo_col']) && $c['is_itogo_col']) {
			$itogo_fld = $fld;
			break;
		}
	}
	
	foreach($rows as &$rr) {
		if ($rr['y']!=$y) {
			$y = $rr['y'];
			$itogo = 0;
		}
		$is_itogo_row = isset($rr['has_itogo']) && $rr['has_itogo'];
		if (!$is_itogo_row) {
			foreach($cols as $fld=>$c) {
				$$fld = (isset($rr[$fld])) ? $rr[$fld] : null;
			}
			foreach($cols as $fld=>$c) {
				$is_itogo_col = isset($c['is_itogo_col']) && $c['is_itogo_col'];
				if (isset($c['formula'])) {
					eval('$$fld=round('.$c['formula'].',2);');
					$rr[$fld] = $$fld;
				}
				if ($fld == $itogo_fld) {
					if ((!isset($rr['not_used_in_itogo']) || $rr['not_used_in_itogo']==0)) {
						$itogo += $rr[$fld];
					}
				}
			}
		} else {
			$rr[$itogo_fld] = $itogo;
			$rr['delta'] = round($rr['sum'] - $itogo,2);			
		}
	}
	unset($rr);
	
	// подсчитаем пользовательские подитоги по строкам
	// если в другом порядке - собьется итоговая строка
	foreach($rows as &$rr) {
		if (isset($rr['is_user_sum']) && $rr['is_user_sum']) {
			$sum_rows = json_decode($rr['user_sum_rows']);
			
			$s = 0;
			foreach ($sub_itogs[$rr['y']] as $r=>$v) {
				if (in_array($r,$sum_rows)) {
					$s+=$v;
				}
			}
			$rr[$itogo_fld]=round($s,2);
		}
	}
	unset($rr);
	return $rows;
}

function pfhd_calc_table_cols($table_id) {
	$cols = [];
	
	$cols['table_id'] = ['hidden'=>true,'readonly'=>true,'label'=>'Таблица Id','hide_in_edior'=>true];
	$cols['row_id'] = ['hidden'=>true,'readonly'=>true,'label'=>'RowId','hide_in_edior'=>true];
	$cols['r'] = ['hidden'=>true,'readonly'=>true,'label'=>'RowId','hide_in_editor'=>true];
	$cols['detail_code'] = ['hidden'=>true,'readonly'=>true,'label'=>'detail_code','hide_in_editor'=>false];	
	$cols['nom'] = ['width'=>'30','label'=>'№','type'=>'autonumber'];
	$cols['y'] = ['width'=>'30','label'=>'Год','type'=>'int'];
	$cols['has_itogo'] = ['hidden'=>true,'readonly'=>true,'label'=>'has_itogo','hide_in_editor'=>true];	
	$itogo_col = sql_get_value("itogo_col","pfhd_calc_table","id=$table_id");
	$c_rows = sql_rows("select title,n,formula from pfhd_calc_table_col WHERE pfhd_calc_table_id='$table_id' order by n");
	foreach($c_rows as $r) {
		$fld = 'C' .$r['n'];
		$d = ['label'=>$r['title'],'width'=>'100'];
		if ($fld == 'C2') { // название показателя
			$d['width'] = 300;
		}
		if ($r['formula']) {
			$d['readonly'] = true;
			$d['formula'] = $r['formula'];
		}
		if ($fld == 'C2') {
			$d['readonly'] = true;
		}
		$d['is_itogo_col'] = ($r['n'] == $itogo_col);
		$cols[$fld] = $d;
	}
	$cols = array_merge($cols,pfhd_calc_table_itogo_cols()) ;

	return $cols;
}

function pfhd_calc_table_itogo_cols() {
	return [ 
		 'budget'=> ['width'=>'100','label'=>'Субсидия ГЗ','not_in_editor'=>true]
		,'budget_special'=> ['width'=>'100','label'=>'Субсидия абз. 2 пункта 1 статьи 78.1 БК РФ','not_in_editor'=>true]
		,'budget_capital'=> ['width'=>'100','label'=>'Субсидия На Кап.Вложения','not_in_editor'=>true]
		,'oms'=> ['width'=>'100','label'=>'Средства ОМС','not_in_editor'=>true]
		,'vne_budget'=> ['width'=>'100','label'=>'ВнеБюджет','not_in_editor'=>true]
		,'vne_budget_grant'=> ['width'=>'100','label'=>'ВнеБюджет Гранты','not_in_editor'=>true]
		,'sum'=>['width'=>'100','label'=>'Итого','not_in_editor'=>true]
		,'delta'=>['width'=>'100','label'=>'Дельта','not_in_editor'=>true]
	];
}

function pfhd_calc_table_rows($table_id,$years) {
	$out_rows = [];
	foreach($years as $y) {
		$rows = [];
		$sql = "select r.id, r.title, r.not_used_in_itogo, r.pfhd_calc_table_row_type_id, r.pfhd_calc_table_dict_val_id dict_val_id, d.name dict_val_name, r.ord
			FROM pfhd_calc_table_row r LEFT join pfhd_calc_table_dict_val d ON r.pfhd_calc_table_dict_val_id = d.id
			where pfhd_calc_table_id = $table_id order by ord";
		$d_rows = sql_rows($sql);
		foreach($d_rows as $r) {
			$row_id = $r['id'];
			$row['ord'] = $r['ord'];
			$row=['y'=>$y,'table_id'=>$table_id,'has_itogo'=>0,'row_id'=>$row_id];
			$row['not_used_in_itogo']=$r['not_used_in_itogo'];
			$row['type'] = $r['pfhd_calc_table_row_type_id'];
			$row['C2'] = $r['dict_val_id'] ? $r['dict_val_name'] : $r['title'];
			$vals=sql_get_array('pfhd_calc_table_value',"pfhd_calc_table_id='$table_id' AND pfhd_calc_table_row_id='$row_id' AND y='$y'");
			
			if ($vals) {
				for ($i=3;$i<=9;$i++) {
					$row['C'.$i]=$vals['C'.$i];
				}				
			}
			
			for ($i=3; $i<=9; $i++) {
				$row['C'.$i]=isset($vals['C'.$i]) ? $vals['C'.$i] : null;
			}
			
			$rows[] = $row;
		}
		//ИТОГО
		$r_itogo = ['y'=>$y,'table_id'=>$table_id,'has_itogo'=>1];
		$cols = array_keys(pfhd_calc_table_itogo_cols());
		$has = sql_get_array("pfhd_calc_table","id=$table_id");
		$itogo = sql_get_array("pfhd_calc_table_itogo","pfhd_calc_table_id=$table_id AND y=$y");
		$sum = 0;
		foreach($cols as $col) {
			if (!in_array($col,['sum','delta']) && $has['has_'.$col] && isset($itogo[$col])) {
				$sum += $r_itogo[$col] = $itogo[$col];
			}
		}
		$r_itogo['sum'] = round($sum,2);

/*		
		if (count($rows)==1) {
			$rows[0] = array_merge($rows[0],$r_itogo);
		} else {
			$r_itogo['r'] = 9999;
			$r_itogo['C2'] = 'Итого';
			$rows[] = $r_itogo;
		}
*/		
		$r_itogo['row_id'] = 'itogo';
		$r_itogo['C2'] = 'Итого';
		$r_itogo['ord'] = 999999999;
		$rows[] = $r_itogo;
		//uasort($rows,'sort_rows');
		$out_rows = array_merge($out_rows,$rows);
	}
	return $out_rows;
}

function sort_rows($a,$b) {
	if ($a['ord'] == $b['ord']) {
		return 0;
	}
	return $a['ord'] < $b['ord'] ? -1 : 1;
}
