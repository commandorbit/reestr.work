<?php
require $_SERVER['DOCUMENT_ROOT'].'/init.php';
require SHARED_PHP_PATH."/controls/controls.php";
$controls = new controls();

$plan_period_id_default = 2017;
$plan_period_id = request_numeric_val('pp_id',$plan_period_id_default);

$plan_dohod_ver_id_def = sql_get_value("MAX(id)","plan_dohod_ver","plan_period_id='$plan_period_id'");
$plan_dohod_ver_id = request_numeric_val('pdv_id',$plan_dohod_ver_id_def);

$plan_periods = sql_to_assoc("SELECT id, name FROM plan_period ORDER BY name ");
$plan_dohod_vers = array();
$plan_dohod_vers['null'] = '';
$plan_dohod_vers += sql_to_assoc("SELECT id, name FROM plan_dohod_ver 
	WHERE plan_period_id='$plan_period_id' ORDER BY name ");

$table = 'plan_dohod';

$filter['f_plan_dohod_ver_id']=$plan_dohod_ver_id;
// Если в плановом периоде несколько лет ставим js - фильтр на первый год 
$plan_period_row = sql_get_array('plan_period',"id=$plan_period_id");
if ($plan_period_row['y_from']!=$plan_period_row['y_to']) {
	$filter['ff_y']=$plan_period_row['y_from'];
}
$TD = newTD($table,$filter);

$used_in_pfhd = sql_get_value("count(*)","stat_pfhd_ver","plan_dohod_ver_id='$plan_dohod_ver_id'");
if (!is_numeric($plan_dohod_ver_id) || $used_in_pfhd) {
	$TD->may_edit = false;
}

$TD->columns['plan_dohod_ver_id']['hidden'] = true;
$TD->columns['plan_dohod_ver_id']['editable'] = false;
$TD->columns['plan_dohod_ver_id']['default_value'] = $plan_dohod_ver_id;
$TD->columns['y']['default_value'] = sql_get_value("y_from","plan_period","id='$plan_period_id'");

$title = get_header($table);
$description = get_table_description($table);

require 'template/header.php';


?>
<link rel="stylesheet" type="text/css" href="/css/report_style.css">
<script type="text/javascript">
	$(document).ready(function(){
		$('input[name="create_version"]').click(function(){
			$('#create_version_div').dialog({
				title: 'СОЗДАТЬ ВЕРСИЮ ПЛАНА ДОХОДОВ',
				width: 450,
				height: 250,
				maxWidth: 450,
				maxHeight: 300,
			});
		});
		$('select[name="plan_period_id"]').change(function(){
			$.ajax({
				url: '/modules/ajax.php?func=get_plan_dohod_options_by_period_id&arg1='+$(this).val()
			}).done(function(data){
				data = '<option value="0"></option>' + data;
				$('select[name="plan_dohod_ver_id"]').html(data);
			});
		});

		$('select[name="pp_id"]').change(function(){
			$.ajax({
				url: '/modules/ajax.php?func=get_plan_dohod_options_by_period_id&arg1='+$(this).val()
			}).done(function(data){
				data = '<option value="0"></option>' + data;
				$('select[name="pdv_id"]').html(data);
			});
		});
		$('input[name="save_version"]').click(function(){
			var ver_name = $('#create_version_div input[name="ver_name"]').val();
			if (!ver_name) {
				alert('Не введено название новой версии!');
				return false;
			}
		});
	});
</script>

<div class="header-tools">
	<h1><?=$title?></h1>
</div>
<p style="text-align: center; color: black;">
	<?=$description?>
</p>
<form method="GET">
<div id="plan_dohod_filter">
	<label>
		Период:
		<?php $controls->print_select('pp_id',$plan_periods,$plan_period_id); ?>
	</label>
	<label>
		Версия:
		<?php $controls->print_select('pdv_id',$plan_dohod_vers,$plan_dohod_ver_id); ?>
	</label>
	<input type="submit" name="change" value="Ок">
	<?php if (is_admin() || user_may('may_edit_plan_dohod')) : ?>
		<input type="button" name="create_version" value="Создать новую версию">
	<?php endif ?>
</div>		
</form>

<div id="create_version_div" style="display: none;">
	<form method="get" action="/plan_dohod/plan_dohod_save.php">
	<?php 
	$controls->print_select_label('plan_period_id',$plan_periods,$plan_period_id,'Период');
	$controls->print_select_label('plan_dohod_ver_id',$plan_dohod_vers,$plan_dohod_ver_id,'Скопировать из');	
	$controls->print_input_label('ver_name','','Название');
	$controls->print_submit('save_version','Сохранить');
	?>
	</form>
</div>
<?php $TD->out_js();?>
<?php
require 'template/footer.php';
?>