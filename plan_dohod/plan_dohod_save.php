<?php 
include $_SERVER['DOCUMENT_ROOT'] . '/init.php';

$ver_table = 'plan_dohod_ver';
$data_table = 'plan_dohod';
$errors = array();

$plan_period_id = request_numeric_val('plan_period_id',null);
$plan_dohod_ver_id = request_numeric_val('plan_dohod_ver_id',null);
$ver_name = request_val('ver_name',null);
// оставлено для совместимости, т.к. y используется много где в реестре
$y = sql_get_value("y_from","plan_period","id='$plan_period_id'"); 


if (!$plan_period_id) {
	$errors[] = "Версия не сохранена. Не указан период плана доходов.";
} elseif (!$ver_name) {
	$errors[] = "Версия не сохранена. Не введено имя версии.";
} else {
	sql_query("START TRANSACTION");
	sql_query("INSERT INTO $ver_table (name, y, plan_period_id) VALUES ('$ver_name','$y','$plan_period_id')");
	$last_insert_id = sql_last_id();
	if ($plan_dohod_ver_id) {
		if ($last_insert_id) {
			sql_query("INSERT INTO $data_table (fp_article_id, zfo_id, fin_source_id, name, y, plan_dohod_ver_id, sum) 
			 SELECT fp_article_id, zfo_id, fin_source_id, name, y, $last_insert_id, sum
			 FROM $data_table 
			 WHERE plan_dohod_ver_id='$plan_dohod_ver_id' ");
		} else {
			$errors[] = "Возникла ошибка при сохранении версии. Обратитесь к разработчикам.";
			sql_query("ROLLBACK");
		}
	}
	sql_query("COMMIT");
}

if($cnt = count($errors)) {
	$_SESSION['SYSTEM_MESSAGE'] = '';
	for($i = 0; $i < $cnt; $i++) {
		$_SESSION['SYSTEM_MESSAGE'] .= $errors[$i] . "<br />";
	}
} else {
	$_SESSION['SYSTEM_MESSAGE_SUCCESS'] = "Версия успешно сохранена";
	header("Location: " . $_SERVER['HTTP_REFERER']);
}

?>