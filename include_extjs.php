<?
$theme_name = 
[
    1 => "orange",
    2 => "gray",
    3 => "blue",
    4 => "green"
];

if (isset($_REQUEST['t']))
{
	$table = sql_escape($_REQUEST['t']);
	$table = explode('/', $table)[0];
}
else
	$table = '';

if (isset($_REQUEST['auto']))
	$auto = 1;
else
	$auto = 0;

//echo '<pre>'; echo json_encode($_REQUEST); /*var_dump($_SERVER);*/ echo '</pre>';

$manifest = (isset($_GET['ext_test'])) ? 'ext_test' : $theme_name[$_SESSION["palette"]];
$userPermission = [
	 'smeta_admin'=>user_has_permission('smeta_admin')
	,'may_create_zayav'=>may_create_zayav()
	,'may_delete_zayav'=>user_may('delete_zayav')
	,'may_edit_dogovor_gz'=>is_admin() || user_has_permission('may_edit_dogovor_gz')
	,'may_edit_plat'=>is_admin() || user_may_edit_plat()
	,'is_admin'=>is_admin()
	,'user_zfo_ids'=>user_zfo_ids()
];
?>
<script>
var Ext = Ext || {};
Ext.manifest = '/extapp/<?=$manifest?>.json';
Ext.myLoadParams = {};
Ext.myLoadParams.userId = <?=$user_id?>;
Ext.myLoadParams.userPermission = <?=json_encode($userPermission); ?>;
Ext.myLoadParams.auto = <?=$auto?>;
Ext.myLoadParams.REQUEST_URI = '<?=$_SERVER["REQUEST_URI"]?>';
Ext.myLoadParams.REQUEST = JSON.parse('<?=json_encode($_REQUEST);?>');
Ext.myLoadParams.altUrl = Ext.myLoadParams.REQUEST_URI.replace(/tedit_new.php/g, 'tedit_old.php');	
</script>
