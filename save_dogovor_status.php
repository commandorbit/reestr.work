<?php

include $_SERVER['DOCUMENT_ROOT'] . '/init.php';
include $_SERVER['DOCUMENT_ROOT'] . '/modules/save.php';

if(!array_key_exists('dogovor_id', $_POST)) die('Не указан id договора, попробуйте повторить операцию снова.');

$dogovor_id = sql_escape($_POST['dogovor_id']);

if(isset($_POST['save_status'])) {
	$status_id = sql_escape($_POST['dogovor_status']);

	$save_data = array('id'=>$dogovor_id, 'dogovor_status_id' => $status_id);

	$dogovor_zfo = sql_get_value('zfo_id', 'dogovor', "id='$dogovor_id'");

	$sostav_details = "SELECT fin_source_id, amount FROM dogovor_sostav WHERE dogovor_id='$dogovor_id' GROUP BY fin_source_id";

	$sostav_fin = sql_rows($sostav_details);

	$use_limit = sql_get_value('use_limit', 'dogovor_status', "id='$status_id'");

	if($use_limit == 1 && !dogovorCheckLimit($dogovor_id)) {
		$_SESSION['SYSTEM_MESSAGE'] = SystemMessage::get();
	} else {
		save('dogovor', $save_data);
		$_SESSION['USER_MESSAGE']['SUCCESS'] = 'Статус договора успешно обновлен!';
	}
} else if(isset($_POST['save_ku'])) {
	$dogovor_ku = 0;

	if(isset($_POST['ku'])) {
		$dogovor_ku = 1;
		
		sql_query("INSERT INTO dogovor_sostav_plan SELECT 0, dogovor_id, fp_article_id, fin_source_id, description, fp_object_id, unit_id, quantity, period_id, amount FROM dogovor_sostav WHERE dogovor_id='$dogovor_id'");
	}

	sql_query("UPDATE dogovor SET concluded_ku = '$dogovor_ku' WHERE id = '$dogovor_id'"); 
}

header('Location: ' . $_SERVER['HTTP_REFERER']);

//save();

?>