<?php
require_once($_SERVER['DOCUMENT_ROOT'].'/init.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/modules/mailer.php');
$message = '';
$error = '';

$lostpass_link = request_val('s','');
$email = request_val('email','');

//AVD
if (isset($_COOKIE['last_user_id']))
	$last_user_id = $_COOKIE['last_user_id'];
else 
	$last_user_id = 0;

if ($last_user_id)
{
	$r = sql_rows("SELECT * FROM user WHERE id = '$last_user_id'");
	$_SESSION["palette"] = $r[0]['palette'];
}
if (!isset($_SESSION["palette"])) $_SESSION["palette"] = 1;
if (!$_SESSION["palette"]) $_SESSION["palette"] = 1;
//

$show_controls = ($lostpass_link) ? 0 : 1;
if ($lostpass_link) {
	$row = sql_get_array("`user`","lostpass_link='$lostpass_link'");
	if (!$row) $error = 'Неверная ссылка!';
	elseif ($row['lostpass_fire_time']<time()) $error = 'Время действия ссылки истекло!';
	else {
		if (!$row['email']) $error = 'В базе не установлен e-mail! ';
		elseif (!mail::is_valid_email($row['email'])) $error = 'В базе установлен некорректный e-mail! ';
		else {
			$new_pass = passgen();
			sql_query("UPDATE `user` SET pass='".md5($new_pass)."', lostpass_link=null, lostpass_fire_time=null WHERE id='".$row['id']."'");
			$s = '
			Смена доступов от <b><a href="http://'.$_SERVER['SERVER_NAME'].'/">'.$_SERVER['SERVER_NAME'].'</b></a><br>
			Ваш логин <b>'.$row['login'].'</b><br>
			Ваш новый пароль <b>'.$new_pass.'</b><br>
			';
			mail::send($row['email'],'Новые доступы от '.$_SERVER['SERVER_NAME'],$s,'no-reply@unecon.ru');
			$message = 'Новый пароль отправлен на ваш e-mail!';
		}
	}
} elseif (isset($_POST['send'])) {
	if (!$email) $error = 'Не заполнено поле "e-mail"!';
	elseif (!mail::is_valid_email($email)) $error = 'Введен некорректный e-mail!';
	else {
		if ($row = sql_get_array("`user`","email='$email'")) {
			$lostpass_link = md5($email . rand(1, 99999));
			$lostpass_fire_time = time() + 15*60;
			sql_query("UPDATE `user` SET lostpass_link='$lostpass_link', lostpass_fire_time='$lostpass_fire_time' 
			WHERE id='".$row['id']."' ");
			$s = '
			Для получения доступов пройдите по <b><a href="http://'.$_SERVER['SERVER_NAME'].'/lost_pass.php?s='.$lostpass_link.'">ссылке</a></b><br>
			';
			mail::send($row['email'],'Ссылка для получения доступов от '.$_SERVER['SERVER_NAME'],$s,'no-reply@unecon.ru');
			$message = 'Ссылка для получения доступов отправлена на ваш e-mail!';
		} else $error = 'Данный e-mail не зарегистрован в системе!';
	}
}

$title = 'Восстановление пароля';
?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8" />
	<title><?=$title?> | Парус</title>
	<link rel="icon" type="image/png" href="/img/ico/favicon16.ico" />		
	<!--
	-->
	<link rel="stylesheet" type="text/css" href="/assets/css/style.css">
	<link rel="stylesheet" type="text/css" href="/css/common.css">
	<meta http-equiv="Pragma" content="no-cache">
</head>
<link rel="stylesheet" type="text/css" href="/css/background<?=$_SESSION["palette"]?>.css">
<link rel="stylesheet" type="text/css" href="/css/theme<?=$_SESSION["palette"]?>.css">
<body>
<div class="logotype-img">
	<img src="assets/images/logo.png" />
</div>

<form method="post" action="">
	<div class="login-card" data-moveable="true">
		<center>Система управленческого планирования (СУП)</center>
		<h1>Восстановление пароля</h1><br>

		<form>
			<input type="text" name="email" placeholder="Введите ваш Email" autocomplete="off">
			<input type="submit" name="send" class="login login-submit" value="Выслать пароль">
		</form>
		
		<div align="center"><a href="/login.php" style="pacity: 1">Войти в систему</a></div>
	</div>
</form>

<?php 
	$color = ($message != '') ? "green" : "red";
?>

<h3 style="color: <?=$color?>"><?= ($message != '') ? $message : $error ?></h3>

</body>
</html>
<script src="/js/avd.js"></script>
<script>
	avd.init();
</script>
